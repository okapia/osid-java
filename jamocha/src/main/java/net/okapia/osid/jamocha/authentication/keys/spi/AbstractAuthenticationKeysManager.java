//
// AbstractAuthenticationKeysManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAuthenticationKeysManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.authentication.keys.AuthenticationKeysManager,
               org.osid.authentication.keys.AuthenticationKeysProxyManager {

    private final Types keyRecordTypes          = new TypeRefSet();
    private final Types keySearchRecordTypes    = new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractAuthenticationKeysManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAuthenticationKeysManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a key lookup service is supported. A key lookup service 
     *  defines methods to access keys. 
     *
     *  @return <code> true </code> if key lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyLookup() {
        return (false);
    }


    /**
     *  Tests if a key search service is supported. 
     *
     *  @return <code> true </code> if key search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeySearch() {
        return (false);
    }


    /**
     *  Tests if a key administrative service is supported. 
     *
     *  @return <code> true </code> if key admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyAdmin() {
        return (false);
    }


    /**
     *  Tests if key notification is supported. Messages may be sent when keys 
     *  are created, modified, or deleted. 
     *
     *  @return <code> true </code> if key notification is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyNotification() {
        return (false);
    }


    /**
     *  Tests if retrieving mappings of keys and agencies is supported. 
     *
     *  @return <code> true </code> if key agency mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyAgency() {
        return (false);
    }


    /**
     *  Tests if managing mappings of keys and agencies is supported. 
     *
     *  @return <code> true </code> if key agency assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyAgencyAssignment() {
        return (false);
    }


    /**
     *  Tests if key smart agency is available. 
     *
     *  @return <code> true </code> if key smart agency is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeySmartAgency() {
        return (false);
    }


    /**
     *  Gets the supported <code> Key </code> record types. 
     *
     *  @return a list containing the supported <code> Key </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getKeyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.keyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Key </code> record type is supported. 
     *
     *  @param  keyRecordType a <code> Type </code> indicating a <code> Key 
     *          </code> type 
     *  @return <code> true </code> if the given key record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> keyRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsKeyRecordType(org.osid.type.Type keyRecordType) {
        return (this.keyRecordTypes.contains(keyRecordType));
    }


    /**
     *  Adds support for a key record type.
     *
     *  @param keyRecordType a key record type
     *  @throws org.osid.NullArgumentException
     *  <code>keyRecordType</code> is <code>null</code>
     */

    protected void addKeyRecordType(org.osid.type.Type keyRecordType) {
        this.keyRecordTypes.add(keyRecordType);
        return;
    }


    /**
     *  Removes support for a key record type.
     *
     *  @param keyRecordType a key record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>keyRecordType</code> is <code>null</code>
     */

    protected void removeKeyRecordType(org.osid.type.Type keyRecordType) {
        this.keyRecordTypes.remove(keyRecordType);
        return;
    }


    /**
     *  Gets the supported key search record types. 
     *
     *  @return a list containing the supported <code> Key </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getKeySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.keySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given key search record type is supported. 
     *
     *  @param  keySearchRecordType a <code> Type </code> indicating a <code> 
     *          Key </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> keySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsKeySearchRecordType(org.osid.type.Type keySearchRecordType) {
        return (this.keySearchRecordTypes.contains(keySearchRecordType));
    }


    /**
     *  Adds support for a key search record type.
     *
     *  @param keySearchRecordType a key search record type
     *  @throws org.osid.NullArgumentException
     *  <code>keySearchRecordType</code> is <code>null</code>
     */

    protected void addKeySearchRecordType(org.osid.type.Type keySearchRecordType) {
        this.keySearchRecordTypes.add(keySearchRecordType);
        return;
    }


    /**
     *  Removes support for a key search record type.
     *
     *  @param keySearchRecordType a key search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>keySearchRecordType</code> is <code>null</code>
     */

    protected void removeKeySearchRecordType(org.osid.type.Type keySearchRecordType) {
        this.keySearchRecordTypes.remove(keySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key lookup 
     *  service. 
     *
     *  @return a <code> KeyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyLookupSession getKeyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> KeyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyLookupSession getKeyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> a KeyLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeyLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyLookupSession getKeyLookupSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyLookupSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> a KeyLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeyLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyLookupSession getKeyLookupSessionForAgency(org.osid.id.Id agencyId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyLookupSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key search 
     *  service. 
     *
     *  @return a <code> KeySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySearchSession getKeySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> KeySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySearchSession getKeySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key search 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> a KeySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySearchSession getKeySearchSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeySearchSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key search 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> a KeySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySearchSession getKeySearchSessionForAgency(org.osid.id.Id agencyId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeySearchSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key 
     *  administration service. 
     *
     *  @return a <code> KeyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAdminSession getKeyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> KeyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAdminSession getKeyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key admin 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> a KeyAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAdminSession getKeyAdminSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyAdminSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key admin 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> a KeyAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAdminSession getKeyAdminSessionForAgency(org.osid.id.Id agencyId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyAdminSessionForAgency not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to service 
     *  changes. 
     *
     *  @param  keyReceiver the key receiver 
     *  @return an <code> KeyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> keyReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyNotificationSession getKeyNotificationSession(org.osid.authentication.keys.KeyReceiver keyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyNotificationSession not implemented");
    }


    /**
     *  Gets the messaging receiver session for notifications pertaining to 
     *  key changes. 
     *
     *  @param  keyReceiver the key receiver 
     *  @param  proxy a proxy 
     *  @return an <code> KeyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          keyReceiver </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyNotificationSession getKeyNotificationSession(org.osid.authentication.keys.KeyReceiver keyReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key 
     *  notification service for the given agency. 
     *
     *  @param  keyReceiver the key receiver 
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> an KeyNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> keyReceiver </code> or 
     *          <code> agencyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyNotificationSession getKeyNotificationSessionForAgency(org.osid.authentication.keys.KeyReceiver keyReceiver, 
                                                                                                  org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyNotificationSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key 
     *  notification service for the given agency. 
     *
     *  @param  keyReceiver the key receiver 
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> an KeyNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> keyReceiver, agencyId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyNotificationSession getKeyNotificationSessionForAgency(org.osid.authentication.keys.KeyReceiver keyReceiver, 
                                                                                                  org.osid.id.Id agencyId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyNotificationSessionForAgency not implemented");
    }


    /**
     *  Gets the session for retrieving key to agency mappings. 
     *
     *  @return an <code> KeyAgencySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAgency() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAgencySession getKeyAgencySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyAgencySession not implemented");
    }


    /**
     *  Gets the session for retrieving key to agency mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> KeyAgencySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAgency() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAgencySession getKeyAgencySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyAgencySession not implemented");
    }


    /**
     *  Gets the session for assigning key to agency mappings. 
     *
     *  @return a <code> KeyAgencyAsignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyAgencyAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAgencyAssignmentSession getKeyAgencyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeyAgencyAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning key to agency mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> KeyAgencyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyAgencyAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAgencyAssignmentSession getKeyAgencyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeyAgencyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key smart 
     *  agency service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return an <code> KeySmartAgencySession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeySmartAgency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySmartAgencySession getKeySmartAgencySession(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysManager.getKeySmartAgencySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key smart 
     *  agency service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> KeySmartAgencySession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeySmartAgency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySmartAgencySession getKeySmartAgencySession(org.osid.id.Id agencyId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.keys.AuthenticationKeysProxyManager.getKeySmartAgencySession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.keyRecordTypes.clear();
        this.keyRecordTypes.clear();

        this.keySearchRecordTypes.clear();
        this.keySearchRecordTypes.clear();

        return;
    }
}
