//
// ResourceRelationshipBatchFormFilterList.java
//
//     Implements a filtering ResourceRelationshipBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.resource.batch.resourcerelationshipbatchform;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering ResourceRelationshipBatchFormList.
 */

public final class ResourceRelationshipBatchFormFilterList
    extends net.okapia.osid.jamocha.inline.filter.resource.batch.resourcerelationshipbatchform.spi.AbstractResourceRelationshipBatchFormFilterList
    implements org.osid.resource.batch.ResourceRelationshipBatchFormList,
               ResourceRelationshipBatchFormFilter {

    private final ResourceRelationshipBatchFormFilter filter;


    /**
     *  Creates a new <code>ResourceRelationshipBatchFormFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>ResourceRelationshipBatchFormList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public ResourceRelationshipBatchFormFilterList(ResourceRelationshipBatchFormFilter filter, org.osid.resource.batch.ResourceRelationshipBatchFormList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters ResourceRelationshipBatchForms.
     *
     *  @param resourceRelationshipBatchForm the resource relationship batch form to filter
     *  @return <code>true</code> if the resource relationship batch form passes the filter,
     *          <code>false</code> if the resource relationship batch form should be filtered
     */

    @Override
    public boolean pass(org.osid.resource.batch.ResourceRelationshipBatchForm resourceRelationshipBatchForm) {
        return (this.filter.pass(resourceRelationshipBatchForm));
    }
}
