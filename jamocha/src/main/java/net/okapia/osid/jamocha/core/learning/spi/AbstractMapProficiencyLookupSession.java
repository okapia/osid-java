//
// AbstractMapProficiencyLookupSession
//
//    A simple framework for providing a Proficiency lookup service
//    backed by a fixed collection of proficiencies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Proficiency lookup service backed by a
 *  fixed collection of proficiencies. The proficiencies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Proficiencies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProficiencyLookupSession
    extends net.okapia.osid.jamocha.learning.spi.AbstractProficiencyLookupSession
    implements org.osid.learning.ProficiencyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.learning.Proficiency> proficiencies = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.learning.Proficiency>());


    /**
     *  Makes a <code>Proficiency</code> available in this session.
     *
     *  @param  proficiency a proficiency
     *  @throws org.osid.NullArgumentException <code>proficiency<code>
     *          is <code>null</code>
     */

    protected void putProficiency(org.osid.learning.Proficiency proficiency) {
        this.proficiencies.put(proficiency.getId(), proficiency);
        return;
    }


    /**
     *  Makes an array of proficiencies available in this session.
     *
     *  @param  proficiencies an array of proficiencies
     *  @throws org.osid.NullArgumentException <code>proficiencies<code>
     *          is <code>null</code>
     */

    protected void putProficiencies(org.osid.learning.Proficiency[] proficiencies) {
        putProficiencies(java.util.Arrays.asList(proficiencies));
        return;
    }


    /**
     *  Makes a collection of proficiencies available in this session.
     *
     *  @param  proficiencies a collection of proficiencies
     *  @throws org.osid.NullArgumentException <code>proficiencies<code>
     *          is <code>null</code>
     */

    protected void putProficiencies(java.util.Collection<? extends org.osid.learning.Proficiency> proficiencies) {
        for (org.osid.learning.Proficiency proficiency : proficiencies) {
            this.proficiencies.put(proficiency.getId(), proficiency);
        }

        return;
    }


    /**
     *  Removes a Proficiency from this session.
     *
     *  @param  proficiencyId the <code>Id</code> of the proficiency
     *  @throws org.osid.NullArgumentException <code>proficiencyId<code> is
     *          <code>null</code>
     */

    protected void removeProficiency(org.osid.id.Id proficiencyId) {
        this.proficiencies.remove(proficiencyId);
        return;
    }


    /**
     *  Gets the <code>Proficiency</code> specified by its <code>Id</code>.
     *
     *  @param  proficiencyId <code>Id</code> of the <code>Proficiency</code>
     *  @return the proficiency
     *  @throws org.osid.NotFoundException <code>proficiencyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>proficiencyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Proficiency getProficiency(org.osid.id.Id proficiencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.learning.Proficiency proficiency = this.proficiencies.get(proficiencyId);
        if (proficiency == null) {
            throw new org.osid.NotFoundException("proficiency not found: " + proficiencyId);
        }

        return (proficiency);
    }


    /**
     *  Gets all <code>Proficiencies</code>. In plenary mode, the returned
     *  list contains all known proficiencies or an error
     *  results. Otherwise, the returned list may contain only those
     *  proficiencies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Proficiencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficiencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.proficiency.ArrayProficiencyList(this.proficiencies.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.proficiencies.clear();
        super.close();
        return;
    }
}
