//
// AbstractAcknowledgementManager.java
//
//     An adapter for a AcknowledgementManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AcknowledgementManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAcknowledgementManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.acknowledgement.AcknowledgementManager>
    implements org.osid.acknowledgement.AcknowledgementManager {


    /**
     *  Constructs a new {@code AbstractAdapterAcknowledgementManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAcknowledgementManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAcknowledgementManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAcknowledgementManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any billing federation is exposed. Federation is exposed when 
     *  a specific billing may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  billings appears as a single billing. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a credit lookup service. 
     *
     *  @return <code> true </code> if credit lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditLookup() {
        return (getAdapteeManager().supportsCreditLookup());
    }


    /**
     *  Tests for the availability of a credit query service. 
     *
     *  @return <code> true </code> if credit query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditQuery() {
        return (getAdapteeManager().supportsCreditQuery());
    }


    /**
     *  Tests if searching for credits is available. 
     *
     *  @return <code> true </code> if credit search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditSearch() {
        return (getAdapteeManager().supportsCreditSearch());
    }


    /**
     *  Tests if managing for credits is available. 
     *
     *  @return <code> true </code> if a credit adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditAdmin() {
        return (getAdapteeManager().supportsCreditAdmin());
    }


    /**
     *  Tests if credit notification is available. 
     *
     *  @return <code> true </code> if credit notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditNotification() {
        return (getAdapteeManager().supportsCreditNotification());
    }


    /**
     *  Tests if a credit to billing lookup session is available. 
     *
     *  @return <code> true </code> if credit billing lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditBilling() {
        return (getAdapteeManager().supportsCreditBilling());
    }


    /**
     *  Tests if a credit to billing assignment session is available. 
     *
     *  @return <code> true </code> if credit billing assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditBillingAssignment() {
        return (getAdapteeManager().supportsCreditBillingAssignment());
    }


    /**
     *  Tests if a credit smart billing session is available. 
     *
     *  @return <code> true </code> if credit smart billing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditSmartBilling() {
        return (getAdapteeManager().supportsCreditSmartBilling());
    }


    /**
     *  Tests for the availability of an billing lookup service. 
     *
     *  @return <code> true </code> if billing lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingLookup() {
        return (getAdapteeManager().supportsBillingLookup());
    }


    /**
     *  Tests if querying billings is available. 
     *
     *  @return <code> true </code> if billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingQuery() {
        return (getAdapteeManager().supportsBillingQuery());
    }


    /**
     *  Tests if searching for billings is available. 
     *
     *  @return <code> true </code> if billing search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingSearch() {
        return (getAdapteeManager().supportsBillingSearch());
    }


    /**
     *  Tests for the availability of a billing administrative service for 
     *  creating and deleting billings. 
     *
     *  @return <code> true </code> if billing administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingAdmin() {
        return (getAdapteeManager().supportsBillingAdmin());
    }


    /**
     *  Tests for the availability of a billing notification service. 
     *
     *  @return <code> true </code> if billing notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingNotification() {
        return (getAdapteeManager().supportsBillingNotification());
    }


    /**
     *  Tests for the availability of a billing hierarchy traversal service. 
     *
     *  @return <code> true </code> if billing hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingHierarchy() {
        return (getAdapteeManager().supportsBillingHierarchy());
    }


    /**
     *  Tests for the availability of a billing hierarchy design service. 
     *
     *  @return <code> true </code> if billing hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingHierarchyDesign() {
        return (getAdapteeManager().supportsBillingHierarchyDesign());
    }


    /**
     *  Tests for the availability of an acknowledgement batch service. 
     *
     *  @return <code> true </code> if an acknowledgement batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementBatch() {
        return (getAdapteeManager().supportsAcknowledgementBatch());
    }


    /**
     *  Gets the supported <code> Credit </code> record types. 
     *
     *  @return a list containing the supported credit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCreditRecordTypes() {
        return (getAdapteeManager().getCreditRecordTypes());
    }


    /**
     *  Tests if the given <code> Credit </code> record type is supported. 
     *
     *  @param  creditRecordType a <code> Type </code> indicating a <code> 
     *          Credit </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> creditRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCreditRecordType(org.osid.type.Type creditRecordType) {
        return (getAdapteeManager().supportsCreditRecordType(creditRecordType));
    }


    /**
     *  Gets the supported credit search record types. 
     *
     *  @return a list containing the supported credit search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCreditSearchRecordTypes() {
        return (getAdapteeManager().getCreditSearchRecordTypes());
    }


    /**
     *  Tests if the given credit search record type is supported. 
     *
     *  @param  creditSearchRecordType a <code> Type </code> indicating a 
     *          credit record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> creditSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCreditSearchRecordType(org.osid.type.Type creditSearchRecordType) {
        return (getAdapteeManager().supportsCreditSearchRecordType(creditSearchRecordType));
    }


    /**
     *  Gets the supported <code> Billing </code> record types. 
     *
     *  @return a list containing the supported billing record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBillingRecordTypes() {
        return (getAdapteeManager().getBillingRecordTypes());
    }


    /**
     *  Tests if the given <code> Billing </code> record type is supported. 
     *
     *  @param  billingRecordType a <code> Type </code> indicating a <code> 
     *          Billing </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> billingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBillingRecordType(org.osid.type.Type billingRecordType) {
        return (getAdapteeManager().supportsBillingRecordType(billingRecordType));
    }


    /**
     *  Gets the supported billing search record types. 
     *
     *  @return a list containing the supported billing search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBillingSearchRecordTypes() {
        return (getAdapteeManager().getBillingSearchRecordTypes());
    }


    /**
     *  Tests if the given billing search record type is supported. 
     *
     *  @param  billingSearchRecordType a <code> Type </code> indicating a 
     *          billing record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> billingSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBillingSearchRecordType(org.osid.type.Type billingSearchRecordType) {
        return (getAdapteeManager().supportsBillingSearchRecordType(billingSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit lookup 
     *  service. 
     *
     *  @return a <code> CreditLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditLookupSession getCreditLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit lookup 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditLookupSession getCreditLookupSessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditLookupSessionForBilling(billingId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit query 
     *  service. 
     *
     *  @return a <code> CreditQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuerySession getCreditQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit query 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuerySession getCreditQuerySessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditQuerySessionForBilling(billingId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit search 
     *  service. 
     *
     *  @return a <code> CreditSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSearchSession getCreditSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit search 
     *  service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSearchSession getCreditSearchSessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditSearchSessionForBilling(billingId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  administration service. 
     *
     *  @return a <code> CreditAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditAdminSession getCreditAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  administration service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditAdminSession getCreditAdminSessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditAdminSessionForBilling(billingId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  notification service. 
     *
     *  @param  creditReceiver the receiver 
     *  @return a <code> CreditNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> creditReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditNotificationSession getCreditNotificationSession(org.osid.acknowledgement.CreditReceiver creditReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditNotificationSession(creditReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credit 
     *  notification service for the given billing. 
     *
     *  @param  creditReceiver the receiver 
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> creditReceiver </code> 
     *          or <code> billingId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditNotificationSession getCreditNotificationSessionForBilling(org.osid.acknowledgement.CreditReceiver creditReceiver, 
                                                                                                     org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditNotificationSessionForBilling(creditReceiver, billingId));
    }


    /**
     *  Gets the session for retrieving credit to billing mappings. 
     *
     *  @return a <code> CreditBillingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCreditBilling() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditBillingSession getCreditBillingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditBillingSession());
    }


    /**
     *  Gets the session for assigning credit to billing mappings. 
     *
     *  @return a <code> CreditBillingAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBillingAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditBillingAssignmentSession getCreditBillingAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditBillingAssignmentSession());
    }


    /**
     *  Gets the session associated with the credit smart billing for the 
     *  given billing. 
     *
     *  @param  billingId the <code> Id </code> of the billing 
     *  @return a <code> CreditSmartBillingSession </code> 
     *  @throws org.osid.NotFoundException <code> billingId </code> not found 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditSmartBilling() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditSmartBillingSession getCreditSmartBillingSession(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCreditSmartBillingSession(billingId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing lookup 
     *  service. 
     *
     *  @return a <code> BillingLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingLookupSession getBillingLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing query 
     *  service. 
     *
     *  @return a <code> BillingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuerySession getBillingQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing search 
     *  service. 
     *
     *  @return a <code> BillingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingSearchSession getBillingSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  administrative service. 
     *
     *  @return a <code> BillingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBillingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingAdminSession getBillingAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  notification service. 
     *
     *  @param  billingReceiver the receiver 
     *  @return a <code> BillingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> billingReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingNotificationSession getBillingNotificationSession(org.osid.acknowledgement.BillingReceiver billingReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingNotificationSession(billingReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  hierarchy service. 
     *
     *  @return a <code> BillingHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingHierarchySession getBillingHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the billing 
     *  hierarchy design service. 
     *
     *  @return a <code> BillingHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingHierarchyDesignSession getBillingHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingHierarchyDesignSession());
    }


    /**
     *  Gets an <code> AcknowledgementBatchManager. </code> 
     *
     *  @return an <code> AcknowledgementBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementBach() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.AcknowledgementBatchManager getAcknowledgementBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcknowledgementBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
