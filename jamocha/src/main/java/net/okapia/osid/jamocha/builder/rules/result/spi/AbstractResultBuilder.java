//
// AbstractResult.java
//
//     Defines a Result builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.result.spi;


/**
 *  Defines a <code>Result</code> builder.
 */

public abstract class AbstractResultBuilder<T extends AbstractResultBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidResultBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.rules.result.ResultMiter result;


    /**
     *  Constructs a new <code>AbstractResultBuilder</code>.
     *
     *  @param result the result to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractResultBuilder(net.okapia.osid.jamocha.builder.rules.result.ResultMiter result) {
        super(result);
        this.result = result;
        return;
    }


    /**
     *  Builds the result.
     *
     *  @return the new result
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.rules.Result build() {
        (new net.okapia.osid.jamocha.builder.validator.rules.result.ResultValidator(getValidations())).validate(this.result);
        return (new net.okapia.osid.jamocha.builder.rules.result.ImmutableResult(this.result));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the result miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.rules.result.ResultMiter getMiter() {
        return (this.result);
    }


    /**
     *  Sets the boolean value.
     *
     *  @return the boolean value
     */

    public T booleanValue(boolean value) {
        getMiter().setBooleanValue(value);
        return (self());
    }


    /**
     *  Adds a Result record.
     *
     *  @param record a result record
     *  @param recordType the type of result record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.rules.records.ResultRecord record, org.osid.type.Type recordType) {
        getMiter().addResultRecord(record, recordType);
        return (self());
    }
}       


