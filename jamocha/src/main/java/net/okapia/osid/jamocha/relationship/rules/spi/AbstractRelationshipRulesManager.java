//
// AbstractRelationshipRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRelationshipRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.relationship.rules.RelationshipRulesManager,
               org.osid.relationship.rules.RelationshipRulesProxyManager {

    private final Types relationshipEnablerRecordTypes     = new TypeRefSet();
    private final Types relationshipEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractRelationshipRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRelationshipRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up relationship enablers is supported. 
     *
     *  @return <code> true </code> if relationship enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying relationship enablers is supported. 
     *
     *  @return <code> true </code> if relationship enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching relationship enablers is supported. 
     *
     *  @return <code> true </code> if relationship enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a relationship enabler administrative service is supported. 
     *
     *  @return <code> true </code> if relationship enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a relationship enabler notification service is supported. 
     *
     *  @return <code> true </code> if relationship enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a relationship enabler family lookup service is supported. 
     *
     *  @return <code> true </code> if a family enabler family lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerFamily() {
        return (false);
    }


    /**
     *  Tests if a relationship enabler family service is supported. 
     *
     *  @return <code> true </code> if relationship enabler family assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerFamilyAssignment() {
        return (false);
    }


    /**
     *  Tests if a relationship enabler smart family service is supported. 
     *
     *  @return <code> true </code> if a relationship enabler smart family 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerSmartFamily() {
        return (false);
    }


    /**
     *  Tests if a relationship enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a enabler relationship rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a relationship enabler rule application service is supported. 
     *
     *  @return <code> true </code> if enabler relationship rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> RelationshipEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> RelationshipEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelationshipEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relationshipEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RelationshipEnabler </code> record type is 
     *  supported. 
     *
     *  @param  relationshipEnablerRecordType a <code> Type </code> indicating 
     *          a <code> RelationshipEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerRecordType(org.osid.type.Type relationshipEnablerRecordType) {
        return (this.relationshipEnablerRecordTypes.contains(relationshipEnablerRecordType));
    }


    /**
     *  Adds support for a relationship enabler record type.
     *
     *  @param relationshipEnablerRecordType a relationship enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipEnablerRecordType</code> is <code>null</code>
     */

    protected void addRelationshipEnablerRecordType(org.osid.type.Type relationshipEnablerRecordType) {
        this.relationshipEnablerRecordTypes.add(relationshipEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a relationship enabler record type.
     *
     *  @param relationshipEnablerRecordType a relationship enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipEnablerRecordType</code> is <code>null</code>
     */

    protected void removeRelationshipEnablerRecordType(org.osid.type.Type relationshipEnablerRecordType) {
        this.relationshipEnablerRecordTypes.remove(relationshipEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RelationshipEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> RelationshipEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelationshipEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relationshipEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RelationshipEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  relationshipEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RelationshipEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRelationshipEnablerSearchRecordType(org.osid.type.Type relationshipEnablerSearchRecordType) {
        return (this.relationshipEnablerSearchRecordTypes.contains(relationshipEnablerSearchRecordType));
    }


    /**
     *  Adds support for a relationship enabler search record type.
     *
     *  @param relationshipEnablerSearchRecordType a relationship enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addRelationshipEnablerSearchRecordType(org.osid.type.Type relationshipEnablerSearchRecordType) {
        this.relationshipEnablerSearchRecordTypes.add(relationshipEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a relationship enabler search record type.
     *
     *  @param relationshipEnablerSearchRecordType a relationship enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeRelationshipEnablerSearchRecordType(org.osid.type.Type relationshipEnablerSearchRecordType) {
        this.relationshipEnablerSearchRecordTypes.remove(relationshipEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler lookup service. 
     *
     *  @return a <code> RelationshipEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerLookupSession getRelationshipEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerLookupSession getRelationshipEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler lookup service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerLookupSession getRelationshipEnablerLookupSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerLookupSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler lookup service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerLookupSession getRelationshipEnablerLookupSessionForFamily(org.osid.id.Id familyId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerLookupSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler query service. 
     *
     *  @return a <code> RelationshipEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerQuerySession getRelationshipEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerQuerySession getRelationshipEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler query service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerQuerySession getRelationshipEnablerQuerySessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerQuerySessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler query service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerQuerySession getRelationshipEnablerQuerySessionForFamily(org.osid.id.Id familyId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerQuerySessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler search service. 
     *
     *  @return a <code> RelationshipEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerSearchSession getRelationshipEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerSearchSession getRelationshipEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enablers earch service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerSearchSession getRelationshipEnablerSearchSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerSearchSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enablers earch service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerSearchSession getRelationshipEnablerSearchSessionForFamily(org.osid.id.Id familyId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerSearchSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler administration service. 
     *
     *  @return a <code> RelationshipEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerAdminSession getRelationshipEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerAdminSession getRelationshipEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerAdminSession getRelationshipEnablerAdminSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerAdminSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerAdminSession getRelationshipEnablerAdminSessionForFamily(org.osid.id.Id familyId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerAdminSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler notification service. 
     *
     *  @param  relationshipEnablerReceiver the notification callback 
     *  @return a <code> RelationshipEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerNotificationSession getRelationshipEnablerNotificationSession(org.osid.relationship.rules.RelationshipEnablerReceiver relationshipEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler notification service. 
     *
     *  @param  relationshipEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerNotificationSession getRelationshipEnablerNotificationSession(org.osid.relationship.rules.RelationshipEnablerReceiver relationshipEnablerReceiver, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler notification service for the given family. 
     *
     *  @param  relationshipEnablerReceiver the notification callback 
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipEnablerReceiver </code> or <code> familyId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerNotificationSession getRelationshipEnablerNotificationSessionForFamily(org.osid.relationship.rules.RelationshipEnablerReceiver relationshipEnablerReceiver, 
                                                                                                                                 org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerNotificationSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler notification service for the given family. 
     *
     *  @param  relationshipEnablerReceiver the notification callback 
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipEnablerReceiver, familyId, </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerNotificationSession getRelationshipEnablerNotificationSessionForFamily(org.osid.relationship.rules.RelationshipEnablerReceiver relationshipEnablerReceiver, 
                                                                                                                                 org.osid.id.Id familyId, 
                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerNotificationSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relationship enabler 
     *  family mappings for relationship enablers. 
     *
     *  @return a <code> RelationshipEnablerRelationshipSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerFamily() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerFamilySession getRelationshipEnablerFamilySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relationship 
     *  enabler/family mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerFamilySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerFamily() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerFamilySession getRelationshipEnablerFamilySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relationship enablers to families. 
     *
     *  @return a <code> RelationshipEnablerFamilyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerFamilyAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerFamilyAssignmentSession getRelationshipEnablerFamilyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerFamilyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relationship enablers to families. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerFamilyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerFamilyAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerFamilyAssignmentSession getRelationshipEnablerFamilyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerFamilyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage relationship enabler 
     *  smart families. 
     *
     *  @param  familyId the Id of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerSmartFamilySession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerSmartFamily() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerSmartFamilySession getRelationshipEnablerSmartFamilySession(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerSmartFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage relationship enabler 
     *  smart families. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerSmartFamilySession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerSmartRelationship() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerSmartFamilySession getRelationshipEnablerSmartFamilySession(org.osid.id.Id familyId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerSmartFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  relationship. 
     *
     *  @return a <code> RelationshipEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleLookupSession getRelationshipEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  relationship. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleLookupSession getRelationshipEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler mapping lookup service for the given relationship for looking 
     *  up rules applied to a relationship. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleLookupSession getRelationshipEnablerRuleLookupSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerRuleLookupSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler mapping lookup service for the given relationship for looking 
     *  up rules applied to a relationship. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleLookupSession getRelationshipEnablerRuleLookupSessionForFamily(org.osid.id.Id familyId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerRuleLookupSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler assignment service to apply enablers to relationships. 
     *
     *  @return a <code> RelationshipEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleApplicationSession getRelationshipEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler assignment service to apply enablers to relationships. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleApplicationSession getRelationshipEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler assignment service for the given relationship to apply 
     *  enablers to relationships. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleApplicationSession getRelationshipEnablerRuleApplicationSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesManager.getRelationshipEnablerRuleApplicationSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  enabler assignment service for the given relationship to apply 
     *  enablers to relationships. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerRuleApplicationSession getRelationshipEnablerRuleApplicationSessionForFamily(org.osid.id.Id familyId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.rules.RelationshipRulesProxyManager.getRelationshipEnablerRuleApplicationSessionForFamily not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.relationshipEnablerRecordTypes.clear();
        this.relationshipEnablerRecordTypes.clear();

        this.relationshipEnablerSearchRecordTypes.clear();
        this.relationshipEnablerSearchRecordTypes.clear();

        return;
    }
}
