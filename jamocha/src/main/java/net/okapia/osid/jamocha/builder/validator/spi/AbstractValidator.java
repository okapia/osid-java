//
// AbstractValidator.java
//
//     Root class for validating objects.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;

import net.okapia.osid.jamocha.builder.validator.Validation;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  The root validator.
 */

public abstract class AbstractValidator {
    private final java.util.EnumSet<Validation> validations;

    private static final java.math.BigDecimal ZERO    = new java.math.BigDecimal(0);
    private static final java.math.BigDecimal HUNDRED = new java.math.BigDecimal(100);


    /**
     *  Constructs a new <code>AbstractValidator</code>.
     */

    protected AbstractValidator() {
        this.validations = java.util.EnumSet.of(Validation.ALL);
        return;
    }


    /**
     *  Constructs a new <code>AbstractValidator</code>.
     *
     *  @param validations an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractValidator(java.util.EnumSet<Validation> validations) {
        nullarg(validations, "validations");
        this.validations = validations;
        return;
    }

    
    /**
     *  Tests for a validation test.
     *
     *  @param validation
     *  @return <code>true</code> if item is to be tested, 
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected boolean perform(Validation validation) {
        return ((this.validations.contains(Validation.ALL)) || 
                this.validations.contains(validation));
    }

    
    /**
     *  Validates an Object.
     *
     *  @param object the object to validate
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     */

    public void validate(Object object) {
        nullarg(object, "object");
        return;
    }


    /**
     *  Tests an object return.
     *
     *  @param object object to test
     *  @param method name of method to include in error
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>object</code> or
     *          <code>method</code> is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    protected void test(Object object, String method) {
        nullarg(method, "method");
        nullarg(object, "object from " + method);

        if (perform(Validation.NULL)) {
            if (object == null) {
                throw new org.osid.NullReturnException(method + " returned null");
            }
        }

        if (perform(Validation.NIL_ID) && (object instanceof org.osid.id.Id)) {
            for (Class<?> c : object.getClass().getClasses()) {
                if (c.getName().startsWith("net.okapia.osid.jamocha.nil")) {
                    throw new org.osid.InvalidReturnException(method + " returned nil implementation");
                }
            }
        } 

        if (perform(Validation.NIL_TYPE) && (object instanceof org.osid.type.Type)) {
            for (Class<?> c : object.getClass().getClasses()) {
                if (c.getName().startsWith("net.okapia.osid.jamocha.nil")) {
                    throw new org.osid.InvalidReturnException(method + " returned nil implementation");
                }
            }
        } 

        /*
        if (perform(Validation.NIL_OBJECT) && (object instanceof org.osid.Identifiable)) {
            for (Class<?> c : object.getClass().getClasses()) {
                if (c.getName().startsWith("net.okapia.osid.jamocha.nil")) {
                    throw new org.osid.InvalidReturnException(method + " returned nil implementation");
                }
            }
        } 
        */
        return;
    }


    /**
     *  Tests a cardinal return.
     *
     *  @param c number to test
     *  @param method name of method to include in error
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>method</code>
     *          is <code>null</code>
     */

    protected void testCardinal(long c, String method) {
        nullarg(method, "method");

        if (perform(Validation.CARDINAL)) {
            if (c < 0) {
                throw new org.osid.InvalidReturnException(method + " is negative");
            }
        }

        return;
    }


    /**
     *  Tests a cardinal return.
     *
     *  @param c number to test
     *  @param method name of method to include in error
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>c</code> or
     *          <code>method</code> is <code>null</code>
     */

    protected void testCardinal(java.math.BigDecimal c, String method) {
        nullarg(method, "method");
        nullarg(c, "number");

        if (perform(Validation.CARDINAL)) {
            if (c.compareTo(ZERO) < 0) {
                throw new org.osid.InvalidReturnException(method + " is negative");
            }
        }

        return;
    }


    /**
     *  Tests a cardinal range.
     *
     *  @param low the low end of range
     *  @param high the high end of range
     *  @param msg the error message
     *  @throws org.osid.InvalidReturnException <code>low</code> is greater
     *          than <code>high</code> or negative
     *  @throws org.osid.NullArgumentException <code>msg</code>
     *          is <code>null</code>
     */

    protected void testCardinalRange(long low, long high, String msg) {
        nullarg(msg, "message");

        if (perform(Validation.CARDINAL)) {
            if ((low < 0) || (high < 0)) {
                throw new org.osid.InvalidReturnException(msg + " is negative");
            }
        }

        if (low > high) {
            throw new org.osid.InvalidReturnException(msg + " reversed");
        }

        return;
    }


    /**
     *  Tests for Id consistency.
     *
     *  @param id
     *  @param identifiable
     *  @param msg msg to include in error
     *  @throws org.osid.BadLogicException an Id mismatch
     *  @throws org.osid.NullArgumentException <code>id</code>,
     *          <code>identifiable</code> or <code>msg</code> is
     *          <code>null</code>
     */

    protected void testId(org.osid.id.Id id, org.osid.Identifiable identifiable, String msg) {
        nullarg(id, "Id");
        nullarg(identifiable, "Identifiable");
        nullarg(msg, "msg");

        if (perform(Validation.ID_CONSISTENCY)) {
            if (!id.equals(identifiable.getId())) {
                throw new org.osid.BadLogicException(msg);
            }
        }

        return;
    }


    /**
     *  Tests a date range.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param msg the error message
     *  @throws org.osid.BadLogicException <code>start</code> is
     *          greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>start</code>,
     *          <code>end</code>, or <code>msg</code> is
     *          <code>null</code>
     */

    protected void testDateRange(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, String msg) {
        nullarg(msg, "msg");

        test(start, "getStartDate()");
        test(end,   "getEndDate()");

        if (perform(Validation.RANGE)) {
            if (start.isGreater(end)) {
                throw new org.osid.BadLogicException(msg);
            }
        }

        return;
    }


    /**
     *  Tests a percentage (0-100).
     *
     *  @param percent percentage
     *  @param msg the error message
     *  @throws org.osid.InvalidReturnException <code>start</code> or
     *          <code>end</code> is out of range
     */

    protected void testPercentage(long percent, String msg) {
        nullarg(msg, "message");

        if ((percent < 0) || (percent > 100)) {
            throw new org.osid.InvalidReturnException(msg + " is out of range");
        }

        return;
    }


    /**
     *  Tests a percentage (0-100).
     *
     *  @param percent percentage
     *  @param msg the error message
     *  @throws org.osid.InvalidReturnException <code>start</code> or
     *          <code>end</code> is out of range
     */

    protected void testPercentage(java.math.BigDecimal percent, String msg) {
        nullarg(msg, "message");
        
        if ((percent.compareTo(ZERO) < 0) || (percent.compareTo(HUNDRED) > 0)) {
            throw new org.osid.InvalidReturnException(msg + " is out of range");
        }

        return;
    }


    /**
     *  Tests a percentage range (0-100).
     *
     *  @param start start of range
     *  @param end end of range
     *  @param msg the error message
     *  @throws org.osid.BadLogicException <code>start</code> is
     *          greater than <code>end</code>
     *  @throws org.osid.InvalidReturnException <code>start</code> or
     *          <code>end</code> is out of range
     */

    protected void testPercentageRange(long start, long end, String msg) {
        nullarg(msg, "message");

        if ((start < 0) || (start > 100)) {
            throw new org.osid.InvalidReturnException(msg + " is out of range");
        }

        if ((end < 0) || (end > 100)) {
            throw new org.osid.InvalidReturnException(msg + " is out of range");
        }

        return;
    }


    /**
     *  Tests an Id/Identifiable pair.
     *
     *  @param object the object
     *  @param methodName the name of the Id method in the object
     *  @throws org.osid.BadLogicException the Id doesn't match the 
     *          Identifiable
     *  @throws org.osid.NullArgumentException <code>object</code> or
     *          <code>methodName</code> is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    protected void testNestedObject(Object object, String methodName) {
        nullarg(object, "object");
        nullarg(methodName, "method name");

        try {
            java.lang.reflect.Method idMethod  = object.getClass().getMethod(methodName + "Id");
            java.lang.reflect.Method objMethod = object.getClass().getMethod(methodName);
            
            org.osid.id.Id id = (org.osid.id.Id) idMethod.invoke(object);
            test(id, methodName + "Id()");

            if (perform(Validation.TRAVERSE)) {
                org.osid.Identifiable ident = (org.osid.Identifiable) objMethod.invoke(object);
                test(ident, methodName + "()");

                if (perform(Validation.ID_CONSISTENCY)) {
                    testId(id, ident, methodName + "Id() != " + methodName + "().getId()");
                }
            }
        } catch (NoSuchMethodException nsme) {
            throw new org.osid.OsidRuntimeException(nsme);
        } catch (java.lang.reflect.InvocationTargetException | IllegalAccessException ie) {
            throw new org.osid.OsidRuntimeException(ie);
        }
    }


    /**
     *  Tests an IdList/IdentifiableList pair.
     *
     *  @param object the object
     *  @param idMethodName the name of the Id method in the object
     *  @param objMethodName the name of the object method in the object
     *  @throws org.osid.BadLogicException the Id doesn't match an
     *          Identifiable
     *  @throws org.osid.NullArgumentException <code>object</code>,
     *          <code>idMethodName</code>, or
     *          <code>objMethodName</code> is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    protected void testNestedObjects(Object object, String idMethodName, String objMethodName) {
        nullarg(object, "object");
        nullarg(idMethodName,  "Id method name");
        nullarg(objMethodName,  "object method name");

        java.util.Collection<org.osid.id.Id> idset = new java.util.HashSet<>();

        try {
            java.lang.reflect.Method idMethod  = object.getClass().getMethod(idMethodName);
            java.lang.reflect.Method objMethod = object.getClass().getMethod(objMethodName);

            try (org.osid.id.IdList ids = (org.osid.id.IdList) idMethod.invoke(object)) {
                test(ids, idMethodName);
                while (ids.hasNext()) {
                    org.osid.id.Id id = ids.getNextId();
                    test(id, "getNextId()");

                    if (perform(Validation.ID_CONSISTENCY)) {
                        idset.add(id);
                    }
                }
            }

            if (perform(Validation.TRAVERSE)) {
                try (org.osid.OsidList list = (org.osid.OsidList) objMethod.invoke(object)) {
                    test(list, objMethodName + "()");

                    java.lang.reflect.Method listMethod = null;
                    for (java.lang.reflect.Method m : list.getClass().getDeclaredMethods()) {
                        if (m.getName().startsWith("getNext") &&
                            (m.getParameterTypes().length == 0)) {
                            listMethod = m;
                            break;
                        }
                    }

                    if (listMethod == null) {
                        throw new org.osid.OsidRuntimeException("cannot find list access for " + 
                                                                list.getClass().getName());
                    }

                    while(list.hasNext()) {
                        org.osid.Identifiable ident = (org.osid.Identifiable) listMethod.invoke(list);
                        test(ident, listMethod.getName() + "()");
                        if (perform(Validation.ID_CONSISTENCY)) {
                            if (!idset.remove(ident.getId())) {
                                throw new org.osid.BadLogicException("mismatched id in " + objMethodName);
                            }
                        }
                    }
                }

                if (idset.size() > 0) {
                    throw new org.osid.BadLogicException("oprhaned Ids in " + idMethodName + "()");
                }
            }
        } catch (org.osid.OsidException oe) {
            throw new org.osid.OsidRuntimeException(oe);            
        } catch (NoSuchMethodException nsme) {
            throw new org.osid.OsidRuntimeException(nsme);
        } catch (java.lang.reflect.InvocationTargetException | IllegalAccessException ie) {
            throw new org.osid.OsidRuntimeException(ie);
        }
    }


    /**
     *  Tests for support of a method.
     *
     *  @param object the object to test
     *  @param methodName the name of the method in the object
     *  @param exists <code>true</code> if the method should return a
     *                value, <code>false</code> if should throw an
     *                <code>IllegalStateException</code>
     *  @param conditional the name of the conditional method for the 
     *         error message
     *  @throws org.osid.BadLogicException method doesn't comply with
     *          the <code>exists</code> value
     *  @throws org.osid.NullArgumentException <code>object</code>,
     *          <code>idMethodName</code>, or
     *          <code>objMethodName</code> is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    protected void testConditionalMethod(Object object, String methodName, boolean exists, String conditional) {
        nullarg(object, "object");
        nullarg(methodName, methodName + "() is null");
        nullarg(conditional, "conditional method");

        try {
            java.lang.reflect.Method method  = object.getClass().getMethod(methodName);
            Object obj = method.invoke(object);
            if (obj == null) {
                System.out.println(methodName + " is null");
            }

            test(obj, methodName + "()");
            if (obj instanceof AutoCloseable) {
                ((AutoCloseable) obj).close();
            }

            if (!exists) {
                throw new org.osid.BadLogicException(conditional + " is false");
            }
        } catch (org.osid.IllegalStateException ise) {
            if (exists) {
                throw new org.osid.BadLogicException(conditional  + " is true");
            }
        } catch (org.osid.NullReturnException nre) {
            if (exists) {
                throw new org.osid.BadLogicException(conditional  + " is true");
            }
        } catch (NoSuchMethodException | IllegalAccessException nsme) {
            throw new org.osid.OsidRuntimeException(nsme);
        } catch (java.lang.reflect.InvocationTargetException ite) {
            if (ite.getCause() instanceof org.osid.IllegalStateException) {
                if (exists) {
                    throw new org.osid.BadLogicException(conditional  + " is true");
                } else {
                    return;
                }
            }

            throw new org.osid.OsidRuntimeException(ite);
        } catch (Exception e) {
            throw new org.osid.OsidRuntimeException(e);
        }
    }
}
