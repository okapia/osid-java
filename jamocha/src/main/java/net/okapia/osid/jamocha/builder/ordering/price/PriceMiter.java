//
// PriceMiter.java
//
//     Defines a Price miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.price;


/**
 *  Defines a <code>Price</code> miter for use with the builders.
 */

public interface PriceMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.ordering.Price {


    /**
     *  Sets the price schedule.
     *
     *  @param schedule a price schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public void setPriceSchedule(org.osid.ordering.PriceSchedule schedule);


    /**
     *  Sets the minimum quantity.
     *
     *  @param quantity a minimum quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public void setMinimumQuantity(long quantity);


    /**
     *  Sets the maximum quantity.
     *
     *  @param quantity a maximum quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public void setMaximumQuantity(long quantity);


    /**
     *  Sets the demographic.
     *
     *  @param demographic a demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public void setDemographic(org.osid.resource.Resource demographic);


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setAmount(org.osid.financials.Currency amount);


    /**
     *  Sets the recurring interval.
     *
     *  @param interval a recurring interval
     *  @throws org.osid.NullArgumentException <code>interval</code>
     *          is <code>null</code>
     */

    public void setRecurringInterval(org.osid.calendaring.Duration interval);


    /**
     *  Adds a Price record.
     *
     *  @param record a price record
     *  @param recordType the type of price record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPriceRecord(org.osid.ordering.records.PriceRecord record, org.osid.type.Type recordType);
}       


