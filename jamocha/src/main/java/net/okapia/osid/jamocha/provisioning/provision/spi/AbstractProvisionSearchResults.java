//
// AbstractProvisionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProvisionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.ProvisionSearchResults {

    private org.osid.provisioning.ProvisionList provisions;
    private final org.osid.provisioning.ProvisionQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.records.ProvisionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProvisionSearchResults.
     *
     *  @param provisions the result set
     *  @param provisionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>provisions</code>
     *          or <code>provisionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProvisionSearchResults(org.osid.provisioning.ProvisionList provisions,
                                            org.osid.provisioning.ProvisionQueryInspector provisionQueryInspector) {
        nullarg(provisions, "provisions");
        nullarg(provisionQueryInspector, "provision query inspectpr");

        this.provisions = provisions;
        this.inspector = provisionQueryInspector;

        return;
    }


    /**
     *  Gets the provision list resulting from a search.
     *
     *  @return a provision list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisions() {
        if (this.provisions == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.ProvisionList provisions = this.provisions;
        this.provisions = null;
	return (provisions);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.ProvisionQueryInspector getProvisionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  provision search record <code> Type. </code> This method must
     *  be used to retrieve a provision implementing the requested
     *  record.
     *
     *  @param provisionSearchRecordType a provision search 
     *         record type 
     *  @return the provision search
     *  @throws org.osid.NullArgumentException
     *          <code>provisionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(provisionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionSearchResultsRecord getProvisionSearchResultsRecord(org.osid.type.Type provisionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.records.ProvisionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(provisionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(provisionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record provision search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProvisionRecord(org.osid.provisioning.records.ProvisionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "provision record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
