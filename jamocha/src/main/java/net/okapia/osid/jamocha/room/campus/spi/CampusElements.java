//
// CampusElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.campus.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CampusElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the CampusElement Id.
     *
     *  @return the campus element Id
     */

    public static org.osid.id.Id getCampusEntityId() {
        return (makeEntityId("osid.room.Campus"));
    }


    /**
     *  Gets the RoomId element Id.
     *
     *  @return the RoomId element Id
     */

    public static org.osid.id.Id getRoomId() {
        return (makeQueryElementId("osid.room.campus.RoomId"));
    }


    /**
     *  Gets the Room element Id.
     *
     *  @return the Room element Id
     */

    public static org.osid.id.Id getRoom() {
        return (makeQueryElementId("osid.room.campus.Room"));
    }


    /**
     *  Gets the FloorId element Id.
     *
     *  @return the FloorId element Id
     */

    public static org.osid.id.Id getFloorId() {
        return (makeQueryElementId("osid.room.campus.FloorId"));
    }


    /**
     *  Gets the Floor element Id.
     *
     *  @return the Floor element Id
     */

    public static org.osid.id.Id getFloor() {
        return (makeQueryElementId("osid.room.campus.Floor"));
    }


    /**
     *  Gets the BuildingId element Id.
     *
     *  @return the BuildingId element Id
     */

    public static org.osid.id.Id getBuildingId() {
        return (makeQueryElementId("osid.room.campus.BuildingId"));
    }


    /**
     *  Gets the Building element Id.
     *
     *  @return the Building element Id
     */

    public static org.osid.id.Id getBuilding() {
        return (makeQueryElementId("osid.room.campus.Building"));
    }


    /**
     *  Gets the AncestorCampusId element Id.
     *
     *  @return the AncestorCampusId element Id
     */

    public static org.osid.id.Id getAncestorCampusId() {
        return (makeQueryElementId("osid.room.campus.AncestorCampusId"));
    }


    /**
     *  Gets the AncestorCampus element Id.
     *
     *  @return the AncestorCampus element Id
     */

    public static org.osid.id.Id getAncestorCampus() {
        return (makeQueryElementId("osid.room.campus.AncestorCampus"));
    }


    /**
     *  Gets the DescendantCampusId element Id.
     *
     *  @return the DescendantCampusId element Id
     */

    public static org.osid.id.Id getDescendantCampusId() {
        return (makeQueryElementId("osid.room.campus.DescendantCampusId"));
    }


    /**
     *  Gets the DescendantCampus element Id.
     *
     *  @return the DescendantCampus element Id
     */

    public static org.osid.id.Id getDescendantCampus() {
        return (makeQueryElementId("osid.room.campus.DescendantCampus"));
    }
}
