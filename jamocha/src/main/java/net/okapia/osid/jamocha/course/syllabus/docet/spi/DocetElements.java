//
// DocetElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.docet.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class DocetElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the DocetElement Id.
     *
     *  @return the docet element Id
     */

    public static org.osid.id.Id getDocetEntityId() {
        return (makeEntityId("osid.course.syllabus.Docet"));
    }


    /**
     *  Gets the ModuleId element Id.
     *
     *  @return the ModuleId element Id
     */

    public static org.osid.id.Id getModuleId() {
        return (makeElementId("osid.course.syllabus.docet.ModuleId"));
    }


    /**
     *  Gets the Module element Id.
     *
     *  @return the Module element Id
     */

    public static org.osid.id.Id getModule() {
        return (makeElementId("osid.course.syllabus.docet.Module"));
    }


    /**
     *  Gets the ActivityUnitId element Id.
     *
     *  @return the ActivityUnitId element Id
     */

    public static org.osid.id.Id getActivityUnitId() {
        return (makeElementId("osid.course.syllabus.docet.ActivityUnitId"));
    }


    /**
     *  Gets the ActivityUnit element Id.
     *
     *  @return the ActivityUnit element Id
     */

    public static org.osid.id.Id getActivityUnit() {
        return (makeElementId("osid.course.syllabus.docet.ActivityUnit"));
    }


    /**
     *  Gets the LearningObjectiveIds element Id.
     *
     *  @return the LearningObjectiveIds element Id
     */

    public static org.osid.id.Id getLearningObjectiveIds() {
        return (makeElementId("osid.course.syllabus.docet.LearningObjectiveIds"));
    }


    /**
     *  Gets the LearningObjectives element Id.
     *
     *  @return the LearningObjectives element Id
     */

    public static org.osid.id.Id getLearningObjectives() {
        return (makeElementId("osid.course.syllabus.docet.LearningObjectives"));
    }


    /**
     *  Gets the Duration element Id.
     *
     *  @return the Duration element Id
     */

    public static org.osid.id.Id getDuration() {
        return (makeElementId("osid.course.syllabus.docet.Duration"));
    }


    /**
     *  Gets the AssetIds element Id.
     *
     *  @return the AssetIds element Id
     */

    public static org.osid.id.Id getAssetIds() {
        return (makeElementId("osid.course.syllabus.docet.AssetIds"));
    }


    /**
     *  Gets the Assets element Id.
     *
     *  @return the Assets element Id
     */

    public static org.osid.id.Id getAssets() {
        return (makeElementId("osid.course.syllabus.docet.Assets"));
    }


    /**
     *  Gets the AssessmentIds element Id.
     *
     *  @return the AssessmentIds element Id
     */

    public static org.osid.id.Id getAssessmentIds() {
        return (makeElementId("osid.course.syllabus.docet.AssessmentIds"));
    }


    /**
     *  Gets the Assessments element Id.
     *
     *  @return the Assessments element Id
     */

    public static org.osid.id.Id getAssessments() {
        return (makeElementId("osid.course.syllabus.docet.Assessments"));
    }


    /**
     *  Gets the InClass element Id.
     *
     *  @return the InClass element Id
     */

    public static org.osid.id.Id getInClass() {
        return (makeElementId("osid.course.syllabus.docet.InClass"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.syllabus.docet.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.syllabus.docet.CourseCatalog"));
    }
}
