//
// AbstractSummary.java
//
//     Defines a Summary.
//
//
// Tom Coppeto
// Okapia
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.summary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Summary</code>.
 */

public abstract class AbstractSummary
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendium
    implements org.osid.financials.Summary {

    private org.osid.financials.Account account;
    private org.osid.financials.FiscalPeriod period;
    private org.osid.financials.Currency credits;
    private org.osid.financials.Currency debits;
    private org.osid.financials.Currency budget;
    private org.osid.financials.Currency forecast;
    
    private final java.util.Collection<org.osid.financials.records.SummaryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the account. 
     *
     *  @return the account <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        return (this.account.getId());
    }


    /**
     *  Gets the account. 
     *
     *  @return the account 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {
        
        return (this.account);
    }


    /**
     *  Sets the account.
     *
     *  @param account the account
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    protected void setAccount(org.osid.financials.Account account) {
        nullarg(account, "account");
        this.account = account;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the fiscal period 
     *
     *  @return the fiscal period <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFiscalPeriodId() {
        return (this.period.getId());
    }


    /**
     *  Gets the fiscal period 
     *
     *  @return the fiscal period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod()
        throws org.osid.OperationFailedException {
        
        return (this.period);
    }


    /**
     *  Sets the fiscal period.
     *
     *  @param period the fiscal period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    protected void setFiscalPeriod(org.osid.financials.FiscalPeriod period) {
        nullarg(period, "fiscal period");
        this.period = period;
        return;
    }


    /**
     *  Gets the credits for this fiscal period. 
     *
     *  @return the credits 
     */

    @OSID @Override
    public org.osid.financials.Currency getCredits() {
        return (this.credits);
    }


    /**
     *  Sets the credits.
     *
     *  @param credits the credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void setCredits(org.osid.financials.Currency credits) {
        nullarg(credits, "credits");
        this.credits = credits;
        return;
    }


    /**
     *  Gets the debits for this fiscal period. 
     *
     *  @return the debits 
     */

    @OSID @Override
    public org.osid.financials.Currency getDebits() {
        return (this.debits);
    }


    /**
     *  Sets the debits.
     *
     *  @param debits the debits
     *  @throws org.osid.NullArgumentException <code>debits</code> is
     *          <code>null</code>
     */

    protected void setDebits(org.osid.financials.Currency debits) {
        nullarg(debits, "debits");
        this.debits = debits;
        return;
    }


    /**
     *  Tests if this the account on this activity has a budget for this time 
     *  period. 
     *
     *  @return <code> true </code> if a budget is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isBudgeted() {
        if (this.budget == null) {
            return (false);
        } else {
            return (true);
        }
    }

    
    /**
     *  Gets the budget. 
     *
     *  @return the budget 
     *  @throws org.osid.IllegalStateException <code> isBudgeted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getBudget() {
        if (!isBudgeted()) {
            throw new org.osid.IllegalStateException("isBudgeted() is false");
        }

        return (this.budget);
    }


    /**
     *  Sets the budget.
     *
     *  @param budget the budget
     *  @throws org.osid.NullArgumentException <code>budget</code> is
     *          <code>null</code>
     */

    protected void setBudget(org.osid.financials.Currency budget) {
        nullarg(budget, "budget");
        this.budget = budget;
        return;
    }


    /**
     *  Tests if this the account on this activity has a forecast for this 
     *  time period. 
     *
     *  @return <code> true </code> if a forecast is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasForecast() {
        if (this.forecast == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the forecast 
     *
     *  @return the forecast 
     *  @throws org.osid.IllegalStateException <code> hasForecast() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getForecast() {
        if (!hasForecast()) {
            throw new org.osid.IllegalStateException("hasForecast() is false");
        }

        return (this.forecast);
    }


    /**
     *  Sets the forecast.
     *
     *  @param forecast the forecast
     *  @throws org.osid.NullArgumentException <code>forecast</code> is
     *          <code>null</code>
     */

    protected void setForecast(org.osid.financials.Currency forecast) {
        nullarg(forecast, "forecast");
        this.forecast = forecast;
        return;
    }


    /**
     *  Tests if this summary supports the given record
     *  <code>Type</code>.
     *
     *  @param  summaryRecordType a summary record type 
     *  @return <code>true</code> if the summaryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type summaryRecordType) {
        for (org.osid.financials.records.SummaryRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Summary</code> record <code>Type</code>.
     *
     *  @param  summaryRecordType the summary record type 
     *  @return the summary record 
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.SummaryRecord getSummaryRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.SummaryRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this summary. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param summaryRecord the summary record
     *  @param summaryRecordType summary record type
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecord</code> or
     *          <code>summaryRecordTypesummary</code> is
     *          <code>null</code>
     */
            
    protected void addSummaryRecord(org.osid.financials.records.SummaryRecord summaryRecord, 
                                     org.osid.type.Type summaryRecordType) {
        
        nullarg(summaryRecord, "summary record");
        addRecordType(summaryRecordType);
        this.records.add(summaryRecord);
        
        return;
    }
}
