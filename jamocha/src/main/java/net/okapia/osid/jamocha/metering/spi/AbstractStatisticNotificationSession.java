//
// AbstractStatisticNotificationSession.java
//
//     A template for making StatisticNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Statistic} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Statistic} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for statistic entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractStatisticNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.metering.StatisticNotificationSession {

    private boolean federated = false;
    private org.osid.metering.Utility utility = new net.okapia.osid.jamocha.nil.metering.utility.UnknownUtility();


    /**
     *  Gets the {@code Utility/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Utility Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getUtilityId() {
        return (this.utility.getId());
    }

    
    /**
     *  Gets the {@code Utility} associated with this 
     *  session.
     *
     *  @return the {@code Utility} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.utility);
    }


    /**
     *  Sets the {@code Utility}.
     *
     *  @param  utility the utility for this session
     *  @throws org.osid.NullArgumentException {@code utility}
     *          is {@code null}
     */

    protected void setUtility(org.osid.metering.Utility utility) {
        nullarg(utility, "utility");
        this.utility = utility;
        return;
    }


    /**
     *  Tests if this user can register for {@code Statistic}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a
     *  {@code PERMISSION_DENIED}. This is intended as a hint to
     *  an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForStatisticNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include statistics in utilities which are children
     *  of this utility in the utility hierarchy.
     */

    @OSID @Override
    public void useFederatedUtilityView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this utility only.
     */

    @OSID @Override
    public void useIsolatedUtilityView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications sums exceeding a threshold. {@code
     *  StatisticReceiver.exceededSumThreshold()} is invoked when the
     *  sum exceeds the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsExceedingSumThreshold(org.osid.id.Id meterId, 
                                                           long interval, 
                                                           org.osid.calendaring.DateTimeResolution units, 
                                                           java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications sums failing a threshold. {@code 
     *  StatisticReceiver.failedSumThreshold()} is invoked when the sum 
     *  falls below the given value. 
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsFailingSumThreshold(org.osid.id.Id meterId, 
                                                         long interval, 
                                                         org.osid.calendaring.DateTimeResolution units, 
                                                         java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of means exceeding a
     *  threshold. {@code StatisticReceiver.exceededMeanThreshold()}
     *  is invoked when the mean exceeds the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId or
     *          units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsExceedingMeanThreshold(org.osid.id.Id meterId, 
                                                            long interval, 
                                                            org.osid.calendaring.DateTimeResolution units, 
                                                            java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications means failing a threshold. {@code
     *  StatisticReceiver.failedMeanThreshold()} is invoked when the
     *  mean falls below the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsFailingMeanThreshold(org.osid.id.Id meterId, 
                                                          long interval, 
                                                          org.osid.calendaring.DateTimeResolution units, 
                                                          java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of medians exceeding a
     *  threshold. {@code StatisticReceiver.exceededMeanThreshold()}
     *  is invoked when the median exceeds the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsExceedingMedianThreshold(org.osid.id.Id meterId, 
                                                              long interval, 
                                                              org.osid.calendaring.DateTimeResolution units, 
                                                              java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of medians failing a
     *  threshold. {@code StatisticReceiver.failedMeanThreshold()} is
     *  invoked when the median falls below the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsFailingMedianThreshold(org.osid.id.Id meterId, 
                                                            long interval, 
                                                            org.osid.calendaring.DateTimeResolution units, 
                                                            java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the root mean square exceeding a
     *  threshold. {@code StatisticReceiver.exceededRMSThreshold()} is
     *  invoked when the rms exceeds the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId or
     *          units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsExceedingRMSThreshold(org.osid.id.Id meterId, 
                                                           long interval, 
                                                           org.osid.calendaring.DateTimeResolution units, 
                                                           java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the root mean square failing a
     *  threshold. {@code StatisticReceiver.failedRMSThreshold()} is
     *  invoked when the rms falls below the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsFailingRMSThreshold(org.osid.id.Id meterId, 
                                                         long interval, 
                                                         org.osid.calendaring.DateTimeResolution units, 
                                                         java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the delta exceeding a
     *  threshold. {@code StatisticReceiver.exceededDeltaThreshold()}
     *  is invoked when the delta exceeds the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId or
     *          units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsExceedingDeltaThreshold(org.osid.id.Id meterId, 
                                                             long interval, 
                                                             org.osid.calendaring.DateTimeResolution units, 
                                                             java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the delta failing a
     *  threshold. {@code StatisticReceiver.failedDeltaThreshold()} is
     *  invoked when the delta falls below the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsFailingDeltaThreshold(org.osid.id.Id meterId, 
                                                           long interval, 
                                                           org.osid.calendaring.DateTimeResolution units, 
                                                           java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the percentage change exceeding
     *  a threshold. {@code
     *  StatisticReceiver.exceededPercentChangeThreshold()} is invoked
     *  when the change exceeds the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsExceedingPercentChangeThreshold(org.osid.id.Id meterId, 
                                                                     long interval, 
                                                                     org.osid.calendaring.DateTimeResolution units, 
                                                                     java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the percentage change failing a
     *  threshold. {@code
     *  StatisticReceiver.failedPercentChangeThreshold()} is invoked
     *  when the change falls below the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @throws org.osid.NullArgumentException {@code meterId} or
     *          {@code units} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsFailingPercentChangeThreshold(org.osid.id.Id meterId, 
                                                                   long interval, 
                                                                   org.osid.calendaring.DateTimeResolution units, 
                                                                   java.math.BigDecimal value)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the rate exceeding a
     *  threshold. {@code
     *  StatisticReceiver.exceededAverageRateThreshold()} is invoked
     *  when the average rate exceeds the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @param  rateInterval the time interval of the rate 
     *  @throws org.osid.NullArgumentException {@code meterId}, {@code
     *          units}, or {@code rateInterval} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsExceedingAverageRateThreshold(org.osid.id.Id meterId, 
                                                                   long interval, 
                                                                   org.osid.calendaring.DateTimeResolution units, 
                                                                   java.math.BigDecimal value, 
                                                                   org.osid.calendaring.DateTimeResolution rateInterval)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of the rate failing a
     *  threshold. {@code
     *  StatisticReceiver.failedAverageRateThreshold()} is invoked
     *  when the average rate falls below the given value.
     *
     *  @param  meterId the meter {@code Id} 
     *  @param  interval the time interval from the present 
     *  @param  units the date time units 
     *  @param  value the threshold value 
     *  @param  rateInterval the time interval of the rate 
     *  @throws org.osid.NullArgumentException {@code meterId}, {@code
     *          units}, or {@code rateInterval} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForStatisticsFailingAverageRateThreshold(org.osid.id.Id meterId, 
                                                                 long interval, 
                                                                 org.osid.calendaring.DateTimeResolution units, 
                                                                 java.math.BigDecimal value, 
                                                                 org.osid.calendaring.DateTimeResolution rateInterval)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }
}
