//
// AbstractQueueProcessorSearch.java
//
//     A template for making a QueueProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.queueprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing queue processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractQueueProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.tracking.rules.QueueProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.tracking.rules.QueueProcessorSearchOrder queueProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of queue processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  queueProcessorIds list of queue processors
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongQueueProcessors(org.osid.id.IdList queueProcessorIds) {
        while (queueProcessorIds.hasNext()) {
            try {
                this.ids.add(queueProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongQueueProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of queue processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getQueueProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  queueProcessorSearchOrder queue processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>queueProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderQueueProcessorResults(org.osid.tracking.rules.QueueProcessorSearchOrder queueProcessorSearchOrder) {
	this.queueProcessorSearchOrder = queueProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.tracking.rules.QueueProcessorSearchOrder getQueueProcessorSearchOrder() {
	return (this.queueProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given queue processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue processor implementing the requested record.
     *
     *  @param queueProcessorSearchRecordType a queue processor search record
     *         type
     *  @return the queue processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueProcessorSearchRecord getQueueProcessorSearchRecord(org.osid.type.Type queueProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.tracking.rules.records.QueueProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue processor search. 
     *
     *  @param queueProcessorSearchRecord queue processor search record
     *  @param queueProcessorSearchRecordType queueProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueProcessorSearchRecord(org.osid.tracking.rules.records.QueueProcessorSearchRecord queueProcessorSearchRecord, 
                                           org.osid.type.Type queueProcessorSearchRecordType) {

        addRecordType(queueProcessorSearchRecordType);
        this.records.add(queueProcessorSearchRecord);        
        return;
    }
}
