//
// AbstractVersionMetadata.java
//
//     Defines a version Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.installation.SoftwareVersion;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a version Metadata.
 */

public abstract class AbstractVersionMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final Types types = new TypeSet();

    private org.osid.installation.Version minimum = SoftwareVersion.valueOf("0.0.0");
    private org.osid.installation.Version maximum = SoftwareVersion.valueOf("9.9.9");

    private final java.util.Collection<org.osid.installation.Version> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.Version> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.Version> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractVersionMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractVersionMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.VERSION, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractVersionMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractVersionMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.VERSION, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the set of acceptable version types. 
     *
     *  @return a set of version types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>VERSION</code> or <code>DURATION</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getVersionTypes() {
        return (this.types.toArray());
    }
 

    /**
     *  Tests if the given version type is supported. 
     *
     *  @param  versionType a version Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>VERSION</code> or <code>DURATION</code>
     *  @throws org.osid.NullArgumentException <code> versionType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVersionType(org.osid.type.Type versionType) {
        return (this.types.contains(versionType));
    }


    /**
     *  Add support for a version type.
     *
     *  @param versionType the type of version
     *  @throws org.osid.NullArgumentException {@code versionType} is
     *          {@code null}
     */

    protected void addVersionType(org.osid.type.Type versionType) {
        this.types.add(versionType);
        return;
    }


    /**
     *  Gets the minimum version value. 
     *
     *  @return the minimum version 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          VERSION </code>
     */

    @OSID @Override
    public org.osid.installation.Version getMinimumVersion() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum version value. 
     *
     *  @return the maximum version 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          VERSION </code>
     */

    @OSID @Override
    public org.osid.installation.Version getMaximumVersion() {
        return (this.maximum);
    }


    /**
     *  Sets the min and max versions.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          newer than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    protected void setVersionRange(org.osid.installation.Version min, org.osid.installation.Version max) {
        nullarg(min, "min version");
        nullarg(max, "max version");

        if (min.isNewer(max)) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.maximum = min;
        this.maximum = min;

        return;
    }


    /**
     *  Gets the set of acceptable version values. 
     *
     *  @return a set of versions or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          VERSION </code>
     */

    @OSID @Override
    public org.osid.installation.Version[] getVersionSet() {
        return (this.set.toArray(new org.osid.installation.Version[this.set.size()]));
    }

    
    /**
     *  Sets the version set.
     *
     *  @param values a collection of accepted version values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setVersionSet(java.util.Collection<org.osid.installation.Version> values) {
        this.set.clear();
        addToVersionSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the version set.
     *
     *  @param values a collection of accepted version values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToVersionSet(java.util.Collection<org.osid.installation.Version> values) {
        nullarg(values, "version set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the version set.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToVersionSet(org.osid.installation.Version value) {
        nullarg(value, "version value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the version set.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromVersionSet(org.osid.installation.Version value) {
        nullarg(value, "version value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the version set.
     */

    protected void clearVersionSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default version values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default version values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          VERSION </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.installation.Version[] getDefaultVersionValues() {
        return (this.defvals.toArray(new org.osid.installation.Version[this.defvals.size()]));
    }


    /**
     *  Sets the default version set.
     *
     *  @param values a collection of default version values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        clearDefaultVersionValues();
        addDefaultVersionValues(values);
        return;
    }


    /**
     *  Adds a collection of default version values.
     *
     *  @param values a collection of default version values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        nullarg(values, "default version values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultVersionValue(org.osid.installation.Version value) {
        nullarg(value, "default version value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultVersionValue(org.osid.installation.Version value) {
        nullarg(value, "default version value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default version values.
     */

    protected void clearDefaultVersionValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing version values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing version values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          VERSION </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.installation.Version[] getExistingVersionValues() {
        return (this.existing.toArray(new org.osid.installation.Version[this.existing.size()]));
    }


    /**
     *  Sets the existing version set.
     *
     *  @param values a collection of existing version values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        clearExistingVersionValues();
        addExistingVersionValues(values);
        return;
    }


    /**
     *  Adds a collection of existing version values.
     *
     *  @param values a collection of existing version values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingVersionValues(java.util.Collection<org.osid.installation.Version> values) {
        nullarg(values, "existing version values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingVersionValue(org.osid.installation.Version value) {
        nullarg(value, "existing version value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingVersionValue(org.osid.installation.Version value) {
        nullarg(value, "existing version value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing version values.
     */

    protected void clearExistingVersionValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}
