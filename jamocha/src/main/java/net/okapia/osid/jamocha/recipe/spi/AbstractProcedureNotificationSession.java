//
// AbstractProcedureNotificationSession.java
//
//     A template for making ProcedureNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Procedure} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Procedure} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for procedure entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractProcedureNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.recipe.ProcedureNotificationSession {

    private boolean federated = false;
    private org.osid.recipe.Cookbook cookbook = new net.okapia.osid.jamocha.nil.recipe.cookbook.UnknownCookbook();


    /**
     *  Gets the {@code Cookbook/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Cookbook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.cookbook.getId());
    }

    
    /**
     *  Gets the {@code Cookbook} associated with this session.
     *
     *  @return the {@code Cookbook} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.cookbook);
    }


    /**
     *  Sets the {@code Cookbook}.
     *
     *  @param cookbook the cookbook for this session
     *  @throws org.osid.NullArgumentException {@code cookbook}
     *          is {@code null}
     */

    protected void setCookbook(org.osid.recipe.Cookbook cookbook) {
        nullarg(cookbook, "cookbook");
        this.cookbook = cookbook;
        return;
    }


    /**
     *  Tests if this user can register for {@code Procedure}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForProcedureNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include procedures in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new procedures. {@code
     *  ProcedureReceiver.newProcedure()} is invoked when a new {@code
     *  Procedure} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewProcedures()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated procedures. {@code
     *  ProcedureReceiver.changedProcedure()} is invoked when a
     *  procedure is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProcedures()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated procedure. {@code
     *  ProcedureReceiver.changedProcedure()} is invoked when the
     *  specified procedure is changed.
     *
     *  @param procedureId the {@code Id} of the {@code Procedure} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code procedureId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProcedure(org.osid.id.Id procedureId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted procedures. {@code
     *  ProcedureReceiver.deletedProcedure()} is invoked when a
     *  procedure is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProcedures()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted procedure. {@code
     *  ProcedureReceiver.deletedProcedure()} is invoked when the
     *  specified procedure is deleted.
     *
     *  @param procedureId the {@code Id} of the
     *          {@code Procedure} to monitor
     *  @throws org.osid.NullArgumentException {@code procedureId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProcedure(org.osid.id.Id procedureId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
