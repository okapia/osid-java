//
// OrderElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.order.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OrderElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the OrderElement Id.
     *
     *  @return the order element Id
     */

    public static org.osid.id.Id getOrderEntityId() {
        return (makeEntityId("osid.ordering.Order"));
    }


    /**
     *  Gets the CustomerId element Id.
     *
     *  @return the CustomerId element Id
     */

    public static org.osid.id.Id getCustomerId() {
        return (makeElementId("osid.ordering.order.CustomerId"));
    }


    /**
     *  Gets the Customer element Id.
     *
     *  @return the Customer element Id
     */

    public static org.osid.id.Id getCustomer() {
        return (makeElementId("osid.ordering.order.Customer"));
    }


    /**
     *  Gets the ItemIds element Id.
     *
     *  @return the ItemIds element Id
     */

    public static org.osid.id.Id getItemIds() {
        return (makeElementId("osid.ordering.order.ItemIds"));
    }


    /**
     *  Gets the Items element Id.
     *
     *  @return the Items element Id
     */

    public static org.osid.id.Id getItems() {
        return (makeElementId("osid.ordering.order.Items"));
    }


    /**
     *  Gets the TotalCost element Id.
     *
     *  @return the TotalCost element Id
     */

    public static org.osid.id.Id getTotalCost() {
        return (makeElementId("osid.ordering.order.TotalCost"));
    }


    /**
     *  Gets the SubmitDate element Id.
     *
     *  @return the SubmitDate element Id
     */

    public static org.osid.id.Id getSubmitDate() {
        return (makeElementId("osid.ordering.order.SubmitDate"));
    }


    /**
     *  Gets the SubmitterId element Id.
     *
     *  @return the SubmitterId element Id
     */

    public static org.osid.id.Id getSubmitterId() {
        return (makeElementId("osid.ordering.order.SubmitterId"));
    }


    /**
     *  Gets the Submitter element Id.
     *
     *  @return the Submitter element Id
     */

    public static org.osid.id.Id getSubmitter() {
        return (makeElementId("osid.ordering.order.Submitter"));
    }


    /**
     *  Gets the SubmittingAgentId element Id.
     *
     *  @return the SubmittingAgentId element Id
     */

    public static org.osid.id.Id getSubmittingAgentId() {
        return (makeElementId("osid.ordering.order.SubmittingAgentId"));
    }


    /**
     *  Gets the SubmittingAgent element Id.
     *
     *  @return the SubmittingAgent element Id
     */

    public static org.osid.id.Id getSubmittingAgent() {
        return (makeElementId("osid.ordering.order.SubmittingAgent"));
    }


    /**
     *  Gets the ClosedDate element Id.
     *
     *  @return the ClosedDate element Id
     */

    public static org.osid.id.Id getClosedDate() {
        return (makeElementId("osid.ordering.order.ClosedDate"));
    }


    /**
     *  Gets the CloserId element Id.
     *
     *  @return the CloserId element Id
     */

    public static org.osid.id.Id getCloserId() {
        return (makeElementId("osid.ordering.order.CloserId"));
    }


    /**
     *  Gets the Closer element Id.
     *
     *  @return the Closer element Id
     */

    public static org.osid.id.Id getCloser() {
        return (makeElementId("osid.ordering.order.Closer"));
    }


    /**
     *  Gets the ClosingAgentId element Id.
     *
     *  @return the ClosingAgentId element Id
     */

    public static org.osid.id.Id getClosingAgentId() {
        return (makeElementId("osid.ordering.order.ClosingAgentId"));
    }


    /**
     *  Gets the ClosingAgent element Id.
     *
     *  @return the ClosingAgent element Id
     */

    public static org.osid.id.Id getClosingAgent() {
        return (makeElementId("osid.ordering.order.ClosingAgent"));
    }


    /**
     *  Gets the MinimumTotalCost element Id.
     *
     *  @return the MinimumTotalCost element Id
     */

    public static org.osid.id.Id getMinimumTotalCost() {
        return (makeQueryElementId("osid.ordering.order.MinimumTotalCost"));
    }


    /**
     *  Gets the Atomic element Id.
     *
     *  @return the Atomic element Id
     */

    public static org.osid.id.Id getAtomic() {
        return (makeElementId("osid.ordering.order.Atomic"));
    }


    /**
     *  Gets the StoreId element Id.
     *
     *  @return the StoreId element Id
     */

    public static org.osid.id.Id getStoreId() {
        return (makeQueryElementId("osid.ordering.order.StoreId"));
    }


    /**
     *  Gets the Store element Id.
     *
     *  @return the Store element Id
     */

    public static org.osid.id.Id getStore() {
        return (makeQueryElementId("osid.ordering.order.Store"));
    }


    /**
     *  Gets the Closed element Id.
     *
     *  @return the Closed element Id
     */

    public static org.osid.id.Id getClosed() {
        return (makeElementId("osid.ordering.order.Closed"));
    }
}
