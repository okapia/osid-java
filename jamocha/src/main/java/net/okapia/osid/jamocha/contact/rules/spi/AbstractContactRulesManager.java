//
// AbstractContactRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractContactRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.contact.rules.ContactRulesManager,
               org.osid.contact.rules.ContactRulesProxyManager {

    private final Types contactEnablerRecordTypes          = new TypeRefSet();
    private final Types contactEnablerSearchRecordTypes    = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractContactRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractContactRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up contact enablers is supported. 
     *
     *  @return <code> true </code> if contact enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying contact enablers is supported. 
     *
     *  @return <code> true </code> if contact enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching contact enablers is supported. 
     *
     *  @return <code> true </code> if contact enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a contact enabler administrative service is supported. 
     *
     *  @return <code> true </code> if contact enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a contact enabler notification service is supported. 
     *
     *  @return <code> true </code> if contact enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a contact enabler address book lookup service is supported. 
     *
     *  @return <code> true </code> if a contact enabler address book lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerAddressBook() {
        return (false);
    }


    /**
     *  Tests if a contact enabler address book service is supported. 
     *
     *  @return <code> true </code> if contact enabler address book assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerAddressBookAssignment() {
        return (false);
    }


    /**
     *  Tests if a contact enabler address book lookup service is supported. 
     *
     *  @return <code> true </code> if a contact enabler address book service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerSmartAddressBook() {
        return (false);
    }


    /**
     *  Tests if a contact enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a contact enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a contact enabler rule application service is supported. 
     *
     *  @return <code> true </code> if contact enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> ContactEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ContactEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContactEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.contactEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ContactEnabler </code> record type is 
     *  supported. 
     *
     *  @param  contactEnablerRecordType a <code> Type </code> indicating a 
     *          <code> ContactEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> contactEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContactEnablerRecordType(org.osid.type.Type contactEnablerRecordType) {
        return (this.contactEnablerRecordTypes.contains(contactEnablerRecordType));
    }


    /**
     *  Adds support for a contact enabler record type.
     *
     *  @param contactEnablerRecordType a contact enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>contactEnablerRecordType</code> is <code>null</code>
     */

    protected void addContactEnablerRecordType(org.osid.type.Type contactEnablerRecordType) {
        this.contactEnablerRecordTypes.add(contactEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a contact enabler record type.
     *
     *  @param contactEnablerRecordType a contact enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>contactEnablerRecordType</code> is <code>null</code>
     */

    protected void removeContactEnablerRecordType(org.osid.type.Type contactEnablerRecordType) {
        this.contactEnablerRecordTypes.remove(contactEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ContactEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ContactEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContactEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.contactEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ContactEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  contactEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> ContactEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          contactEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContactEnablerSearchRecordType(org.osid.type.Type contactEnablerSearchRecordType) {
        return (this.contactEnablerSearchRecordTypes.contains(contactEnablerSearchRecordType));
    }


    /**
     *  Adds support for a contact enabler search record type.
     *
     *  @param contactEnablerSearchRecordType a contact enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>contactEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addContactEnablerSearchRecordType(org.osid.type.Type contactEnablerSearchRecordType) {
        this.contactEnablerSearchRecordTypes.add(contactEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a contact enabler search record type.
     *
     *  @param contactEnablerSearchRecordType a contact enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>contactEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeContactEnablerSearchRecordType(org.osid.type.Type contactEnablerSearchRecordType) {
        this.contactEnablerSearchRecordTypes.remove(contactEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler lookup service. 
     *
     *  @return a <code> ContactEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerLookupSession getContactEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerLookupSession getContactEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler lookup service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerLookupSession getContactEnablerLookupSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerLookupSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler lookup service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerLookupSession getContactEnablerLookupSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerLookupSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler query service. 
     *
     *  @return a <code> ContactEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerQuerySession getContactEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerQuerySession getContactEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler query service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerQuerySession getContactEnablerQuerySessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerQuerySessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler query service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerQuerySession getContactEnablerQuerySessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerQuerySessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler search service. 
     *
     *  @return a <code> ContactEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSearchSession getContactEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSearchSession getContactEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enablers earch service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSearchSession getContactEnablerSearchSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerSearchSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enablers earch service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSearchSession getContactEnablerSearchSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerSearchSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler administration service. 
     *
     *  @return a <code> ContactEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAdminSession getContactEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAdminSession getContactEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler administration service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAdminSession getContactEnablerAdminSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerAdminSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler administration service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAdminSession getContactEnablerAdminSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerAdminSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler notification service. 
     *
     *  @param  contactEnablerReceiver the notification callback 
     *  @return a <code> ContactEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> contactEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerNotificationSession getContactEnablerNotificationSession(org.osid.contact.rules.ContactEnablerReceiver contactEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler notification service. 
     *
     *  @param  contactEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> contactEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerNotificationSession getContactEnablerNotificationSession(org.osid.contact.rules.ContactEnablerReceiver contactEnablerReceiver, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler notification service for the given address book. 
     *
     *  @param  contactEnablerReceiver the notification callback 
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no address book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> contactEnablerReceiver 
     *          </code> or <code> addressBookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerNotificationSession getContactEnablerNotificationSessionForAddressBook(org.osid.contact.rules.ContactEnablerReceiver contactEnablerReceiver, 
                                                                                                                       org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerNotificationSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler notification service for the given address book. 
     *
     *  @param  contactEnablerReceiver the notification callback 
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no address book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> contactEnablerReceiver, 
     *          addressBookId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerNotificationSession getContactEnablerNotificationSessionForAddressBook(org.osid.contact.rules.ContactEnablerReceiver contactEnablerReceiver, 
                                                                                                                       org.osid.id.Id addressBookId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerNotificationSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup contact enabler/address 
     *  book mappings for contact enablers. 
     *
     *  @return a <code> ContactEnablerAddressBookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAddressBook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAddressBookSession getContactEnablerAddressBookSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerAddressBookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup contact enabler/address 
     *  book mappings for contact enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerAddressBookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAddressBook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAddressBookSession getContactEnablerAddressBookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerAddressBookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning contact 
     *  enablers to address books for contact. 
     *
     *  @return a <code> ContactEnablerAddressBookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAddressBookAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAddressBookAssignmentSession getContactEnablerAddressBookAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerAddressBookAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning contact 
     *  enablers to address books for contact. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerAddressBookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerAddressBookAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerAddressBookAssignmentSession getContactEnablerAddressBookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerAddressBookAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage contact enabler smart 
     *  address books. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerSmartAddressBookSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSmartAddressBook() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSmartAddressBookSession getContactEnablerSmartAddressBookSession(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerSmartAddressBookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage contact enabler smart 
     *  address books. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerSmartAddressBookSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerSmartAddressBook() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerSmartAddressBookSession getContactEnablerSmartAddressBookSession(org.osid.id.Id addressBookId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerSmartAddressBookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> ContactEnablertRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleLookupSession getContactEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  address book. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleLookupSession getContactEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler mapping lookup service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleLookupSession getContactEnablerRuleLookupSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerRuleLookupSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler mapping lookup service for the given address book for looking 
     *  up rules applied to an address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleLookupSession getContactEnablerRuleLookupSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerRuleLookupSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler assignment service to apply enablers. 
     *
     *  @return a <code> ContactEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleApplicationSession getContactEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler assignment service to apply enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleApplicationSession getContactEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler assignment service for the given address book to apply 
     *  enablers. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @return a <code> ContactEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleApplicationSession getContactEnablerRuleApplicationSessionForAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesManager.getContactEnablerRuleApplicationSessionForAddressBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  enabler assignment service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerRuleApplicationSession getContactEnablerRuleApplicationSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.contact.rules.ContactRulesProxyManager.getContactEnablerRuleApplicationSessionForAddressBook not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.contactEnablerRecordTypes.clear();
        this.contactEnablerRecordTypes.clear();

        this.contactEnablerSearchRecordTypes.clear();
        this.contactEnablerSearchRecordTypes.clear();

        return;
    }
}
