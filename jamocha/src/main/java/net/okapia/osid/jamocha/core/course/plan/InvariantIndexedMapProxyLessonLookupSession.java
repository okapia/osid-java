//
// InvariantIndexedMapProxyLessonLookupSession
//
//    Implements a Lesson lookup service backed by a fixed
//    collection of lessons indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.plan;


/**
 *  Implements a Lesson lookup service backed by a fixed
 *  collection of lessons. The lessons are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some lessons may be compatible
 *  with more types than are indicated through these lesson
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyLessonLookupSession
    extends net.okapia.osid.jamocha.core.course.plan.spi.AbstractIndexedMapLessonLookupSession
    implements org.osid.course.plan.LessonLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyLessonLookupSession}
     *  using an array of lessons.
     *
     *  @param courseCatalog the course catalog
     *  @param lessons an array of lessons
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code lessons} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyLessonLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                         org.osid.course.plan.Lesson[] lessons, 
                                                         org.osid.proxy.Proxy proxy) {

        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        putLessons(lessons);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyLessonLookupSession}
     *  using a collection of lessons.
     *
     *  @param courseCatalog the course catalog
     *  @param lessons a collection of lessons
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code lessons} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyLessonLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                         java.util.Collection<? extends org.osid.course.plan.Lesson> lessons,
                                                         org.osid.proxy.Proxy proxy) {

        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        putLessons(lessons);

        return;
    }
}
