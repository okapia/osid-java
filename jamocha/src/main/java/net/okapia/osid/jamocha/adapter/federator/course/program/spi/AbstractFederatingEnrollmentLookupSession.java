//
// AbstractFederatingEnrollmentLookupSession.java
//
//     An abstract federating adapter for an EnrollmentLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EnrollmentLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEnrollmentLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.program.EnrollmentLookupSession>
    implements org.osid.course.program.EnrollmentLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingEnrollmentLookupSession</code>.
     */

    protected AbstractFederatingEnrollmentLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.program.EnrollmentLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Enrollment</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEnrollments() {
        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            if (session.canLookupEnrollments()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Enrollment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEnrollmentView() {
        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            session.useComparativeEnrollmentView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Enrollment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEnrollmentView() {
        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            session.usePlenaryEnrollmentView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include enrollments in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only enrollments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEnrollmentView() {
        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            session.useEffectiveEnrollmentView();
        }

        return;
    }


    /**
     *  All enrollments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEnrollmentView() {
        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            session.useAnyEffectiveEnrollmentView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Enrollment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Enrollment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Enrollment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentId <code>Id</code> of the
     *          <code>Enrollment</code>
     *  @return the enrollment
     *  @throws org.osid.NotFoundException <code>enrollmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>enrollmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Enrollment getEnrollment(org.osid.id.Id enrollmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            try {
                return (session.getEnrollment(enrollmentId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(enrollmentId + " not found");
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  enrollments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Enrollments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, enrollments are returned that are currently effective.
     *  In any effective mode, effective enrollments and those currently expired
     *  are returned.
     *
     *  @param  enrollmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByIds(org.osid.id.IdList enrollmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.program.enrollment.MutableEnrollmentList ret = new net.okapia.osid.jamocha.course.program.enrollment.MutableEnrollmentList();

        try (org.osid.id.IdList ids = enrollmentIds) {
            while (ids.hasNext()) {
                ret.addEnrollment(getEnrollment(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  enrollment genus <code>Type</code> which does not include
     *  enrollments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently effective.
     *  In any effective mode, effective enrollments and those currently expired
     *  are returned.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsByGenusType(enrollmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  enrollment genus <code>Type</code> and include any additional
     *  enrollments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByParentGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsByParentGenusType(enrollmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EnrollmentList</code> containing the given
     *  enrollment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByRecordType(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsByRecordType(enrollmentRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EnrollmentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *  
     *  In active mode, enrollments are returned that are currently
     *  active. In any status mode, active and inactive enrollments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Enrollment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOffering(org.osid.id.Id programOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgramOffering(programOfferingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingOnDate(org.osid.id.Id programOfferingId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgramOfferingOnDate(programOfferingId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.program.EnrollmentList getEnrollmentsForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForStudent(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForStudentOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to program offering and student
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudent(org.osid.id.Id programOfferingId,
                                                                                             org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgramOfferingAndStudent(programOfferingId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to program offering
     *  and student <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudentOnDate(org.osid.id.Id programOfferingId,
                                                                                                   org.osid.id.Id resourceId,
                                                                                                   org.osid.calendaring.DateTime from,
                                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgramOfferingAndStudentOnDate(programOfferingId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to a program 
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program 
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.program.EnrollmentList getEnrollmentsForProgram(org.osid.id.Id programId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgram(programId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to a program 
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program 
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOnDate(org.osid.id.Id programId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgramOnDate(programId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to program and
     *  student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program 
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramAndStudent(org.osid.id.Id programId,
                                                                                     org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgramAndStudent(programId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of enrollments corresponding to program 
     *  and student <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramAndStudentOnDate(org.osid.id.Id programId,
                                                                                           org.osid.id.Id resourceId,
                                                                                           org.osid.calendaring.DateTime from,
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollmentsForProgramAndStudentOnDate(programId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Enrollments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Enrollments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList ret = getEnrollmentList();

        for (org.osid.course.program.EnrollmentLookupSession session : getSessions()) {
            ret.addEnrollmentList(session.getEnrollments());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.FederatingEnrollmentList getEnrollmentList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.ParallelEnrollmentList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.program.enrollment.CompositeEnrollmentList());
        }
    }
}
