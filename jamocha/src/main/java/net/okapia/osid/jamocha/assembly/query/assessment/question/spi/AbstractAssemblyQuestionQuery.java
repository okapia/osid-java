//
// AbstractAssemblyQuestionQuery.java
//
//     A QuestionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.question.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QuestionQuery that stores terms.
 */

public abstract class AbstractAssemblyQuestionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.assessment.QuestionQuery,
               org.osid.assessment.QuestionQueryInspector,
               org.osid.assessment.QuestionSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.QuestionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.QuestionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.QuestionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyQuestionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyQuestionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    




    /**
     *  Tests if this question supports the given record
     *  <code>Type</code>.
     *
     *  @param  questionRecordType a question record type 
     *  @return <code>true</code> if the questionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>questionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type questionRecordType) {
        for (org.osid.assessment.records.QuestionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(questionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  questionRecordType the question record type 
     *  @return the question query record 
     *  @throws org.osid.NullArgumentException
     *          <code>questionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(questionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.QuestionQueryRecord getQuestionQueryRecord(org.osid.type.Type questionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.QuestionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(questionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(questionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  questionRecordType the question record type 
     *  @return the question query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>questionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(questionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.QuestionQueryInspectorRecord getQuestionQueryInspectorRecord(org.osid.type.Type questionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.QuestionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(questionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(questionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param questionRecordType the question record type
     *  @return the question search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>questionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(questionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.QuestionSearchOrderRecord getQuestionSearchOrderRecord(org.osid.type.Type questionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.QuestionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(questionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(questionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this question. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param questionQueryRecord the question query record
     *  @param questionQueryInspectorRecord the question query inspector
     *         record
     *  @param questionSearchOrderRecord the question search order record
     *  @param questionRecordType question record type
     *  @throws org.osid.NullArgumentException
     *          <code>questionQueryRecord</code>,
     *          <code>questionQueryInspectorRecord</code>,
     *          <code>questionSearchOrderRecord</code> or
     *          <code>questionRecordTypequestion</code> is
     *          <code>null</code>
     */
            
    protected void addQuestionRecords(org.osid.assessment.records.QuestionQueryRecord questionQueryRecord, 
                                      org.osid.assessment.records.QuestionQueryInspectorRecord questionQueryInspectorRecord, 
                                      org.osid.assessment.records.QuestionSearchOrderRecord questionSearchOrderRecord, 
                                      org.osid.type.Type questionRecordType) {

        addRecordType(questionRecordType);

        nullarg(questionQueryRecord, "question query record");
        nullarg(questionQueryInspectorRecord, "question query inspector record");
        nullarg(questionSearchOrderRecord, "question search odrer record");

        this.queryRecords.add(questionQueryRecord);
        this.queryInspectorRecords.add(questionQueryInspectorRecord);
        this.searchOrderRecords.add(questionSearchOrderRecord);
        
        return;
    }
}
