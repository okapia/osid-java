//
// AbstractRecurringEventEnablerNotificationSession.java
//
//     A template for making RecurringEventEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code RecurringEventEnabler} objects. This
 *  session is intended for consumers needing to synchronize their
 *  state with this service without the use of polling. Notifications
 *  are cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code RecurringEventEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for recurring event enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRecurringEventEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.rules.RecurringEventEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Gets the {@code Calendar} {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }

    
    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the {@code Calendar}.
     *
     *  @param calendar the calendar for this session
     *  @throws org.osid.NullArgumentException {@code calendar}
     *          is {@code null}
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  RecurringEventEnabler} notifications.  A return of true does
     *  not guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRecurringEventEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeRecurringEventEnablerNotification() </code>.
     */

    @OSID @Override
    public void reliableRecurringEventEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableRecurringEventEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge a recurring event enabler notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeRecurringEventEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for recurring event enablersin
     *  calendars which are children of this calendar in the calendar
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new recurring event
     *  enablers. {@code
     *  RecurringEventEnablerReceiver.newRecurringEventEnabler()} is
     *  invoked when a new {@code RecurringEventEnabler} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRecurringEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated recurring event
     *  enablers. {@code
     *  RecurringEventEnablerReceiver.changedRecurringEventEnabler()}
     *  is invoked when a recurring event enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRecurringEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated recurring event
     *  enabler. {@code
     *  RecurringEventEnablerReceiver.changedRecurringEventEnabler()}
     *  is invoked when the specified recurring event enabler is
     *  changed.
     *
     *  @param recurringEventEnablerId the {@code Id} of the {@code RecurringEventEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code recurringEventEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted recurring event
     *  enablers. {@code
     *  RecurringEventEnablerReceiver.deletedRecurringEventEnabler()}
     *  is invoked when a recurring event enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRecurringEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted recurring event
     *  enabler. {@code
     *  RecurringEventEnablerReceiver.deletedRecurringEventEnabler()}
     *  is invoked when the specified recurring event enabler is
     *  deleted.
     *
     *  @param recurringEventEnablerId the {@code Id} of the
     *          {@code RecurringEventEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code recurringEventEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
