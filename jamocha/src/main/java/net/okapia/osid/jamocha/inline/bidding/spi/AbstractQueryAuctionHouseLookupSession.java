//
// AbstractQueryAuctionHouseLookupSession.java
//
//    An inline adapter that maps an AuctionHouseLookupSession to
//    an AuctionHouseQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AuctionHouseLookupSession to
 *  an AuctionHouseQuerySession.
 */

public abstract class AbstractQueryAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.bidding.spi.AbstractAuctionHouseLookupSession
    implements org.osid.bidding.AuctionHouseLookupSession {

    private final org.osid.bidding.AuctionHouseQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAuctionHouseLookupSession.
     *
     *  @param querySession the underlying auction house query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAuctionHouseLookupSession(org.osid.bidding.AuctionHouseQuerySession querySession) {
        nullarg(querySession, "auction house query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>AuctionHouse</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionHouses() {
        return (this.session.canSearchAuctionHouses());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>AuctionHouse</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionHouse</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionHouse</code> and
     *  retained for compatibility.
     *
     *  @param  auctionHouseId <code>Id</code> of the
     *          <code>AuctionHouse</code>
     *  @return the auction house
     *  @throws org.osid.NotFoundException <code>auctionHouseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionHouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionHouseQuery query = getQuery();
        query.matchId(auctionHouseId, true);
        org.osid.bidding.AuctionHouseList auctionHouses = this.session.getAuctionHousesByQuery(query);
        if (auctionHouses.hasNext()) {
            return (auctionHouses.getNextAuctionHouse());
        } 
        
        throw new org.osid.NotFoundException(auctionHouseId + " not found");
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionHouses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuctionHouses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  auctionHouseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByIds(org.osid.id.IdList auctionHouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionHouseQuery query = getQuery();

        try (org.osid.id.IdList ids = auctionHouseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAuctionHousesByQuery(query));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  auction house genus <code>Type</code> which does not include
     *  auction houses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionHouseQuery query = getQuery();
        query.matchGenusType(auctionHouseGenusType, true);
        return (this.session.getAuctionHousesByQuery(query));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  auction house genus <code>Type</code> and include any additional
     *  auction houses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByParentGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionHouseQuery query = getQuery();
        query.matchParentGenusType(auctionHouseGenusType, true);
        return (this.session.getAuctionHousesByQuery(query));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> containing the given
     *  auction house record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseRecordType an auctionHouse record type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByRecordType(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionHouseQuery query = getQuery();
        query.matchRecordType(auctionHouseRecordType, true);
        return (this.session.getAuctionHousesByQuery(query));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known auction houses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  auction houses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>AuctionHouse</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionHouseQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getAuctionHousesByQuery(query));        
    }

    
    /**
     *  Gets all <code>AuctionHouses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AuctionHouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionHouseQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAuctionHousesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.bidding.AuctionHouseQuery getQuery() {
        org.osid.bidding.AuctionHouseQuery query = this.session.getAuctionHouseQuery();
        return (query);
    }
}
