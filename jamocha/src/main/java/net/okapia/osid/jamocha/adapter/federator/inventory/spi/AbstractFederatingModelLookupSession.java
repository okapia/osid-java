//
// AbstractFederatingModelLookupSession.java
//
//     An abstract federating adapter for a ModelLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ModelLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingModelLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inventory.ModelLookupSession>
    implements org.osid.inventory.ModelLookupSession {

    private boolean parallel = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();


    /**
     *  Constructs a new <code>AbstractFederatingModelLookupSession</code>.
     */

    protected AbstractFederatingModelLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inventory.ModelLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Model</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupModels() {
        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            if (session.canLookupModels()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Model</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeModelView() {
        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            session.useComparativeModelView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Model</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryModelView() {
        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            session.usePlenaryModelView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include models in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            session.useFederatedWarehouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            session.useIsolatedWarehouseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Model</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Model</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Model</code> and
     *  retained for compatibility.
     *
     *  @param  modelId <code>Id</code> of the
     *          <code>Model</code>
     *  @return the model
     *  @throws org.osid.NotFoundException <code>modelId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>modelId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel(org.osid.id.Id modelId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            try {
                return (session.getModel(modelId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(modelId + " not found");
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  models specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Models</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  modelIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>modelIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByIds(org.osid.id.IdList modelIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inventory.model.MutableModelList ret = new net.okapia.osid.jamocha.inventory.model.MutableModelList();

        try (org.osid.id.IdList ids = modelIds) {
            while (ids.hasNext()) {
                ret.addModel(getModel(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  model genus <code>Type</code> which does not include
     *  models of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.model.FederatingModelList ret = getModelList();

        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            ret.addModelList(session.getModelsByGenusType(modelGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  model genus <code>Type</code> and include any additional
     *  models with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByParentGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.model.FederatingModelList ret = getModelList();

        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            ret.addModelList(session.getModelsByParentGenusType(modelGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ModelList</code> containing the given
     *  model record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelRecordType a model record type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByRecordType(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.model.FederatingModelList ret = getModelList();

        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            ret.addModelList(session.getModelsByRecordType(modelRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ModelList</code> for the given resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known models
     *  or an error results. Otherwise, the returned list may contain
     *  only those models that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByManufacturer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.model.FederatingModelList ret = getModelList();

        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            ret.addModelList(session.getModelsByManufacturer(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Models</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Models</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModels()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.model.FederatingModelList ret = getModelList();

        for (org.osid.inventory.ModelLookupSession session : getSessions()) {
            ret.addModelList(session.getModels());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inventory.model.FederatingModelList getModelList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.model.ParallelModelList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.model.CompositeModelList());
        }
    }
}
