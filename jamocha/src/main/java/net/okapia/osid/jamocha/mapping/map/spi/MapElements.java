//
// MapElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.map.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class MapElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the MapElement Id.
     *
     *  @return the map element Id
     */

    public static org.osid.id.Id getMapEntityId() {
        return (makeEntityId("osid.mapping.Map"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeQueryElementId("osid.mapping.map.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeQueryElementId("osid.mapping.map.Location"));
    }


    /**
     *  Gets the PathId element Id.
     *
     *  @return the PathId element Id
     */

    public static org.osid.id.Id getPathId() {
        return (makeQueryElementId("osid.mapping.map.PathId"));
    }


    /**
     *  Gets the Path element Id.
     *
     *  @return the Path element Id
     */

    public static org.osid.id.Id getPath() {
        return (makeQueryElementId("osid.mapping.map.Path"));
    }


    /**
     *  Gets the RouteId element Id.
     *
     *  @return the RouteId element Id
     */

    public static org.osid.id.Id getRouteId() {
        return (makeQueryElementId("osid.mapping.map.RouteId"));
    }


    /**
     *  Gets the Route element Id.
     *
     *  @return the Route element Id
     */

    public static org.osid.id.Id getRoute() {
        return (makeQueryElementId("osid.mapping.map.Route"));
    }


    /**
     *  Gets the AncestorMapId element Id.
     *
     *  @return the AncestorMapId element Id
     */

    public static org.osid.id.Id getAncestorMapId() {
        return (makeQueryElementId("osid.mapping.map.AncestorMapId"));
    }


    /**
     *  Gets the AncestorMap element Id.
     *
     *  @return the AncestorMap element Id
     */

    public static org.osid.id.Id getAncestorMap() {
        return (makeQueryElementId("osid.mapping.map.AncestorMap"));
    }


    /**
     *  Gets the DescendantMapId element Id.
     *
     *  @return the DescendantMapId element Id
     */

    public static org.osid.id.Id getDescendantMapId() {
        return (makeQueryElementId("osid.mapping.map.DescendantMapId"));
    }


    /**
     *  Gets the DescendantMap element Id.
     *
     *  @return the DescendantMap element Id
     */

    public static org.osid.id.Id getDescendantMap() {
        return (makeQueryElementId("osid.mapping.map.DescendantMap"));
    }
}
