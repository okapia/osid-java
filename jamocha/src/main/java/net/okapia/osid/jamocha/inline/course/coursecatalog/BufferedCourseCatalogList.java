//
// BufferedCourseCatalogList.java
//
//     Implements a CourseCatalogList. This list wraps another list with
//     an internal buffer.
//
//
// Tom Coppeto
// OnTapSolutions
// 17 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.coursecatalog;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a CourseCatalogList. This list wraps another list in a
 *  buffer.
 */

public final class BufferedCourseCatalogList
    extends net.okapia.osid.jamocha.course.coursecatalog.spi.AbstractCourseCatalogList
    implements org.osid.course.CourseCatalogList,
               Runnable {

    private java.util.Queue<org.osid.course.CourseCatalog> stack = new java.util.concurrent.ConcurrentLinkedQueue<>();
    private org.osid.course.CourseCatalogList courseCatalogList;
    private int bufferSize = 10;
    private volatile boolean eol = false;

    private volatile boolean running = false;
    private volatile int consumerIsWaiting = 0;
    private volatile int providerIsWaiting = 0;

    private volatile org.osid.OsidException error;
    private CourseCatalogListCallback callback;


    /**
     *  Creates a new <code>BufferedCourseCatalogList</code>.
     *
     *  @param courseCatalogList a <code>CourseCatalogList</code>
     *  @throws org.osid.NullArgumentException <code>courseCatalogList</code>
     *          is <code>null</code>
     */

    public BufferedCourseCatalogList(org.osid.course.CourseCatalogList courseCatalogList) {
        nullarg(courseCatalogList, "courseCatalog list");
        this.courseCatalogList = courseCatalogList;
        return;
    }


    /**
     *  Creates a new <code>BufferedCourseCatalogList</code>.
     *
     *  @param courseCatalogList a <code>CourseCatalogList</code>
     *  @param callback callback interface for events
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogList</code> is <code>null</code>
     */

    public BufferedCourseCatalogList(org.osid.course.CourseCatalogList courseCatalogList, CourseCatalogListCallback callback) {
        nullarg(courseCatalogList, "coursecataloglist");
        nullarg(callback, "callback");

        this.courseCatalogList = courseCatalogList;
        this.callback   = callback;

        return;
    }


    /**
     *  Creates a new <code>BufferedCourseCatalogList</code>.
     *
     *  @param courseCatalogList a <code>CourseCatalogList</code>
     *  @param bufferSize number of xlabelsx to buffer
     *  @param callback callback interface for events
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogList</code> is <code>null</code>
     *  @throws org.osid.InvalidArgumentException buffer size must be
     *          greater than zero
     */

    public BufferedCourseCatalogList(org.osid.course.CourseCatalogList courseCatalogList, int bufferSize, 
                                CourseCatalogListCallback callback) {

        nullarg(courseCatalogList, "coursecatalog list");
        nullarg(callback, "callback");

        if (bufferSize < 1) {
            throw new org.osid.InvalidArgumentException("buffer size must be at least 1");
        }

        this.courseCatalogList = courseCatalogList;
        this.bufferSize   = bufferSize;
        this.callback     = callback;

        return;
    }


    /**
     *  Creates a new <code>BufferedCourseCatalogList</code> and runs it
     *  upon instantiation.
     *
     *  @param courseCatalogList a <code>CourseCatalogList</code>
     *  @param bufferSize number of coursecatalogs to buffer
     *  @param callback callback interface for events
     *  @param run
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogList</code> is <code>null</code>
     *  @throws org.osid.InvalidArgumentException buffer size must be greater
     *          than zero
     */

    public BufferedCourseCatalogList(org.osid.course.CourseCatalogList courseCatalogList, int bufferSize, 
                                CourseCatalogListCallback callback, boolean run) {

        this(courseCatalogList, bufferSize, callback);
        run();
        return;
    }


    /**
     *  Starts buffering coursecatalogs.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    @Override
    public void run() {
        if (this.running || this.eol) {
            throw new org.osid.IllegalStateException("already ran");
        }

        (new CourseCatalogFetcher()).start();
        return;
    }

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        synchronized (this.stack) {
            while (true) {
                if (this.stack.size() > 0) {
                    return (true);
                }

                if (hasError()) {
                    return (true);
                }

                if (this.eol) {
                    return (false);
                }

                waitForProvider();
            }
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The number 
     *  returned by this method may be less than or equal to the total number 
     *  of elements in this list. To determine if the end of the list has been 
     *  reached, the method <code> hasNext() </code> should be used. This 
     *  method conveys what is known about the number of remaining elements at 
     *  a point in time and can be used to determine a minimum size of the 
     *  remaining elements, if known. A valid return is zero even if <code> 
     *  hasNext() </code> is true. 
     *  <p>
     *  This method does not imply asynchronous usage. All OSID methods may 
     *  block. 
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        synchronized (this.stack) {
            return (this.stack.size());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        synchronized (this.stack) {
            while (n > 0) {

                if (hasError()) {
                    return;
                }

                /* blasted protected removeRange */
                if (this.stack.size() > 0) {
                    this.stack.remove();
                    --n;
                } else if (this.eol) {
                    return;
                } else {
                    waitForProvider();
                }
            }
            
            notifyProvider();
        }

        return;
    }


    /**
     *  Gets the next <code>CourseCatalog</code> in this list. 
     *
     *  @return the next <code>CourseCatalog</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>CourseCatalog</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getNextCourseCatalog()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            synchronized (this.stack) {
                org.osid.course.CourseCatalog ret = this.stack.remove();
                notifyProvider();
                return (ret);
            }
        } else {
            throw new org.osid.IllegalStateException("no more elements available in coursecatalog list");
        }
    }

        
    /**
     *  Gets the next set of <code>CourseCatalog</code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param  n the number of <code>CourseCatalog</code> elements requested which 
     *          must be less than or equal to <code> available() </code> 
     *  @return an array of <code>CourseCatalog</code> elements. <code> </code> The 
     *          length of the array is less than or equal to the number 
     *          specified. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog[] getNextCourseCatalogs(long n)
        throws org.osid.OperationFailedException {

        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        org.osid.course.CourseCatalog[] ret = new org.osid.course.CourseCatalog[(int) n];

        synchronized (this.stack) {
            for (int i = 0; i < n; i++) {
                ret[i] = this.stack.remove();
            }

            notifyProvider();
        }       

        return (ret);
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.stack = null;
        this.running = false;
        
        notifyProvider();

        if (this.callback != null) {
            this.callback.doneForList(this);
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked and no error has
     *  occurred.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational() {
        if (hasError()) {
            return (false);
        }

        if (this.stack == null) {
            return (false);
        }

        
        return (true);
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    /**
     *  Tests if the list is empty however it may still be running.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        if (this.stack == null) {
            return (true);
        } else if (this.stack.size() > 0) {
            return (false);
        }

        return (true);
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    private void error(org.osid.OsidException error) {
        this.error = error;
        notifyConsumer();

        if (this.callback != null) {
            this.callback.errorInList(this);
        }

        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    private boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  There are no more elements to be added to this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    private void eol() {
        this.eol = true;
        return;
    }

    
    /* wait/notify on the stack */

    public void waitForProvider() {
        this.consumerIsWaiting++;

        try {
            this.stack.wait();
        } catch (InterruptedException ie) { }

        this.consumerIsWaiting--;
        return;
    }


    private void notifyProvider() {
        if (this.providerIsWaiting > 0) {
            this.stack.notify();
        }

        return;
    }


    private void waitForConsumer() {
        this.providerIsWaiting++;

        try {
            this.stack.wait();
        } catch (InterruptedException ie) { }

        this.providerIsWaiting--;
        return;
    }


    private void notifyNewCourseCatalog() {
        notifyConsumer();
        if (this.callback != null) {
            this.callback.newCourseCatalogInList(this);
        }
    }
        

    private void notifyConsumer() {     
        if (this.consumerIsWaiting > 0) {
            this.stack.notify();
        }

        return;
    }

 
    class CourseCatalogFetcher
        extends Thread {
        
        public void run() {
            BufferedCourseCatalogList enc = BufferedCourseCatalogList.this;
            enc.running = true;

            try {
                while (enc.courseCatalogList.hasNext()) {
                    if (!enc.running) {
                        return;
                    }

                    synchronized (enc.stack) {
                        if (enc.stack.size() < enc.bufferSize) {
                            enc.stack.add(enc.courseCatalogList.getNextCourseCatalog());
                            notifyNewCourseCatalog();
                        } else {
                            waitForConsumer();
                        }
                    }
                }
            } catch (Exception e) {
                enc.running = false;
                enc.error(new org.osid.OperationFailedException(e));
                return;
            } finally {
                try {
                    enc.courseCatalogList.close();
                } catch (Exception e) {}
            }

            enc.running = false;            
            enc.eol();
            return;
        }
    }
}
