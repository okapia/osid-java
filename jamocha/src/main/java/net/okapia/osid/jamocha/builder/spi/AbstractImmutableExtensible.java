//
// AbstractImmutableExtensible
//
//     Defines an immutable wrapper for an Extensible Object.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an immutable wrapper for an Extensible Object.
 */

public abstract class AbstractImmutableExtensible
    extends AbstractImmutable
    implements org.osid.Extensible {

    private final org.osid.Extensible object;


    /**
     *  Constructs a new <code>AbstractImmutableExtensible</code>.
     *
     *  @param object
     *  @throws org.osid.NullArgumentException <code>object</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableExtensible(org.osid.Extensible object) {
        super(object);
        this.object = object;
        return;
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.object.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code> Type. </code> 
     *
     *  @param  recordType a type 
     *  @return <code> false </code>
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.object.hasRecordType(recordType));
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public boolean equals(Object obj) {
        return (this.object.equals(obj));
    }


    /**
     *  Returns a hash code value for this <code>OsidObject</code>
     *  based on the <code>Id</code>.
     *
     *  @return a hash code value for this object
     */

    @Override
    public int hashCode() {
        return (this.object.hashCode());
    }


    /**
     *  Returns a string representation of this OsidObject.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        return (this.object.toString());
    }
}
