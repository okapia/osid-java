//
// AbstractSupersedingEvent.java
//
//     Defines a SupersedingEvent builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.supersedingevent.spi;


/**
 *  Defines a <code>SupersedingEvent</code> builder.
 */

public abstract class AbstractSupersedingEventBuilder<T extends AbstractSupersedingEventBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.supersedingevent.SupersedingEventMiter supersedingEvent;


    /**
     *  Constructs a new <code>AbstractSupersedingEventBuilder</code>.
     *
     *  @param supersedingEvent the superseding event to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSupersedingEventBuilder(net.okapia.osid.jamocha.builder.calendaring.supersedingevent.SupersedingEventMiter supersedingEvent) {
        super(supersedingEvent);
        this.supersedingEvent = supersedingEvent;
        return;
    }


    /**
     *  Builds the superseding event.
     *
     *  @return the new superseding event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.SupersedingEvent build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.supersedingevent.SupersedingEventValidator(getValidations())).validate(this.supersedingEvent);
        return (new net.okapia.osid.jamocha.builder.calendaring.supersedingevent.ImmutableSupersedingEvent(this.supersedingEvent));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the superseding event miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.supersedingevent.SupersedingEventMiter getMiter() {
        return (this.supersedingEvent);
    }


    /**
     *  Sets the superseded event.
     *
     *  @param event a superseded event
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public T supersededEvent(org.osid.calendaring.Event event) {
        getMiter().setSupersededEvent(event);
        return (self());
    }


    /**
     *  Sets the superseding event.
     *
     *  @param event a superseding event
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public T supersedingEvent(org.osid.calendaring.Event event) {
        getMiter().setSupersedingEvent(event);
        return (self());
    }


    /**
     *  Sets the superseded date.
     *
     *  @param date a superseded date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T supersededDate(org.osid.calendaring.DateTime date) {
        getMiter().setSupersededDate(date);
        return (self());
    }


    /**
     *  Sets the superseded event position.
     *
     *  @param position a superseded event position
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>position</code> is negative
     */

    public T supersededEventPosition(long position) {
        getMiter().setSupersededEventPosition(position);
        return (self());
    }


    /**
     *  Adds a SupersedingEvent record.
     *
     *  @param record a superseding event record
     *  @param recordType the type of superseding event record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.SupersedingEventRecord record, org.osid.type.Type recordType) {
        getMiter().addSupersedingEventRecord(record, recordType);
        return (self());
    }
}       


