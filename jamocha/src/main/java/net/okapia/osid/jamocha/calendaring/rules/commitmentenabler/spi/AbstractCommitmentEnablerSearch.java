//
// AbstractCommitmentEnablerSearch.java
//
//     A template for making a CommitmentEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing commitment enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCommitmentEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.rules.CommitmentEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.CommitmentEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.rules.CommitmentEnablerSearchOrder commitmentEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of commitment enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  commitmentEnablerIds list of commitment enablers
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCommitmentEnablers(org.osid.id.IdList commitmentEnablerIds) {
        while (commitmentEnablerIds.hasNext()) {
            try {
                this.ids.add(commitmentEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCommitmentEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of commitment enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCommitmentEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  commitmentEnablerSearchOrder commitment enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>commitmentEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCommitmentEnablerResults(org.osid.calendaring.rules.CommitmentEnablerSearchOrder commitmentEnablerSearchOrder) {
	this.commitmentEnablerSearchOrder = commitmentEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.rules.CommitmentEnablerSearchOrder getCommitmentEnablerSearchOrder() {
	return (this.commitmentEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given commitment enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a commitment enabler implementing the requested record.
     *
     *  @param commitmentEnablerSearchRecordType a commitment enabler search record
     *         type
     *  @return the commitment enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.CommitmentEnablerSearchRecord getCommitmentEnablerSearchRecord(org.osid.type.Type commitmentEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.rules.records.CommitmentEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(commitmentEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commitment enabler search. 
     *
     *  @param commitmentEnablerSearchRecord commitment enabler search record
     *  @param commitmentEnablerSearchRecordType commitmentEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommitmentEnablerSearchRecord(org.osid.calendaring.rules.records.CommitmentEnablerSearchRecord commitmentEnablerSearchRecord, 
                                           org.osid.type.Type commitmentEnablerSearchRecordType) {

        addRecordType(commitmentEnablerSearchRecordType);
        this.records.add(commitmentEnablerSearchRecord);        
        return;
    }
}
