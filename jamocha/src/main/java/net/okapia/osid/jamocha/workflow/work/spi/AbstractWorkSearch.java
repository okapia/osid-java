//
// AbstractWorkSearch.java
//
//     A template for making a Work Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.work.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing work searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractWorkSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.workflow.WorkSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.workflow.records.WorkSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.workflow.WorkSearchOrder workSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of works. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  workIds list of works
     *  @throws org.osid.NullArgumentException
     *          <code>workIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongWorks(org.osid.id.IdList workIds) {
        while (workIds.hasNext()) {
            try {
                this.ids.add(workIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongWorks</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of work Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getWorkIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  workSearchOrder work search order 
     *  @throws org.osid.NullArgumentException
     *          <code>workSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>workSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderWorkResults(org.osid.workflow.WorkSearchOrder workSearchOrder) {
	this.workSearchOrder = workSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.workflow.WorkSearchOrder getWorkSearchOrder() {
	return (this.workSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given work search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a work implementing the requested record.
     *
     *  @param workSearchRecordType a work search record
     *         type
     *  @return the work search record
     *  @throws org.osid.NullArgumentException
     *          <code>workSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.WorkSearchRecord getWorkSearchRecord(org.osid.type.Type workSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.workflow.records.WorkSearchRecord record : this.records) {
            if (record.implementsRecordType(workSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this work search. 
     *
     *  @param workSearchRecord work search record
     *  @param workSearchRecordType work search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addWorkSearchRecord(org.osid.workflow.records.WorkSearchRecord workSearchRecord, 
                                           org.osid.type.Type workSearchRecordType) {

        addRecordType(workSearchRecordType);
        this.records.add(workSearchRecord);        
        return;
    }
}
