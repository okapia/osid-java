//
// AbstractEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inventory.shipment.EntrySearchResults {

    private org.osid.inventory.shipment.EntryList entries;
    private final org.osid.inventory.shipment.EntryQueryInspector inspector;
    private final java.util.Collection<org.osid.inventory.shipment.records.EntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractEntrySearchResults.
     *
     *  @param entries the result set
     *  @param entryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>entries</code>
     *          or <code>entryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractEntrySearchResults(org.osid.inventory.shipment.EntryList entries,
                                            org.osid.inventory.shipment.EntryQueryInspector entryQueryInspector) {
        nullarg(entries, "entries");
        nullarg(entryQueryInspector, "entry query inspectpr");

        this.entries = entries;
        this.inspector = entryQueryInspector;

        return;
    }


    /**
     *  Gets the entry list resulting from a search.
     *
     *  @return an entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inventory.shipment.EntryList getEntries() {
        if (this.entries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inventory.shipment.EntryList entries = this.entries;
        this.entries = null;
	return (entries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inventory.shipment.EntryQueryInspector getEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  entry search record <code> Type. </code> This method must
     *  be used to retrieve an entry implementing the requested
     *  record.
     *
     *  @param entrySearchRecordType an entry search 
     *         record type 
     *  @return the entry search
     *  @throws org.osid.NullArgumentException
     *          <code>entrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(entrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.EntrySearchResultsRecord getEntrySearchResultsRecord(org.osid.type.Type entrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inventory.shipment.records.EntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(entrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(entrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addEntryRecord(org.osid.inventory.shipment.records.EntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
