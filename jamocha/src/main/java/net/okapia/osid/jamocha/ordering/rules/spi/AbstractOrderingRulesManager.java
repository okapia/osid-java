//
// AbstractOrderingRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOrderingRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.ordering.rules.OrderingRulesManager,
               org.osid.ordering.rules.OrderingRulesProxyManager {

    private final Types priceEnablerRecordTypes            = new TypeRefSet();
    private final Types priceEnablerSearchRecordTypes      = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractOrderingRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOrderingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up price enablers is supported. 
     *
     *  @return <code> true </code> if price enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying price enablers is supported. 
     *
     *  @return <code> true </code> if price enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching price enablers is supported. 
     *
     *  @return <code> true </code> if price enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a price enabler administrative service is supported. 
     *
     *  @return <code> true </code> if price enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a price enabler notification service is supported. 
     *
     *  @return <code> true </code> if price enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a price enabler store lookup service is supported. 
     *
     *  @return <code> true </code> if a price enabler store lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerStore() {
        return (false);
    }


    /**
     *  Tests if a price enabler store service is supported. 
     *
     *  @return <code> true </code> if price enabler store assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerStoreAssignment() {
        return (false);
    }


    /**
     *  Tests if a price enabler store lookup service is supported. 
     *
     *  @return <code> true </code> if a price enabler store service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerSmartStore() {
        return (false);
    }


    /**
     *  Tests if a price enabler price rule lookup service is supported. 
     *
     *  @return <code> true </code> if a price enabler price rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a price enabler price rule application service is supported. 
     *
     *  @return <code> true </code> if price enabler price rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> PriceEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> PriceEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.priceEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> PriceEnabler </code> record type is 
     *  supported. 
     *
     *  @param  priceEnablerRecordType a <code> Type </code> indicating a 
     *          <code> PriceEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priceEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceEnablerRecordType(org.osid.type.Type priceEnablerRecordType) {
        return (this.priceEnablerRecordTypes.contains(priceEnablerRecordType));
    }


    /**
     *  Adds support for a price enabler record type.
     *
     *  @param priceEnablerRecordType a price enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>priceEnablerRecordType</code> is <code>null</code>
     */

    protected void addPriceEnablerRecordType(org.osid.type.Type priceEnablerRecordType) {
        this.priceEnablerRecordTypes.add(priceEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a price enabler record type.
     *
     *  @param priceEnablerRecordType a price enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>priceEnablerRecordType</code> is <code>null</code>
     */

    protected void removePriceEnablerRecordType(org.osid.type.Type priceEnablerRecordType) {
        this.priceEnablerRecordTypes.remove(priceEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> PriceEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> PriceEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.priceEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> PriceEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  priceEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> PriceEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          priceEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceEnablerSearchRecordType(org.osid.type.Type priceEnablerSearchRecordType) {
        return (this.priceEnablerSearchRecordTypes.contains(priceEnablerSearchRecordType));
    }


    /**
     *  Adds support for a price enabler search record type.
     *
     *  @param priceEnablerSearchRecordType a price enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>priceEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addPriceEnablerSearchRecordType(org.osid.type.Type priceEnablerSearchRecordType) {
        this.priceEnablerSearchRecordTypes.add(priceEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a price enabler search record type.
     *
     *  @param priceEnablerSearchRecordType a price enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>priceEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removePriceEnablerSearchRecordType(org.osid.type.Type priceEnablerSearchRecordType) {
        this.priceEnablerSearchRecordTypes.remove(priceEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  lookup service. 
     *
     *  @return a <code> PriceEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerLookupSession getPriceEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerLookupSession getPriceEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  lookup service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerLookupSession getPriceEnablerLookupSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  lookup service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerLookupSession getPriceEnablerLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  query service. 
     *
     *  @return a <code> PriceEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerQuerySession getPriceEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerQuerySession getPriceEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  query service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerQuerySession getPriceEnablerQuerySessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  query service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerQuerySession getPriceEnablerQuerySessionForStore(org.osid.id.Id storeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerQuerySessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  search service. 
     *
     *  @return a <code> PriceEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSearchSession getPriceEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSearchSession getPriceEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enablers 
     *  earch service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSearchSession getPriceEnablerSearchSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enablers 
     *  earch service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSearchSession getPriceEnablerSearchSessionForStore(org.osid.id.Id storeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerSearchSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  administration service. 
     *
     *  @return a <code> PriceEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerAdminSession getPriceEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerAdminSession getPriceEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerAdminSession getPriceEnablerAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerAdminSession getPriceEnablerAdminSessionForStore(org.osid.id.Id storeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerAdminSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  notification service. 
     *
     *  @param  priceEnablerReceiver the notification callback 
     *  @return a <code> PriceEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> priceEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerNotificationSession getPriceEnablerNotificationSession(org.osid.ordering.rules.PriceEnablerReceiver priceEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  notification service. 
     *
     *  @param  priceEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> priceEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerNotificationSession getPriceEnablerNotificationSession(org.osid.ordering.rules.PriceEnablerReceiver priceEnablerReceiver, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  notification service for the given store. 
     *
     *  @param  priceEnablerReceiver the notification callback 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no store found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> priceEnablerReceiver 
     *          </code> or <code> storeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerNotificationSession getPriceEnablerNotificationSessionForStore(org.osid.ordering.rules.PriceEnablerReceiver priceEnablerReceiver, 
                                                                                                              org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  notification service for the given store. 
     *
     *  @param  priceEnablerReceiver the notification callback 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no store found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> priceEnablerReceiver, 
     *          storeId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerNotificationSession getPriceEnablerNotificationSessionForStore(org.osid.ordering.rules.PriceEnablerReceiver priceEnablerReceiver, 
                                                                                                              org.osid.id.Id storeId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerNotificationSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup price enabler/store 
     *  mappings for price enablers. 
     *
     *  @return a <code> PriceEnablerStoreSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerStoreSession getPriceEnablerStoreSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerStoreSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup price enabler/store 
     *  mappings for price enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerStoreSession getPriceEnablerStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerStoreSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning price 
     *  enablers to stores for price. 
     *
     *  @return a <code> PriceEnablerStoreAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerStoreAssignmentSession getPriceEnablerStoreAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning price 
     *  enablers to stores for price. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerStoreAssignmentSession getPriceEnablerStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerStoreAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage price enabler smart 
     *  stores. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSmartStore() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSmartStoreSession getPriceEnablerSmartStoreSession(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerSmartStoreSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage price enabler smart 
     *  stores. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSmartStore() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSmartStoreSession getPriceEnablerSmartStoreSession(org.osid.id.Id storeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerSmartStoreSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price mapping lookup service for looking up the rules applied to the 
     *  store. 
     *
     *  @return a <code> PriceEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleLookupSession getPriceEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price mapping lookup service for looking up the rules applied to the 
     *  store. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerPriceRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleLookupSession getPriceEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price mapping lookup service for the given store for looking up rules 
     *  applied to an store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleLookupSession getPriceEnablerRuleLookupSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerRuleLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price mapping lookup service for the given store for looking up rules 
     *  applied to an store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleLookupSession getPriceEnablerRuleLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerRuleLookupSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price assignment service to apply enablers to stores. 
     *
     *  @return a <code> PriceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleApplicationSession getPriceEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price assignment service to apply enablers to stores. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerPricApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleApplicationSession getPriceEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price assignment service for the given store to apply enablers to 
     *  stores. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleApplicationSession getPriceEnablerRuleApplicationSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesManager.getPriceEnablerRuleApplicationSessionForStore not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price assignment service for the given store to apply enablers to 
     *  stores. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleApplicationSession getPriceEnablerRuleApplicationSessionForStore(org.osid.id.Id storeId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ordering.rules.OrderingRulesProxyManager.getPriceEnablerRuleApplicationSessionForStore not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.priceEnablerRecordTypes.clear();
        this.priceEnablerRecordTypes.clear();

        this.priceEnablerSearchRecordTypes.clear();
        this.priceEnablerSearchRecordTypes.clear();

        return;
    }
}
