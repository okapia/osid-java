//
// AbstractMapInstructionLookupSession
//
//    A simple framework for providing an Instruction lookup service
//    backed by a fixed collection of instructions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Instruction lookup service backed by a
 *  fixed collection of instructions. The instructions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Instructions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInstructionLookupSession
    extends net.okapia.osid.jamocha.rules.check.spi.AbstractInstructionLookupSession
    implements org.osid.rules.check.InstructionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.rules.check.Instruction> instructions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.rules.check.Instruction>());


    /**
     *  Makes an <code>Instruction</code> available in this session.
     *
     *  @param  instruction an instruction
     *  @throws org.osid.NullArgumentException <code>instruction<code>
     *          is <code>null</code>
     */

    protected void putInstruction(org.osid.rules.check.Instruction instruction) {
        this.instructions.put(instruction.getId(), instruction);
        return;
    }


    /**
     *  Makes an array of instructions available in this session.
     *
     *  @param  instructions an array of instructions
     *  @throws org.osid.NullArgumentException <code>instructions<code>
     *          is <code>null</code>
     */

    protected void putInstructions(org.osid.rules.check.Instruction[] instructions) {
        putInstructions(java.util.Arrays.asList(instructions));
        return;
    }


    /**
     *  Makes a collection of instructions available in this session.
     *
     *  @param  instructions a collection of instructions
     *  @throws org.osid.NullArgumentException <code>instructions<code>
     *          is <code>null</code>
     */

    protected void putInstructions(java.util.Collection<? extends org.osid.rules.check.Instruction> instructions) {
        for (org.osid.rules.check.Instruction instruction : instructions) {
            this.instructions.put(instruction.getId(), instruction);
        }

        return;
    }


    /**
     *  Removes an Instruction from this session.
     *
     *  @param  instructionId the <code>Id</code> of the instruction
     *  @throws org.osid.NullArgumentException <code>instructionId<code> is
     *          <code>null</code>
     */

    protected void removeInstruction(org.osid.id.Id instructionId) {
        this.instructions.remove(instructionId);
        return;
    }


    /**
     *  Gets the <code>Instruction</code> specified by its <code>Id</code>.
     *
     *  @param  instructionId <code>Id</code> of the <code>Instruction</code>
     *  @return the instruction
     *  @throws org.osid.NotFoundException <code>instructionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>instructionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Instruction getInstruction(org.osid.id.Id instructionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.check.Instruction instruction = this.instructions.get(instructionId);
        if (instruction == null) {
            throw new org.osid.NotFoundException("instruction not found: " + instructionId);
        }

        return (instruction);
    }


    /**
     *  Gets all <code>Instructions</code>. In plenary mode, the returned
     *  list contains all known instructions or an error
     *  results. Otherwise, the returned list may contain only those
     *  instructions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Instructions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.instruction.ArrayInstructionList(this.instructions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.instructions.clear();
        super.close();
        return;
    }
}
