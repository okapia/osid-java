//
// AbstractReply.java
//
//     Defines a Reply.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Reply</code>.
 */

public abstract class AbstractReply
    extends net.okapia.osid.jamocha.spi.AbstractContainableOsidObject
    implements org.osid.forum.Reply {

    private org.osid.forum.Post post;
    private final java.util.Collection<org.osid.forum.Reply> replies = new java.util.LinkedHashSet<>();
    private org.osid.calendaring.DateTime timestamp;
    private org.osid.resource.Resource poster;
    private org.osid.authentication.Agent postingAgent;
    private org.osid.locale.DisplayText subjectLine;
    private org.osid.locale.DisplayText text;

    private final java.util.Collection<org.osid.forum.records.ReplyRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the original top level post for this 
     *  reply. 
     *
     *  @return the post <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostId() {
        return (this.post.getId());
    }


    /**
     *  Gets the original top level post. 
     *
     *  @return the post 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.forum.Post getPost()
        throws org.osid.OperationFailedException {

        return (this.post);
    }


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @throws org.osid.NullArgumentException
     *          <code>post</code> is <code>null</code>
     */

    protected void setPost(org.osid.forum.Post post) {
        nullarg(post, "post");
        this.post = post;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the replies to this rpely. 
     *
     *  @return the reply <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getReplyIds() {
        try {
            org.osid.forum.ReplyList replies = getReplies();
            return (new net.okapia.osid.jamocha.adapter.converter.forum.reply.ReplyToIdList(replies));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the replies to this reply. 
     *
     *  @return the replies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getReplies()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.forum.reply.ArrayReplyList(this.replies));
    }


    /**
     *  Adds a reply.
     *
     *  @param reply a reply
     *  @throws org.osid.NullArgumentException
     *          <code>reply</code> is <code>null</code>
     */

    protected void addReply(org.osid.forum.Reply reply) {
        nullarg(reply, "reply");
        this.replies.add(reply);
        return;
    }


    /**
     *  Sets all the replies.
     *
     *  @param replies a collection of replies
     *  @throws org.osid.NullArgumentException
     *          <code>replies</code> is <code>null</code>
     */

    protected void setReplies(java.util.Collection<org.osid.forum.Reply> replies) {
        nullarg(replies, "replies");

        this.replies.clear();
        this.replies.addAll(replies);

        return;
    }


    /**
     *  Gets the time of this entry. 
     *
     *  @return the time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.timestamp);
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException
     *          <code>timestamp</code> is <code>null</code>
     */

    protected void setTimestamp(org.osid.calendaring.DateTime timestamp) {
        nullarg(timestamp, "timestamp");
        this.timestamp = timestamp;
        return;
    }


    /**
     *  Gets the poster resource <code> Id </code> of this entry. 
     *
     *  @return the poster resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPosterId() {
        return (this.poster.getId());
    }


    /**
     *  Gets the posting of this entry. 
     *
     *  @return the poster resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getPoster()
        throws org.osid.OperationFailedException {

        return (this.poster);
    }


    /**
     *  Sets the poster.
     *
     *  @param poster a poster
     *  @throws org.osid.NullArgumentException
     *          <code>poster</code> is <code>null</code>
     */

    protected void setPoster(org.osid.resource.Resource poster) {
        nullarg(poster, "poster");
        this.poster = poster;
        return;
    }


    /**
     *  Gets the posting <code> Id </code> of this entry. 
     *
     *  @return the posting agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostingAgentId() {
        return (this.postingAgent.getId());
    }


    /**
     *  Gets the posting of this entry. 
     *
     *  @return the posting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getPostingAgent()
        throws org.osid.OperationFailedException {

        return (this.postingAgent);
    }


    /**
     *  Sets the posting agent.
     *
     *  @param agent a posting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setPostingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "posting agent");
        this.postingAgent = agent;
        return;
    }


    /**
     *  Gets the subject line of this entry. 
     *
     *  @return the subject 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSubjectLine() {
        return (this.subjectLine);
    }


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    protected void setSubjectLine(org.osid.locale.DisplayText subjectLine) {
        nullarg(subjectLine, "subject line");
        this.subjectLine = subjectLine;
        return;
    }


    /**
     *  Gets the text of the entry. 
     *
     *  @return the entry text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.text);
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException
     *          <code>text</code> is <code>null</code>
     */

    protected void setText(org.osid.locale.DisplayText text) {
        nullarg(text, "text");
        this.text = text;
        return;
    }


    /**
     *  Tests if this reply supports the given record
     *  <code>Type</code>.
     *
     *  @param  replyRecordType a reply record type 
     *  @return <code>true</code> if the replyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type replyRecordType) {
        for (org.osid.forum.records.ReplyRecord record : this.records) {
            if (record.implementsRecordType(replyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Reply</code> record <code>Type</code>.
     *
     *  @param  replyRecordType the reply record type 
     *  @return the reply record 
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(replyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplyRecord getReplyRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ReplyRecord record : this.records) {
            if (record.implementsRecordType(replyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this reply. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param replyRecord the reply record
     *  @param replyRecordType reply record type
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecord</code> or
     *          <code>replyRecordTypereply</code> is
     *          <code>null</code>
     */
            
    protected void addReplyRecord(org.osid.forum.records.ReplyRecord replyRecord, 
                                  org.osid.type.Type replyRecordType) {
        
        nullarg(replyRecord, "reply record");
        addRecordType(replyRecordType);
        this.records.add(replyRecord);
        
        return;
    }
}
