//
// AbstractInventoryShipmentProxyManager.java
//
//     An adapter for a InventoryShipmentProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InventoryShipmentProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInventoryShipmentProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.inventory.shipment.InventoryShipmentProxyManager>
    implements org.osid.inventory.shipment.InventoryShipmentProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterInventoryShipmentProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInventoryShipmentProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInventoryShipmentProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInventoryShipmentProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any warehouse federation is exposed. Federation is exposed 
     *  when a specific warehouse may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up shipments is supported. 
     *
     *  @return <code> true </code> if shipment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentLookup() {
        return (getAdapteeManager().supportsShipmentLookup());
    }


    /**
     *  Tests if querying shipments is supported. 
     *
     *  @return <code> true </code> if shipment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentQuery() {
        return (getAdapteeManager().supportsShipmentQuery());
    }


    /**
     *  Tests if searching shipments is supported. 
     *
     *  @return <code> true </code> if shipment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentSearch() {
        return (getAdapteeManager().supportsShipmentSearch());
    }


    /**
     *  Tests if shipment <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if shipment administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentAdmin() {
        return (getAdapteeManager().supportsShipmentAdmin());
    }


    /**
     *  Tests if a shipment <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if shipment notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentNotification() {
        return (getAdapteeManager().supportsShipmentNotification());
    }


    /**
     *  Tests if a warehouseing service is supported. 
     *
     *  @return <code> true </code> if warehouseing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentWarehouse() {
        return (getAdapteeManager().supportsShipmentWarehouse());
    }


    /**
     *  Tests if a warehouseing service is supported. A warehouseing service 
     *  maps shipments to catalogs. 
     *
     *  @return <code> true </code> if warehouseing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentWarehouseAssignment() {
        return (getAdapteeManager().supportsShipmentWarehouseAssignment());
    }


    /**
     *  Tests if a shipment smart warehouse session is available. 
     *
     *  @return <code> true </code> if a shipment smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentSmartWarehouse() {
        return (getAdapteeManager().supportsShipmentSmartWarehouse());
    }


    /**
     *  Tests for the availability of a inventory shipment batch service. 
     *
     *  @return <code> true </code> if an inventory shipment batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryShipmentBatchBatch() {
        return (getAdapteeManager().supportsInventoryShipmentBatchBatch());
    }


    /**
     *  Gets the supported <code> Shipment </code> record types. 
     *
     *  @return a list containing the supported <code> Shipment </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getShipmentRecordTypes() {
        return (getAdapteeManager().getShipmentRecordTypes());
    }


    /**
     *  Tests if the given <code> Shipment </code> record type is supported. 
     *
     *  @param  shipmentRecordType a <code> Type </code> indicating a <code> 
     *          Shipment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> shipmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsShipmentRecordType(org.osid.type.Type shipmentRecordType) {
        return (getAdapteeManager().supportsShipmentRecordType(shipmentRecordType));
    }


    /**
     *  Gets the supported <code> Shipment </code> search record types. 
     *
     *  @return a list containing the supported <code> Shipment </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getShipmentSearchRecordTypes() {
        return (getAdapteeManager().getShipmentSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Shipment </code> search record type is 
     *  supported. 
     *
     *  @param  shipmentSearchRecordType a <code> Type </code> indicating a 
     *          <code> Shipment </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> shipmentSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsShipmentSearchRecordType(org.osid.type.Type shipmentSearchRecordType) {
        return (getAdapteeManager().supportsShipmentSearchRecordType(shipmentSearchRecordType));
    }


    /**
     *  Gets the supported <code> Entry </code> record types. 
     *
     *  @return a list containing the supported <code> Entry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (getAdapteeManager().getEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> record type is supported. 
     *
     *  @param  entryRecordType a <code> Type </code> indicating an <code> 
     *          Entry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (getAdapteeManager().supportsEntryRecordType(entryRecordType));
    }


    /**
     *  Gets the supported <code> Entry </code> search record types. 
     *
     *  @return a list containing the supported <code> Entry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (getAdapteeManager().getEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Entry </code> search record type is 
     *  supported. 
     *
     *  @param  entrySearchRecordType a <code> Type </code> indicating an 
     *          <code> Entry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (getAdapteeManager().supportsEntrySearchRecordType(entrySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentLookupSession getShipmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  lookup service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentLookupSession getShipmentLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentLookupSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentQuerySession getShipmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentQuerySession getShipmentQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentQuerySessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSearchSession getShipmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  search service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSearchSession getShipmentSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentSearchSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentAdminSession getShipmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsShipmentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentAdminSession getShipmentAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentAdminSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  notification service. 
     *
     *  @param  shipmentReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> shipmentReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentNotificationSession getShipmentNotificationSession(org.osid.inventory.shipment.ShipmentReceiver shipmentReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentNotificationSession(shipmentReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment 
     *  notification service for the given warehouse. 
     *
     *  @param  shipmentReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> shipmentReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentNotificationSession getShipmentNotificationSessionForWarehouse(org.osid.inventory.shipment.ShipmentReceiver shipmentReceiver, 
                                                                                                              org.osid.id.Id warehouseId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentNotificationSessionForWarehouse(shipmentReceiver, warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup shipment/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentWarehouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentWarehouseSession getShipmentWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentWarehouseSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  shipments to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ShipmentWarehouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentWarehouseAssignmentSession getShipmentWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentWarehouseAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the shipment smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ShipmentSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentSmartWarehouseSession getShipmentSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentSmartWarehouseSession(warehouseId, proxy));
    }


    /**
     *  Gets the <code> InventoryShipmentBatchProxyManager. </code> 
     *
     *  @return an <code> InventoryShipmentBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryShipmentBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.batch.InventoryShipmentBatchProxyManager getInventoryShipmentBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryShipmentBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
