//
// ObjectiveMiter.java
//
//     Defines an Objective miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.objective;


/**
 *  Defines an <code>Objective</code> miter for use with the builders.
 */

public interface ObjectiveMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.learning.Objective {


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    public void setAssessment(org.osid.assessment.Assessment assessment);


    /**
     *  Sets the knowledge category.
     *
     *  @param category a knowledge category
     *  @throws org.osid.NullArgumentException <code>category</code>
     *          is <code>null</code>
     */

    public void setKnowledgeCategory(org.osid.grading.Grade category);


    /**
     *  Sets the cognitive process.
     *
     *  @param process a cognitive process
     *  @throws org.osid.NullArgumentException <code>process</code> is
     *          <code>null</code>
     */

    public void setCognitiveProcess(org.osid.grading.Grade process);


    /**
     *  Adds an Objective record.
     *
     *  @param record an objective record
     *  @param recordType the type of objective record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addObjectiveRecord(org.osid.learning.records.ObjectiveRecord record, org.osid.type.Type recordType);
}       


