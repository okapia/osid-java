//
// AbstractRaceProcessorEnablerQuery.java
//
//     A template for making a RaceProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for race processor enablers.
 */

public abstract class AbstractRaceProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.voting.rules.RaceProcessorEnablerQuery {

    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the race processor. 
     *
     *  @param  raceProcessorId the race processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRaceProcessorId(org.osid.id.Id raceProcessorId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the race processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRaceProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RaceProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a race processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRaceProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the race processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRaceProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuery getRuledRaceProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRaceProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any race processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any race 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          race processors 
     */

    @OSID @Override
    public void matchAnyRuledRaceProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the race processor query terms. 
     */

    @OSID @Override
    public void clearRuledRaceProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given race processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race processor enabler implementing the requested record.
     *
     *  @param raceProcessorEnablerRecordType a race processor enabler record type
     *  @return the race processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord getRaceProcessorEnablerQueryRecord(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race processor enabler query. 
     *
     *  @param raceProcessorEnablerQueryRecord race processor enabler query record
     *  @param raceProcessorEnablerRecordType raceProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceProcessorEnablerQueryRecord(org.osid.voting.rules.records.RaceProcessorEnablerQueryRecord raceProcessorEnablerQueryRecord, 
                                          org.osid.type.Type raceProcessorEnablerRecordType) {

        addRecordType(raceProcessorEnablerRecordType);
        nullarg(raceProcessorEnablerQueryRecord, "race processor enabler query record");
        this.records.add(raceProcessorEnablerQueryRecord);        
        return;
    }
}
