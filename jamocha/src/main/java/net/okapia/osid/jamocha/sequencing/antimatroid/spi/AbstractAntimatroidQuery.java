//
// AbstractAntimatroidQuery.java
//
//     A template for making an Antimatroid Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.antimatroid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for antimatroids.
 */

public abstract class AbstractAntimatroidQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.sequencing.AntimatroidQuery {

    private final java.util.Collection<org.osid.sequencing.records.AntimatroidQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the chain <code> Id </code> for this query. 
     *
     *  @param  chainId the chain <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> chainId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchChainId(org.osid.id.Id chainId, boolean match) {
        return;
    }


    /**
     *  Clears the chain <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearChainIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ChainQuery </code> is available. 
     *
     *  @return <code> true </code> if an input query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputQuery() {
        return (false);
    }


    /**
     *  Gets the query for a chain. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the chain query 
     *  @throws org.osid.UnimplementedException <code> supportsChainQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQuery getChainQuery() {
        throw new org.osid.UnimplementedException("supportsChainQuery() is false");
    }


    /**
     *  Matches action groups with any chain. 
     *
     *  @param  match <code> true </code> to match antimatroids with any 
     *          chain, <code> false </code> to match antimatroids with no 
     *          chains 
     */

    @OSID @Override
    public void matchAnyChain(boolean match) {
        return;
    }


    /**
     *  Clears the chain query terms. 
     */

    @OSID @Override
    public void clearChainTerms() {
        return;
    }


    /**
     *  Sets the antimatroid <code> Id </code> for this query to match 
     *  antimatroids that have the specified antimatroid as an ancestor. 
     *
     *  @param  antimatroidId an antimatroid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAntimatroidId(org.osid.id.Id antimatroidId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the ancestor antimatroid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorAntimatroidIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AntimatroidQuery </code> is available. 
     *
     *  @return <code> true </code> if an antimatroid query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAntimatroidQuery() {
        return (false);
    }


    /**
     *  Gets the query for an antimatroid. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the antimatroid query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAntimatroidQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuery getAncestorAntimatroidQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAntimatroidQuery() is false");
    }


    /**
     *  Matches antimatroids with any ancestor. 
     *
     *  @param  match <code> true </code> to match antimatroids with any 
     *          ancestor, <code> false </code> to match root antimatroids 
     */

    @OSID @Override
    public void matchAnyAncestorAntimatroid(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor antimatroid query terms. 
     */

    @OSID @Override
    public void clearAncestorAntimatroidTerms() {
        return;
    }


    /**
     *  Sets the antimatroid <code> Id </code> for this query to match 
     *  antimatroids that have the specified antimatroid as a descendant. 
     *
     *  @param  antimatroidId an antimatroid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAntimatroidId(org.osid.id.Id antimatroidId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the descendant antimatroid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantAntimatroidIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AntimatroidQuery </code> is available. 
     *
     *  @return <code> true </code> if an antimatroid query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAntimatroidQuery() {
        return (false);
    }


    /**
     *  Gets the query for an antimatroid/ Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the antimatroid query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAntimatroidQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuery getDescendantAntimatroidQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAntimatroidQuery() is false");
    }


    /**
     *  Matches antimatroids with any descendant. 
     *
     *  @param  match <code> true </code> to match antimatroids with any 
     *          descendant, <code> false </code> to match leaf antimatroids 
     */

    @OSID @Override
    public void matchAnyDescendantAntimatroid(boolean match) {
        return;
    }


    /**
     *  Clears the descendant antimatroid query terms. 
     */

    @OSID @Override
    public void clearDescendantAntimatroidTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given antimatroid query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an antimatroid implementing the requested record.
     *
     *  @param antimatroidRecordType an antimatroid record type
     *  @return the antimatroid query record
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(antimatroidRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidQueryRecord getAntimatroidQueryRecord(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.AntimatroidQueryRecord record : this.records) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(antimatroidRecordType + " is not supported");
    }


    /**
     *  Adds a record to this antimatroid query. 
     *
     *  @param antimatroidQueryRecord antimatroid query record
     *  @param antimatroidRecordType antimatroid record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAntimatroidQueryRecord(org.osid.sequencing.records.AntimatroidQueryRecord antimatroidQueryRecord, 
                                          org.osid.type.Type antimatroidRecordType) {

        addRecordType(antimatroidRecordType);
        nullarg(antimatroidQueryRecord, "antimatroid query record");
        this.records.add(antimatroidQueryRecord);        
        return;
    }
}
