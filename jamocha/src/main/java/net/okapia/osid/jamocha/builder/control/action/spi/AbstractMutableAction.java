//
// AbstractMutableAction.java
//
//     Defines a mutable Action.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.action.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Action</code>.
 */

public abstract class AbstractMutableAction
    extends net.okapia.osid.jamocha.control.action.spi.AbstractAction
    implements org.osid.control.Action,
               net.okapia.osid.jamocha.builder.control.action.ActionMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this action. 
     *
     *  @param record action record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addActionRecord(org.osid.control.records.ActionRecord record, org.osid.type.Type recordType) {
        super.addActionRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this action. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if disabled
     */
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (isDisabled()) {
            setDisabled(false);
        }

        return;
    }


    /**
     *  Disables this action. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code>true</code> if disabled, <code>false<code>
     *         if disabled
     */
    
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);

        if (isEnabled()) {
            setEnabled(false);
        }

        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this action.
     *
     *  @param displayName the name for this action
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this action.
     *
     *  @param description the description of this action
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException
     *          <code>rule</code> is <code>null</code>
     */
    
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the action group.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    @Override
    public void setActionGroup(org.osid.control.ActionGroup actionGroup) {
        super.setActionGroup(actionGroup);
        return;
    }


    /**
     *  Sets the delay.
     *
     *  @param delay a delay
     *  @throws org.osid.NullArgumentException <code>delay</code> is
     *          <code>null</code>
     */

    @Override
    public void setDelay(org.osid.calendaring.Duration delay) {
        super.setDelay(delay);
        return;
    }


    /**
     *  Sets the next action group.
     *
     *  @param actionGroup a next action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    @Override
    public void setNextActionGroup(org.osid.control.ActionGroup actionGroup) {
        super.setNextActionGroup(actionGroup);
        return;
    }


    /**
     *  Sets the scene.
     *
     *  @param scene a scene
     *  @throws org.osid.NullArgumentException <code>scene</code> is
     *          <code>null</code>
     */

    @Override
    public void setScene(org.osid.control.Scene scene) {
        super.setScene(scene);
        return;
    }


    /**
     *  Sets the setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException <code>setting</code> is
     *          <code>null</code>
     */

    @Override
    public void setSetting(org.osid.control.Setting setting) {
        super.setSetting(setting);
        return;
    }


    /**
     *  Sets the matching controller.
     *
     *  @param controller a matching controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    @Override
    public void setMatchingController(org.osid.control.Controller controller) {
        super.setMatchingController(controller);
        return;
    }


    /**
     *  Sets the matching amount factor.
     *
     *  @param factor a matching amount factor
     *  @throws org.osid.NullArgumentException <code>factor</code> is
     *          <code>null</code>
     */

    @Override
    public void setMatchingAmountFactor(java.math.BigDecimal factor) {
        super.setMatchingAmountFactor(factor);
        return;
    }


    /**
     *  Sets the matching rate factor.
     *
     *  @param factor a matching rate factor
     *  @throws org.osid.NullArgumentException <code>factor</code> is
     *          <code>null</code>
     */

    @Override
    public void setMatchingRateFactor(java.math.BigDecimal factor) {
        super.setMatchingRateFactor(factor);
        return;
    }
}

