//
// AbstractAdapterDeviceEnablerLookupSession.java
//
//    A DeviceEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A DeviceEnabler lookup session adapter.
 */

public abstract class AbstractAdapterDeviceEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.rules.DeviceEnablerLookupSession {

    private final org.osid.control.rules.DeviceEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDeviceEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDeviceEnablerLookupSession(org.osid.control.rules.DeviceEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code DeviceEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDeviceEnablers() {
        return (this.session.canLookupDeviceEnablers());
    }


    /**
     *  A complete view of the {@code DeviceEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDeviceEnablerView() {
        this.session.useComparativeDeviceEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code DeviceEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDeviceEnablerView() {
        this.session.usePlenaryDeviceEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include device enablers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    

    /**
     *  Only active device enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveDeviceEnablerView() {
        this.session.useActiveDeviceEnablerView();
        return;
    }


    /**
     *  Active and inactive device enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDeviceEnablerView() {
        this.session.useAnyStatusDeviceEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code DeviceEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code DeviceEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code DeviceEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @param deviceEnablerId {@code Id} of the {@code DeviceEnabler}
     *  @return the device enabler
     *  @throws org.osid.NotFoundException {@code deviceEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code deviceEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnabler getDeviceEnabler(org.osid.id.Id deviceEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeviceEnabler(deviceEnablerId));
    }


    /**
     *  Gets a {@code DeviceEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  deviceEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code DeviceEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @param  deviceEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code DeviceEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code deviceEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersByIds(org.osid.id.IdList deviceEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeviceEnablersByIds(deviceEnablerIds));
    }


    /**
     *  Gets a {@code DeviceEnablerList} corresponding to the given
     *  device enabler genus {@code Type} which does not include
     *  device enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  device enablers or an error results. Otherwise, the returned list
     *  may contain only those device enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @param  deviceEnablerGenusType a deviceEnabler genus type 
     *  @return the returned {@code DeviceEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code deviceEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersByGenusType(org.osid.type.Type deviceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeviceEnablersByGenusType(deviceEnablerGenusType));
    }


    /**
     *  Gets a {@code DeviceEnablerList} corresponding to the given
     *  device enabler genus {@code Type} and include any additional
     *  device enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  device enablers or an error results. Otherwise, the returned list
     *  may contain only those device enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @param  deviceEnablerGenusType a deviceEnabler genus type 
     *  @return the returned {@code DeviceEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code deviceEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersByParentGenusType(org.osid.type.Type deviceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeviceEnablersByParentGenusType(deviceEnablerGenusType));
    }


    /**
     *  Gets a {@code DeviceEnablerList} containing the given
     *  device enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  device enablers or an error results. Otherwise, the returned list
     *  may contain only those device enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @param  deviceEnablerRecordType a deviceEnabler record type 
     *  @return the returned {@code DeviceEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code deviceEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersByRecordType(org.osid.type.Type deviceEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeviceEnablersByRecordType(deviceEnablerRecordType));
    }


    /**
     *  Gets a {@code DeviceEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  device enablers or an error results. Otherwise, the returned list
     *  may contain only those device enablers that are accessible
     *  through this session.
     *  
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code DeviceEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeviceEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code DeviceEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  device enablers or an error results. Otherwise, the returned list
     *  may contain only those device enablers that are accessible
     *  through this session.
     *
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code DeviceEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getDeviceEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code DeviceEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  device enablers or an error results. Otherwise, the returned list
     *  may contain only those device enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, device enablers are returned that are currently
     *  active. In any status mode, active and inactive device enablers
     *  are returned.
     *
     *  @return a list of {@code DeviceEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDeviceEnablers());
    }
}
