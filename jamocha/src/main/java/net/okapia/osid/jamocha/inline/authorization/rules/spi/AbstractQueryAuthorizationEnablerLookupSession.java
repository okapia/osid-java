//
// AbstractQueryAuthorizationEnablerLookupSession.java
//
//    An inline adapter that maps an AuthorizationEnablerLookupSession to
//    an AuthorizationEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authorization.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AuthorizationEnablerLookupSession to
 *  an AuthorizationEnablerQuerySession.
 */

public abstract class AbstractQueryAuthorizationEnablerLookupSession
    extends net.okapia.osid.jamocha.authorization.rules.spi.AbstractAuthorizationEnablerLookupSession
    implements org.osid.authorization.rules.AuthorizationEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.authorization.rules.AuthorizationEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAuthorizationEnablerLookupSession.
     *
     *  @param querySession the underlying authorization enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAuthorizationEnablerLookupSession(org.osid.authorization.rules.AuthorizationEnablerQuerySession querySession) {
        nullarg(querySession, "authorization enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Vault</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform <code>AuthorizationEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuthorizationEnablers() {
        return (this.session.canSearchAuthorizationEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include authorization enablers in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    

    /**
     *  Only active authorization enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuthorizationEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive authorization enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuthorizationEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>AuthorizationEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuthorizationEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuthorizationEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, authorization enablers are returned that are currently
     *  active. In any status mode, active and inactive authorization enablers
     *  are returned.
     *
     *  @param  authorizationEnablerId <code>Id</code> of the
     *          <code>AuthorizationEnabler</code>
     *  @return the authorization enabler
     *  @throws org.osid.NotFoundException <code>authorizationEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>authorizationEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnabler getAuthorizationEnabler(org.osid.id.Id authorizationEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.rules.AuthorizationEnablerQuery query = getQuery();
        query.matchId(authorizationEnablerId, true);
        org.osid.authorization.rules.AuthorizationEnablerList authorizationEnablers = this.session.getAuthorizationEnablersByQuery(query);
        if (authorizationEnablers.hasNext()) {
            return (authorizationEnablers.getNextAuthorizationEnabler());
        } 
        
        throw new org.osid.NotFoundException(authorizationEnablerId + " not found");
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  authorizationEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuthorizationEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, authorization enablers are returned that are currently
     *  active. In any status mode, active and inactive authorization enablers
     *  are returned.
     *
     *  @param  authorizationEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByIds(org.osid.id.IdList authorizationEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.rules.AuthorizationEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = authorizationEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAuthorizationEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> corresponding to the given
     *  authorization enabler genus <code>Type</code> which does not include
     *  authorization enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the returned list
     *  may contain only those authorization enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, authorization enablers are returned that are currently
     *  active. In any status mode, active and inactive authorization enablers
     *  are returned.
     *
     *  @param  authorizationEnablerGenusType an authorizationEnabler genus type 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByGenusType(org.osid.type.Type authorizationEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.rules.AuthorizationEnablerQuery query = getQuery();
        query.matchGenusType(authorizationEnablerGenusType, true);
        return (this.session.getAuthorizationEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> corresponding to the given
     *  authorization enabler genus <code>Type</code> and include any additional
     *  authorization enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the returned list
     *  may contain only those authorization enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, authorization enablers are returned that are currently
     *  active. In any status mode, active and inactive authorization enablers
     *  are returned.
     *
     *  @param  authorizationEnablerGenusType an authorizationEnabler genus type 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByParentGenusType(org.osid.type.Type authorizationEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.rules.AuthorizationEnablerQuery query = getQuery();
        query.matchParentGenusType(authorizationEnablerGenusType, true);
        return (this.session.getAuthorizationEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> containing the given
     *  authorization enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the returned list
     *  may contain only those authorization enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, authorization enablers are returned that are currently
     *  active. In any status mode, active and inactive authorization enablers
     *  are returned.
     *
     *  @param  authorizationEnablerRecordType an authorizationEnabler record type 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByRecordType(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.rules.AuthorizationEnablerQuery query = getQuery();
        query.matchRecordType(authorizationEnablerRecordType, true);
        return (this.session.getAuthorizationEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the returned list
     *  may contain only those authorization enablers that are accessible
     *  through this session.
     *  
     *  In active mode, authorization enablers are returned that are currently
     *  active. In any status mode, active and inactive authorization enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AuthorizationEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.rules.AuthorizationEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getAuthorizationEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>AuthorizationEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the returned list
     *  may contain only those authorization enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, authorization enablers are returned that are currently
     *  active. In any status mode, active and inactive authorization enablers
     *  are returned.
     *
     *  @return a list of <code>AuthorizationEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.rules.AuthorizationEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAuthorizationEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authorization.rules.AuthorizationEnablerQuery getQuery() {
        org.osid.authorization.rules.AuthorizationEnablerQuery query = this.session.getAuthorizationEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
