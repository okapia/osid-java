//
// AbstractGradeEntrySearch.java
//
//     A template for making a GradeEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing grade entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractGradeEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.grading.GradeEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.grading.GradeEntrySearchOrder gradeEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of grade entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  gradeEntryIds list of grade entries
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongGradeEntries(org.osid.id.IdList gradeEntryIds) {
        while (gradeEntryIds.hasNext()) {
            try {
                this.ids.add(gradeEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongGradeEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of grade entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getGradeEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  gradeEntrySearchOrder grade entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>gradeEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderGradeEntryResults(org.osid.grading.GradeEntrySearchOrder gradeEntrySearchOrder) {
	this.gradeEntrySearchOrder = gradeEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.grading.GradeEntrySearchOrder getGradeEntrySearchOrder() {
	return (this.gradeEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given grade entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a grade entry implementing the requested record.
     *
     *  @param gradeEntrySearchRecordType a grade entry search record
     *         type
     *  @return the grade entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntrySearchRecord getGradeEntrySearchRecord(org.osid.type.Type gradeEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.grading.records.GradeEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(gradeEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade entry search. 
     *
     *  @param gradeEntrySearchRecord grade entry search record
     *  @param gradeEntrySearchRecordType gradeEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradeEntrySearchRecord(org.osid.grading.records.GradeEntrySearchRecord gradeEntrySearchRecord, 
                                           org.osid.type.Type gradeEntrySearchRecordType) {

        addRecordType(gradeEntrySearchRecordType);
        this.records.add(gradeEntrySearchRecord);        
        return;
    }
}
