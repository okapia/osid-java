//
// AbstractAvailability.java
//
//     Defines an Availability builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.availability.spi;


/**
 *  Defines an <code>Availability</code> builder.
 */

public abstract class AbstractAvailabilityBuilder<T extends AbstractAvailabilityBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resourcing.availability.AvailabilityMiter availability;


    /**
     *  Constructs a new <code>AbstractAvailabilityBuilder</code>.
     *
     *  @param availability the availability to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAvailabilityBuilder(net.okapia.osid.jamocha.builder.resourcing.availability.AvailabilityMiter availability) {
        super(availability);
        this.availability = availability;
        return;
    }


    /**
     *  Builds the availability.
     *
     *  @return the new availability
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resourcing.Availability build() {
        (new net.okapia.osid.jamocha.builder.validator.resourcing.availability.AvailabilityValidator(getValidations())).validate(this.availability);
        return (new net.okapia.osid.jamocha.builder.resourcing.availability.ImmutableAvailability(this.availability));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the availability miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resourcing.availability.AvailabilityMiter getMiter() {
        return (this.availability);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the job.
     *
     *  @param job a job
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>job</code> is
     *          <code>null</code>
     */

    public T job(org.osid.resourcing.Job job) {
        getMiter().setJob(job);
        return (self());
    }


    /**
     *  Sets the competency.
     *
     *  @param competency a competency
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    public T competency(org.osid.resourcing.Competency competency) {
        getMiter().setCompetency(competency);
        return (self());
    }


    /**
     *  Sets the percentage.
     *
     *  @param percentage a percentage
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>percentage</code>
     *          is <code>null</code>
     */

    public T percentage(long percentage) {
        getMiter().setPercentage(percentage);
        return (self());
    }


    /**
     *  Adds an Availability record.
     *
     *  @param record an availability record
     *  @param recordType the type of availability record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resourcing.records.AvailabilityRecord record, org.osid.type.Type recordType) {
        getMiter().addAvailabilityRecord(record, recordType);
        return (self());
    }
}       


