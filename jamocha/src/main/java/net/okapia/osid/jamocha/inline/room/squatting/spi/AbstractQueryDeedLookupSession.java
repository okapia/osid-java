//
// AbstractQueryDeedLookupSession.java
//
//    An inline adapter that maps a DeedLookupSession to
//    a DeedQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a DeedLookupSession to
 *  a DeedQuerySession.
 */

public abstract class AbstractQueryDeedLookupSession
    extends net.okapia.osid.jamocha.room.squatting.spi.AbstractDeedLookupSession
    implements org.osid.room.squatting.DeedLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.room.squatting.DeedQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryDeedLookupSession.
     *
     *  @param querySession the underlying deed query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryDeedLookupSession(org.osid.room.squatting.DeedQuerySession querySession) {
        nullarg(querySession, "deed query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Campus</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform <code>Deed</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDeeds() {
        return (this.session.canSearchDeeds());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include deeds in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only deeds whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveDeedView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All deeds of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveDeedView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Deed</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Deed</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Deed</code> and
     *  retained for compatibility.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param  deedId <code>Id</code> of the
     *          <code>Deed</code>
     *  @return the deed
     *  @throws org.osid.NotFoundException <code>deedId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>deedId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Deed getDeed(org.osid.id.Id deedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchId(deedId, true);
        org.osid.room.squatting.DeedList deeds = this.session.getDeedsByQuery(query);
        if (deeds.hasNext()) {
            return (deeds.getNextDeed());
        } 
        
        throw new org.osid.NotFoundException(deedId + " not found");
    }


    /**
     *  Gets a <code>DeedList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  deeds specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Deeds</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, deeds are returned that are currently effective.
     *  In any effective mode, effective deeds and those currently expired
     *  are returned.
     *
     *  @param  deedIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>deedIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByIds(org.osid.id.IdList deedIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();

        try (org.osid.id.IdList ids = deedIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a <code>DeedList</code> corresponding to the given
     *  deed genus <code>Type</code> which does not include
     *  deeds of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, deeds are returned that are currently effective.
     *  In any effective mode, effective deeds and those currently expired
     *  are returned.
     *
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusType(org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchGenusType(deedGenusType, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a <code>DeedList</code> corresponding to the given
     *  deed genus <code>Type</code> and include any additional
     *  deeds with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByParentGenusType(org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchParentGenusType(deedGenusType, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a <code>DeedList</code> containing the given
     *  deed record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param  deedRecordType a deed record type 
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByRecordType(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchRecordType(deedRecordType, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a <code>DeedList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible
     *  through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsOnDate(org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }
        

    /**
     *  Gets a list of all deeds with a genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> deedGenusType, from 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeOnDate(org.osid.type.Type deedGenusType, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchGenusType(deedGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of deeds corresponding to a building
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible
     *  through this session.
     *
     *  In effective mode, deeds are returned that are
     *  currently effective.  In any effective mode, effective
     *  deeds and those currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.DeedList getDeedsForBuilding(org.osid.id.Id buildingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a <code> DeedList </code> containing the given building
     *  and genus type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> or 
     *          <code> deedGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                           org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchGenusType(deedGenusType, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of deeds corresponding to a building
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible
     *  through this session.
     *
     *  In effective mode, deeds are returned that are
     *  currently effective.  In any effective mode, effective
     *  deeds and those currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingOnDate(org.osid.id.Id buildingId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of all deeds for a building with a genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> buildingId, 
     *          deedGenusType, from </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                 org.osid.type.Type deedGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchGenusType(deedGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of deeds corresponding to a owner
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible
     *  through this session.
     *
     *  In effective mode, deeds are returned that are
     *  currently effective.  In any effective mode, effective
     *  deeds and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the owner
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.DeedList getDeedsForOwner(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchOwnerId(resourceId, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a <code> DeedList </code> containing the given owner and
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> deedGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForOwner(org.osid.id.Id resourceId, 
                                                                        org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchOwnerId(resourceId, true);
        query.matchGenusType(deedGenusType, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of deeds corresponding to a owner
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible
     *  through this session.
     *
     *  In effective mode, deeds are returned that are
     *  currently effective.  In any effective mode, effective
     *  deeds and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the owner
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForOwnerOnDate(org.osid.id.Id resourceId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchOwnerId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of all deeds for a resource owner with a genus
     *  type and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, 
     *          deedGenusType, from </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForOwnerOnDate(org.osid.id.Id resourceId, 
                                                                              org.osid.type.Type deedGenusType, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchOwnerId(resourceId, true);
        query.matchGenusType(deedGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of deeds corresponding to building and owner
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible
     *  through this session.
     *
     *  In effective mode, deeds are returned that are
     *  currently effective.  In any effective mode, effective
     *  deeds and those currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @param  resourceId the <code>Id</code> of the owner
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingAndOwner(org.osid.id.Id buildingId,
                                                                        org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchOwnerId(resourceId, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a <code> DeedList </code> for the given buildin, owner,
     *  and genus type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.NullArgumentException <code> buildingId,
     *          resourceId, </code> or <code> deedGenusType </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingAndOwner(org.osid.id.Id buildingId, 
                                                                                   org.osid.id.Id resourceId, 
                                                                                   org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchOwnerId(resourceId, true);
        query.matchGenusType(deedGenusType, true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets a list of deeds corresponding to building and owner
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible
     *  through this session.
     *
     *  In effective mode, deeds are returned that are
     *  currently effective.  In any effective mode, effective
     *  deeds and those currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @param  resourceId the <code>Id</code> of the owner
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingAndOwnerOnDate(org.osid.id.Id buildingId,
                                                                              org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchOwnerId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }

    
    /**
     *  Gets a list of all deeds for a building and owner with a genus
     *  type and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Deed </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> buildingId, resourceId, 
     *          deedGenusType, from, </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingAndOwnerOnDate(org.osid.id.Id buildingId, 
                                                                                         org.osid.id.Id resourceId, 
                                                                                         org.osid.type.Type deedGenusType, 
                                                                                         org.osid.calendaring.DateTime from, 
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchBuildingId(buildingId, true);
        query.matchOwnerId(resourceId, true);
        query.matchGenusType(deedGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getDeedsByQuery(query));
    }

    
    /**
     *  Gets all <code>Deeds</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  deeds or an error results. Otherwise, the returned list
     *  may contain only those deeds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Deeds</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeeds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.DeedQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getDeedsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.room.squatting.DeedQuery getQuery() {
        org.osid.room.squatting.DeedQuery query = this.session.getDeedQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
