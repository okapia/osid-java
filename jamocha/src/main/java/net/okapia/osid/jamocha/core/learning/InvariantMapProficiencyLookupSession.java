//
// InvariantMapProficiencyLookupSession
//
//    Implements a Proficiency lookup service backed by a fixed collection of
//    proficiencies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Implements a Proficiency lookup service backed by a fixed
 *  collection of proficiencies. The proficiencies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProficiencyLookupSession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractMapProficiencyLookupSession
    implements org.osid.learning.ProficiencyLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProficiencyLookupSession</code> with no
     *  proficiencies.
     *  
     *  @param objectiveBank the objective bank
     *  @throws org.osid.NullArgumnetException {@code objectiveBank} is
     *          {@code null}
     */

    public InvariantMapProficiencyLookupSession(org.osid.learning.ObjectiveBank objectiveBank) {
        setObjectiveBank(objectiveBank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProficiencyLookupSession</code> with a single
     *  proficiency.
     *  
     *  @param objectiveBank the objective bank
     *  @param proficiency a single proficiency
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code proficiency} is <code>null</code>
     */

      public InvariantMapProficiencyLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                               org.osid.learning.Proficiency proficiency) {
        this(objectiveBank);
        putProficiency(proficiency);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProficiencyLookupSession</code> using an array
     *  of proficiencies.
     *  
     *  @param objectiveBank the objective bank
     *  @param proficiencies an array of proficiencies
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code proficiencies} is <code>null</code>
     */

      public InvariantMapProficiencyLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                               org.osid.learning.Proficiency[] proficiencies) {
        this(objectiveBank);
        putProficiencies(proficiencies);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProficiencyLookupSession</code> using a
     *  collection of proficiencies.
     *
     *  @param objectiveBank the objective bank
     *  @param proficiencies a collection of proficiencies
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code proficiencies} is <code>null</code>
     */

      public InvariantMapProficiencyLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                               java.util.Collection<? extends org.osid.learning.Proficiency> proficiencies) {
        this(objectiveBank);
        putProficiencies(proficiencies);
        return;
    }
}
