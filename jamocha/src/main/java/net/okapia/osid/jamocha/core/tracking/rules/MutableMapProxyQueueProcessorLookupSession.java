//
// MutableMapProxyQueueProcessorLookupSession
//
//    Implements a QueueProcessor lookup service backed by a collection of
//    queueProcessors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueProcessor lookup service backed by a collection of
 *  queueProcessors. The queueProcessors are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of queue processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractMapQueueProcessorLookupSession
    implements org.osid.tracking.rules.QueueProcessorLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyQueueProcessorLookupSession}
     *  with no queue processors.
     *
     *  @param frontOffice the front office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  org.osid.proxy.Proxy proxy) {
        setFrontOffice(frontOffice);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyQueueProcessorLookupSession} with a
     *  single queue processor.
     *
     *  @param frontOffice the front office
     *  @param queueProcessor a queue processor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessor}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                org.osid.tracking.rules.QueueProcessor queueProcessor, org.osid.proxy.Proxy proxy) {
        this(frontOffice, proxy);
        putQueueProcessor(queueProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyQueueProcessorLookupSession} using an
     *  array of queue processors.
     *
     *  @param frontOffice the front office
     *  @param queueProcessors an array of queue processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessors}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                org.osid.tracking.rules.QueueProcessor[] queueProcessors, org.osid.proxy.Proxy proxy) {
        this(frontOffice, proxy);
        putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyQueueProcessorLookupSession} using a
     *  collection of queue processors.
     *
     *  @param frontOffice the front office
     *  @param queueProcessors a collection of queue processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessors}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                java.util.Collection<? extends org.osid.tracking.rules.QueueProcessor> queueProcessors,
                                                org.osid.proxy.Proxy proxy) {
   
        this(frontOffice, proxy);
        setSessionProxy(proxy);
        putQueueProcessors(queueProcessors);
        return;
    }

    
    /**
     *  Makes a {@code QueueProcessor} available in this session.
     *
     *  @param queueProcessor an queue processor
     *  @throws org.osid.NullArgumentException {@code queueProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessor(org.osid.tracking.rules.QueueProcessor queueProcessor) {
        super.putQueueProcessor(queueProcessor);
        return;
    }


    /**
     *  Makes an array of queueProcessors available in this session.
     *
     *  @param queueProcessors an array of queue processors
     *  @throws org.osid.NullArgumentException {@code queueProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessors(org.osid.tracking.rules.QueueProcessor[] queueProcessors) {
        super.putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Makes collection of queue processors available in this session.
     *
     *  @param queueProcessors
     *  @throws org.osid.NullArgumentException {@code queueProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessors(java.util.Collection<? extends org.osid.tracking.rules.QueueProcessor> queueProcessors) {
        super.putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Removes a QueueProcessor from this session.
     *
     *  @param queueProcessorId the {@code Id} of the queue processor
     *  @throws org.osid.NullArgumentException {@code queueProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeQueueProcessor(org.osid.id.Id queueProcessorId) {
        super.removeQueueProcessor(queueProcessorId);
        return;
    }    
}
