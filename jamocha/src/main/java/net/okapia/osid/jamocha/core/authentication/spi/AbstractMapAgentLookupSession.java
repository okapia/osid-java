//
// AbstractMapAgentLookupSession
//
//    A simple framework for providing an Agent lookup service
//    backed by a fixed collection of agents.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Agent lookup service backed by a
 *  fixed collection of agents. The agents are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Agents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAgentLookupSession
    extends net.okapia.osid.jamocha.authentication.spi.AbstractAgentLookupSession
    implements org.osid.authentication.AgentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authentication.Agent> agents = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authentication.Agent>());


    /**
     *  Makes an <code>Agent</code> available in this session.
     *
     *  @param  agent an agent
     *  @throws org.osid.NullArgumentException <code>agent<code>
     *          is <code>null</code>
     */

    protected void putAgent(org.osid.authentication.Agent agent) {
        this.agents.put(agent.getId(), agent);
        return;
    }


    /**
     *  Makes an array of agents available in this session.
     *
     *  @param  agents an array of agents
     *  @throws org.osid.NullArgumentException <code>agents<code>
     *          is <code>null</code>
     */

    protected void putAgents(org.osid.authentication.Agent[] agents) {
        putAgents(java.util.Arrays.asList(agents));
        return;
    }


    /**
     *  Makes a collection of agents available in this session.
     *
     *  @param  agents a collection of agents
     *  @throws org.osid.NullArgumentException <code>agents<code>
     *          is <code>null</code>
     */

    protected void putAgents(java.util.Collection<? extends org.osid.authentication.Agent> agents) {
        for (org.osid.authentication.Agent agent : agents) {
            this.agents.put(agent.getId(), agent);
        }

        return;
    }


    /**
     *  Removes an Agent from this session.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @throws org.osid.NullArgumentException <code>agentId<code> is
     *          <code>null</code>
     */

    protected void removeAgent(org.osid.id.Id agentId) {
        this.agents.remove(agentId);
        return;
    }


    /**
     *  Gets the <code>Agent</code> specified by its <code>Id</code>.
     *
     *  @param  agentId <code>Id</code> of the <code>Agent</code>
     *  @return the agent
     *  @throws org.osid.NotFoundException <code>agentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>agentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authentication.Agent agent = this.agents.get(agentId);
        if (agent == null) {
            throw new org.osid.NotFoundException("agent not found: " + agentId);
        }

        return (agent);
    }


    /**
     *  Gets all <code>Agents</code>. In plenary mode, the returned
     *  list contains all known agents or an error
     *  results. Otherwise, the returned list may contain only those
     *  agents that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Agents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.agent.ArrayAgentList(this.agents.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.agents.clear();
        super.close();
        return;
    }
}
