//
// AbstractDeleteResponse.java
//
//     Defines a DeleteResponse.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transaction.batch.deleteresponse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>DeleteResponse</code>.
 */

public abstract class AbstractDeleteResponse
    implements org.osid.transaction.batch.DeleteResponse {

    private org.osid.id.Id deletedId;
    private org.osid.locale.DisplayText message;
    

    /**
     *  Gets the reference <code> Id </code> of the object on which
     *  this item within the delete operation operated.
     *
     *  @return the reference object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDeletedId() {
        return (this.deletedId);
    }


    /**
     *  Sets the deleted Id.
     *
     *  @param deletedId the deleted Id
     *  @throws org.osid.NullArgumentException <code>deletedId</code> is
     *          <code>null</code>
     */
    
    protected void setDeletedId(org.osid.id.Id deletedId) {
        nullarg(deletedId, "deleted Id");
        this.deletedId = deletedId;
        return;
    }


    /**
     *  Tests if this item within the delete operation was successful. 
     *
     *  @return <code> true </code> if the delete operation was
     *          successful, <code> false </code> if it was not
     *          successful
     */

    @OSID @Override
    public boolean isSuccessful() {
        if (this.message == null) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the error message for an unsuccessful item within the
     *  delete operation.
     *
     *  @return the message 
     *  @throws org.osid.IllegalStateException <code> isSuccessful()
     *          </code> is <code> true </code>
     */

    @OSID @Override
    public org.osid.locale.DisplayText getErrorMessage() {
        if (isSuccessful()) {
            throw new org.osid.IllegalStateException("isSuccessful() is true");
        }

        return (this.message);
    }

    
    /**
     *  Sets the error message.
     *
     *  @param message the error message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    protected void setErrorMessage(org.osid.locale.DisplayText message) {
        nullarg(message, "error message");
        this.message = message;
        return;
    }
}
