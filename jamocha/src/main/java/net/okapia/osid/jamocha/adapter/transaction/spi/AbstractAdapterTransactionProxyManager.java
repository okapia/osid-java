//
// AbstractTransactionProxyManager.java
//
//     An adapter for a TransactionProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.transaction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TransactionProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTransactionProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.transaction.TransactionProxyManager>
    implements org.osid.transaction.TransactionProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterTransactionProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTransactionProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTransactionProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTransactionProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if transactions are supported. 
     *
     *  @return <code> true </code> if transactions are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactions() {
        return (getAdapteeManager().supportsTransactions());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the transaction 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a transaction session 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnimplementedException <code> supportsTransactions() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transaction.TransactionSession getTransactionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAdapteeManager().getTransactionSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
