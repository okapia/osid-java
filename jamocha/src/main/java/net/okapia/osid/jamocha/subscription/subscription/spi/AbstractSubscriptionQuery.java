//
// AbstractSubscriptionQuery.java
//
//     A template for making a Subscription Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for subscriptions.
 */

public abstract class AbstractSubscriptionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.subscription.SubscriptionQuery {

    private final java.util.Collection<org.osid.subscription.records.SubscriptionQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to dispatches. 
     *
     *  @param  dispatchId a dispatch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dispatchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDispatchId(org.osid.id.Id dispatchId, boolean match) {
        return;
    }


    /**
     *  Clears the dispatch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDispatchIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DispatchQuery </code> is available. 
     *
     *  @return <code> true </code> if a dispatch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dispatch query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the dispatch query 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuery getDispatchQuery() {
        throw new org.osid.UnimplementedException("supportsDispatchQuery() is false");
    }


    /**
     *  Clears the dispatch terms. 
     */

    @OSID @Override
    public void clearDispatchTerms() {
        return;
    }


    /**
     *  Sets a subscriber <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubscriberId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the subscriber <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubscriberIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriberQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subscriber query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriberQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSubscriberQuery() {
        throw new org.osid.UnimplementedException("supportsSubscriberQuery() is false");
    }


    /**
     *  Clears the subscriber terms. 
     */

    @OSID @Override
    public void clearSubscriberTerms() {
        return;
    }


    /**
     *  Sets an address <code> Id. </code> 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Matches any address. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyAddress(boolean match) {
        return;
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        return;
    }


    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to publishers. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPublisherId(org.osid.id.Id publisherId, boolean match) {
        return;
    }


    /**
     *  Clears the publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPublisherIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsPublisherQuery() is false");
    }


    /**
     *  Clears the publisher terms. 
     */

    @OSID @Override
    public void clearPublisherTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given subscription query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a subscription implementing the requested record.
     *
     *  @param subscriptionRecordType a subscription record type
     *  @return the subscription query record
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionQueryRecord getSubscriptionQueryRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.SubscriptionQueryRecord record : this.records) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subscription query. 
     *
     *  @param subscriptionQueryRecord subscription query record
     *  @param subscriptionRecordType subscription record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubscriptionQueryRecord(org.osid.subscription.records.SubscriptionQueryRecord subscriptionQueryRecord, 
                                          org.osid.type.Type subscriptionRecordType) {

        addRecordType(subscriptionRecordType);
        nullarg(subscriptionQueryRecord, "subscription query record");
        this.records.add(subscriptionQueryRecord);        
        return;
    }
}
