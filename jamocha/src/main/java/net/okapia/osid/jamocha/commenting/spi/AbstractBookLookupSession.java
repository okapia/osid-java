//
// AbstractBookLookupSession.java
//
//    A starter implementation framework for providing a Book
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Book
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBooks(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBookLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.commenting.BookLookupSession {

    private boolean pedantic = false;
    private org.osid.commenting.Book book = new net.okapia.osid.jamocha.nil.commenting.book.UnknownBook();
    

    /**
     *  Tests if this user can perform <code>Book</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBooks() {
        return (true);
    }


    /**
     *  A complete view of the <code>Book</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBookView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Book</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBookView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Book</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Book</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Book</code> and
     *  retained for compatibility.
     *
     *  @param  bookId <code>Id</code> of the
     *          <code>Book</code>
     *  @return the book
     *  @throws org.osid.NotFoundException <code>bookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>bookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.commenting.BookList books = getBooks()) {
            while (books.hasNext()) {
                org.osid.commenting.Book book = books.getNextBook();
                if (book.getId().equals(bookId)) {
                    return (book);
                }
            }
        } 

        throw new org.osid.NotFoundException(bookId + " not found");
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  books specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Books</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBooks()</code>.
     *
     *  @param  bookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>bookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByIds(org.osid.id.IdList bookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.commenting.Book> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = bookIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBook(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("book " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.commenting.book.LinkedBookList(ret));
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  book genus <code>Type</code> which does not include
     *  books of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBooks()</code>.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.commenting.book.BookGenusFilterList(getBooks(), bookGenusType));
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  book genus <code>Type</code> and include any additional
     *  books with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBooks()</code>.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByParentGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBooksByGenusType(bookGenusType));
    }


    /**
     *  Gets a <code>BookList</code> containing the given
     *  book record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBooks()</code>.
     *
     *  @param  bookRecordType a book record type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByRecordType(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.commenting.book.BookRecordFilterList(getBooks(), bookRecordType));
    }


    /**
     *  Gets a <code>BookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known books or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  books that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Book</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.commenting.book.BookProviderFilterList(getBooks(), resourceId));
    }


    /**
     *  Gets all <code>Books</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Books</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.commenting.BookList getBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the book list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of books
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.commenting.BookList filterBooksOnViews(org.osid.commenting.BookList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
