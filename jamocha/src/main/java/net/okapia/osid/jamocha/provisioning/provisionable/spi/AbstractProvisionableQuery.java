//
// AbstractProvisionableQuery.java
//
//     A template for making a Provisionable Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionable.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for provisionables.
 */

public abstract class AbstractProvisionableQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.provisioning.ProvisionableQuery {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionableQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Matches provisionables provisioned the number of times in the given 
     *  range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchUse(long start, long end, boolean match) {
        return;
    }


    /**
     *  Clears the position query terms. 
     */

    @OSID @Override
    public void clearUseTerms() {
        return;
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given provisionable query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a provisionable implementing the requested record.
     *
     *  @param provisionableRecordType a provisionable record type
     *  @return the provisionable query record
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionableRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableQueryRecord getProvisionableQueryRecord(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionableQueryRecord record : this.records) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionableRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provisionable query. 
     *
     *  @param provisionableQueryRecord provisionable query record
     *  @param provisionableRecordType provisionable record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProvisionableQueryRecord(org.osid.provisioning.records.ProvisionableQueryRecord provisionableQueryRecord, 
                                          org.osid.type.Type provisionableRecordType) {

        addRecordType(provisionableRecordType);
        nullarg(provisionableQueryRecord, "provisionable query record");
        this.records.add(provisionableQueryRecord);        
        return;
    }
}
