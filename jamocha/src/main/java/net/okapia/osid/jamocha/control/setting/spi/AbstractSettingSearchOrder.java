//
// AbstractSettingSearchOdrer.java
//
//     Defines a SettingSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.setting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code SettingSearchOrder}.
 */

public abstract class AbstractSettingSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.control.SettingSearchOrder {

    private final java.util.Collection<org.osid.control.records.SettingSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by controller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByController(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a controller search order is available. 
     *
     *  @return <code> true </code> if a controller search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the controller search order. 
     *
     *  @return the controller search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsControllerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchOrder getControllerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsControllerSearchOrder() is false");
    }


    /**
     *  Orders the results by on status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOn(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by off status, 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOff(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by variable percentage. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariablePercentage(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by variable amount. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariableAmount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by variable discreet state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDiscreetState(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsDiscreetStateSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getDiscreetStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateSearchOrder() is false");
    }


    /**
     *  Orders the results by ramp rate. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRampRate(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  settingRecordType a setting record type 
     *  @return {@code true} if the settingRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code settingRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type settingRecordType) {
        for (org.osid.control.records.SettingSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(settingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  settingRecordType the setting record type 
     *  @return the setting search order record
     *  @throws org.osid.NullArgumentException
     *          {@code settingRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(settingRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.control.records.SettingSearchOrderRecord getSettingSearchOrderRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SettingSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(settingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this setting. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param settingRecord the setting search odrer record
     *  @param settingRecordType setting record type
     *  @throws org.osid.NullArgumentException
     *          {@code settingRecord} or
     *          {@code settingRecordTypesetting} is
     *          {@code null}
     */
            
    protected void addSettingRecord(org.osid.control.records.SettingSearchOrderRecord settingSearchOrderRecord, 
                                     org.osid.type.Type settingRecordType) {

        addRecordType(settingRecordType);
        this.records.add(settingSearchOrderRecord);
        
        return;
    }
}
