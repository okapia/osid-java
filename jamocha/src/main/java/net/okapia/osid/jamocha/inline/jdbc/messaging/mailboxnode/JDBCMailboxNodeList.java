//
// JDBCMailboxNodeList
//
//     Implements a MailboxNodeList. This list creates mailboxnodes from
//     the return of a JDBC query.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.jdbc.messaging.mailboxnode;

import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements a MailboxNodeList. This list creates mailboxnodes from the
 *  return of a JDBC query. The query is processed in a separate
 *  thread.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more mailboxnodes to be added.</p>
 */

public final class JDBCMailboxNodeList
    extends net.okapia.osid.jamocha.messaging.mailboxnode.spi.AbstractMutableMailboxNodeList
    implements org.osid.messaging.MailboxNodeList,
               Runnable {

    private boolean running = false;
    private MailboxNodeFetcher fetcher;


    /**
     *  Creates a new <code>JDBCMailboxNodeList</code>.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a mailboxNode to parse a result row and
     *         generate a MailboxNode
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of mailboxnodes in this list
     *         buffer such that when the number of mailboxnodes exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until mailboxnodes are retrieved. The value
     *         of this may depend on the amount of memory a MailboxNode
     *         consumes.
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>mailboxNode</code>
     *          is <code>null</code>
     */

    public JDBCMailboxNodeList(String query, java.sql.Connection connection, JDBCMailboxNodeGenerator generator,
                        boolean closeWhenDone, int bufferSize)
        throws org.osid.OperationFailedException {

        nullarg(query, "query");
        nullarg(connection, "connection");
        nullarg(generator, "generator");

        if (bufferSize <= 0) {
            throw new org.osid.InvalidArgumentException("buffer size too small");
        }

        this.fetcher = new MailboxNodeFetcher(query, connection, generator, closeWhenDone, bufferSize);
        return;
    }


    /**
     *  Creates a new <code>JDBCMailboxNodeList</code> and runs it upon
     *  instantiation.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a MailboxNode to parse a result row and
     *         generate a MailboxNode
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of mailboxnodes in this list
     *         buffer such that when the number of mailboxnodes exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until mailboxnodes are retrieved. The value
     *         of this may depend on the amount of memory a MailboxNode
     *         consumes.
     *  @param run <code>true</code> to start the fetching thread
     *         immediately
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>mailboxNode</code>
     *          is <code>null</code>
     */

    public JDBCMailboxNodeList(String query, java.sql.Connection connection, JDBCMailboxNodeGenerator generator,
                        boolean closeWhenDone, int bufferSize, boolean run)
        throws org.osid.OperationFailedException {

        this(query, connection, generator, closeWhenDone, bufferSize);
        if (run) {
            run();
        }
        return;
    }


    /**
     *  Starts the JDBC process.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    public void run() {
        if (this.running || hasError()) {
            throw new org.osid.IllegalStateException("already started");
        }

        this.fetcher.start();
        return;
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.running = false;
        return;
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    class MailboxNodeFetcher
        extends Thread {
        
        private String query;
        private java.sql.Connection connection;
        private JDBCMailboxNodeGenerator generator;
        private boolean closeWhenDone;
        private int bufferSize;

        
        MailboxNodeFetcher(String query, java.sql.Connection connection, JDBCMailboxNodeGenerator generator,
                        boolean closeWhenDone, int bufferSize) {

            this.query         = query;
            this.connection    = connection;
            this.generator     = generator;
            this.closeWhenDone = closeWhenDone;
            this.bufferSize    = bufferSize;

            return;
        }
            
        
        public void run() {
            java.sql.Statement statement = null;
            java.sql.ResultSet resultset = null;
            JDBCMailboxNodeList.this.running = true;

            try {
                statement = this.connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                                                            java.sql.ResultSet.CONCUR_READ_ONLY);
                resultset = statement.executeQuery(this.query);
                long length = 0;

                while (resultset.next() && JDBCMailboxNodeList.this.running && !JDBCMailboxNodeList.this.hasError()) {
                    JDBCMailboxNodeList.this.addMailboxNode(generator.makeMailboxNode(resultset));
                    synchronized (JDBCMailboxNodeList.this) {
                        JDBCMailboxNodeList.this.notifyAll();
                    }

                    if (++length > this.bufferSize) {
                        length = JDBCMailboxNodeList.this.available();
                        if (length > this.bufferSize) {
                            synchronized (JDBCMailboxNodeList.this) {
                                try {
                                    JDBCMailboxNodeList.this.wait();
                                } catch (InterruptedException ie) {}
                            }
                        }
                    } 
                }
            } catch (Exception e) {
                JDBCMailboxNodeList.this.error(new org.osid.OperationFailedException("cannot generate mailboxnode", e));
                return;
            } finally {
                try {
                    if (this.closeWhenDone) {
                        this.connection.close();
                    }
                 
                    JDBCMailboxNodeList.this.running = false;

                    if (statement != null) {
                        statement.close();
                    }

                    if (resultset != null) {
                        resultset.close();
                    }
                } catch (Exception e) {}
            }

            JDBCMailboxNodeList.this.eol();
            return;
        }
    }
}
