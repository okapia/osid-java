//
// AbstractQueryOfficeLookupSession.java
//
//    An OfficeQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OfficeQuerySession adapter.
 */

public abstract class AbstractAdapterOfficeQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.OfficeQuerySession {

    private final org.osid.workflow.OfficeQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterOfficeQuerySession.
     *
     *  @param session the underlying office query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOfficeQuerySession(org.osid.workflow.OfficeQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeOffice</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchOffices() {
        return (this.session.canSearchOffices());
    }

      
    /**
     *  Gets an office query. The returned query will not have an
     *  extension query.
     *
     *  @return the office query 
     */
      
    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        return (this.session.getOfficeQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  officeQuery the office query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code officeQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code officeQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByQuery(org.osid.workflow.OfficeQuery officeQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getOfficesByQuery(officeQuery));
    }
}
