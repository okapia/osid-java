//
// MutableIndexedMapRaceLookupSession
//
//    Implements a Race lookup service backed by a collection of
//    races indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Race lookup service backed by a collection of
 *  races. The races are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some races may be compatible
 *  with more types than are indicated through these race
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of races can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRaceLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractIndexedMapRaceLookupSession
    implements org.osid.voting.RaceLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRaceLookupSession} with no races.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapRaceLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceLookupSession} with a
     *  single race.
     *  
     *  @param polls the polls
     *  @param  race a single race
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code race} is {@code null}
     */

    public MutableIndexedMapRaceLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Race race) {
        this(polls);
        putRace(race);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceLookupSession} using an
     *  array of races.
     *
     *  @param polls the polls
     *  @param  races an array of races
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code races} is {@code null}
     */

    public MutableIndexedMapRaceLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Race[] races) {
        this(polls);
        putRaces(races);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceLookupSession} using a
     *  collection of races.
     *
     *  @param polls the polls
     *  @param  races a collection of races
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code races} is {@code null}
     */

    public MutableIndexedMapRaceLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.Race> races) {

        this(polls);
        putRaces(races);
        return;
    }
    

    /**
     *  Makes a {@code Race} available in this session.
     *
     *  @param  race a race
     *  @throws org.osid.NullArgumentException {@code race{@code  is
     *          {@code null}
     */

    @Override
    public void putRace(org.osid.voting.Race race) {
        super.putRace(race);
        return;
    }


    /**
     *  Makes an array of races available in this session.
     *
     *  @param  races an array of races
     *  @throws org.osid.NullArgumentException {@code races{@code 
     *          is {@code null}
     */

    @Override
    public void putRaces(org.osid.voting.Race[] races) {
        super.putRaces(races);
        return;
    }


    /**
     *  Makes collection of races available in this session.
     *
     *  @param  races a collection of races
     *  @throws org.osid.NullArgumentException {@code race{@code  is
     *          {@code null}
     */

    @Override
    public void putRaces(java.util.Collection<? extends org.osid.voting.Race> races) {
        super.putRaces(races);
        return;
    }


    /**
     *  Removes a Race from this session.
     *
     *  @param raceId the {@code Id} of the race
     *  @throws org.osid.NullArgumentException {@code raceId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRace(org.osid.id.Id raceId) {
        super.removeRace(raceId);
        return;
    }    
}
