//
// AbstractResource.java
//
//     Defines a Resource builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.resource.spi;


/**
 *  Defines a <code>Resource</code> builder.
 */

public abstract class AbstractResourceBuilder<T extends AbstractResourceBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resource.resource.ResourceMiter resource;


    /**
     *  Constructs a new <code>AbstractResourceBuilder</code>.
     *
     *  @param resource the resource to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractResourceBuilder(net.okapia.osid.jamocha.builder.resource.resource.ResourceMiter resource) {
        super(resource);
        this.resource = resource;
        return;
    }


    /**
     *  Builds the resource.
     *
     *  @return the new resource
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resource.Resource build() {
        (new net.okapia.osid.jamocha.builder.validator.resource.resource.ResourceValidator(getValidations())).validate(this.resource);
        return (new net.okapia.osid.jamocha.builder.resource.resource.ImmutableResource(this.resource));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the resource miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resource.resource.ResourceMiter getMiter() {
        return (this.resource);
    }


    /**
     *  Sets the group flag.
     *
     *  @return the builder
     */

    public T group() {
        getMiter().setGroup(true);
        return (self());
    }


    /**
     *  Unsets the group flag.
     *
     *  @return the builder
     */

    public T single() {
        getMiter().setGroup(false);
        return (self());
    }


    /**
     *  Sets the demographic flag.
     *
     *  @return the builder
     */

    public T demographic() {
        getMiter().setDemographic(true);
        return (self());
    }


    /**
     *  Unsets the demographic flag.
     *
     *  @return the builder
     */

    public T noDemographic() {
        getMiter().setDemographic(false);
        return (self());
    }


    /**
     *  Sets the avatar.
     *
     *  @param avatar an avatar
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>avatar</code> is
     *          <code>null</code>
     */

    public T avatar(org.osid.repository.Asset avatar) {
        getMiter().setAvatar(avatar);
        return (self());
    }


    /**
     *  Adds a Resource record.
     *
     *  @param record a resource record
     *  @param recordType the type of resource record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resource.records.ResourceRecord record, org.osid.type.Type recordType) {
        getMiter().addResourceRecord(record, recordType);
        return (self());
    }
}       


