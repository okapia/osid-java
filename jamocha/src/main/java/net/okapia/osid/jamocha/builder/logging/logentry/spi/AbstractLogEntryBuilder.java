//
// AbstractLogEntry.java
//
//     Defines a LogEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.logging.logentry.spi;


/**
 *  Defines a <code>LogEntry</code> builder.
 */

public abstract class AbstractLogEntryBuilder<T extends AbstractLogEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.logging.logentry.LogEntryMiter logEntry;


    /**
     *  Constructs a new <code>AbstractLogEntryBuilder</code>.
     *
     *  @param logEntry the log entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractLogEntryBuilder(net.okapia.osid.jamocha.builder.logging.logentry.LogEntryMiter logEntry) {
        super(logEntry);
        this.logEntry = logEntry;
        return;
    }


    /**
     *  Builds the log entry.
     *
     *  @return the new log entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.logging.LogEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.logging.logentry.LogEntryValidator(getValidations())).validate(this.logEntry);
        return (new net.okapia.osid.jamocha.builder.logging.logentry.ImmutableLogEntry(this.logEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the log entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.logging.logentry.LogEntryMiter getMiter() {
        return (this.logEntry);
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>priority</code>
     *          is <code>null</code>
     */

    public T priority(org.osid.type.Type priority) {
        getMiter().setPriority(priority);
        return (self());
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public T timestamp(org.osid.calendaring.DateTime timestamp) {
        getMiter().setTimestamp(timestamp);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Adds a LogEntry record.
     *
     *  @param record a log entry record
     *  @param recordType the type of log entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.logging.records.LogEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addLogEntryRecord(record, recordType);
        return (self());
    }
}       


