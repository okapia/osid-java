//
// AbstractSignalEnablerLookupSession.java
//
//    A starter implementation framework for providing a SignalEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a SignalEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSignalEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSignalEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.path.rules.SignalEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();
    

    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>SignalEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSignalEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>SignalEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSignalEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>SignalEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySignalEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include signal enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active signal enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSignalEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive signal enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSignalEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SignalEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SignalEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SignalEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  signalEnablerId <code>Id</code> of the
     *          <code>SignalEnabler</code>
     *  @return the signal enabler
     *  @throws org.osid.NotFoundException <code>signalEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>signalEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnabler getSignalEnabler(org.osid.id.Id signalEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.path.rules.SignalEnablerList signalEnablers = getSignalEnablers()) {
            while (signalEnablers.hasNext()) {
                org.osid.mapping.path.rules.SignalEnabler signalEnabler = signalEnablers.getNextSignalEnabler();
                if (signalEnabler.getId().equals(signalEnablerId)) {
                    return (signalEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(signalEnablerId + " not found");
    }


    /**
     *  Gets a <code>SignalEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  signalEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SignalEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSignalEnablers()</code>.
     *
     *  @param  signalEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByIds(org.osid.id.IdList signalEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.path.rules.SignalEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = signalEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSignalEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("signal enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.path.rules.signalenabler.LinkedSignalEnablerList(ret));
    }


    /**
     *  Gets a <code>SignalEnablerList</code> corresponding to the given
     *  signal enabler genus <code>Type</code> which does not include
     *  signal enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSignalEnablers()</code>.
     *
     *  @param  signalEnablerGenusType a signalEnabler genus type 
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByGenusType(org.osid.type.Type signalEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.rules.signalenabler.SignalEnablerGenusFilterList(getSignalEnablers(), signalEnablerGenusType));
    }


    /**
     *  Gets a <code>SignalEnablerList</code> corresponding to the given
     *  signal enabler genus <code>Type</code> and include any additional
     *  signal enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSignalEnablers()</code>.
     *
     *  @param  signalEnablerGenusType a signalEnabler genus type 
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByParentGenusType(org.osid.type.Type signalEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSignalEnablersByGenusType(signalEnablerGenusType));
    }


    /**
     *  Gets a <code>SignalEnablerList</code> containing the given
     *  signal enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSignalEnablers()</code>.
     *
     *  @param  signalEnablerRecordType a signalEnabler record type 
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByRecordType(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.rules.signalenabler.SignalEnablerRecordFilterList(getSignalEnablers(), signalEnablerRecordType));
    }


    /**
     *  Gets a <code>SignalEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible
     *  through this session.
     *  
     *  In active mode, signal enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive signal enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SignalEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.rules.signalenabler.TemporalSignalEnablerFilterList(getSignalEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>SignalEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible
     *  through this session.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive signal enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getSignalEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>SignalEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @return a list of <code>SignalEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.mapping.path.rules.SignalEnablerList getSignalEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the signal enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of signal enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.mapping.path.rules.SignalEnablerList filterSignalEnablersOnViews(org.osid.mapping.path.rules.SignalEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.mapping.path.rules.SignalEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.mapping.path.rules.signalenabler.ActiveSignalEnablerFilterList(ret);
        }

        return (ret);
    }
}
