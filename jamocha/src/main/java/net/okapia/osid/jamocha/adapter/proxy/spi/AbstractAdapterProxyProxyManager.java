//
// AbstractProxyProxyManager.java
//
//     An adapter for a ProxyProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.proxy.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ProxyProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterProxyProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.proxy.ProxyProxyManager>
    implements org.osid.proxy.ProxyProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterProxyProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterProxyProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterProxyProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterProxyProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if a proxy session is supported. 
     *
     *  @return <code> true </code> if proxy is supported <code> , </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProxy() {
        return (getAdapteeManager().supportsProxy());
    }


    /**
     *  Gets the supported <code> Proxy </code> record interface types. 
     *
     *  @return a list containing the supported <code> Proxy </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProxyRecordTypes() {
        return (getAdapteeManager().getProxyRecordTypes());
    }


    /**
     *  Tests if the given <code> Proxy </code> record interface type is 
     *  supported. 
     *
     *  @param  proxyRecordType a <code> Type </code> indicating a <code> 
     *          Proxy </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proxyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProxyRecordType(org.osid.type.Type proxyRecordType) {
        return (getAdapteeManager().supportsProxyRecordType(proxyRecordType));
    }


    /**
     *  Gets the supported <code> ProxyCondition </code> record interface 
     *  types. 
     *
     *  @return a list containing the supported <code> ProxyCondition </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProxyConditionRecordTypes() {
        return (getAdapteeManager().getProxyConditionRecordTypes());
    }


    /**
     *  Tests if the given <code> ProxyCondition </code> record interface type 
     *  is supported. 
     *
     *  @param  proxyConditionRecordType a <code> Type </code> indicating a 
     *          <code> ProxyCondition </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proxyConditionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProxyConditionRecordType(org.osid.type.Type proxyConditionRecordType) {
        return (getAdapteeManager().supportsProxyConditionRecordType(proxyConditionRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the <code> 
     *  ProxySession </code> using the supplied <code> Proxy. </code> 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProxySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProxy() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.ProxySession getProxySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProxySession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
