//
// AbstractAdapterAssessmentOfferedLookupSession.java
//
//    An AssessmentOffered lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AssessmentOffered lookup session adapter.
 */

public abstract class AbstractAdapterAssessmentOfferedLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.AssessmentOfferedLookupSession {

    private final org.osid.assessment.AssessmentOfferedLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentOfferedLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentOfferedLookupSession(org.osid.assessment.AssessmentOfferedLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@code Bank} associated with this session.
     *
     *  @return the {@code Bank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@code AssessmentOffered} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAssessmentsOffered() {
        return (this.session.canLookupAssessmentsOffered());
    }


    /**
     *  A complete view of the {@code AssessmentOffered} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentOfferedView() {
        this.session.useComparativeAssessmentOfferedView();
        return;
    }


    /**
     *  A complete view of the {@code AssessmentOffered} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentOfferedView() {
        this.session.usePlenaryAssessmentOfferedView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments offered in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
     
    /**
     *  Gets the {@code AssessmentOffered} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AssessmentOffered} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AssessmentOffered} and
     *  retained for compatibility.
     *
     *  @param assessmentOfferedId {@code Id} of the {@code AssessmentOffered}
     *  @return the assessment offered
     *  @throws org.osid.NotFoundException {@code assessmentOfferedId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code assessmentOfferedId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentOffered(assessmentOfferedId));
    }


    /**
     *  Gets an {@code AssessmentOfferedList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsOffered specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AssessmentsOffered} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  assessmentOfferedIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AssessmentOffered} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentOfferedIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByIds(org.osid.id.IdList assessmentOfferedIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsOfferedByIds(assessmentOfferedIds));
    }


    /**
     *  Gets an {@code AssessmentOfferedList} corresponding to the given
     *  assessment offered genus {@code Type} which does not include
     *  assessments offered of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned {@code AssessmentOffered} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentOfferedGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsOfferedByGenusType(assessmentOfferedGenusType));
    }


    /**
     *  Gets an {@code AssessmentOfferedList} corresponding to the given
     *  assessment offered genus {@code Type} and include any additional
     *  assessments offered with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned {@code AssessmentOffered} list
     *  @throws org.osid.NullArgumentException {@code
     *          assessmentOfferedGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByParentGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsOfferedByParentGenusType(assessmentOfferedGenusType));
    }


    /**
     *  Gets an {@code AssessmentOfferedList} containing the given
     *  assessment offered record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  @param  assessmentOfferedRecordType an assessmentOffered record type 
     *  @return the returned {@code AssessmentOffered} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentOfferedRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByRecordType(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsOfferedByRecordType(assessmentOfferedRecordType));
    }


    /**
     *  Gets an {@code AssessmentOfferedList} that have
     *  designated start times where the start times fall in the given
     *  range inclusive.  In plenary mode, the returned list contains
     *  all known assessments offered or an error results. Otherwise,
     *  the returned list may contain only those assessments offered
     *  that are accessible through this session.
     *
     *  @param start start of time range
     *  @param  end end of time range 
     *  @return the returned {@code AssessmentOffered} list 
     *  @throws org.osid.InvalidArgumentException {@code end} is less 
     *          than {@code start} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByDate(org.osid.calendaring.DateTime start, 
                                                                                 org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsOfferedByDate(start, end));
    }


    /**
     *  Gets an {@code AssessmentOfferedList} by the given
     *  assessment.  In plenary mode, the returned list contains all
     *  known assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param  assessmentId {@code Id} of an {@code Assessment} 
     *  @return the returned {@code AssessmentOffered} list 
     *  @throws org.osid.NullArgumentException {@code assessmentId}
     *         is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAssessmentsOfferedForAssessment(assessmentId));
    }


    /**
     *  Gets all {@code AssessmentsOffered}. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code AssessmentsOffered} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOffered()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsOffered());
    }
}
