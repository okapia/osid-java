//
// AbstractBlock.java
//
//     Defines a Block builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.block.spi;


/**
 *  Defines a <code>Block</code> builder.
 */

public abstract class AbstractBlockBuilder<T extends AbstractBlockBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.hold.block.BlockMiter block;


    /**
     *  Constructs a new <code>AbstractBlockBuilder</code>.
     *
     *  @param block the block to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBlockBuilder(net.okapia.osid.jamocha.builder.hold.block.BlockMiter block) {
        super(block);
        this.block = block;
        return;
    }


    /**
     *  Builds the block.
     *
     *  @return the new block
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.hold.Block build() {
        (new net.okapia.osid.jamocha.builder.validator.hold.block.BlockValidator(getValidations())).validate(this.block);
        return (new net.okapia.osid.jamocha.builder.hold.block.ImmutableBlock(this.block));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the block miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.hold.block.BlockMiter getMiter() {
        return (this.block);
    }


    /**
     *  Adds an issue.
     *
     *  @param issue an issue
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>issue</code> is <code>null</code>
     */

    public T issue(org.osid.hold.Issue issue) {
        getMiter().addIssue(issue);
        return (self());
    }


    /**
     *  Sets all the issues.
     *
     *  @param issues a collection of issues
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>issues</code> is <code>null</code>
     */

    public T issues(java.util.Collection<org.osid.hold.Issue> issues) {
        getMiter().setIssues(issues);
        return (self());
    }

    
    /**
     *  Adds a Block record.
     *
     *  @param record a block record
     *  @param recordType the type of block record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.hold.records.BlockRecord record, org.osid.type.Type recordType) {
        getMiter().addBlockRecord(record, recordType);
        return (self());
    }
}       


