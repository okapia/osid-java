//
// AbstractParticipantSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractParticipantSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.ParticipantSearchResults {

    private org.osid.offering.ParticipantList participants;
    private final org.osid.offering.ParticipantQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.records.ParticipantSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractParticipantSearchResults.
     *
     *  @param participants the result set
     *  @param participantQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>participants</code>
     *          or <code>participantQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractParticipantSearchResults(org.osid.offering.ParticipantList participants,
                                            org.osid.offering.ParticipantQueryInspector participantQueryInspector) {
        nullarg(participants, "participants");
        nullarg(participantQueryInspector, "participant query inspectpr");

        this.participants = participants;
        this.inspector = participantQueryInspector;

        return;
    }


    /**
     *  Gets the participant list resulting from a search.
     *
     *  @return a participant list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipants() {
        if (this.participants == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.ParticipantList participants = this.participants;
        this.participants = null;
	return (participants);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.ParticipantQueryInspector getParticipantQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  participant search record <code> Type. </code> This method must
     *  be used to retrieve a participant implementing the requested
     *  record.
     *
     *  @param participantSearchRecordType a participant search 
     *         record type 
     *  @return the participant search
     *  @throws org.osid.NullArgumentException
     *          <code>participantSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(participantSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantSearchResultsRecord getParticipantSearchResultsRecord(org.osid.type.Type participantSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.records.ParticipantSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(participantSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(participantSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record participant search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addParticipantRecord(org.osid.offering.records.ParticipantSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "participant record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
