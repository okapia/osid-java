//
// AbstractImmutableAwardEntry.java
//
//     Wraps a mutable AwardEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.awardentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AwardEntry</code> to hide modifiers. This
 *  wrapper provides an immutized AwardEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying awardEntry whose state changes are visible.
 */

public abstract class AbstractImmutableAwardEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.chronicle.AwardEntry {

    private final org.osid.course.chronicle.AwardEntry awardEntry;


    /**
     *  Constructs a new <code>AbstractImmutableAwardEntry</code>.
     *
     *  @param awardEntry the award entry to immutablize
     *  @throws org.osid.NullArgumentException <code>awardEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAwardEntry(org.osid.course.chronicle.AwardEntry awardEntry) {
        super(awardEntry);
        this.awardEntry = awardEntry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.awardEntry.getStudentId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.awardEntry.getStudent());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Award. </code> 
     *
     *  @return the award <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAwardId() {
        return (this.awardEntry.getAwardId());
    }


    /**
     *  Gets the <code> Award. </code> 
     *
     *  @return the award 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward()
        throws org.osid.OperationFailedException {

        return (this.awardEntry.getAward());
    }


    /**
     *  Gets the award date. 
     *
     *  @return the date awarded 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateAwarded() {
        return (this.awardEntry.getDateAwarded());
    }


    /**
     *  Tests if this award applies to a specific program. If <code> 
     *  hasProgram() </code> is <code> true, </code> <code> hasCourse() 
     *  </code> and <code> hasAssessment() </code> must be false. 
     *
     *  @return <code> true </code> if a program is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgram() {
        return (this.awardEntry.hasProgram());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.awardEntry.getProgramId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.awardEntry.getProgram());
    }


    /**
     *  Tests if this award applies to a specific course. If <code> 
     *  hasCourse() </code> is <code> true, </code> <code> hasProgram() 
     *  </code> and <code> hasAssessment() </code> must be <code> false. 
     *  </code> 
     *
     *  @return <code> true </code> if a course is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCourse() {
        return (this.awardEntry.hasCourse());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Course. </code> 
     *
     *  @return the course <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.awardEntry.getCourseId());
    }


    /**
     *  Gets the <code> Course. </code> 
     *
     *  @return the course 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.awardEntry.getCourse());
    }


    /**
     *  Tests if this award applies to a specific assessment. If <code> 
     *  hasAssessment() </code> is <code> true, </code> <code> hasCourse() 
     *  </code> and <code> hasProgram() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if an assessment is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAssessment() {
        return (this.awardEntry.hasAssessment());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Assessment. </code> 
     *
     *  @return the assessment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.awardEntry.getAssessmentId());
    }


    /**
     *  Gets the <code> Assessment. </code> 
     *
     *  @return the assessment 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.awardEntry.getAssessment());
    }


    /**
     *  Gets the award entry record corresponding to the given <code> 
     *  AwardEntry </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  awardEntryRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(awardEntryRecordType) </code> is <code> true </code> . 
     *
     *  @param  awardEntryRecordType the type of award entry record to 
     *          retrieve 
     *  @return the award entry record 
     *  @throws org.osid.NullArgumentException <code> awardEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(awardEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AwardEntryRecord getAwardEntryRecord(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.awardEntry.getAwardEntryRecord(awardEntryRecordType));
    }
}

