//
// AbstractIdentifiableBuilder.java
//
//     The Identifiable builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines the Identifiable builder.
 */

public abstract class AbstractIdentifiableBuilder<T extends AbstractIdentifiableBuilder<? extends T>>
    extends AbstractBuilder<T> {

    private final IdentifiableMiter identifiable;


    /**
     *  Creates a new <code>AbstractIdentifiableBuilder</code>.
     *
     *  @param identifiable an identifiable miter interface
     *  @throws org.osid.NullArgumentException
     *          <code>identifiable</code> is <code>null</code>
     */

    protected AbstractIdentifiableBuilder(IdentifiableMiter identifiable) {
        nullarg(identifiable, "identifiable");
        this.identifiable = identifiable;
        return;
    }


    /**
     *  Sets the <code> Id </code> associated with this instance of this OSID 
     *  object. 
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is 
     *          <code>null</code>
     */
    
    public T id(org.osid.id.Id id) {
        this.identifiable.setId(id);
        return (self());
    }


    /**
     *  Sets the current flag to <code>true</code>.
     *
     *  @return the builder
     */

    public T current() {
        this.identifiable.current();
        return (self());
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    public T stale() {
        this.identifiable.stale();
        return (self());
    }
}
