//
// BranchMiter.java
//
//     Defines a Branch miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.journaling.branch;


/**
 *  Defines a <code>Branch</code> miter for use with the builders.
 */

public interface BranchMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            org.osid.journaling.Branch {


    /**
     *  Sets the origin journal entry.
     *
     *  @param entry an origin journal entry
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public void setOriginJournalEntry(org.osid.journaling.JournalEntry entry);


    /**
     *  Sets the latest journal entry.
     *
     *  @param entry a latest journal entry
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public void setLatestJournalEntry(org.osid.journaling.JournalEntry entry);


    /**
     *  Adds a Branch record.
     *
     *  @param record a branch record
     *  @param recordType the type of branch record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addBranchRecord(org.osid.journaling.records.BranchRecord record, org.osid.type.Type recordType);
}       


