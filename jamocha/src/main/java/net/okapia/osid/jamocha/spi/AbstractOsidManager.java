//
// AbstractOsidManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// OnTapSolutions
// 27 June 2008
//
// Copyright (c) 2008 Massachusetts Institute of Technology.
// Copyright (c) 2010,2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This is a manager abstract class for handling common manager and
 *  profile methods. Objects extending this class can use various
 *  setters to supply the information. This abstract class implements
 *  both the <code>OsidManager</code> and
 *  <code>OsidProxyManager</code> interfaces.
 *
 *  Objects extending this class should use the following to set their
 *  information using the ServiceProvider.
 *
 *  The service messages, locale and proxy types are optional. This
 *  manager does not support journaling but the journaling methods can
 *  be overridden.
 */

public abstract class AbstractOsidManager
    implements org.osid.OsidManager,
	       org.osid.OsidProxyManager {

    private final net.okapia.osid.provider.ServiceProvider provider;
    private org.osid.OsidRuntimeManager runtime;
    private ManagerConfiguration configuration;
    private ManagerProfile profile;
    private ManagerLoader loader;


    /**
     *  Constructs a new <code>AbstractOsidManager</code>.
     *
     *  @param provider a service provider
     *  @throws org.osid.NullArgumentException <code>provider</code> is
     *          <code>null</code>
     */

    protected AbstractOsidManager(net.okapia.osid.provider.ServiceProvider provider) {
        nullarg(provider, "provider");

        provider.validate();
        this.provider = provider;

        this.profile  = new ManagerProfile();
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the time of 
     *  creation. 
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        this.runtime = runtime;
        this.configuration = new ManagerConfiguration(runtime);
        this.loader = new ManagerLoader(runtime);

        return;
    }


    /**
     *  Gets the runtime environment.
     *
     *  @return the runtime
     */

    protected org.osid.OsidRuntimeManager getRuntime() {
        return (this.runtime);
    }


    /**
     *  Sets the runtime environment.
     *
     *  @param runtime
     *  @throws org.osid.NullArgumentException <code>runtime</code> is null
     */

    protected void setRuntime(org.osid.OsidRuntimeManager runtime) {
        nullarg(runtime, "runtime");
        this.runtime = runtime;
        return;
    }


    /**
     *  Gets an identifier for this service implementation. The identifier is 
     *  unique among services but multiple instantiations of the same service 
     *  use the same <code> Id. </code> This identifier is the same identifier 
     *  used in managing OSID installations. 
     *
     *  @return the <code> Id </code> 
     *  @throws org.osid.IllegalStateException This manager has been closed. 
     */

    @OSID @Override
    public org.osid.id.Id getId() {
        return (this.provider.getServiceId());
    }


    /**
     *  Gets a display name for this service implementation. 
     *
     *  @return a display name 
     *  @throws org.osid.IllegalStateException This manager has been closed. 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayName() {
        return (this.provider.getServiceName());
    }


    /**
     *  Gets a description of this service implementation. 
     *
     *  @return a description 
     *  @throws org.osid.IllegalStateException This manager has been closed. 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDescription() {
        return (this.provider.getServiceDescription());
    }


    /**
     *  Gets the version of this service implementation. 
     *
     *  @return the version 
     *  @throws org.osid.IllegalStateException This manager has been closed. 
     */

    @OSID @Override
    public org.osid.installation.Version getVersion() {
        return (this.provider.getImplementationVersion());
    }


    /**
     *  Gets the date this service implementation was released. 
     *
     *  @return the release date 
     *  @throws org.osid.IllegalStateException This manager has been closed. 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReleaseDate() {
        return (this.provider.getReleaseDate());
    }


    /**
     *  Test for support of an OSID version. 
     *
     *  @param  version the version string to test 
     *  @return <code> true </code> if this manager supports the given 
     *          version, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOSIDVersion(org.osid.installation.Version version) {
        return (this.profile.supportsOSIDVersion(version));
    }


    /**
     *  Adds support for an OSID version.
     *
     *  @param version an OSID version
     *  @throws org.osid.NullargumentException {@code version} is
     *          {@code null}
     */

    protected void addOSIDVersion(org.osid.installation.Version version) {
        this.profile.addOSIDVersion(version);
        return;
    }


    /**
     *  Gets the locales supported in this service. 
     *
     *  @return list of locales supported 
     */

    @OSID @Override
    public org.osid.locale.LocaleList getLocales() {
        return (this.profile.getLocales());
    }


    /**
     *  Adds support for a locale.
     *
     *  @param locale the locale to add
     *  @throws org.osid.NullArgumentException <code>locale</code> is
     *          <code>null</code>
     */

    public void addLocale(org.osid.locale.Locale locale) {
        this.profile.addLocale(locale);
        return;
    }

    
    /**
     *  Test for support of a journaling rollback service. 
     *
     *  @return <code> true </code> if this manager supports the journal 
     *          rollback, <code> false </code> otherwise 
     */

    public boolean supportsJournalRollback() {
        return (false);
    }


    /**
     *  Test for support of a journal branching service. 
     *
     *  @return <code> true </code> if this manager supports the journal 
     *          branching, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalBranching() {
        return (false);
    }


    /**
     *  Gets the <code> Branch Id </code> representing this service branch. 
     *
     *  @return the branch <code> Id </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalBranching() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBranchId() {
        throw new org.osid.UnimplementedException("journal branching not supported");
    }


    /**
     *  Gets this service branch. 
     *
     *  @return the service branch 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalBranching() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.Branch getBranch()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("journal branching not supported");
    }


    /**
     *  Rolls back this service to a point in time. 
     *
     *  @param  rollbackTime the requested time 
     *  @return the journal entry corresponding to the actual state of this 
     *          service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalRollback() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry rollbackService(java.util.Date rollbackTime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        throw new org.osid.UnimplementedException("journal branching not supported");
    }


    /**
     *  Rolls back this service to a point in time. 
     *
     *  @param  rollbackTime the requested time 
     *  @param  proxy a proxy 
     *  @return the journal entry corresponding to the actual state of this 
     *          service 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalRollback() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry rollbackService(java.util.Date rollbackTime, 
                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("journal branching not supported");
    }


    /**
     *  Changes the service branch. 
     *
     *  @param  branchId the new service branch 
     *  @throws org.osid.NotFoundException <code> branchId </code> not found 
     *  @throws org.osid.NullArgumentException <code> branchId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalBranching() </code> is <code> false </code> 
     */

    @OSID @Override
    public void changeBranch(org.osid.id.Id branchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("journal branching not supported");
    }


    /**
     *  Changes the service branch. 
     *
     *  @param  branchId the new service branch 
     *  @param  proxy a proxy 
     *  @throws org.osid.NotFoundException <code> branchId </code> not found 
     *  @throws org.osid.NullArgumentException <code> branchId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalBranching() </code> is <code> false </code> 
     */

    @OSID @Override
    public void changeBranch(org.osid.id.Id branchId, 
                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("journal branching not supported");
    }


    /**
     *  Gets the proxy record <code> Types </code> supported in this
     *  service.  If no proxy manager is available, an empty list is
     *  returned.
     *
     *  @return list of proxy record types supported 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    @OSID @Override
    public org.osid.type.TypeList getProxyRecordTypes() {
        return (this.profile.getProxyRecordTypes());
    }


    /**
     *  Test for support of a proxy type. 
     *
     *  @param  proxyRecordType a proxy record type 
     *  @return <code> true </code> if this service supports the given proxy 
     *          record type, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proxyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProxyRecordType(org.osid.type.Type proxyRecordType) {
        return (this.profile.supportsProxyRecordType(proxyRecordType));
    }


    /**
     *  Adds support for a proxy record type.
     *
     *  @param type a proxy type to add
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void addProxyRecordType(org.osid.type.Type type) {
        this.profile.addProxyRecordType(type);
        return;
    }


    /**
     *  Removes support for a proxy record type.
     *
     *  @param type
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    protected void removeProxyRecordType(org.osid.type.Type type) {
        this.profile.removeProxyRecordType(type);
        return;
    }


    /**
     *  Gets the terms of usage with respect to this service
     *  implementation.
     *
     *  @return the license 
     *  @throws org.osid.IllegalStateException This manager has been closed.
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLicense() {
        return (this.provider.getProvider().getLicense());
    }


    /**
     *  Gets the <code> Resource Id </code> representing the provider
     *  of this service.
     *
     *  @return the provider <code> Id </code> 
     *  @throws org.osid.IllegalStateException This manager has been closed. 
     */

    @OSID @Override
    public org.osid.id.Id getProviderId() {
        return (this.provider.getProvider().getResource().getId());
    }


    /**
     *  Gets the provider of this service, expressed using the <code> Resource 
     *  </code> interface. 
     *
     *  @return the service provider resource 
     *  @throws org.osid.IllegalStateException This manager has been closed. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getProvider()
        throws org.osid.OperationFailedException {

        return (this.provider.getProvider().getResource());
    }


    /**
     *  Gets the branding asset Ids, such as an image or logo,
     *  expressed using the <code> Asset </code> interface.
     *
     *  @return a list of asset Ids
     */

    @OSID @Override
    public org.osid.id.IdList getBrandingIds() {
        try {
            return (new net.okapia.osid.jamocha.adapter.converter.repository.asset.AssetToIdList(getBranding()));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a branding, such as an image or logo, expressed using the
     *  <code>Asset</code> interface.
     *
     *  @return a list of assets 
     *  @throws org.osid.IllegalStateException This manager has been closed.
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getBranding()
        throws org.osid.OperationFailedException {
        
        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.provider.getProvider().getBranding()));
    }


    /**
     *  Instantiates an OsidManager.
     *
     *  @param osid the OSID to load
     *  @param  implClassName the name of the implementation 
     *  @return the OsidManager
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.NullArgumentException <code> implClassName </code> or 
     *          <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    protected org.osid.OsidManager getOsidManager(org.osid.OSID osid, String implClassName)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.loader == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.loader.getOsidManager(osid, implClassName));
    }


    /**
     *  Instantiates an OsidProxyManager.
     *
     *  @param osid the OSID to load
     *  @param  implClassName the name of the implementation 
     *  @return the OsidManager
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.NullArgumentException <code> implClassName </code> or 
     *          <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    protected org.osid.OsidProxyManager getOsidProxyManager(org.osid.OSID osid, String implClassName)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.loader == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.loader.getOsidProxyManager(osid, implClassName));
    }


    /**
     *  Gets a <code> Value </code> for the given parameter <code>
     *  Id. </code> If more than one value exists for the given
     *  parameter, the most preferred value is returned. This method
     *  can be used as a convenience when only one value is
     *  expected. <code> getValuesByParameters() </code> should be
     *  used for getting all the active values.
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value 
     *  @throws org.osid.ConfigurationErrorException the <code>
     *          parameterId </code> not found or no value available
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.configuration.Value getConfigurationValue(org.osid.id.Id parameterId)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.configuration == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.configuration.getConfigurationValue(parameterId));
    }


    /**
     *  Gets all the <code> Values </code> for the given parameter
     *  <code> Id.  </code>
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.ConfigurationErrorException the <code>
     *          parameterId </code> not found
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.configuration.ValueList getConfigurationValues(org.osid.id.Id parameterId)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.configuration == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.configuration.getValuesByParameter(parameterId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        return;
    }
}
