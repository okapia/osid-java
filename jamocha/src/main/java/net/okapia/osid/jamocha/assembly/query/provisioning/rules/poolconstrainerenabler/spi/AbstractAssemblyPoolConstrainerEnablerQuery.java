//
// AbstractAssemblyPoolConstrainerEnablerQuery.java
//
//     A PoolConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.poolconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PoolConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyPoolConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.provisioning.rules.PoolConstrainerEnablerQuery,
               org.osid.provisioning.rules.PoolConstrainerEnablerQueryInspector,
               org.osid.provisioning.rules.PoolConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPoolConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPoolConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the pool constrainer. 
     *
     *  @param  poolConstrainerId the pool constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPoolConstrainerId(org.osid.id.Id poolConstrainerId, 
                                            boolean match) {
        getAssembler().addIdTerm(getRuledPoolConstrainerIdColumn(), poolConstrainerId, match);
        return;
    }


    /**
     *  Clears the pool constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPoolConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledPoolConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the pool constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledPoolConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledPoolConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledPoolConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledPoolConstrainerIdColumn() {
        return ("ruled_pool_constrainer_id");
    }


    /**
     *  Tests if a <code> PoolConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPoolConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the pool constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPoolConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerQuery getRuledPoolConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPoolConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any pool constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any pool 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no pool constrainers 
     */

    @OSID @Override
    public void matchAnyRuledPoolConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledPoolConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the pool constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledPoolConstrainerTerms() {
        getAssembler().clearTerms(getRuledPoolConstrainerColumn());
        return;
    }


    /**
     *  Gets the pool constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerQueryInspector[] getRuledPoolConstrainerTerms() {
        return (new org.osid.provisioning.rules.PoolConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledPoolConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledPoolConstrainerColumn() {
        return ("ruled_pool_constrainer");
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this poolConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  poolConstrainerEnablerRecordType a pool constrainer enabler record type 
     *  @return <code>true</code> if the poolConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolConstrainerEnablerRecordType) {
        for (org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  poolConstrainerEnablerRecordType the pool constrainer enabler record type 
     *  @return the pool constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryRecord getPoolConstrainerEnablerQueryRecord(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  poolConstrainerEnablerRecordType the pool constrainer enabler record type 
     *  @return the pool constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord getPoolConstrainerEnablerQueryInspectorRecord(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(poolConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param poolConstrainerEnablerRecordType the pool constrainer enabler record type
     *  @return the pool constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchOrderRecord getPoolConstrainerEnablerSearchOrderRecord(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(poolConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this pool constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolConstrainerEnablerQueryRecord the pool constrainer enabler query record
     *  @param poolConstrainerEnablerQueryInspectorRecord the pool constrainer enabler query inspector
     *         record
     *  @param poolConstrainerEnablerSearchOrderRecord the pool constrainer enabler search order record
     *  @param poolConstrainerEnablerRecordType pool constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerQueryRecord</code>,
     *          <code>poolConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>poolConstrainerEnablerSearchOrderRecord</code> or
     *          <code>poolConstrainerEnablerRecordTypepoolConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addPoolConstrainerEnablerRecords(org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryRecord poolConstrainerEnablerQueryRecord, 
                                      org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord poolConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchOrderRecord poolConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type poolConstrainerEnablerRecordType) {

        addRecordType(poolConstrainerEnablerRecordType);

        nullarg(poolConstrainerEnablerQueryRecord, "pool constrainer enabler query record");
        nullarg(poolConstrainerEnablerQueryInspectorRecord, "pool constrainer enabler query inspector record");
        nullarg(poolConstrainerEnablerSearchOrderRecord, "pool constrainer enabler search odrer record");

        this.queryRecords.add(poolConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(poolConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(poolConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
