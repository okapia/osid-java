//
// AbstractNodeFoundryHierarchySession.java
//
//     Defines a Foundry hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a foundry hierarchy session for delivering a hierarchy
 *  of foundries using the FoundryNode interface.
 */

public abstract class AbstractNodeFoundryHierarchySession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractFoundryHierarchySession
    implements org.osid.resourcing.FoundryHierarchySession {

    private java.util.Collection<org.osid.resourcing.FoundryNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root foundry <code> Ids </code> in this hierarchy.
     *
     *  @return the root foundry <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootFoundryIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.resourcing.foundrynode.FoundryNodeToIdList(this.roots));
    }


    /**
     *  Gets the root foundries in the foundry hierarchy. A node
     *  with no parents is an orphan. While all foundry <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root foundries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getRootFoundries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resourcing.foundrynode.FoundryNodeToFoundryList(new net.okapia.osid.jamocha.resourcing.foundrynode.ArrayFoundryNodeList(this.roots)));
    }


    /**
     *  Adds a root foundry node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootFoundry(org.osid.resourcing.FoundryNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root foundry nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootFoundries(java.util.Collection<org.osid.resourcing.FoundryNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root foundry node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootFoundry(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.resourcing.FoundryNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Foundry </code> has any parents. 
     *
     *  @param  foundryId a foundry <code> Id </code> 
     *  @return <code> true </code> if the foundry has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentFoundries(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getFoundryNode(foundryId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  foundry.
     *
     *  @param  id an <code> Id </code> 
     *  @param  foundryId the <code> Id </code> of a foundry 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> foundryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> foundryId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfFoundry(org.osid.id.Id id, org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.FoundryNodeList parents = getFoundryNode(foundryId).getParentFoundryNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextFoundryNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given foundry. 
     *
     *  @param  foundryId a foundry <code> Id </code> 
     *  @return the parent <code> Ids </code> of the foundry 
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentFoundryIds(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resourcing.foundry.FoundryToIdList(getParentFoundries(foundryId)));
    }


    /**
     *  Gets the parents of the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> to query 
     *  @return the parents of the foundry 
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getParentFoundries(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resourcing.foundrynode.FoundryNodeToFoundryList(getFoundryNode(foundryId).getParentFoundryNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  foundry.
     *
     *  @param  id an <code> Id </code> 
     *  @param  foundryId the Id of a foundry 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> foundryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfFoundry(org.osid.id.Id id, org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfFoundry(id, foundryId)) {
            return (true);
        }

        try (org.osid.resourcing.FoundryList parents = getParentFoundries(foundryId)) {
            while (parents.hasNext()) {
                if (isAncestorOfFoundry(id, parents.getNextFoundry().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a foundry has any children. 
     *
     *  @param  foundryId a foundry <code> Id </code> 
     *  @return <code> true </code> if the <code> foundryId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildFoundries(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFoundryNode(foundryId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  foundry.
     *
     *  @param  id an <code> Id </code> 
     *  @param foundryId the <code> Id </code> of a 
     *         foundry
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> foundryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> foundryId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfFoundry(org.osid.id.Id id, org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfFoundry(foundryId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  foundry.
     *
     *  @param  foundryId the <code> Id </code> to query 
     *  @return the children of the foundry 
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildFoundryIds(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resourcing.foundry.FoundryToIdList(getChildFoundries(foundryId)));
    }


    /**
     *  Gets the children of the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> to query 
     *  @return the children of the foundry 
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getChildFoundries(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resourcing.foundrynode.FoundryNodeToFoundryList(getFoundryNode(foundryId).getChildFoundryNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  foundry.
     *
     *  @param  id an <code> Id </code> 
     *  @param foundryId the <code> Id </code> of a 
     *         foundry
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> foundryId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfFoundry(org.osid.id.Id id, org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfFoundry(foundryId, id)) {
            return (true);
        }

        try (org.osid.resourcing.FoundryList children = getChildFoundries(foundryId)) {
            while (children.hasNext()) {
                if (isDescendantOfFoundry(id, children.getNextFoundry().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  foundry.
     *
     *  @param  foundryId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified foundry node 
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getFoundryNodeIds(org.osid.id.Id foundryId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.resourcing.foundrynode.FoundryNodeToNode(getFoundryNode(foundryId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given foundry.
     *
     *  @param  foundryId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified foundry node 
     *  @throws org.osid.NotFoundException <code> foundryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> foundryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryNode getFoundryNodes(org.osid.id.Id foundryId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFoundryNode(foundryId));
    }


    /**
     *  Closes this <code>FoundryHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a foundry node.
     *
     *  @param foundryId the id of the foundry node
     *  @throws org.osid.NotFoundException <code>foundryId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>foundryId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.resourcing.FoundryNode getFoundryNode(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(foundryId, "foundry Id");
        for (org.osid.resourcing.FoundryNode foundry : this.roots) {
            if (foundry.getId().equals(foundryId)) {
                return (foundry);
            }

            org.osid.resourcing.FoundryNode r = findFoundry(foundry, foundryId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(foundryId + " is not found");
    }


    protected org.osid.resourcing.FoundryNode findFoundry(org.osid.resourcing.FoundryNode node, 
                                                          org.osid.id.Id foundryId) 
        throws  org.osid.OperationFailedException {

        try (org.osid.resourcing.FoundryNodeList children = node.getChildFoundryNodes()) {
            while (children.hasNext()) {
                org.osid.resourcing.FoundryNode foundry = children.getNextFoundryNode();
                if (foundry.getId().equals(foundryId)) {
                    return (foundry);
                }
                
                foundry = findFoundry(foundry, foundryId);
                if (foundry != null) {
                    return (foundry);
                }
            }
        }

        return (null);
    }
}
