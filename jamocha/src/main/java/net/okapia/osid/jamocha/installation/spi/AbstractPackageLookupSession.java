//
// AbstractPackageLookupSession.java
//
//    A starter implementation framework for providing a Package
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Package
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPackages(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPackageLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.installation.PackageLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private boolean normalized    = false;
    private org.osid.installation.Depot depot = new net.okapia.osid.jamocha.nil.installation.depot.UnknownDepot();
    

    /**
     *  Gets the <code>Depot/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Depot Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDepotId() {
        return (this.depot.getId());
    }


    /**
     *  Gets the <code>Depot</code> associated with this 
     *  session.
     *
     *  @return the <code>Depot</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.depot);
    }


    /**
     *  Sets the <code>Depot</code>.
     *
     *  @param  depot the depot for this session
     *  @throws org.osid.NullArgumentException <code>depot</code>
     *          is <code>null</code>
     */

    protected void setDepot(org.osid.installation.Depot depot) {
        nullarg(depot, "depot");
        this.depot = depot;
        return;
    }


    /**
     *  Tests if this user can perform <code>Package</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPackages() {
        return (true);
    }


    /**
     *  A complete view of the <code>Package</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePackageView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Package</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPackageView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include packages in depots which are children
     *  of this depot in the depot hierarchy.
     */

    @OSID @Override
    public void useFederatedDepotView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this depot only.
     */

    @OSID @Override
    public void useIsolatedDepotView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  The returns from the lookup methods may omit multiple versions
     *  of the same package.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.normalized = true;
        return;
    }


    /**
     *  All versions of the same package are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.normalized = false;
        return;
    }
        
     
    /**
     *  Tests if a normalized or denormalized view is set.
     *
     *  @return <code>true</code> if normalized</code>,
     *          <code>false</code> if denormalized
     */

    protected boolean isNormalized() {
        return (this.normalized);
    }


    /**
     *  Gets the <code>Package</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Package</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Package</code> and
     *  retained for compatibility.
     *
     *  @param  pkgId <code>Id</code> of the
     *          <code>Package</code>
     *  @return the package
     *  @throws org.osid.NotFoundException <code>pkgId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pkgId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage(org.osid.id.Id pkgId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.installation.PackageList packages = getPackages()) {
            while (packages.hasNext()) {
                org.osid.installation.Package pkg = packages.getNextPackage();
                if (pkg.getId().equals(pkgId)) {
                    return (pkg);
                }
            }
        } 

        throw new org.osid.NotFoundException(pkgId + " not found");
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  packages specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Packages</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPackages()</code>.
     *
     *  @param  pkgIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>pkgIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByIds(org.osid.id.IdList pkgIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.installation.Package> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = pkgIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPackage(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("package " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.installation.pkg.LinkedPackageList(ret));
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  package genus <code>Type</code> which does not include
     *  packages of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPackages()</code>.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.installation.pkg.PackageGenusFilterList(getPackages(), pkgGenusType));
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  package genus <code>Type</code> and include any additional
     *  packages with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPackages()</code>.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByParentGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPackagesByGenusType(pkgGenusType));
    }


    /**
     *  Gets a <code>PackageList</code> containing the given
     *  package record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPackages()</code>.
     *
     *  @param  pkgRecordType a pkg record type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByRecordType(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.installation.pkg.PackageRecordFilterList(getPackages(), pkgRecordType));
    }


    /**
     *  Gets a <code>PackageList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known packages or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  packages that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Package</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.installation.pkg.PackageProviderFilterList(getPackages(), resourceId));
    }


    /**
     *  Gets a list of packages depending on the given package. 
     *
     *  @param  packageId an <code>Id</code> of a <code>Package</code> 
     *  @return list of package dependents 
     *  @throws org.osid.NotFoundException <code>packageId</code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code>packageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getDependentPackages(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.installation.Package> ret = new java.util.ArrayList<>();

        try (org.osid.installation.PackageList packages = getPackages()) {
            while (packages.hasNext()) {
                org.osid.installation.Package pkg = packages.getNextPackage();
                try (org.osid.id.IdList ids = pkg.getDependencyIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(packageId)) {
                            ret.add(pkg);
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.installation.pkg.LinkedPackageList(ret));
    }


    /**
     *  Gets a list of packages in the specified package version chain. 
     *
     *  @param  packageId an <code>Id</code> of a <code>Package</code> 
     *  @return list of dependencies 
     *  @throws org.osid.NotFoundException <code>packageId</code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code>packageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.installation.PackageList getPackageVersions(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets all <code>Packages</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Packages</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.installation.PackageList getPackages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the package list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of packages
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.installation.PackageList filterPackagesOnViews(org.osid.installation.PackageList list)
        throws org.osid.OperationFailedException {

        org.osid.installation.PackageList ret = list;

        return (ret);
    }
}
