//
// MutableIndexedMapProxyValueEnablerLookupSession
//
//    Implements a ValueEnabler lookup service backed by a collection of
//    valueEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules;


/**
 *  Implements a ValueEnabler lookup service backed by a collection of
 *  valueEnablers. The value enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some valueEnablers may be compatible
 *  with more types than are indicated through these valueEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of value enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyValueEnablerLookupSession
    extends net.okapia.osid.jamocha.core.configuration.rules.spi.AbstractIndexedMapValueEnablerLookupSession
    implements org.osid.configuration.rules.ValueEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyValueEnablerLookupSession} with
     *  no value enabler.
     *
     *  @param configuration the configuration
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyValueEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                       org.osid.proxy.Proxy proxy) {
        setConfiguration(configuration);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyValueEnablerLookupSession} with
     *  a single value enabler.
     *
     *  @param configuration the configuration
     *  @param  valueEnabler an value enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code valueEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyValueEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                       org.osid.configuration.rules.ValueEnabler valueEnabler, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putValueEnabler(valueEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyValueEnablerLookupSession} using
     *  an array of value enablers.
     *
     *  @param configuration the configuration
     *  @param  valueEnablers an array of value enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code valueEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyValueEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                       org.osid.configuration.rules.ValueEnabler[] valueEnablers, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putValueEnablers(valueEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyValueEnablerLookupSession} using
     *  a collection of value enablers.
     *
     *  @param configuration the configuration
     *  @param  valueEnablers a collection of value enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code valueEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyValueEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                       java.util.Collection<? extends org.osid.configuration.rules.ValueEnabler> valueEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(configuration, proxy);
        putValueEnablers(valueEnablers);
        return;
    }

    
    /**
     *  Makes a {@code ValueEnabler} available in this session.
     *
     *  @param  valueEnabler a value enabler
     *  @throws org.osid.NullArgumentException {@code valueEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putValueEnabler(org.osid.configuration.rules.ValueEnabler valueEnabler) {
        super.putValueEnabler(valueEnabler);
        return;
    }


    /**
     *  Makes an array of value enablers available in this session.
     *
     *  @param  valueEnablers an array of value enablers
     *  @throws org.osid.NullArgumentException {@code valueEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putValueEnablers(org.osid.configuration.rules.ValueEnabler[] valueEnablers) {
        super.putValueEnablers(valueEnablers);
        return;
    }


    /**
     *  Makes collection of value enablers available in this session.
     *
     *  @param  valueEnablers a collection of value enablers
     *  @throws org.osid.NullArgumentException {@code valueEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putValueEnablers(java.util.Collection<? extends org.osid.configuration.rules.ValueEnabler> valueEnablers) {
        super.putValueEnablers(valueEnablers);
        return;
    }


    /**
     *  Removes a ValueEnabler from this session.
     *
     *  @param valueEnablerId the {@code Id} of the value enabler
     *  @throws org.osid.NullArgumentException {@code valueEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeValueEnabler(org.osid.id.Id valueEnablerId) {
        super.removeValueEnabler(valueEnablerId);
        return;
    }    
}
