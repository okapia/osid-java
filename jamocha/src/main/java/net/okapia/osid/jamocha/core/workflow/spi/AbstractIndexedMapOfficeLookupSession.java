//
// AbstractIndexedMapOfficeLookupSession.java
//
//    A simple framework for providing an Office lookup service
//    backed by a fixed collection of offices with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Office lookup service backed by a
 *  fixed collection of offices. The offices are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some offices may be compatible
 *  with more types than are indicated through these office
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Offices</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOfficeLookupSession
    extends AbstractMapOfficeLookupSession
    implements org.osid.workflow.OfficeLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.Office> officesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.Office>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.Office> officesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.Office>());


    /**
     *  Makes an <code>Office</code> available in this session.
     *
     *  @param  office an office
     *  @throws org.osid.NullArgumentException <code>office<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOffice(org.osid.workflow.Office office) {
        super.putOffice(office);

        this.officesByGenus.put(office.getGenusType(), office);
        
        try (org.osid.type.TypeList types = office.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.officesByRecord.put(types.getNextType(), office);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an office from this session.
     *
     *  @param officeId the <code>Id</code> of the office
     *  @throws org.osid.NullArgumentException <code>officeId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOffice(org.osid.id.Id officeId) {
        org.osid.workflow.Office office;
        try {
            office = getOffice(officeId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.officesByGenus.remove(office.getGenusType());

        try (org.osid.type.TypeList types = office.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.officesByRecord.remove(types.getNextType(), office);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOffice(officeId);
        return;
    }


    /**
     *  Gets an <code>OfficeList</code> corresponding to the given
     *  office genus <code>Type</code> which does not include
     *  offices of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known offices or an error results. Otherwise,
     *  the returned list may contain only those offices that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  officeGenusType an office genus type 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByGenusType(org.osid.type.Type officeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.office.ArrayOfficeList(this.officesByGenus.get(officeGenusType)));
    }


    /**
     *  Gets an <code>OfficeList</code> containing the given
     *  office record <code>Type</code>. In plenary mode, the
     *  returned list contains all known offices or an error
     *  results. Otherwise, the returned list may contain only those
     *  offices that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  officeRecordType an office record type 
     *  @return the returned <code>office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByRecordType(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.office.ArrayOfficeList(this.officesByRecord.get(officeRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.officesByGenus.clear();
        this.officesByRecord.clear();

        super.close();

        return;
    }
}
