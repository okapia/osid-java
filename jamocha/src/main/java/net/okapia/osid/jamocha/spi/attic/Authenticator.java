//
// Authenticator
//
//     A means to proxy authentication data through fixed classes.
//
//
// Tom Coppeto
// OnTapSolutions
// 10 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha;


/**
 *  The <code>Authenticator</code> works with the
 *  <code>AbstractOsidSession</code> to optionally outsource
 *  authentication data available in the OSID Sessions.  
 */

public interface Authenticator {

    
    /**
     *  This method will be called for
     *  <code>OsidSession.isAuthenticated()</code>.
     *
     *  @return <code> true </code> if valid authentication credentials exist, 
     *          <code> false </code> otherwise 
     */

    public boolean isAuthenticated();
    

    /**
     *  This method will be called for
     *  <code>OsidSession.getAuthenticatedAgents()</code>.
     *
     *  Gets the authenticated identities used by this service to give the 
     *  user feedback as to which of the Agent identitites are actively being 
     *  used on the user's behalf. 
     *
     *  @return the list of authenticated Agents 
     */

    public org.osid.authentication.AgentList getAuthenticatedAgents();

    
    /**
     * This method is called during session close to release any
     * resources or stop runing threads spawned by the object owner.
     */

    public void close(); 
}