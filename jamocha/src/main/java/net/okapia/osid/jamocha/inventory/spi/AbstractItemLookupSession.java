//
// AbstractItemLookupSession.java
//
//    A starter implementation framework for providing an Item
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Item
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getItems(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractItemLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.ItemLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();
    
    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Item</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupItems() {
        return (true);
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeItemView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryItemView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Item</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Item</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Item</code> and
     *  retained for compatibility.
     *
     *  @param  itemId <code>Id</code> of the
     *          <code>Item</code>
     *  @return the item
     *  @throws org.osid.NotFoundException <code>itemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>itemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.ItemList items = getItems()) {
            while (items.hasNext()) {
                org.osid.inventory.Item item = items.getNextItem();
                if (item.getId().equals(itemId)) {
                    return (item);
                }
            }
        } 

        throw new org.osid.NotFoundException(itemId + " not found");
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Items</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getItems()</code>.
     *
     *  @param  itemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>itemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.Item> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = itemIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getItem(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("item " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.item.LinkedItemList(ret));
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> which does not include
     *  items of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getItems()</code>.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.item.ItemGenusFilterList(getItems(), itemGenusType));
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> and include any additional
     *  items with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getItems()</code>.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getItemsByGenusType(itemGenusType));
    }


    /**
     *  Gets an <code>ItemList</code> containing the given
     *  item record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getItems()</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.item.ItemRecordFilterList(getItems(), itemRecordType));
    }


    /**
     *  Gets an <code>ItemList</code> for the given stock. In plenary
     *  mode, the returned list contains all known items or an error
     *  results.  Otherwise, the returned list may contain only those
     *  items that are accessible through this session.
     *
     *  @param  stockId a stock <code>Id</code> 
     *  @return the returned <code>Item</code> list 
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilterList(new StockFilter(stockId), getItems()));
    }


    /**
     *  Gets an <code>ItemList</code> for the given property tag. In
     *  plenary mode, the returned list contains all known items or an
     *  error results.  Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  property a property number 
     *  @return the returned <code>Item</code> list 
     *  @throws org.osid.NullArgumentException <code>property</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByPropertyTag(String property)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilterList(new TagFilter(property), getItems()));
    }        


    /**
     *  Gets an <code>ItemList</code> for the given serial number. In
     *  plenary mode, the returned list contains all known items or an
     *  error results. Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  serialNumber a serial number 
     *  @return the returned <code>Itemd </code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>serialNumber</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsBySerialNumber(String serialNumber)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilterList(new SerialFilter(serialNumber), getItems()));
    }


    /**
     *  Gets an <code>ItemList</code> immediately contained within the
     *  given item. In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through this
     *  session.
     *
     *  @param  itemId an item <code>Id</code> 
     *  @return the returned <code>Item</code> list 
     *  @throws org.osid.NullArgumentException <code>itemId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemParts(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilterList(new ItemPartFilter(itemId), getItems()));
    }


    /**
     *  Gets all <code>Items</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Items</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inventory.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the item list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of items
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inventory.ItemList filterItemsOnViews(org.osid.inventory.ItemList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class StockFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilter {
         
        private final org.osid.id.Id stockId;
         
         
        /**
         *  Constructs a new <code>StockFilter</code>.
         *
         *  @param stockId the stock to filter
         *  @throws org.osid.NullArgumentException
         *          <code>stockId</code> is <code>null</code>
         */
        
        public StockFilter(org.osid.id.Id stockId) {
            nullarg(stockId, "stock Id");
            this.stockId = stockId;
            return;
        }

         
        /**
         *  Used by the ItemFilterList to filter the 
         *  item list based on stock.
         *
         *  @param item the item
         *  @return <code>true</code> to pass the item,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Item item) {
            return (item.getStockId().equals(this.stockId));
        }
    }


    public static class TagFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilter {
         
        private final String tag;
         
         
        /**
         *  Constructs a new <code>TagFilter</code>.
         *
         *  @param tag the tag to filter
         *  @throws org.osid.NullArgumentException
         *          <code>tag</code> is <code>null</code>
         */
        
        public TagFilter(String tag) {
            nullarg(tag, "tag");
            this.tag = tag;
            return;
        }

         
        /**
         *  Used by the ItemFilterList to filter the item list based
         *  on tag.
         *
         *  @param item the item
         *  @return <code>true</code> to pass the item,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Item item) {
            return (item.getPropertyTag().equals(this.tag));
        }
    }


    public static class SerialFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilter {
         
        private final String serial;
         
         
        /**
         *  Constructs a new <code>SerialFilter</code>.
         *
         *  @param serial the serial to filter
         *  @throws org.osid.NullArgumentException
         *          <code>serial</code> is <code>null</code>
         */
        
        public SerialFilter(String serial) {
            nullarg(serial, "serial");
            this.serial = serial;
            return;
        }

         
        /**
         *  Used by the ItemFilterList to filter the item list based
         *  on serial number.
         *
         *  @param item the item
         *  @return <code>true</code> to pass the item,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Item item) {
            return (item.getSerialNumber().equals(this.serial));
        }
    }


    public static class ItemPartFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.item.ItemFilter {
         
        private final org.osid.id.Id itemId;
         
         
        /**
         *  Constructs a new <code>ItemPartFilter</code>.
         *
         *  @param itemId the parent item to filter
         *  @throws org.osid.NullArgumentException
         *          <code>itemId</code> is <code>null</code>
         */
        
        public ItemPartFilter(org.osid.id.Id itemId) {
            nullarg(itemId, "item Id");
            this.itemId = itemId;
            return;
        }

         
        /**
         *  Used by the ItemFilterList to filter the 
         *  item list based on parent item.
         *
         *  @param item the item
         *  @return <code>true</code> to pass the item,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Item item) {
            return (item.getItemId().equals(this.itemId));
        }
    }
}
