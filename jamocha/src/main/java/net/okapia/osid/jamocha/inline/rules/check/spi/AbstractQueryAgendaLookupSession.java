//
// AbstractQueryAgendaLookupSession.java
//
//    An inline adapter that maps an AgendaLookupSession to
//    an AgendaQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AgendaLookupSession to
 *  an AgendaQuerySession.
 */

public abstract class AbstractQueryAgendaLookupSession
    extends net.okapia.osid.jamocha.rules.check.spi.AbstractAgendaLookupSession
    implements org.osid.rules.check.AgendaLookupSession {

    private final org.osid.rules.check.AgendaQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAgendaLookupSession.
     *
     *  @param querySession the underlying agenda query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAgendaLookupSession(org.osid.rules.check.AgendaQuerySession querySession) {
        nullarg(querySession, "agenda query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Engine</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.session.getEngineId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this 
     *  session.
     *
     *  @return the <code>Engine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getEngine());
    }


    /**
     *  Tests if this user can perform <code>Agenda</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAgendas() {
        return (this.session.canSearchAgendas());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agendas in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.session.useFederatedEngineView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.session.useIsolatedEngineView();
        return;
    }
    
     
    /**
     *  Gets the <code>Agenda</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Agenda</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Agenda</code> and
     *  retained for compatibility.
     *
     *  @param  agendaId <code>Id</code> of the
     *          <code>Agenda</code>
     *  @return the agenda
     *  @throws org.osid.NotFoundException <code>agendaId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>agendaId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getAgenda(org.osid.id.Id agendaId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.AgendaQuery query = getQuery();
        query.matchId(agendaId, true);
        org.osid.rules.check.AgendaList agendas = this.session.getAgendasByQuery(query);
        if (agendas.hasNext()) {
            return (agendas.getNextAgenda());
        } 
        
        throw new org.osid.NotFoundException(agendaId + " not found");
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agendas specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Agendas</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  agendaIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>agendaIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByIds(org.osid.id.IdList agendaIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.AgendaQuery query = getQuery();

        try (org.osid.id.IdList ids = agendaIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAgendasByQuery(query));
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  agenda genus <code>Type</code> which does not include
     *  agendas of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.AgendaQuery query = getQuery();
        query.matchGenusType(agendaGenusType, true);
        return (this.session.getAgendasByQuery(query));
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  agenda genus <code>Type</code> and include any additional
     *  agendas with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByParentGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.AgendaQuery query = getQuery();
        query.matchParentGenusType(agendaGenusType, true);
        return (this.session.getAgendasByQuery(query));
    }


    /**
     *  Gets an <code>AgendaList</code> containing the given
     *  agenda record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agendaRecordType an agenda record type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByRecordType(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.AgendaQuery query = getQuery();
        query.matchRecordType(agendaRecordType, true);
        return (this.session.getAgendasByQuery(query));
    }

    
    /**
     *  Gets all <code>Agendas</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Agendas</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendas()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.rules.check.AgendaQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAgendasByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.rules.check.AgendaQuery getQuery() {
        org.osid.rules.check.AgendaQuery query = this.session.getAgendaQuery();
        return (query);
    }
}
