//
// AbstractPeriod.java
//
//     Defines a Period builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.period.spi;


/**
 *  Defines a <code>Period</code> builder.
 */

public abstract class AbstractPeriodBuilder<T extends AbstractPeriodBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.billing.period.PeriodMiter period;


    /**
     *  Constructs a new <code>AbstractPeriodBuilder</code>.
     *
     *  @param period the period to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPeriodBuilder(net.okapia.osid.jamocha.builder.billing.period.PeriodMiter period) {
        super(period);
        this.period = period;
        return;
    }


    /**
     *  Builds the period.
     *
     *  @return the new period
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.billing.Period build() {
        (new net.okapia.osid.jamocha.builder.validator.billing.period.PeriodValidator(getValidations())).validate(this.period);
        return (new net.okapia.osid.jamocha.builder.billing.period.ImmutablePeriod(this.period));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the period miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.billing.period.PeriodMiter getMiter() {
        return (this.period);
    }


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    public T displayLabel(org.osid.locale.DisplayText label) {
        getMiter().setDisplayLabel(label);
        return (self());
    }


    /**
     *  Sets the open date.
     *
     *  @param date an open date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T openDate(org.osid.calendaring.DateTime date) {
        getMiter().setOpenDate(date);
        return (self());
    }


    /**
     *  Sets the close date.
     *
     *  @param date a close date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T closeDate(org.osid.calendaring.DateTime date) {
        getMiter().setCloseDate(date);
        return (self());
    }


    /**
     *  Sets the billing date.
     *
     *  @param date a billing date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T billingDate(org.osid.calendaring.DateTime date) {
        getMiter().setBillingDate(date);
        return (self());
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T dueDate(org.osid.calendaring.DateTime date) {
        getMiter().setDueDate(date);
        return (self());
    }


    /**
     *  Adds a Period record.
     *
     *  @param record a period record
     *  @param recordType the type of period record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.billing.records.PeriodRecord record, org.osid.type.Type recordType) {
        getMiter().addPeriodRecord(record, recordType);
        return (self());
    }
}       


