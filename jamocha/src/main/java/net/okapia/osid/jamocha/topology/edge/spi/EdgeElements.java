//
// EdgeElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.edge.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class EdgeElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the EdgeElement Id.
     *
     *  @return the edge element Id
     */

    public static org.osid.id.Id getEdgeEntityId() {
        return (makeEntityId("osid.topology.Edge"));
    }


    /**
     *  Gets the Cost element Id.
     *
     *  @return the Cost element Id
     */

    public static org.osid.id.Id getCost() {
        return (makeElementId("osid.topology.edge.Cost"));
    }


    /**
     *  Gets the Distance element Id.
     *
     *  @return the Distance element Id
     */

    public static org.osid.id.Id getDistance() {
        return (makeElementId("osid.topology.edge.Distance"));
    }


    /**
     *  Gets the SourceNodeId element Id.
     *
     *  @return the SourceNodeId element Id
     */

    public static org.osid.id.Id getSourceNodeId() {
        return (makeElementId("osid.topology.edge.SourceNodeId"));
    }


    /**
     *  Gets the SourceNode element Id.
     *
     *  @return the SourceNode element Id
     */

    public static org.osid.id.Id getSourceNode() {
        return (makeElementId("osid.topology.edge.SourceNode"));
    }


    /**
     *  Gets the DestinationNodeId element Id.
     *
     *  @return the DestinationNodeId element Id
     */

    public static org.osid.id.Id getDestinationNodeId() {
        return (makeElementId("osid.topology.edge.DestinationNodeId"));
    }


    /**
     *  Gets the DestinationNode element Id.
     *
     *  @return the DestinationNode element Id
     */

    public static org.osid.id.Id getDestinationNode() {
        return (makeElementId("osid.topology.edge.DestinationNode"));
    }


    /**
     *  Gets the SameNode element Id.
     *
     *  @return the SameNode element Id
     */

    public static org.osid.id.Id getSameNode() {
        return (makeQueryElementId("osid.topology.edge.SameNode"));
    }


    /**
     *  Gets the Directional element Id.
     *
     *  @return the Directional element Id
     */

    public static org.osid.id.Id getDirectional() {
        return (makeElementId("osid.topology.edge.Directional"));
    }


    /**
     *  Gets the BiDirectional element Id.
     *
     *  @return the BiDirectional element Id
     */

    public static org.osid.id.Id getBiDirectional() {
        return (makeElementId("osid.topology.edge.BiDirectional"));
    }


    /**
     *  Gets the GraphId element Id.
     *
     *  @return the GraphId element Id
     */

    public static org.osid.id.Id getGraphId() {
        return (makeQueryElementId("osid.topology.edge.GraphId"));
    }


    /**
     *  Gets the Graph element Id.
     *
     *  @return the Graph element Id
     */

    public static org.osid.id.Id getGraph() {
        return (makeQueryElementId("osid.topology.edge.Graph"));
    }
}
