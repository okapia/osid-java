//
// AbstractNodeBlogHierarchySession.java
//
//     Defines a Blog hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a blog hierarchy session for delivering a hierarchy
 *  of blogs using the BlogNode interface.
 */

public abstract class AbstractNodeBlogHierarchySession
    extends net.okapia.osid.jamocha.blogging.spi.AbstractBlogHierarchySession
    implements org.osid.blogging.BlogHierarchySession {

    private java.util.Collection<org.osid.blogging.BlogNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root blog <code> Ids </code> in this hierarchy.
     *
     *  @return the root blog <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootBlogIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToIdList(this.roots));
    }


    /**
     *  Gets the root blogs in the blog hierarchy. A node
     *  with no parents is an orphan. While all blog <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root blogs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getRootBlogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToBlogList(new net.okapia.osid.jamocha.blogging.blognode.ArrayBlogNodeList(this.roots)));
    }


    /**
     *  Adds a root blog node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootBlog(org.osid.blogging.BlogNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root blog nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootBlogs(java.util.Collection<org.osid.blogging.BlogNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root blog node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootBlog(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.blogging.BlogNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Blog </code> has any parents. 
     *
     *  @param  blogId a blog <code> Id </code> 
     *  @return <code> true </code> if the blog has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentBlogs(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getBlogNode(blogId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  blog.
     *
     *  @param  id an <code> Id </code> 
     *  @param  blogId the <code> Id </code> of a blog 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> blogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> blogId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfBlog(org.osid.id.Id id, org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.blogging.BlogNodeList parents = getBlogNode(blogId).getParentBlogNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextBlogNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given blog. 
     *
     *  @param  blogId a blog <code> Id </code> 
     *  @return the parent <code> Ids </code> of the blog 
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentBlogIds(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blog.BlogToIdList(getParentBlogs(blogId)));
    }


    /**
     *  Gets the parents of the given blog. 
     *
     *  @param  blogId the <code> Id </code> to query 
     *  @return the parents of the blog 
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getParentBlogs(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToBlogList(getBlogNode(blogId).getParentBlogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  blog.
     *
     *  @param  id an <code> Id </code> 
     *  @param  blogId the Id of a blog 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> blogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfBlog(org.osid.id.Id id, org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBlog(id, blogId)) {
            return (true);
        }

        try (org.osid.blogging.BlogList parents = getParentBlogs(blogId)) {
            while (parents.hasNext()) {
                if (isAncestorOfBlog(id, parents.getNextBlog().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a blog has any children. 
     *
     *  @param  blogId a blog <code> Id </code> 
     *  @return <code> true </code> if the <code> blogId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildBlogs(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBlogNode(blogId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  blog.
     *
     *  @param  id an <code> Id </code> 
     *  @param blogId the <code> Id </code> of a 
     *         blog
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> blogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> blogId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfBlog(org.osid.id.Id id, org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfBlog(blogId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  blog.
     *
     *  @param  blogId the <code> Id </code> to query 
     *  @return the children of the blog 
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildBlogIds(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blog.BlogToIdList(getChildBlogs(blogId)));
    }


    /**
     *  Gets the children of the given blog. 
     *
     *  @param  blogId the <code> Id </code> to query 
     *  @return the children of the blog 
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getChildBlogs(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToBlogList(getBlogNode(blogId).getChildBlogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  blog.
     *
     *  @param  id an <code> Id </code> 
     *  @param blogId the <code> Id </code> of a 
     *         blog
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> blogId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfBlog(org.osid.id.Id id, org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBlog(blogId, id)) {
            return (true);
        }

        try (org.osid.blogging.BlogList children = getChildBlogs(blogId)) {
            while (children.hasNext()) {
                if (isDescendantOfBlog(id, children.getNextBlog().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  blog.
     *
     *  @param  blogId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified blog node 
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getBlogNodeIds(org.osid.id.Id blogId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToNode(getBlogNode(blogId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given blog.
     *
     *  @param  blogId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified blog node 
     *  @throws org.osid.NotFoundException <code> blogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> blogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogNode getBlogNodes(org.osid.id.Id blogId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBlogNode(blogId));
    }


    /**
     *  Closes this <code>BlogHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a blog node.
     *
     *  @param blogId the id of the blog node
     *  @throws org.osid.NotFoundException <code>blogId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>blogId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.blogging.BlogNode getBlogNode(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(blogId, "blog Id");
        for (org.osid.blogging.BlogNode blog : this.roots) {
            if (blog.getId().equals(blogId)) {
                return (blog);
            }

            org.osid.blogging.BlogNode r = findBlog(blog, blogId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(blogId + " is not found");
    }


    protected org.osid.blogging.BlogNode findBlog(org.osid.blogging.BlogNode node, 
                                                            org.osid.id.Id blogId)
	throws org.osid.OperationFailedException {

        try (org.osid.blogging.BlogNodeList children = node.getChildBlogNodes()) {
            while (children.hasNext()) {
                org.osid.blogging.BlogNode blog = children.getNextBlogNode();
                if (blog.getId().equals(blogId)) {
                    return (blog);
                }
                
                blog = findBlog(blog, blogId);
                if (blog != null) {
                    return (blog);
                }
            }
        }

        return (null);
    }
}
