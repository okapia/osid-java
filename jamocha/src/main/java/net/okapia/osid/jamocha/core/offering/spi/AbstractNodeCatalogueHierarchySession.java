//
// AbstractNodeCatalogueHierarchySession.java
//
//     Defines a Catalogue hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a catalogue hierarchy session for delivering a hierarchy
 *  of catalogues using the CatalogueNode interface.
 */

public abstract class AbstractNodeCatalogueHierarchySession
    extends net.okapia.osid.jamocha.offering.spi.AbstractCatalogueHierarchySession
    implements org.osid.offering.CatalogueHierarchySession {

    private java.util.Collection<org.osid.offering.CatalogueNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root catalogue <code> Ids </code> in this hierarchy.
     *
     *  @return the root catalogue <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootCatalogueIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.offering.cataloguenode.CatalogueNodeToIdList(this.roots));
    }


    /**
     *  Gets the root catalogues in the catalogue hierarchy. A node
     *  with no parents is an orphan. While all catalogue <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root catalogues 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getRootCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.offering.cataloguenode.CatalogueNodeToCatalogueList(new net.okapia.osid.jamocha.offering.cataloguenode.ArrayCatalogueNodeList(this.roots)));
    }


    /**
     *  Adds a root catalogue node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootCatalogue(org.osid.offering.CatalogueNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root catalogue nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootCatalogues(java.util.Collection<org.osid.offering.CatalogueNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root catalogue node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootCatalogue(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.offering.CatalogueNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Catalogue </code> has any parents. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @return <code> true </code> if the catalogue has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentCatalogues(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getCatalogueNode(catalogueId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  catalogue.
     *
     *  @param  id an <code> Id </code> 
     *  @param  catalogueId the <code> Id </code> of a catalogue 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> catalogueId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> catalogueId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfCatalogue(org.osid.id.Id id, org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.offering.CatalogueNodeList parents = getCatalogueNode(catalogueId).getParentCatalogueNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextCatalogueNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given catalogue. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @return the parent <code> Ids </code> of the catalogue 
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentCatalogueIds(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.offering.catalogue.CatalogueToIdList(getParentCatalogues(catalogueId)));
    }


    /**
     *  Gets the parents of the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> to query 
     *  @return the parents of the catalogue 
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getParentCatalogues(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.offering.cataloguenode.CatalogueNodeToCatalogueList(getCatalogueNode(catalogueId).getParentCatalogueNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  catalogue.
     *
     *  @param  id an <code> Id </code> 
     *  @param  catalogueId the Id of a catalogue 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> catalogueId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfCatalogue(org.osid.id.Id id, org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCatalogue(id, catalogueId)) {
            return (true);
        }

        try (org.osid.offering.CatalogueList parents = getParentCatalogues(catalogueId)) {
            while (parents.hasNext()) {
                if (isAncestorOfCatalogue(id, parents.getNextCatalogue().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a catalogue has any children. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @return <code> true </code> if the <code> catalogueId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildCatalogues(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCatalogueNode(catalogueId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  catalogue.
     *
     *  @param  id an <code> Id </code> 
     *  @param catalogueId the <code> Id </code> of a 
     *         catalogue
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> catalogueId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> catalogueId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfCatalogue(org.osid.id.Id id, org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfCatalogue(catalogueId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  catalogue.
     *
     *  @param  catalogueId the <code> Id </code> to query 
     *  @return the children of the catalogue 
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildCatalogueIds(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.offering.catalogue.CatalogueToIdList(getChildCatalogues(catalogueId)));
    }


    /**
     *  Gets the children of the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> to query 
     *  @return the children of the catalogue 
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getChildCatalogues(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.offering.cataloguenode.CatalogueNodeToCatalogueList(getCatalogueNode(catalogueId).getChildCatalogueNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  catalogue.
     *
     *  @param  id an <code> Id </code> 
     *  @param catalogueId the <code> Id </code> of a 
     *         catalogue
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> catalogueId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfCatalogue(org.osid.id.Id id, org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCatalogue(catalogueId, id)) {
            return (true);
        }

        try (org.osid.offering.CatalogueList children = getChildCatalogues(catalogueId)) {
            while (children.hasNext()) {
                if (isDescendantOfCatalogue(id, children.getNextCatalogue().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  catalogue.
     *
     *  @param  catalogueId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified catalogue node 
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getCatalogueNodeIds(org.osid.id.Id catalogueId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.offering.cataloguenode.CatalogueNodeToNode(getCatalogueNode(catalogueId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given catalogue.
     *
     *  @param  catalogueId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified catalogue node 
     *  @throws org.osid.NotFoundException <code> catalogueId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogueId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueNode getCatalogueNodes(org.osid.id.Id catalogueId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCatalogueNode(catalogueId));
    }


    /**
     *  Closes this <code>CatalogueHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a catalogue node.
     *
     *  @param catalogueId the id of the catalogue node
     *  @throws org.osid.NotFoundException <code>catalogueId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>catalogueId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.offering.CatalogueNode getCatalogueNode(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(catalogueId, "catalogue Id");
        for (org.osid.offering.CatalogueNode catalogue : this.roots) {
            if (catalogue.getId().equals(catalogueId)) {
                return (catalogue);
            }

            org.osid.offering.CatalogueNode r = findCatalogue(catalogue, catalogueId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(catalogueId + " is not found");
    }


    protected org.osid.offering.CatalogueNode findCatalogue(org.osid.offering.CatalogueNode node, 
                                                            org.osid.id.Id catalogueId) 
	throws org.osid.OperationFailedException {

        try (org.osid.offering.CatalogueNodeList children = node.getChildCatalogueNodes()) {
            while (children.hasNext()) {
                org.osid.offering.CatalogueNode catalogue = children.getNextCatalogueNode();
                if (catalogue.getId().equals(catalogueId)) {
                    return (catalogue);
                }
                
                catalogue = findCatalogue(catalogue, catalogueId);
                if (catalogue != null) {
                    return (catalogue);
                }
            }
        }

        return (null);
    }
}
