//
// AbstractMapConferralLookupSession
//
//    A simple framework for providing a Conferral lookup service
//    backed by a fixed collection of conferrals.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Conferral lookup service backed by a
 *  fixed collection of conferrals. The conferrals are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Conferrals</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapConferralLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractConferralLookupSession
    implements org.osid.recognition.ConferralLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.recognition.Conferral> conferrals = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.recognition.Conferral>());


    /**
     *  Makes a <code>Conferral</code> available in this session.
     *
     *  @param  conferral a conferral
     *  @throws org.osid.NullArgumentException <code>conferral<code>
     *          is <code>null</code>
     */

    protected void putConferral(org.osid.recognition.Conferral conferral) {
        this.conferrals.put(conferral.getId(), conferral);
        return;
    }


    /**
     *  Makes an array of conferrals available in this session.
     *
     *  @param  conferrals an array of conferrals
     *  @throws org.osid.NullArgumentException <code>conferrals<code>
     *          is <code>null</code>
     */

    protected void putConferrals(org.osid.recognition.Conferral[] conferrals) {
        putConferrals(java.util.Arrays.asList(conferrals));
        return;
    }


    /**
     *  Makes a collection of conferrals available in this session.
     *
     *  @param  conferrals a collection of conferrals
     *  @throws org.osid.NullArgumentException <code>conferrals<code>
     *          is <code>null</code>
     */

    protected void putConferrals(java.util.Collection<? extends org.osid.recognition.Conferral> conferrals) {
        for (org.osid.recognition.Conferral conferral : conferrals) {
            this.conferrals.put(conferral.getId(), conferral);
        }

        return;
    }


    /**
     *  Removes a Conferral from this session.
     *
     *  @param  conferralId the <code>Id</code> of the conferral
     *  @throws org.osid.NullArgumentException <code>conferralId<code> is
     *          <code>null</code>
     */

    protected void removeConferral(org.osid.id.Id conferralId) {
        this.conferrals.remove(conferralId);
        return;
    }


    /**
     *  Gets the <code>Conferral</code> specified by its <code>Id</code>.
     *
     *  @param  conferralId <code>Id</code> of the <code>Conferral</code>
     *  @return the conferral
     *  @throws org.osid.NotFoundException <code>conferralId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>conferralId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Conferral getConferral(org.osid.id.Id conferralId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recognition.Conferral conferral = this.conferrals.get(conferralId);
        if (conferral == null) {
            throw new org.osid.NotFoundException("conferral not found: " + conferralId);
        }

        return (conferral);
    }


    /**
     *  Gets all <code>Conferrals</code>. In plenary mode, the returned
     *  list contains all known conferrals or an error
     *  results. Otherwise, the returned list may contain only those
     *  conferrals that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Conferrals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferrals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.conferral.ArrayConferralList(this.conferrals.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.conferrals.clear();
        super.close();
        return;
    }
}
