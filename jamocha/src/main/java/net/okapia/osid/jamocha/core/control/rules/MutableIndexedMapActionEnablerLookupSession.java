//
// MutableIndexedMapActionEnablerLookupSession
//
//    Implements an ActionEnabler lookup service backed by a collection of
//    actionEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements an ActionEnabler lookup service backed by a collection of
 *  action enablers. The action enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some action enablers may be compatible
 *  with more types than are indicated through these action enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of action enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapActionEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractIndexedMapActionEnablerLookupSession
    implements org.osid.control.rules.ActionEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapActionEnablerLookupSession} with no action enablers.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system}
     *          is {@code null}
     */

      public MutableIndexedMapActionEnablerLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActionEnablerLookupSession} with a
     *  single action enabler.
     *  
     *  @param system the system
     *  @param  actionEnabler an single actionEnabler
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionEnabler} is {@code null}
     */

    public MutableIndexedMapActionEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.control.rules.ActionEnabler actionEnabler) {
        this(system);
        putActionEnabler(actionEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActionEnablerLookupSession} using an
     *  array of action enablers.
     *
     *  @param system the system
     *  @param  actionEnablers an array of action enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionEnablers} is {@code null}
     */

    public MutableIndexedMapActionEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.control.rules.ActionEnabler[] actionEnablers) {
        this(system);
        putActionEnablers(actionEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapActionEnablerLookupSession} using a
     *  collection of action enablers.
     *
     *  @param system the system
     *  @param  actionEnablers a collection of action enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionEnablers} is {@code null}
     */

    public MutableIndexedMapActionEnablerLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.rules.ActionEnabler> actionEnablers) {

        this(system);
        putActionEnablers(actionEnablers);
        return;
    }
    

    /**
     *  Makes an {@code ActionEnabler} available in this session.
     *
     *  @param  actionEnabler an action enabler
     *  @throws org.osid.NullArgumentException {@code actionEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putActionEnabler(org.osid.control.rules.ActionEnabler actionEnabler) {
        super.putActionEnabler(actionEnabler);
        return;
    }


    /**
     *  Makes an array of action enablers available in this session.
     *
     *  @param  actionEnablers an array of action enablers
     *  @throws org.osid.NullArgumentException {@code actionEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putActionEnablers(org.osid.control.rules.ActionEnabler[] actionEnablers) {
        super.putActionEnablers(actionEnablers);
        return;
    }


    /**
     *  Makes collection of action enablers available in this session.
     *
     *  @param  actionEnablers a collection of action enablers
     *  @throws org.osid.NullArgumentException {@code actionEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putActionEnablers(java.util.Collection<? extends org.osid.control.rules.ActionEnabler> actionEnablers) {
        super.putActionEnablers(actionEnablers);
        return;
    }


    /**
     *  Removes an ActionEnabler from this session.
     *
     *  @param actionEnablerId the {@code Id} of the action enabler
     *  @throws org.osid.NullArgumentException {@code actionEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActionEnabler(org.osid.id.Id actionEnablerId) {
        super.removeActionEnabler(actionEnablerId);
        return;
    }    
}
