//
// AbstractAdapterBrokerConstrainerEnablerLookupSession.java
//
//    A BrokerConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A BrokerConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterBrokerConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession {

    private final org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBrokerConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBrokerConstrainerEnablerLookupSession(org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code BrokerConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBrokerConstrainerEnablers() {
        return (this.session.canLookupBrokerConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code BrokerConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerConstrainerEnablerView() {
        this.session.useComparativeBrokerConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code BrokerConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerConstrainerEnablerView() {
        this.session.usePlenaryBrokerConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker constrainer enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active broker constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerConstrainerEnablerView() {
        this.session.useActiveBrokerConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive broker constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerConstrainerEnablerView() {
        this.session.useAnyStatusBrokerConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code BrokerConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code BrokerConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code BrokerConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param brokerConstrainerEnablerId {@code Id} of the {@code BrokerConstrainerEnabler}
     *  @return the broker constrainer enabler
     *  @throws org.osid.NotFoundException {@code brokerConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnabler getBrokerConstrainerEnabler(org.osid.id.Id brokerConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainerEnabler(brokerConstrainerEnablerId));
    }


    /**
     *  Gets a {@code BrokerConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerConstrainerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code BrokerConstrainerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param  brokerConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code BrokerConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByIds(org.osid.id.IdList brokerConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainerEnablersByIds(brokerConstrainerEnablerIds));
    }


    /**
     *  Gets a {@code BrokerConstrainerEnablerList} corresponding to the given
     *  broker constrainer enabler genus {@code Type} which does not include
     *  broker constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param  brokerConstrainerEnablerGenusType a brokerConstrainerEnabler genus type 
     *  @return the returned {@code BrokerConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByGenusType(org.osid.type.Type brokerConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainerEnablersByGenusType(brokerConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code BrokerConstrainerEnablerList} corresponding to the given
     *  broker constrainer enabler genus {@code Type} and include any additional
     *  broker constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param  brokerConstrainerEnablerGenusType a brokerConstrainerEnabler genus type 
     *  @return the returned {@code BrokerConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByParentGenusType(org.osid.type.Type brokerConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainerEnablersByParentGenusType(brokerConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code BrokerConstrainerEnablerList} containing the given
     *  broker constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param  brokerConstrainerEnablerRecordType a brokerConstrainerEnabler record type 
     *  @return the returned {@code BrokerConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByRecordType(org.osid.type.Type brokerConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainerEnablersByRecordType(brokerConstrainerEnablerRecordType));
    }


    /**
     *  Gets a {@code BrokerConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code BrokerConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code BrokerConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code BrokerConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getBrokerConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code BrokerConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @return a list of {@code BrokerConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainerEnablers());
    }
}
