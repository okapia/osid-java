//
// AbstractMapValueLookupSession
//
//    A simple framework for providing a Value lookup service
//    backed by a fixed collection of values.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Value lookup service backed by a
 *  fixed collection of values. The values are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Values</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapValueLookupSession
    extends net.okapia.osid.jamocha.configuration.spi.AbstractValueLookupSession
    implements org.osid.configuration.ValueLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.configuration.Value> values = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.configuration.Value>());


    /**
     *  Makes a <code>Value</code> available in this session.
     *
     *  @param  value a value
     *  @throws org.osid.NullArgumentException <code>value<code>
     *          is <code>null</code>
     */

    protected void putValue(org.osid.configuration.Value value) {
        this.values.put(value.getId(), value);
        return;
    }


    /**
     *  Makes an array of values available in this session.
     *
     *  @param  values an array of values
     *  @throws org.osid.NullArgumentException <code>values<code>
     *          is <code>null</code>
     */

    protected void putValues(org.osid.configuration.Value[] values) {
        putValues(java.util.Arrays.asList(values));
        return;
    }


    /**
     *  Makes a collection of values available in this session.
     *
     *  @param  values a collection of values
     *  @throws org.osid.NullArgumentException <code>values<code>
     *          is <code>null</code>
     */

    protected void putValues(java.util.Collection<? extends org.osid.configuration.Value> values) {
        for (org.osid.configuration.Value value : values) {
            this.values.put(value.getId(), value);
        }

        return;
    }


    /**
     *  Removes a Value from this session.
     *
     *  @param  valueId the <code>Id</code> of the value
     *  @throws org.osid.NullArgumentException <code>valueId<code> is
     *          <code>null</code>
     */

    protected void removeValue(org.osid.id.Id valueId) {
        this.values.remove(valueId);
        return;
    }


    /**
     *  Gets the <code>Value</code> specified by its <code>Id</code>.
     *
     *  @param  valueId <code>Id</code> of the <code>Value</code>
     *  @return the value
     *  @throws org.osid.NotFoundException <code>valueId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>valueId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Value getValue(org.osid.id.Id valueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.configuration.Value value = this.values.get(valueId);
        if (value == null) {
            throw new org.osid.NotFoundException("value not found: " + valueId);
        }

        return (value);
    }


    /**
     *  Gets all <code>Values</code>. In plenary mode, the returned
     *  list contains all known values or an error
     *  results. Otherwise, the returned list may contain only those
     *  values that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Values</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.value.ArrayValueList(this.values.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.values.clear();
        super.close();
        return;
    }
}
