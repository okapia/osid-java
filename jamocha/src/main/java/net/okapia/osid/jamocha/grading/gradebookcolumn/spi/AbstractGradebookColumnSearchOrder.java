//
// AbstractGradebookColumnSearchOdrer.java
//
//     Defines a GradebookColumnSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code GradebookColumnSearchOrder}.
 */

public abstract class AbstractGradebookColumnSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.grading.GradebookColumnSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the grade system. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradeSystem(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemSearchOrder </code> is available for 
     *  grade systems. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grade system. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGradebookColumnSummarySearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnSummarySearchOrder() is false");
    }


    /**
     *  Tests if a <code> GradebookColumnSummarySearchOrder </code> is 
     *  available for gradebook column summaries. 
     *
     *  @return <code> true </code> if a gradebook column summary search order 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSummarySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a gradebook column summary search order. 
     *
     *  @return the gradebook column summary search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSummarySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummarySearchOrder getGradeSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSystemSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  gradebookColumnRecordType a gradebook column record type 
     *  @return {@code true} if the gradebookColumnRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookColumnRecordType) {
        for (org.osid.grading.records.GradebookColumnSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  gradebookColumnRecordType the gradebook column record type 
     *  @return the gradebook column search order record
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(gradebookColumnRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSearchOrderRecord getGradebookColumnSearchOrderRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this gradebook column. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookColumnRecord the gradebook column search odrer record
     *  @param gradebookColumnRecordType gradebook column record type
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnRecord} or
     *          {@code gradebookColumnRecordTypegradebookColumn} is
     *          {@code null}
     */
            
    protected void addGradebookColumnRecord(org.osid.grading.records.GradebookColumnSearchOrderRecord gradebookColumnSearchOrderRecord, 
                                     org.osid.type.Type gradebookColumnRecordType) {

        addRecordType(gradebookColumnRecordType);
        this.records.add(gradebookColumnSearchOrderRecord);
        
        return;
    }
}
