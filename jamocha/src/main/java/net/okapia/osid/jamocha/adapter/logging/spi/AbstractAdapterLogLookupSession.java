//
// AbstractAdapterLogLookupSession.java
//
//    A Log lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.logging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Log lookup session adapter.
 */

public abstract class AbstractAdapterLogLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.logging.LogLookupSession {

    private final org.osid.logging.LogLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterLogLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterLogLookupSession(org.osid.logging.LogLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Log} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupLogs() {
        return (this.session.canLookupLogs());
    }


    /**
     *  A complete view of the {@code Log} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLogView() {
        this.session.useComparativeLogView();
        return;
    }


    /**
     *  A complete view of the {@code Log} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLogView() {
        this.session.usePlenaryLogView();
        return;
    }

     
    /**
     *  Gets the {@code Log} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Log} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Log} and
     *  retained for compatibility.
     *
     *  @param logId {@code Id} of the {@code Log}
     *  @return the log
     *  @throws org.osid.NotFoundException {@code logId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code logId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.Log getLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLog(logId));
    }


    /**
     *  Gets a {@code LogList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  logs specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Logs} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  logIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Log} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code logIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByIds(org.osid.id.IdList logIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogsByIds(logIds));
    }


    /**
     *  Gets a {@code LogList} corresponding to the given
     *  log genus {@code Type} which does not include
     *  logs of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logGenusType a log genus type 
     *  @return the returned {@code Log} list
     *  @throws org.osid.NullArgumentException
     *          {@code logGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByGenusType(org.osid.type.Type logGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogsByGenusType(logGenusType));
    }


    /**
     *  Gets a {@code LogList} corresponding to the given
     *  log genus {@code Type} and include any additional
     *  logs with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logGenusType a log genus type 
     *  @return the returned {@code Log} list
     *  @throws org.osid.NullArgumentException
     *          {@code logGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByParentGenusType(org.osid.type.Type logGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogsByParentGenusType(logGenusType));
    }


    /**
     *  Gets a {@code LogList} containing the given
     *  log record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logRecordType a log record type 
     *  @return the returned {@code Log} list
     *  @throws org.osid.NullArgumentException
     *          {@code logRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByRecordType(org.osid.type.Type logRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogsByRecordType(logRecordType));
    }


    /**
     *  Gets a {@code LogList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Log} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Logs}. 
     *
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Logs} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogs());
    }
}
