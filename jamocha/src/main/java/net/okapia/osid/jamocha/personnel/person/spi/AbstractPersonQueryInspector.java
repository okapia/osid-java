//
// AbstractPersonQueryInspector.java
//
//     A template for making a PersonQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.person.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for persons.
 */

public abstract class AbstractPersonQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.personnel.PersonQueryInspector {

    private final java.util.Collection<org.osid.personnel.records.PersonQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the salutation query terms. 
     *
     *  @return the salutation terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSalutationTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the given name query terms. 
     *
     *  @return the given name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getGivenNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the preferred name query terms. 
     *
     *  @return the preferred name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPreferredNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the forename alias query terms. 
     *
     *  @return the forename alias terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getForenameAliasTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the middle name query terms. 
     *
     *  @return the middle name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getMiddleNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the surname query terms. 
     *
     *  @return the surname terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSurnameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the surname alias query terms. 
     *
     *  @return the surname alias terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSurnameAliasTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the generation qualifier query terms. 
     *
     *  @return the generation qualifier terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getGenerationQualifierTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the qualifier suffix query terms. 
     *
     *  @return the qualifier suffix terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getQualificationSuffixTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the birth date query terms. 
     *
     *  @return the birth date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBirthDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the death date query terms. 
     *
     *  @return the death date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDeathDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the living date query terms. 
     *
     *  @return the living date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getLivingDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the identifier query terms. 
     *
     *  @return the identifier terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getInstitutionalIdentifierTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given person query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a person implementing the requested record.
     *
     *  @param personRecordType a person record type
     *  @return the person query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(personRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PersonQueryInspectorRecord getPersonQueryInspectorRecord(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PersonQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(personRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(personRecordType + " is not supported");
    }


    /**
     *  Adds a record to this person query. 
     *
     *  @param personQueryInspectorRecord person query inspector
     *         record
     *  @param personRecordType person record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPersonQueryInspectorRecord(org.osid.personnel.records.PersonQueryInspectorRecord personQueryInspectorRecord, 
                                                   org.osid.type.Type personRecordType) {

        addRecordType(personRecordType);
        nullarg(personRecordType, "person record type");
        this.records.add(personQueryInspectorRecord);        
        return;
    }
}
