//
// AbstractFrontOfficeQuery.java
//
//     A template for making a FrontOffice Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.frontoffice.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for front offices.
 */

public abstract class AbstractFrontOfficeQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.tracking.FrontOfficeQuery {

    private final java.util.Collection<org.osid.tracking.records.FrontOfficeQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the issue <code> Id </code> for this query to match front offices 
     *  that have a related issue. 
     *
     *  @param  issueId a issue <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Matches front offices that have any issue. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          issue, <code> false </code> to match front offices with no 
     *          issue 
     */

    @OSID @Override
    public void matchAnyIssue(boolean match) {
        return;
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        return;
    }


    /**
     *  Sets the effort <code> Id </code> for this query to match front 
     *  offices containing queues. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Matches front offices that have any queue. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          queue, <code> false </code> to match front offices with no 
     *          queue 
     */

    @OSID @Override
    public void matchAnyQueue(boolean match) {
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        return;
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match front 
     *  offices that have the specified front office as an ancestor. 
     *
     *  @param  frontOfficeId a front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorFrontOfficeId(org.osid.id.Id frontOfficeId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the ancestor front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorFrontOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a frontOffice/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorFrontOfficeQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getAncestorFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorFrontOfficeQuery() is false");
    }


    /**
     *  Matches front offices with any ancestor. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          ancestor, <code> false </code> to match root frontOffices 
     */

    @OSID @Override
    public void matchAnyAncestorFrontOffice(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor front office query terms. 
     */

    @OSID @Override
    public void clearAncestorFrontOfficeTerms() {
        return;
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match front 
     *  offices that have the specified front office as a descendant. 
     *
     *  @param  frontOfficeId a front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantFrontOfficeId(org.osid.id.Id frontOfficeId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the descendant front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantFrontOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a frontOffice/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantFrontOfficeQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getDescendantFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantFrontOfficeQuery() is false");
    }


    /**
     *  Matches front offices with any descendant. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          descendant, <code> false </code> to match leaf frontOffices 
     */

    @OSID @Override
    public void matchAnyDescendantFrontOffice(boolean match) {
        return;
    }


    /**
     *  Clears the descendant front office query terms. 
     */

    @OSID @Override
    public void clearDescendantFrontOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given front office query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a front office implementing the requested record.
     *
     *  @param frontOfficeRecordType a front office record type
     *  @return the front office query record
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(frontOfficeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.FrontOfficeQueryRecord getFrontOfficeQueryRecord(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.FrontOfficeQueryRecord record : this.records) {
            if (record.implementsRecordType(frontOfficeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(frontOfficeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this front office query. 
     *
     *  @param frontOfficeQueryRecord front office query record
     *  @param frontOfficeRecordType frontOffice record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFrontOfficeQueryRecord(org.osid.tracking.records.FrontOfficeQueryRecord frontOfficeQueryRecord, 
                                          org.osid.type.Type frontOfficeRecordType) {

        addRecordType(frontOfficeRecordType);
        nullarg(frontOfficeQueryRecord, "front office query record");
        this.records.add(frontOfficeQueryRecord);        
        return;
    }
}
