//
// AbstractBillingPaymentManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractBillingPaymentManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.billing.payment.BillingPaymentManager,
               org.osid.billing.payment.BillingPaymentProxyManager {

    private final Types payerRecordTypes                   = new TypeRefSet();
    private final Types payerSearchRecordTypes             = new TypeRefSet();

    private final Types paymentRecordTypes                 = new TypeRefSet();
    private final Types paymentSearchRecordTypes           = new TypeRefSet();

    private final Types summaryRecordTypes                 = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractBillingPaymentManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractBillingPaymentManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up payers is supported. 
     *
     *  @return <code> true </code> if payer lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerLookup() {
        return (false);
    }


    /**
     *  Tests if querying payers is supported. 
     *
     *  @return <code> true </code> if payer query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerQuery() {
        return (false);
    }


    /**
     *  Tests if searching payers is supported. 
     *
     *  @return <code> true </code> if payer search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerSearch() {
        return (false);
    }


    /**
     *  Tests if payer <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if payer administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerAdmin() {
        return (false);
    }


    /**
     *  Tests if a payer <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if payer notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerNotification() {
        return (false);
    }


    /**
     *  Tests if a businessing service is supported. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerBusiness() {
        return (false);
    }


    /**
     *  Tests if a businessing service is supported. A businessing service 
     *  maps payers to catalogs. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a payer smart business session is available. 
     *
     *  @return <code> true </code> if a payer smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if looking up payments is supported. 
     *
     *  @return <code> true </code> if payment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentLookup() {
        return (false);
    }


    /**
     *  Tests if querying payments is supported. 
     *
     *  @return <code> true </code> if payment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentQuery() {
        return (false);
    }


    /**
     *  Tests if searching payments is supported. 
     *
     *  @return <code> true </code> if payment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentSearch() {
        return (false);
    }


    /**
     *  Tests if payment <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if payment administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentAdmin() {
        return (false);
    }


    /**
     *  Tests if a payment <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if payment notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentNotification() {
        return (false);
    }


    /**
     *  Tests if a payment cataloging service is supported. 
     *
     *  @return <code> true </code> if payment catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentBusiness() {
        return (false);
    }


    /**
     *  Tests if a payment cataloging service is supported. A cataloging 
     *  service maps payments to catalogs. 
     *
     *  @return <code> true </code> if payment cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentBusinessAssignment() {
        return (false);
    }


    /**
     *  Tests if a payment smart business session is available. 
     *
     *  @return <code> true </code> if a payment smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentSmartBusiness() {
        return (false);
    }


    /**
     *  Tests if a payment summary session is available. 
     *
     *  @return <code> true </code> if a summary session is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSummary() {
        return (false);
    }


    /**
     *  Tests if a payment batch service is available. 
     *
     *  @return <code> true </code> if a payment batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingPaymentBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Payer </code> record types. 
     *
     *  @return a list containing the supported <code> Payer </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPayerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.payerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Payer </code> record type is supported. 
     *
     *  @param  payerRecordType a <code> Type </code> indicating a <code> 
     *          Payer </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> payerRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPayerRecordType(org.osid.type.Type payerRecordType) {
        return (this.payerRecordTypes.contains(payerRecordType));
    }


    /**
     *  Adds support for a payer record type.
     *
     *  @param payerRecordType a payer record type
     *  @throws org.osid.NullArgumentException
     *  <code>payerRecordType</code> is <code>null</code>
     */

    protected void addPayerRecordType(org.osid.type.Type payerRecordType) {
        this.payerRecordTypes.add(payerRecordType);
        return;
    }


    /**
     *  Removes support for a payer record type.
     *
     *  @param payerRecordType a payer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>payerRecordType</code> is <code>null</code>
     */

    protected void removePayerRecordType(org.osid.type.Type payerRecordType) {
        this.payerRecordTypes.remove(payerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Payer </code> search record types. 
     *
     *  @return a list containing the supported <code> Payer </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPayerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.payerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Payer </code> search record type is 
     *  supported. 
     *
     *  @param  payerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Payer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> payerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPayerSearchRecordType(org.osid.type.Type payerSearchRecordType) {
        return (this.payerSearchRecordTypes.contains(payerSearchRecordType));
    }


    /**
     *  Adds support for a payer search record type.
     *
     *  @param payerSearchRecordType a payer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>payerSearchRecordType</code> is <code>null</code>
     */

    protected void addPayerSearchRecordType(org.osid.type.Type payerSearchRecordType) {
        this.payerSearchRecordTypes.add(payerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a payer search record type.
     *
     *  @param payerSearchRecordType a payer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>payerSearchRecordType</code> is <code>null</code>
     */

    protected void removePayerSearchRecordType(org.osid.type.Type payerSearchRecordType) {
        this.payerSearchRecordTypes.remove(payerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Payment </code> record types. 
     *
     *  @return a list containing the supported <code> Payment </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPaymentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.paymentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Payment </code> record type is supported. 
     *
     *  @param  paymentRecordType a <code> Type </code> indicating a <code> 
     *          Payment </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> paymentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPaymentRecordType(org.osid.type.Type paymentRecordType) {
        return (this.paymentRecordTypes.contains(paymentRecordType));
    }


    /**
     *  Adds support for a payment record type.
     *
     *  @param paymentRecordType a payment record type
     *  @throws org.osid.NullArgumentException
     *  <code>paymentRecordType</code> is <code>null</code>
     */

    protected void addPaymentRecordType(org.osid.type.Type paymentRecordType) {
        this.paymentRecordTypes.add(paymentRecordType);
        return;
    }


    /**
     *  Removes support for a payment record type.
     *
     *  @param paymentRecordType a payment record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>paymentRecordType</code> is <code>null</code>
     */

    protected void removePaymentRecordType(org.osid.type.Type paymentRecordType) {
        this.paymentRecordTypes.remove(paymentRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Payment </code> search record types. 
     *
     *  @return a list containing the supported <code> Payment </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPaymentSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.paymentSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Payment </code> search record type is 
     *  supported. 
     *
     *  @param  paymentSearchRecordType a <code> Type </code> indicating a 
     *          <code> Payment </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> paymentSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPaymentSearchRecordType(org.osid.type.Type paymentSearchRecordType) {
        return (this.paymentSearchRecordTypes.contains(paymentSearchRecordType));
    }


    /**
     *  Adds support for a payment search record type.
     *
     *  @param paymentSearchRecordType a payment search record type
     *  @throws org.osid.NullArgumentException
     *  <code>paymentSearchRecordType</code> is <code>null</code>
     */

    protected void addPaymentSearchRecordType(org.osid.type.Type paymentSearchRecordType) {
        this.paymentSearchRecordTypes.add(paymentSearchRecordType);
        return;
    }


    /**
     *  Removes support for a payment search record type.
     *
     *  @param paymentSearchRecordType a payment search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>paymentSearchRecordType</code> is <code>null</code>
     */

    protected void removePaymentSearchRecordType(org.osid.type.Type paymentSearchRecordType) {
        this.paymentSearchRecordTypes.remove(paymentSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Summary </code> record types. 
     *
     *  @return a list containing the supported <code> Summary </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSummaryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.summaryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Summary </code> record type is supported. 
     *
     *  @param  summaryRecordType a <code> Type </code> indicating a <code> 
     *          Summary </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> summaryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSummaryRecordType(org.osid.type.Type summaryRecordType) {
        return (this.summaryRecordTypes.contains(summaryRecordType));
    }


    /**
     *  Adds support for a summary record type.
     *
     *  @param summaryRecordType a summary record type
     *  @throws org.osid.NullArgumentException
     *  <code>summaryRecordType</code> is <code>null</code>
     */

    protected void addSummaryRecordType(org.osid.type.Type summaryRecordType) {
        this.summaryRecordTypes.add(summaryRecordType);
        return;
    }


    /**
     *  Removes support for a summary record type.
     *
     *  @param summaryRecordType a summary record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>summaryRecordType</code> is <code>null</code>
     */

    protected void removeSummaryRecordType(org.osid.type.Type summaryRecordType) {
        this.summaryRecordTypes.remove(summaryRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer lookup 
     *  service. 
     *
     *  @return a <code> PayerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerLookupSession getPayerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerLookupSession getPayerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> PayerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerLookupSession getPayerLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> PayerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerLookupSession getPayerLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer query 
     *  service. 
     *
     *  @return a <code> PayerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPayerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPayerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PayerQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPayerQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPayerQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer search 
     *  service. 
     *
     *  @return a <code> PayerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchSession getPayerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchSession getPayerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PayerSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchSession getPayerSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchSession getPayerSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  administration service. 
     *
     *  @return a <code> PayerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerAdminSession getPayerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerAdminSession getPayerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PayerAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerAdminSession getPayerAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerAdminSession getPayerAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  notification service. 
     *
     *  @param  payerReceiver the notification callback 
     *  @return a <code> PayerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> payerReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerNotificationSession getPayerNotificationSession(org.osid.billing.payment.PayerReceiver payerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  notification service. 
     *
     *  @param  payerReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> PayerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> payerReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerNotificationSession getPayerNotificationSession(org.osid.billing.payment.PayerReceiver payerReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  notification service for the given business. 
     *
     *  @param  payerReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PayerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> payerReceiver </code> or 
     *          <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerNotificationSession getPayerNotificationSessionForBusiness(org.osid.billing.payment.PayerReceiver payerReceiver, 
                                                                                                    org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  notification service for the given business. 
     *
     *  @param  payerReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> payerReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerNotificationSession getPayerNotificationSessionForBusiness(org.osid.billing.payment.PayerReceiver payerReceiver, 
                                                                                                    org.osid.id.Id businessId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup payer/catalog mappings. 
     *
     *  @return a <code> PayerPayerBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerBusinessSession getPayerBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup payer/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerBusinessSession getPayerBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning payers 
     *  to businesses. 
     *
     *  @return a <code> PayerBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerBusinessAssignmentSession getPayerBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning payers 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerBusinessAssignmentSession getPayerBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PayerSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSmartBusinessSession getPayerSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPayerSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSmartBusinessSession getPayerSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPayerSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment lookup 
     *  service. 
     *
     *  @return a <code> PaymentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentLookupSession getPaymentLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentLookupSession getPaymentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @return a <code> PaymentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentLookupSession getPaymentLookupSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> PaymentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentLookupSession getPaymentLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentLookupSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment query 
     *  service. 
     *
     *  @return a <code> PaymentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPaymentQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentQuerySession getPaymentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PaymentQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPaymentQuerySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentQuerySession getPaymentQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentQuerySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment search 
     *  service. 
     *
     *  @return a <code> PaymentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSearchSession getPaymentSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSearchSession getPaymentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PaymentSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSearchSession getPaymentSearchSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSearchSession getPaymentSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentSearchSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  administration service. 
     *
     *  @return a <code> PaymentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentAdminSession getPaymentAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentAdminSession getPaymentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PaymentAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentAdminSession getPaymentAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentAdminSession getPaymentAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentAdminSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  notification service. 
     *
     *  @param  paymentReceiver the notification callback 
     *  @return a <code> PaymentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> paymentReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentNotificationSession getPaymentNotificationSession(org.osid.billing.payment.PaymentReceiver paymentReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  notification service. 
     *
     *  @param  paymentReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> PaymentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> paymentReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentNotificationSession getPaymentNotificationSession(org.osid.billing.payment.PaymentReceiver paymentReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  notification service for the given business. 
     *
     *  @param  paymentReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PaymentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> paymentReceiver </code> 
     *          or <code> businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentNotificationSession getPaymentNotificationSessionForBusiness(org.osid.billing.payment.PaymentReceiver paymentReceiver, 
                                                                                                        org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  notification service for the given business. 
     *
     *  @param  paymentReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> paymentReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentNotificationSession getPaymentNotificationSessionForBusiness(org.osid.billing.payment.PaymentReceiver paymentReceiver, 
                                                                                                        org.osid.id.Id businessId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentNotificationSessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup payment/catalog 
     *  mappings. 
     *
     *  @return a <code> PaymentBusinessSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentBusinessSession getPaymentBusinessSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup payment/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentBusinessSession getPaymentBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning payments 
     *  to businesses. 
     *
     *  @return a <code> PaymentBusinessAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentBusinessAssignmentSession getPaymentBusinessAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning payments 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentBusinessAssignmentSession getPaymentBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentBusinessAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PaymentSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSmartBusinessSession getPaymentSmartBusinessSession(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getPaymentSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSmartBusinessSession getPaymentSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getPaymentSmartBusinessSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  summary service. 
     *
     *  @return a <code> SummarySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSummary() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.SummarySession getSummarySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getSummarySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  summary service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> SummarySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSummary() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.SummarySession getSummarySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getSummarySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the summary 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> SummarySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSummary() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.SummarySession getSummarySessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getSummarySessionForBusiness not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the summary 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> SummarySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSummary() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.SummarySession getSummarySessionForBusiness(org.osid.id.Id businessId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getSummarySessionForBusiness not implemented");
    }


    /**
     *  Gets a <code> BillingPaymentBatchManager. </code> 
     *
     *  @return a <code> BillingPaymentBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingPaymentBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.batch.BillingPaymentBatchManager getBillingPaymentBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentManager.getBillingPaymentBatchManager not implemented");
    }


    /**
     *  Gets a <code> BillingPaymentBatchProxyManager. </code> 
     *
     *  @return a <code> BillingPaymentBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingPaymentBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.batch.BillingPaymentBatchProxyManager getBillingPaymentBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.billing.payment.BillingPaymentProxyManager.getBillingPaymentBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.payerRecordTypes.clear();
        this.payerRecordTypes.clear();

        this.payerSearchRecordTypes.clear();
        this.payerSearchRecordTypes.clear();

        this.paymentRecordTypes.clear();
        this.paymentRecordTypes.clear();

        this.paymentSearchRecordTypes.clear();
        this.paymentSearchRecordTypes.clear();

        this.summaryRecordTypes.clear();
        this.summaryRecordTypes.clear();

        return;
    }
}
