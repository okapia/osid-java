//
// AbstractAssetContentQuery.java
//
//     A template for making an AssetContent Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.assetcontent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for asset contents.
 */

public abstract class AbstractAssetContentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.repository.AssetContentQuery {

    private final java.util.Collection<org.osid.repository.records.AssetContentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the accessibility types for this query. Supplying multiple types 
     *  behaves like a boolean OR among the elements. 
     *
     *  @param  accessibilityType an accessibilityType 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accessibilityType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAccessibilityType(org.osid.type.Type accessibilityType, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches asset content that has any accessibility type. 
     *
     *  @param  match <code> true </code> to match content with any 
     *          accessibility type, <code> false </code> to match content with 
     *          no accessibility type 
     */

    @OSID @Override
    public void matchAnyAccessibilityType(boolean match) {
        return;
    }


    /**
     *  Clears the accessibility terms. 
     */

    @OSID @Override
    public void clearAccessibilityTypeTerms() {
        return;
    }


    /**
     *  Matches content whose length of the data in bytes are inclusive of the 
     *  given range. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchDataLength(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches content that has any data length. 
     *
     *  @param  match <code> true </code> to match content with any data 
     *          length, <code> false </code> to match content with no data 
     *          length 
     */

    @OSID @Override
    public void matchAnyDataLength(boolean match) {
        return;
    }


    /**
     *  Clears the data length terms. 
     */

    @OSID @Override
    public void clearDataLengthTerms() {
        return;
    }


    /**
     *  Matches data in this content. 
     *
     *  @param  data list of matching strings 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @param partial <code> true </code> for a partial match, <code>
     *          false </code> for a complete match
     *  @throws org.osid.NullArgumentException <code> data </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchData(byte[] data, boolean match, boolean partial) {
        return;
    }


    /**
     *  Matches content that has any data. 
     *
     *  @param  match <code> true </code> to match content with any data, 
     *          <code> false </code> to match content with no data 
     */

    @OSID @Override
    public void matchAnyData(boolean match) {
        return;
    }


    /**
     *  Clears the data terms. 
     */

    @OSID @Override
    public void clearDataTerms() {
        return;
    }


    /**
     *  Sets the url for this query. Supplying multiple strings behaves like a 
     *  boolean <code> OR </code> among the elements each which must 
     *  correspond to the <code> stringMatchType. </code> 
     *
     *  @param  url url string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(url) </code> is <code> false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        return;
    }


    /**
     *  Matches content that has any url. 
     *
     *  @param  match <code> true </code> to match content with any url, 
     *          <code> false </code> to match content with no url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        return;
    }


    /**
     *  Clears the url terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given asset content query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an asset content implementing the requested record.
     *
     *  @param assetContentRecordType an asset content record type
     *  @return the asset content query record
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetContentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetContentQueryRecord getAssetContentQueryRecord(org.osid.type.Type assetContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetContentQueryRecord record : this.records) {
            if (record.implementsRecordType(assetContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetContentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this asset content query. 
     *
     *  @param assetContentQueryRecord asset content query record
     *  @param assetContentRecordType assetContent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssetContentQueryRecord(org.osid.repository.records.AssetContentQueryRecord assetContentQueryRecord, 
                                          org.osid.type.Type assetContentRecordType) {

        addRecordType(assetContentRecordType);
        nullarg(assetContentQueryRecord, "asset content query record");
        this.records.add(assetContentQueryRecord);        
        return;
    }
}
