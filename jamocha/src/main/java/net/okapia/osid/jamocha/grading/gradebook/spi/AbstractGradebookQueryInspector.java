//
// AbstractGradebookQueryInspector.java
//
//     A template for making a GradebookQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for gradebooks.
 */

public abstract class AbstractGradebookQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.grading.GradebookQueryInspector {

    private final java.util.Collection<org.osid.grading.records.GradebookQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the grade system <code> Id </code> terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the grade entry <code> Id </code> terms. 
     *
     *  @return the grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade entry terms. 
     *
     *  @return the grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebook column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Gets the ancestor gradebook <code> Id </code> terms. 
     *
     *  @return the ancestor gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorGradebookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor gradebook terms. 
     *
     *  @return the ancestor gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getAncestorGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the descendant gradebook <code> Id </code> terms. 
     *
     *  @return the descendant gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantGradebookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant gradebook terms. 
     *
     *  @return the descendant gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getDescendantGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given gradebook query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a gradebook implementing the requested record.
     *
     *  @param gradebookRecordType a gradebook record type
     *  @return the gradebook query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookQueryInspectorRecord getGradebookQueryInspectorRecord(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook query. 
     *
     *  @param gradebookQueryInspectorRecord gradebook query inspector
     *         record
     *  @param gradebookRecordType gradebook record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookQueryInspectorRecord(org.osid.grading.records.GradebookQueryInspectorRecord gradebookQueryInspectorRecord, 
                                                   org.osid.type.Type gradebookRecordType) {

        addRecordType(gradebookRecordType);
        nullarg(gradebookRecordType, "gradebook record type");
        this.records.add(gradebookQueryInspectorRecord);        
        return;
    }
}
