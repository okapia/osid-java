//
// AbstractAdapterModuleLookupSession.java
//
//    A Module lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Module lookup session adapter.
 */

public abstract class AbstractAdapterModuleLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.syllabus.ModuleLookupSession {

    private final org.osid.course.syllabus.ModuleLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterModuleLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterModuleLookupSession(org.osid.course.syllabus.ModuleLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Module} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupModules() {
        return (this.session.canLookupModules());
    }


    /**
     *  A complete view of the {@code Module} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeModuleView() {
        this.session.useComparativeModuleView();
        return;
    }


    /**
     *  A complete view of the {@code Module} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryModuleView() {
        this.session.usePlenaryModuleView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include modules in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active modules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveModuleView() {
        this.session.useActiveModuleView();
        return;
    }


    /**
     *  Active and inactive modules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusModuleView() {
        this.session.useAnyStatusModuleView();
        return;
    }
    
     
    /**
     *  Gets the {@code Module} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Module} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Module} and
     *  retained for compatibility.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param moduleId {@code Id} of the {@code Module}
     *  @return the module
     *  @throws org.osid.NotFoundException {@code moduleId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code moduleId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Module getModule(org.osid.id.Id moduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModule(moduleId));
    }


    /**
     *  Gets a {@code ModuleList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  modules specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Modules} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Module} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code moduleIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByIds(org.osid.id.IdList moduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModulesByIds(moduleIds));
    }


    /**
     *  Gets a {@code ModuleList} corresponding to the given
     *  module genus {@code Type} which does not include
     *  modules of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned {@code Module} list
     *  @throws org.osid.NullArgumentException
     *          {@code moduleGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModulesByGenusType(moduleGenusType));
    }


    /**
     *  Gets a {@code ModuleList} corresponding to the given
     *  module genus {@code Type} and include any additional
     *  modules with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned {@code Module} list
     *  @throws org.osid.NullArgumentException
     *          {@code moduleGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByParentGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModulesByParentGenusType(moduleGenusType));
    }


    /**
     *  Gets a {@code ModuleList} containing the given
     *  module record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleRecordType a module record type 
     *  @return the returned {@code Module} list
     *  @throws org.osid.NullArgumentException
     *          {@code moduleRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByRecordType(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModulesByRecordType(moduleRecordType));
    }


    /**
     *  Gets a {@code ModuleList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Module} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModulesByProvider(resourceId));
    }


    /**
     *  Gets a {@code ModuleList} for the given syllabus {@code Id}.
     *  
     *  In plenary mode, the returned list contains all known modules
     *  or an error results. Otherwise, the returned list may contain
     *  only those modules that are accessible through this session.
     *  
     *  In active mode, modeules are returned that are currently
     *  active. In any status mode, active and inactive modules are
     *  returned.
     *
     *  @param  syllabusId a syllabus {@code Id} 
     *  @return the returned {@code Module} list 
     *  @throws org.osid.NullArgumentException {@code syllabusId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesForSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModulesForSyllabus(syllabusId));
    }


    /**
     *  Gets all {@code Modules}. 
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @return a list of {@code Modules} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getModules());
    }
}
