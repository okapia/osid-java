//
// AuctionProcessorElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessor.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AuctionProcessorElements
    extends net.okapia.osid.jamocha.spi.OsidProcessorElements {


    /**
     *  Gets the AuctionProcessorElement Id.
     *
     *  @return the auction processor element Id
     */

    public static org.osid.id.Id getAuctionProcessorEntityId() {
        return (makeEntityId("osid.bidding.rules.AuctionProcessor"));
    }


    /**
     *  Gets the RuledAuctionId element Id.
     *
     *  @return the RuledAuctionId element Id
     */

    public static org.osid.id.Id getRuledAuctionId() {
        return (makeQueryElementId("osid.bidding.rules.auctionprocessor.RuledAuctionId"));
    }


    /**
     *  Gets the RuledAuction element Id.
     *
     *  @return the RuledAuction element Id
     */

    public static org.osid.id.Id getRuledAuction() {
        return (makeQueryElementId("osid.bidding.rules.auctionprocessor.RuledAuction"));
    }


    /**
     *  Gets the AuctionHouseId element Id.
     *
     *  @return the AuctionHouseId element Id
     */

    public static org.osid.id.Id getAuctionHouseId() {
        return (makeQueryElementId("osid.bidding.rules.auctionprocessor.AuctionHouseId"));
    }


    /**
     *  Gets the AuctionHouse element Id.
     *
     *  @return the AuctionHouse element Id
     */

    public static org.osid.id.Id getAuctionHouse() {
        return (makeQueryElementId("osid.bidding.rules.auctionprocessor.AuctionHouse"));
    }
}
