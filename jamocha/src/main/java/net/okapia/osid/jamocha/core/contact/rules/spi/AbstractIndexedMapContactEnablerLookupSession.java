//
// AbstractIndexedMapContactEnablerLookupSession.java
//
//    A simple framework for providing a ContactEnabler lookup service
//    backed by a fixed collection of contact enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ContactEnabler lookup service backed by a
 *  fixed collection of contact enablers. The contact enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some contact enablers may be compatible
 *  with more types than are indicated through these contact enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ContactEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapContactEnablerLookupSession
    extends AbstractMapContactEnablerLookupSession
    implements org.osid.contact.rules.ContactEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.contact.rules.ContactEnabler> contactEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.rules.ContactEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.contact.rules.ContactEnabler> contactEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.rules.ContactEnabler>());


    /**
     *  Makes a <code>ContactEnabler</code> available in this session.
     *
     *  @param  contactEnabler a contact enabler
     *  @throws org.osid.NullArgumentException <code>contactEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putContactEnabler(org.osid.contact.rules.ContactEnabler contactEnabler) {
        super.putContactEnabler(contactEnabler);

        this.contactEnablersByGenus.put(contactEnabler.getGenusType(), contactEnabler);
        
        try (org.osid.type.TypeList types = contactEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.contactEnablersByRecord.put(types.getNextType(), contactEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a contact enabler from this session.
     *
     *  @param contactEnablerId the <code>Id</code> of the contact enabler
     *  @throws org.osid.NullArgumentException <code>contactEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeContactEnabler(org.osid.id.Id contactEnablerId) {
        org.osid.contact.rules.ContactEnabler contactEnabler;
        try {
            contactEnabler = getContactEnabler(contactEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.contactEnablersByGenus.remove(contactEnabler.getGenusType());

        try (org.osid.type.TypeList types = contactEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.contactEnablersByRecord.remove(types.getNextType(), contactEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeContactEnabler(contactEnablerId);
        return;
    }


    /**
     *  Gets a <code>ContactEnablerList</code> corresponding to the given
     *  contact enabler genus <code>Type</code> which does not include
     *  contact enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known contact enablers or an error results. Otherwise,
     *  the returned list may contain only those contact enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  contactEnablerGenusType a contact enabler genus type 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByGenusType(org.osid.type.Type contactEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.rules.contactenabler.ArrayContactEnablerList(this.contactEnablersByGenus.get(contactEnablerGenusType)));
    }


    /**
     *  Gets a <code>ContactEnablerList</code> containing the given
     *  contact enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known contact enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  contact enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  contactEnablerRecordType a contact enabler record type 
     *  @return the returned <code>contactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByRecordType(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.rules.contactenabler.ArrayContactEnablerList(this.contactEnablersByRecord.get(contactEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.contactEnablersByGenus.clear();
        this.contactEnablersByRecord.clear();

        super.close();

        return;
    }
}
