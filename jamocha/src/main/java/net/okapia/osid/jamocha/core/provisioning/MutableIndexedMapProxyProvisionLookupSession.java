//
// MutableIndexedMapProxyProvisionLookupSession
//
//    Implements a Provision lookup service backed by a collection of
//    provisions indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Provision lookup service backed by a collection of
 *  provisions. The provisions are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some provisions may be compatible
 *  with more types than are indicated through these provision
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of provisions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyProvisionLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractIndexedMapProvisionLookupSession
    implements org.osid.provisioning.ProvisionLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionLookupSession} with
     *  no provision.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionLookupSession} with
     *  a single provision.
     *
     *  @param distributor the distributor
     *  @param  provision an provision
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provision}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.Provision provision, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putProvision(provision);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionLookupSession} using
     *  an array of provisions.
     *
     *  @param distributor the distributor
     *  @param  provisions an array of provisions
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provisions}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.Provision[] provisions, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putProvisions(provisions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProvisionLookupSession} using
     *  a collection of provisions.
     *
     *  @param distributor the distributor
     *  @param  provisions a collection of provisions
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provisions}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                       java.util.Collection<? extends org.osid.provisioning.Provision> provisions,
                                                       org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putProvisions(provisions);
        return;
    }

    
    /**
     *  Makes a {@code Provision} available in this session.
     *
     *  @param  provision a provision
     *  @throws org.osid.NullArgumentException {@code provision{@code 
     *          is {@code null}
     */

    @Override
    public void putProvision(org.osid.provisioning.Provision provision) {
        super.putProvision(provision);
        return;
    }


    /**
     *  Makes an array of provisions available in this session.
     *
     *  @param  provisions an array of provisions
     *  @throws org.osid.NullArgumentException {@code provisions{@code 
     *          is {@code null}
     */

    @Override
    public void putProvisions(org.osid.provisioning.Provision[] provisions) {
        super.putProvisions(provisions);
        return;
    }


    /**
     *  Makes collection of provisions available in this session.
     *
     *  @param  provisions a collection of provisions
     *  @throws org.osid.NullArgumentException {@code provision{@code 
     *          is {@code null}
     */

    @Override
    public void putProvisions(java.util.Collection<? extends org.osid.provisioning.Provision> provisions) {
        super.putProvisions(provisions);
        return;
    }


    /**
     *  Removes a Provision from this session.
     *
     *  @param provisionId the {@code Id} of the provision
     *  @throws org.osid.NullArgumentException {@code provisionId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProvision(org.osid.id.Id provisionId) {
        super.removeProvision(provisionId);
        return;
    }    
}
