//
// AbstractAssemblyVoteQuery.java
//
//     A VoteQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.vote.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A VoteQuery that stores terms.
 */

public abstract class AbstractAssemblyVoteQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.voting.VoteQuery,
               org.osid.voting.VoteQueryInspector,
               org.osid.voting.VoteSearchOrder {

    private final java.util.Collection<org.osid.voting.records.VoteQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.VoteQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.VoteSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyVoteQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyVoteQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the candidate <code> Id </code> for this query. 
     *
     *  @param  candidateId a candidate <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> candidateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCandidateId(org.osid.id.Id candidateId, boolean match) {
        getAssembler().addIdTerm(getCandidateIdColumn(), candidateId, match);
        return;
    }


    /**
     *  Clears the candidate <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCandidateIdTerms() {
        getAssembler().clearTerms(getCandidateIdColumn());
        return;
    }


    /**
     *  Gets the candidate <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCandidateIdTerms() {
        return (getAssembler().getIdTerms(getCandidateIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the candidate. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCandidate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCandidateColumn(), style);
        return;
    }


    /**
     *  Gets the CandidateId column name.
     *
     * @return the column name
     */

    protected String getCandidateIdColumn() {
        return ("candidate_id");
    }


    /**
     *  Tests if a <code> CandidateQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a candidate query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a candidate. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the candidate query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuery getCandidateQuery() {
        throw new org.osid.UnimplementedException("supportsCandidateQuery() is false");
    }


    /**
     *  Clears the candidate terms. 
     */

    @OSID @Override
    public void clearCandidateTerms() {
        getAssembler().clearTerms(getCandidateColumn());
        return;
    }


    /**
     *  Gets the candidate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.CandidateQueryInspector[] getCandidateTerms() {
        return (new org.osid.voting.CandidateQueryInspector[0]);
    }


    /**
     *  Tests if a <code> CandidateSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a candidate search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order interface for a candidate. 
     *
     *  @return the candidate search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchOrder getCandidateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCandidateSearchOrder() is false");
    }


    /**
     *  Gets the Candidate column name.
     *
     * @return the column name
     */

    protected String getCandidateColumn() {
        return ("candidate");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVoterId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getVoterIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the voter agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearVoterIdTerms() {
        getAssembler().clearTerms(getVoterIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVoterIdTerms() {
        return (getAssembler().getIdTerms(getVoterIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the voter. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVoter(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVoterColumn(), style);
        return;
    }


    /**
     *  Gets the VoterId column name.
     *
     * @return the column name
     */

    protected String getVoterIdColumn() {
        return ("voter_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a voter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsVoterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getVoterQuery() {
        throw new org.osid.UnimplementedException("supportsVoterQuery() is false");
    }


    /**
     *  Clears the voter terms. 
     */

    @OSID @Override
    public void clearVoterTerms() {
        getAssembler().clearTerms(getVoterColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getVoterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getVoterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsVoterSearchOrder() is false");
    }


    /**
     *  Gets the Voter column name.
     *
     * @return the column name
     */

    protected String getVoterColumn() {
        return ("voter");
    }


    /**
     *  Sets the voting agent <code> Id </code> for this query. 
     *
     *  @param  agentId a voting agent agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVotingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getVotingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the voting agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearVotingAgentIdTerms() {
        getAssembler().clearTerms(getVotingAgentIdColumn());
        return;
    }


    /**
     *  Gets the voting agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVotingAgentIdTerms() {
        return (getAssembler().getIdTerms(getVotingAgentIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the voteing agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVotingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVotingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the VotingAgentId column name.
     *
     * @return the column name
     */

    protected String getVotingAgentIdColumn() {
        return ("voting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a voting agent query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a voter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getVotingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsVotingAgentQuery() is false");
    }


    /**
     *  Clears the voter terms. 
     */

    @OSID @Override
    public void clearVotingAgentTerms() {
        getAssembler().clearTerms(getVotingAgentColumn());
        return;
    }


    /**
     *  Gets the voting agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getVotingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an <code> AgentSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an agent. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getVotingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsVotingAgentSearchOrder() is false");
    }


    /**
     *  Gets the VotingAgent column name.
     *
     * @return the column name
     */

    protected String getVotingAgentColumn() {
        return ("voting_agent");
    }


    /**
     *  Matches the number of votes within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchVotes(long from, long to, boolean match) {
        getAssembler().addIntegerRangeTerm(getVotesColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the votes terms. 
     */

    @OSID @Override
    public void clearVotesTerms() {
        getAssembler().clearTerms(getVotesColumn());
        return;
    }


    /**
     *  Gets the votes query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getVotesTerms() {
        return (getAssembler().getIntegerRangeTerms(getVotesColumn()));
    }


    /**
     *  Specified a preference for ordering results by the number of votes. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVotes(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVotesColumn(), style);
        return;
    }


    /**
     *  Gets the Votes column name.
     *
     * @return the column name
     */

    protected String getVotesColumn() {
        return ("votes");
    }


    /**
     *  Matches the number of minimum votes inclusive. 
     *
     *  @param  votes the numbe rof votes 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumVotes(long votes, boolean match) {
        getAssembler().addIntegerTerm(getMinimumVotesColumn(), votes, match);
        return;
    }


    /**
     *  Clears the minimum votes terms. 
     */

    @OSID @Override
    public void clearMinimumVotesTerms() {
        getAssembler().clearTerms(getMinimumVotesColumn());
        return;
    }


    /**
     *  Gets the minimum votes query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerTerm[] getMinimumVotesTerms() {
        return (getAssembler().getIntegerTerms(getMinimumVotesColumn()));
    }


    /**
     *  Gets the MinimumVotes column name.
     *
     * @return the column name
     */

    protected String getMinimumVotesColumn() {
        return ("minimum_votes");
    }


    /**
     *  Sets the polls <code> Id </code> for this query to match terms 
     *  assigned to polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this vote supports the given record
     *  <code>Type</code>.
     *
     *  @param  voteRecordType a vote record type 
     *  @return <code>true</code> if the voteRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type voteRecordType) {
        for (org.osid.voting.records.VoteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(voteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  voteRecordType the vote record type 
     *  @return the vote query record 
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(voteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoteQueryRecord getVoteQueryRecord(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.VoteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(voteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voteRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  voteRecordType the vote record type 
     *  @return the vote query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(voteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoteQueryInspectorRecord getVoteQueryInspectorRecord(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.VoteQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(voteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voteRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param voteRecordType the vote record type
     *  @return the vote search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(voteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoteSearchOrderRecord getVoteSearchOrderRecord(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.VoteSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(voteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voteRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this vote. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param voteQueryRecord the vote query record
     *  @param voteQueryInspectorRecord the vote query inspector
     *         record
     *  @param voteSearchOrderRecord the vote search order record
     *  @param voteRecordType vote record type
     *  @throws org.osid.NullArgumentException
     *          <code>voteQueryRecord</code>,
     *          <code>voteQueryInspectorRecord</code>,
     *          <code>voteSearchOrderRecord</code> or
     *          <code>voteRecordTypevote</code> is
     *          <code>null</code>
     */
            
    protected void addVoteRecords(org.osid.voting.records.VoteQueryRecord voteQueryRecord, 
                                      org.osid.voting.records.VoteQueryInspectorRecord voteQueryInspectorRecord, 
                                      org.osid.voting.records.VoteSearchOrderRecord voteSearchOrderRecord, 
                                      org.osid.type.Type voteRecordType) {

        addRecordType(voteRecordType);

        nullarg(voteQueryRecord, "vote query record");
        nullarg(voteQueryInspectorRecord, "vote query inspector record");
        nullarg(voteSearchOrderRecord, "vote search odrer record");

        this.queryRecords.add(voteQueryRecord);
        this.queryInspectorRecords.add(voteQueryInspectorRecord);
        this.searchOrderRecords.add(voteSearchOrderRecord);
        
        return;
    }
}
