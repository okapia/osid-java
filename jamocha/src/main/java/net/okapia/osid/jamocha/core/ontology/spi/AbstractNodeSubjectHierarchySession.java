//
// AbstractNodeSubjectHierarchySession.java
//
//     Defines a Subject hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a subject hierarchy session for delivering a hierarchy
 *  of subjects using the SubjectNode interface.
 */

public abstract class AbstractNodeSubjectHierarchySession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractSubjectHierarchySession
    implements org.osid.ontology.SubjectHierarchySession {

    private java.util.Collection<org.osid.ontology.SubjectNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root subject <code> Ids </code> in this hierarchy.
     *
     *  @return the root subject <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootSubjectIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.subjectnode.SubjectNodeToIdList(this.roots));
    }


    /**
     *  Gets the root subjects in the subject hierarchy. A node
     *  with no parents is an orphan. While all subject <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root subjects 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getRootSubjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.subjectnode.SubjectNodeToSubjectList(new net.okapia.osid.jamocha.ontology.subjectnode.ArraySubjectNodeList(this.roots)));
    }


    /**
     *  Adds a root subject node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootSubject(org.osid.ontology.SubjectNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root subject nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootSubjects(java.util.Collection<org.osid.ontology.SubjectNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root subject node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootSubject(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.ontology.SubjectNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Subject </code> has any parents. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @return <code> true </code> if the subject has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentSubjects(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getSubjectNode(subjectId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  subject.
     *
     *  @param  id an <code> Id </code> 
     *  @param  subjectId the <code> Id </code> of a subject 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> subjectId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> subjectId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfSubject(org.osid.id.Id id, org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ontology.SubjectNodeList parents = getSubjectNode(subjectId).getParentSubjectNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextSubjectNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given subject. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @return the parent <code> Ids </code> of the subject 
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentSubjectIds(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.subject.SubjectToIdList(getParentSubjects(subjectId)));
    }


    /**
     *  Gets the parents of the given subject. 
     *
     *  @param  subjectId the <code> Id </code> to query 
     *  @return the parents of the subject 
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getParentSubjects(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.subjectnode.SubjectNodeToSubjectList(getSubjectNode(subjectId).getParentSubjectNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  subject.
     *
     *  @param  id an <code> Id </code> 
     *  @param  subjectId the Id of a subject 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> subjectId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfSubject(org.osid.id.Id id, org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfSubject(id, subjectId)) {
            return (true);
        }

        try (org.osid.ontology.SubjectList parents = getParentSubjects(subjectId)) {
            while (parents.hasNext()) {
                if (isAncestorOfSubject(id, parents.getNextSubject().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a subject has any children. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @return <code> true </code> if the <code> subjectId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildSubjects(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSubjectNode(subjectId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  subject.
     *
     *  @param  id an <code> Id </code> 
     *  @param subjectId the <code> Id </code> of a 
     *         subject
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> subjectId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> subjectId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfSubject(org.osid.id.Id id, org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfSubject(subjectId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  subject.
     *
     *  @param  subjectId the <code> Id </code> to query 
     *  @return the children of the subject 
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildSubjectIds(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.subject.SubjectToIdList(getChildSubjects(subjectId)));
    }


    /**
     *  Gets the children of the given subject. 
     *
     *  @param  subjectId the <code> Id </code> to query 
     *  @return the children of the subject 
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getChildSubjects(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ontology.subjectnode.SubjectNodeToSubjectList(getSubjectNode(subjectId).getChildSubjectNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  subject.
     *
     *  @param  id an <code> Id </code> 
     *  @param subjectId the <code> Id </code> of a 
     *         subject
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> subjectId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfSubject(org.osid.id.Id id, org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfSubject(subjectId, id)) {
            return (true);
        }

        try (org.osid.ontology.SubjectList children = getChildSubjects(subjectId)) {
            while (children.hasNext()) {
                if (isDescendantOfSubject(id, children.getNextSubject().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  subject.
     *
     *  @param  subjectId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified subject node 
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getSubjectNodeIds(org.osid.id.Id subjectId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.ontology.subjectnode.SubjectNodeToNode(getSubjectNode(subjectId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given subject.
     *
     *  @param  subjectId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified subject node 
     *  @throws org.osid.NotFoundException <code> subjectId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectNode getSubjectNodes(org.osid.id.Id subjectId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSubjectNode(subjectId));
    }


    /**
     *  Closes this <code>SubjectHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a subject node.
     *
     *  @param subjectId the id of the subject node
     *  @throws org.osid.NotFoundException <code>subjectId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>subjectId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.ontology.SubjectNode getSubjectNode(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(subjectId, "subject Id");
        for (org.osid.ontology.SubjectNode subject : this.roots) {
            if (subject.getId().equals(subjectId)) {
                return (subject);
            }

            org.osid.ontology.SubjectNode r = findSubject(subject, subjectId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(subjectId + " is not found");
    }


    protected org.osid.ontology.SubjectNode findSubject(org.osid.ontology.SubjectNode node, 
                                                        org.osid.id.Id subjectId) 
	throws org.osid.OperationFailedException {

        try (org.osid.ontology.SubjectNodeList children = node.getChildSubjectNodes()) {
            while (children.hasNext()) {
                org.osid.ontology.SubjectNode subject = children.getNextSubjectNode();
                if (subject.getId().equals(subjectId)) {
                    return (subject);
                }
                
                subject = findSubject(subject, subjectId);
                if (subject != null) {
                    return (subject);
                }
            }
        }

        return (null);
    }
}
