//
// UnknownPayment.java
//
//     Defines an unknown Payment.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.billing.payment.payment;


/**
 *  Defines an unknown <code>Payment</code>.
 */

public final class UnknownPayment
    extends net.okapia.osid.jamocha.nil.billing.payment.payment.spi.AbstractUnknownPayment
    implements org.osid.billing.payment.Payment {


    /**
     *  Constructs a new <code>UnknownPayment</code>.
     */

    public UnknownPayment() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownPayment</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownPayment(boolean optional) {
        super(optional);
        addPaymentRecord(new PaymentRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Payment.
     *
     *  @return an unknown Payment
     */

    public static org.osid.billing.payment.Payment create() {
        return (net.okapia.osid.jamocha.builder.validator.billing.payment.payment.PaymentValidator.validatePayment(new UnknownPayment()));
    }


    public class PaymentRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.billing.payment.records.PaymentRecord {

        
        protected PaymentRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
