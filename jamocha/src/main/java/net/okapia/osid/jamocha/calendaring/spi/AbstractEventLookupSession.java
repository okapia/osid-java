//
// AbstractEventLookupSession.java
//
//    A starter implementation framework for providing an Event
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Event
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEvents(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEventLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.EventLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private boolean normalized    = false;
    private boolean sequestered   = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>Event</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEvents() {
        return (true);
    }


    /**
     *  A complete view of the <code>Event</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEventView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Event</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEventView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include events in calendars which are children of
     *  this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only events whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveEventView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All events of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveEventView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

    
    /**
     *  A normalized view uses a single <code> Event </code> to represent a
     *  set of recurring events.
     */

    @OSID @Override
    public void useNormalizedEventView() {
        this.normalized = true;
        return;
    }


    /**
     *  A denormalized view expands recurring events into a series of <code>
     *  Events. </code>
     */

    @OSID @Override
    public void useDenormalizedEventView() {
        this.normalized = false;
        return;
    }


    /**
     *  Tests if a normalized or denormalized view is set.
     *
     *  @return <code>true</code> if normalized</code>,
     *          <code>false</code> if denormalized
     */

    protected boolean isNormalized() {
        return (this.normalized);
    }


    /**
     *  The returns from the lookup methods omit sequestered events.
     */

    @OSID @Override
    public void useSequesteredEventView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All events are returned including sequestered events.
     */

    @OSID @Override
    public void useUnsequesteredEventView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if undequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }


    /**
     *  Gets the <code>Event</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Event</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Event</code> and
     *  retained for compatibility.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventId <code>Id</code> of the
     *          <code>Event</code>
     *  @return the event
     *  @throws org.osid.NotFoundException <code>eventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>eventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent(org.osid.id.Id eventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.EventList events = getEvents()) {
            while (events.hasNext()) {
                org.osid.calendaring.Event event = events.getNextEvent();
                if (event.getId().equals(eventId)) {
                    return (event);
                }
            }
        } 

        throw new org.osid.NotFoundException(eventId + " not found");
    }


    /**
     *  Gets a <code>EventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  events specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Events</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In normalized mode, recurring events appear as a single event. In
     *  denormalized mode, each element in the recurring series appears as a
     *  separate event.
     *
     *  In effective mode, events are returned that are currently effective.
     *  In any effective mode, effective events and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEvents()</code>.
     *
     *  @param  eventIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>eventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByIds(org.osid.id.IdList eventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Event> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = eventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getEvent(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("event " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.event.LinkedEventList(ret));
    }


    /**
     *  Gets a <code>EventList</code> corresponding to the given event
     *  genus <code>Type</code> which does not include events of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEvents()</code>.
     *
     *  @param  eventGenusType an event genus type 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByGenusType(org.osid.type.Type eventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.EventGenusFilterList(getEvents(), eventGenusType));
    }


    /**
     *  Gets a <code>EventList</code> corresponding to the given event
     *  genus <code>Type</code> and include any additional events with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEvents()</code>.
     *
     *  @param  eventGenusType an event genus type 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByParentGenusType(org.osid.type.Type eventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEventsByGenusType(eventGenusType));
    }


    /**
     *  Gets a <code>EventList</code> containing the given
     *  event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEvents()</code>.
     *
     *  @param  eventRecordType an event record type 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByRecordType(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.EventRecordFilterList(getEvents(), eventRecordType));
    }


    /**
     *  Gets a list of events where the given date range falls within
     *  an event inclusive.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single event. In
     *  denormalized mode, each element in the recurring series appears as a
     *  separate event.
     *
     *  In effective mode, events are returned that are currently effective.
     *  In any effective mode, effective events and those currently expired
     *  are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsOnDate(org.osid.calendaring.DateTime from,
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.TemporalEventFilterList(getEvents(), from, to));
    }


    /**
     *  Gets an <code> EventList </code> that fall within the given
     *  range inclusive.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsInDateRange(org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilterList(new DateRangeFilter(from, to), getEvents()));
    }


    /**
     *  Gets the next upcoming events on this calendar.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  number the number of events
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getUpcomingEvents(long number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Event> ret = new java.util.ArrayList<>();

        try (org.osid.calendaring.EventList events = getEvents()) {
            org.osid.calendaring.DateTime now = net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.now();
            int n = 0;
        
            while (events.hasNext()) {
                if (n >= number) {
                    break;
                }
                
                org.osid.calendaring.Event event = events.getNextEvent();
                if (event.getStartDate().isGreater(now)) {
                    ret.add(event);
                    ++n;
                }
            }
        }

        return (new net.okapia.osid.jamocha.calendaring.event.LinkedEventList(ret));
    }


    /**
     *  Gets a list of events with the given location.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.NullArgumentException <code> locationId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilterList(new LocationFilter(locationId), getEvents()));
    }


    /**
     *  Gets an <code> EventList </code> at the given location where
     *  the given date range falls within the event dates
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> locationId,
     *          from</code>, or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocationOnDate(org.osid.id.Id locationId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.TemporalEventFilterList(getEventsByLocation(locationId), from, to));
    }


    /**
     *  Gets an <code> EventList </code> that fall within the given
     *  range inclusive at the given location.
     *
     *  <code> </code> In plenary mode, the returned list contains all
     *  known events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through this
     *  session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> locationId, from </code>
     *          , or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocationInDateRange(org.osid.id.Id locationId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilterList(new DateRangeFilter(from, to), getEventsByLocation(locationId)));
    }


    /**
     *  Gets a list of events with the given sponsor.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsor(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Event> ret = new java.util.ArrayList<>();

        try (org.osid.calendaring.EventList events = getEventsBySponsor(resourceId)) {
            while (events.hasNext()) {
                org.osid.calendaring.Event event = events.getNextEvent();
                if (event.hasSponsors()) {
                    try (org.osid.id.IdList sponsorIds = event.getSponsorIds()) {
                        org.osid.id.Id id = sponsorIds.getNextId();
                        if (id.equals(resourceId)) {
                            ret.add(event);
                        }
                    }
                }
            }
        }

        return (new net.okapia.osid.jamocha.calendaring.event.LinkedEventList(ret));
    }


    /**
     *  Gets an <code> EventList </code> with the given sponsor where
     *  the given date range falls within the event dates
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsorOnDate(org.osid.id.Id resourceId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.TemporalEventFilterList(getEventsBySponsor(resourceId), from, to));        
    }


    /**
     *  Gets an <code> EventList </code> that fall within the given range
     *  inclusive at the given location.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsorInDateRange(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilterList(new DateRangeFilter(from, to), getEventsBySponsor(resourceId)));
    }


    /**
     *  Gets all <code>Events</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Events</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.EventList getEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the event list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of events
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.EventList filterEventsOnViews(org.osid.calendaring.EventList list)
        throws org.osid.OperationFailedException {
            
        org.osid.calendaring.EventList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.event.EffectiveEventFilterList(ret);
        }

        if (isSequestered()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.event.SequesteredEventFilterList(ret);
        }

        return (ret);
    }

    
    public static class DateRangeFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilter {
         
        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;
        
         
        /**
         *  Constructs a new <code>DateRangeFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */
        
        public DateRangeFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to = to;

            return;
        }

         
        /**
         *  Used by the EventFilterList to filter the 
         *  event list based on date range.
         *
         *  @param event the event
         *  @return <code>true</code> to pass the event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.Event event) {
            if (event.getStartDate().isLess(this.from)) {
                return (false);
            }
                
            if (event.getStartDate().isGreater(this.to)) {
                return (false);
            }
                
            if (event.getEndDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }


    public static class LocationFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.event.EventFilter {
         
        private final org.osid.id.Id locationId;
         
         
        /**
         *  Constructs a new <code>LocationFilter</code>.
         *
         *  @param locationId the location to filter
         *  @throws org.osid.NullArgumentException
         *          <code>locationId</code> is <code>null</code>
         */
        
        public LocationFilter(org.osid.id.Id locationId) {
            nullarg(locationId, "location Id");
            this.locationId = locationId;
            return;
        }

         
        /**
         *  Used by the EventFilterList to filter the 
         *  event list based on location.
         *
         *  @param event the event
         *  @return <code>true</code> to pass the event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.Event event) {
            if (event.hasLocation()) {
                return (event.getLocationId().equals(this.locationId));
            } else {
                return (false);
            }
        }
    }
}
