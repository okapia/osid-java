//
// AbstractOsidQuery.java
//
//     Defines a simple OSID query to draw from.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQuery to extend. This class
 *  does nothing except for storing string match types.
 */

public abstract class AbstractOsidQuery
    implements org.osid.OsidQuery {

    private final java.util.Collection<org.osid.type.Type> stringMatchTypes = new java.util.HashSet<>();


    /**
     *  Gets the string matching types supported. A string match type 
     *  specifies the syntax of the string query, such as matching a word or 
     *  including a wildcard or regular expression. 
     *
     *  @return a list containing the supported string match types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStringMatchTypes() {
	return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stringMatchTypes));
    }


    /**
     *  Tests if the given string matching type is supported. 
     *
     *  @param stringMatchType a <code> Type </code> indicating a
     *         string match type
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException
     *          <code>stringMatchType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean supportsStringMatchType(org.osid.type.Type stringMatchType) {
	if (this.stringMatchTypes.contains(stringMatchType)) {
	    return (true);
	} else {
	    return (false);
	}
    }


    /**
     *  Adds a string match type.
     *
     *  @param stringMatchType
     *  @throws org.osid.NullArgumentException
     *          <code>stringMatchType</code> is <code>null</code>
     */

    protected void addStringMatchType(org.osid.type.Type stringMatchType) {
        nullarg(stringMatchType, "string match type");
	this.stringMatchTypes.add(stringMatchType);
	return;
    }


    /**
     *  Adds a keyword to match. Multiple keywords can be added to perform a 
     *  boolean <code> OR </code> among them. A keyword may be applied to any 
     *  of the elements defined in this object such as the display name, 
     *  description or any method defined in an interface implemented by this 
     *  object. 
     *
     *  @param  keyword keyword to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> keyword </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchKeyword(String keyword, org.osid.type.Type stringMatchType, boolean match) {
	if (!supportsStringMatchType(stringMatchType)) {
	    throw new org.osid.UnsupportedException(stringMatchType + " is not supported");
	}

	return;
    }


    /**
     *  Clears all keyword terms. 
     */

    @OSID @Override
    public void clearKeywordTerms() {
        return;
    }


    /**
     *  Matches any object. 
     *
     *  @param match <code> true </code> to match any object, <code>
     *         false </code> to match no objectsx
     */

    @OSID @Override
    public void matchAny(boolean match) {
        return;
    }


    /**
     *  Clears the match any terms. 
     */

    @OSID @Override
    public void clearAnyTerms() {
        return;
    }
}
