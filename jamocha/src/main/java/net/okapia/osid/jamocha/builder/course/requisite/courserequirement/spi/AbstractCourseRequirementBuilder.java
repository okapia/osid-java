//
// AbstractCourseRequirement.java
//
//     Defines a CourseRequirement builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.courserequirement.spi;


/**
 *  Defines a <code>CourseRequirement</code> builder.
 */

public abstract class AbstractCourseRequirementBuilder<T extends AbstractCourseRequirementBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.requisite.courserequirement.CourseRequirementMiter courseRequirement;


    /**
     *  Constructs a new <code>AbstractCourseRequirementBuilder</code>.
     *
     *  @param courseRequirement the course requirement to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCourseRequirementBuilder(net.okapia.osid.jamocha.builder.course.requisite.courserequirement.CourseRequirementMiter courseRequirement) {
        super(courseRequirement);
        this.courseRequirement = courseRequirement;
        return;
    }


    /**
     *  Builds the course requirement.
     *
     *  @return the new course requirement
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.requisite.CourseRequirement build() {
        (new net.okapia.osid.jamocha.builder.validator.course.requisite.courserequirement.CourseRequirementValidator(getValidations())).validate(this.courseRequirement);
        return (new net.okapia.osid.jamocha.builder.course.requisite.courserequirement.ImmutableCourseRequirement(this.courseRequirement));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the course requirement miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.requisite.courserequirement.CourseRequirementMiter getMiter() {
        return (this.courseRequirement);
    }


    /**
     *  Adds an alternative requisite.
     *
     *  @param requisite an alternative requisite
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisite</code> is <code>null</code>
     */

    public T altRequisite(org.osid.course.requisite.Requisite requisite) {
        getMiter().addAltRequisite(requisite);
        return (self());
    }


    /**
     *  Sets all the alternative requisites.
     *
     *  @param requisites a collection of alternative requisites
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>eequisites</code> is <code>null</code>
     */

    public T altRequisites(java.util.Collection<org.osid.course.requisite.Requisite> requisites) {
        getMiter().setAltRequisites(requisites);
        return (self());
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public T course(org.osid.course.Course course) {
        getMiter().setCourse(course);
        return (self());
    }


    /**
     *  Marks this record as requiring completion for the course.
     *
     *  @return the builder
     */

    public T requiresCompletion() {
        getMiter().setRequiresCompletion(true);
        return (self());
    }


    /**
     *  Marks this record as not requiring completion for the program.
     *
     *  @return the builder
     */

    public T notRequiresCompletion() {
        getMiter().setRequiresCompletion(false);
        return (self());
    }


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>timeframe</code>
     *          is <code>null</code>
     */

    public T timeframe(org.osid.calendaring.Duration timeframe) {
        getMiter().setTimeframe(timeframe);
        return (self());
    }


    /**
     *  Sets the minimum grade.
     *
     *  @param grade a minimum grade
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public T minimumGrade(org.osid.grading.Grade grade) {
        getMiter().setMinimumGrade(grade);
        return (self());
    }


    /**
     *  Sets the minimum score system.
     *
     *  @param system a minimum score system
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public T minimumScoreSystem(org.osid.grading.GradeSystem system) {
        getMiter().setMinimumScoreSystem(system);
        return (self());
    }


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T minimumScore(java.math.BigDecimal score) {
        getMiter().setMinimumScore(score);
        return (self());
    }


    /**
     *  Sets the minimum earned credits.
     *
     *  @param credits the minimum earned credits
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T minimumEarnedCredit(java.math.BigDecimal credits) {
        getMiter().setMinimumEarnedCredits(credits);
        return (self());
    }


    /**
     *  Adds a CourseRequirement record.
     *
     *  @param record a course requirement record
     *  @param recordType the type of course requirement record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.requisite.records.CourseRequirementRecord record, org.osid.type.Type recordType) {
        getMiter().addCourseRequirementRecord(record, recordType);
        return (self());
    }
}       


