//
// AbstractFederatingOrderLookupSession.java
//
//     An abstract federating adapter for an OrderLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  OrderLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingOrderLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.ordering.OrderLookupSession>
    implements org.osid.ordering.OrderLookupSession {

    private boolean parallel = false;
    private org.osid.ordering.Store store = new net.okapia.osid.jamocha.nil.ordering.store.UnknownStore();


    /**
     *  Constructs a new <code>AbstractFederatingOrderLookupSession</code>.
     */

    protected AbstractFederatingOrderLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.ordering.OrderLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Store/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.store.getId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.store);
    }


    /**
     *  Sets the <code>Store</code>.
     *
     *  @param  store the store for this session
     *  @throws org.osid.NullArgumentException <code>store</code>
     *          is <code>null</code>
     */

    protected void setStore(org.osid.ordering.Store store) {
        nullarg(store, "store");
        this.store = store;
        return;
    }


    /**
     *  Tests if this user can perform <code>Order</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOrders() {
        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            if (session.canLookupOrders()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Order</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOrderView() {
        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            session.useComparativeOrderView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Order</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOrderView() {
        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            session.usePlenaryOrderView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include orders in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            session.useFederatedStoreView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            session.useIsolatedStoreView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Order</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Order</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Order</code> and
     *  retained for compatibility.
     *
     *  @param  orderId <code>Id</code> of the
     *          <code>Order</code>
     *  @return the order
     *  @throws org.osid.NotFoundException <code>orderId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>orderId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder(org.osid.id.Id orderId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            try {
                return (session.getOrder(orderId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(orderId + " not found");
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  orders specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Orders</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  orderIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>orderIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByIds(org.osid.id.IdList orderIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.ordering.order.MutableOrderList ret = new net.okapia.osid.jamocha.ordering.order.MutableOrderList();

        try (org.osid.id.IdList ids = orderIds) {
            while (ids.hasNext()) {
                ret.addOrder(getOrder(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  order genus <code>Type</code> which does not include
     *  orders of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersByGenusType(orderGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  order genus <code>Type</code> and include any additional
     *  orders with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByParentGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersByParentGenusType(orderGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OrderList</code> containing the given
     *  order record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderRecordType an order record type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByRecordType(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersByRecordType(orderRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all orders corresponding to a customer
     *  <code>Id</code> In plenary mode, the returned list contains
     *  all known orders or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId the <code>Id</code> of the customer
     *  @return the returned <code>OrderList /code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersByCustomer(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all orders corresponding to a date
     *  range. Entries are returned with a submitted date that falsl
     *  between the requested dates inclusive. In plenary mode, the
     *  returned list contains all known orders or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>OrderList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByDate(org.osid.calendaring.DateTime from,
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all orders corresponding to a customer
     *  <code>Id</code> and date range. Entries are returned with
     *  submit dates that fall between the requested dates
     *  inclusive. In plenary mode, the returned list contains all
     *  known orders or an error results.  Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId the <code>Id</code> of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>OrderList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomerAndDate(org.osid.id.Id resourceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersByCustomerAndDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all orders corresponding to a product
     *  <code>Id</code> In plenary mode, the returned list contains
     *  all known orders or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  productId the <code>Id</code> of the product
     *  @return the returned <code>OrderList /code>
     *  @throws org.osid.NullArgumentException <code>productId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProduct(org.osid.id.Id productId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersForProduct(productId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all orders corresponding to a product
     *  <code>Id</code> and date range. Entries are returned with
     *  submit dates that fall between the requested dates
     *  inclusive. In plenary mode, the returned list contains all
     *  known orders or an error results.  Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  productId the <code>Id</code> of the product
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>OrderList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>productId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProductAndDate(org.osid.id.Id productId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrdersForProductAndDate(productId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Orders</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Orders</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOrders());
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all submitted and not closed orders. In plenary mode, the
     *  returned list contains all known entries or an error results.
     *  Otherwise, the returned list may contain only those entries
     *  that are accessible through this session.
     *
     *  @return a list of orders
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOpenOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getOpenOrders());
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all closed orders. In plenary mode, the returned list
     *  contains all known entries or an error results. Otherwise, the
     *  returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @return a list of orders
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.OrderList getClosedOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList ret = getOrderList();

        for (org.osid.ordering.OrderLookupSession session : getSessions()) {
            ret.addOrderList(session.getClosedOrders());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.ordering.order.FederatingOrderList getOrderList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.ordering.order.ParallelOrderList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.ordering.order.CompositeOrderList());
        }
    }
}
