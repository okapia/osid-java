//
// MutableIndexedMapPriceEnablerLookupSession
//
//    Implements a PriceEnabler lookup service backed by a collection of
//    priceEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.rules;


/**
 *  Implements a PriceEnabler lookup service backed by a collection of
 *  price enablers. The price enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some price enablers may be compatible
 *  with more types than are indicated through these price enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of price enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.core.ordering.rules.spi.AbstractIndexedMapPriceEnablerLookupSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapPriceEnablerLookupSession} with no price enablers.
     *
     *  @param store the store
     *  @throws org.osid.NullArgumentException {@code store}
     *          is {@code null}
     */

      public MutableIndexedMapPriceEnablerLookupSession(org.osid.ordering.Store store) {
        setStore(store);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPriceEnablerLookupSession} with a
     *  single price enabler.
     *  
     *  @param store the store
     *  @param  priceEnabler a single priceEnabler
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceEnabler} is {@code null}
     */

    public MutableIndexedMapPriceEnablerLookupSession(org.osid.ordering.Store store,
                                                  org.osid.ordering.rules.PriceEnabler priceEnabler) {
        this(store);
        putPriceEnabler(priceEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPriceEnablerLookupSession} using an
     *  array of price enablers.
     *
     *  @param store the store
     *  @param  priceEnablers an array of price enablers
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceEnablers} is {@code null}
     */

    public MutableIndexedMapPriceEnablerLookupSession(org.osid.ordering.Store store,
                                                  org.osid.ordering.rules.PriceEnabler[] priceEnablers) {
        this(store);
        putPriceEnablers(priceEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPriceEnablerLookupSession} using a
     *  collection of price enablers.
     *
     *  @param store the store
     *  @param  priceEnablers a collection of price enablers
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceEnablers} is {@code null}
     */

    public MutableIndexedMapPriceEnablerLookupSession(org.osid.ordering.Store store,
                                                  java.util.Collection<? extends org.osid.ordering.rules.PriceEnabler> priceEnablers) {

        this(store);
        putPriceEnablers(priceEnablers);
        return;
    }
    

    /**
     *  Makes a {@code PriceEnabler} available in this session.
     *
     *  @param  priceEnabler a price enabler
     *  @throws org.osid.NullArgumentException {@code priceEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putPriceEnabler(org.osid.ordering.rules.PriceEnabler priceEnabler) {
        super.putPriceEnabler(priceEnabler);
        return;
    }


    /**
     *  Makes an array of price enablers available in this session.
     *
     *  @param  priceEnablers an array of price enablers
     *  @throws org.osid.NullArgumentException {@code priceEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putPriceEnablers(org.osid.ordering.rules.PriceEnabler[] priceEnablers) {
        super.putPriceEnablers(priceEnablers);
        return;
    }


    /**
     *  Makes collection of price enablers available in this session.
     *
     *  @param  priceEnablers a collection of price enablers
     *  @throws org.osid.NullArgumentException {@code priceEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putPriceEnablers(java.util.Collection<? extends org.osid.ordering.rules.PriceEnabler> priceEnablers) {
        super.putPriceEnablers(priceEnablers);
        return;
    }


    /**
     *  Removes a PriceEnabler from this session.
     *
     *  @param priceEnablerId the {@code Id} of the price enabler
     *  @throws org.osid.NullArgumentException {@code priceEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removePriceEnabler(org.osid.id.Id priceEnablerId) {
        super.removePriceEnabler(priceEnablerId);
        return;
    }    
}
