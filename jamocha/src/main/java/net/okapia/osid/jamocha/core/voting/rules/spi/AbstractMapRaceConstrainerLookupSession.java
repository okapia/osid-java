//
// AbstractMapRaceConstrainerLookupSession
//
//    A simple framework for providing a RaceConstrainer lookup service
//    backed by a fixed collection of race constrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RaceConstrainer lookup service backed by a
 *  fixed collection of race constrainers. The race constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRaceConstrainerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractRaceConstrainerLookupSession
    implements org.osid.voting.rules.RaceConstrainerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.rules.RaceConstrainer> raceConstrainers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.rules.RaceConstrainer>());


    /**
     *  Makes a <code>RaceConstrainer</code> available in this session.
     *
     *  @param  raceConstrainer a race constrainer
     *  @throws org.osid.NullArgumentException <code>raceConstrainer<code>
     *          is <code>null</code>
     */

    protected void putRaceConstrainer(org.osid.voting.rules.RaceConstrainer raceConstrainer) {
        this.raceConstrainers.put(raceConstrainer.getId(), raceConstrainer);
        return;
    }


    /**
     *  Makes an array of race constrainers available in this session.
     *
     *  @param  raceConstrainers an array of race constrainers
     *  @throws org.osid.NullArgumentException <code>raceConstrainers<code>
     *          is <code>null</code>
     */

    protected void putRaceConstrainers(org.osid.voting.rules.RaceConstrainer[] raceConstrainers) {
        putRaceConstrainers(java.util.Arrays.asList(raceConstrainers));
        return;
    }


    /**
     *  Makes a collection of race constrainers available in this session.
     *
     *  @param  raceConstrainers a collection of race constrainers
     *  @throws org.osid.NullArgumentException <code>raceConstrainers<code>
     *          is <code>null</code>
     */

    protected void putRaceConstrainers(java.util.Collection<? extends org.osid.voting.rules.RaceConstrainer> raceConstrainers) {
        for (org.osid.voting.rules.RaceConstrainer raceConstrainer : raceConstrainers) {
            this.raceConstrainers.put(raceConstrainer.getId(), raceConstrainer);
        }

        return;
    }


    /**
     *  Removes a RaceConstrainer from this session.
     *
     *  @param  raceConstrainerId the <code>Id</code> of the race constrainer
     *  @throws org.osid.NullArgumentException <code>raceConstrainerId<code> is
     *          <code>null</code>
     */

    protected void removeRaceConstrainer(org.osid.id.Id raceConstrainerId) {
        this.raceConstrainers.remove(raceConstrainerId);
        return;
    }


    /**
     *  Gets the <code>RaceConstrainer</code> specified by its <code>Id</code>.
     *
     *  @param  raceConstrainerId <code>Id</code> of the <code>RaceConstrainer</code>
     *  @return the raceConstrainer
     *  @throws org.osid.NotFoundException <code>raceConstrainerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>raceConstrainerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainer getRaceConstrainer(org.osid.id.Id raceConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.rules.RaceConstrainer raceConstrainer = this.raceConstrainers.get(raceConstrainerId);
        if (raceConstrainer == null) {
            throw new org.osid.NotFoundException("raceConstrainer not found: " + raceConstrainerId);
        }

        return (raceConstrainer);
    }


    /**
     *  Gets all <code>RaceConstrainers</code>. In plenary mode, the returned
     *  list contains all known raceConstrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  raceConstrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RaceConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceconstrainer.ArrayRaceConstrainerList(this.raceConstrainers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceConstrainers.clear();
        super.close();
        return;
    }
}
