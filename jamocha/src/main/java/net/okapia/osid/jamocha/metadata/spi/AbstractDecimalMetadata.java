//
// AbstractDecimalMetadata.java
//
//     Defines a decimal Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a decimal Metadata.
 */

public abstract class AbstractDecimalMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private long scale = 2;

    private java.math.BigDecimal minimum = new java.math.BigDecimal("0");
    private java.math.BigDecimal maximum = new java.math.BigDecimal("100");

    private final java.util.Collection<java.math.BigDecimal> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<java.math.BigDecimal> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<java.math.BigDecimal> existing = new java.util.LinkedHashSet<>();    


    /**
     *  Constructs a new {@code AbstractDecimalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDecimalMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.DECIMAL, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractDecimalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDecimalMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.DECIMAL, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the number of digits to the right of the decimal point. 
     *
     *  @return the scale 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DECIMAL 
     *          </code> 
     */

    @OSID @Override
    public long getDecimalScale() {
        return (this.scale);
    }


    /**
     *  Sets the scale.
     *
     *  @param scale the number of digits to the right of the decimal
     *         point
     *  @throws org.osid.InvalidArgumentException {@code scale} is
     *          negative
     */

    protected void setScale(long scale) {
        cardinalarg(scale, "scale");
        this.scale = scale;
        return;
    }


    /**
     *  Gets the minimum decimal value. 
     *
     *  @return the minimum decimal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DECIMAL 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumDecimal() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum decimal value. 
     *
     *  @return the maximum decimal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DECIMAL 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMaximumDecimal() {
        return (this.maximum);
    }

    
    /**
     *  Sets the decimal range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     */

    protected void setDecimalRange(java.math.BigDecimal min, java.math.BigDecimal max) {
        nullarg(min, "min");
        nullarg(max, "max");

        if (min.compareTo(max) > 0) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.minimum = min;
        this.maximum = max;

        return;
    }


    /**
     *  Gets the set of acceptable decimal values. 
     *
     *  @return a set of decimals or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DECIMAL </code>
     */

    @OSID @Override
    public java.math.BigDecimal[] getDecimalSet() {
        return (this.set.toArray(new java.math.BigDecimal[this.set.size()]));
    }

    
    /**
     *  Sets the decimal set.
     *
     *  @param values a collection of accepted decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDecimalSet(java.util.Collection<java.math.BigDecimal> values) {
        this.set.clear();
        addToDecimalSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the decimal set.
     *
     *  @param values a collection of accepted decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToDecimalSet(java.util.Collection<java.math.BigDecimal> values) {
        nullarg(values, "decimal set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the decimal set.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *         null}
     */

    protected void addToDecimalSet(java.math.BigDecimal value) {
        nullarg(value, "decimal value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the decimal set.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *         null}
     */

    protected void removeFromDecimalSet(java.math.BigDecimal value) {
        nullarg(value, "decimal value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the decimal set.
     */

    protected void clearDecimalSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default decimal values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default decimal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DECIMAL </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public java.math.BigDecimal[] getDefaultDecimalValues() {
        return (this.defvals.toArray(new java.math.BigDecimal[this.defvals.size()]));
    }


    /**
     *  Sets the default decimal set.
     *
     *  @param values a collection of default decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        clearDefaultDecimalValues();
        addDefaultDecimalValues(values);
        return;
    }


    /**
     *  Adds a collection of default decimal values.
     *
     *  @param values a collection of default decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        nullarg(values, "default decimal values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultDecimalValue(java.math.BigDecimal value) {
        nullarg(value, "default decimal value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultDecimalValue(java.math.BigDecimal value) {
        nullarg(value, "default decimal value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default decimal values.
     */

    protected void clearDefaultDecimalValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing decimal values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing decimal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          DECIMAL </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getExistingDecimalValues() {
        return (this.existing.toArray(new java.math.BigDecimal[this.existing.size()]));
    }


    /**
     *  Sets the existing decimal set.
     *
     *  @param values a collection of existing decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        clearExistingDecimalValues();
        addExistingDecimalValues(values);
        return;
    }


    /**
     *  Adds a collection of existing decimal values.
     *
     *  @param values a collection of existing decimal values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingDecimalValues(java.util.Collection<java.math.BigDecimal> values) {
        nullarg(values, "existing decimal values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingDecimalValue(java.math.BigDecimal value) {
        nullarg(value, "existing decimal value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingDecimalValue(java.math.BigDecimal value) {
        nullarg(value, "existing decimal value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing decimal values.
     */

    protected void clearExistingDecimalValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }        
}