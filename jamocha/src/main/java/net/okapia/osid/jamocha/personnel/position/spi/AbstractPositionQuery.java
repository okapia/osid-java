//
// AbstractPositionQuery.java
//
//     A template for making a Position Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for positions.
 */

public abstract class AbstractPositionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.personnel.PositionQuery {

    private final java.util.Collection<org.osid.personnel.records.PositionQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets an organization <code> Id. </code> 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOrganizationId(org.osid.id.Id organizationId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears all organization <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrganizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OrganizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsOrganizationQuery() is false");
    }


    /**
     *  Clears all organization terms. 
     */

    @OSID @Override
    public void clearOrganizationTerms() {
        return;
    }


    /**
     *  Matches a title. 
     *
     *  @param  title a title 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches persons with any title. 
     *
     *  @param  match <code> true </code> to match positions with any title, 
     *          <code> false </code> to match positions with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears all title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Sets a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears all grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches positions with any level. 
     *
     *  @param  match <code> true </code> to match positions with any level, 
     *          <code> false </code> to match positions with no level 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        return;
    }


    /**
     *  Sets an objective <code> Id. </code> 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQualificationId(org.osid.id.Id objectiveId, boolean match) {
        return;
    }


    /**
     *  Clears all objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearQualificationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualificationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualificationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getQualificationQuery() {
        throw new org.osid.UnimplementedException("supportsQualificationQuery() is false");
    }


    /**
     *  Matches positions with any qualification. 
     *
     *  @param  match <code> true </code> to match positions with any 
     *          qualification, <code> false </code> to match positions with no 
     *          qualifications 
     */

    @OSID @Override
    public void matchAnyQualification(boolean match) {
        return;
    }


    /**
     *  Clears all qualification terms. 
     */

    @OSID @Override
    public void clearQualificationTerms() {
        return;
    }


    /**
     *  Matches a target appointments between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchTargetAppointments(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches positions with any target appointments. 
     *
     *  @param  match <code> true </code> to match positions with any target 
     *          appointments, <code> false </code> to match positions with no 
     *          target appointments 
     */

    @OSID @Override
    public void matchAnyTargetAppointments(boolean match) {
        return;
    }


    /**
     *  Clears all target appointments terms. 
     */

    @OSID @Override
    public void clearTargetAppointmentsTerms() {
        return;
    }


    /**
     *  Matches a required commitment between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchRequiredCommitment(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches positions with any required commitment. 
     *
     *  @param  match <code> true </code> to match positions with any required 
     *          commitment, <code> false </code> to match positions with no 
     *          required commitment 
     */

    @OSID @Override
    public void matchAnyRequiredCommitment(boolean match) {
        return;
    }


    /**
     *  Clears all required commitment terms. 
     */

    @OSID @Override
    public void clearRequiredCommitmentTerms() {
        return;
    }


    /**
     *  Matches a low salary between the given range inclusive. 
     *
     *  @param  from a starting salary 
     *  @param  to an ending salary 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLowSalaryRange(org.osid.financials.Currency from, 
                                    org.osid.financials.Currency to, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches positions with any low salary. 
     *
     *  @param  match <code> true </code> to match positions with any low 
     *          salary range, commitment, <code> false </code> to match 
     *          positions with no low salary range 
     */

    @OSID @Override
    public void matchAnyLowSalaryRange(boolean match) {
        return;
    }


    /**
     *  Clears all low salary terms. 
     */

    @OSID @Override
    public void clearLowSalaryRangeTerms() {
        return;
    }


    /**
     *  Matches a midpoint salary between the given range inclusive. 
     *
     *  @param  from a starting salary 
     *  @param  to an ending salary 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchMidpointSalaryRange(org.osid.financials.Currency from, 
                                         org.osid.financials.Currency to, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches positions with any midpoint salary. 
     *
     *  @param  match <code> true </code> to match positions with any midpoint 
     *          salary range, commitment, <code> false </code> to match 
     *          positions with no midpoint salary range 
     */

    @OSID @Override
    public void matchAnyMidpointSalaryRange(boolean match) {
        return;
    }


    /**
     *  Clears all midpoint salary terms. 
     */

    @OSID @Override
    public void clearMidpointSalaryRangeTerms() {
        return;
    }


    /**
     *  Matches a high salary between the given range inclusive. 
     *
     *  @param  from a starting salary 
     *  @param  to an ending salary 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchHighSalaryRange(org.osid.financials.Currency from, 
                                     org.osid.financials.Currency to, 
                                     boolean match) {
        return;
    }


    /**
     *  Matches positions with any high salary. 
     *
     *  @param  match <code> true </code> to match positions with any high 
     *          salary range, <code> false </code> to match positions with no 
     *          high salary range 
     */

    @OSID @Override
    public void matchAnyHighSalaryRange(boolean match) {
        return;
    }


    /**
     *  Clears all high salary terms. 
     */

    @OSID @Override
    public void clearHighSalaryRangeTerms() {
        return;
    }


    /**
     *  Matches a compensation frequency between the given range inclusive. 
     *
     *  @param  low low range of time frequency 
     *  @param  high high range of time frequency 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> frequency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompensationFrequency(org.osid.calendaring.Duration low, 
                                           org.osid.calendaring.Duration high, 
                                           boolean match) {
        return;
    }


    /**
     *  Matches positions with any compensation frequency. 
     *
     *  @param  match <code> true </code> to match positions with any 
     *          compensation frequency, <code> false </code> to match 
     *          positions with no compensation frequency 
     */

    @OSID @Override
    public void matchAnyCompensationFrequency(boolean match) {
        return;
    }


    /**
     *  Clears all compensation frequency terms. 
     */

    @OSID @Override
    public void clearCompensationFrequencyTerms() {
        return;
    }


    /**
     *  Matches exempt positions. 
     *
     *  @param  match <code> true </code> to match exempt positions, <code> 
     *          false </code> to match non-exempt positions 
     */

    @OSID @Override
    public void matchExempt(boolean match) {
        return;
    }


    /**
     *  Matches positions with any exempt flag set. 
     *
     *  @param  match <code> true </code> to match positions with any exempt 
     *          status,, <code> false </code> to match positions with no 
     *          exempt status 
     */

    @OSID @Override
    public void matchAnyExempt(boolean match) {
        return;
    }


    /**
     *  Clears all exempt terms. 
     */

    @OSID @Override
    public void clearExemptTerms() {
        return;
    }


    /**
     *  Matches a benefits type. 
     *
     *  @param  type a benefits type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> type </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBenefitsType(org.osid.type.Type type, boolean match) {
        return;
    }


    /**
     *  Matches positions with any benefits type. 
     *
     *  @param  match <code> true </code> to match positions with any benefits 
     *          type, <code> false </code> to match positions with no benefits 
     *          type 
     */

    @OSID @Override
    public void matchAnyBenefitsType(boolean match) {
        return;
    }


    /**
     *  Clears all benefits type terms. 
     */

    @OSID @Override
    public void clearBenefitsTypeTerms() {
        return;
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match positions 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given position query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a position implementing the requested record.
     *
     *  @param positionRecordType a position record type
     *  @return the position query record
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(positionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PositionQueryRecord getPositionQueryRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PositionQueryRecord record : this.records) {
            if (record.implementsRecordType(positionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this position query. 
     *
     *  @param positionQueryRecord position query record
     *  @param positionRecordType position record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPositionQueryRecord(org.osid.personnel.records.PositionQueryRecord positionQueryRecord, 
                                          org.osid.type.Type positionRecordType) {

        addRecordType(positionRecordType);
        nullarg(positionQueryRecord, "position query record");
        this.records.add(positionQueryRecord);        
        return;
    }
}
