//
// AbstractIndexedMapGradeSystemLookupSession.java
//
//    A simple framework for providing a GradeSystem lookup service
//    backed by a fixed collection of grade systems with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a GradeSystem lookup service backed by a
 *  fixed collection of grade systems. The grade systems are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some grade systems may be compatible
 *  with more types than are indicated through these grade system
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradeSystems</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapGradeSystemLookupSession
    extends AbstractMapGradeSystemLookupSession
    implements org.osid.grading.GradeSystemLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.grading.GradeSystem> gradeSystemsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.GradeSystem>());
    private final MultiMap<org.osid.type.Type, org.osid.grading.GradeSystem> gradeSystemsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.GradeSystem>());


    /**
     *  Makes a <code>GradeSystem</code> available in this session.
     *
     *  @param  gradeSystem a grade system
     *  @throws org.osid.NullArgumentException <code>gradeSystem<code> is
     *          <code>null</code>
     */

    @Override
    protected void putGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        super.putGradeSystem(gradeSystem);

        this.gradeSystemsByGenus.put(gradeSystem.getGenusType(), gradeSystem);
        
        try (org.osid.type.TypeList types = gradeSystem.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradeSystemsByRecord.put(types.getNextType(), gradeSystem);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a grade system from this session.
     *
     *  @param gradeSystemId the <code>Id</code> of the grade system
     *  @throws org.osid.NullArgumentException <code>gradeSystemId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeGradeSystem(org.osid.id.Id gradeSystemId) {
        org.osid.grading.GradeSystem gradeSystem;
        try {
            gradeSystem = getGradeSystem(gradeSystemId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.gradeSystemsByGenus.remove(gradeSystem.getGenusType());

        try (org.osid.type.TypeList types = gradeSystem.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradeSystemsByRecord.remove(types.getNextType(), gradeSystem);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeGradeSystem(gradeSystemId);
        return;
    }


    /**
     *  Gets a <code>GradeSystemList</code> corresponding to the given
     *  grade system genus <code>Type</code> which does not include
     *  grade systems of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known grade systems or an error results. Otherwise,
     *  the returned list may contain only those grade systems that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  gradeSystemGenusType a grade system genus type 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByGenusType(org.osid.type.Type gradeSystemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.gradeSystemsByGenus.get(gradeSystemGenusType)));
    }


    /**
     *  Gets a <code>GradeSystemList</code> containing the given
     *  grade system record <code>Type</code>. In plenary mode, the
     *  returned list contains all known grade systems or an error
     *  results. Otherwise, the returned list may contain only those
     *  grade systems that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  gradeSystemRecordType a grade system record type 
     *  @return the returned <code>gradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByRecordType(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.gradeSystemsByRecord.get(gradeSystemRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradeSystemsByGenus.clear();
        this.gradeSystemsByRecord.clear();

        super.close();

        return;
    }
}
