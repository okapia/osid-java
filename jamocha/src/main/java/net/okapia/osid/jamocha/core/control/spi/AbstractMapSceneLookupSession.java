//
// AbstractMapSceneLookupSession
//
//    A simple framework for providing a Scene lookup service
//    backed by a fixed collection of scenes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Scene lookup service backed by a
 *  fixed collection of scenes. The scenes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Scenes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSceneLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractSceneLookupSession
    implements org.osid.control.SceneLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.Scene> scenes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.Scene>());


    /**
     *  Makes a <code>Scene</code> available in this session.
     *
     *  @param  scene a scene
     *  @throws org.osid.NullArgumentException <code>scene<code>
     *          is <code>null</code>
     */

    protected void putScene(org.osid.control.Scene scene) {
        this.scenes.put(scene.getId(), scene);
        return;
    }


    /**
     *  Makes an array of scenes available in this session.
     *
     *  @param  scenes an array of scenes
     *  @throws org.osid.NullArgumentException <code>scenes<code>
     *          is <code>null</code>
     */

    protected void putScenes(org.osid.control.Scene[] scenes) {
        putScenes(java.util.Arrays.asList(scenes));
        return;
    }


    /**
     *  Makes a collection of scenes available in this session.
     *
     *  @param  scenes a collection of scenes
     *  @throws org.osid.NullArgumentException <code>scenes<code>
     *          is <code>null</code>
     */

    protected void putScenes(java.util.Collection<? extends org.osid.control.Scene> scenes) {
        for (org.osid.control.Scene scene : scenes) {
            this.scenes.put(scene.getId(), scene);
        }

        return;
    }


    /**
     *  Removes a Scene from this session.
     *
     *  @param  sceneId the <code>Id</code> of the scene
     *  @throws org.osid.NullArgumentException <code>sceneId<code> is
     *          <code>null</code>
     */

    protected void removeScene(org.osid.id.Id sceneId) {
        this.scenes.remove(sceneId);
        return;
    }


    /**
     *  Gets the <code>Scene</code> specified by its <code>Id</code>.
     *
     *  @param  sceneId <code>Id</code> of the <code>Scene</code>
     *  @return the scene
     *  @throws org.osid.NotFoundException <code>sceneId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>sceneId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Scene getScene(org.osid.id.Id sceneId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.Scene scene = this.scenes.get(sceneId);
        if (scene == null) {
            throw new org.osid.NotFoundException("scene not found: " + sceneId);
        }

        return (scene);
    }


    /**
     *  Gets all <code>Scenes</code>. In plenary mode, the returned
     *  list contains all known scenes or an error
     *  results. Otherwise, the returned list may contain only those
     *  scenes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Scenes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.scene.ArraySceneList(this.scenes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.scenes.clear();
        super.close();
        return;
    }
}
