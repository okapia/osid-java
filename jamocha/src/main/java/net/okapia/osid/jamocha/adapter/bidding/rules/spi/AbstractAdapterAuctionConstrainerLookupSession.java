//
// AbstractAdapterAuctionConstrainerLookupSession.java
//
//    An AuctionConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AuctionConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterAuctionConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.rules.AuctionConstrainerLookupSession {

    private final org.osid.bidding.rules.AuctionConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuctionConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionConstrainerLookupSession(org.osid.bidding.rules.AuctionConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AuctionHouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform {@code AuctionConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAuctionConstrainers() {
        return (this.session.canLookupAuctionConstrainers());
    }


    /**
     *  A complete view of the {@code AuctionConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionConstrainerView() {
        this.session.useComparativeAuctionConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code AuctionConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionConstrainerView() {
        this.session.usePlenaryAuctionConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction constrainers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auction constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionConstrainerView() {
        this.session.useActiveAuctionConstrainerView();
        return;
    }


    /**
     *  Active and inactive auction constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionConstrainerView() {
        this.session.useAnyStatusAuctionConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code AuctionConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AuctionConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AuctionConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @param auctionConstrainerId {@code Id} of the {@code AuctionConstrainer}
     *  @return the auction constrainer
     *  @throws org.osid.NotFoundException {@code auctionConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code auctionConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainer getAuctionConstrainer(org.osid.id.Id auctionConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainer(auctionConstrainerId));
    }


    /**
     *  Gets an {@code AuctionConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AuctionConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @param  auctionConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AuctionConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByIds(org.osid.id.IdList auctionConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainersByIds(auctionConstrainerIds));
    }


    /**
     *  Gets an {@code AuctionConstrainerList} corresponding to the given
     *  auction constrainer genus {@code Type} which does not include
     *  auction constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  @param  auctionConstrainerGenusType an auctionConstrainer genus type 
     *  @return the returned {@code AuctionConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByGenusType(org.osid.type.Type auctionConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainersByGenusType(auctionConstrainerGenusType));
    }


    /**
     *  Gets an {@code AuctionConstrainerList} corresponding to the given
     *  auction constrainer genus {@code Type} and include any additional
     *  auction constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  @param  auctionConstrainerGenusType an auctionConstrainer genus type 
     *  @return the returned {@code AuctionConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByParentGenusType(org.osid.type.Type auctionConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainersByParentGenusType(auctionConstrainerGenusType));
    }


    /**
     *  Gets an {@code AuctionConstrainerList} containing the given
     *  auction constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known auction
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  @param  auctionConstrainerRecordType an auctionConstrainer record type 
     *  @return the returned {@code AuctionConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByRecordType(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainersByRecordType(auctionConstrainerRecordType));
    }


    /**
     *  Gets all {@code AuctionConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @return a list of {@code AuctionConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainers());
    }
}
