//
// AbstractProvisionNotificationSession.java
//
//     A template for making ProvisionNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Provision} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Provision} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for provision entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractProvisionNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.ProvisionNotificationSession {

    private boolean federated = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }

    
    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the {@code Distributor}.
     *
     *  @param distributor the distributor for this session
     *  @throws org.osid.NullArgumentException {@code distributor}
     *          is {@code null}
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can register for {@code Provision}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForProvisionNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisions in distributors which are
     *  children of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new provisions. {@code
     *  ProvisionReceiver.newProvision()} is invoked when a new {@code
     *  Provision} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new provisions for the given
     *  request {@code Id}. {@code ProvisionReceiver.newProvision()}
     *  is invoked when a new {@code Provision} is created.
     *
     *  @param  requestId the {@code Id} of the request to monitor 
     *  @throws org.osid.NullArgumentException {@code requestId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewProvisionsForRequest(org.osid.id.Id requestId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new provisions for the given
     *  broker {@code Id}. {@code ProvisionReceiver.newProvision()} is
     *  invoked when a new {@code Provision} is created.
     *
     *  @param  brokerId the {@code Id} of the broker to monitor 
     *  @throws org.osid.NullArgumentException {@code brokerId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewProvisionsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new provisions for the given
     *  provisionable {@code Id}. {@code
     *  ProvisionReceiver.newProvision()} is invoked when a new {@code
     *  Provision} is created.
     *
     *  @param  provisionableId the {@code Id} of the provisionable to monitor
     *  @throws org.osid.NullArgumentException {@code provisionableId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProvisionsForProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new provisions for the given
     *  recipient {@code Id}. {@code ProvisionReceiver.newProvision()}
     *  is invoked when a new {@code Provision} is created.
     *
     *  @param  resourceId the {@code Id} of the recipient to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProvisionsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated provisions. {@code
     *  ProvisionReceiver.changedProvision()} is invoked when a
     *  provision is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated provisions for the given
     *  request {@code Id}. {@code
     *  ProvisionReceiver.changedProvision()} is invoked when a
     *  provision in this distributor is changed.
     *
     *  @param  requestId the {@code Id} of the request to monitor 
     *  @throws org.osid.NullArgumentException {@code requestId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedProvisionsForRequest(org.osid.id.Id requestId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated provisions for the given
     *  broker {@code Id}. {@code
     *  ProvisionReceiver.changedProvision()} is invoked when a
     *  provision in this distributor is changed.
     *
     *  @param  brokerId the {@code Id} of the broker to monitor 
     *  @throws org.osid.NullArgumentException {@code brokerId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedProvisionsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated provisions for the given
     *  provisionable {@code Id}. {@code
     *  ProvisionReceiver.changedProvision()} is invoked when a {@code
     *  Provision} in this distributor is changed.
     *
     *  @param  provisionableId the {@code Id} of the provisionable to monitor
     *  @throws org.osid.NullArgumentException {@code provisionableId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProvisionsForProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated provisions for the given
     *  recipient {@code Id}. {@code
     *  ProvisionReceiver.changedProvision()} is invoked when a {@code
     *  Provision} in this distributor is changed.
     *
     *  @param  resourceId the {@code Id} of the recipient to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProvisionsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated provision. {@code
     *  ProvisionReceiver.changedProvision()} is invoked when the
     *  specified provision is changed.
     *
     *  @param provisionId the {@code Id} of the {@code Provision} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code provisionId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProvision(org.osid.id.Id provisionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted provisions. {@code
     *  ProvisionReceiver.deletedProvision()} is invoked when a
     *  provision is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted provisions for the given
     *  request {@code Id.} {@code
     *  ProvisionReceiver.deletedProvision()} is invoked when a
     *  provision in this distributor is removed or deleted.
     *
     *  @param  requestId the {@code Id} of the request to monitor 
     *  @throws org.osid.NullArgumentException {@code requestId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedProvisionsForRequest(org.osid.id.Id requestId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted provisions for the given
     *  broker {@code Id}. {@code
     *  ProvisionReceiver.deletedProvision()} is invoked when a
     *  provision in this distributor is removed or deleted.
     *
     *  @param  brokerId the {@code Id} of the broker to monitor 
     *  @throws org.osid.NullArgumentException {@code brokerId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedProvisionsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted provisions for the given
     *  provisionable {@code Id}. {@code
     *  ProvisionReceiver.deletedProvision()} is invoked when a {@code
     *  Provision} is deleted or removed from this distributor.
     *
     *  @param  provisionableId the {@code Id} of the provisionable to monitor
     *  @throws org.osid.NullArgumentException {@code provisionableId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedProvisionsForProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted provisions for the given
     *  recipient {@code Id}. {@code
     *  ProvisionReceiver.deletedProvision()} is invoked when a {@code
     *  Provision} is deleted or removed from this distributor.
     *
     *  @param  resourceId the {@code Id} of the recipient to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedProvisionsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted provision. {@code
     *  ProvisionReceiver.deletedProvision()} is invoked when the
     *  specified provision is deleted.
     *
     *  @param provisionId the {@code Id} of the
     *          {@code Provision} to monitor
     *  @throws org.osid.NullArgumentException {@code provisionId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProvision(org.osid.id.Id provisionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
