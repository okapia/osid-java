//
// AbstractAssemblyRelationshipEnablerQuery.java
//
//     A RelationshipEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.relationship.rules.relationshipenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RelationshipEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyRelationshipEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.relationship.rules.RelationshipEnablerQuery,
               org.osid.relationship.rules.RelationshipEnablerQueryInspector,
               org.osid.relationship.rules.RelationshipEnablerSearchOrder {

    private final java.util.Collection<org.osid.relationship.rules.records.RelationshipEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.relationship.rules.records.RelationshipEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRelationshipEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRelationshipEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the relationship. 
     *
     *  @param  relationshipId the relationship <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relationshipId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRelationshipId(org.osid.id.Id relationshipId, 
                                         boolean match) {
        getAssembler().addIdTerm(getRuledRelationshipIdColumn(), relationshipId, match);
        return;
    }


    /**
     *  Clears the relationship <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRelationshipIdTerms() {
        getAssembler().clearTerms(getRuledRelationshipIdColumn());
        return;
    }


    /**
     *  Gets the relationship <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRelationshipIdTerms() {
        return (getAssembler().getIdTerms(getRuledRelationshipIdColumn()));
    }


    /**
     *  Gets the RuledRelationshipId column name.
     *
     * @return the column name
     */

    protected String getRuledRelationshipIdColumn() {
        return ("ruled_relationship_id");
    }


    /**
     *  Tests if a <code> RelationshipQuery </code> is available. 
     *
     *  @return <code> true </code> if a relationship query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relationship. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the relationship query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRuledRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRelationshipQuery() is false");
    }


    /**
     *  Matches enablers mapped to any relationship. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          relationship, <code> false </code> to match enablers mapped to 
     *          no relationship 
     */

    @OSID @Override
    public void matchAnyRuledRelationship(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledRelationshipColumn(), match);
        return;
    }


    /**
     *  Clears the relationship query terms. 
     */

    @OSID @Override
    public void clearRuledRelationshipTerms() {
        getAssembler().clearTerms(getRuledRelationshipColumn());
        return;
    }


    /**
     *  Gets the relationship query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQueryInspector[] getRuledRelationshipTerms() {
        return (new org.osid.relationship.RelationshipQueryInspector[0]);
    }


    /**
     *  Gets the RuledRelationship column name.
     *
     * @return the column name
     */

    protected String getRuledRelationshipColumn() {
        return ("ruled_relationship");
    }


    /**
     *  Matches enablers mapped to a family. 
     *
     *  @param  familyId the family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFamilyId(org.osid.id.Id familyId, boolean match) {
        getAssembler().addIdTerm(getFamilyIdColumn(), familyId, match);
        return;
    }


    /**
     *  Clears the family <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFamilyIdTerms() {
        getAssembler().clearTerms(getFamilyIdColumn());
        return;
    }


    /**
     *  Gets the family <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFamilyIdTerms() {
        return (getAssembler().getIdTerms(getFamilyIdColumn()));
    }


    /**
     *  Gets the FamilyId column name.
     *
     * @return the column name
     */

    protected String getFamilyIdColumn() {
        return ("family_id");
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a family. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsFamilyQuery() is false");
    }


    /**
     *  Clears the family query terms. 
     */

    @OSID @Override
    public void clearFamilyTerms() {
        getAssembler().clearTerms(getFamilyColumn());
        return;
    }


    /**
     *  Gets the family query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }


    /**
     *  Gets the Family column name.
     *
     * @return the column name
     */

    protected String getFamilyColumn() {
        return ("family");
    }


    /**
     *  Tests if this relationshipEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  relationshipEnablerRecordType a relationship enabler record type 
     *  @return <code>true</code> if the relationshipEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type relationshipEnablerRecordType) {
        for (org.osid.relationship.rules.records.RelationshipEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relationshipEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  relationshipEnablerRecordType the relationship enabler record type 
     *  @return the relationship enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.rules.records.RelationshipEnablerQueryRecord getRelationshipEnablerQueryRecord(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.rules.records.RelationshipEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relationshipEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  relationshipEnablerRecordType the relationship enabler record type 
     *  @return the relationship enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord getRelationshipEnablerQueryInspectorRecord(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(relationshipEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param relationshipEnablerRecordType the relationship enabler record type
     *  @return the relationship enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.rules.records.RelationshipEnablerSearchOrderRecord getRelationshipEnablerSearchOrderRecord(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.rules.records.RelationshipEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(relationshipEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this relationship enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param relationshipEnablerQueryRecord the relationship enabler query record
     *  @param relationshipEnablerQueryInspectorRecord the relationship enabler query inspector
     *         record
     *  @param relationshipEnablerSearchOrderRecord the relationship enabler search order record
     *  @param relationshipEnablerRecordType relationship enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerQueryRecord</code>,
     *          <code>relationshipEnablerQueryInspectorRecord</code>,
     *          <code>relationshipEnablerSearchOrderRecord</code> or
     *          <code>relationshipEnablerRecordTyperelationshipEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addRelationshipEnablerRecords(org.osid.relationship.rules.records.RelationshipEnablerQueryRecord relationshipEnablerQueryRecord, 
                                      org.osid.relationship.rules.records.RelationshipEnablerQueryInspectorRecord relationshipEnablerQueryInspectorRecord, 
                                      org.osid.relationship.rules.records.RelationshipEnablerSearchOrderRecord relationshipEnablerSearchOrderRecord, 
                                      org.osid.type.Type relationshipEnablerRecordType) {

        addRecordType(relationshipEnablerRecordType);

        nullarg(relationshipEnablerQueryRecord, "relationship enabler query record");
        nullarg(relationshipEnablerQueryInspectorRecord, "relationship enabler query inspector record");
        nullarg(relationshipEnablerSearchOrderRecord, "relationship enabler search odrer record");

        this.queryRecords.add(relationshipEnablerQueryRecord);
        this.queryInspectorRecords.add(relationshipEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(relationshipEnablerSearchOrderRecord);
        
        return;
    }
}
