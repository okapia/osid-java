//
// AbstractForumQueryInspector.java
//
//     A template for making a ForumQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for forums.
 */

public abstract class AbstractForumQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.forum.ForumQueryInspector {

    private final java.util.Collection<org.osid.forum.records.ForumQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the reply <code> Id </code> terms. 
     *
     *  @return the reply <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReplyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the reply terms. 
     *
     *  @return the reply terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getReplyTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the post <code> Id </code> terms. 
     *
     *  @return the post <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the post terms. 
     *
     *  @return the post terms 
     */

    @OSID @Override
    public org.osid.forum.PostQueryInspector[] getPostTerms() {
        return (new org.osid.forum.PostQueryInspector[0]);
    }


    /**
     *  Gets the ancestor forum <code> Id </code> terms. 
     *
     *  @return the ancestor forum <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorForumIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor forum terms. 
     *
     *  @return the ancestor forum terms 
     */

    @OSID @Override
    public org.osid.forum.ForumQueryInspector[] getAncestorForumTerms() {
        return (new org.osid.forum.ForumQueryInspector[0]);
    }


    /**
     *  Gets the descendant forum <code> Id </code> terms. 
     *
     *  @return the descendant forum <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantForumIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant forum terms. 
     *
     *  @return the descendant forum terms 
     */

    @OSID @Override
    public org.osid.forum.ForumQueryInspector[] getDescendantForumTerms() {
        return (new org.osid.forum.ForumQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given forum query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a forum implementing the requested record.
     *
     *  @param forumRecordType a forum record type
     *  @return the forum query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(forumRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ForumQueryInspectorRecord getForumQueryInspectorRecord(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ForumQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(forumRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(forumRecordType + " is not supported");
    }


    /**
     *  Adds a record to this forum query. 
     *
     *  @param forumQueryInspectorRecord forum query inspector
     *         record
     *  @param forumRecordType forum record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addForumQueryInspectorRecord(org.osid.forum.records.ForumQueryInspectorRecord forumQueryInspectorRecord, 
                                                   org.osid.type.Type forumRecordType) {

        addRecordType(forumRecordType);
        nullarg(forumRecordType, "forum record type");
        this.records.add(forumQueryInspectorRecord);        
        return;
    }
}
