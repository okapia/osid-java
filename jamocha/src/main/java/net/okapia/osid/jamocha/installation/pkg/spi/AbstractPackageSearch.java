//
// AbstractPackageSearch.java
//
//     A template for making a Package Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing package searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPackageSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.installation.PackageSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.installation.records.PackageSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.installation.PackageSearchOrder pkgSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of packages. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  pkgIds list of packages
     *  @throws org.osid.NullArgumentException
     *          <code>pkgIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPackages(org.osid.id.IdList pkgIds) {
        while (pkgIds.hasNext()) {
            try {
                this.ids.add(pkgIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPackages</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of package Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPackageIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  pkgSearchOrder package search order 
     *  @throws org.osid.NullArgumentException
     *          <code>pkgSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>pkgSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPackageResults(org.osid.installation.PackageSearchOrder pkgSearchOrder) {
	this.pkgSearchOrder = pkgSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.installation.PackageSearchOrder getPackageSearchOrder() {
	return (this.pkgSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given package search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a package implementing the requested record.
     *
     *  @param pkgSearchRecordType a package search record
     *         type
     *  @return the package search record
     *  @throws org.osid.NullArgumentException
     *          <code>pkgSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pkgSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageSearchRecord getPackageSearchRecord(org.osid.type.Type pkgSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.installation.records.PackageSearchRecord record : this.records) {
            if (record.implementsRecordType(pkgSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this package search. 
     *
     *  @param pkgSearchRecord package search record
     *  @param pkgSearchRecordType pkg search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPackageSearchRecord(org.osid.installation.records.PackageSearchRecord pkgSearchRecord, 
                                           org.osid.type.Type pkgSearchRecordType) {

        addRecordType(pkgSearchRecordType);
        this.records.add(pkgSearchRecord);        
        return;
    }
}
