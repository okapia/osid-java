//
// AbstractPoolConstrainerLookupSession.java
//
//    A starter implementation framework for providing a PoolConstrainer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a PoolConstrainer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPoolConstrainers(), this other methods may need to be
 *  overridden for better performance.
 */

public abstract class AbstractPoolConstrainerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    

    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>PoolConstrainer</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPoolConstrainers() {
        return (true);
    }


    /**
     *  A complete view of the <code>PoolConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolConstrainerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>PoolConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolConstrainerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool constrainers in distributors which are
     *  children of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active pool constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActivePoolConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive pool constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>PoolConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PoolConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>PoolConstrainer</code> and retained for compatibility.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @param  poolConstrainerId <code>Id</code> of the
     *          <code>PoolConstrainer</code>
     *  @return the pool constrainer
     *  @throws org.osid.NotFoundException <code>poolConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>poolConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainer getPoolConstrainer(org.osid.id.Id poolConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.rules.PoolConstrainerList poolConstrainers = getPoolConstrainers()) {
            while (poolConstrainers.hasNext()) {
                org.osid.provisioning.rules.PoolConstrainer poolConstrainer = poolConstrainers.getNextPoolConstrainer();
                if (poolConstrainer.getId().equals(poolConstrainerId)) {
                    return (poolConstrainer);
                }
            }
        } 

        throw new org.osid.NotFoundException(poolConstrainerId + " not found");
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  poolConstrainers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>PoolConstrainers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPoolConstrainers()</code>.
     *
     *  @param  poolConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByIds(org.osid.id.IdList poolConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.rules.PoolConstrainer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = poolConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPoolConstrainer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("pool constrainer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.LinkedPoolConstrainerList(ret));
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given pool constrainer genus <code>Type</code> which does not
     *  include pool constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPoolConstrainers()</code>.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.rules.poolconstrainer.PoolConstrainerGenusFilterList(getPoolConstrainers(), poolConstrainerGenusType));
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given pool constrainer genus <code>Type</code> and include any
     *  additional pool constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPoolConstrainers()</code>.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByParentGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPoolConstrainersByGenusType(poolConstrainerGenusType));
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> containing the given
     *  pool constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPoolConstrainers()</code>.
     *
     *  @param poolConstrainerRecordType a poolConstrainer record type
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByRecordType(org.osid.type.Type poolConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.rules.poolconstrainer.PoolConstrainerRecordFilterList(getPoolConstrainers(), poolConstrainerRecordType));
    }


    /**
     *  Gets all <code>PoolConstrainers</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @return a list of <code>PoolConstrainers</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the pool constrainer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of pool constrainers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.rules.PoolConstrainerList filterPoolConstrainersOnViews(org.osid.provisioning.rules.PoolConstrainerList list)
        throws org.osid.OperationFailedException {

        org.osid.provisioning.rules.PoolConstrainerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.provisioning.rules.poolconstrainer.ActivePoolConstrainerFilterList(ret);
        }

        return (ret);
    }
}
