//
// AbstractSummary.java
//
//     Defines a Summary.
//
//
// Tom Coppeto
// Okapia
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.summary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Summary</code>.
 */

public abstract class AbstractSummary
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendium
    implements org.osid.billing.payment.Summary {

    private org.osid.billing.Customer customer;
    private org.osid.billing.Period period;
    private boolean credit = false;
    private org.osid.financials.Currency balance;
    private org.osid.calendaring.DateTime lastPaymentDate;
    private org.osid.financials.Currency lastPaymentAmount;
    private org.osid.calendaring.DateTime dueDate;

    private final java.util.Collection<org.osid.billing.payment.records.SummaryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the customer <code> Id. </code> 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.customer.getId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        return (this.customer);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    protected void setCustomer(org.osid.billing.Customer customer) {
        nullarg(customer, "customer");
        this.customer = customer;
        return;
    }


    /**
     *  Gets the period <code>Id</code>.
     *
     *  @return the period <code>Id</code> 
     */

    @OSID @Override
    public org.osid.id.Id getPeriodId() {
        return (this.period.getId());
    }


    /**
     *  Gets the period. 
     *
     *  @return the period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod()
        throws org.osid.OperationFailedException {

        return (this.period);
    }


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException <code>period</code>
     *          is <code>null</code>
     */

    protected void setPeriod(org.osid.billing.Period period) {
        nullarg(period, "period");
        this.period = period;
        return;
    }


    /**
     *  Tests if the balance is a credit or customer owes money. 
     *
     *  @return <code>true</code> if a credit, <code>false</code> if a
     *          debit
     */

    @OSID @Override
    public boolean isCreditBalance() {
         return (this.credit);
    }


    /**
     *  Sets the credit balance flag.
     *
     *  @param credit <code>true</code> if a credit,
     *         <code>false</code> if a debit
     */

    protected void setCreditBalance(boolean credit) {
        this.credit = credit;
        return;
    }


    /**
     *  Gets the balance for this customer. 
     *
     *  @return the balance 
     */
    
    @OSID @Override
    public org.osid.financials.Currency getBalance() {
        return (this.balance);
    }


    /**
     *  Sets the balance.
     * 
     *  @param balance the balance amount
     *  @throws org.osid.NullArgumentException <code>balance</code> is
     *          <code>null</code>
     */

    protected void setBalance(org.osid.financials.Currency balance) {
        nullarg(balance, "balance");
        this.balance = balance;
        return;
    }


    /**
     *  Tests if the customer has paid before. 
     *
     *  @return <code> true </code> if there is a last payment, <code> false 
     *          </code> otehrwise 
     */

    @OSID @Override
    public boolean hasLastPayment() {
        if ((this.lastPaymentDate != null) && (this.lastPaymentAmount != null)) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the date the last payment was made. 
     *
     *  @return the last payment date 
     *  @throws org.osid.IllegalStateException <code> hasLastPayment()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastPaymentDate() {
        if (!hasLastPayment()) {
            throw new org.osid.IllegalStateException("hasLastPayment() is false");
        }

        return (this.lastPaymentDate);
    }


    /**
     *  Sets the last payment date.
     *
     *  @param date the last payment date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setLastPaymentDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "last payment date");
        this.lastPaymentDate = date;
        return;
    }


    /**
     *  Gets the last payment for this customer. 
     *
     *  @return the payment 
     *  @throws org.osid.IllegalStateException <code> hasLastPayment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getLastPayment() {
        if (!hasLastPayment()) {
            throw new org.osid.IllegalStateException("hasLastPayment() is false");
        }

        return (this.lastPaymentAmount);
    }


    /**
     *  Sets the last payment amount.
     *
     *  @param amount the last payment amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setLastPaymentAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "last payment amount");
        this.lastPaymentAmount = amount;
        return;
    }


    /**
     *  Gets the date the next payment is due. 
     *
     *  @return the payment due date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPaymentDueDate() {
        return (this.dueDate);
    }


    /**
     *  Sets the due date.
     *
     *  @param date due date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setPaymentDueDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "due date");
        this.dueDate = date;
        return;
    }


    /**
     *  Tests if this summary supports the given record
     *  <code>Type</code>.
     *
     *  @param  summaryRecordType a summary record type 
     *  @return <code>true</code> if the summaryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type summaryRecordType) {
        for (org.osid.billing.payment.records.SummaryRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Summary</code>
     *  record <code>Type</code>.
     *
     *  @param  summaryRecordType the summary record type 
     *  @return the summary record 
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.SummaryRecord getSummaryRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.SummaryRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this summary. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param summaryRecord the summary record
     *  @param summaryRecordType summary record type
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecord</code> or
     *          <code>summaryRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addSummaryRecord(org.osid.billing.payment.records.SummaryRecord summaryRecord, 
                                  org.osid.type.Type summaryRecordType) {

        nullarg(summaryRecord, "summary record");
        addRecordType(summaryRecordType);
        this.records.add(summaryRecord);
        
        return;
    }
}
