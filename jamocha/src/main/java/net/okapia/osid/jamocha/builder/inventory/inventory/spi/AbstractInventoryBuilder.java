//
// AbstractInventory.java
//
//     Defines an Inventory builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.inventory.spi;


/**
 *  Defines an <code>Inventory</code> builder.
 */

public abstract class AbstractInventoryBuilder<T extends AbstractInventoryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inventory.inventory.InventoryMiter inventory;


    /**
     *  Constructs a new <code>AbstractInventoryBuilder</code>.
     *
     *  @param inventory the inventory to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractInventoryBuilder(net.okapia.osid.jamocha.builder.inventory.inventory.InventoryMiter inventory) {
        super(inventory);
        this.inventory = inventory;
        return;
    }


    /**
     *  Builds the inventory.
     *
     *  @return the new inventory
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inventory.Inventory build() {
        (new net.okapia.osid.jamocha.builder.validator.inventory.inventory.InventoryValidator(getValidations())).validate(this.inventory);
        return (new net.okapia.osid.jamocha.builder.inventory.inventory.ImmutableInventory(this.inventory));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the inventory miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.inventory.inventory.InventoryMiter getMiter() {
        return (this.inventory);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>stock</code> is
     *          <code>null</code>
     */

    public T stock(org.osid.inventory.Stock stock) {
        getMiter().setStock(stock);
        return (self());
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T date(org.osid.calendaring.DateTime date) {
        getMiter().setDate(date);
        return (self());
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>quantity</code>
     *          is <code>null</code>
     */

    public T quantity(java.math.BigDecimal quantity) {
        getMiter().setQuantity(quantity);
        return (self());
    }


    /**
     *  Adds an Inventory record.
     *
     *  @param record an inventory record
     *  @param recordType the type of inventory record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inventory.records.InventoryRecord record, org.osid.type.Type recordType) {
        getMiter().addInventoryRecord(record, recordType);
        return (self());
    }
}       


