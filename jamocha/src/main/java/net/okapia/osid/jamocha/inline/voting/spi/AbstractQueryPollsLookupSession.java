//
// AbstractQueryPollsLookupSession.java
//
//    An inline adapter that maps a PollsLookupSession to
//    a PollsQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PollsLookupSession to
 *  a PollsQuerySession.
 */

public abstract class AbstractQueryPollsLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractPollsLookupSession
    implements org.osid.voting.PollsLookupSession {

    private final org.osid.voting.PollsQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPollsLookupSession.
     *
     *  @param querySession the underlying polls query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPollsLookupSession(org.osid.voting.PollsQuerySession querySession) {
        nullarg(querySession, "polls query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Polls</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPolls() {
        return (this.session.canSearchPolls());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Polls</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Polls</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Polls</code> and
     *  retained for compatibility.
     *
     *  @param  pollsId <code>Id</code> of the
     *          <code>Polls</code>
     *  @return the polls
     *  @throws org.osid.NotFoundException <code>pollsId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pollsId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.PollsQuery query = getQuery();
        query.matchId(pollsId, true);
        org.osid.voting.PollsList polls = this.session.getPollsByQuery(query);
        if (polls.hasNext()) {
            return (polls.getNextPolls());
        } 
        
        throw new org.osid.NotFoundException(pollsId + " not found");
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  polls specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Polls</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  pollsIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pollsIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByIds(org.osid.id.IdList pollsIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.PollsQuery query = getQuery();

        try (org.osid.id.IdList ids = pollsIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPollsByQuery(query));
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  polls genus <code>Type</code> which does not include
     *  polls of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.PollsQuery query = getQuery();
        query.matchGenusType(pollsGenusType, true);
        return (this.session.getPollsByQuery(query));
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  polls genus <code>Type</code> and include any additional
     *  polls with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByParentGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.PollsQuery query = getQuery();
        query.matchParentGenusType(pollsGenusType, true);
        return (this.session.getPollsByQuery(query));
    }


    /**
     *  Gets a <code>PollsList</code> containing the given
     *  polls record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByRecordType(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.PollsQuery query = getQuery();
        query.matchRecordType(pollsRecordType, true);
        return (this.session.getPollsByQuery(query));
    }


    /**
     *  Gets a <code>PollsList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known polls or
     *  an error results. Otherwise, the returned list may contain
     *  only those polls that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Polls</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.PollsQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getPollsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Polls</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Polls</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getAllPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.PollsQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPollsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.PollsQuery getQuery() {
        org.osid.voting.PollsQuery query = this.session.getPollsQuery();
        return (query);
    }
}
