//
// AbstractMapActivityLookupSession
//
//    A simple framework for providing an Activity lookup service
//    backed by a fixed collection of activities.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Activity lookup service backed by a
 *  fixed collection of activities. The activities are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Activities</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapActivityLookupSession
    extends net.okapia.osid.jamocha.financials.spi.AbstractActivityLookupSession
    implements org.osid.financials.ActivityLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.financials.Activity> activities = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.financials.Activity>());


    /**
     *  Makes an <code>Activity</code> available in this session.
     *
     *  @param  activity an activity
     *  @throws org.osid.NullArgumentException <code>activity<code>
     *          is <code>null</code>
     */

    protected void putActivity(org.osid.financials.Activity activity) {
        this.activities.put(activity.getId(), activity);
        return;
    }


    /**
     *  Makes an array of activities available in this session.
     *
     *  @param  activities an array of activities
     *  @throws org.osid.NullArgumentException <code>activities<code>
     *          is <code>null</code>
     */

    protected void putActivities(org.osid.financials.Activity[] activities) {
        putActivities(java.util.Arrays.asList(activities));
        return;
    }


    /**
     *  Makes a collection of activities available in this session.
     *
     *  @param  activities a collection of activities
     *  @throws org.osid.NullArgumentException <code>activities<code>
     *          is <code>null</code>
     */

    protected void putActivities(java.util.Collection<? extends org.osid.financials.Activity> activities) {
        for (org.osid.financials.Activity activity : activities) {
            this.activities.put(activity.getId(), activity);
        }

        return;
    }


    /**
     *  Removes an Activity from this session.
     *
     *  @param  activityId the <code>Id</code> of the activity
     *  @throws org.osid.NullArgumentException <code>activityId<code> is
     *          <code>null</code>
     */

    protected void removeActivity(org.osid.id.Id activityId) {
        this.activities.remove(activityId);
        return;
    }


    /**
     *  Gets the <code>Activity</code> specified by its <code>Id</code>.
     *
     *  @param  activityId <code>Id</code> of the <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>activityId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.financials.Activity activity = this.activities.get(activityId);
        if (activity == null) {
            throw new org.osid.NotFoundException("activity not found: " + activityId);
        }

        return (activity);
    }


    /**
     *  Gets all <code>Activities</code>. In plenary mode, the returned
     *  list contains all known activities or an error
     *  results. Otherwise, the returned list may contain only those
     *  activities that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.activity.ArrayActivityList(this.activities.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activities.clear();
        super.close();
        return;
    }
}
