//
// SourceableMiter.java
//
//     Miter for Sourceables.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Miter for Sourceables.
 */

public interface SourceableMiter
    extends Miter,
            org.osid.Sourceable {


    /**
     *  Sets the provider for this catalog.
     *
     *  @param provider the name for this catalog
     *  @throws org.osid.NullArgumentException <code>provider</code> is
     *          <code>null</code>
     */
    
    public void setProvider(net.okapia.osid.provider.Provider provider);


    /**
     *  Sets the provider for this catalog.
     *
     *  @param provider the name for this catalog
     *  @throws org.osid.NullArgumentException <code>provider</code> is
     *          <code>null</code>
     */
    
    public void setProvider(org.osid.resource.Resource provider);
    

    /**
     *  Adds an asset to the service branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */
    
    public void addAssetToBranding(org.osid.repository.Asset asset);


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code> is
     *          <code>null</code>
     */
    
    public void setLicense(org.osid.locale.DisplayText license);
}
