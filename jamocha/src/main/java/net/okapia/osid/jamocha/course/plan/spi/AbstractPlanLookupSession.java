//
// AbstractPlanLookupSession.java
//
//    A starter implementation framework for providing a Plan
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Plan
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPlans(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPlanLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.plan.PlanLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Plan</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPlans() {
        return (true);
    }


    /**
     *  A complete view of the <code>Plan</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePlanView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Plan</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPlanView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include plans in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only plans whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectivePlanView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All plans of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectivePlanView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Plan</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Plan</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Plan</code> and
     *  retained for compatibility.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planId <code>Id</code> of the
     *          <code>Plan</code>
     *  @return the plan
     *  @throws org.osid.NotFoundException <code>planId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>planId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.Plan getPlan(org.osid.id.Id planId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.plan.PlanList plans = getPlans()) {
            while (plans.hasNext()) {
                org.osid.course.plan.Plan plan = plans.getNextPlan();
                if (plan.getId().equals(planId)) {
                    return (plan);
                }
            }
        } 

        throw new org.osid.NotFoundException(planId + " not found");
    }


    /**
     *  Gets a <code>PlanList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  plans specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Plans</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, plans are returned that are currently effective.
     *  In any effective mode, effective plans and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPlans()</code>.
     *
     *  @param  planIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>planIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByIds(org.osid.id.IdList planIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.plan.Plan> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = planIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPlan(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("plan " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.plan.plan.LinkedPlanList(ret));
    }


    /**
     *  Gets a <code>PlanList</code> corresponding to the given
     *  plan genus <code>Type</code> which does not include
     *  plans of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPlans()</code>.
     *
     *  @param  planGenusType a plan genus type 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByGenusType(org.osid.type.Type planGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.PlanGenusFilterList(getPlans(), planGenusType));
    }


    /**
     *  Gets a <code>PlanList</code> corresponding to the given
     *  plan genus <code>Type</code> and include any additional
     *  plans with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPlans()</code>.
     *
     *  @param  planGenusType a plan genus type 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByParentGenusType(org.osid.type.Type planGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPlansByGenusType(planGenusType));
    }


    /**
     *  Gets a <code>PlanList</code> containing the given
     *  plan record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPlans()</code>.
     *
     *  @param  planRecordType a plan record type 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByRecordType(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.PlanRecordFilterList(getPlans(), planRecordType));
    }


    /**
     *  Gets a <code>PlanList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *  
     *  In active mode, plans are returned that are currently
     *  active. In any status mode, active and inactive plans
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Plan</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.plan.PlanList getPlansOnDate(org.osid.calendaring.DateTime from, 
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.TemporalPlanFilterList(getPlans(), from, to));
    }
        

    /**
     *  Gets a list of plans corresponding to a syllabus
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the <code>Id</code> of the syllabus
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.plan.PlanList getPlansForSyllabus(org.osid.id.Id syllabusId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.PlanFilterList(new SyllabusFilter(syllabusId), getPlans()));
    }


    /**
     *  Gets a list of plans corresponding to a syllabus
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the <code>Id</code> of the syllabus
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusOnDate(org.osid.id.Id syllabusId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.TemporalPlanFilterList(getPlansForSyllabus(syllabusId), from, to));
    }


    /**
     *  Gets a list of plans corresponding to a course offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.plan.PlanList getPlansForCourseOffering(org.osid.id.Id courseOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.PlanFilterList(new CourseOfferingFilter(courseOfferingId), getPlans()));
    }


    /**
     *  Gets a list of plans corresponding to a course offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param courseOfferingId the <code>Id</code> of the course
     *         offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.TemporalPlanFilterList(getPlansForCourseOffering(courseOfferingId), from, to));
    }


    /**
     *  Gets a list of plans corresponding to syllabus and course
     *  offering <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the <code>Id</code> of the syllabus
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>,
     *          <code>courseOfferingId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusAndCourseOffering(org.osid.id.Id syllabusId,
                                                                              org.osid.id.Id courseOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.PlanFilterList(new CourseOfferingFilter(courseOfferingId), getPlansForSyllabus(syllabusId)));
    }


    /**
     *  Gets a list of plans corresponding to syllabus and course
     *  offering <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param courseOfferingId the <code>Id</code> of the course
     *         offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PlanList</code>
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>,
     *          <code>courseOfferingId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusAndCourseOfferingOnDate(org.osid.id.Id syllabusId,
                                                                                    org.osid.id.Id courseOfferingId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.plan.plan.TemporalPlanFilterList(getPlansForSyllabusAndCourseOffering(syllabusId, courseOfferingId), from, to));
    }


    /**
     *  Gets all <code>Plans</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Plans</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.plan.PlanList getPlans()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the plan list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of plans
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.plan.PlanList filterPlansOnViews(org.osid.course.plan.PlanList list)
        throws org.osid.OperationFailedException {

        org.osid.course.plan.PlanList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.plan.plan.EffectivePlanFilterList(ret);
        }

        return (ret);
    }

    public static class SyllabusFilter
        implements net.okapia.osid.jamocha.inline.filter.course.plan.plan.PlanFilter {
         
        private final org.osid.id.Id syllabusId;
         
         
        /**
         *  Constructs a new <code>SyllabusFilter</code>.
         *
         *  @param syllabusId the syllabus to filter
         *  @throws org.osid.NullArgumentException
         *          <code>syllabusId</code> is <code>null</code>
         */
        
        public SyllabusFilter(org.osid.id.Id syllabusId) {
            nullarg(syllabusId, "syllabus Id");
            this.syllabusId = syllabusId;
            return;
        }

         
        /**
         *  Used by the PlanFilterList to filter the 
         *  plan list based on syllabus.
         *
         *  @param plan the plan
         *  @return <code>true</code> to pass the plan,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.plan.Plan plan) {
            return (plan.getSyllabusId().equals(this.syllabusId));
        }
    }


    public static class CourseOfferingFilter
        implements net.okapia.osid.jamocha.inline.filter.course.plan.plan.PlanFilter {
         
        private final org.osid.id.Id courseOfferingId;
         
         
        /**
         *  Constructs a new <code>CourseOfferingFilter</code>.
         *
         *  @param courseOfferingId the course offering to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseOfferingId</code> is <code>null</code>
         */
        
        public CourseOfferingFilter(org.osid.id.Id courseOfferingId) {
            nullarg(courseOfferingId, "course offering Id");
            this.courseOfferingId = courseOfferingId;
            return;
        }

         
        /**
         *  Used by the PlanFilterList to filter the 
         *  plan list based on course offering.
         *
         *  @param plan the plan
         *  @return <code>true</code> to pass the plan,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.plan.Plan plan) {
            return (plan.getCourseOfferingId().equals(this.courseOfferingId));
        }
    }
}
