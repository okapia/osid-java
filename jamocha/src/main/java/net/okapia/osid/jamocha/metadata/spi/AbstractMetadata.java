//
// AbstractMetadata.java
//
//     Defines a basic Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a basic Metadata.
 */

public abstract class AbstractMetadata
    implements org.osid.Metadata {

    private final org.osid.id.Id elementId;
    private final boolean isLinked;
    private final boolean isArray;
    private final org.osid.Syntax syntax;

    private boolean isRequired   = false;
    private boolean isReadOnly   = true;
    private boolean isValueKnown = false;
    private boolean hasValue     = false;

    private long minElements = 1;
    private long maxElements = 1;

    private org.osid.locale.DisplayText label = Plain.valueOf("element");
    private org.osid.locale.DisplayText instructions = Plain.valueOf("enter data");
    private org.osid.locale.DisplayText units = Plain.valueOf("");
    

    /**
     *  Constructs a new {@code AbstractMetadata}.
     *
     *  @param syntax the syntax
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code syntax} or
     *          {@code elementId} is {@code null}
     */

    protected AbstractMetadata(org.osid.Syntax syntax, org.osid.id.Id elementId) {

        nullarg(syntax, "syntax");
        nullarg(elementId, "element Id");

        this.syntax    = syntax;
        this.elementId = elementId;
        this.isLinked  = false;
        this.isArray   = false;

        return;
    }


    /**
     *  Constructs a new {@code AbstractMetadata}.
     *
     *  @param syntax the syntax
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code syntax} or
     *          {@code elementId} is {@code null}
     */

    protected AbstractMetadata(org.osid.Syntax syntax, org.osid.id.Id elementId, 
                               boolean isArray, boolean isLinked) {

        nullarg(syntax, "syntax");
        nullarg(elementId, "element Id");

        this.syntax    = syntax;
        this.elementId = elementId;
        this.isLinked  = isLinked;
        this.isArray   = isArray;

        return;
    }

    
    /**
     *  Gets a unique <code> Id </code> for the data element. 
     *
     *  @return an <code> Id </code> 
     */

    @OSID @Override
    public final org.osid.id.Id getElementId() {
        return (this.elementId);
    }


    /**
     *  Gets a display label for the data element. 
     *
     *  @return a display label 
     */

    @OSID @Override
    public final org.osid.locale.DisplayText getElementLabel() {
        return (this.label);
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    protected void setLabel(org.osid.locale.DisplayText label) {
        nullarg(label, "label");
        this.label = label;
        return;
    }


    /**
     *  Gets instructions for updating this data. This is a human
     *  readable description of the data element or property that may
     *  include special instructions or caveats to the end-user above
     *  and beyond what this interface provides.
     *
     *  @return instructions 
     */

    @OSID @Override
    public final org.osid.locale.DisplayText getInstructions() {
        return (this.instructions);
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    protected void setInstructions(org.osid.locale.DisplayText instructions) {
        nullarg(instructions, "instructions");
        this.instructions = instructions;
        return;
    }


    /**
     *  Tests if this data element is required for creating new objects. 
     *
     *  @return <code> true </code> if this data is required, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public final boolean isRequired() {
        return (this.isRequired);
    }


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    protected void setRequired(boolean required) {
        this.isRequired = required;
        return;
    }


    /**
     *  Tests if an existing value is known for this data element. If
     *  it is known that a value does not exist, then this method
     *  returns <code> true. </code>
     *
     *  @return <code> true </code> if the element value is known, <code> 
     *          false </code> if the element value is not known 
     */

    @OSID @Override
    public boolean isValueKnown() {
        return (this.isValueKnown);
    }


    /**
     *  Sets a flag that the value is known to exist or not exist.
     *
     *  @param known {@code true} if the value is known to exist or
     *         not exist, {@code false} if it is unknown
     */

    protected void setValueKnown(boolean known) {
        this.isValueKnown = known;
        return;
    }


    /**
     *  Tests if this data element has a set non-default value. 
     *
     *  @return <code> true </code> if this data has been set, <code>
     *          false </code> otherwise
     *  @throws org.osid.IllegalStateException <code> isValueKnown() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public final boolean hasValue() {
        if (!isValueKnown()) {
            throw new org.osid.IllegalStateException("isValueKnown() is false");
        }

        return (this.hasValue);
    }


    /**
     *  Sets the has value flag. This also sets the isValueKnown flag. 
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    protected void setValueExists(boolean exists) {
        this.hasValue = exists;
        setValueKnown(true);
        return;
    }


    /**
     *  Tests if this data can be updated. This may indicate the
     *  result of a pre-authorization but is not a guarantee that an
     *  authorization failure will not occur when the create or update
     *  transaction is issued.
     *
     *  @return <code> true </code> if this data is not updatable,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public final boolean isReadOnly() {
        return (this.isReadOnly);
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    protected void setReadOnly(boolean readonly) {
        this.isReadOnly = readonly;
        return;
    }


    /**
     *  Tests if this data element is linked to other data in the
     *  object.  Updating linked data should refresh all metadata and
     *  revalidate object elements.
     *
     *  @return true if this element is linked, false if updates have
     *          no side effect
     */

    @OSID @Override
    public final boolean isLinked() {
        return (this.isLinked);
    }


    /**
     *  Gets the syntax of this data. 
     *
     *  @return an enumeration indicating the <code> </code> type of value 
     */

    @OSID @Override
    public final org.osid.Syntax getSyntax() {
        return (this.syntax);
    }


    /**
     *  Tests if this data element is an array. 
     *
     *  @return <code> true </code> if this data is an array, <code> false 
     *          </code> if a single element 
     */

    @OSID @Override
    public final boolean isArray() {
        return (this.isArray);
    }


    /**
     *  Gets the units of this data for display purposes ('lbs', 'gills', 
     *  'furlongs'). 
     *
     *  @return the display units of this data or an empty string if not 
     *          applicable 
     */

    @OSID @Override
    public final org.osid.locale.DisplayText getUnits() {
        return (this.units);
    }

 
    /**
     *  Sets the units.
     *
     *  @param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    protected void setUnits(org.osid.locale.DisplayText units) {
        nullarg(units, "units");
        this.units = units;
        return;
    }


    /**
     *  In the case where an array or list of elements is specified in
     *  an <code> OsidForm, </code> this specifies the minimum number
     *  of elements that must be included.
     *
     *  @return the minimum elements or <code> 1 </code> if <code>
     *          isArray() </code> is <code> false </code>
     */

    @OSID @Override
    public final long getMinimumElements() {
        if (!isArray()) {
            return (1);
        }

        return (this.minElements);
    }


    /**
     *  In the case where an array or list of elements is specified in an 
     *  <code> OsidForm, </code> this specifies the maximum number of elements 
     *  that must be included. 
     *
     *  @return the maximum elements or <code> 1 </code> if <code> isArray() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public final long getMaximumElements() {
        if (!isArray()) {
            return (1);
        }

        return (this.maxElements);
    }


    /**
     *  Sets the minimum and maximum number of elements if an array.
     *
     *  @param min the minimum number of elements 
     *  @param max the maximum number of elements 
     *  @throws org.osid.IllegalStateException {@code isArray()} is
     *          {@code false}
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    protected void setNumberElements(long min, long max) {
        cardinalarg(min, "min elements");
        cardinalarg(max, "max elements");

        if (min > max) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        if (!isArray()) {
            throw new org.osid.IllegalStateException("isArray() is false");
        }

        this.minElements = min;
        this.maxElements = max;

        return;
    }


    /**
     *  Gets the minimum cardinal value. 
     *
     *  @return the minimum cardinal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> 
     */

    @OSID @Override
    public long getMinimumCardinal() {
        throw new org.osid.IllegalStateException("not a CARDINAL");
    }


    /**
     *  Gets the maximum cardinal value. 
     *
     *  @return the maximum cardinal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> 
     */

    @OSID @Override
    public long getMaximumCardinal() {
        throw new org.osid.IllegalStateException("not a CARDINAL");
    }


    /**
     *  Gets the set of acceptable cardinal values.
     *
     *  @return a set of cardinals or an empty array if not restricted
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CARDINAL </code>
     */

    @OSID @Override
    public long[] getCardinalSet() {
        throw new org.osid.IllegalStateException("not a CARDINAL");
    }


    /**
     *  Gets the default cardinal values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default cardinal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> or <code> isRequired() </code> is <code> true </code> 
     */

    @OSID @Override
    public long[] getDefaultCardinalValues() {
        throw new org.osid.IllegalStateException("not a CARDINAL");
    }        


    /**
     *  Gets the existing cardinal values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing cardinal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> or <code> isValueKnown() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long[] getExistingCardinalValues() {
        throw new org.osid.IllegalStateException("not a CARDINAL");
    }


    /**
     *  Gets the set of acceptable coordinate types.
     *
     *  @return the set of coordinate types
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          COORDINATE or SPATIALUNIT </code>
     */

    @OSID @Override
    public org.osid.type.Type[] getCoordinateTypes() {
        throw new org.osid.IllegalStateException("not a COORDINATE or SPATIALUNIT");
    }


    /**
     *  Tests if the given coordinate type is supported. 
     *
     *  @param  coordinateType a coordinate Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateType(org.osid.type.Type coordinateType) {
        throw new org.osid.IllegalStateException("not a COORDINATE");
    }


    /**
     *  Gets the number of axes for a given supported coordinate type. 
     *
     *  @param  coordinateType a coordinate Type 
     *  @return the number of axes 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateType(coordinateType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long getAxesForCoordinateType(org.osid.type.Type coordinateType) {
        throw new org.osid.IllegalStateException("not a COORDINATE");
    }


    /**
     *  Gets the minimum coordinate values given supported coordinate
     *  type.
     *
     *  @param  coordinateType a coordinate Type 
     *  @return the minimum coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code>
     *          supportsCoordinateType(coordinateType) </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal[] getMinimumCoordinateValues(org.osid.type.Type coordinateType) {
        throw new org.osid.IllegalStateException("not a COORDINATE");
    }


    /**
     *  Gets the maximum coordinate values given supported coordinate type. 
     *
     *  @param  coordinateType a coordinate Type 
     *  @return the maximum coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateType(coordinateType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getMaximumCoordinateValues(org.osid.type.Type coordinateType) {
        throw new org.osid.IllegalStateException("not a COORDINATE");
    }


    /**
     *  Gets the set of acceptable coordinate values. 
     *
     *  @return a set of coordinates or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate[] getCoordinateSet() {
        throw new org.osid.IllegalStateException("not a COORDINATE");
    }


    /**
     *  Gets the default coordinate values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> or <code> isRequired() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate[] getDefaultCoordinateValues() {
        throw new org.osid.IllegalStateException("not a COORDINATE");
    }


    /**
     *  Gets the existing coordinate values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate[] getExistingCoordinateValues() {
        throw new org.osid.IllegalStateException("not a COORDINATE");
    }


    /**
     *  Gets the set of acceptable currency types. 
     *
     *  @return the set of currency types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CURRENCY 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getCurrencyTypes() {
        throw new org.osid.IllegalStateException("not a CURRENCY");
    }


    /**
     *  Tests if the given currency type is supported. 
     *
     *  @param  currencyType a currency Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CURRENCY 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCurrencyType(org.osid.type.Type currencyType) {
        throw new org.osid.IllegalStateException("not a CURRENCY");
    }


    /**
     *  Gets the minimum currency value. 
     *
     *  @return the minimum currency 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CURRENCY 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getMinimumCurrency() {
        throw new org.osid.IllegalStateException("not a CURRENCY");
    }


    /**
     *  Gets the maximum currency value. 
     *
     *  @return the maximum currency 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CURRENCY 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getMaximumCurrency() {
        throw new org.osid.IllegalStateException("not a CURRENCY");
    }


    /**
     *  Gets the set of acceptable currency values. 
     *
     *  @return a set of currencies or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CURRENCY 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency[] getCurrencySet() {
        throw new org.osid.IllegalStateException("not a CURRENCY");
    }


    /**
     *  Gets the default currency values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default currency values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CURRENCY </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.financials.Currency[] getDefaultCurrencyValues() {
        throw new org.osid.IllegalStateException("not a CURRENCY");
    }


    /**
     *  Gets the existing currency values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing currency values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CURRENCY </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.financials.Currency[] getExistingCurrencyValues() {
        throw new org.osid.IllegalStateException("not a CURRENCY");
    }


    /**
     *  Gets the smallest resolution of the date time value. 
     *
     *  @return the resolution 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeResolution getDateTimeResolution() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the set of acceptable calendar types. 
     *
     *  @return the set of calendar types 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME</code> or <code>DURATION</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getCalendarTypes() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Tests if the given calendar type is supported. 
     *
     *  @param  calendarType a calendar Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME</code> or <code>DURATION</code>
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarType(org.osid.type.Type calendarType) {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the set of acceptable time types. 
     *
     *  @return a set of time types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME</code>, <code>DURATION</code>, or <code> TIME
     *          </code>
     */

    @OSID @Override
    public org.osid.type.Type[] getTimeTypes() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Tests if the given time type is supported. 
     *
     *  @param  timeType a time Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME</code>, <code>DURATION</code>, or <code> TIME
     *          </code>
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimeType(org.osid.type.Type timeType) {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the minimum date time value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DATETIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getMinimumDateTime() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the maximum date time value. 
     *
     *  @return the maximum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DATETIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getMaximumDateTime() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the set of acceptable date time values. 
     *
     *  @return a set of values or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DATETIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime[] getDateTimeSet() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the default date time values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default date time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime[] getDefaultDateTimeValues() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the existing date time values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values.  If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing date time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime[] getExistingDateTimeValues() {
        throw new org.osid.IllegalStateException("not a DATETIME");
    }


    /**
     *  Gets the number of digits to the right of the decimal point. 
     *
     *  @return the scale 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DECIMAL 
     *          </code> 
     */

    @OSID @Override
    public long getDecimalScale() {
        throw new org.osid.IllegalStateException("not a DECIMAL");
    }


    /**
     *  Gets the minimum decimal value. 
     *
     *  @return the minimum decimal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DECIMAL 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumDecimal() {
        throw new org.osid.IllegalStateException("not a DECIMAL");
    }


    /**
     *  Gets the maximum decimal value. 
     *
     *  @return the maximum decimal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DECIMAL 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMaximumDecimal() {
        throw new org.osid.IllegalStateException("not a DECIMAL");
    }


    /**
     *  Gets the set of acceptable decimal values. 
     *
     *  @return a set of decimals or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DECIMAL 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getDecimalSet() {
        throw new org.osid.IllegalStateException("not a DECIMAL");
    }


    /**
     *  Gets the default decimal values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default decimal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DECIMAL </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public java.math.BigDecimal[] getDefaultDecimalValues() {
        throw new org.osid.IllegalStateException("not a DECIMAL");
    }


    /**
     *  Gets the existing decimal values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values.If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing decimal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DECIMAL </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */
    
    @OSID @Override
    public java.math.BigDecimal[] getExistingDecimalValues() {
        throw new org.osid.IllegalStateException("not a DECIMAL");
    }


    /**
     *  Gets the smallest resolution of the distance value. 
     *
     *  @return the resolution 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.DistanceResolution getDistanceResolution() {
        throw new org.osid.IllegalStateException("not a DISTANCE");
    }


    /**
     *  Gets the minimum distance value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getMinimumDistance() {
        throw new org.osid.IllegalStateException("not a DISTANCE");
    }


    /**
     *  Gets the maximum distance value. 
     *
     *  @return the maximum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getMaximumDistance() {
        throw new org.osid.IllegalStateException("not a DISTANCE");
    }


    /**
     *  Gets the set of acceptable distance values. 
     *
     *  @return a set of values or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DISTANCE </code>
     */

    @OSID @Override
    public org.osid.mapping.Distance[] getDistanceSet() {
        throw new org.osid.IllegalStateException("not a DISTANCE");
    }


    /**
     *  Gets the default distance values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default distance values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DISTANCE </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.Distance[] getDefaultDistanceValues() {
        throw new org.osid.IllegalStateException("not a DISTANCE");
    }


    /**
     *  Gets the existing distance values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing distance values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DISTANCE </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.mapping.Distance[] getExistingDistanceValues() {
        throw new org.osid.IllegalStateException("not a DISTANCE");
    }


    /**
     *  Gets the minimum duration. 
     *
     *  @return the minimum duration 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DURATION 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getMinimumDuration() {
        throw new org.osid.IllegalStateException("not a DURATION");
    }


    /**
     *  Gets the maximum duration. 
     *
     *  @return the maximum duration 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DURATION 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getMaximumDuration() {
        throw new org.osid.IllegalStateException("not a DURATION");
    }


    /**
     *  Gets the set of acceptable duration values. 
     *
     *  @return a set of durations or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DURATION 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration[] getDurationSet() {
        throw new org.osid.IllegalStateException("not a DURATION");
    }


    /**
     *  Gets the default duration values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most
     *  at most a single value.
     *
     *  @return the default duration values 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          DURATION or <code> isRequired() </code> is <code> true
     *          </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration[] getDefaultDurationValues() {
        throw new org.osid.IllegalStateException("not a DURATION");
    }


    /**
     *  Gets the existing duration values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing duration values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DURATION </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration[] getExistingDurationValues() {
        throw new org.osid.IllegalStateException("not a DURATION");
    }


    /**
     *  Gets the set of acceptable heading types. 
     *
     *  @return a set of heading types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> HEADING 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getHeadingTypes() {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Tests if the given heading type is supported. 
     *
     *  @param  headingType a heading Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code> HEADING 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHeadingType(org.osid.type.Type headingType) {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Gets the number of axes for a given supported heading type.
     *
     *  @param  headingType a heading Type 
     *  @return the number of axes 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          HEADING </code>
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsHeadingType(headingType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long getAxesForHeadingType(org.osid.type.Type headingType) {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Gets the minimum heading values given supported heading type. 
     *
     *  @param  headingType a heading Type 
     *  @return the minimum heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          HEADING </code>
     *  @throws org.osid.NullArgumentException <code> headingType
     *          </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException <code>
     *          supportsHeadingType(headingType) </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public java.math.BigDecimal[] getMinimumHeadingValues(org.osid.type.Type headingType) {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Gets the maximum heading values given supported heading type. 
     *
     *  @param  headingType a heading Type 
     *  @return the maximum heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> HEADING 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsHeadingType(headingType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getMaximumHeadingValues(org.osid.type.Type headingType) {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Gets the set of acceptable heading values. 
     *
     *  @return the set of heading 
     *  @throws org.osid.IllegalStateException syntax is not a <code> HEADING 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Heading[] getHeadingSet() {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Gets the default heading values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          HEADING </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.Heading[] getDefaultHeadingValues() {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Gets the existing heading values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          HEADING </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.mapping.Heading[] getExistingHeadingValues() {
        throw new org.osid.IllegalStateException("not a HEADING");
    }


    /**
     *  Gets the set of acceptable <code> Ids. </code> 
     *
     *  @return a set of <code> Ids </code> or an empty array if not 
     *          restricted 
     *  @throws org.osid.IllegalStateException syntax is not an <code> ID 
     *          </code> 
     */

    @OSID @Override
    public org.osid.id.Id[] getIdSet() {
        throw new org.osid.IllegalStateException("not an ID");
    }


    /**
     *  Gets the default <code> Id </code> values. These are the
     *  values used if the element value is not provided or is
     *  cleared. If <code> isArray() </code> is false, then this
     *  method returns at most a single value.
     *
     *  @return the default <code> Id </code> values 
     *  @throws org.osid.IllegalStateException syntax is not an <code>
     *          ID </code> or <code> isRequired() </code> is <code>
     *          true </code>
     */

    @OSID @Override
    public org.osid.id.Id[] getDefaultIdValues() {
        throw new org.osid.IllegalStateException("not an ID");
    }


    /**
     *  Gets the existing <code> Id </code> values. If <code>
     *  hasValue() </code> and <code> isRequired() </code> are <code>
     *  false, </code> then these values are the default values.  If
     *  <code> isArray() </code> is false, then this method returns at
     *  most a single value.
     *
     *  @return the existing <code> Id </code> values 
     *  @throws org.osid.IllegalStateException syntax is not an <code>
     *          ID </code>
     */

    @OSID @Override
    public org.osid.id.Id[] getExistingIdValues() {
        throw new org.osid.IllegalStateException("not an ID");
    }

    
    /**
     *  Gets the minimum integer value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not an <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long getMinimumInteger() {
        throw new org.osid.IllegalStateException("not an INTEGER");
    }


    /**
     *  Gets the maximum integer value. 
     *
     *  @return the maximum value 
     *  @throws org.osid.IllegalStateException syntax is not an <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long getMaximumInteger() {
        throw new org.osid.IllegalStateException("not an INTEGER");
    }


    /**
     *  Gets the set of acceptable integer values. 
     *
     *  @return a set of values or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not an <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long[] getIntegerSet() {
        throw new org.osid.IllegalStateException("not an INTEGER");
    }


    /**
     *  Gets the default integer values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default integer values 
     *  @throws org.osid.IllegalStateException syntax is not an <code>
     *          INTEGER </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public long[] getDefaultIntegerValues() {
        throw new org.osid.IllegalStateException("not an INTEGER");
    }


    /**
     *  Gets the existing integer values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing integer values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          INTEGER </code> or isValueKnown() is false
     */

    @OSID @Override
    public long[] getExistingIntegerValues() {
        throw new org.osid.IllegalStateException("not an INTEGER");
    }


    /**
     *  Gets the set of acceptable <code> Types </code> for an arbitrary 
     *  object. 
     *
     *  @return a set of <code> Types </code> or an empty array if not 
     *          restricted 
     *  @throws org.osid.IllegalStateException syntax is not an <code> OBJECT 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getObjectTypes() {
        throw new org.osid.IllegalStateException("not an OBJECT");
    }


    /**
     *  Tests if the given object type is supported. 
     *
     *  @param  objectType an object Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not an <code> OBJECT 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> objectType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectType(org.osid.type.Type objectType) {
        throw new org.osid.IllegalStateException("not an OBJECT");
    }


    /**
     *  Gets the set of acceptable object values. 
     *
     *  @return a set of values or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not an <code>
     *          OBJECT </code>
     */

    @OSID @Override
    public java.lang.Object[] getObjectSet() {
        throw new org.osid.IllegalStateException("not an OBJECT");
    }


    /**
     *  Gets the default object values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default object values 
     *  @throws org.osid.IllegalStateException syntax is not an <code>
     *          OBJECT </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public java.lang.Object[] getDefaultObjectValues() {
        throw new org.osid.IllegalStateException("not an OBJECT");
    }


    /**
     *  Gets the existing object values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing object values 
     *  @throws org.osid.IllegalStateException syntax is not an OBJECT
     *          or <code> isValueKnown() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public java.lang.Object[] getExistingObjectValues() {
        throw new org.osid.IllegalStateException("not an OBJECT");
    }


    /**
     *  Gets the set of acceptable spatial unit record types. 
     *
     *  @return the set of spatial unit types 
     *  @throws org.osid.IllegalStateException syntax is not <code> 
     *          SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getSpatialUnitRecordTypes() {
        throw new org.osid.IllegalStateException("not a SPATIALUNIT");
    }


    /**
     *  Tests if the given spatial unit record type is supported. 
     *
     *  @param  spatialUnitRecordType a spatial unit record Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not an <code> 
     *          SPATIALUNIT </code> 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        throw new org.osid.IllegalStateException("not a SPATIALUNIT");
    }


    /**
     *  Gets the set of acceptable spatial unit values. 
     *
     *  @return a set of spatial units or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit[] getSpatialUnitSet() {
        throw new org.osid.IllegalStateException("not a SPATIALUNIT");
    }


    /**
     *  Gets the default spatial unit values. These are the values
     *  used if the element value is not provided or is cleared. If
     *  <code> isArray() </code> is false, then this method returns at
     *  most a single value.
     *
     *  @return the default spatial unit values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPATIALUNIT </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit[] getDefaultSpatialUnitValues() {
        throw new org.osid.IllegalStateException("not a SPATIALUNIT");
    }


    /**
     *  Gets the existing spatial unit values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing spatial unit values 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          SPATIALUNIT or <code> isValueKnown() </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit[] getExistingSpatialUnitValues() {
        throw new org.osid.IllegalStateException("not a SPATIALUNIT");
    }


    /**
     *  Gets the minimum speed value. 
     *
     *  @return the minimum speed 
     *  @throws org.osid.IllegalStateException syntax is not a <code> SPEED 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Speed getMinimumSpeed() {
        throw new org.osid.IllegalStateException("not a SPEED");
    }


    /**
     *  Gets the maximum speed value. 
     *
     *  @return the maximum speed 
     *  @throws org.osid.IllegalStateException syntax is not a <code> SPEED 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Speed getMaximumSpeed() {
        throw new org.osid.IllegalStateException("not a SPEED");
    }


    /**
     *  Gets the set of acceptable speed values. 
     *
     *  @return a set of speeds or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> SPEED 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Speed[] getSpeedSet() {
        throw new org.osid.IllegalStateException("not a SPEED");
    }


    /**
     *  Gets the default speed values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default speed values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPEED </code> or <code> isRequired() </code> is <code>
     *          true </code>
     */

    @OSID @Override
    public org.osid.mapping.Speed[] getDefaultSpeedValues() {
        throw new org.osid.IllegalStateException("not a SPEED");
    }


    /**
     *  Gets the existing speed values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing speed values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPEED </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.mapping.Speed[] getExistingSpeedValues() {
        throw new org.osid.IllegalStateException("not a SPEED");
    }


    /**
     *  Gets the minimum string length. 
     *
     *  @return the minimum string length 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public long getMinimumStringLength() {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the maximum string length. 
     *
     *  @return the maximum string length 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public long getMaximumStringLength() {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the set of valid string match types for use in validating
     *  a string. If the string match type indicates a regular
     *  expression then <code> getStringExpression() </code> returns a
     *  regular expression.
     *
     *  @return the set of string match types 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          STRING </code>
     */

    @OSID @Override
    public org.osid.type.Type[] getStringMatchTypes() {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Tests if the given string match type is supported. 
     *
     *  @param  stringMatchType a string match type 
     *  @return <code> true </code> if the given string match type Is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> stringMatchType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStringMatchType(org.osid.type.Type stringMatchType) {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the regular expression of an acceptable string for the given 
     *  string match type. 
     *
     *  @param  stringMatchType a string match type 
     *  @return the regular expression 
     *  @throws org.osid.NullArgumentException <code> stringMatchType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType </code> ) is <code> 
     *          false </code> 
     */

    @OSID @Override
    public String getStringExpression(org.osid.type.Type stringMatchType) {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the set of valid string formats. 
     *
     *  @return the set of valid text format types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getStringFormatTypes() {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the set of acceptable string values. 
     *
     *  @return a set of strings or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public String[] getStringSet() {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the default string values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default string values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          STRING </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public String[] getDefaultStringValues() {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the existing string values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing string values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          STRING </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public String[] getExistingStringValues() {
        throw new org.osid.IllegalStateException("not a STRING");
    }


    /**
     *  Gets the minimum time value. 
     *
     *  @return the minimum time 
     *  @throws org.osid.IllegalStateException syntax is not a <code> TIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time getMinimumTime() {
        throw new org.osid.IllegalStateException("not a TIME");
    }


    /**
     *  Gets the maximum time value. 
     *
     *  @return the maximum time 
     *  @throws org.osid.IllegalStateException syntax is not a <code> TIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time getMaximumTime() {
        throw new org.osid.IllegalStateException("not a TIME");
    }


    /**
     *  Gets the set of acceptable time values. 
     *
     *  @return a set of times or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> TIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time[] getTimeSet() {
        throw new org.osid.IllegalStateException("not a TIME");
    }


    /**
     *  Gets the default time values. These are the values used if the
     *  element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TIME </code> or <code> isRequired() </code> is <code>
     *          true </code>
     */

    @OSID @Override
    public org.osid.calendaring.Time[] getDefaultTimeValues() {
        throw new org.osid.IllegalStateException("not a TIME");
    }


    /**
     *  Gets the existing time values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TIME </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Time[] getExistingTimeValues() {
        throw new org.osid.IllegalStateException("not a TIME");
    }


    /**
     *  Gets the set of acceptable <code> Types. </code> 
     *
     *  @return a set of <code> Types </code> or an empty array if not 
     *          restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> TYPE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getTypeSet() {
        throw new org.osid.IllegalStateException("not a TYPE");
    }


    /**
     *  Gets the default type values. These are the values used if the element
     *  value is not provided or is cleared. If <code> isArray() </code> is 
     *  false, then this method returns at most a single value. 
     *
     *  @return the default type values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> TYPE 
     *          </code> or <code> isRequired() </code> is <code> true </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getDefaultTypeValues() {
        throw new org.osid.IllegalStateException("not a TYPE");
    }


    /**
     *  Gets the existing type values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing type values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TYPE </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.type.Type[] getExistingTypeValues() {
        throw new org.osid.IllegalStateException("not a TYPE");
    }


    /**
     *  Gets the set of acceptable version types. 
     *
     *  @return the set of version types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> VERSION 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getVersionTypes() {
        throw new org.osid.IllegalStateException("not a VERSION");
    }


    /**
     *  Tests if the given version type is supported. 
     *
     *  @param  versionType a version Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code> VERSION 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> versionType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVersionType(org.osid.type.Type versionType) {
        throw new org.osid.IllegalStateException("not a VERSION");
    }


    /**
     *  Gets the minumim acceptable <code> Version. </code> 
     *
     *  @return the minumim <code> Version </code> 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          VERSION </code>
     */

    @OSID @Override
    public org.osid.installation.Version getMinimumVersion() {
        throw new org.osid.IllegalStateException("not a VERSION");
    }


    /**
     *  Gets the maximum acceptable <code> Version. </code> 
     *
     *  @return the maximum <code> Version </code> 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          VERSION </code>
     */

    @OSID @Override
    public org.osid.installation.Version getMaximumVersion() {
        throw new org.osid.IllegalStateException("not a VERSION");
    }


    /**
     *  Gets the set of acceptable <code> Versions. </code> 
     *
     *  @return a set of <code> Versions </code> or an empty array if not 
     *          restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code> VERSION 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.Version[] getVersionSet() {
        throw new org.osid.IllegalStateException("not a VERSION");
    }

    
    /**
     *  Gets the default version values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default version values 
     *  @throws org.osid.IllegalStateException syntax is not a TIME or
     *          isValueKnown() is false
     */

    @OSID @Override
    public org.osid.installation.Version[] getDefaultVersionValues() {
        throw new org.osid.IllegalStateException("not a VERSION");
    }


    /**
     *  Gets the existing version values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing version values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          VERSION </code> or <code> isValueKnown() </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public org.osid.installation.Version[] getExistingVersionValues() {
        throw new org.osid.IllegalStateException("not a VERSION");
    }
}
