//
// InvariantMapJobConstrainerEnablerLookupSession
//
//    Implements a JobConstrainerEnabler lookup service backed by a fixed collection of
//    jobConstrainerEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobConstrainerEnabler lookup service backed by a fixed
 *  collection of job constrainer enablers. The job constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapJobConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobConstrainerEnablerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerEnablerLookupSession</code> with no
     *  job constrainer enablers.
     *  
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumnetException {@code foundry} is
     *          {@code null}
     */

    public InvariantMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerEnablerLookupSession</code> with a single
     *  job constrainer enabler.
     *  
     *  @param foundry the foundry
     *  @param jobConstrainerEnabler a single job constrainer enabler
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainerEnabler} is <code>null</code>
     */

      public InvariantMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {
        this(foundry);
        putJobConstrainerEnabler(jobConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerEnablerLookupSession</code> using an array
     *  of job constrainer enablers.
     *  
     *  @param foundry the foundry
     *  @param jobConstrainerEnablers an array of job constrainer enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.JobConstrainerEnabler[] jobConstrainerEnablers) {
        this(foundry);
        putJobConstrainerEnablers(jobConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobConstrainerEnablerLookupSession</code> using a
     *  collection of job constrainer enablers.
     *
     *  @param foundry the foundry
     *  @param jobConstrainerEnablers a collection of job constrainer enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapJobConstrainerEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainerEnabler> jobConstrainerEnablers) {
        this(foundry);
        putJobConstrainerEnablers(jobConstrainerEnablers);
        return;
    }
}
