//
// AbstractModelSearchOdrer.java
//
//     Defines a ModelSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.model.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ModelSearchOrder}.
 */

public abstract class AbstractModelSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.inventory.ModelSearchOrder {

    private final java.util.Collection<org.osid.inventory.records.ModelSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the 
     *  manufacturer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByManufacturer(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsManufacturerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a manufacturer resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManufacturerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getManufacturerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsManufacturerSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the archetype. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByArchetype(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the model 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  modelRecordType a model record type 
     *  @return {@code true} if the modelRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code modelRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type modelRecordType) {
        for (org.osid.inventory.records.ModelSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(modelRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  modelRecordType the model record type 
     *  @return the model search order record
     *  @throws org.osid.NullArgumentException
     *          {@code modelRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(modelRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.inventory.records.ModelSearchOrderRecord getModelSearchOrderRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ModelSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(modelRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(modelRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this model. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param modelRecord the model search odrer record
     *  @param modelRecordType model record type
     *  @throws org.osid.NullArgumentException
     *          {@code modelRecord} or
     *          {@code modelRecordTypemodel} is
     *          {@code null}
     */
            
    protected void addModelRecord(org.osid.inventory.records.ModelSearchOrderRecord modelSearchOrderRecord, 
                                     org.osid.type.Type modelRecordType) {

        addRecordType(modelRecordType);
        this.records.add(modelSearchOrderRecord);
        
        return;
    }
}
