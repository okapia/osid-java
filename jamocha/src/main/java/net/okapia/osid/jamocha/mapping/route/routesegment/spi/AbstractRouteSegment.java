//
// AbstractRouteSegment.java
//
//     Defines a RouteSegment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.routesegment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>RouteSegment</code>.
 */

public abstract class AbstractRouteSegment
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.mapping.route.RouteSegment {

    private org.osid.locale.DisplayText startingInstructions;
    private org.osid.locale.DisplayText endingInstructions;
    private org.osid.mapping.Distance distance;
    private org.osid.calendaring.Duration eta;
    private org.osid.mapping.path.Path path;

    private final java.util.Collection<org.osid.mapping.route.records.RouteSegmentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the starting instructions for this segment. 
     *
     *  @return the starting instructions 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getStartingInstructions() {
        return (this.startingInstructions);
    }


    /**
     *  Sets the starting instructions.
     *
     *  @param instructions the starting instructions
     *  @throws org.osid.NullArgumentException
     *          <code>instructions</code> is <code>null</code>
     */

    protected void setStartingInstructions(org.osid.locale.DisplayText instructions) {
        nullarg(instructions, "starting instructions");
        this.startingInstructions = instructions;
        return;
    }


    /**
     *  Gets the ending instructions for this segment. 
     *
     *  @return the ending instructions 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getEndingInstructions() {
        return (this.endingInstructions);
    }


    /**
     *  Sets the ending instructions.
     *
     *  @param instructions the ending instructions
     *  @throws org.osid.NullArgumentException
     *          <code>instructions</code> is <code>null</code>
     */

    protected void setEndingInstructions(org.osid.locale.DisplayText instructions) {
        nullarg(instructions, "ending instructions");
        this.endingInstructions = instructions;
        return;
    }


    /**
     *  Gets the length of the entire segment. 
     *
     *  @return the distance 
     */

    @OSID @Override
    public org.osid.mapping.Distance getDistance() {
        return (this.distance);
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException
     *          <code>distance</code> is <code>null</code>
     */

    protected void setDistance(org.osid.mapping.Distance distance) {
        nullarg(distance, "distance");
        this.distance = distance;
        return;
    }


    /**
     *  Gets the estimated travel time across the entire segment. 
     *
     *  @return the estimated travel time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getETA() {
        return (this.eta);
    }


    /**
     *  Sets the eta.
     *
     *  @param eta estimated time of arrival
     *  @throws org.osid.NullArgumentException <code>eta</code> is
     *          <code>null</code>
     */

    protected void setETA(org.osid.calendaring.Duration eta) {
        nullarg(eta, "estimated time of arrival");
        this.eta = eta;
        return;
    }


    /**
     *  Tests if this segment has a corresponding path. 
     *
     *  @return <code> true </code> if there is a path, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean hasPath() {
        return (this.path != null);
    }


    /**
     *  Gets the corresponding path <code> Id </code> on which this segment 
     *  travels. 
     *
     *  @return the path 
     *  @throws org.osid.IllegalStateException <code> hasPath() </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        if (!hasPath()) {
            throw new org.osid.IllegalStateException("hasPath() is false");
        }

        return (this.path.getId());
    }


    /**
     *  Gets the corresponding path on which this segment travels. 
     *
     *  @return the path 
     *  @throws org.osid.IllegalStateException <code> hasPath() </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        if (!hasPath()) {
            throw new org.osid.IllegalStateException("hasPath() is false");
        }

        return (this.path);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    protected void setPath(org.osid.mapping.path.Path path) {
        nullarg(path, "path");
        this.path = path;
        return;
    }


    /**
     *  Tests if this routeSegment supports the given record
     *  <code>Type</code>.
     *
     *  @param  routeSegmentRecordType a route segment record type 
     *  @return <code>true</code> if the routeSegmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type routeSegmentRecordType) {
        for (org.osid.mapping.route.records.RouteSegmentRecord record : this.records) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>RouteSegment</code> record <code>Type</code>.
     *
     *  @param  routeSegmentRecordType the route segment record type 
     *  @return the route segment record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeSegmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentRecord getRouteSegmentRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSegmentRecord record : this.records) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSegmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this route segment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param routeSegmentRecord the route segment record
     *  @param routeSegmentRecordType route segment record type
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecord</code> or
     *          <code>routeSegmentRecordTyperouteSegment</code> is
     *          <code>null</code>
     */
            
    protected void addRouteSegmentRecord(org.osid.mapping.route.records.RouteSegmentRecord routeSegmentRecord, 
                                         org.osid.type.Type routeSegmentRecordType) {

        nullarg(routeSegmentRecord, "route segment record");
        addRecordType(routeSegmentRecordType);
        this.records.add(routeSegmentRecord);
        
        return;
    }
}
