//
// AbstractMapAuctionConstrainerEnablerLookupSession
//
//    A simple framework for providing an AuctionConstrainerEnabler lookup service
//    backed by a fixed collection of auction constrainer enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AuctionConstrainerEnabler lookup service backed by a
 *  fixed collection of auction constrainer enablers. The auction constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuctionConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.bidding.rules.spi.AbstractAuctionConstrainerEnablerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.bidding.rules.AuctionConstrainerEnabler> auctionConstrainerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.bidding.rules.AuctionConstrainerEnabler>());


    /**
     *  Makes an <code>AuctionConstrainerEnabler</code> available in this session.
     *
     *  @param  auctionConstrainerEnabler an auction constrainer enabler
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnabler<code>
     *          is <code>null</code>
     */

    protected void putAuctionConstrainerEnabler(org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler) {
        this.auctionConstrainerEnablers.put(auctionConstrainerEnabler.getId(), auctionConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of auction constrainer enablers available in this session.
     *
     *  @param  auctionConstrainerEnablers an array of auction constrainer enablers
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuctionConstrainerEnablers(org.osid.bidding.rules.AuctionConstrainerEnabler[] auctionConstrainerEnablers) {
        putAuctionConstrainerEnablers(java.util.Arrays.asList(auctionConstrainerEnablers));
        return;
    }


    /**
     *  Makes a collection of auction constrainer enablers available in this session.
     *
     *  @param  auctionConstrainerEnablers a collection of auction constrainer enablers
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuctionConstrainerEnablers(java.util.Collection<? extends org.osid.bidding.rules.AuctionConstrainerEnabler> auctionConstrainerEnablers) {
        for (org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler : auctionConstrainerEnablers) {
            this.auctionConstrainerEnablers.put(auctionConstrainerEnabler.getId(), auctionConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes an AuctionConstrainerEnabler from this session.
     *
     *  @param  auctionConstrainerEnablerId the <code>Id</code> of the auction constrainer enabler
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeAuctionConstrainerEnabler(org.osid.id.Id auctionConstrainerEnablerId) {
        this.auctionConstrainerEnablers.remove(auctionConstrainerEnablerId);
        return;
    }


    /**
     *  Gets the <code>AuctionConstrainerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  auctionConstrainerEnablerId <code>Id</code> of the <code>AuctionConstrainerEnabler</code>
     *  @return the auctionConstrainerEnabler
     *  @throws org.osid.NotFoundException <code>auctionConstrainerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnabler getAuctionConstrainerEnabler(org.osid.id.Id auctionConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler = this.auctionConstrainerEnablers.get(auctionConstrainerEnablerId);
        if (auctionConstrainerEnabler == null) {
            throw new org.osid.NotFoundException("auctionConstrainerEnabler not found: " + auctionConstrainerEnablerId);
        }

        return (auctionConstrainerEnabler);
    }


    /**
     *  Gets all <code>AuctionConstrainerEnablers</code>. In plenary mode, the returned
     *  list contains all known auctionConstrainerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  auctionConstrainerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AuctionConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.ArrayAuctionConstrainerEnablerList(this.auctionConstrainerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionConstrainerEnablers.clear();
        super.close();
        return;
    }
}
