//
// AbstractPersonnelManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractPersonnelManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.personnel.PersonnelManager,
               org.osid.personnel.PersonnelProxyManager {

    private final Types personRecordTypes                  = new TypeRefSet();
    private final Types personSearchRecordTypes            = new TypeRefSet();

    private final Types organizationRecordTypes            = new TypeRefSet();
    private final Types organizationSearchRecordTypes      = new TypeRefSet();

    private final Types positionRecordTypes                = new TypeRefSet();
    private final Types positionSearchRecordTypes          = new TypeRefSet();

    private final Types appointmentRecordTypes             = new TypeRefSet();
    private final Types appointmentSearchRecordTypes       = new TypeRefSet();

    private final Types realmRecordTypes                   = new TypeRefSet();
    private final Types realmSearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractPersonnelManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractPersonnelManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any realm federation is exposed. Federation is exposed when a 
     *  specific realm may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of realms 
     *  appears as a single realm. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a person lookup service. 
     *
     *  @return <code> true </code> if person lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a person query service. 
     *
     *  @return <code> true </code> if person query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonQuery() {
        return (false);
    }


    /**
     *  Tests if searching for persons is available. 
     *
     *  @return <code> true </code> if person search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonSearch() {
        return (false);
    }


    /**
     *  Tests if managing for persons is available. 
     *
     *  @return <code> true </code> if a person adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonAdmin() {
        return (false);
    }


    /**
     *  Tests if person notification is available. 
     *
     *  @return <code> true </code> if person notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonNotification() {
        return (false);
    }


    /**
     *  Tests if a person to realm lookup session is available. 
     *
     *  @return <code> true </code> if person realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonRealm() {
        return (false);
    }


    /**
     *  Tests if a person to realm assignment session is available. 
     *
     *  @return <code> true </code> if person realm assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonRealmAssignment() {
        return (false);
    }


    /**
     *  Tests if a person smart realm session is available. 
     *
     *  @return <code> true </code> if person smart realm is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonSmartRealm() {
        return (false);
    }


    /**
     *  Tests for the availability of an organization lookup service. 
     *
     *  @return <code> true </code> if organization lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of an organization query service. 
     *
     *  @return <code> true </code> if organization query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (false);
    }


    /**
     *  Tests if searching for organizations is available. 
     *
     *  @return <code> true </code> if organization search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationSearch() {
        return (false);
    }


    /**
     *  Tests if managing for organizations is available. 
     *
     *  @return <code> true </code> if an organization adminstrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationAdmin() {
        return (false);
    }


    /**
     *  Tests if organization notification is available. 
     *
     *  @return <code> true </code> if organization notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationNotification() {
        return (false);
    }


    /**
     *  Tests if an organization hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an organization hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationHierarchy() {
        return (false);
    }


    /**
     *  Tests if organization hierarchy design is supported. 
     *
     *  @return <code> true </code> if an organization hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an organization to realm lookup session is available. 
     *
     *  @return <code> true </code> if organization realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationRealm() {
        return (false);
    }


    /**
     *  Tests if an organization to realm assignment session is available. 
     *
     *  @return <code> true </code> if organization realm assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationRealmAssignment() {
        return (false);
    }


    /**
     *  Tests if an organization smart realm session is available. 
     *
     *  @return <code> true </code> if organization smart realm is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationSmartRealm() {
        return (false);
    }


    /**
     *  Tests for the availability of a position lookup service. 
     *
     *  @return <code> true </code> if position lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a position query service. 
     *
     *  @return <code> true </code> if position query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionQuery() {
        return (false);
    }


    /**
     *  Tests if searching for positions is available. 
     *
     *  @return <code> true </code> if position search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionSearch() {
        return (false);
    }


    /**
     *  Tests if managing for positions is available. 
     *
     *  @return <code> true </code> if a position adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionAdmin() {
        return (false);
    }


    /**
     *  Tests if position notification is available. 
     *
     *  @return <code> true </code> if position notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionNotification() {
        return (false);
    }


    /**
     *  Tests if a position to realm lookup session is available. 
     *
     *  @return <code> true </code> if position realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionRealm() {
        return (false);
    }


    /**
     *  Tests if a position to realm assignment session is available. 
     *
     *  @return <code> true </code> if position realm assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionRealmAssignment() {
        return (false);
    }


    /**
     *  Tests if a position smart realm session is available. 
     *
     *  @return <code> true </code> if position smart realm is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionSmartRealm() {
        return (false);
    }


    /**
     *  Tests for the availability of an appointment lookup service. 
     *
     *  @return <code> true </code> if appointment lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of an appointment query service. 
     *
     *  @return <code> true </code> if appointment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentQuery() {
        return (false);
    }


    /**
     *  Tests if searching for appointments is available. 
     *
     *  @return <code> true </code> if appointment search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentSearch() {
        return (false);
    }


    /**
     *  Tests if managing for appointments is available. 
     *
     *  @return <code> true </code> if an appointment adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentAdmin() {
        return (false);
    }


    /**
     *  Tests if appointment notification is available. 
     *
     *  @return <code> true </code> if appointment notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentNotification() {
        return (false);
    }


    /**
     *  Tests if an appointment to realm lookup session is available. 
     *
     *  @return <code> true </code> if appointment realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentRealm() {
        return (false);
    }


    /**
     *  Tests if an appointment to realm assignment session is available. 
     *
     *  @return <code> true </code> if appointment realm assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentRealmAssignment() {
        return (false);
    }


    /**
     *  Tests if an appointment smart realm session is available. 
     *
     *  @return <code> true </code> if appointment smart realm is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentSmartRealm() {
        return (false);
    }


    /**
     *  Tests for the availability of an realm lookup service. 
     *
     *  @return <code> true </code> if realm lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmLookup() {
        return (false);
    }


    /**
     *  Tests if querying realms is available. 
     *
     *  @return <code> true </code> if realm query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Tests if searching for realms is available. 
     *
     *  @return <code> true </code> if realm search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a realm administrative service for 
     *  creating and deleting realms. 
     *
     *  @return <code> true </code> if realm administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a realm notification service. 
     *
     *  @return <code> true </code> if realm notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a realm hierarchy traversal service. 
     *
     *  @return <code> true </code> if realm hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a realm hierarchy design service. 
     *
     *  @return <code> true </code> if realm hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a personnel batch service. 
     *
     *  @return <code> true </code> if a personnel batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonnelBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Person </code> record types. 
     *
     *  @return a list containing the supported person record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPersonRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.personRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Person </code> record type is supported. 
     *
     *  @param  personRecordType a <code> Type </code> indicating a <code> 
     *          Person </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> personRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPersonRecordType(org.osid.type.Type personRecordType) {
        return (this.personRecordTypes.contains(personRecordType));
    }


    /**
     *  Adds support for a person record type.
     *
     *  @param personRecordType a person record type
     *  @throws org.osid.NullArgumentException
     *  <code>personRecordType</code> is <code>null</code>
     */

    protected void addPersonRecordType(org.osid.type.Type personRecordType) {
        this.personRecordTypes.add(personRecordType);
        return;
    }


    /**
     *  Removes support for a person record type.
     *
     *  @param personRecordType a person record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>personRecordType</code> is <code>null</code>
     */

    protected void removePersonRecordType(org.osid.type.Type personRecordType) {
        this.personRecordTypes.remove(personRecordType);
        return;
    }


    /**
     *  Gets the supported person search record types. 
     *
     *  @return a list containing the supported person search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPersonSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.personSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given person search record type is supported. 
     *
     *  @param  personSearchRecordType a <code> Type </code> indicating a 
     *          person record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> personSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPersonSearchRecordType(org.osid.type.Type personSearchRecordType) {
        return (this.personSearchRecordTypes.contains(personSearchRecordType));
    }


    /**
     *  Adds support for a person search record type.
     *
     *  @param personSearchRecordType a person search record type
     *  @throws org.osid.NullArgumentException
     *  <code>personSearchRecordType</code> is <code>null</code>
     */

    protected void addPersonSearchRecordType(org.osid.type.Type personSearchRecordType) {
        this.personSearchRecordTypes.add(personSearchRecordType);
        return;
    }


    /**
     *  Removes support for a person search record type.
     *
     *  @param personSearchRecordType a person search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>personSearchRecordType</code> is <code>null</code>
     */

    protected void removePersonSearchRecordType(org.osid.type.Type personSearchRecordType) {
        this.personSearchRecordTypes.remove(personSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Organization </code> record types. 
     *
     *  @return a list containing the supported organization record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrganizationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.organizationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Organization </code> record type is 
     *  supported. 
     *
     *  @param  organizationRecordType a <code> Type </code> indicating an 
     *          <code> Organization </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> organizationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrganizationRecordType(org.osid.type.Type organizationRecordType) {
        return (this.organizationRecordTypes.contains(organizationRecordType));
    }


    /**
     *  Adds support for an organization record type.
     *
     *  @param organizationRecordType an organization record type
     *  @throws org.osid.NullArgumentException
     *  <code>organizationRecordType</code> is <code>null</code>
     */

    protected void addOrganizationRecordType(org.osid.type.Type organizationRecordType) {
        this.organizationRecordTypes.add(organizationRecordType);
        return;
    }


    /**
     *  Removes support for an organization record type.
     *
     *  @param organizationRecordType an organization record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>organizationRecordType</code> is <code>null</code>
     */

    protected void removeOrganizationRecordType(org.osid.type.Type organizationRecordType) {
        this.organizationRecordTypes.remove(organizationRecordType);
        return;
    }


    /**
     *  Gets the supported organization search record types. 
     *
     *  @return a list containing the supported organization search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrganizationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.organizationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given organization search record type is supported. 
     *
     *  @param  organizationSearchRecordType a <code> Type </code> indicating 
     *          an organization record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          organizationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrganizationSearchRecordType(org.osid.type.Type organizationSearchRecordType) {
        return (this.organizationSearchRecordTypes.contains(organizationSearchRecordType));
    }


    /**
     *  Adds support for an organization search record type.
     *
     *  @param organizationSearchRecordType an organization search record type
     *  @throws org.osid.NullArgumentException
     *  <code>organizationSearchRecordType</code> is <code>null</code>
     */

    protected void addOrganizationSearchRecordType(org.osid.type.Type organizationSearchRecordType) {
        this.organizationSearchRecordTypes.add(organizationSearchRecordType);
        return;
    }


    /**
     *  Removes support for an organization search record type.
     *
     *  @param organizationSearchRecordType an organization search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>organizationSearchRecordType</code> is <code>null</code>
     */

    protected void removeOrganizationSearchRecordType(org.osid.type.Type organizationSearchRecordType) {
        this.organizationSearchRecordTypes.remove(organizationSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Position </code> record types. 
     *
     *  @return a list containing the supported position record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPositionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.positionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Position </code> record type is supported. 
     *
     *  @param  positionRecordType a <code> Type </code> indicating a <code> 
     *          Position </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> positionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPositionRecordType(org.osid.type.Type positionRecordType) {
        return (this.positionRecordTypes.contains(positionRecordType));
    }


    /**
     *  Adds support for a position record type.
     *
     *  @param positionRecordType a position record type
     *  @throws org.osid.NullArgumentException
     *  <code>positionRecordType</code> is <code>null</code>
     */

    protected void addPositionRecordType(org.osid.type.Type positionRecordType) {
        this.positionRecordTypes.add(positionRecordType);
        return;
    }


    /**
     *  Removes support for a position record type.
     *
     *  @param positionRecordType a position record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>positionRecordType</code> is <code>null</code>
     */

    protected void removePositionRecordType(org.osid.type.Type positionRecordType) {
        this.positionRecordTypes.remove(positionRecordType);
        return;
    }


    /**
     *  Gets the supported position search record types. 
     *
     *  @return a list containing the supported position search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPositionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.positionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given position search record type is supported. 
     *
     *  @param  positionSearchRecordType a <code> Type </code> indicating a 
     *          position record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> positionSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPositionSearchRecordType(org.osid.type.Type positionSearchRecordType) {
        return (this.positionSearchRecordTypes.contains(positionSearchRecordType));
    }


    /**
     *  Adds support for a position search record type.
     *
     *  @param positionSearchRecordType a position search record type
     *  @throws org.osid.NullArgumentException
     *  <code>positionSearchRecordType</code> is <code>null</code>
     */

    protected void addPositionSearchRecordType(org.osid.type.Type positionSearchRecordType) {
        this.positionSearchRecordTypes.add(positionSearchRecordType);
        return;
    }


    /**
     *  Removes support for a position search record type.
     *
     *  @param positionSearchRecordType a position search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>positionSearchRecordType</code> is <code>null</code>
     */

    protected void removePositionSearchRecordType(org.osid.type.Type positionSearchRecordType) {
        this.positionSearchRecordTypes.remove(positionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Appointment </code> record types. 
     *
     *  @return a list containing the supported appointment record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAppointmentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.appointmentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Appointment </code> record type is 
     *  supported. 
     *
     *  @param  appointmentRecordType a <code> Type </code> indicating an 
     *          <code> Appointment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> appointmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAppointmentRecordType(org.osid.type.Type appointmentRecordType) {
        return (this.appointmentRecordTypes.contains(appointmentRecordType));
    }


    /**
     *  Adds support for an appointment record type.
     *
     *  @param appointmentRecordType an appointment record type
     *  @throws org.osid.NullArgumentException
     *  <code>appointmentRecordType</code> is <code>null</code>
     */

    protected void addAppointmentRecordType(org.osid.type.Type appointmentRecordType) {
        this.appointmentRecordTypes.add(appointmentRecordType);
        return;
    }


    /**
     *  Removes support for an appointment record type.
     *
     *  @param appointmentRecordType an appointment record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>appointmentRecordType</code> is <code>null</code>
     */

    protected void removeAppointmentRecordType(org.osid.type.Type appointmentRecordType) {
        this.appointmentRecordTypes.remove(appointmentRecordType);
        return;
    }


    /**
     *  Gets the supported appointment search record types. 
     *
     *  @return a list containing the supported appointment search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAppointmentSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.appointmentSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given appointment search record type is supported. 
     *
     *  @param  appointmentSearchRecordType a <code> Type </code> indicating 
     *          an appointment record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          appointmentSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAppointmentSearchRecordType(org.osid.type.Type appointmentSearchRecordType) {
        return (this.appointmentSearchRecordTypes.contains(appointmentSearchRecordType));
    }


    /**
     *  Adds support for an appointment search record type.
     *
     *  @param appointmentSearchRecordType an appointment search record type
     *  @throws org.osid.NullArgumentException
     *  <code>appointmentSearchRecordType</code> is <code>null</code>
     */

    protected void addAppointmentSearchRecordType(org.osid.type.Type appointmentSearchRecordType) {
        this.appointmentSearchRecordTypes.add(appointmentSearchRecordType);
        return;
    }


    /**
     *  Removes support for an appointment search record type.
     *
     *  @param appointmentSearchRecordType an appointment search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>appointmentSearchRecordType</code> is <code>null</code>
     */

    protected void removeAppointmentSearchRecordType(org.osid.type.Type appointmentSearchRecordType) {
        this.appointmentSearchRecordTypes.remove(appointmentSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Realm </code> record types. 
     *
     *  @return a list containing the supported realm record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRealmRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.realmRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Realm </code> record type is supported. 
     *
     *  @param  realmRecordType a <code> Type </code> indicating a <code> 
     *          Realm </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> realmRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRealmRecordType(org.osid.type.Type realmRecordType) {
        return (this.realmRecordTypes.contains(realmRecordType));
    }


    /**
     *  Adds support for a realm record type.
     *
     *  @param realmRecordType a realm record type
     *  @throws org.osid.NullArgumentException
     *  <code>realmRecordType</code> is <code>null</code>
     */

    protected void addRealmRecordType(org.osid.type.Type realmRecordType) {
        this.realmRecordTypes.add(realmRecordType);
        return;
    }


    /**
     *  Removes support for a realm record type.
     *
     *  @param realmRecordType a realm record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>realmRecordType</code> is <code>null</code>
     */

    protected void removeRealmRecordType(org.osid.type.Type realmRecordType) {
        this.realmRecordTypes.remove(realmRecordType);
        return;
    }


    /**
     *  Gets the supported realm search record types. 
     *
     *  @return a list containing the supported realm search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRealmSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.realmSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given realm search record type is supported. 
     *
     *  @param  realmSearchRecordType a <code> Type </code> indicating a realm 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> realmSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRealmSearchRecordType(org.osid.type.Type realmSearchRecordType) {
        return (this.realmSearchRecordTypes.contains(realmSearchRecordType));
    }


    /**
     *  Adds support for a realm search record type.
     *
     *  @param realmSearchRecordType a realm search record type
     *  @throws org.osid.NullArgumentException
     *  <code>realmSearchRecordType</code> is <code>null</code>
     */

    protected void addRealmSearchRecordType(org.osid.type.Type realmSearchRecordType) {
        this.realmSearchRecordTypes.add(realmSearchRecordType);
        return;
    }


    /**
     *  Removes support for a realm search record type.
     *
     *  @param realmSearchRecordType a realm search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>realmSearchRecordType</code> is <code>null</code>
     */

    protected void removeRealmSearchRecordType(org.osid.type.Type realmSearchRecordType) {
        this.realmSearchRecordTypes.remove(realmSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person lookup 
     *  service. 
     *
     *  @return a <code> PersonLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonLookupSession getPersonLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonLookupSession getPersonLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person lookup 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PersonLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonLookupSession getPersonLookupSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person lookup 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonLookupSession getPersonLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person query 
     *  service. 
     *
     *  @return a <code> PersonQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuerySession getPersonQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuerySession getPersonQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person query 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PersonQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuerySession getPersonQuerySessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person query 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuerySession getPersonQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person search 
     *  service. 
     *
     *  @return a <code> PersonSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSearchSession getPersonSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSearchSession getPersonSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person search 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PersonSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSearchSession getPersonSearchSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person search 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSearchSession getPersonSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  administration service. 
     *
     *  @return a <code> PersonAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonAdminSession getPersonAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonAdminSession getPersonAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PersonAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonAdminSession getPersonAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonAdminSession getPersonAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  notification service. 
     *
     *  @param  personReceiver the receiver 
     *  @return a <code> PersonNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> personReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonNotificationSession getPersonNotificationSession(org.osid.personnel.PersonReceiver personReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  notification service. 
     *
     *  @param  personReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PersonNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> personReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonNotificationSession getPersonNotificationSession(org.osid.personnel.PersonReceiver personReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  notification service for the given realm. 
     *
     *  @param  personReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PersonNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> personReceiver </code> 
     *          or <code> realmId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonNotificationSession getPersonNotificationSessionForRealm(org.osid.personnel.PersonReceiver personReceiver, 
                                                                                             org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  notification service for the given realm. 
     *
     *  @param  personReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> personReceiver, realmId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonNotificationSession getPersonNotificationSessionForRealm(org.osid.personnel.PersonReceiver personReceiver, 
                                                                                             org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the session for retrieving person to realm mappings. 
     *
     *  @return a <code> PersonRealmSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonRealm() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonRealmSession getPersonRealmSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonRealmSession not implemented");
    }


    /**
     *  Gets the session for retrieving person to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonRealm() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonRealmSession getPersonRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonRealmSession not implemented");
    }


    /**
     *  Gets the session for assigning person to realm mappings. 
     *
     *  @return a <code> PersonRealmAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonRealmAssignmentSession getPersonRealmAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning person to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonRealmAssignmentSession getPersonRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the person smart realm for the given 
     *  realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @return a <code> PersonSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonSmartRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSmartRealmSession getPersonSmartRealmSession(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonSmartRealmSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic person realms for the given 
     *  realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return a <code> PersonSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonSmartRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSmartRealmSession getPersonSmartRealmSession(org.osid.id.Id realmId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonSmartRealmSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  lookup service. 
     *
     *  @return an <code> OrganizationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationLookupSession getOrganizationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationLookupSession getOrganizationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> OrganizationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationLookupSession getOrganizationLookupSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationLookupSession getOrganizationLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  query service. 
     *
     *  @return an <code> OrganizationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuerySession getOrganizationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuerySession getOrganizationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  query service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> OrganizationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuerySession getOrganizationQuerySessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  query service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuerySession getOrganizationQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  search service. 
     *
     *  @return an <code> OrganizationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchSession getOrganizationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchSession getOrganizationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> OrganizationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchSession getOrganizationSearchSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchSession getOrganizationSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  administration service. 
     *
     *  @return an <code> OrganizationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationAdminSession getOrganizationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationAdminSession getOrganizationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> OrganizationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationAdminSession getOrganizationAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationAdminSession getOrganizationAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  notification service. 
     *
     *  @param  organizationReceiver the receiver 
     *  @return an <code> OrganizationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> organizationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationNotificationSession getOrganizationNotificationSession(org.osid.personnel.OrganizationReceiver organizationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  notification service. 
     *
     *  @param  organizationReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> organizationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationNotificationSession getOrganizationNotificationSession(org.osid.personnel.OrganizationReceiver organizationReceiver, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  notification service for the given realm. 
     *
     *  @param  organizationReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> OrganizationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> organizationReceiver 
     *          </code> or <code> realmId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationNotificationSession getOrganizationNotificationSessionForRealm(org.osid.personnel.OrganizationReceiver organizationReceiver, 
                                                                                                         org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  notification service for the given realm. 
     *
     *  @param  organizationReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> organizationReceiver, 
     *          realmId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationNotificationSession getOrganizationNotificationSessionForRealm(org.osid.personnel.OrganizationReceiver organizationReceiver, 
                                                                                                         org.osid.id.Id realmId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the session traversing organization hierarchies. 
     *
     *  @return an <code> OrganizationHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchySession getOrganizationHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing organization hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchySession getOrganizationHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  heirarchy traversal service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @return an <code> OrganizationHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchySession getOrganizationHierarchySessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationHierarchySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  heirarchy traversal service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchySession getOrganizationHierarchySessionForRealm(org.osid.id.Id realmId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationHierarchySessionForRealm not implemented");
    }


    /**
     *  Gets the session designing organization hierarchies. 
     *
     *  @return an <code> OrganizationHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchyDesignSession getOrganizationHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing organization hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchyDesignSession getOrganizationHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  heirarchy design service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @return an <code> OrganizationHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchyDesignSession getOrganizationHierarchyDesignSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationHierarchyDesignSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  heirarchy design service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchyDesignSession getOrganizationHierarchyDesignSessionForRealm(org.osid.id.Id realmId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationHierarchyDesignSessionForRealm not implemented");
    }


    /**
     *  Gets the session for retrieving organization to realm mappings. 
     *
     *  @return an <code> OrganizationRealmSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationRealmSession getOrganizationRealmSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationRealmSession not implemented");
    }


    /**
     *  Gets the session for retrieving organization to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationRealmSession getOrganizationRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationRealmSession not implemented");
    }


    /**
     *  Gets the session for assigning organization to realm mappings. 
     *
     *  @return an <code> OrganizationRealmAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationRealmAssignmentSession getOrganizationRealmAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning organization to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationRealmAssignmentSession getOrganizationRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the organization smart realm for the 
     *  given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @return an <code> OrganizationSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSmartRealm() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSmartRealmSession getOrganizationSmartRealmSession(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getOrganizationSmartRealmSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic organization realms for the 
     *  given realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSmartRealm() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSmartRealmSession getOrganizationSmartRealmSession(org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getOrganizationSmartRealmSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  lookup service. 
     *
     *  @return a <code> PositionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionLookupSession getPositionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionLookupSession getPositionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PositionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionLookupSession getPositionLookupSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionLookupSession getPositionLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position query 
     *  service. 
     *
     *  @return a <code> PositionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuerySession getPositionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuerySession getPositionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position query 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PositionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuerySession getPositionQuerySessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position query 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuerySession getPositionQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  search service. 
     *
     *  @return a <code> PositionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSearchSession getPositionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSearchSession getPositionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PositionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSearchSession getPositionSearchSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSearchSession getPositionSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  administration service. 
     *
     *  @return a <code> PositionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionAdminSession getPositionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionAdminSession getPositionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PositionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionAdminSession getPositionAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionAdminSession getPositionAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  notification service. 
     *
     *  @param  positionReceiver the receiver 
     *  @return a <code> PositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> positionReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionNotificationSession getPositionNotificationSession(org.osid.personnel.PositionReceiver positionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  notification service. 
     *
     *  @param  positionReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> positionReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionNotificationSession getPositionNotificationSession(org.osid.personnel.PositionReceiver positionReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  notification service for the given realm. 
     *
     *  @param  positionReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> positionReceiver </code> 
     *          or <code> realmId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionNotificationSession getPositionNotificationSessionForRealm(org.osid.personnel.PositionReceiver positionReceiver, 
                                                                                                 org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  notification service for the given realm. 
     *
     *  @param  positionReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> positionReceiver, 
     *          realmId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionNotificationSession getPositionNotificationSessionForRealm(org.osid.personnel.PositionReceiver positionReceiver, 
                                                                                                 org.osid.id.Id realmId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the session for retrieving position to realm mappings. 
     *
     *  @return a <code> PositionRealmSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionRealm() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionRealmSession getPositionRealmSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionRealmSession not implemented");
    }


    /**
     *  Gets the session for retrieving position to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionRealm() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionRealmSession getPositionRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionRealmSession not implemented");
    }


    /**
     *  Gets the session for assigning position to realm mappings. 
     *
     *  @return a <code> PositionRealmAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionRealmAssignmentSession getPositionRealmAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning position to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionRealmAssignmentSession getPositionRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the position smart realm for the 
     *  given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @return a <code> PositionSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSmartRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSmartRealmSession getPositionSmartRealmSession(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPositionSmartRealmSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic position realms for the given 
     *  realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return a <code> PositionSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSmartRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSmartRealmSession getPositionSmartRealmSession(org.osid.id.Id realmId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPositionSmartRealmSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  lookup service. 
     *
     *  @return an <code> AppointmentLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentLookupSession getAppointmentLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentLookupSession getAppointmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> AppointmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentLookupSession getAppointmentLookupSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentLookupSession getAppointmentLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentLookupSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  query service. 
     *
     *  @return an <code> AppointmentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuerySession getAppointmentQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuerySession getAppointmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  query service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> AppointmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuerySession getAppointmentQuerySessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  query service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuerySession getAppointmentQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentQuerySessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  search service. 
     *
     *  @return an <code> AppointmentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSearchSession getAppointmentSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSearchSession getAppointmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> AppointmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSearchSession getAppointmentSearchSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSearchSession getAppointmentSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentSearchSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  administration service. 
     *
     *  @return an <code> AppointmentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentAdminSession getAppointmentAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentAdminSession getAppointmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> AppointmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentAdminSession getAppointmentAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentAdminSession getAppointmentAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  notification service. 
     *
     *  @param  appointmentReceiver the receiver 
     *  @return an <code> AppointmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> appointmentReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentNotificationSession getAppointmentNotificationSession(org.osid.personnel.AppointmentReceiver appointmentReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  notification service. 
     *
     *  @param  appointmentReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> appointmentReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentNotificationSession getAppointmentNotificationSession(org.osid.personnel.AppointmentReceiver appointmentReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  notification service for the given realm. 
     *
     *  @param  appointmentReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> AppointmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> appointmentReceiver 
     *          </code> or <code> realmId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentNotificationSession getAppointmentNotificationSessionForRealm(org.osid.personnel.AppointmentReceiver appointmentReceiver, 
                                                                                                       org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  notification service for the given realm. 
     *
     *  @param  appointmentReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> appointmentReceiver, 
     *          realmId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentNotificationSession getAppointmentNotificationSessionForRealm(org.osid.personnel.AppointmentReceiver appointmentReceiver, 
                                                                                                       org.osid.id.Id realmId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentNotificationSessionForRealm not implemented");
    }


    /**
     *  Gets the session for retrieving appointment to realm mappings. 
     *
     *  @return an <code> AppointmentRealmSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentRealmSession getAppointmentRealmSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentRealmSession not implemented");
    }


    /**
     *  Gets the session for retrieving appointment to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentRealmSession getAppointmentRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentRealmSession not implemented");
    }


    /**
     *  Gets the session for assigning appointment to realm mappings. 
     *
     *  @return an <code> AppointmentRealmAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentRealmAssignmentSession getAppointmentRealmAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning appointment to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentRealmAssignmentSession getAppointmentRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentRealmAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the appointment smart realm for the 
     *  given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @return an <code> AppointmentSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSmartRealm() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSmartRealmSession getAppointmentSmartRealmSession(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getAppointmentSmartRealmSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic appointment realms for the given 
     *  realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSmartRealm() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSmartRealmSession getAppointmentSmartRealmSession(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getAppointmentSmartRealmSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm lookup 
     *  service. 
     *
     *  @return a <code> RealmLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmLookupSession getRealmLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getRealmLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmLookupSession getRealmLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getRealmLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm query 
     *  service. 
     *
     *  @return a <code> RealmQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuerySession getRealmQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getRealmQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuerySession getRealmQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getRealmQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm search 
     *  service. 
     *
     *  @return a <code> RealmSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmSearchSession getRealmSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getRealmSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmSearchSession getRealmSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getRealmSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  administrative service. 
     *
     *  @return a <code> RealmAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmAdminSession getRealmAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getRealmAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmAdminSession getRealmAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getRealmAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  notification service. 
     *
     *  @param  realmReceiver the receiver 
     *  @return a <code> RealmNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> realmReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmNotificationSession getRealmNotificationSession(org.osid.personnel.RealmReceiver realmReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getRealmNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  notification service. 
     *
     *  @param  realmReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RealmNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> realmReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmNotificationSession getRealmNotificationSession(org.osid.personnel.RealmReceiver realmReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getRealmNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  hierarchy service. 
     *
     *  @return a <code> RealmHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmHierarchySession getRealmHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getRealmHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmHierarchySession getRealmHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getRealmHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  hierarchy design service. 
     *
     *  @return a <code> RealmHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmHierarchyDesignSession getRealmHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getRealmHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmHierarchyDesignSession getRealmHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getRealmHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> PersonnelBatchManager. </code> 
     *
     *  @return a <code> PersonnelBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonnelBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonnelBatchManager getPersonnelBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelManager.getPersonnelBatchManager not implemented");
    }


    /**
     *  Gets a <code> PersonnelBatchProxyManager. </code> 
     *
     *  @return a <code> PersonnelBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonnelBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonnelBatchProxyManager getPersonnelBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.PersonnelProxyManager.getPersonnelBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.personRecordTypes.clear();
        this.personRecordTypes.clear();

        this.personSearchRecordTypes.clear();
        this.personSearchRecordTypes.clear();

        this.organizationRecordTypes.clear();
        this.organizationRecordTypes.clear();

        this.organizationSearchRecordTypes.clear();
        this.organizationSearchRecordTypes.clear();

        this.positionRecordTypes.clear();
        this.positionRecordTypes.clear();

        this.positionSearchRecordTypes.clear();
        this.positionSearchRecordTypes.clear();

        this.appointmentRecordTypes.clear();
        this.appointmentRecordTypes.clear();

        this.appointmentSearchRecordTypes.clear();
        this.appointmentSearchRecordTypes.clear();

        this.realmRecordTypes.clear();
        this.realmRecordTypes.clear();

        this.realmSearchRecordTypes.clear();
        this.realmSearchRecordTypes.clear();

        return;
    }
}
