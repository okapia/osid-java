//
// InvariantMapRecurringEventEnablerLookupSession
//
//    Implements a RecurringEventEnabler lookup service backed by a fixed collection of
//    recurringEventEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements a RecurringEventEnabler lookup service backed by a fixed
 *  collection of recurring event enablers. The recurring event enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRecurringEventEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractMapRecurringEventEnablerLookupSession
    implements org.osid.calendaring.rules.RecurringEventEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventEnablerLookupSession</code> with no
     *  recurring event enablers.
     *  
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumnetException {@code calendar} is
     *          {@code null}
     */

    public InvariantMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventEnablerLookupSession</code> with a single
     *  recurring event enabler.
     *  
     *  @param calendar the calendar
     *  @param recurringEventEnabler a single recurring event enabler
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEventEnabler} is <code>null</code>
     */

      public InvariantMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler) {
        this(calendar);
        putRecurringEventEnabler(recurringEventEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventEnablerLookupSession</code> using an array
     *  of recurring event enablers.
     *  
     *  @param calendar the calendar
     *  @param recurringEventEnablers an array of recurring event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEventEnablers} is <code>null</code>
     */

      public InvariantMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.rules.RecurringEventEnabler[] recurringEventEnablers) {
        this(calendar);
        putRecurringEventEnablers(recurringEventEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventEnablerLookupSession</code> using a
     *  collection of recurring event enablers.
     *
     *  @param calendar the calendar
     *  @param recurringEventEnablers a collection of recurring event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEventEnablers} is <code>null</code>
     */

      public InvariantMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               java.util.Collection<? extends org.osid.calendaring.rules.RecurringEventEnabler> recurringEventEnablers) {
        this(calendar);
        putRecurringEventEnablers(recurringEventEnablers);
        return;
    }
}
