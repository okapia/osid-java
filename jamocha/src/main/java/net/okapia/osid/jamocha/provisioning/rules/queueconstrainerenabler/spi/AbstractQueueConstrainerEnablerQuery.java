//
// AbstractQueueConstrainerEnablerQuery.java
//
//     A template for making a QueueConstrainerEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for queue constrainer enablers.
 */

public abstract class AbstractQueueConstrainerEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.provisioning.rules.QueueConstrainerEnablerQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the queue constrainer. 
     *
     *  @param  queueConstrainerId the queue constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledQueueConstrainerId(org.osid.id.Id queueConstrainerId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the queue constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledQueueConstrainerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledQueueConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the queue constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledQueueConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerQuery getRuledQueueConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledQueueConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any queue constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any queue 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no queue constrainers 
     */

    @OSID @Override
    public void matchAnyRuledQueueConstrainer(boolean match) {
        return;
    }


    /**
     *  Clears the queue constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledQueueConstrainerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given queue constrainer enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue constrainer enabler implementing the requested record.
     *
     *  @param queueConstrainerEnablerRecordType a queue constrainer enabler record type
     *  @return the queue constrainer enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryRecord getQueueConstrainerEnablerQueryRecord(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue constrainer enabler query. 
     *
     *  @param queueConstrainerEnablerQueryRecord queue constrainer enabler query record
     *  @param queueConstrainerEnablerRecordType queueConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueConstrainerEnablerQueryRecord(org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryRecord queueConstrainerEnablerQueryRecord, 
                                          org.osid.type.Type queueConstrainerEnablerRecordType) {

        addRecordType(queueConstrainerEnablerRecordType);
        nullarg(queueConstrainerEnablerQueryRecord, "queue constrainer enabler query record");
        this.records.add(queueConstrainerEnablerQueryRecord);        
        return;
    }
}
