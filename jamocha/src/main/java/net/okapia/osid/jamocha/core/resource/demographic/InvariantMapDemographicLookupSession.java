//
// InvariantMapDemographicLookupSession
//
//    Implements a Demographic lookup service backed by a fixed collection of
//    demographics.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic;


/**
 *  Implements a Demographic lookup service backed by a fixed
 *  collection of demographics. The demographics are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapDemographicLookupSession
    extends net.okapia.osid.jamocha.core.resource.demographic.spi.AbstractMapDemographicLookupSession
    implements org.osid.resource.demographic.DemographicLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapDemographicLookupSession</code> with no
     *  demographics.
     *  
     *  @param bin the bin
     *  @throws org.osid.NullArgumnetException {@code bin} is
     *          {@code null}
     */

    public InvariantMapDemographicLookupSession(org.osid.resource.Bin bin) {
        setBin(bin);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDemographicLookupSession</code> with a single
     *  demographic.
     *  
     *  @param bin the bin
     *  @param demographic a single demographic
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code demographic} is <code>null</code>
     */

      public InvariantMapDemographicLookupSession(org.osid.resource.Bin bin,
                                               org.osid.resource.demographic.Demographic demographic) {
        this(bin);
        putDemographic(demographic);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDemographicLookupSession</code> using an array
     *  of demographics.
     *  
     *  @param bin the bin
     *  @param demographics an array of demographics
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code demographics} is <code>null</code>
     */

      public InvariantMapDemographicLookupSession(org.osid.resource.Bin bin,
                                               org.osid.resource.demographic.Demographic[] demographics) {
        this(bin);
        putDemographics(demographics);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDemographicLookupSession</code> using a
     *  collection of demographics.
     *
     *  @param bin the bin
     *  @param demographics a collection of demographics
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code demographics} is <code>null</code>
     */

      public InvariantMapDemographicLookupSession(org.osid.resource.Bin bin,
                                               java.util.Collection<? extends org.osid.resource.demographic.Demographic> demographics) {
        this(bin);
        putDemographics(demographics);
        return;
    }
}
