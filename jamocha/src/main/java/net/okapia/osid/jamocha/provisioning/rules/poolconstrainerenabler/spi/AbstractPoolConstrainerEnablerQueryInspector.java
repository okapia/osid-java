//
// AbstractPoolConstrainerEnablerQueryInspector.java
//
//     A template for making a PoolConstrainerEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for pool constrainer enablers.
 */

public abstract class AbstractPoolConstrainerEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.provisioning.rules.PoolConstrainerEnablerQueryInspector {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the pool constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledPoolConstrainerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the pool constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerQueryInspector[] getRuledPoolConstrainerTerms() {
        return (new org.osid.provisioning.rules.PoolConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given pool constrainer enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a pool constrainer enabler implementing the requested record.
     *
     *  @param poolConstrainerEnablerRecordType a pool constrainer enabler record type
     *  @return the pool constrainer enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord getPoolConstrainerEnablerQueryInspectorRecord(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(poolConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool constrainer enabler query. 
     *
     *  @param poolConstrainerEnablerQueryInspectorRecord pool constrainer enabler query inspector
     *         record
     *  @param poolConstrainerEnablerRecordType poolConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolConstrainerEnablerQueryInspectorRecord(org.osid.provisioning.rules.records.PoolConstrainerEnablerQueryInspectorRecord poolConstrainerEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type poolConstrainerEnablerRecordType) {

        addRecordType(poolConstrainerEnablerRecordType);
        nullarg(poolConstrainerEnablerRecordType, "pool constrainer enabler record type");
        this.records.add(poolConstrainerEnablerQueryInspectorRecord);        
        return;
    }
}
