//
// AbstractQueryModelLookupSession.java
//
//    An inline adapter that maps a ModelLookupSession to
//    a ModelQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ModelLookupSession to
 *  a ModelQuerySession.
 */

public abstract class AbstractQueryModelLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractModelLookupSession
    implements org.osid.inventory.ModelLookupSession {

    private final org.osid.inventory.ModelQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryModelLookupSession.
     *
     *  @param querySession the underlying model query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryModelLookupSession(org.osid.inventory.ModelQuerySession querySession) {
        nullarg(querySession, "model query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Warehouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform <code>Model</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupModels() {
        return (this.session.canSearchModels());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include models in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the <code>Model</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Model</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Model</code> and
     *  retained for compatibility.
     *
     *  @param  modelId <code>Id</code> of the
     *          <code>Model</code>
     *  @return the model
     *  @throws org.osid.NotFoundException <code>modelId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>modelId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel(org.osid.id.Id modelId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ModelQuery query = getQuery();
        query.matchId(modelId, true);
        org.osid.inventory.ModelList models = this.session.getModelsByQuery(query);
        if (models.hasNext()) {
            return (models.getNextModel());
        } 
        
        throw new org.osid.NotFoundException(modelId + " not found");
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  models specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Models</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  modelIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>modelIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByIds(org.osid.id.IdList modelIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ModelQuery query = getQuery();

        try (org.osid.id.IdList ids = modelIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getModelsByQuery(query));
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  model genus <code>Type</code> which does not include
     *  models of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ModelQuery query = getQuery();
        query.matchGenusType(modelGenusType, true);
        return (this.session.getModelsByQuery(query));
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  model genus <code>Type</code> and include any additional
     *  models with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByParentGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ModelQuery query = getQuery();
        query.matchParentGenusType(modelGenusType, true);
        return (this.session.getModelsByQuery(query));
    }


    /**
     *  Gets a <code>ModelList</code> containing the given
     *  model record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  modelRecordType a model record type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByRecordType(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ModelQuery query = getQuery();
        query.matchRecordType(modelRecordType, true);
        return (this.session.getModelsByQuery(query));
    }

    
    /**
     *  Gets a <code> ModelList </code> for the given resource <code>
     *  Id.  </code> In plenary mode, the returned list contains all
     *  known models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> Model </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByManufacturer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ModelQuery query = getQuery();
        query.matchManufacturerId(resourceId, true);
        return (this.session.getModelsByQuery(query));
    }


    /**
     *  Gets all <code>Models</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Models</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModels()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ModelQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getModelsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.inventory.ModelQuery getQuery() {
        org.osid.inventory.ModelQuery query = this.session.getModelQuery();
        return (query);
    }
}
