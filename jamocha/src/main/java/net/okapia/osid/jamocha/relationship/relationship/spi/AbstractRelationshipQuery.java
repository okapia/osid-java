//
// AbstractRelationshipQuery.java
//
//     A template for making a Relationship Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for relationships.
 */

public abstract class AbstractRelationshipQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.relationship.RelationshipQuery {

    private final java.util.Collection<org.osid.relationship.records.RelationshipQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a relationship peer. 
     *
     *  @param  peer peer <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peer </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id peer, boolean match) {
        return;
    }


    /**
     *  Clears the peer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        return;
    }


    /**
     *  Matches the other relationship peer. 
     *
     *  @param  peer peer <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peer </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDestinationId(org.osid.id.Id peer, boolean match) {
        return;
    }


    /**
     *  Clears the other peer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDestinationIdTerms() {
        return;
    }


    /**
     *  Matches circular relationships to the same peer. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSamePeerId(boolean match) {
        return;
    }


    /**
     *  Clears the same peer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSamePeerIdTerms() {
        return;
    }


    /**
     *  Sets the family <code> Id </code> for this query. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFamilyId(org.osid.id.Id familyId, boolean match) {
        return;
    }


    /**
     *  Clears the family <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFamilyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a family. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsFamilyQuery() is false");
    }


    /**
     *  Clears the family terms. 
     */

    @OSID @Override
    public void clearFamilyTerms() {
        return;
    }


    /**
     *  Gets the record corresponding to the given relationship query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a relationship implementing the requested record.
     *
     *  @param relationshipRecordType a relationship record type
     *  @return the relationship query record
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipQueryRecord getRelationshipQueryRecord(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.RelationshipQueryRecord record : this.records) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relationship query. 
     *
     *  @param relationshipQueryRecord relationship query record
     *  @param relationshipRecordType relationship record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelationshipQueryRecord(org.osid.relationship.records.RelationshipQueryRecord relationshipQueryRecord, 
                                          org.osid.type.Type relationshipRecordType) {

        addRecordType(relationshipRecordType);
        nullarg(relationshipQueryRecord, "relationship query record");
        this.records.add(relationshipQueryRecord);        
        return;
    }
}
