//
// AbstractDeedSearch.java
//
//     A template for making a Deed Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing deed searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDeedSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.room.squatting.DeedSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.room.squatting.records.DeedSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.room.squatting.DeedSearchOrder deedSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of deeds. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  deedIds list of deeds
     *  @throws org.osid.NullArgumentException
     *          <code>deedIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDeeds(org.osid.id.IdList deedIds) {
        while (deedIds.hasNext()) {
            try {
                this.ids.add(deedIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDeeds</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of deed Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDeedIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  deedSearchOrder deed search order 
     *  @throws org.osid.NullArgumentException
     *          <code>deedSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>deedSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDeedResults(org.osid.room.squatting.DeedSearchOrder deedSearchOrder) {
	this.deedSearchOrder = deedSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.room.squatting.DeedSearchOrder getDeedSearchOrder() {
	return (this.deedSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given deed search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a deed implementing the requested record.
     *
     *  @param deedSearchRecordType a deed search record
     *         type
     *  @return the deed search record
     *  @throws org.osid.NullArgumentException
     *          <code>deedSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deedSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedSearchRecord getDeedSearchRecord(org.osid.type.Type deedSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.room.squatting.records.DeedSearchRecord record : this.records) {
            if (record.implementsRecordType(deedSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this deed search. 
     *
     *  @param deedSearchRecord deed search record
     *  @param deedSearchRecordType deed search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDeedSearchRecord(org.osid.room.squatting.records.DeedSearchRecord deedSearchRecord, 
                                           org.osid.type.Type deedSearchRecordType) {

        addRecordType(deedSearchRecordType);
        this.records.add(deedSearchRecord);        
        return;
    }
}
