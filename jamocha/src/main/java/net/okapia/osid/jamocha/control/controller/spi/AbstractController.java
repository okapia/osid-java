//
// AbstractController.java
//
//     Defines a Controller.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.controller.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Controller</code>.
 */

public abstract class AbstractController
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.control.Controller {

    private String address;
    private org.osid.inventory.Model model;
    private org.osid.installation.Version version;

    private boolean isToggleable;
    private boolean isVariable;
    private boolean isVariableByPercentage;
    private boolean hasDiscreetStates;
    private boolean isRampable;

    private java.math.BigDecimal variableMinimum;
    private java.math.BigDecimal variableMaximum;
    private java.math.BigDecimal variableIncrement;
    private final java.util.Collection<org.osid.process.State> discreetStates = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.control.records.ControllerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the controller address. 
     *
     *  @return the address 
     */

    @OSID @Override
    public String getAddress() {
        return (this.address);
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    protected void setAddress(String address) {
        this.address = address;
        return;
    }


    /**
     *  Gets the controller model <code> Id. </code> 
     *
     *  @return the model <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getModelId() {
        return (this.model.getId());
    }


    /**
     *  Gets the controller model. 
     *
     *  @return the model 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel()
        throws org.osid.OperationFailedException {

        return (this.model);
    }


    /**
     *  Sets the model.
     *
     *  @param model a model
     *  @throws org.osid.NullArgumentException
     *          <code>model</code> is <code>null</code>
     */

    protected void setModel(org.osid.inventory.Model model) {
        nullarg(model, "model");
        this.model = model;
        return;
    }


    /**
     *  Gets the controller version. 
     *
     *  @return the version 
     */

    @OSID @Override
    public org.osid.installation.Version getVersion() {
        return (this.version);
    }


    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @throws org.osid.NullArgumentException
     *          <code>version</code> is <code>null</code>
     */

    protected void setVersion(org.osid.installation.Version version) {
        nullarg(version, "version");
        this.version = version;
        return;
    }


    /**
     *  Tests if this controller can be turned on and off. A
     *  controller that may also be variable if the minimum and
     *  maximum settings correspond to ON and OFF. A controller that
     *  defines discreet states may also presnet itself as toggleable
     *  if there are ON and OFF states.
     *
     *  @return <code> true </code> if this controller can be toggled,
     *          false otherwise
     */

    @OSID @Override
    public boolean isToggleable() {
        return (this.isToggleable);
    }


    /**
     *  Sets the toggleable.
     *
     *  @param toggleable {@code true} if a toggle, {@code false}
     *         otherwise
     */

    protected void setToggleable(boolean toggleable) {
        this.isToggleable = toggleable;
        return;
    }


    /**
     *  Tests if this controller supports levels between on and off. A
     *  variable controller may also be toggleable but does not define
     *  discreet states.
     *
     *  @return <code> true </code> if this controller has levels, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean isVariable() {
        return (this.isVariable);
    }


    /**
     *  Sets the variable.
     *
     *  @param variable {@code true} if a variable dial, {@code
     *         false} otherwise
     */

    protected void setVariable(boolean variable) {
        this.isVariable = variable;
        return;
    }


    /**
     *  Tests if the levels represent a percentage. 
     *
     *  @return <code> true </code> if this controller has levels as a 
     *          percentage, false otherwise 
     *  @throws org.osid.IllegalStateException <code> isVariable() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean isVariableByPercentage() {
        if (!isVariable()) {
            throw new org.osid.IllegalStateException("isVariable() is false");
        }

        return (this.isVariableByPercentage);
    }


    /**
     *  Sets the variable by percentage.
     *
     *  @param variableByPercentage {@code true} if a variable
     *         percentage dial, {@code false} otherwise
     */

    protected void setVariableByPercentage(boolean variableByPercentage) {
        setVariable(true);
        this.isVariableByPercentage = variableByPercentage;
        return;
    }


    /**
     *  Gets the minimum level. 
     *
     *  @return the minimum level 
     *  @throws org.osid.IllegalStateException <code> isVariable()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getVariableMinimum() {
        if (!isVariable()) {
            throw new org.osid.IllegalStateException("isVariable() is false");
        }

        return (this.variableMinimum);
    }


    /**
     *  Sets the variable minimum.
     *
     *  @param value a variable minimum
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    protected void setVariableMinimum(java.math.BigDecimal value) {
        nullarg(value, "variable minimum");
        this.variableMinimum = variableMinimum;
        return;
    }


    /**
     *  Gets the maximum level. 
     *
     *  @return the maximum level 
     *  @throws org.osid.IllegalStateException <code> isVariable() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getVariableMaximum() {
        if (!isVariable()) {
            throw new org.osid.IllegalStateException("isVariable() is false");
        }

        return (this.variableMaximum);
    }


    /**
     *  Sets the variable maximum.
     *
     *  @param value a variable maximum
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    protected void setVariableMaximum(java.math.BigDecimal value) {
        nullarg(value, "variable maximum");
        this.variableMaximum = value;
        return;
    }


    /**
     *  Gets the increments in the level. 
     *
     *  @return the increment 
     *  @throws org.osid.IllegalStateException <code> isVariable() </code> is 
     *          <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getVariableIncrement() {
        if (!isVariable()) {
            throw new org.osid.IllegalStateException("isVariable() is false");
        }

        return (this.variableIncrement);
    }


    /**
     *  Sets the variable increment.
     *
     *  @param increment a variable increment
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    protected void setVariableIncrement(java.math.BigDecimal increment) {
        nullarg(increment, "variable increment");
        this.variableIncrement = increment;
        return;
    }


    /**
     *  Tests if this controller supports discreet states. A state
     *  controller may also be toggleable but not variable.
     *
     *  @return <code> true </code> if this controller has discreet states, 
     *          false otherwise 
     */

    @OSID @Override
    public boolean hasDiscreetStates() {
        return (this.hasDiscreetStates);
    }


    /**
     *  Sets the has discreet states flag.
     *
     *  @param states {@code true} if has discreet states, {@code
     *         false} otherwise
     */

    protected void setDiscreetStates(boolean states) {
        this.hasDiscreetStates = states;
        return;
    }


    /**
     *  Gets the discreet <code> State </code> <code> Ids. </code> 
     *
     *  @return a list of state <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasDiscreetStates() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getDiscreetStateIds() {
        try (org.osid.process.StateList discreetStates = getDiscreetStates()) {
            return (new net.okapia.osid.jamocha.adapter.converter.process.state.StateToIdList(discreetStates));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the discreet <code> States. </code> 
     *
     *  @return a list of states 
     *  @throws org.osid.IllegalStateException <code> hasDiscreetStates() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.StateList getDiscreetStates()
        throws org.osid.OperationFailedException {

        if (!hasDiscreetStates()) {
            throw new org.osid.IllegalStateException("hasDoscreetStates() is false");
        }

        return (new net.okapia.osid.jamocha.process.state.ArrayStateList(this.discreetStates));
    }


    /**
     *  Adds a discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    protected void addDiscreetState(org.osid.process.State state) {
        nullarg(state, "discreet state");
        this.discreetStates.add(state);
        return;
    }


    /**
     *  Sets all the discreet states.
     *
     *  @param states a collection of discreet states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    protected void setDiscreetStates(java.util.Collection<org.osid.process.State> states) {
        nullarg(states, "discreet states");
        this.discreetStates.clear();
        this.discreetStates.addAll(discreetStates);
        return;
    }


    /**
     *  Clears all the discreet states.
     */

    protected void clearDiscreetStates() {
        this.discreetStates.clear();
        return;
    }


    /**
     *  Tests if this controller supports a ramp rate for a transition from 
     *  off to on. 
     *
     *  @return <code> true </code> if this controller supports ramp rates, 
     *          false otherwise 
     */

    @OSID @Override
    public boolean isRampable() {
        return (this.isRampable);
    }


    /**
     *  Sets the rampable.
     *
     *  @param rampable a rampable
     */

    protected void setRampable(boolean rampable) {
        this.isRampable = rampable;
        return;
    }


    /**
     *  Tests if this controller supports the given record
     *  <code>Type</code>.
     *
     *  @param  controllerRecordType a controller record type 
     *  @return <code>true</code> if the controllerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type controllerRecordType) {
        for (org.osid.control.records.ControllerRecord record : this.records) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  controllerRecordType the controller record type 
     *  @return the controller record 
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(controllerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerRecord getControllerRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ControllerRecord record : this.records) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this controller. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param controllerRecord the controller record
     *  @param controllerRecordType controller record type
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecord</code> or
     *          <code>controllerRecordTypecontroller</code> is
     *          <code>null</code>
     */
            
    protected void addControllerRecord(org.osid.control.records.ControllerRecord controllerRecord, 
                                     org.osid.type.Type controllerRecordType) {

        addRecordType(controllerRecordType);
        this.records.add(controllerRecord);
        
        return;
    }
}
