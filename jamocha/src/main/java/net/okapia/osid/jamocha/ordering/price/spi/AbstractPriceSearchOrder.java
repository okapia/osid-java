//
// AbstractPriceSearchOdrer.java
//
//     Defines a PriceSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.price.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PriceSearchOrder}.
 */

public abstract class AbstractPriceSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.ordering.PriceSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.PriceSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the price 
     *  schedule. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriceSchedule(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a price schedule order is available. 
     *
     *  @return <code> true </code> if a price schedule order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the price schedule order. 
     *
     *  @return the price schedule search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchOrder getPriceScheduleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  quantity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumQuantity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  quanttity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumQuantity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the demographic. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDemographic(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a demographic order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getDemographicSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDemographicSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the recurring 
     *  interval. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecurringInterval(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  priceRecordType a price record type 
     *  @return {@code true} if the priceRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code priceRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type priceRecordType) {
        for (org.osid.ordering.records.PriceSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(priceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  priceRecordType the price record type 
     *  @return the price search order record
     *  @throws org.osid.NullArgumentException
     *          {@code priceRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(priceRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.ordering.records.PriceSearchOrderRecord getPriceSearchOrderRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(priceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this price. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param priceRecord the price search odrer record
     *  @param priceRecordType price record type
     *  @throws org.osid.NullArgumentException
     *          {@code priceRecord} or
     *          {@code priceRecordTypeprice} is
     *          {@code null}
     */
            
    protected void addPriceRecord(org.osid.ordering.records.PriceSearchOrderRecord priceSearchOrderRecord, 
                                     org.osid.type.Type priceRecordType) {

        addRecordType(priceRecordType);
        this.records.add(priceSearchOrderRecord);
        
        return;
    }
}
