//
// InvariantIndexedMapProxyMapLookupSession
//
//    Implements a Map lookup service backed by a fixed
//    collection of maps indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping;


/**
 *  Implements a Map lookup service backed by a fixed
 *  collection of maps. The maps are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some maps may be compatible
 *  with more types than are indicated through these map
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyMapLookupSession
    extends net.okapia.osid.jamocha.core.mapping.spi.AbstractIndexedMapMapLookupSession
    implements org.osid.mapping.MapLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyMapLookupSession}
     *  using an array of maps.
     *
     *  @param maps an array of maps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code maps} or
     *          {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyMapLookupSession(org.osid.mapping.Map[] maps, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMaps(maps);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyMapLookupSession}
     *  using a collection of maps.
     *
     *  @param maps a collection of maps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code maps} or
     *          {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyMapLookupSession(java.util.Collection<? extends org.osid.mapping.Map> maps,
                                                         org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMaps(maps);
        return;
    }
}
