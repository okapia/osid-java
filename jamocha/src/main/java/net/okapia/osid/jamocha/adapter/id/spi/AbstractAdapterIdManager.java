//
// AbstractIdManager.java
//
//     An adapter for a IdManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.id.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a IdManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterIdManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.id.IdManager>
    implements org.osid.id.IdManager {


    /**
     *  Constructs a new {@code AbstractAdapterIdManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterIdManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterIdManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterIdManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if <code> Id </code> lookup is supported. 
     *
     *  @return <code> true </code> if <code> Id </code> lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdLookup() {
        return (getAdapteeManager().supportsIdLookup());
    }


    /**
     *  Tests if an <code> Id </code> issue service is supported. 
     *
     *  @return <code> true </code> if <code> Id </code> issuing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdIssue() {
        return (getAdapteeManager().supportsIdIssue());
    }


    /**
     *  Tests if an <code> Id </code> administrative service is supported. 
     *
     *  @return <code> true </code> if <code> Id </code> administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdAdmin() {
        return (getAdapteeManager().supportsIdAdmin());
    }


    /**
     *  Tests for the availability of an Id batch service. 
     *
     *  @return <code> true </code> if an Id batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdBatch() {
        return (getAdapteeManager().supportsIdBatch());
    }


    /**
     *  Gets the session associated with the id lookup service. 
     *
     *  @return an <code> IdLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdLookupSession getIdLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdLookupSession());
    }


    /**
     *  Gets the session associated with the id issue service. 
     *
     *  @return an <code> IdIssueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdIssue() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdIssueSession getIdIssueSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdIssueSession());
    }


    /**
     *  Gets the session associated with the id admin service. 
     *
     *  @return an <code> IdAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdAdminSession getIdAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdAdminSession());
    }


    /**
     *  Gets an <code> IdBatchManager. </code> 
     *
     *  @return an <code> IdBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.batch.IdBatchManager getIdBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
