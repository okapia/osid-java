//
// AbstractMapCanonicalUnitLookupSession
//
//    A simple framework for providing a CanonicalUnit lookup service
//    backed by a fixed collection of canonical units.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CanonicalUnit lookup service backed by a
 *  fixed collection of canonical units. The canonical units are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractCanonicalUnitLookupSession
    implements org.osid.offering.CanonicalUnitLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.CanonicalUnit> canonicalUnits = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.CanonicalUnit>());


    /**
     *  Makes a <code>CanonicalUnit</code> available in this session.
     *
     *  @param  canonicalUnit a canonical unit
     *  @throws org.osid.NullArgumentException <code>canonicalUnit<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit) {
        this.canonicalUnits.put(canonicalUnit.getId(), canonicalUnit);
        return;
    }


    /**
     *  Makes an array of canonical units available in this session.
     *
     *  @param  canonicalUnits an array of canonical units
     *  @throws org.osid.NullArgumentException <code>canonicalUnits<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnits(org.osid.offering.CanonicalUnit[] canonicalUnits) {
        putCanonicalUnits(java.util.Arrays.asList(canonicalUnits));
        return;
    }


    /**
     *  Makes a collection of canonical units available in this session.
     *
     *  @param  canonicalUnits a collection of canonical units
     *  @throws org.osid.NullArgumentException <code>canonicalUnits<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnits(java.util.Collection<? extends org.osid.offering.CanonicalUnit> canonicalUnits) {
        for (org.osid.offering.CanonicalUnit canonicalUnit : canonicalUnits) {
            this.canonicalUnits.put(canonicalUnit.getId(), canonicalUnit);
        }

        return;
    }


    /**
     *  Removes a CanonicalUnit from this session.
     *
     *  @param  canonicalUnitId the <code>Id</code> of the canonical unit
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId<code> is
     *          <code>null</code>
     */

    protected void removeCanonicalUnit(org.osid.id.Id canonicalUnitId) {
        this.canonicalUnits.remove(canonicalUnitId);
        return;
    }


    /**
     *  Gets the <code>CanonicalUnit</code> specified by its <code>Id</code>.
     *
     *  @param  canonicalUnitId <code>Id</code> of the <code>CanonicalUnit</code>
     *  @return the canonicalUnit
     *  @throws org.osid.NotFoundException <code>canonicalUnitId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnit getCanonicalUnit(org.osid.id.Id canonicalUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.CanonicalUnit canonicalUnit = this.canonicalUnits.get(canonicalUnitId);
        if (canonicalUnit == null) {
            throw new org.osid.NotFoundException("canonicalUnit not found: " + canonicalUnitId);
        }

        return (canonicalUnit);
    }


    /**
     *  Gets all <code>CanonicalUnits</code>. In plenary mode, the returned
     *  list contains all known canonicalUnits or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonicalUnits that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CanonicalUnits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.canonicalunit.ArrayCanonicalUnitList(this.canonicalUnits.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnits.clear();
        super.close();
        return;
    }
}
