//
// ProficiencyElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.proficiency.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProficiencyElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ProficiencyElement Id.
     *
     *  @return the proficiency element Id
     */

    public static org.osid.id.Id getProficiencyEntityId() {
        return (makeEntityId("osid.learning.Proficiency"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.learning.proficiency.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.learning.proficiency.Resource"));
    }


    /**
     *  Gets the ObjectiveId element Id.
     *
     *  @return the ObjectiveId element Id
     */

    public static org.osid.id.Id getObjectiveId() {
        return (makeElementId("osid.learning.proficiency.ObjectiveId"));
    }


    /**
     *  Gets the Objective element Id.
     *
     *  @return the Objective element Id
     */

    public static org.osid.id.Id getObjective() {
        return (makeElementId("osid.learning.proficiency.Objective"));
    }


    /**
     *  Gets the Completion element Id.
     *
     *  @return the Completion element Id
     */

    public static org.osid.id.Id getCompletion() {
        return (makeElementId("osid.learning.proficiency.Completion"));
    }


    /**
     *  Gets the LevelId element Id.
     *
     *  @return the LevelId element Id
     */

    public static org.osid.id.Id getLevelId() {
        return (makeElementId("osid.learning.proficiency.LevelId"));
    }


    /**
     *  Gets the Level element Id.
     *
     *  @return the Level element Id
     */

    public static org.osid.id.Id getLevel() {
        return (makeElementId("osid.learning.proficiency.Level"));
    }


    /**
     *  Gets the MinimumCompletion element Id.
     *
     *  @return the MinimumCompletion element Id
     */

    public static org.osid.id.Id getMinimumCompletion() {
        return (makeQueryElementId("osid.learning.proficiency.MinimumCompletion"));
    }


    /**
     *  Gets the ObjectiveBankId element Id.
     *
     *  @return the ObjectiveBankId element Id
     */

    public static org.osid.id.Id getObjectiveBankId() {
        return (makeQueryElementId("osid.learning.proficiency.ObjectiveBankId"));
    }


    /**
     *  Gets the ObjectiveBank element Id.
     *
     *  @return the ObjectiveBank element Id
     */

    public static org.osid.id.Id getObjectiveBank() {
        return (makeQueryElementId("osid.learning.proficiency.ObjectiveBank"));
    }
}
