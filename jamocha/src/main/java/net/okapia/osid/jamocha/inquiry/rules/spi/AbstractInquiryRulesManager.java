//
// AbstractInquiryRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInquiryRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.inquiry.rules.InquiryRulesManager,
               org.osid.inquiry.rules.InquiryRulesProxyManager {

    private final Types inquiryEnablerRecordTypes          = new TypeRefSet();
    private final Types inquiryEnablerSearchRecordTypes    = new TypeRefSet();

    private final Types auditEnablerRecordTypes            = new TypeRefSet();
    private final Types auditEnablerSearchRecordTypes      = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractInquiryRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInquiryRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up inquiry enablers is supported. 
     *
     *  @return <code> true </code> if inquiry enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying inquiry enablers is supported. 
     *
     *  @return <code> true </code> if inquiry enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching inquiry enablers is supported. 
     *
     *  @return <code> true </code> if inquiry enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an inquiry enabler administrative service is supported. 
     *
     *  @return <code> true </code> if inquiry enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an inquiry enabler notification service is supported. 
     *
     *  @return <code> true </code> if inquiry enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an inquiry enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an inquiry enabler inquest lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerInquest() {
        return (false);
    }


    /**
     *  Tests if an inquiry enabler inquest service is supported. 
     *
     *  @return <code> true </code> if inquiry enabler inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerInquestAssignment() {
        return (false);
    }


    /**
     *  Tests if an inquiry enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an inquiry enabler inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerSmartInquest() {
        return (false);
    }


    /**
     *  Tests if looking up audit enablers is supported. 
     *
     *  @return <code> true </code> if audit enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying audit enablers is supported. 
     *
     *  @return <code> true </code> if audit enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching audit enablers is supported. 
     *
     *  @return <code> true </code> if audit enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an audit enabler administrative service is supported. 
     *
     *  @return <code> true </code> if audit enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an audit enabler notification service is supported. 
     *
     *  @return <code> true </code> if audit enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an audit enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit enabler inquest lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerInquest() {
        return (false);
    }


    /**
     *  Tests if an audit enabler inquest service is supported. 
     *
     *  @return <code> true </code> if audit enabler inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerInquestAssignment() {
        return (false);
    }


    /**
     *  Tests if an audit enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit enabler inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerSmartInquest() {
        return (false);
    }


    /**
     *  Tests if an audit enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an audit enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an audit enabler rule application service is supported. 
     *
     *  @return <code> true </code> if audit enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> InquiryEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> InquiryEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquiryEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inquiryEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> InquiryEnabler </code> record type is 
     *  supported. 
     *
     *  @param  inquiryEnablerRecordType a <code> Type </code> indicating an 
     *          <code> InquiryEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerRecordType(org.osid.type.Type inquiryEnablerRecordType) {
        return (this.inquiryEnablerRecordTypes.contains(inquiryEnablerRecordType));
    }


    /**
     *  Adds support for an inquiry enabler record type.
     *
     *  @param inquiryEnablerRecordType an inquiry enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>inquiryEnablerRecordType</code> is <code>null</code>
     */

    protected void addInquiryEnablerRecordType(org.osid.type.Type inquiryEnablerRecordType) {
        this.inquiryEnablerRecordTypes.add(inquiryEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an inquiry enabler record type.
     *
     *  @param inquiryEnablerRecordType an inquiry enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inquiryEnablerRecordType</code> is <code>null</code>
     */

    protected void removeInquiryEnablerRecordType(org.osid.type.Type inquiryEnablerRecordType) {
        this.inquiryEnablerRecordTypes.remove(inquiryEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> InquiryEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> InquiryEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquiryEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inquiryEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> InquiryEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  inquiryEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> InquiryEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          inquiryEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerSearchRecordType(org.osid.type.Type inquiryEnablerSearchRecordType) {
        return (this.inquiryEnablerSearchRecordTypes.contains(inquiryEnablerSearchRecordType));
    }


    /**
     *  Adds support for an inquiry enabler search record type.
     *
     *  @param inquiryEnablerSearchRecordType an inquiry enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>inquiryEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addInquiryEnablerSearchRecordType(org.osid.type.Type inquiryEnablerSearchRecordType) {
        this.inquiryEnablerSearchRecordTypes.add(inquiryEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an inquiry enabler search record type.
     *
     *  @param inquiryEnablerSearchRecordType an inquiry enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inquiryEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeInquiryEnablerSearchRecordType(org.osid.type.Type inquiryEnablerSearchRecordType) {
        this.inquiryEnablerSearchRecordTypes.remove(inquiryEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuditEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> AuditEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auditEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuditEnabler </code> record type is 
     *  supported. 
     *
     *  @param  auditEnablerRecordType a <code> Type </code> indicating an 
     *          <code> AuditEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auditEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditEnablerRecordType(org.osid.type.Type auditEnablerRecordType) {
        return (this.auditEnablerRecordTypes.contains(auditEnablerRecordType));
    }


    /**
     *  Adds support for an audit enabler record type.
     *
     *  @param auditEnablerRecordType an audit enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>auditEnablerRecordType</code> is <code>null</code>
     */

    protected void addAuditEnablerRecordType(org.osid.type.Type auditEnablerRecordType) {
        this.auditEnablerRecordTypes.add(auditEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an audit enabler record type.
     *
     *  @param auditEnablerRecordType an audit enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auditEnablerRecordType</code> is <code>null</code>
     */

    protected void removeAuditEnablerRecordType(org.osid.type.Type auditEnablerRecordType) {
        this.auditEnablerRecordTypes.remove(auditEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuditEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> AuditEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auditEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuditEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  auditEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> AuditEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auditEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditEnablerSearchRecordType(org.osid.type.Type auditEnablerSearchRecordType) {
        return (this.auditEnablerSearchRecordTypes.contains(auditEnablerSearchRecordType));
    }


    /**
     *  Adds support for an audit enabler search record type.
     *
     *  @param auditEnablerSearchRecordType an audit enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auditEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addAuditEnablerSearchRecordType(org.osid.type.Type auditEnablerSearchRecordType) {
        this.auditEnablerSearchRecordTypes.add(auditEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an audit enabler search record type.
     *
     *  @param auditEnablerSearchRecordType an audit enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auditEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuditEnablerSearchRecordType(org.osid.type.Type auditEnablerSearchRecordType) {
        this.auditEnablerSearchRecordTypes.remove(auditEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler lookup service. 
     *
     *  @return an <code> InquiryEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerLookupSession getInquiryEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerLookupSession getInquiryEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerLookupSession getInquiryEnablerLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerLookupSession getInquiryEnablerLookupSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler query service. 
     *
     *  @return an <code> InquiryEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerQuerySession getInquiryEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerQuerySession getInquiryEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler query service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerQuerySession getInquiryEnablerQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler query service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerQuerySession getInquiryEnablerQuerySessionForInquest(org.osid.id.Id inquestId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler search service. 
     *
     *  @return an <code> InquiryEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSearchSession getInquiryEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSearchSession getInquiryEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enablers earch service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSearchSession getInquiryEnablerSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enablers earch service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSearchSession getInquiryEnablerSearchSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler administration service. 
     *
     *  @return an <code> InquiryEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerAdminSession getInquiryEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerAdminSession getInquiryEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerAdminSession getInquiryEnablerAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerAdminSession getInquiryEnablerAdminSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler notification service. 
     *
     *  @param  inquiryEnablerReceiver the notification callback 
     *  @return an <code> InquiryEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerNotificationSession getInquiryEnablerNotificationSession(org.osid.inquiry.rules.InquiryEnablerReceiver inquiryEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler notification service. 
     *
     *  @param  inquiryEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerNotificationSession getInquiryEnablerNotificationSession(org.osid.inquiry.rules.InquiryEnablerReceiver inquiryEnablerReceiver, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler notification service for the given inquest. 
     *
     *  @param  inquiryEnablerReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerReceiver 
     *          </code> or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerNotificationSession getInquiryEnablerNotificationSessionForInquest(org.osid.inquiry.rules.InquiryEnablerReceiver inquiryEnablerReceiver, 
                                                                                                                   org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler notification service for the given inquest. 
     *
     *  @param  inquiryEnablerReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerReceiver, 
     *          inquestId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerNotificationSession getInquiryEnablerNotificationSessionForInquest(org.osid.inquiry.rules.InquiryEnablerReceiver inquiryEnablerReceiver, 
                                                                                                                   org.osid.id.Id inquestId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inquiry enabler/inquest 
     *  mappings for inquiry enablers. 
     *
     *  @return an <code> InquiryEnablerInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerInquestSession getInquiryEnablerInquestSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inquiry enabler/inquest 
     *  mappings for inquiry enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerInquestSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerInquestSession getInquiryEnablerInquestSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning inquiry 
     *  enablers to inquests. 
     *
     *  @return an <code> InquiryEnablerInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerInquestAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerInquestAssignmentSession getInquiryEnablerInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning inquiry 
     *  enablers to inquests. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerInquestAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerInquestAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerInquestAssignmentSession getInquiryEnablerInquestAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage inquiry enabler smart 
     *  inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSmartInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSmartInquestSession getInquiryEnablerSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage inquiry enabler smart 
     *  inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSmartInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSmartInquestSession getInquiryEnablerSmartInquestSession(org.osid.id.Id inquestId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler mapping lookup service. 
     *
     *  @return an <code> InquiryEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleLookupSession getInquiryEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleLookupSession getInquiryEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler mapping lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleLookupSession getInquiryEnablerRuleLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerRuleLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler mapping lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleLookupSession getInquiryEnablerRuleLookupSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerRuleLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler assignment service. 
     *
     *  @return an <code> InquiryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleApplicationSession getInquiryEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleApplicationSession getInquiryEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler assignment service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleApplicationSession getInquiryEnablerRuleApplicationSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getInquiryEnablerRuleApplicationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler assignment service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleApplicationSession getInquiryEnablerRuleApplicationSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getInquiryEnablerRuleApplicationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  lookup service. 
     *
     *  @return an <code> AuditEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerLookupSession getAuditEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerLookupSession getAuditEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerLookupSession getAuditEnablerLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerLookupSession getAuditEnablerLookupSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  query service. 
     *
     *  @return an <code> AuditEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerQuerySession getAuditEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerQuerySession getAuditEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  query service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerQuerySession getAuditEnablerQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  query service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerQuerySession getAuditEnablerQuerySessionForInquest(org.osid.id.Id inquestId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  search service. 
     *
     *  @return an <code> AuditEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSearchSession getAuditEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSearchSession getAuditEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enablers 
     *  earch service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSearchSession getAuditEnablerSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enablers 
     *  earch service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSearchSession getAuditEnablerSearchSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  administration service. 
     *
     *  @return an <code> AuditEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerAdminSession getAuditEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerAdminSession getAuditEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerAdminSession getAuditEnablerAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerAdminSession getAuditEnablerAdminSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  notification service. 
     *
     *  @param  auditEnablerReceiver the notification callback 
     *  @return an <code> AuditEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auditEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerNotificationSession getAuditEnablerNotificationSession(org.osid.inquiry.rules.AuditEnablerReceiver auditEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  notification service. 
     *
     *  @param  auditEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auditEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerNotificationSession getAuditEnablerNotificationSession(org.osid.inquiry.rules.AuditEnablerReceiver auditEnablerReceiver, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  notification service for the given inquest. 
     *
     *  @param  auditEnablerReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auditEnablerReceiver 
     *          </code> or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerNotificationSession getAuditEnablerNotificationSessionForInquest(org.osid.inquiry.rules.AuditEnablerReceiver auditEnablerReceiver, 
                                                                                                               org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  notification service for the given inquest. 
     *
     *  @param  auditEnablerReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auditEnablerReceiver, 
     *          inquestId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerNotificationSession getAuditEnablerNotificationSessionForInquest(org.osid.inquiry.rules.AuditEnablerReceiver auditEnablerReceiver, 
                                                                                                               org.osid.id.Id inquestId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup audit enabler/inquest 
     *  mappings for audit enablers. 
     *
     *  @return an <code> AuditEnablerInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerInquestSession getAuditEnablerInquestSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup audit enabler/inquest 
     *  mappings for audit enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerInquestSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerInquestSession getAuditEnablerInquestSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning audit 
     *  enablers to inquests. 
     *
     *  @return an <code> AuditEnablerInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerInquestAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerInquestAssignmentSession getAuditEnablerInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning audit 
     *  enablers to inquests. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerInquestAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerInquestAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerInquestAssignmentSession getAuditEnablerInquestAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage audit enabler smart 
     *  inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSmartInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSmartInquestSession getAuditEnablerSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage audit enabler smart 
     *  inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSmartInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSmartInquestSession getAuditEnablerSmartInquestSession(org.osid.id.Id inquestId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  mapping lookup service. 
     *
     *  @return an <code> AuditEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleLookupSession getAuditEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleLookupSession getAuditEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  mapping lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleLookupSession getAuditEnablerRuleLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerRuleLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  mapping lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleLookupSession getAuditEnablerRuleLookupSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerRuleLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  assignment service. 
     *
     *  @return an <code> AuditEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleApplicationSession getAuditEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleApplicationSession getAuditEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  assignment service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleApplicationSession getAuditEnablerRuleApplicationSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesManager.getAuditEnablerRuleApplicationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  assignment service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleApplicationSession getAuditEnablerRuleApplicationSessionForInquest(org.osid.id.Id inquestId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.rules.InquiryRulesProxyManager.getAuditEnablerRuleApplicationSessionForInquest not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.inquiryEnablerRecordTypes.clear();
        this.inquiryEnablerRecordTypes.clear();

        this.inquiryEnablerSearchRecordTypes.clear();
        this.inquiryEnablerSearchRecordTypes.clear();

        this.auditEnablerRecordTypes.clear();
        this.auditEnablerRecordTypes.clear();

        this.auditEnablerSearchRecordTypes.clear();
        this.auditEnablerSearchRecordTypes.clear();

        return;
    }
}
