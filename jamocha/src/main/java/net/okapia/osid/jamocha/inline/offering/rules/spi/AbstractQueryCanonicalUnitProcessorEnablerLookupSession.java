//
// AbstractQueryCanonicalUnitProcessorEnablerLookupSession.java
//
//    An inline adapter that maps a CanonicalUnitProcessorEnablerLookupSession to
//    a CanonicalUnitProcessorEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CanonicalUnitProcessorEnablerLookupSession to
 *  a CanonicalUnitProcessorEnablerQuerySession.
 */

public abstract class AbstractQueryCanonicalUnitProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractCanonicalUnitProcessorEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession {

      private boolean activeonly    = false;
      private boolean effectiveonly = false;

    private final org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCanonicalUnitProcessorEnablerLookupSession.
     *
     *  @param querySession the underlying canonical unit processor enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession querySession) {
        nullarg(querySession, "canonical unit processor enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnitProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitProcessorEnablers() {
        return (this.session.canSearchCanonicalUnitProcessorEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit processor enablers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active canonical unit processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitProcessorEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive canonical unit processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitProcessorEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CanonicalUnitProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnitProcessorEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CanonicalUnitProcessorEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, canonical unit processor enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processor enablers
     *  are returned.
     *
     *  @param  canonicalUnitProcessorEnablerId <code>Id</code> of the
     *          <code>CanonicalUnitProcessorEnabler</code>
     *  @return the canonical unit processor enabler
     *  @throws org.osid.NotFoundException <code>canonicalUnitProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnabler getCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = getQuery();
        query.matchId(canonicalUnitProcessorEnablerId, true);
        org.osid.offering.rules.CanonicalUnitProcessorEnablerList canonicalUnitProcessorEnablers = this.session.getCanonicalUnitProcessorEnablersByQuery(query);
        if (canonicalUnitProcessorEnablers.hasNext()) {
            return (canonicalUnitProcessorEnablers.getNextCanonicalUnitProcessorEnabler());
        } 
        
        throw new org.osid.NotFoundException(canonicalUnitProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitProcessorEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CanonicalUnitProcessorEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, canonical unit processor enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processor enablers
     *  are returned.
     *
     *  @param  canonicalUnitProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByIds(org.osid.id.IdList canonicalUnitProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = canonicalUnitProcessorEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCanonicalUnitProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code> corresponding to the given
     *  canonical unit processor enabler genus <code>Type</code> which does not include
     *  canonical unit processor enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processor enablers
     *  are returned.
     *
     *  @param  canonicalUnitProcessorEnablerGenusType a canonicalUnitProcessorEnabler genus type 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByGenusType(org.osid.type.Type canonicalUnitProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = getQuery();
        query.matchGenusType(canonicalUnitProcessorEnablerGenusType, true);
        return (this.session.getCanonicalUnitProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code> corresponding to the given
     *  canonical unit processor enabler genus <code>Type</code> and include any additional
     *  canonical unit processor enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processor enablers
     *  are returned.
     *
     *  @param  canonicalUnitProcessorEnablerGenusType a canonicalUnitProcessorEnabler genus type 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByParentGenusType(org.osid.type.Type canonicalUnitProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = getQuery();
        query.matchParentGenusType(canonicalUnitProcessorEnablerGenusType, true);
        return (this.session.getCanonicalUnitProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code> containing the given
     *  canonical unit processor enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processor enablers
     *  are returned.
     *
     *  @param  canonicalUnitProcessorEnablerRecordType a canonicalUnitProcessorEnabler record type 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = getQuery();
        query.matchRecordType(canonicalUnitProcessorEnablerRecordType, true);
        return (this.session.getCanonicalUnitProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, canonical unit processor enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processor enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCanonicalUnitProcessorEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>CanonicalUnitProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processor enablers
     *  are returned.
     *
     *  @return a list of <code>CanonicalUnitProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCanonicalUnitProcessorEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery getQuery() {
        org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery query = this.session.getCanonicalUnitProcessorEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
