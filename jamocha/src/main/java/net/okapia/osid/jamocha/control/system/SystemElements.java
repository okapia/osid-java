//
// SystemElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.system;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class SystemElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the SystemElement Id.
     *
     *  @return the system element Id
     */

    public static org.osid.id.Id getSystemEntityId() {
        return (makeEntityId("osid.control.System"));
    }


    /**
     *  Gets the DeviceId element Id.
     *
     *  @return the DeviceId element Id
     */

    public static org.osid.id.Id getDeviceId() {
        return (makeQueryElementId("osid.control.system.DeviceId"));
    }


    /**
     *  Gets the Device element Id.
     *
     *  @return the Device element Id
     */

    public static org.osid.id.Id getDevice() {
        return (makeQueryElementId("osid.control.system.Device"));
    }


    /**
     *  Gets the ControllerId element Id.
     *
     *  @return the ControllerId element Id
     */

    public static org.osid.id.Id getControllerId() {
        return (makeQueryElementId("osid.control.system.ControllerId"));
    }


    /**
     *  Gets the Controller element Id.
     *
     *  @return the Controller element Id
     */

    public static org.osid.id.Id getController() {
        return (makeQueryElementId("osid.control.system.Controller"));
    }


    /**
     *  Gets the InputId element Id.
     *
     *  @return the InputId element Id
     */

    public static org.osid.id.Id getInputId() {
        return (makeQueryElementId("osid.control.system.InputId"));
    }


    /**
     *  Gets the Input element Id.
     *
     *  @return the Input element Id
     */

    public static org.osid.id.Id getInput() {
        return (makeQueryElementId("osid.control.system.Input"));
    }


    /**
     *  Gets the SettingId element Id.
     *
     *  @return the SettingId element Id
     */

    public static org.osid.id.Id getSettingId() {
        return (makeQueryElementId("osid.control.system.SettingId"));
    }


    /**
     *  Gets the Setting element Id.
     *
     *  @return the Setting element Id
     */

    public static org.osid.id.Id getSetting() {
        return (makeQueryElementId("osid.control.system.Setting"));
    }


    /**
     *  Gets the SceneId element Id.
     *
     *  @return the SceneId element Id
     */

    public static org.osid.id.Id getSceneId() {
        return (makeQueryElementId("osid.control.system.SceneId"));
    }


    /**
     *  Gets the Scene element Id.
     *
     *  @return the Scene element Id
     */

    public static org.osid.id.Id getScene() {
        return (makeQueryElementId("osid.control.system.Scene"));
    }


    /**
     *  Gets the TriggerId element Id.
     *
     *  @return the TriggerId element Id
     */

    public static org.osid.id.Id getTriggerId() {
        return (makeQueryElementId("osid.control.system.TriggerId"));
    }


    /**
     *  Gets the Trigger element Id.
     *
     *  @return the Trigger element Id
     */

    public static org.osid.id.Id getTrigger() {
        return (makeQueryElementId("osid.control.system.Trigger"));
    }


    /**
     *  Gets the ActionGroupId element Id.
     *
     *  @return the ActionGroupId element Id
     */

    public static org.osid.id.Id getActionGroupId() {
        return (makeQueryElementId("osid.control.system.ActionGroupId"));
    }


    /**
     *  Gets the ActionGroup element Id.
     *
     *  @return the ActionGroup element Id
     */

    public static org.osid.id.Id getActionGroup() {
        return (makeQueryElementId("osid.control.system.ActionGroup"));
    }


    /**
     *  Gets the AncestorSystemId element Id.
     *
     *  @return the AncestorSystemId element Id
     */

    public static org.osid.id.Id getAncestorSystemId() {
        return (makeQueryElementId("osid.control.system.AncestorSystemId"));
    }


    /**
     *  Gets the AncestorSystem element Id.
     *
     *  @return the AncestorSystem element Id
     */

    public static org.osid.id.Id getAncestorSystem() {
        return (makeQueryElementId("osid.control.system.AncestorSystem"));
    }


    /**
     *  Gets the DescendantSystemId element Id.
     *
     *  @return the DescendantSystemId element Id
     */

    public static org.osid.id.Id getDescendantSystemId() {
        return (makeQueryElementId("osid.control.system.DescendantSystemId"));
    }


    /**
     *  Gets the DescendantSystem element Id.
     *
     *  @return the DescendantSystem element Id
     */

    public static org.osid.id.Id getDescendantSystem() {
        return (makeQueryElementId("osid.control.system.DescendantSystem"));
    }
}
