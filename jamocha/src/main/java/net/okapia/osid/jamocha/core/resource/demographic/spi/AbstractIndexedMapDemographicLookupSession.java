//
// AbstractIndexedMapDemographicLookupSession.java
//
//    A simple framework for providing a Demographic lookup service
//    backed by a fixed collection of demographics with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Demographic lookup service backed by a
 *  fixed collection of demographics. The demographics are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some demographics may be compatible
 *  with more types than are indicated through these demographic
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Demographics</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDemographicLookupSession
    extends AbstractMapDemographicLookupSession
    implements org.osid.resource.demographic.DemographicLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resource.demographic.Demographic> demographicsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.demographic.Demographic>());
    private final MultiMap<org.osid.type.Type, org.osid.resource.demographic.Demographic> demographicsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.demographic.Demographic>());


    /**
     *  Makes a <code>Demographic</code> available in this session.
     *
     *  @param  demographic a demographic
     *  @throws org.osid.NullArgumentException <code>demographic<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDemographic(org.osid.resource.demographic.Demographic demographic) {
        super.putDemographic(demographic);

        this.demographicsByGenus.put(demographic.getGenusType(), demographic);
        
        try (org.osid.type.TypeList types = demographic.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.demographicsByRecord.put(types.getNextType(), demographic);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a demographic from this session.
     *
     *  @param demographicId the <code>Id</code> of the demographic
     *  @throws org.osid.NullArgumentException <code>demographicId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDemographic(org.osid.id.Id demographicId) {
        org.osid.resource.demographic.Demographic demographic;
        try {
            demographic = getDemographic(demographicId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.demographicsByGenus.remove(demographic.getGenusType());

        try (org.osid.type.TypeList types = demographic.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.demographicsByRecord.remove(types.getNextType(), demographic);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDemographic(demographicId);
        return;
    }


    /**
     *  Gets a <code>DemographicList</code> corresponding to the given
     *  demographic genus <code>Type</code> which does not include
     *  demographics of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known demographics or an error results. Otherwise,
     *  the returned list may contain only those demographics that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  demographicGenusType a demographic genus type 
     *  @return the returned <code>Demographic</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByGenusType(org.osid.type.Type demographicGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographic.ArrayDemographicList(this.demographicsByGenus.get(demographicGenusType)));
    }


    /**
     *  Gets a <code>DemographicList</code> containing the given
     *  demographic record <code>Type</code>. In plenary mode, the
     *  returned list contains all known demographics or an error
     *  results. Otherwise, the returned list may contain only those
     *  demographics that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  demographicRecordType a demographic record type 
     *  @return the returned <code>demographic</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByRecordType(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographic.ArrayDemographicList(this.demographicsByRecord.get(demographicRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.demographicsByGenus.clear();
        this.demographicsByRecord.clear();

        super.close();

        return;
    }
}
