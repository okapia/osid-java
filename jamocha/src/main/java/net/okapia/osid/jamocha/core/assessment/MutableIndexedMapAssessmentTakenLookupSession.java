//
// MutableIndexedMapAssessmentTakenLookupSession
//
//    Implements an AssessmentTaken lookup service backed by a collection of
//    assessmentsTaken indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an AssessmentTaken lookup service backed by a collection of
 *  assessments taken. The assessments taken are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some assessments taken may be compatible
 *  with more types than are indicated through these assessment taken
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of assessments taken can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractIndexedMapAssessmentTakenLookupSession
    implements org.osid.assessment.AssessmentTakenLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAssessmentTakenLookupSession} with no assessments taken.
     *
     *  @param bank the bank
     *  @throws org.osid.NullArgumentException {@code bank}
     *          is {@code null}
     */

      public MutableIndexedMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentTakenLookupSession} with a
     *  single assessment taken.
     *  
     *  @param bank the bank
     *  @param  assessmentTaken an single assessmentTaken
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentTaken} is {@code null}
     */

    public MutableIndexedMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.AssessmentTaken assessmentTaken) {
        this(bank);
        putAssessmentTaken(assessmentTaken);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentTakenLookupSession} using an
     *  array of assessments taken.
     *
     *  @param bank the bank
     *  @param  assessmentsTaken an array of assessments taken
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsTaken} is {@code null}
     */

    public MutableIndexedMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.AssessmentTaken[] assessmentsTaken) {
        this(bank);
        putAssessmentsTaken(assessmentsTaken);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentTakenLookupSession} using a
     *  collection of assessments taken.
     *
     *  @param bank the bank
     *  @param  assessmentsTaken a collection of assessments taken
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsTaken} is {@code null}
     */

    public MutableIndexedMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                  java.util.Collection<? extends org.osid.assessment.AssessmentTaken> assessmentsTaken) {

        this(bank);
        putAssessmentsTaken(assessmentsTaken);
        return;
    }
    

    /**
     *  Makes an {@code AssessmentTaken} available in this session.
     *
     *  @param  assessmentTaken an assessment taken
     *  @throws org.osid.NullArgumentException {@code assessmentTaken{@code  is
     *          {@code null}
     */

    @Override
    public void putAssessmentTaken(org.osid.assessment.AssessmentTaken assessmentTaken) {
        super.putAssessmentTaken(assessmentTaken);
        return;
    }


    /**
     *  Makes an array of assessments taken available in this session.
     *
     *  @param  assessmentsTaken an array of assessments taken
     *  @throws org.osid.NullArgumentException {@code assessmentsTaken{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentsTaken(org.osid.assessment.AssessmentTaken[] assessmentsTaken) {
        super.putAssessmentsTaken(assessmentsTaken);
        return;
    }


    /**
     *  Makes collection of assessments taken available in this session.
     *
     *  @param  assessmentsTaken a collection of assessments taken
     *  @throws org.osid.NullArgumentException {@code assessmentTaken{@code  is
     *          {@code null}
     */

    @Override
    public void putAssessmentsTaken(java.util.Collection<? extends org.osid.assessment.AssessmentTaken> assessmentsTaken) {
        super.putAssessmentsTaken(assessmentsTaken);
        return;
    }


    /**
     *  Removes an AssessmentTaken from this session.
     *
     *  @param assessmentTakenId the {@code Id} of the assessment taken
     *  @throws org.osid.NullArgumentException {@code assessmentTakenId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAssessmentTaken(org.osid.id.Id assessmentTakenId) {
        super.removeAssessmentTaken(assessmentTakenId);
        return;
    }    
}
