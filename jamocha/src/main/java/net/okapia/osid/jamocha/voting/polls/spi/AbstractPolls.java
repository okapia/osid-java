//
// AbstractPolls.java
//
//     Defines a Polls.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.polls.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Polls</code>.
 */

public abstract class AbstractPolls
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalog
    implements org.osid.voting.Polls {

    private final java.util.Collection<org.osid.voting.records.PollsRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this polls supports the given record
     *  <code>Type</code>.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return <code>true</code> if the pollsRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pollsRecordType) {
        for (org.osid.voting.records.PollsRecord record : this.records) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Polls</code>
     *  record <code>Type</code>.
     *
     *  @param  pollsRecordType the polls record type 
     *  @return the polls record 
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pollsRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsRecord getPollsRecord(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.PollsRecord record : this.records) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pollsRecordType + " is not supported");
    }


    /**
     *  Adds a record to this polls. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pollsRecord the polls record
     *  @param pollsRecordType polls record type
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecord</code> or
     *          <code>pollsRecordTypepolls</code> is
     *          <code>null</code>
     */
            
    protected void addPollsRecord(org.osid.voting.records.PollsRecord pollsRecord, 
                                  org.osid.type.Type pollsRecordType) {

        nullarg(pollsRecord, "polls record");
        addRecordType(pollsRecordType);
        this.records.add(pollsRecord);
        
        return;
    }
}
