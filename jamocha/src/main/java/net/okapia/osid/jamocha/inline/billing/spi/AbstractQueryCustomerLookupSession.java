//
// AbstractQueryCustomerLookupSession.java
//
//    An inline adapter that maps a CustomerLookupSession to
//    a CustomerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CustomerLookupSession to
 *  a CustomerQuerySession.
 */

public abstract class AbstractQueryCustomerLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractCustomerLookupSession
    implements org.osid.billing.CustomerLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.billing.CustomerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCustomerLookupSession.
     *
     *  @param querySession the underlying customer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCustomerLookupSession(org.osid.billing.CustomerQuerySession querySession) {
        nullarg(querySession, "customer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Customer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCustomers() {
        return (this.session.canSearchCustomers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include customers in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only customers whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCustomerView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All customers of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCustomerView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Customer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Customer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Customer</code> and
     *  retained for compatibility.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  customerId <code>Id</code> of the
     *          <code>Customer</code>
     *  @return the customer
     *  @throws org.osid.NotFoundException <code>customerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>customerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer(org.osid.id.Id customerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchId(customerId, true);
        org.osid.billing.CustomerList customers = this.session.getCustomersByQuery(query);
        if (customers.hasNext()) {
            return (customers.getNextCustomer());
        } 
        
        throw new org.osid.NotFoundException(customerId + " not found");
    }


    /**
     *  Gets a <code>CustomerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  customers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Customers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, customers are returned that are currently effective.
     *  In any effective mode, effective customers and those currently expired
     *  are returned.
     *
     *  @param  customerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>customerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByIds(org.osid.id.IdList customerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();

        try (org.osid.id.IdList ids = customerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCustomersByQuery(query));
    }


    /**
     *  Gets a <code>CustomerList</code> corresponding to the given
     *  customer genus <code>Type</code> which does not include
     *  customers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently effective.
     *  In any effective mode, effective customers and those currently expired
     *  are returned.
     *
     *  @param  customerGenusType a customer genus type 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByGenusType(org.osid.type.Type customerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchGenusType(customerGenusType, true);
        return (this.session.getCustomersByQuery(query));
    }


    /**
     *  Gets a <code>CustomerList</code> corresponding to the given
     *  customer genus <code>Type</code> and include any additional
     *  customers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  customerGenusType a customer genus type 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByParentGenusType(org.osid.type.Type customerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchParentGenusType(customerGenusType, true);
        return (this.session.getCustomersByQuery(query));
    }


    /**
     *  Gets a <code>CustomerList</code> containing the given
     *  customer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  customerRecordType a customer record type 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByRecordType(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchRecordType(customerRecordType, true);
        return (this.session.getCustomersByQuery(query));
    }


    /**
     *  Gets a <code>CustomerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible
     *  through this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Customer</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.billing.CustomerList getCustomersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCustomersByQuery(query));
    }
        

    /**
     *  Gets a <code> CustomerList </code> related to the given
     *  customer number.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  number a customer number 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.NullArgumentException <code> number </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchCustomerNumber(number, getStringMatchType(), true);
        return (this.session.getCustomersByQuery(query));
    }
        

    /**
     *  Gets a <code> CustomerList </code> related to the given resource 
     *  <code> . </code> 
     *  
     *  In plenary mode, the returned list contains all known customers or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  customers that are accessible through this session. 
     *  
     *  In effective mode, customers are returned that are currently 
     *  effective. In any effective mode, effective customers and those 
     *  currently expired are returned. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getCustomersByQuery(query));
    }
        

    /**
     *  Gets a <code> CustomerList </code> of the given resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective customers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          from, </code> or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByResourceOnDate(org.osid.id.Id resourceId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCustomersByQuery(query));
    }


    /**
     *  Gets a <code> CustomerList </code> having the given activity.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchActivityId(activityId, true);
        return (this.session.getCustomersByQuery(query));
    }


    /**
     *  Gets all <code>Customers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Customers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CustomerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCustomersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.billing.CustomerQuery getQuery() {
        org.osid.billing.CustomerQuery query = this.session.getCustomerQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }


    /**
     *  Gets the string match type for use in string queries.
     *
     *  @return a type
     */

    protected org.osid.type.Type getStringMatchType() {
        return (net.okapia.osid.primordium.types.search.StringMatchTypes.EXACT.getType());
    }
}
