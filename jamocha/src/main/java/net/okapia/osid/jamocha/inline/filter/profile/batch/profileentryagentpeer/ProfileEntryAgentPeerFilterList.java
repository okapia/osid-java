//
// ProfileEntryAgentPeerFilterList.java
//
//     Implements a filtering ProfileEntryAgentPeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.profile.batch.profileentryagentpeer;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering ProfileEntryAgentPeerList.
 */

public final class ProfileEntryAgentPeerFilterList
    extends net.okapia.osid.jamocha.inline.filter.profile.batch.profileentryagentpeer.spi.AbstractProfileEntryAgentPeerFilterList
    implements org.osid.profile.batch.ProfileEntryAgentPeerList,
               ProfileEntryAgentPeerFilter {

    private final ProfileEntryAgentPeerFilter filter;


    /**
     *  Creates a new <code>ProfileEntryAgentPeerFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>ProfileEntryAgentPeerList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public ProfileEntryAgentPeerFilterList(ProfileEntryAgentPeerFilter filter, org.osid.profile.batch.ProfileEntryAgentPeerList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters ProfileEntryAgentPeers.
     *
     *  @param profileEntryAgentPeer the profile entry agent peer to filter
     *  @return <code>true</code> if the profile entry agent peer passes the filter,
     *          <code>false</code> if the profile entry agent peer should be filtered
     */

    @Override
    public boolean pass(org.osid.profile.batch.ProfileEntryAgentPeer profileEntryAgentPeer) {
        return (this.filter.pass(profileEntryAgentPeer));
    }
}
