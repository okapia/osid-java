//
// FrontOfficeElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.frontoffice.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class FrontOfficeElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the FrontOfficeElement Id.
     *
     *  @return the front office element Id
     */

    public static org.osid.id.Id getFrontOfficeEntityId() {
        return (makeEntityId("osid.tracking.FrontOffice"));
    }


    /**
     *  Gets the IssueId element Id.
     *
     *  @return the IssueId element Id
     */

    public static org.osid.id.Id getIssueId() {
        return (makeQueryElementId("osid.tracking.frontoffice.IssueId"));
    }


    /**
     *  Gets the Issue element Id.
     *
     *  @return the Issue element Id
     */

    public static org.osid.id.Id getIssue() {
        return (makeQueryElementId("osid.tracking.frontoffice.Issue"));
    }


    /**
     *  Gets the QueueId element Id.
     *
     *  @return the QueueId element Id
     */

    public static org.osid.id.Id getQueueId() {
        return (makeQueryElementId("osid.tracking.frontoffice.QueueId"));
    }


    /**
     *  Gets the Queue element Id.
     *
     *  @return the Queue element Id
     */

    public static org.osid.id.Id getQueue() {
        return (makeQueryElementId("osid.tracking.frontoffice.Queue"));
    }


    /**
     *  Gets the AncestorFrontOfficeId element Id.
     *
     *  @return the AncestorFrontOfficeId element Id
     */

    public static org.osid.id.Id getAncestorFrontOfficeId() {
        return (makeQueryElementId("osid.tracking.frontoffice.AncestorFrontOfficeId"));
    }


    /**
     *  Gets the AncestorFrontOffice element Id.
     *
     *  @return the AncestorFrontOffice element Id
     */

    public static org.osid.id.Id getAncestorFrontOffice() {
        return (makeQueryElementId("osid.tracking.frontoffice.AncestorFrontOffice"));
    }


    /**
     *  Gets the DescendantFrontOfficeId element Id.
     *
     *  @return the DescendantFrontOfficeId element Id
     */

    public static org.osid.id.Id getDescendantFrontOfficeId() {
        return (makeQueryElementId("osid.tracking.frontoffice.DescendantFrontOfficeId"));
    }


    /**
     *  Gets the DescendantFrontOffice element Id.
     *
     *  @return the DescendantFrontOffice element Id
     */

    public static org.osid.id.Id getDescendantFrontOffice() {
        return (makeQueryElementId("osid.tracking.frontoffice.DescendantFrontOffice"));
    }
}
