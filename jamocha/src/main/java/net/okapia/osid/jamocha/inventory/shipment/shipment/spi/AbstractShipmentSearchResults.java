//
// AbstractShipmentSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractShipmentSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inventory.shipment.ShipmentSearchResults {

    private org.osid.inventory.shipment.ShipmentList shipments;
    private final org.osid.inventory.shipment.ShipmentQueryInspector inspector;
    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractShipmentSearchResults.
     *
     *  @param shipments the result set
     *  @param shipmentQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>shipments</code>
     *          or <code>shipmentQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractShipmentSearchResults(org.osid.inventory.shipment.ShipmentList shipments,
                                            org.osid.inventory.shipment.ShipmentQueryInspector shipmentQueryInspector) {
        nullarg(shipments, "shipments");
        nullarg(shipmentQueryInspector, "shipment query inspectpr");

        this.shipments = shipments;
        this.inspector = shipmentQueryInspector;

        return;
    }


    /**
     *  Gets the shipment list resulting from a search.
     *
     *  @return a shipment list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipments() {
        if (this.shipments == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inventory.shipment.ShipmentList shipments = this.shipments;
        this.shipments = null;
	return (shipments);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inventory.shipment.ShipmentQueryInspector getShipmentQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  shipment search record <code> Type. </code> This method must
     *  be used to retrieve a shipment implementing the requested
     *  record.
     *
     *  @param shipmentSearchRecordType a shipment search 
     *         record type 
     *  @return the shipment search
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(shipmentSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentSearchResultsRecord getShipmentSearchResultsRecord(org.osid.type.Type shipmentSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inventory.shipment.records.ShipmentSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(shipmentSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(shipmentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record shipment search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addShipmentRecord(org.osid.inventory.shipment.records.ShipmentSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "shipment record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
