//
// AbstractActivityUnit.java
//
//     Defines an ActivityUnit.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>ActivityUnit</code>.
 */

public abstract class AbstractActivityUnit
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.course.ActivityUnit {

    private org.osid.course.Course course;

    private boolean offerable = true;
    private boolean contact   = true;
    
    private org.osid.calendaring.Duration totalTargetEffort;
    private org.osid.calendaring.Duration totalTargetContactTime;
    private org.osid.calendaring.Duration totalTargetIndividualEffort;
    private org.osid.calendaring.Duration weeklyEffort;
    private org.osid.calendaring.Duration weeklyContactTime;
    private org.osid.calendaring.Duration weeklyIndividualEffort;

    private boolean hasLearningObjectives = false;
    private final java.util.Collection<org.osid.learning.Objective> learningObjectives = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.course.records.ActivityUnitRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Course </code>.
     *
     *  @return the <code> Course </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.course.getId());
    }


    /**
     *  Gets the <code>Course</code>. 
     *
     *  @return the course  
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.course);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course 
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    protected void setCourse(org.osid.course.Course course) {
        nullarg(course, "course");
        this.course = course;
        return;
    }


    /**
     *  Sets the offerable flag.
     *
     *  @param offerable a <code> true </code> if this activity is
     *          offerable, <code> false </code> otherwise
     */

    protected void setOfferable(boolean offerable) {
        this.offerable = offerable;
        return;
    }


    /**
     *  Gets the total time required for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetEffort() {
        return (this.totalTargetEffort);
    }


    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setTotalTargetEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "total target effort");
        this.totalTargetEffort = effort;
        return;
    }


    /**
     *  Tests if this is a contact activity. 
     *
     *  @return <code> true </code> if this is a contact activity,
     *          <code> false </code> if an independent activity
     */

    @OSID @Override
    public boolean isContact() {
        return (this.contact);
    }


    /**
     *  Sets the contact flag.
     *
     *  @param contact <code> true </code> if this is a contact
     *          activity, <code> false </code> if an independent
     *          activity
     */

    protected void setContact(boolean contact) {
        this.contact = contact;
        return;
    }


    /**
     *  Gets the total contact time for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetContactTime() {
        return (this.totalTargetContactTime);
    }


    /**
     *  Sets the total target contact time.
     *
     *  @param duration a total target contact time
     *  @throws org.osid.NullArgumentException <code>duration</code> is
     *          <code>null</code>
     */

    protected void setTotalTargetContactTime(org.osid.calendaring.Duration duration) {
        nullarg(duration, "total target contact time");
        this.totalTargetContactTime = duration;
        return;
    }


    /**
     *  Gets the total indivudal time required for this activity. 
     *
     *  @return the total individual effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetIndividualEffort() {
        return (this.totalTargetIndividualEffort);
    }


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setTotalTargetIndividualEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "total target individual ffort");
        this.totalTargetIndividualEffort = effort;
        return;
    }


    /**
     *  Tests if this activity is recurring. 
     *
     *  @return <code> true </code> if this activity is recurring, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isRecurringWeekly() {
        return ((this.weeklyEffort != null) && (this.weeklyContactTime != null) &&
                (this.weeklyIndividualEffort != null));
    }


    /**
     *  Gets the time required for this recurring effort on a weekly basis. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyEffort() {
        if (!isRecurringWeekly()) {
            throw new org.osid.IllegalStateException("isRecurringWeekly() is false");
        }

        return (this.weeklyEffort);
    }


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setWeeklyEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "weekly effort");
        this.weeklyEffort = effort;
        return;
    }


    /**
     *  Gets the weekly contact time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyContactTime() {
        if (!isRecurringWeekly()) {
            throw new org.osid.IllegalStateException("isRecurringWeekly() is false");
        }

        return (this.weeklyContactTime);
    }


    /**
     *  Sets the weekly contact time.
     *
     *  @param time a weekly contact time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setWeeklyContactTime(org.osid.calendaring.Duration time) {
        nullarg(time, "weekly cotact time");
        this.weeklyContactTime = time;
        return;
    }


    /**
     *  Gets the weekly individual time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyIndividualEffort() {
        if (!isRecurringWeekly()) {
            throw new org.osid.IllegalStateException("isRecurringWeekly() is false");
        }

        return (this.weeklyIndividualEffort);
    }


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setWeeklyIndividualEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "weekly individual effort");
        this.weeklyIndividualEffort = effort;
        return;
    }


    /**
     *  Tests if this activity unit has associated learning objectives. 
     *
     *  @return <code> true </code> if this activity unit has a learning 
     *          objective, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLearningObjectives() {
        return (this.hasLearningObjectives);
    }


    /**
     *  Gets the overall learning objective <code> Ids </code> for this 
     *  activity unit. 
     *
     *  @return <code> Ids </code> of the learning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        if (!hasLearningObjectives()) {
            throw new org.osid.IllegalStateException("hasLearningObjectives() is false");
        }

        try {
            org.osid.learning.ObjectiveList learningObjectives = getLearningObjectives();
            return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(learningObjectives));
        } catch (org.osid.OperationFailedException ofe) {
            throw new org.osid.IllegalStateException("hasLearningObjectives() is false");
        }
    }


    /**
     *  Gets the overall learning objectives for this activity unit. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        if (!hasLearningObjectives()) {
            throw new org.osid.IllegalStateException("hasLearningObjectives() is false");
        }

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.learningObjectives));
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    protected void addLearningObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "objective");

        this.learningObjectives.add(objective);
        this.hasLearningObjectives = true;

        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    protected void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        nullarg(objectives, "objectives");
        
        this.learningObjectives.clear();
        this.learningObjectives.addAll(objectives);
        this.hasLearningObjectives = true;

        return;
    }


    /**
     *  Tests if this activityUnit supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityUnitRecordType an activity unit record type 
     *  @return <code>true</code> if the activityUnitRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityUnitRecordType) {
        for (org.osid.course.records.ActivityUnitRecord record : this.records) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ActivityUnit</code> record <code>Type</code>.
     *
     *  @param  activityUnitRecordType the activity unit record type 
     *  @return the activity unit record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitRecord getActivityUnitRecord(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityUnitRecord record : this.records) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityUnitRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity unit. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityUnitRecord the activity unit record
     *  @param activityUnitRecordType activity unit record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecord</code> or
     *          <code>activityUnitRecordTypeactivityUnit</code> is
     *          <code>null</code>
     */
            
    protected void addActivityUnitRecord(org.osid.course.records.ActivityUnitRecord activityUnitRecord, 
                                         org.osid.type.Type activityUnitRecordType) {

        nullarg(activityUnitRecord, "activity unit record");
        addRecordType(activityUnitRecordType);
        this.records.add(activityUnitRecord);
        
        return;
    }
}
