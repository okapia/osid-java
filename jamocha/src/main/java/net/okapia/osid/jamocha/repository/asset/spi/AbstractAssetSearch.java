//
// AbstractAssetSearch.java
//
//     A template for making an Asset Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing asset searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAssetSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.repository.AssetSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.repository.records.AssetSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.repository.AssetSearchOrder assetSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of assets. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  assetIds list of assets
     *  @throws org.osid.NullArgumentException
     *          <code>assetIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAssets(org.osid.id.IdList assetIds) {
        while (assetIds.hasNext()) {
            try {
                this.ids.add(assetIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAssets</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of asset Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAssetIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  assetSearchOrder asset search order 
     *  @throws org.osid.NullArgumentException
     *          <code>assetSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>assetSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAssetResults(org.osid.repository.AssetSearchOrder assetSearchOrder) {
	this.assetSearchOrder = assetSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.repository.AssetSearchOrder getAssetSearchOrder() {
	return (this.assetSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given asset search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an asset implementing the requested record.
     *
     *  @param assetSearchRecordType an asset search record
     *         type
     *  @return the asset search record
     *  @throws org.osid.NullArgumentException
     *          <code>assetSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetSearchRecord getAssetSearchRecord(org.osid.type.Type assetSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.repository.records.AssetSearchRecord record : this.records) {
            if (record.implementsRecordType(assetSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this asset search. 
     *
     *  @param assetSearchRecord asset search record
     *  @param assetSearchRecordType asset search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssetSearchRecord(org.osid.repository.records.AssetSearchRecord assetSearchRecord, 
                                           org.osid.type.Type assetSearchRecordType) {

        addRecordType(assetSearchRecordType);
        this.records.add(assetSearchRecord);        
        return;
    }
}
