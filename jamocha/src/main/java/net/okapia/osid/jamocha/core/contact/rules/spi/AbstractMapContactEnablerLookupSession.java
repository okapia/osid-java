//
// AbstractMapContactEnablerLookupSession
//
//    A simple framework for providing a ContactEnabler lookup service
//    backed by a fixed collection of contact enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ContactEnabler lookup service backed by a
 *  fixed collection of contact enablers. The contact enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ContactEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapContactEnablerLookupSession
    extends net.okapia.osid.jamocha.contact.rules.spi.AbstractContactEnablerLookupSession
    implements org.osid.contact.rules.ContactEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.contact.rules.ContactEnabler> contactEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.contact.rules.ContactEnabler>());


    /**
     *  Makes a <code>ContactEnabler</code> available in this session.
     *
     *  @param  contactEnabler a contact enabler
     *  @throws org.osid.NullArgumentException <code>contactEnabler<code>
     *          is <code>null</code>
     */

    protected void putContactEnabler(org.osid.contact.rules.ContactEnabler contactEnabler) {
        this.contactEnablers.put(contactEnabler.getId(), contactEnabler);
        return;
    }


    /**
     *  Makes an array of contact enablers available in this session.
     *
     *  @param  contactEnablers an array of contact enablers
     *  @throws org.osid.NullArgumentException <code>contactEnablers<code>
     *          is <code>null</code>
     */

    protected void putContactEnablers(org.osid.contact.rules.ContactEnabler[] contactEnablers) {
        putContactEnablers(java.util.Arrays.asList(contactEnablers));
        return;
    }


    /**
     *  Makes a collection of contact enablers available in this session.
     *
     *  @param  contactEnablers a collection of contact enablers
     *  @throws org.osid.NullArgumentException <code>contactEnablers<code>
     *          is <code>null</code>
     */

    protected void putContactEnablers(java.util.Collection<? extends org.osid.contact.rules.ContactEnabler> contactEnablers) {
        for (org.osid.contact.rules.ContactEnabler contactEnabler : contactEnablers) {
            this.contactEnablers.put(contactEnabler.getId(), contactEnabler);
        }

        return;
    }


    /**
     *  Removes a ContactEnabler from this session.
     *
     *  @param  contactEnablerId the <code>Id</code> of the contact enabler
     *  @throws org.osid.NullArgumentException <code>contactEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeContactEnabler(org.osid.id.Id contactEnablerId) {
        this.contactEnablers.remove(contactEnablerId);
        return;
    }


    /**
     *  Gets the <code>ContactEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  contactEnablerId <code>Id</code> of the <code>ContactEnabler</code>
     *  @return the contactEnabler
     *  @throws org.osid.NotFoundException <code>contactEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>contactEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnabler getContactEnabler(org.osid.id.Id contactEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.contact.rules.ContactEnabler contactEnabler = this.contactEnablers.get(contactEnablerId);
        if (contactEnabler == null) {
            throw new org.osid.NotFoundException("contactEnabler not found: " + contactEnablerId);
        }

        return (contactEnabler);
    }


    /**
     *  Gets all <code>ContactEnablers</code>. In plenary mode, the returned
     *  list contains all known contactEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  contactEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ContactEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.rules.contactenabler.ArrayContactEnablerList(this.contactEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.contactEnablers.clear();
        super.close();
        return;
    }
}
