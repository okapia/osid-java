//
// GradebookBatchFormFilterList.java
//
//     Implements a filtering GradebookBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.grading.batch.gradebookbatchform;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering GradebookBatchFormList.
 */

public final class GradebookBatchFormFilterList
    extends net.okapia.osid.jamocha.inline.filter.grading.batch.gradebookbatchform.spi.AbstractGradebookBatchFormFilterList
    implements org.osid.grading.batch.GradebookBatchFormList,
               GradebookBatchFormFilter {

    private final GradebookBatchFormFilter filter;


    /**
     *  Creates a new <code>GradebookBatchFormFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>GradebookBatchFormList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public GradebookBatchFormFilterList(GradebookBatchFormFilter filter, org.osid.grading.batch.GradebookBatchFormList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters GradebookBatchForms.
     *
     *  @param gradebookBatchForm the gradebook batch form to filter
     *  @return <code>true</code> if the gradebook batch form passes the filter,
     *          <code>false</code> if the gradebook batch form should be filtered
     */

    @Override
    public boolean pass(org.osid.grading.batch.GradebookBatchForm gradebookBatchForm) {
        return (this.filter.pass(gradebookBatchForm));
    }
}
