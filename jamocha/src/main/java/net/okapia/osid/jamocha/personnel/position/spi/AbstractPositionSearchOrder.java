//
// AbstractPositionSearchOdrer.java
//
//     Defines a PositionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PositionSearchOrder}.
 */

public abstract class AbstractPositionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectSearchOrder
    implements org.osid.personnel.PositionSearchOrder {

    private final java.util.Collection<org.osid.personnel.records.PositionSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the 
     *  organization. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOrganization(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a organization search order is available. 
     *
     *  @return <code> true </code> if a organization search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the organization search order. 
     *
     *  @return the organization search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchOrder getOrganizationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOrganizationSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the title. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the level. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade level search order is available. 
     *
     *  @return <code> true </code> if a level search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets the level search order. 
     *
     *  @return the level search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the target 
     *  appointments. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTargetAppointments(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the required 
     *  commitment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiredCommitment(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the low salary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLowSalaryRange(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the midpoint 
     *  salary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMidpointSalaryRange(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the high salary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHighSalaryRange(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the compensation 
     *  frequency. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompensationFrequency(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the exempt flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByExempt(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the benefits 
     *  type. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBenefitsType(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  positionRecordType a position record type 
     *  @return {@code true} if the positionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code positionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type positionRecordType) {
        for (org.osid.personnel.records.PositionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(positionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  positionRecordType the position record type 
     *  @return the position search order record
     *  @throws org.osid.NullArgumentException
     *          {@code positionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(positionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.personnel.records.PositionSearchOrderRecord getPositionSearchOrderRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PositionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(positionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this position. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param positionRecord the position search odrer record
     *  @param positionRecordType position record type
     *  @throws org.osid.NullArgumentException
     *          {@code positionRecord} or
     *          {@code positionRecordTypeposition} is
     *          {@code null}
     */
            
    protected void addPositionRecord(org.osid.personnel.records.PositionSearchOrderRecord positionSearchOrderRecord, 
                                     org.osid.type.Type positionRecordType) {

        addRecordType(positionRecordType);
        this.records.add(positionSearchOrderRecord);
        
        return;
    }
}
