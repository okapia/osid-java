//
// AbstractAdapterPublisherLookupSession.java
//
//    A Publisher lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Publisher lookup session adapter.
 */

public abstract class AbstractAdapterPublisherLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.subscription.PublisherLookupSession {

    private final org.osid.subscription.PublisherLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPublisherLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPublisherLookupSession(org.osid.subscription.PublisherLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Publisher} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPublishers() {
        return (this.session.canLookupPublishers());
    }


    /**
     *  A complete view of the {@code Publisher} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePublisherView() {
        this.session.useComparativePublisherView();
        return;
    }


    /**
     *  A complete view of the {@code Publisher} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPublisherView() {
        this.session.usePlenaryPublisherView();
        return;
    }

     
    /**
     *  Gets the {@code Publisher} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Publisher} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Publisher} and
     *  retained for compatibility.
     *
     *  @param publisherId {@code Id} of the {@code Publisher}
     *  @return the publisher
     *  @throws org.osid.NotFoundException {@code publisherId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code publisherId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPublisher(publisherId));
    }


    /**
     *  Gets a {@code PublisherList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  publishers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Publishers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  publisherIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Publisher} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code publisherIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByIds(org.osid.id.IdList publisherIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPublishersByIds(publisherIds));
    }


    /**
     *  Gets a {@code PublisherList} corresponding to the given
     *  publisher genus {@code Type} which does not include
     *  publishers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  publisherGenusType a publisher genus type 
     *  @return the returned {@code Publisher} list
     *  @throws org.osid.NullArgumentException
     *          {@code publisherGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByGenusType(org.osid.type.Type publisherGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPublishersByGenusType(publisherGenusType));
    }


    /**
     *  Gets a {@code PublisherList} corresponding to the given
     *  publisher genus {@code Type} and include any additional
     *  publishers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  publisherGenusType a publisher genus type 
     *  @return the returned {@code Publisher} list
     *  @throws org.osid.NullArgumentException
     *          {@code publisherGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByParentGenusType(org.osid.type.Type publisherGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPublishersByParentGenusType(publisherGenusType));
    }


    /**
     *  Gets a {@code PublisherList} containing the given
     *  publisher record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  publisherRecordType a publisher record type 
     *  @return the returned {@code Publisher} list
     *  @throws org.osid.NullArgumentException
     *          {@code publisherRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByRecordType(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPublishersByRecordType(publisherRecordType));
    }


    /**
     *  Gets a {@code PublisherList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Publisher} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPublishersByProvider(resourceId));
    }


    /**
     *  Gets all {@code Publishers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Publishers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPublishers());
    }
}
