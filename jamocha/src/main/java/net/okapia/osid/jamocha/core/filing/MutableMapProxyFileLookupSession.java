//
// MutableMapProxyFileLookupSession
//
//    Implements a File lookup service backed by a collection of
//    files that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing;


/**
 *  Implements a File lookup service backed by a collection of
 *  files. The files are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of files can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyFileLookupSession
    extends net.okapia.osid.jamocha.core.filing.spi.AbstractMapFileLookupSession
    implements org.osid.filing.FileLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyFileLookupSession}
     *  with no files.
     *
     *  @param directory the directory
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyFileLookupSession(org.osid.filing.Directory directory,
                                                  org.osid.proxy.Proxy proxy) {
        setDirectory(directory);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyFileLookupSession} with a
     *  single file.
     *
     *  @param directory the directory
     *  @param file a file
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directory},
     *          {@code file}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyFileLookupSession(org.osid.filing.Directory directory,
                                                org.osid.filing.File file, org.osid.proxy.Proxy proxy) {
        this(directory, proxy);
        putFile(file);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyFileLookupSession} using an
     *  array of files.
     *
     *  @param directory the directory
     *  @param files an array of files
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directory},
     *          {@code files}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyFileLookupSession(org.osid.filing.Directory directory,
                                                org.osid.filing.File[] files, org.osid.proxy.Proxy proxy) {
        this(directory, proxy);
        putFiles(files);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyFileLookupSession} using a
     *  collection of files.
     *
     *  @param directory the directory
     *  @param files a collection of files
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directory},
     *          {@code files}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyFileLookupSession(org.osid.filing.Directory directory,
                                                java.util.Collection<? extends org.osid.filing.File> files,
                                                org.osid.proxy.Proxy proxy) {
   
        this(directory, proxy);
        setSessionProxy(proxy);
        putFiles(files);
        return;
    }

    
    /**
     *  Makes a {@code File} available in this session.
     *
     *  @param file an file
     *  @throws org.osid.NullArgumentException {@code file{@code 
     *          is {@code null}
     */

    @Override
    public void putFile(org.osid.filing.File file) {
        super.putFile(file);
        return;
    }


    /**
     *  Makes an array of files available in this session.
     *
     *  @param files an array of files
     *  @throws org.osid.NullArgumentException {@code files{@code 
     *          is {@code null}
     */

    @Override
    public void putFiles(org.osid.filing.File[] files) {
        super.putFiles(files);
        return;
    }


    /**
     *  Makes collection of files available in this session.
     *
     *  @param files
     *  @throws org.osid.NullArgumentException {@code file{@code 
     *          is {@code null}
     */

    @Override
    public void putFiles(java.util.Collection<? extends org.osid.filing.File> files) {
        super.putFiles(files);
        return;
    }


    /**
     *  Removes a File from this session.
     *
     *  @param fileId the {@code Id} of the file
     *  @throws org.osid.NullArgumentException {@code fileId{@code  is
     *          {@code null}
     */

    @Override
    public void removeFile(org.osid.id.Id fileId) {
        super.removeFile(fileId);
        return;
    }    
}
