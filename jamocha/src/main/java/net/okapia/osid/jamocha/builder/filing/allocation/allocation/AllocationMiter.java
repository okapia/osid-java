//
// AllocationMiter.java
//
//     Defines an Allocation miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.allocation.allocation;


/**
 *  Defines an <code>Allocation</code> miter for use with the builders.
 */

public interface AllocationMiter
    extends net.okapia.osid.jamocha.builder.spi.BrowsableMiter,
            org.osid.filing.allocation.Allocation {


    /**
     *  Sets the directory.
     *
     *  @param directory a directory
     *  @throws org.osid.NullArgumentException
     *          <code>directory</code> is <code>null</code>
     */

    public void setDirectory(org.osid.filing.Directory directory);


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    public void setAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the total space.
     *
     *  @param bytes the total space
     *  @throws org.osid.InvalidArgumentException <code>bytes</code>
     *          is negative
     */

    public void setTotalSpace(long bytes);


    /**
     *  Sets the used space.
     *
     *  @param bytes the used space
     *  @throws org.osid.InvalidArgumentException <code>bytes</code>
     *          is negative
     */

    public void setUsedSpace(long bytes);


    /**
     *  Sets the total files.
     *
     *  @param files the total files
     *  @throws org.osid.InvalidArgumentException <code>files</code>
     *          is negative
     */

    public void setTotalFiles(long files);


    /**
     *  Sets the used files.
     *
     *  @param files the used files
     *  @throws org.osid.InvalidArgumentException <code>files</code>
     *          is negative
     */

    public void setUsedFiles(long files);


    /**
     *  Adds an Allocation record.
     *
     *  @param record an allocation record
     *  @param recordType the type of allocation record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAllocationRecord(org.osid.filing.allocation.records.AllocationRecord record, org.osid.type.Type recordType);
}       


