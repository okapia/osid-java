//
// AbstractIndexedMapWarehouseLookupSession.java
//
//    A simple framework for providing a Warehouse lookup service
//    backed by a fixed collection of warehouses with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Warehouse lookup service backed by a
 *  fixed collection of warehouses. The warehouses are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some warehouses may be compatible
 *  with more types than are indicated through these warehouse
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Warehouses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapWarehouseLookupSession
    extends AbstractMapWarehouseLookupSession
    implements org.osid.inventory.WarehouseLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inventory.Warehouse> warehousesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Warehouse>());
    private final MultiMap<org.osid.type.Type, org.osid.inventory.Warehouse> warehousesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Warehouse>());


    /**
     *  Makes a <code>Warehouse</code> available in this session.
     *
     *  @param  warehouse a warehouse
     *  @throws org.osid.NullArgumentException <code>warehouse<code> is
     *          <code>null</code>
     */

    @Override
    protected void putWarehouse(org.osid.inventory.Warehouse warehouse) {
        super.putWarehouse(warehouse);

        this.warehousesByGenus.put(warehouse.getGenusType(), warehouse);
        
        try (org.osid.type.TypeList types = warehouse.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.warehousesByRecord.put(types.getNextType(), warehouse);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a warehouse from this session.
     *
     *  @param warehouseId the <code>Id</code> of the warehouse
     *  @throws org.osid.NullArgumentException <code>warehouseId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeWarehouse(org.osid.id.Id warehouseId) {
        org.osid.inventory.Warehouse warehouse;
        try {
            warehouse = getWarehouse(warehouseId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.warehousesByGenus.remove(warehouse.getGenusType());

        try (org.osid.type.TypeList types = warehouse.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.warehousesByRecord.remove(types.getNextType(), warehouse);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeWarehouse(warehouseId);
        return;
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  warehouse genus <code>Type</code> which does not include
     *  warehouses of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known warehouses or an error results. Otherwise,
     *  the returned list may contain only those warehouses that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.warehouse.ArrayWarehouseList(this.warehousesByGenus.get(warehouseGenusType)));
    }


    /**
     *  Gets a <code>WarehouseList</code> containing the given
     *  warehouse record <code>Type</code>. In plenary mode, the
     *  returned list contains all known warehouses or an error
     *  results. Otherwise, the returned list may contain only those
     *  warehouses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  warehouseRecordType a warehouse record type 
     *  @return the returned <code>warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByRecordType(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.warehouse.ArrayWarehouseList(this.warehousesByRecord.get(warehouseRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.warehousesByGenus.clear();
        this.warehousesByRecord.clear();

        super.close();

        return;
    }
}
