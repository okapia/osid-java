//
// AbstractDeleteResponse.java
//
//     Defines a DeleteResponse builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transaction.batch.deleteresponse.spi;


/**
 *  Defines a <code>DeleteResponse</code> builder.
 */

public abstract class AbstractDeleteResponseBuilder<T extends AbstractDeleteResponseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.transaction.batch.deleteresponse.DeleteResponseMiter deleteResponse;


    /**
     *  Constructs a new <code>AbstractDeleteResponseBuilder</code>.
     *
     *  @param deleteResponse the delete response to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDeleteResponseBuilder(net.okapia.osid.jamocha.builder.transaction.batch.deleteresponse.DeleteResponseMiter deleteResponse) {
        this.deleteResponse = deleteResponse;
        return;
    }


    /**
     *  Builds the delete response.
     *
     *  @return the new delete response
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>deleteResponse</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.transaction.batch.DeleteResponse build() {
        (new net.okapia.osid.jamocha.builder.validator.transaction.batch.deleteresponse.DeleteResponseValidator(getValidations())).validate(this.deleteResponse);
        return (new net.okapia.osid.jamocha.builder.transaction.batch.deleteresponse.ImmutableDeleteResponse(this.deleteResponse));
    }


    /**
     *  Gets the delete response. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new deleteResponse
     */

    @Override
    public net.okapia.osid.jamocha.builder.transaction.batch.deleteresponse.DeleteResponseMiter getMiter() {
        return (this.deleteResponse);
    }


    /**
     *  Sets the deleted id.
     *
     *  @param deletedId a deleted id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>deletedId</code> is <code>null</code>
     */

    public T deleted(org.osid.id.Id deletedId) {
        getMiter().setDeletedId(deletedId);
        return (self());
    }


    /**
     *  Sets the error message.
     *
     *  @param message an error message
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public T errorMessage(org.osid.locale.DisplayText message) {
        getMiter().setErrorMessage(message);
        return (self());
    }


}       


