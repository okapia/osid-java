//
// AbstractDocet.java
//
//     Defines a Docet builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.syllabus.docet.spi;


/**
 *  Defines a <code>Docet</code> builder.
 */

public abstract class AbstractDocetBuilder<T extends AbstractDocetBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.syllabus.docet.DocetMiter docet;


    /**
     *  Constructs a new <code>AbstractDocetBuilder</code>.
     *
     *  @param docet the docet to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDocetBuilder(net.okapia.osid.jamocha.builder.course.syllabus.docet.DocetMiter docet) {
        super(docet);
        this.docet = docet;
        return;
    }


    /**
     *  Builds the docet.
     *
     *  @return the new docet
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.syllabus.Docet build() {
        (new net.okapia.osid.jamocha.builder.validator.course.syllabus.docet.DocetValidator(getValidations())).validate(this.docet);
        return (new net.okapia.osid.jamocha.builder.course.syllabus.docet.ImmutableDocet(this.docet));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the docet miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.syllabus.docet.DocetMiter getMiter() {
        return (this.docet);
    }


    /**
     *  Sets the module.
     *
     *  @param module a module
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>module</code> is
     *          <code>null</code>
     */

    public T module(org.osid.course.syllabus.Module module) {
        getMiter().setModule(module);
        return (self());
    }


    /**
     *  Sets the activity unit.
     *
     *  @param activityUnit an activity unit
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnit</code> is <code>null</code>
     */

    public T activityUnit(org.osid.course.ActivityUnit activityUnit) {
        getMiter().setActivityUnit(activityUnit);
        return (self());
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public T objective(org.osid.learning.Objective objective) {
        getMiter().addLearningObjective(objective);
        return (self());
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    public T learningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        getMiter().setLearningObjectives(objectives);
        return (self());
    }


    /**
     *  Marks this docet as in class.
     *
     *  @return the builder
     */

    public T inClass() {
        getMiter().setInClass(true);
        return (self());
    }


    /**
     *  Marks this docet as not in class.
     *
     *  @return the builder
     */

    public T outOfClass() {
        getMiter().setInClass(false);
        return (self());
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public T duration(org.osid.calendaring.Duration duration) {
        getMiter().setDuration(duration);
        return (self());
    }


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public T asset(org.osid.repository.Asset asset) {
        getMiter().addAsset(asset);
        return (self());
    }


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    public T assets(java.util.Collection<org.osid.repository.Asset> assets) {
        getMiter().setAssets(assets);
        return (self());
    }


    /**
     *  Adds an assessment.
     *
     *  @param assessment an assessment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    public T assessment(org.osid.assessment.Assessment assessment) {
        getMiter().addAssessment(assessment);
        return (self());
    }


    /**
     *  Sets all the assessments.
     *
     *  @param assessments a collection of assessments
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assessments</code> is <code>null</code>
     */

    public T assessments(java.util.Collection<org.osid.assessment.Assessment> assessments) {
        getMiter().setAssessments(assessments);
        return (self());
    }


    /**
     *  Adds a Docet record.
     *
     *  @param record a docet record
     *  @param recordType the type of docet record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.syllabus.records.DocetRecord record, org.osid.type.Type recordType) {
        getMiter().addDocetRecord(record, recordType);
        return (self());
    }
}       


