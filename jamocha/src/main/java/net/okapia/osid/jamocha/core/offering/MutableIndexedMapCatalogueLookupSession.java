//
// MutableIndexedMapCatalogueLookupSession
//
//    Implements a Catalogue lookup service backed by a collection of
//    catalogues indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a Catalogue lookup service backed by a collection of
 *  catalogues. The catalogues are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some catalogues may be compatible
 *  with more types than are indicated through these catalogue
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of catalogues can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCatalogueLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractIndexedMapCatalogueLookupSession
    implements org.osid.offering.CatalogueLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCatalogueLookupSession} with no
     *  catalogues.
     */

    public MutableIndexedMapCatalogueLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCatalogueLookupSession} with a
     *  single catalogue.
     *  
     *  @param  catalogue a single catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue}
     *          is {@code null}
     */

    public MutableIndexedMapCatalogueLookupSession(org.osid.offering.Catalogue catalogue) {
        putCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCatalogueLookupSession} using an
     *  array of catalogues.
     *
     *  @param  catalogues an array of catalogues
     *  @throws org.osid.NullArgumentException {@code catalogues}
     *          is {@code null}
     */

    public MutableIndexedMapCatalogueLookupSession(org.osid.offering.Catalogue[] catalogues) {
        putCatalogues(catalogues);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCatalogueLookupSession} using a
     *  collection of catalogues.
     *
     *  @param  catalogues a collection of catalogues
     *  @throws org.osid.NullArgumentException {@code catalogues} is
     *          {@code null}
     */

    public MutableIndexedMapCatalogueLookupSession(java.util.Collection<? extends org.osid.offering.Catalogue> catalogues) {
        putCatalogues(catalogues);
        return;
    }
    

    /**
     *  Makes a {@code Catalogue} available in this session.
     *
     *  @param  catalogue a catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue{@code  is
     *          {@code null}
     */

    @Override
    public void putCatalogue(org.osid.offering.Catalogue catalogue) {
        super.putCatalogue(catalogue);
        return;
    }


    /**
     *  Makes an array of catalogues available in this session.
     *
     *  @param  catalogues an array of catalogues
     *  @throws org.osid.NullArgumentException {@code catalogues{@code 
     *          is {@code null}
     */

    @Override
    public void putCatalogues(org.osid.offering.Catalogue[] catalogues) {
        super.putCatalogues(catalogues);
        return;
    }


    /**
     *  Makes collection of catalogues available in this session.
     *
     *  @param  catalogues a collection of catalogues
     *  @throws org.osid.NullArgumentException {@code catalogue{@code  is
     *          {@code null}
     */

    @Override
    public void putCatalogues(java.util.Collection<? extends org.osid.offering.Catalogue> catalogues) {
        super.putCatalogues(catalogues);
        return;
    }


    /**
     *  Removes a Catalogue from this session.
     *
     *  @param catalogueId the {@code Id} of the catalogue
     *  @throws org.osid.NullArgumentException {@code catalogueId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCatalogue(org.osid.id.Id catalogueId) {
        super.removeCatalogue(catalogueId);
        return;
    }    
}
