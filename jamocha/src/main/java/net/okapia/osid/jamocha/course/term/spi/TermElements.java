//
// TermElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.term.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class TermElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the TermElement Id.
     *
     *  @return the term element Id
     */

    public static org.osid.id.Id getTermEntityId() {
        return (makeEntityId("osid.course.Term"));
    }


    /**
     *  Gets the DisplayLabel element Id.
     *
     *  @return the DisplayLabel element Id
     */

    public static org.osid.id.Id getDisplayLabel() {
        return (makeElementId("osid.course.term.DisplayLabel"));
    }


    /**
     *  Gets the OpenDate element Id.
     *
     *  @return the OpenDate element Id
     */

    public static org.osid.id.Id getOpenDate() {
        return (makeElementId("osid.course.term.OpenDate"));
    }


    /**
     *  Gets the RegistrationStart element Id.
     *
     *  @return the RegistrationStart element Id
     */

    public static org.osid.id.Id getRegistrationStart() {
        return (makeElementId("osid.course.term.RegistrationStart"));
    }


    /**
     *  Gets the RegistrationEnd element Id.
     *
     *  @return the RegistrationEnd element Id
     */

    public static org.osid.id.Id getRegistrationEnd() {
        return (makeElementId("osid.course.term.RegistrationEnd"));
    }


    /**
     *  Gets the ClassesStart element Id.
     *
     *  @return the ClassesStart element Id
     */

    public static org.osid.id.Id getClassesStart() {
        return (makeElementId("osid.course.term.ClassesStart"));
    }


    /**
     *  Gets the ClassesEnd element Id.
     *
     *  @return the ClassesEnd element Id
     */

    public static org.osid.id.Id getClassesEnd() {
        return (makeElementId("osid.course.term.ClassesEnd"));
    }


    /**
     *  Gets the AddDate element Id.
     *
     *  @return the AddDate element Id
     */

    public static org.osid.id.Id getAddDate() {
        return (makeElementId("osid.course.term.AddDate"));
    }


    /**
     *  Gets the DropDate element Id.
     *
     *  @return the DropDate element Id
     */

    public static org.osid.id.Id getDropDate() {
        return (makeElementId("osid.course.term.DropDate"));
    }


    /**
     *  Gets the FinalExamStart element Id.
     *
     *  @return the FinalExamStart element Id
     */

    public static org.osid.id.Id getFinalExamStart() {
        return (makeElementId("osid.course.term.FinalExamStart"));
    }


    /**
     *  Gets the FinalExamEnd element Id.
     *
     *  @return the FinalExamEnd element Id
     */

    public static org.osid.id.Id getFinalExamEnd() {
        return (makeElementId("osid.course.term.FinalExamEnd"));
    }


    /**
     *  Gets the CloseDate element Id.
     *
     *  @return the CloseDate element Id
     */

    public static org.osid.id.Id getCloseDate() {
        return (makeElementId("osid.course.term.CloseDate"));
    }


    /**
     *  Gets the RegistrationPeriod element Id.
     *
     *  @return the RegistrationPeriod element Id
     */

    public static org.osid.id.Id getRegistrationPeriod() {
        return (makeQueryElementId("osid.course.term.RegistrationPeriod"));
    }


    /**
     *  Gets the RegistrationDuration element Id.
     *
     *  @return the RegistrationDuration element Id
     */

    public static org.osid.id.Id getRegistrationDuration() {
        return (makeQueryElementId("osid.course.term.RegistrationDuration"));
    }


    /**
     *  Gets the ClassesPeriod element Id.
     *
     *  @return the ClassesPeriod element Id
     */

    public static org.osid.id.Id getClassesPeriod() {
        return (makeQueryElementId("osid.course.term.ClassesPeriod"));
    }


    /**
     *  Gets the ClassesDuration element Id.
     *
     *  @return the ClassesDuration element Id
     */

    public static org.osid.id.Id getClassesDuration() {
        return (makeQueryElementId("osid.course.term.ClassesDuration"));
    }


    /**
     *  Gets the FinalExamPeriod element Id.
     *
     *  @return the FinalExamPeriod element Id
     */

    public static org.osid.id.Id getFinalExamPeriod() {
        return (makeQueryElementId("osid.course.term.FinalExamPeriod"));
    }


    /**
     *  Gets the FinalExamDuration element Id.
     *
     *  @return the FinalExamDuration element Id
     */

    public static org.osid.id.Id getFinalExamDuration() {
        return (makeQueryElementId("osid.course.term.FinalExamDuration"));
    }


    /**
     *  Gets the AncestorTermId element Id.
     *
     *  @return the AncestorTermId element Id
     */

    public static org.osid.id.Id getAncestorTermId() {
        return (makeQueryElementId("osid.course.term.AncestorTermId"));
    }


    /**
     *  Gets the AncestorTerm element Id.
     *
     *  @return the AncestorTerm element Id
     */

    public static org.osid.id.Id getAncestorTerm() {
        return (makeQueryElementId("osid.course.term.AncestorTerm"));
    }


    /**
     *  Gets the DescendantTermId element Id.
     *
     *  @return the DescendantTermId element Id
     */

    public static org.osid.id.Id getDescendantTermId() {
        return (makeQueryElementId("osid.course.term.DescendantTermId"));
    }


    /**
     *  Gets the DescendantTerm element Id.
     *
     *  @return the DescendantTerm element Id
     */

    public static org.osid.id.Id getDescendantTerm() {
        return (makeQueryElementId("osid.course.term.DescendantTerm"));
    }


    /**
     *  Gets the CourseOfferingId element Id.
     *
     *  @return the CourseOfferingId element Id
     */

    public static org.osid.id.Id getCourseOfferingId() {
        return (makeQueryElementId("osid.course.term.CourseOfferingId"));
    }


    /**
     *  Gets the CourseOffering element Id.
     *
     *  @return the CourseOffering element Id
     */

    public static org.osid.id.Id getCourseOffering() {
        return (makeQueryElementId("osid.course.term.CourseOffering"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.term.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.term.CourseCatalog"));
    }
}
