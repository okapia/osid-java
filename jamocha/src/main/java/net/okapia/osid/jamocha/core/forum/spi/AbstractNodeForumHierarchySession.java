//
// AbstractNodeForumHierarchySession.java
//
//     Defines a Forum hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a forum hierarchy session for delivering a hierarchy
 *  of forums using the ForumNode interface.
 */

public abstract class AbstractNodeForumHierarchySession
    extends net.okapia.osid.jamocha.forum.spi.AbstractForumHierarchySession
    implements org.osid.forum.ForumHierarchySession {

    private java.util.Collection<org.osid.forum.ForumNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root forum <code> Ids </code> in this hierarchy.
     *
     *  @return the root forum <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootForumIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.forum.forumnode.ForumNodeToIdList(this.roots));
    }


    /**
     *  Gets the root forums in the forum hierarchy. A node
     *  with no parents is an orphan. While all forum <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root forums 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getRootForums()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.forum.forumnode.ForumNodeToForumList(new net.okapia.osid.jamocha.forum.forumnode.ArrayForumNodeList(this.roots)));
    }


    /**
     *  Adds a root forum node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootForum(org.osid.forum.ForumNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root forum nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootForums(java.util.Collection<org.osid.forum.ForumNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root forum node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootForum(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.forum.ForumNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Forum </code> has any parents. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @return <code> true </code> if the forum has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentForums(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getForumNode(forumId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  forum.
     *
     *  @param  id an <code> Id </code> 
     *  @param  forumId the <code> Id </code> of a forum 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> forumId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> forumId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfForum(org.osid.id.Id id, org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.forum.ForumNodeList parents = getForumNode(forumId).getParentForumNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextForumNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given forum. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @return the parent <code> Ids </code> of the forum 
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentForumIds(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.forum.forum.ForumToIdList(getParentForums(forumId)));
    }


    /**
     *  Gets the parents of the given forum. 
     *
     *  @param  forumId the <code> Id </code> to query 
     *  @return the parents of the forum 
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getParentForums(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.forum.forumnode.ForumNodeToForumList(getForumNode(forumId).getParentForumNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  forum.
     *
     *  @param  id an <code> Id </code> 
     *  @param  forumId the Id of a forum 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> forumId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfForum(org.osid.id.Id id, org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfForum(id, forumId)) {
            return (true);
        }

        try (org.osid.forum.ForumList parents = getParentForums(forumId)) {
            while (parents.hasNext()) {
                if (isAncestorOfForum(id, parents.getNextForum().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a forum has any children. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @return <code> true </code> if the <code> forumId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildForums(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getForumNode(forumId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  forum.
     *
     *  @param  id an <code> Id </code> 
     *  @param forumId the <code> Id </code> of a 
     *         forum
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> forumId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> forumId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfForum(org.osid.id.Id id, org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfForum(forumId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  forum.
     *
     *  @param  forumId the <code> Id </code> to query 
     *  @return the children of the forum 
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildForumIds(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.forum.forum.ForumToIdList(getChildForums(forumId)));
    }


    /**
     *  Gets the children of the given forum. 
     *
     *  @param  forumId the <code> Id </code> to query 
     *  @return the children of the forum 
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getChildForums(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.forum.forumnode.ForumNodeToForumList(getForumNode(forumId).getChildForumNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  forum.
     *
     *  @param  id an <code> Id </code> 
     *  @param forumId the <code> Id </code> of a 
     *         forum
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> forumId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfForum(org.osid.id.Id id, org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfForum(forumId, id)) {
            return (true);
        }

        try (org.osid.forum.ForumList children = getChildForums(forumId)) {
            while (children.hasNext()) {
                if (isDescendantOfForum(id, children.getNextForum().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  forum.
     *
     *  @param  forumId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified forum node 
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getForumNodeIds(org.osid.id.Id forumId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.forum.forumnode.ForumNodeToNode(getForumNode(forumId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given forum.
     *
     *  @param  forumId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified forum node 
     *  @throws org.osid.NotFoundException <code> forumId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> forumId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumNode getForumNodes(org.osid.id.Id forumId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getForumNode(forumId));
    }


    /**
     *  Closes this <code>ForumHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a forum node.
     *
     *  @param forumId the id of the forum node
     *  @throws org.osid.NotFoundException <code>forumId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>forumId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.forum.ForumNode getForumNode(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(forumId, "forum Id");
        for (org.osid.forum.ForumNode forum : this.roots) {
            if (forum.getId().equals(forumId)) {
                return (forum);
            }

            org.osid.forum.ForumNode r = findForum(forum, forumId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(forumId + " is not found");
    }


    protected org.osid.forum.ForumNode findForum(org.osid.forum.ForumNode node, 
                                                 org.osid.id.Id forumId) 
	throws org.osid.OperationFailedException {

        try (org.osid.forum.ForumNodeList children = node.getChildForumNodes()) {
            while (children.hasNext()) {
                org.osid.forum.ForumNode forum = children.getNextForumNode();
                if (forum.getId().equals(forumId)) {
                    return (forum);
                }
                
                forum = findForum(forum, forumId);
                if (forum != null) {
                    return (forum);
                }
            }
        }

        return (null);
    }
}
