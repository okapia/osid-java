//
// AbstractOsidCompendiumQueryInspector.java
//
//     Defines a generic OsidCompendiumQiueryInspector.
//
//
// Tom Coppeto
// Okapia
// 5 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a generic <code>OsidCompendiumQueryInspector</code>.
 */

public abstract class AbstractOsidCompendiumQueryInspector
    extends AbstractSourceableOsidObjectQueryInspector
    implements org.osid.OsidCompendiumQueryInspector {


    /**
     *  Gets the start date query terms.
     *
     *  @return the start date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the end date query terms.
     *
     *  @return the end date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }

    
    /**
     *  Gets the interpolated query terms. 
     *
     *  @return the query terms 
     */
    
    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getInterpolatedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the extrapolated query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getExtrapolatedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }
}
