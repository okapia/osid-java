//
// AbstractFederatingAwardLookupSession.java
//
//     An abstract federating adapter for an AwardLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AwardLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAwardLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.recognition.AwardLookupSession>
    implements org.osid.recognition.AwardLookupSession {

    private boolean parallel = false;
    private org.osid.recognition.Academy academy = new net.okapia.osid.jamocha.nil.recognition.academy.UnknownAcademy();


    /**
     *  Constructs a new <code>AbstractFederatingAwardLookupSession</code>.
     */

    protected AbstractFederatingAwardLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.recognition.AwardLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Academy/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.academy.getId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this 
     *  session.
     *
     *  @return the <code>Academy</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.academy);
    }


    /**
     *  Sets the <code>Academy</code>.
     *
     *  @param  academy the academy for this session
     *  @throws org.osid.NullArgumentException <code>academy</code>
     *          is <code>null</code>
     */

    protected void setAcademy(org.osid.recognition.Academy academy) {
        nullarg(academy, "academy");
        this.academy = academy;
        return;
    }


    /**
     *  Tests if this user can perform <code>Award</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAwards() {
        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            if (session.canLookupAwards()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Award</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAwardView() {
        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            session.useComparativeAwardView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Award</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAwardView() {
        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            session.usePlenaryAwardView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include awards in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            session.useFederatedAcademyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            session.useIsolatedAcademyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Award</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Award</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Award</code> and
     *  retained for compatibility.
     *
     *  @param  awardId <code>Id</code> of the
     *          <code>Award</code>
     *  @return the award
     *  @throws org.osid.NotFoundException <code>awardId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward(org.osid.id.Id awardId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            try {
                return (session.getAward(awardId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(awardId + " not found");
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  awards specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Awards</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  awardIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>awardIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByIds(org.osid.id.IdList awardIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.recognition.award.MutableAwardList ret = new net.okapia.osid.jamocha.recognition.award.MutableAwardList();

        try (org.osid.id.IdList ids = awardIds) {
            while (ids.hasNext()) {
                ret.addAward(getAward(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  award genus <code>Type</code> which does not include
     *  awards of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.award.FederatingAwardList ret = getAwardList();

        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            ret.addAwardList(session.getAwardsByGenusType(awardGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  award genus <code>Type</code> and include any additional
     *  awards with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByParentGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.award.FederatingAwardList ret = getAwardList();

        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            ret.addAwardList(session.getAwardsByParentGenusType(awardGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AwardList</code> containing the given
     *  award record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardRecordType an award record type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByRecordType(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.award.FederatingAwardList ret = getAwardList();

        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            ret.addAwardList(session.getAwardsByRecordType(awardRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Awards</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Awards</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwards()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.award.FederatingAwardList ret = getAwardList();

        for (org.osid.recognition.AwardLookupSession session : getSessions()) {
            ret.addAwardList(session.getAwards());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.recognition.award.FederatingAwardList getAwardList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.award.ParallelAwardList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.award.CompositeAwardList());
        }
    }
}
