//
// AbstractSpatialUnitMetadata.java
//
//     Defines a spatial unit Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a spatial unit Metadata.
 */

public abstract class AbstractSpatialUnitMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final Types recordTypes = new TypeSet();

    private final java.util.Collection<org.osid.mapping.SpatialUnit> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.SpatialUnit> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.SpatialUnit> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractSpatialUnitMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractSpatialUnitMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.SPATIALUNIT, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractSpatialUnitMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractSpatialUnitMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.SPATIALUNIT, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the set of acceptable spatial unit record types. 
     *
     *  @return the set of spatial unit record types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          SPATIALUNIT or SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getSpatialUnitRecordTypes() {
        return (this.recordTypes.toArray());
    }


    /**
     *  Tests if the given spatial unit record type is supported. 
     *
     *  @param  spatialUnitRecordType a spatial unit record Type 
     *  @return <code> true </code> if the record type is supported,
     *          <code> false </code> otherwise
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          SPATIALUNIT </code> 
     *  @throws org.osid.NullArgumentException <code>
     *          spatialUnitRecordType </code> is <code> null </code>
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (this.recordTypes.contains(spatialUnitRecordType));
    }


    /**
     *  Add support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType the type of spatial unit record
     *  @throws org.osid.NullArgumentException {@code
     *          spatialUnitRecordType} is {@code null}
     */

    protected void addSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        this.recordTypes.add(spatialUnitRecordType);
        return;
    }


    /**
     *  Gets the set of acceptable spatial unit values. 
     *
     *  @return a set of spatial units or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPATIALUNIT </code>
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit[] getSpatialUnitSet() {
        return (this.set.toArray(new org.osid.mapping.SpatialUnit[this.set.size()]));
    }

    
    /**
     *  Sets the spatial unit set.
     *
     *  @param values a collection of accepted spatial unit values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setSpatialUnitSet(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        this.set.clear();
        addToSpatialUnitSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the spatial unit set.
     *
     *  @param values a collection of accepted spatial unit values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToSpatialUnitSet(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        nullarg(values, "spatial unit set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the spatial unit set.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToSpatialUnitSet(org.osid.mapping.SpatialUnit value) {
        nullarg(value, "spatial unit value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the spatial unit set.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromSpatialUnitSet(org.osid.mapping.SpatialUnit value) {
        nullarg(value, "spatial unit value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the spatial unit set.
     */

    protected void clearSpatialUnitSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default spatial unit values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default spatial unit values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPATIAL UNIT </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit[] getDefaultSpatialUnitValues() {
        return (this.defvals.toArray(new org.osid.mapping.SpatialUnit[this.defvals.size()]));
    }


    /**
     *  Sets the default spatial unit set.
     *
     *  @param values a collection of default spatial unit values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        clearDefaultSpatialUnitValues();
        addDefaultSpatialUnitValues(values);
        return;
    }


    /**
     *  Adds a collection of default spatial unit values.
     *
     *  @param values a collection of default spatial unit values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        nullarg(values, "default spatial unit values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        nullarg(value, "default spatial unit value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        nullarg(value, "default spatial unit value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default spatial unit values.
     */

    protected void clearDefaultSpatialUnitValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing spatial unit values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing spatial unit values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          SPATIAL UNIT </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit[] getExistingSpatialUnitValues() {
        return (this.existing.toArray(new org.osid.mapping.SpatialUnit[this.existing.size()]));
    }


    /**
     *  Sets the existing spatial unit set.
     *
     *  @param values a collection of existing spatial unit values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        clearExistingSpatialUnitValues();
        addExistingSpatialUnitValues(values);
        return;
    }


    /**
     *  Adds a collection of existing spatial unit values.
     *
     *  @param values a collection of existing spatial unit values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        nullarg(values, "existing spatial unit values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        nullarg(value, "existing spatial unit value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        nullarg(value, "existing spatial unit value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing spatial unit values.
     */

    protected void clearExistingSpatialUnitValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}