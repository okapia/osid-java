//
// AbstractFederatingBudgetLookupSession.java
//
//     An abstract federating adapter for a BudgetLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BudgetLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBudgetLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.financials.budgeting.BudgetLookupSession>
    implements org.osid.financials.budgeting.BudgetLookupSession {

    private boolean parallel = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingBudgetLookupSession</code>.
     */

    protected AbstractFederatingBudgetLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.financials.budgeting.BudgetLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Budget</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBudgets() {
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            if (session.canLookupBudgets()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Budget</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBudgetView() {
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            session.useComparativeBudgetView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Budget</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBudgetView() {
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            session.usePlenaryBudgetView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include budgets in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }


    /**
     *  Only budgets whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveBudgetView() {
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            session.useEffectiveBudgetView();
        }

        return;
    }


    /**
     *  All budgets of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveBudgetView() {
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            session.useAnyEffectiveBudgetView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Budget</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Budget</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Budget</code> and
     *  retained for compatibility.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param  budgetId <code>Id</code> of the
     *          <code>Budget</code>
     *  @return the budget
     *  @throws org.osid.NotFoundException <code>budgetId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>budgetId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.Budget getBudget(org.osid.id.Id budgetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            try {
                return (session.getBudget(budgetId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(budgetId + " not found");
    }


    /**
     *  Gets a <code>BudgetList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  budgets specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Budgets</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  budgetIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>budgetIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByIds(org.osid.id.IdList budgetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.financials.budgeting.budget.MutableBudgetList ret = new net.okapia.osid.jamocha.financials.budgeting.budget.MutableBudgetList();

        try (org.osid.id.IdList ids = budgetIds) {
            while (ids.hasNext()) {
                ret.addBudget(getBudget(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetList</code> corresponding to the given
     *  budget genus <code>Type</code> which does not include
     *  budgets of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  budgetGenusType a budget genus type 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByGenusType(org.osid.type.Type budgetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();

        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsByGenusType(budgetGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetList</code> corresponding to the given
     *  budget genus <code>Type</code> and include any additional
     *  budgets with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param  budgetGenusType a budget genus type 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByParentGenusType(org.osid.type.Type budgetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();

        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsByParentGenusType(budgetGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetList</code> containing the given
     *  budget record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param  budgetRecordType a budget record type 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByRecordType(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();

        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsByRecordType(budgetRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible
     *  through this session.
     *  
     *  In active mode, budgets are returned that are currently
     *  active. In any status mode, active and inactive budgets
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsOnDate(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();

        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>BudgetList</code> for the given activity.
     *
     *  In plenary mode, the returned list contains all known mode,s or an
     *  error results. Otherwise, the returned list may contain only those
     *  budgets that are accessible through this session.
     *
     *  In effective mode, budgets are returned that are currently effective.
     *  In any effective mode, effective budgets and those currently expired
     *  are returned.
     *
     *  @param  activityId an activity <code>Id /code>
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();

        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsForActivity(activityId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetList</code> for the given activity and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityId</code>, <code>from</code> or
     *          <code>to</code> </code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityOnDate(org.osid.id.Id activityId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();
        
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsForActivityOnDate(activityId, from, to));
        }
        
        ret.noMore();
        return (ret);
    }

 
    /**
     *  Gets a <code>BudgetList</code> for the given fiscal period.
     *
     *  In plenary mode, the returned list contains all known mode,s
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  fiscalPeriodId a fiscal period <code>Id</code>
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();
        
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsForFiscalPeriod(fiscalPeriodId));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetList</code> for the given fiscal period and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  fiscalPeriodId a fiscal period <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodId</code>, <code>from</code> or to
     *          </code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForFiscalPeriodOnDate(org.osid.id.Id fiscalPeriodId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();
        
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsForFiscalPeriodOnDate(fiscalPeriodId, from, to));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetList</code> for the given activity and
     *  fiscal period.
     *
     *  In plenary mode, the returned list contains all known mode,s
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity <code>Id</code>
     *  @param  fiscalPeriodId a fiscal period <code>Id</code>
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          or <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityAndFiscalPeriod(org.osid.id.Id activityId,
                                                                                         org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();
        
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsForActivityAndFiscalPeriod(activityId, fiscalPeriodId));
        }
        
        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>BudgetList</code> for the given activity, fiscal
     *  period, and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity <code>Id</code>
     *  @param  fiscalPeriodId a fiscal period <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityId</code>, <code>fiscalPeriodId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityAndFiscalPeriodOnDate(org.osid.id.Id activityId,
                                                                                               org.osid.id.Id fiscalPeriodId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();
        
        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgetsForActivityAndFiscalPeriodOnDate(activityId, fiscalPeriodId, from, to));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Budgets</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Budgets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList ret = getBudgetList();

        for (org.osid.financials.budgeting.BudgetLookupSession session : getSessions()) {
            ret.addBudgetList(session.getBudgets());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.FederatingBudgetList getBudgetList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.ParallelBudgetList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budget.CompositeBudgetList());
        }
    }
}
