//
// MutableIndexedMapParameterLookupSession
//
//    Implements a Parameter lookup service backed by a collection of
//    parameters indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration;


/**
 *  Implements a Parameter lookup service backed by a collection of
 *  parameters. The parameters are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some parameters may be compatible
 *  with more types than are indicated through these parameter
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of parameters can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapParameterLookupSession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractIndexedMapParameterLookupSession
    implements org.osid.configuration.ParameterLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapParameterLookupSession} with no parameters.
     *
     *  @param configuration the configuration
     *  @throws org.osid.NullArgumentException {@code configuration}
     *          is {@code null}
     */

      public MutableIndexedMapParameterLookupSession(org.osid.configuration.Configuration configuration) {
        setConfiguration(configuration);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParameterLookupSession} with a
     *  single parameter.
     *  
     *  @param configuration the configuration
     *  @param  parameter a single parameter
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameter} is {@code null}
     */

    public MutableIndexedMapParameterLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.Parameter parameter) {
        this(configuration);
        putParameter(parameter);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParameterLookupSession} using an
     *  array of parameters.
     *
     *  @param configuration the configuration
     *  @param  parameters an array of parameters
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameters} is {@code null}
     */

    public MutableIndexedMapParameterLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.Parameter[] parameters) {
        this(configuration);
        putParameters(parameters);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParameterLookupSession} using a
     *  collection of parameters.
     *
     *  @param configuration the configuration
     *  @param  parameters a collection of parameters
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameters} is {@code null}
     */

    public MutableIndexedMapParameterLookupSession(org.osid.configuration.Configuration configuration,
                                                  java.util.Collection<? extends org.osid.configuration.Parameter> parameters) {

        this(configuration);
        putParameters(parameters);
        return;
    }
    

    /**
     *  Makes a {@code Parameter} available in this session.
     *
     *  @param  parameter a parameter
     *  @throws org.osid.NullArgumentException {@code parameter{@code  is
     *          {@code null}
     */

    @Override
    public void putParameter(org.osid.configuration.Parameter parameter) {
        super.putParameter(parameter);
        return;
    }


    /**
     *  Makes an array of parameters available in this session.
     *
     *  @param  parameters an array of parameters
     *  @throws org.osid.NullArgumentException {@code parameters{@code 
     *          is {@code null}
     */

    @Override
    public void putParameters(org.osid.configuration.Parameter[] parameters) {
        super.putParameters(parameters);
        return;
    }


    /**
     *  Makes collection of parameters available in this session.
     *
     *  @param  parameters a collection of parameters
     *  @throws org.osid.NullArgumentException {@code parameter{@code  is
     *          {@code null}
     */

    @Override
    public void putParameters(java.util.Collection<? extends org.osid.configuration.Parameter> parameters) {
        super.putParameters(parameters);
        return;
    }


    /**
     *  Removes a Parameter from this session.
     *
     *  @param parameterId the {@code Id} of the parameter
     *  @throws org.osid.NullArgumentException {@code parameterId{@code  is
     *          {@code null}
     */

    @Override
    public void removeParameter(org.osid.id.Id parameterId) {
        super.removeParameter(parameterId);
        return;
    }    
}
