//
// AbstractAddressBookQueryInspector.java
//
//     A template for making an AddressBookQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.addressbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for address books.
 */

public abstract class AbstractAddressBookQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.contact.AddressBookQueryInspector {

    private final java.util.Collection<org.osid.contact.records.AddressBookQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the contact <code> Id </code> terms. 
     *
     *  @return the contact <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContactIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the contact terms. 
     *
     *  @return the contact terms 
     */

    @OSID @Override
    public org.osid.contact.ContactQueryInspector[] getContactTerms() {
        return (new org.osid.contact.ContactQueryInspector[0]);
    }


    /**
     *  Gets the address <code> Id </code> terms. 
     *
     *  @return the address <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the address terms. 
     *
     *  @return the address terms 
     */

    @OSID @Override
    public org.osid.contact.AddressQueryInspector[] getAddressTerms() {
        return (new org.osid.contact.AddressQueryInspector[0]);
    }


    /**
     *  Gets the ancestor address book <code> Id </code> terms. 
     *
     *  @return the ancestor address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAddressBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor address book terms. 
     *
     *  @return the ancestor address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAncestorAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }


    /**
     *  Gets the descendant address book <code> Id </code> terms. 
     *
     *  @return the descendant address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAddressBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant address book terms. 
     *
     *  @return the descendant address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getDescendantAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given address book query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an address book implementing the requested record.
     *
     *  @param addressBookRecordType an address book record type
     *  @return the address book query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressBookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressBookQueryInspectorRecord getAddressBookQueryInspectorRecord(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressBookQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(addressBookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressBookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this address book query. 
     *
     *  @param addressBookQueryInspectorRecord address book query inspector
     *         record
     *  @param addressBookRecordType addressBook record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAddressBookQueryInspectorRecord(org.osid.contact.records.AddressBookQueryInspectorRecord addressBookQueryInspectorRecord, 
                                                   org.osid.type.Type addressBookRecordType) {

        addRecordType(addressBookRecordType);
        nullarg(addressBookRecordType, "address book record type");
        this.records.add(addressBookQueryInspectorRecord);        
        return;
    }
}
