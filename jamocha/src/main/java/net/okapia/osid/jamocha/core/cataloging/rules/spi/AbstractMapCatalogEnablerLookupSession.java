//
// AbstractMapCatalogEnablerLookupSession
//
//    A simple framework for providing a CatalogEnabler lookup service
//    backed by a fixed collection of catalog enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CatalogEnabler lookup service backed by a
 *  fixed collection of catalog enablers. The catalog enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CatalogEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCatalogEnablerLookupSession
    extends net.okapia.osid.jamocha.cataloging.rules.spi.AbstractCatalogEnablerLookupSession
    implements org.osid.cataloging.rules.CatalogEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.cataloging.rules.CatalogEnabler> catalogEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.cataloging.rules.CatalogEnabler>());


    /**
     *  Makes a <code>CatalogEnabler</code> available in this session.
     *
     *  @param  catalogEnabler a catalog enabler
     *  @throws org.osid.NullArgumentException <code>catalogEnabler<code>
     *          is <code>null</code>
     */

    protected void putCatalogEnabler(org.osid.cataloging.rules.CatalogEnabler catalogEnabler) {
        this.catalogEnablers.put(catalogEnabler.getId(), catalogEnabler);
        return;
    }


    /**
     *  Makes an array of catalog enablers available in this session.
     *
     *  @param  catalogEnablers an array of catalog enablers
     *  @throws org.osid.NullArgumentException <code>catalogEnablers<code>
     *          is <code>null</code>
     */

    protected void putCatalogEnablers(org.osid.cataloging.rules.CatalogEnabler[] catalogEnablers) {
        putCatalogEnablers(java.util.Arrays.asList(catalogEnablers));
        return;
    }


    /**
     *  Makes a collection of catalog enablers available in this session.
     *
     *  @param  catalogEnablers a collection of catalog enablers
     *  @throws org.osid.NullArgumentException <code>catalogEnablers<code>
     *          is <code>null</code>
     */

    protected void putCatalogEnablers(java.util.Collection<? extends org.osid.cataloging.rules.CatalogEnabler> catalogEnablers) {
        for (org.osid.cataloging.rules.CatalogEnabler catalogEnabler : catalogEnablers) {
            this.catalogEnablers.put(catalogEnabler.getId(), catalogEnabler);
        }

        return;
    }


    /**
     *  Removes a CatalogEnabler from this session.
     *
     *  @param  catalogEnablerId the <code>Id</code> of the catalog enabler
     *  @throws org.osid.NullArgumentException <code>catalogEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeCatalogEnabler(org.osid.id.Id catalogEnablerId) {
        this.catalogEnablers.remove(catalogEnablerId);
        return;
    }


    /**
     *  Gets the <code>CatalogEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  catalogEnablerId <code>Id</code> of the <code>CatalogEnabler</code>
     *  @return the catalogEnabler
     *  @throws org.osid.NotFoundException <code>catalogEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>catalogEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnabler getCatalogEnabler(org.osid.id.Id catalogEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.cataloging.rules.CatalogEnabler catalogEnabler = this.catalogEnablers.get(catalogEnablerId);
        if (catalogEnabler == null) {
            throw new org.osid.NotFoundException("catalogEnabler not found: " + catalogEnablerId);
        }

        return (catalogEnabler);
    }


    /**
     *  Gets all <code>CatalogEnablers</code>. In plenary mode, the returned
     *  list contains all known catalogEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  catalogEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CatalogEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.cataloging.rules.catalogenabler.ArrayCatalogEnablerList(this.catalogEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.catalogEnablers.clear();
        super.close();
        return;
    }
}
