//
// AbstractBidNotificationSession.java
//
//     A template for making BidNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Bid} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Bid} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for bid entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractBidNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.BidNotificationSession {

    private boolean federated = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();


    /**
     *  Gets the {@code AuctionHouse/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }

    
    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the {@code AuctionHouse}.
     *
     *  @param auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException {@code auctionHouse}
     *          is {@code null}
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can register for {@code Bid} notifications.
     *  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForBidNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeBidNotification() </code>.
     */

    @OSID @Override
    public void reliableBidNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableBidNotifications() {
        return;
    }


    /**
     *  Acknowledge a bid notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeBidNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for bids in auction houses
     *  which are children of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new bids. {@code
     *  BidReceiver.newBid()} is invoked when a new {@code Bid} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new bids for the given auction
     *  {@code Id}. {@code BidReceiver.newBid()} is invoked when a new
     *  {@code Bid} is created.
     *
     *  @param  auctionId the {@code Id} of the auction to monitor
     *  @throws org.osid.NullArgumentException {@code auctionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new bids for the given bidder
     *  {@code Id}. {@code BidReceiver.newBid()} is invoked when a new
     *  {@code Bid} is created.
     *
     *  @param  resourceId the {@code Id} of the bidder to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated bids. {@code
     *  BidReceiver.changedBid()} is invoked when a bid is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated bids for the given
     *  auction {@code Id}. {@code BidReceiver.changedBid()} is
     *  invoked when a {@code Bid} in this auctionHouse is changed.
     *
     *  @param  auctionId the {@code Id} of the auction to monitor
     *  @throws org.osid.NullArgumentException {@code auctionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated bids for the given
     *  bidder {@code Id}. {@code BidReceiver.changedBid()} is invoked
     *  when a {@code Bid} in this auctionHouse is changed.
     *
     *  @param  resourceId the {@code Id} of the bidder to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated bid. {@code
     *  BidReceiver.changedBid()} is invoked when the specified bid is
     *  changed.
     *
     *  @param bidId the {@code Id} of the {@code Bid} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code bidId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedBid(org.osid.id.Id bidId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted bids. {@code
     *  BidReceiver.deletedBid()} is invoked when a bid is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted bids for the given
     *  auction {@code Id}. {@code BidReceiver.deletedBid()} is
     *  invoked when a {@code Bid} is deleted or removed from this
     *  auctionHouse.
     *
     *  @param  auctionId the {@code Id} of the auction to monitor
     *  @throws org.osid.NullArgumentException {@code auctionId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted bids for the given
     *  bidder {@code Id}. {@code BidReceiver.deletedBid()} is invoked
     *  when a {@code Bid} is deleted or removed from this
     *  auctionHouse.
     *
     *  @param  resourceId the {@code Id} of the bidder to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted bid. {@code
     *  BidReceiver.deletedBid()} is invoked when the specified bid is
     *  deleted.
     *
     *  @param bidId the {@code Id} of the
     *          {@code Bid} to monitor
     *  @throws org.osid.NullArgumentException {@code bidId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedBid(org.osid.id.Id bidId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
