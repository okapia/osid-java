//
// AbstractUnknownFile.java
//
//     Defines an unknown File.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.filing.file.spi;


/**
 *  Defines an unknown <code>File</code>.
 */

public abstract class AbstractUnknownFile
    extends net.okapia.osid.jamocha.filing.file.spi.AbstractFile
    implements org.osid.filing.File {

    protected static final String OBJECT = "osid.filing.File";


    /**
     *  Constructs a new <code>AbstractUnknownFile</code>.
     */

    public AbstractUnknownFile() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));
        
        setName("unknown");
        setPath("/dev/null");
        setRealPath("/dev/null");
        setOwner(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
        setCreatedTime(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setLastModifiedTime(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setLastAccessTime(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownFile</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownFile(boolean optional) {
        this();
        setSize(0);
        return;
    }
}
