//
// AbstractParameterProcessorLookupSession.java
//
//    A starter implementation framework for providing a ParameterProcessor
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ParameterProcessor
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getParameterProcessors(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession,
               net.okapia.osid.jamocha.inline.filter.configuration.rules.parameterprocessor.ParameterProcessorFilter {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();
    

    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }

    /**
     *  Tests if this user can perform <code>ParameterProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParameterProcessors() {
        return (true);
    }


    /**
     *  A complete view of the <code>ParameterProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParameterProcessorView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ParameterProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParameterProcessorView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameter processors in configurations which
     *  are children of this configuration in the configuration
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active parameter processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive parameter processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ParameterProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ParameterProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ParameterProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorId <code>Id</code> of the
     *          <code>ParameterProcessor</code>
     *  @return the parameter processor
     *  @throws org.osid.NotFoundException <code>parameterProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>parameterProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessor getParameterProcessor(org.osid.id.Id parameterProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.configuration.rules.ParameterProcessorList parameterProcessors = getParameterProcessors()) {
            while (parameterProcessors.hasNext()) {
                org.osid.configuration.rules.ParameterProcessor parameterProcessor = parameterProcessors.getNextParameterProcessor();
                if (parameterProcessor.getId().equals(parameterProcessorId)) {
                    return (parameterProcessor);
                }
            }
        } 

        throw new org.osid.NotFoundException(parameterProcessorId + " not found");
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameterProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ParameterProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getParameterProcessors()</code>.
     *
     *  @param  parameterProcessorIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByIds(org.osid.id.IdList parameterProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.rules.ParameterProcessor> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = parameterProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getParameterProcessor(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("parameter processor " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessor.LinkedParameterProcessorList(ret));
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  parameter processor genus <code>Type</code> which does not include
     *  parameter processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getParameterProcessors()</code>.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.rules.ParameterProcessor> ret = new java.util.ArrayList<>();

        try (org.osid.configuration.rules.ParameterProcessorList parameterProcessors = getParameterProcessors()) {
            while (parameterProcessors.hasNext()) {
                org.osid.configuration.rules.ParameterProcessor parameterProcessor = parameterProcessors.getNextParameterProcessor();
                if (parameterProcessor.isOfGenusType(parameterProcessorGenusType)) {
                    ret.add(parameterProcessor);
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessor.LinkedParameterProcessorList(ret));
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  parameter processor genus <code>Type</code> and include any additional
     *  parameter processors with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParameterProcessors()</code>.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByParentGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getParameterProcessorsByGenusType(parameterProcessorGenusType));
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> containing the given
     *  parameter processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParameterProcessors()</code>.
     *
     *  @param  parameterProcessorRecordType a parameterProcessor record type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByRecordType(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.rules.ParameterProcessor> ret = new java.util.ArrayList<>();

        try (org.osid.configuration.rules.ParameterProcessorList parameterProcessors = getParameterProcessors()) {
            while (parameterProcessors.hasNext()) {
                org.osid.configuration.rules.ParameterProcessor parameterProcessor = parameterProcessors.getNextParameterProcessor();
                if (parameterProcessor.hasRecordType(parameterProcessorRecordType)) {
                    ret.add(parameterProcessor);
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessor.LinkedParameterProcessorList(ret));
    }


    /**
     *  Gets all <code>ParameterProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @return a list of <code>ParameterProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.configuration.rules.ParameterProcessorList getParameterProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the parameter processor list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of parameter processors
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.configuration.rules.ParameterProcessorList filterParameterProcessors(org.osid.configuration.rules.ParameterProcessorList list)
        throws org.osid.OperationFailedException {
            
        return (new net.okapia.osid.jamocha.inline.filter.configuration.rules.parameterprocessor.ParameterProcessorFilterList(this, list));
    }

       
    /**
     *  Used by the ParameterProcessorFilterList to filter the parameter processor list
     *  based on views.
     *
     *  @param parameterProcessor the parameter processor
     *  @return <code>true</code> to pass the parameter processor,
     *          <code>false</code> to filer it
     */

    @Override
    public boolean pass(org.osid.configuration.rules.ParameterProcessor parameterProcessor) {
        if (isActiveOnly()) {
            if (!parameterProcessor.isActive()) {
                return (false);
            }
        }
          
        return (true);     
    }
}
