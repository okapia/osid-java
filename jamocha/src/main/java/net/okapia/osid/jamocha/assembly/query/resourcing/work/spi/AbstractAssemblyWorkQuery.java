//
// AbstractAssemblyWorkQuery.java
//
//     A WorkQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.work.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A WorkQuery that stores terms.
 */

public abstract class AbstractAssemblyWorkQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.resourcing.WorkQuery,
               org.osid.resourcing.WorkQueryInspector,
               org.osid.resourcing.WorkSearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.WorkQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.WorkQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.WorkSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyWorkQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyWorkQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the job <code> Id </code> for this query. 
     *
     *  @param  jobId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchJobId(org.osid.id.Id jobId, boolean match) {
        getAssembler().addIdTerm(getJobIdColumn(), jobId, match);
        return;
    }


    /**
     *  Clears the job <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearJobIdTerms() {
        getAssembler().clearTerms(getJobIdColumn());
        return;
    }


    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJobIdTerms() {
        return (getAssembler().getIdTerms(getJobIdColumn()));
    }


    /**
     *  Orders the results by the job. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByJob(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getJobColumn(), style);
        return;
    }


    /**
     *  Gets the JobId column name.
     *
     * @return the column name
     */

    protected String getJobIdColumn() {
        return ("job_id");
    }


    /**
     *  Tests if a <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getJobQuery() {
        throw new org.osid.UnimplementedException("supportsJobQuery() is false");
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobTerms() {
        getAssembler().clearTerms(getJobColumn());
        return;
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Tests if a job search order is available. 
     *
     *  @return <code> true </code> if a job search order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobSearchOrder() {
        return (false);
    }


    /**
     *  Gets the job search order. 
     *
     *  @return the job search order 
     *  @throws org.osid.IllegalStateException <code> supportsJobSearchOrder() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchOrder getJobSearchOrder() {
        throw new org.osid.UnimplementedException("supportsJobSearchOrder() is false");
    }


    /**
     *  Gets the Job column name.
     *
     * @return the column name
     */

    protected String getJobColumn() {
        return ("job");
    }


    /**
     *  Sets the competency <code> Id </code> for this query. 
     *
     *  @param  competencyId the competency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> competencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompetencyId(org.osid.id.Id competencyId, boolean match) {
        getAssembler().addIdTerm(getCompetencyIdColumn(), competencyId, match);
        return;
    }


    /**
     *  Clears the competency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCompetencyIdTerms() {
        getAssembler().clearTerms(getCompetencyIdColumn());
        return;
    }


    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (getAssembler().getIdTerms(getCompetencyIdColumn()));
    }


    /**
     *  Gets the CompetencyId column name.
     *
     * @return the column name
     */

    protected String getCompetencyIdColumn() {
        return ("competency_id");
    }


    /**
     *  Tests if a <code> CompetencyQuery </code> is available. 
     *
     *  @return <code> true </code> if a competency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a competency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the competency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportscompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuery getCompetencyQuery() {
        throw new org.osid.UnimplementedException("supportsCompetencyQuery() is false");
    }


    /**
     *  Matches work that have any competency. 
     *
     *  @param  match <code> true </code> to match work with any competency, 
     *          <code> false </code> to match work with no competency 
     */

    @OSID @Override
    public void matchAnyCompetency(boolean match) {
        getAssembler().addIdWildcardTerm(getCompetencyColumn(), match);
        return;
    }


    /**
     *  Clears the competency query terms. 
     */

    @OSID @Override
    public void clearCompetencyTerms() {
        getAssembler().clearTerms(getCompetencyColumn());
        return;
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Gets the Competency column name.
     *
     * @return the column name
     */

    protected String getCompetencyColumn() {
        return ("competency");
    }


    /**
     *  Matches the creation date between the given date range inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedDate(org.osid.calendaring.DateTime start, 
                                 org.osid.calendaring.DateTime end, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getCreatedDateColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the creation date query terms. 
     */

    @OSID @Override
    public void clearCreatedDateTerms() {
        getAssembler().clearTerms(getCreatedDateColumn());
        return;
    }


    /**
     *  Gets the created date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCreatedDateColumn()));
    }


    /**
     *  Orders the results by the created date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByCreatedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreatedDateColumn(), style);
        return;
    }


    /**
     *  Gets the CreatedDate column name.
     *
     *  @return the column name
     */

    protected String getCreatedDateColumn() {
        return ("created_date");
    }


    /**
     *  Matches the completion date between the given date range inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCompletionDate(org.osid.calendaring.DateTime start, 
                                    org.osid.calendaring.DateTime end, 
                                    boolean match) {
        getAssembler().addDateTimeRangeTerm(getCompletionDateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches work that have any completion date. 
     *
     *  @param  match <code> true </code> to match completed work, <code> 
     *          false </code> to match incomplete work 
     */

    @OSID @Override
    public void matchAnyCompletionDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getCompletionDateColumn(), match);
        return;
    }


    /**
     *  Clears the completion date query terms. 
     */

    @OSID @Override
    public void clearCompletionDateTerms() {
        getAssembler().clearTerms(getCompletionDateColumn());
        return;
    }


    /**
     *  Gets the completion date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCompletionDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCompletionDateColumn()));
    }


    /**
     *  Orders the results by the completion date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByCompletionDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompletionDateColumn(), style);
        return;
    }


    /**
     *  Gets the CompletionDate column name.
     *
     * @return the column name
     */

    protected String getCompletionDateColumn() {
        return ("completion_date");
    }


    /**
     *  Sets the commission <code> Id </code> for this query to match 
     *  commissions. 
     *
     *  @param  commissionId the availability <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commissionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommissionId(org.osid.id.Id commissionId, boolean match) {
        getAssembler().addIdTerm(getCommissionIdColumn(), commissionId, match);
        return;
    }


    /**
     *  Clears the commission <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCommissionIdTerms() {
        getAssembler().clearTerms(getCommissionIdColumn());
        return;
    }


    /**
     *  Gets the commission <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommissionIdTerms() {
        return (getAssembler().getIdTerms(getCommissionIdColumn()));
    }


    /**
     *  Gets the CommissionId column name.
     *
     * @return the column name
     */

    protected String getCommissionIdColumn() {
        return ("commission_id");
    }


    /**
     *  Tests if a <code> CommissionQuery </code> is available. 
     *
     *  @return <code> true </code> if a commission query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commission. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commission query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuery getCommissionQuery() {
        throw new org.osid.UnimplementedException("supportsCommissionQuery() is false");
    }


    /**
     *  Matches work that have any commission. 
     *
     *  @param  match <code> true </code> to match work with any commission, 
     *          <code> false </code> to match work with no availability 
     */

    @OSID @Override
    public void matchAnyCommission(boolean match) {
        getAssembler().addIdWildcardTerm(getCommissionColumn(), match);
        return;
    }


    /**
     *  Clears the commission query terms. 
     */

    @OSID @Override
    public void clearCommissionTerms() {
        getAssembler().clearTerms(getCommissionColumn());
        return;
    }


    /**
     *  Gets the commission query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQueryInspector[] getCommissionTerms() {
        return (new org.osid.resourcing.CommissionQueryInspector[0]);
    }


    /**
     *  Gets the Commission column name.
     *
     * @return the column name
     */

    protected String getCommissionColumn() {
        return ("commission");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match works 
     *  assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this work supports the given record
     *  <code>Type</code>.
     *
     *  @param  workRecordType a work record type 
     *  @return <code>true</code> if the workRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type workRecordType) {
        for (org.osid.resourcing.records.WorkQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  workRecordType the work record type 
     *  @return the work query record 
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.WorkQueryRecord getWorkQueryRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.WorkQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  workRecordType the work record type 
     *  @return the work query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.WorkQueryInspectorRecord getWorkQueryInspectorRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.WorkQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param workRecordType the work record type
     *  @return the work search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.WorkSearchOrderRecord getWorkSearchOrderRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.WorkSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this work. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param workQueryRecord the work query record
     *  @param workQueryInspectorRecord the work query inspector
     *         record
     *  @param workSearchOrderRecord the work search order record
     *  @param workRecordType work record type
     *  @throws org.osid.NullArgumentException
     *          <code>workQueryRecord</code>,
     *          <code>workQueryInspectorRecord</code>,
     *          <code>workSearchOrderRecord</code> or
     *          <code>workRecordTypework</code> is
     *          <code>null</code>
     */
            
    protected void addWorkRecords(org.osid.resourcing.records.WorkQueryRecord workQueryRecord, 
                                      org.osid.resourcing.records.WorkQueryInspectorRecord workQueryInspectorRecord, 
                                      org.osid.resourcing.records.WorkSearchOrderRecord workSearchOrderRecord, 
                                      org.osid.type.Type workRecordType) {

        addRecordType(workRecordType);

        nullarg(workQueryRecord, "work query record");
        nullarg(workQueryInspectorRecord, "work query inspector record");
        nullarg(workSearchOrderRecord, "work search odrer record");

        this.queryRecords.add(workQueryRecord);
        this.queryInspectorRecords.add(workQueryInspectorRecord);
        this.searchOrderRecords.add(workSearchOrderRecord);
        
        return;
    }
}
