//
// AbstractEntryQueryInspector.java
//
//     A template for making an EntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for entries.
 */

public abstract class AbstractEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.dictionary.EntryQueryInspector {

    private final java.util.Collection<org.osid.dictionary.records.EntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the key type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getKeyTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the key query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.ObjectTerm[] getKeyTerms() {
        return (new org.osid.search.terms.ObjectTerm[0]);
    }


    /**
     *  Gets the value type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.ObjectTerm[] getValueTerms() {
        return (new org.osid.search.terms.ObjectTerm[0]);
    }


    /**
     *  Gets the dictionary <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDictionaryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the dictionary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQueryInspector[] getDictionaryTerms() {
        return (new org.osid.dictionary.DictionaryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an entry implementing the requested record.
     *
     *  @param entryRecordType an entry record type
     *  @return the entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.EntryQueryInspectorRecord getEntryQueryInspectorRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.EntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry query. 
     *
     *  @param entryQueryInspectorRecord entry query inspector
     *         record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEntryQueryInspectorRecord(org.osid.dictionary.records.EntryQueryInspectorRecord entryQueryInspectorRecord, 
                                                   org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);
        nullarg(entryRecordType, "entry record type");
        this.records.add(entryQueryInspectorRecord);        
        return;
    }
}
