//
// AbstractIndexedMapConfigurationLookupSession.java
//
//    A simple framework for providing a Configuration lookup service
//    backed by a fixed collection of configurations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Configuration lookup service backed by a
 *  fixed collection of configurations. The configurations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some configurations may be compatible
 *  with more types than are indicated through these configuration
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Configurations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapConfigurationLookupSession
    extends AbstractMapConfigurationLookupSession
    implements org.osid.configuration.ConfigurationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.configuration.Configuration> configurationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.Configuration>());
    private final MultiMap<org.osid.type.Type, org.osid.configuration.Configuration> configurationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.Configuration>());


    /**
     *  Makes a <code>Configuration</code> available in this session.
     *
     *  @param  configuration a configuration
     *  @throws org.osid.NullArgumentException <code>configuration<code> is
     *          <code>null</code>
     */

    @Override
    protected void putConfiguration(org.osid.configuration.Configuration configuration) {
        super.putConfiguration(configuration);

        this.configurationsByGenus.put(configuration.getGenusType(), configuration);
        
        try (org.osid.type.TypeList types = configuration.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.configurationsByRecord.put(types.getNextType(), configuration);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a configuration from this session.
     *
     *  @param configurationId the <code>Id</code> of the configuration
     *  @throws org.osid.NullArgumentException <code>configurationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeConfiguration(org.osid.id.Id configurationId) {
        org.osid.configuration.Configuration configuration;
        try {
            configuration = getConfiguration(configurationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.configurationsByGenus.remove(configuration.getGenusType());

        try (org.osid.type.TypeList types = configuration.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.configurationsByRecord.remove(types.getNextType(), configuration);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeConfiguration(configurationId);
        return;
    }


    /**
     *  Gets a <code>ConfigurationList</code> corresponding to the given
     *  configuration genus <code>Type</code> which does not include
     *  configurations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known configurations or an error results. Otherwise,
     *  the returned list may contain only those configurations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  configurationGenusType a configuration genus type 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByGenusType(org.osid.type.Type configurationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.configuration.ArrayConfigurationList(this.configurationsByGenus.get(configurationGenusType)));
    }


    /**
     *  Gets a <code>ConfigurationList</code> containing the given
     *  configuration record <code>Type</code>. In plenary mode, the
     *  returned list contains all known configurations or an error
     *  results. Otherwise, the returned list may contain only those
     *  configurations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return the returned <code>configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByRecordType(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.configuration.ArrayConfigurationList(this.configurationsByRecord.get(configurationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.configurationsByGenus.clear();
        this.configurationsByRecord.clear();

        super.close();

        return;
    }
}
