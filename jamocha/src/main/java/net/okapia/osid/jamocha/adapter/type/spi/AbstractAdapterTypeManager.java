//
// AbstractTypeManager.java
//
//     An adapter for a TypeManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.type.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TypeManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTypeManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.type.TypeManager>
    implements org.osid.type.TypeManager {


    /**
     *  Constructs a new {@code AbstractAdapterTypeManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTypeManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTypeManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTypeManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if <code> Type </code> lookup is supported. 
     *
     *  @return <code> true </code> if <code> Type </code> lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeLookup() {
        return (getAdapteeManager().supportsTypeLookup());
    }


    /**
     *  Tests if a <code> Type </code> administrative service is supported. 
     *
     *  @return <code> true </code> if <code> Type </code> administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeAdmin() {
        return (getAdapteeManager().supportsTypeAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the type lookup 
     *  service. 
     *
     *  @return a <code> TypeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTypeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeLookupSession getTypeLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTypeLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the type admin 
     *  service. 
     *
     *  @return a <code> TypeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTypeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeAdminSession getTypeAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTypeAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
