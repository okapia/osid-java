//
// AbstractQueryPackageLookupSession.java
//
//    A PackageQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PackageQuerySession adapter.
 */

public abstract class AbstractAdapterPackageQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.installation.PackageQuerySession {

    private final org.osid.installation.PackageQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterPackageQuerySession.
     *
     *  @param session the underlying package query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPackageQuerySession(org.osid.installation.PackageQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeDepot</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeDepot Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDepotId() {
        return (this.session.getDepotId());
    }


    /**
     *  Gets the {@codeDepot</code> associated with this 
     *  session.
     *
     *  @return the {@codeDepot</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDepot());
    }


    /**
     *  Tests if this user can perform {@codePackage</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchPackages() {
        return (this.session.canSearchPackages());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include packages in depots which are children
     *  of this depot in the depot hierarchy.
     */

    @OSID @Override
    public void useFederatedDepotView() {
        this.session.useFederatedDepotView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this depot only.
     */
    
    @OSID @Override
    public void useIsolatedDepotView() {
        this.session.useIsolatedDepotView();
        return;
    }
    
      
    /**
     *  The returns from the lookup methods may omit multiple versions
     *  of the same installation.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.session.useNormalizedVersionView();
        return;
    }


    /**
     *  All versions of the same installation are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.session.useDenormalizedVersionView();
        return;
    }


    /**
     *  Gets a package query. The returned query will not have an
     *  extension query.
     *
     *  @return the package query 
     */
      
    @OSID @Override
    public org.osid.installation.PackageQuery getPackageQuery() {
        return (this.session.getPackageQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  pkgQuery the package query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code pkgQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code pkgQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByQuery(org.osid.installation.PackageQuery pkgQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getPackagesByQuery(pkgQuery));
    }
}
