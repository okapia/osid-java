//
// AbstractAssemblyPathQuery.java
//
//     A PathQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PathQuery that stores terms.
 */

public abstract class AbstractAssemblyPathQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.topology.path.PathQuery,
               org.osid.topology.path.PathQueryInspector,
               org.osid.topology.path.PathSearchOrder {

    private final java.util.Collection<org.osid.topology.path.records.PathQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.path.records.PathQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.path.records.PathSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPathQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPathQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches paths that are complete. 
     *
     *  @param  match <code> true </code> to match complete paths, <code> 
     *          false </code> to match inactive paths 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        getAssembler().addBooleanTerm(getCompleteColumn(), match);
        return;
    }


    /**
     *  Clears the path complete query terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        getAssembler().clearTerms(getCompleteColumn());
        return;
    }


    /**
     *  Gets the complete query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (getAssembler().getBooleanTerms(getCompleteColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the complete 
     *  state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompleteColumn(), style);
        return;
    }


    /**
     *  Gets the Complete column name.
     *
     * @return the column name
     */

    protected String getCompleteColumn() {
        return ("complete");
    }


    /**
     *  Matches paths that are closed. 
     *
     *  @param  match <code> true </code> to match closed paths, <code> false 
     *          </code> to match inactive paths 
     */

    @OSID @Override
    public void matchClosed(boolean match) {
        getAssembler().addBooleanTerm(getClosedColumn(), match);
        return;
    }


    /**
     *  Clears the path closed query terms. 
     */

    @OSID @Override
    public void clearClosedTerms() {
        getAssembler().clearTerms(getClosedColumn());
        return;
    }


    /**
     *  Gets the closed query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getClosedTerms() {
        return (getAssembler().getBooleanTerms(getClosedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the open and 
     *  closed paths. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosed(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClosedColumn(), style);
        return;
    }


    /**
     *  Gets the Closed column name.
     *
     * @return the column name
     */

    protected String getClosedColumn() {
        return ("closed");
    }


    /**
     *  Sets the node <code> Id </code> for this query to match paths with a 
     *  starting node. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStartingNodeId(org.osid.id.Id nodeId, boolean match) {
        getAssembler().addIdTerm(getStartingNodeIdColumn(), nodeId, match);
        return;
    }


    /**
     *  Clears the starting node <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStartingNodeIdTerms() {
        getAssembler().clearTerms(getStartingNodeIdColumn());
        return;
    }


    /**
     *  Gets the starting node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStartingNodeIdTerms() {
        return (getAssembler().getIdTerms(getStartingNodeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the starting 
     *  node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartingNode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartingNodeColumn(), style);
        return;
    }


    /**
     *  Gets the StartingNodeId column name.
     *
     * @return the column name
     */

    protected String getStartingNodeIdColumn() {
        return ("starting_node_id");
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available for a starting node. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartingNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a starting node. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartingNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getStartingNodeQuery() {
        throw new org.osid.UnimplementedException("supportsStartingNodeQuery() is false");
    }


    /**
     *  Clears the starting node query terms. 
     */

    @OSID @Override
    public void clearStartingNodeTerms() {
        getAssembler().clearTerms(getStartingNodeColumn());
        return;
    }


    /**
     *  Gets the starting node query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getStartingNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Tests if a starting node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartingNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a starting node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartingNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getStartingNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStartingNodeSearchOrder() is false");
    }


    /**
     *  Gets the StartingNode column name.
     *
     * @return the column name
     */

    protected String getStartingNodeColumn() {
        return ("starting_node");
    }


    /**
     *  Sets the node <code> Id </code> for this query to match paths with an 
     *  ending node. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEndingNodeId(org.osid.id.Id nodeId, boolean match) {
        getAssembler().addIdTerm(getEndingNodeIdColumn(), nodeId, match);
        return;
    }


    /**
     *  Clears the ending node <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEndingNodeIdTerms() {
        getAssembler().clearTerms(getEndingNodeIdColumn());
        return;
    }


    /**
     *  Gets the ending node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndingNodeIdTerms() {
        return (getAssembler().getIdTerms(getEndingNodeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the starting 
     *  node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndingNode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndingNodeColumn(), style);
        return;
    }


    /**
     *  Gets the EndingNodeId column name.
     *
     * @return the column name
     */

    protected String getEndingNodeIdColumn() {
        return ("ending_node_id");
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available for an ending node. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndingNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ending node. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getEndingNodeQuery() {
        throw new org.osid.UnimplementedException("supportsEndingNodeQuery() is false");
    }


    /**
     *  Clears the ending node query terms. 
     */

    @OSID @Override
    public void clearEndingNodeTerms() {
        getAssembler().clearTerms(getEndingNodeColumn());
        return;
    }


    /**
     *  Gets the ending node query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getEndingNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Tests if an ending node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndingNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets an ending node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getEndingNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEndingNodeSearchOrder() is false");
    }


    /**
     *  Gets the EndingNode column name.
     *
     * @return the column name
     */

    protected String getEndingNodeColumn() {
        return ("ending_node");
    }


    /**
     *  Sets the node <code> Ids </code> for this query to match paths along 
     *  the given node. 
     *
     *  @param  nodeIds the node <code> Ids </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeIds </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAlongNodeIds(org.osid.id.Id[] nodeIds, boolean match) {
        getAssembler().addIdSetTerm(getAlongNodeIdsColumn(), nodeIds, match);
        return;
    }


    /**
     *  Clears the along node <code> Ids </code> query terms. 
     */

    @OSID @Override
    public void clearAlongNodeIdsTerms() {
        getAssembler().clearTerms(getAlongNodeIdsColumn());
        return;
    }


    /**
     *  Gets the along node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdSetTerm[] getAlongNodeIdsTerms() {
        return (getAssembler().getIdSetTerms(getAlongNodeIdsColumn()));
    }


    /**
     *  Gets the Along Node Ids column name.
     *
     *  @return the column name
     */

    protected String getAlongNodeIdsColumn() {
        return ("along_node_ids");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match paths 
     *  intersecting with another path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchIntersectingPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getIntersectingPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the intersecting path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathIdTerms() {
        getAssembler().clearTerms(getIntersectingPathIdColumn());
        return;
    }


    /**
     *  Gets the intersecting path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIntersectingPathIdTerms() {
        return (getAssembler().getIdTerms(getIntersectingPathIdColumn()));
    }


    /**
     *  Gets the IntersectingPathId column name.
     *
     * @return the column name
     */

    protected String getIntersectingPathIdColumn() {
        return ("intersecting_path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available for intersecting 
     *  paths, 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectingPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for an intersecting path, Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectingPathQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuery getIntersectingPathQuery() {
        throw new org.osid.UnimplementedException("supportsIntersectingPathQuery() is false");
    }


    /**
     *  Matches paths with any intersecting path, 
     *
     *  @param  match <code> true </code> to match paths with any intersecting 
     *          path, <code> false </code> to match paths with no intersecting 
     *          path 
     */

    @OSID @Override
    public void matchAnyIntersectingPath(boolean match) {
        getAssembler().addIdWildcardTerm(getIntersectingPathColumn(), match);
        return;
    }


    /**
     *  Clears the intersecting path query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathTerms() {
        getAssembler().clearTerms(getIntersectingPathColumn());
        return;
    }


    /**
     *  Gets the intersecting path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.path.PathQueryInspector[] getIntersectingPathTerms() {
        return (new org.osid.topology.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the IntersectingPath column name.
     *
     * @return the column name
     */

    protected String getIntersectingPathColumn() {
        return ("intersecting_path");
    }


    /**
     *  Matches paths that have a number of hops within the given range 
     *  inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchHops(long from, long to, boolean match) {
        getAssembler().addCardinalRangeTerm(getHopsColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the number of hops query terms. 
     */

    @OSID @Override
    public void clearHopsTerms() {
        getAssembler().clearTerms(getHopsColumn());
        return;
    }


    /**
     *  Gets the hops query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getHopsTerms() {
        return (getAssembler().getCardinalRangeTerms(getHopsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the number of 
     *  hops in the path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHops(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getHopsColumn(), style);
        return;
    }


    /**
     *  Gets the Hops column name.
     *
     * @return the column name
     */

    protected String getHopsColumn() {
        return ("hops");
    }


    /**
     *  Matches paths that have a distance with the given range inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchDistance(java.math.BigDecimal from, 
                              java.math.BigDecimal to, boolean match) {
        getAssembler().addDecimalRangeTerm(getDistanceColumn(), from, to, match);
        return;
    }


    /**
     *  Matches paths that has any distance assigned. 
     *
     *  @param  match <code> true </code> to match paths with any distance, 
     *          <code> false </code> to match paths with no distance assigned 
     */

    @OSID @Override
    public void matchAnyDistance(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getDistanceColumn(), match);
        return;
    }


    /**
     *  Clears the distance query terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        getAssembler().clearTerms(getDistanceColumn());
        return;
    }


    /**
     *  Gets the distance query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDistanceTerms() {
        return (getAssembler().getDecimalRangeTerms(getDistanceColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the path 
     *  distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistanceColumn(), style);
        return;
    }


    /**
     *  Gets the Distance column name.
     *
     * @return the column name
     */

    protected String getDistanceColumn() {
        return ("distance");
    }


    /**
     *  Matches paths that have a cost with the given range inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchCost(java.math.BigDecimal from, java.math.BigDecimal to, 
                          boolean match) {
        getAssembler().addDecimalRangeTerm(getCostColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the cost query terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        getAssembler().clearTerms(getCostColumn());
        return;
    }


    /**
     *  Gets the cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCostTerms() {
        return (getAssembler().getDecimalRangeTerms(getCostColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the path cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCostColumn(), style);
        return;
    }


    /**
     *  Gets the Cost column name.
     *
     * @return the column name
     */

    protected String getCostColumn() {
        return ("cost");
    }


    /**
     *  Sets the node <code> Id </code> for this query to match paths that 
     *  pass through nodes. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchNodeId(org.osid.id.Id nodeId, boolean match) {
        getAssembler().addIdTerm(getNodeIdColumn(), nodeId, match);
        return;
    }


    /**
     *  Clears the node <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearNodeIdTerms() {
        getAssembler().clearTerms(getNodeIdColumn());
        return;
    }


    /**
     *  Gets the node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNodeIdTerms() {
        return (getAssembler().getIdTerms(getNodeIdColumn()));
    }


    /**
     *  Gets the NodeId column name.
     *
     * @return the column name
     */

    protected String getNodeIdColumn() {
        return ("node_id");
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a node. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getNodeQuery() {
        throw new org.osid.UnimplementedException("supportsNodeQuery() is false");
    }


    /**
     *  Clears the node query terms. 
     */

    @OSID @Override
    public void clearNodeTerms() {
        getAssembler().clearTerms(getNodeColumn());
        return;
    }


    /**
     *  Gets the node query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the Node column name.
     *
     * @return the column name
     */

    protected String getNodeColumn() {
        return ("node");
    }


    /**
     *  Sets the edge <code> Id </code> for this query to match paths contain 
     *  the edge. 
     *
     *  @param  edgeId the edge <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> edgeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEdgeId(org.osid.id.Id edgeId, boolean match) {
        getAssembler().addIdTerm(getEdgeIdColumn(), edgeId, match);
        return;
    }


    /**
     *  Clears the edge <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEdgeIdTerms() {
        getAssembler().clearTerms(getEdgeIdColumn());
        return;
    }


    /**
     *  Gets the edge <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEdgeIdTerms() {
        return (getAssembler().getIdTerms(getEdgeIdColumn()));
    }


    /**
     *  Gets the EdgeId column name.
     *
     * @return the column name
     */

    protected String getEdgeIdColumn() {
        return ("edge_id");
    }


    /**
     *  Tests if an <code> EdgeQuery </code> is available. 
     *
     *  @return <code> true </code> if an edge query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an edge. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the edge query 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuery getEdgeQuery() {
        throw new org.osid.UnimplementedException("supportsEdgeQuery() is false");
    }


    /**
     *  Clears the edge query terms. 
     */

    @OSID @Override
    public void clearEdgeTerms() {
        getAssembler().clearTerms(getEdgeColumn());
        return;
    }


    /**
     *  Gets the edge query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.EdgeQueryInspector[] getEdgeTerms() {
        return (new org.osid.topology.EdgeQueryInspector[0]);
    }


    /**
     *  Gets the Edge column name.
     *
     * @return the column name
     */

    protected String getEdgeColumn() {
        return ("edge");
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match edges 
     *  assigned to graphs. 
     *
     *  @param  graphId the graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraphId(org.osid.id.Id graphId, boolean match) {
        getAssembler().addIdTerm(getGraphIdColumn(), graphId, match);
        return;
    }


    /**
     *  Clears the graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGraphIdTerms() {
        getAssembler().clearTerms(getGraphIdColumn());
        return;
    }


    /**
     *  Gets the graph <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraphIdTerms() {
        return (getAssembler().getIdTerms(getGraphIdColumn()));
    }


    /**
     *  Gets the GraphId column name.
     *
     * @return the column name
     */

    protected String getGraphIdColumn() {
        return ("graph_id");
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getGraphQuery() {
        throw new org.osid.UnimplementedException("supportsGraphQuery() is false");
    }


    /**
     *  Clears the graph terms. 
     */

    @OSID @Override
    public void clearGraphTerms() {
        getAssembler().clearTerms(getGraphColumn());
        return;
    }


    /**
     *  Gets the graph query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }


    /**
     *  Gets the Graph column name.
     *
     * @return the column name
     */

    protected String getGraphColumn() {
        return ("graph");
    }


    /**
     *  Tests if this path supports the given record
     *  <code>Type</code>.
     *
     *  @param  pathRecordType a path record type 
     *  @return <code>true</code> if the pathRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pathRecordType) {
        for (org.osid.topology.path.records.PathQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  pathRecordType the path record type 
     *  @return the path query record 
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.path.records.PathQueryRecord getPathQueryRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.path.records.PathQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  pathRecordType the path record type 
     *  @return the path query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.path.records.PathQueryInspectorRecord getPathQueryInspectorRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.path.records.PathQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param pathRecordType the path record type
     *  @return the path search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.path.records.PathSearchOrderRecord getPathSearchOrderRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.path.records.PathSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this path. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pathQueryRecord the path query record
     *  @param pathQueryInspectorRecord the path query inspector
     *         record
     *  @param pathSearchOrderRecord the path search order record
     *  @param pathRecordType path record type
     *  @throws org.osid.NullArgumentException
     *          <code>pathQueryRecord</code>,
     *          <code>pathQueryInspectorRecord</code>,
     *          <code>pathSearchOrderRecord</code> or
     *          <code>pathRecordTypepath</code> is
     *          <code>null</code>
     */
            
    protected void addPathRecords(org.osid.topology.path.records.PathQueryRecord pathQueryRecord, 
                                      org.osid.topology.path.records.PathQueryInspectorRecord pathQueryInspectorRecord, 
                                      org.osid.topology.path.records.PathSearchOrderRecord pathSearchOrderRecord, 
                                      org.osid.type.Type pathRecordType) {

        addRecordType(pathRecordType);

        nullarg(pathQueryRecord, "path query record");
        nullarg(pathQueryInspectorRecord, "path query inspector record");
        nullarg(pathSearchOrderRecord, "path search odrer record");

        this.queryRecords.add(pathQueryRecord);
        this.queryInspectorRecords.add(pathQueryInspectorRecord);
        this.searchOrderRecords.add(pathSearchOrderRecord);
        
        return;
    }
}
