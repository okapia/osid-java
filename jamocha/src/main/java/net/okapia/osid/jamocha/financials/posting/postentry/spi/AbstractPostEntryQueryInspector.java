//
// AbstractPostEntryQueryInspector.java
//
//     A template for making a PostEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for post entries.
 */

public abstract class AbstractPostEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.financials.posting.PostEntryQueryInspector {

    private final java.util.Collection<org.osid.financials.posting.records.PostEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the post <code> Id </code> query terms. 
     *
     *  @return the post <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the post query terms. 
     *
     *  @return the post query terms 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQueryInspector[] getPostTerms() {
        return (new org.osid.financials.posting.PostQueryInspector[0]);
    }


    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account query terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the debit query terms. 
     *
     *  @return the debit query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDebitTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given post entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a post entry implementing the requested record.
     *
     *  @param postEntryRecordType a post entry record type
     *  @return the post entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntryQueryInspectorRecord getPostEntryQueryInspectorRecord(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this post entry query. 
     *
     *  @param postEntryQueryInspectorRecord post entry query inspector
     *         record
     *  @param postEntryRecordType postEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPostEntryQueryInspectorRecord(org.osid.financials.posting.records.PostEntryQueryInspectorRecord postEntryQueryInspectorRecord, 
                                                   org.osid.type.Type postEntryRecordType) {

        addRecordType(postEntryRecordType);
        nullarg(postEntryRecordType, "post entry record type");
        this.records.add(postEntryQueryInspectorRecord);        
        return;
    }
}
