//
// AbstractAssessmentSearch.java
//
//     A template for making an Assessment Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing assessment searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAssessmentSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.assessment.AssessmentSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.assessment.AssessmentSearchOrder assessmentSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of assessments. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  assessmentIds list of assessments
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAssessments(org.osid.id.IdList assessmentIds) {
        while (assessmentIds.hasNext()) {
            try {
                this.ids.add(assessmentIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAssessments</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of assessment Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAssessmentIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  assessmentSearchOrder assessment search order 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>assessmentSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAssessmentResults(org.osid.assessment.AssessmentSearchOrder assessmentSearchOrder) {
	this.assessmentSearchOrder = assessmentSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
	return (this.assessmentSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given assessment search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment implementing the requested record.
     *
     *  @param assessmentSearchRecordType an assessment search record
     *         type
     *  @return the assessment search record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentSearchRecord getAssessmentSearchRecord(org.osid.type.Type assessmentSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.assessment.records.AssessmentSearchRecord record : this.records) {
            if (record.implementsRecordType(assessmentSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment search. 
     *
     *  @param assessmentSearchRecord assessment search record
     *  @param assessmentSearchRecordType assessment search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentSearchRecord(org.osid.assessment.records.AssessmentSearchRecord assessmentSearchRecord, 
                                           org.osid.type.Type assessmentSearchRecordType) {

        addRecordType(assessmentSearchRecordType);
        this.records.add(assessmentSearchRecord);        
        return;
    }
}
