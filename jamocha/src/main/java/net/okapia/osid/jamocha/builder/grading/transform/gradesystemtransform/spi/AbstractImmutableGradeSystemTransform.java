//
// AbstractImmutableGradeSystemTransform.java
//
//     Wraps a mutable GradeSystemTransform to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.transform.gradesystemtransform.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>GradeSystemTransform</code> to hide modifiers. This
 *  wrapper provides an immutized GradeSystemTransform from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying gradeSystemTransform whose state changes are visible.
 */

public abstract class AbstractImmutableGradeSystemTransform
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.grading.transform.GradeSystemTransform {

    private final org.osid.grading.transform.GradeSystemTransform gradeSystemTransform;


    /**
     *  Constructs a new <code>AbstractImmutableGradeSystemTransform</code>.
     *
     *  @param gradeSystemTransform the grade system transform to immutablize
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransform</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableGradeSystemTransform(org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        super(gradeSystemTransform);
        this.gradeSystemTransform = gradeSystemTransform;
        return;
    }


    /**
     *  Gets the source <code> GradeSystem Id. </code> 
     *
     *  @return the source grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceGradeSystemId() {
        return (this.gradeSystemTransform.getSourceGradeSystemId());
    }


    /**
     *  Gets the source <code> GradeSystem. </code> 
     *
     *  @return the source grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getSourceGradeSystem()
        throws org.osid.OperationFailedException {

        return (this.gradeSystemTransform.getSourceGradeSystem());
    }


    /**
     *  Gets the target <code> GradeSystem Id. </code> 
     *
     *  @return the target grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTargetGradeSystemId() {
        return (this.gradeSystemTransform.getTargetGradeSystemId());
    }


    /**
     *  Gets the target <code> GradeSystem. </code> 
     *
     *  @return the target grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getTargetGradeSystem()
        throws org.osid.OperationFailedException {

        return (this.gradeSystemTransform.getTargetGradeSystem());
    }


    /**
     *  Tests if this transformation is based on normalization of the input 
     *  scores. 
     *
     *  @return <code> true </code> of a normalization is performed, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean normalizesInputScores() {
        return (this.gradeSystemTransform.normalizesInputScores());
    }


    /**
     *  Gets any one-to-one mapping of grades. 
     *
     *  @return the grade map 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeMapList getGradeMaps() {
        return (this.gradeSystemTransform.getGradeMaps());
    }


    /**
     *  Gets the grade system transform record corresponding to the given 
     *  <code> GradeSystemTransform </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> gradeSystemTransformRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(gradeSystemTransformRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  gradeSystemTransformRecordType the type of the record to 
     *          retrieve 
     *  @return the grade system transform record 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradeSystemTransformRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(gradeSystemTransformRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.records.GradeSystemTransformRecord getGradeSystemTransformRecord(org.osid.type.Type gradeSystemTransformRecordType)
        throws org.osid.OperationFailedException {

        return (this.gradeSystemTransform.getGradeSystemTransformRecord(gradeSystemTransformRecordType));
    }
}

