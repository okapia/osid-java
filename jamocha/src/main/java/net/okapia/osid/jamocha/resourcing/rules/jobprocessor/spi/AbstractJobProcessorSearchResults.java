//
// AbstractJobProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractJobProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.rules.JobProcessorSearchResults {

    private org.osid.resourcing.rules.JobProcessorList jobProcessors;
    private final org.osid.resourcing.rules.JobProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractJobProcessorSearchResults.
     *
     *  @param jobProcessors the result set
     *  @param jobProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>jobProcessors</code>
     *          or <code>jobProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractJobProcessorSearchResults(org.osid.resourcing.rules.JobProcessorList jobProcessors,
                                            org.osid.resourcing.rules.JobProcessorQueryInspector jobProcessorQueryInspector) {
        nullarg(jobProcessors, "job processors");
        nullarg(jobProcessorQueryInspector, "job processor query inspectpr");

        this.jobProcessors = jobProcessors;
        this.inspector = jobProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the job processor list resulting from a search.
     *
     *  @return a job processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessors() {
        if (this.jobProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.rules.JobProcessorList jobProcessors = this.jobProcessors;
        this.jobProcessors = null;
	return (jobProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.rules.JobProcessorQueryInspector getJobProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  job processor search record <code> Type. </code> This method must
     *  be used to retrieve a jobProcessor implementing the requested
     *  record.
     *
     *  @param jobProcessorSearchRecordType a jobProcessor search 
     *         record type 
     *  @return the job processor search
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(jobProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorSearchResultsRecord getJobProcessorSearchResultsRecord(org.osid.type.Type jobProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.rules.records.JobProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(jobProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(jobProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record job processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addJobProcessorRecord(org.osid.resourcing.rules.records.JobProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "job processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
