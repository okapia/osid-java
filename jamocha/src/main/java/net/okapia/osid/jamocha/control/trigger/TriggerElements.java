//
// TriggerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.trigger;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class TriggerElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the TriggerElement Id.
     *
     *  @return the trigger element Id
     */

    public static org.osid.id.Id getTriggerEntityId() {
        return (makeEntityId("osid.control.Trigger"));
    }


    /**
     *  Gets the ControllerId element Id.
     *
     *  @return the ControllerId element Id
     */

    public static org.osid.id.Id getControllerId() {
        return (makeElementId("osid.control.trigger.ControllerId"));
    }


    /**
     *  Gets the Controller element Id.
     *
     *  @return the Controller element Id
     */

    public static org.osid.id.Id getController() {
        return (makeElementId("osid.control.trigger.Controller"));
    }


    /**
     *  Gets the DiscreetStateId element Id.
     *
     *  @return the DiscreetStateId element Id
     */

    public static org.osid.id.Id getDiscreetStateId() {
        return (makeElementId("osid.control.trigger.DiscreetStateId"));
    }


    /**
     *  Gets the DiscreetState element Id.
     *
     *  @return the DiscreetState element Id
     */

    public static org.osid.id.Id getDiscreetState() {
        return (makeElementId("osid.control.trigger.DiscreetState"));
    }


    /**
     *  Gets the ActionGroupIds element Id.
     *
     *  @return the ActionGroupIds element Id
     */

    public static org.osid.id.Id getActionGroupIds() {
        return (makeElementId("osid.control.trigger.ActionGroupIds"));
    }


    /**
     *  Gets the ActionGroups element Id.
     *
     *  @return the ActionGroups element Id
     */

    public static org.osid.id.Id getActionGroups() {
        return (makeElementId("osid.control.trigger.ActionGroups"));
    }


    /**
     *  Gets the SceneIds element Id.
     *
     *  @return the SceneIds element Id
     */

    public static org.osid.id.Id getSceneIds() {
        return (makeElementId("osid.control.trigger.SceneIds"));
    }


    /**
     *  Gets the Scenes element Id.
     *
     *  @return the Scenes element Id
     */

    public static org.osid.id.Id getScenes() {
        return (makeElementId("osid.control.trigger.Scenes"));
    }


    /**
     *  Gets the SettingIds element Id.
     *
     *  @return the SettingIds element Id
     */

    public static org.osid.id.Id getSettingIds() {
        return (makeElementId("osid.control.trigger.SettingIds"));
    }


    /**
     *  Gets the Settings element Id.
     *
     *  @return the Settings element Id
     */

    public static org.osid.id.Id getSettings() {
        return (makeElementId("osid.control.trigger.Settings"));
    }


    /**
     *  Gets the TurnedOn element Id.
     *
     *  @return the TurnedOn element Id
     */

    public static org.osid.id.Id getTurnedOn() {
        return (makeQueryElementId("osid.control.trigger.TurnedOn"));
    }


    /**
     *  Gets the TurnedOff element Id.
     *
     *  @return the TurnedOff element Id
     */

    public static org.osid.id.Id getTurnedOff() {
        return (makeQueryElementId("osid.control.trigger.TurnedOff"));
    }


    /**
     *  Gets the ChangedVariableAmount element Id.
     *
     *  @return the ChangedVariableAmount element Id
     */

    public static org.osid.id.Id getChangedVariableAmount() {
        return (makeQueryElementId("osid.control.trigger.ChangedVariableAmount"));
    }


    /**
     *  Gets the ExceedsVariableAmount element Id.
     *
     *  @return the ExceedsVariableAmount element Id
     */

    public static org.osid.id.Id getExceedsVariableAmount() {
        return (makeQueryElementId("osid.control.trigger.ExceedsVariableAmount"));
    }


    /**
     *  Gets the DeceedsVariableAmount element Id.
     *
     *  @return the DeceedsVariableAmount element Id
     */

    public static org.osid.id.Id getDeceedsVariableAmount() {
        return (makeQueryElementId("osid.control.trigger.DeceedsVariableAmount"));
    }


    /**
     *  Gets the ChangedDiscreetState element Id.
     *
     *  @return the ChangedDiscreetState element Id
     */

    public static org.osid.id.Id getChangedDiscreetState() {
        return (makeQueryElementId("osid.control.trigger.ChangedDiscreetState"));
    }


    /**
     *  Gets the SystemId element Id.
     *
     *  @return the SystemId element Id
     */

    public static org.osid.id.Id getSystemId() {
        return (makeQueryElementId("osid.control.trigger.SystemId"));
    }


    /**
     *  Gets the System element Id.
     *
     *  @return the System element Id
     */

    public static org.osid.id.Id getSystem() {
        return (makeQueryElementId("osid.control.trigger.System"));
    }
}
