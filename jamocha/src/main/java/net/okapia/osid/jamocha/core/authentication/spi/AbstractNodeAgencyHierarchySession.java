//
// AbstractNodeAgencyHierarchySession.java
//
//     Defines an Agency hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an agency hierarchy session for delivering a hierarchy
 *  of agencies using the AgencyNode interface.
 */

public abstract class AbstractNodeAgencyHierarchySession
    extends net.okapia.osid.jamocha.authentication.spi.AbstractAgencyHierarchySession
    implements org.osid.authentication.AgencyHierarchySession {

    private java.util.Collection<org.osid.authentication.AgencyNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root agency <code> Ids </code> in this hierarchy.
     *
     *  @return the root agency <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootAgencyIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.authentication.agencynode.AgencyNodeToIdList(this.roots));
    }


    /**
     *  Gets the root agencies in the agency hierarchy. A node
     *  with no parents is an orphan. While all agency <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root agencies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getRootAgencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authentication.agencynode.AgencyNodeToAgencyList(new net.okapia.osid.jamocha.authentication.agencynode.ArrayAgencyNodeList(this.roots)));
    }


    /**
     *  Adds a root agency node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootAgency(org.osid.authentication.AgencyNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root agency nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootAgencies(java.util.Collection<org.osid.authentication.AgencyNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root agency node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootAgency(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.authentication.AgencyNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Agency </code> has any parents. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @return <code> true </code> if the agency has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentAgencies(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getAgencyNode(agencyId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  agency.
     *
     *  @param  id an <code> Id </code> 
     *  @param  agencyId the <code> Id </code> of an agency 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> agencyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> agencyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfAgency(org.osid.id.Id id, org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authentication.AgencyNodeList parents = getAgencyNode(agencyId).getParentAgencyNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextAgencyNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given agency. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @return the parent <code> Ids </code> of the agency 
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentAgencyIds(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authentication.agency.AgencyToIdList(getParentAgencies(agencyId)));
    }


    /**
     *  Gets the parents of the given agency. 
     *
     *  @param  agencyId the <code> Id </code> to query 
     *  @return the parents of the agency 
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getParentAgencies(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authentication.agencynode.AgencyNodeToAgencyList(getAgencyNode(agencyId).getParentAgencyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  agency.
     *
     *  @param  id an <code> Id </code> 
     *  @param  agencyId the Id of an agency 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> agencyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfAgency(org.osid.id.Id id, org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAgency(id, agencyId)) {
            return (true);
        }

        try (org.osid.authentication.AgencyList parents = getParentAgencies(agencyId)) {
            while (parents.hasNext()) {
                if (isAncestorOfAgency(id, parents.getNextAgency().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an agency has any children. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @return <code> true </code> if the <code> agencyId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildAgencies(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAgencyNode(agencyId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  agency.
     *
     *  @param  id an <code> Id </code> 
     *  @param agencyId the <code> Id </code> of an 
     *         agency
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> agencyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> agencyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfAgency(org.osid.id.Id id, org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfAgency(agencyId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  agency.
     *
     *  @param  agencyId the <code> Id </code> to query 
     *  @return the children of the agency 
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAgencyIds(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authentication.agency.AgencyToIdList(getChildAgencies(agencyId)));
    }


    /**
     *  Gets the children of the given agency. 
     *
     *  @param  agencyId the <code> Id </code> to query 
     *  @return the children of the agency 
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getChildAgencies(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authentication.agencynode.AgencyNodeToAgencyList(getAgencyNode(agencyId).getChildAgencyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  agency.
     *
     *  @param  id an <code> Id </code> 
     *  @param agencyId the <code> Id </code> of an 
     *         agency
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> agencyId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfAgency(org.osid.id.Id id, org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAgency(agencyId, id)) {
            return (true);
        }

        try (org.osid.authentication.AgencyList children = getChildAgencies(agencyId)) {
            while (children.hasNext()) {
                if (isDescendantOfAgency(id, children.getNextAgency().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  agency.
     *
     *  @param  agencyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified agency node 
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getAgencyNodeIds(org.osid.id.Id agencyId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.authentication.agencynode.AgencyNodeToNode(getAgencyNode(agencyId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given agency.
     *
     *  @param  agencyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified agency node 
     *  @throws org.osid.NotFoundException <code> agencyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> agencyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyNode getAgencyNodes(org.osid.id.Id agencyId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAgencyNode(agencyId));
    }


    /**
     *  Closes this <code>AgencyHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an agency node.
     *
     *  @param agencyId the id of the agency node
     *  @throws org.osid.NotFoundException <code>agencyId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>agencyId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.authentication.AgencyNode getAgencyNode(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(agencyId, "agency Id");
        for (org.osid.authentication.AgencyNode agency : this.roots) {
            if (agency.getId().equals(agencyId)) {
                return (agency);
            }

            org.osid.authentication.AgencyNode r = findAgency(agency, agencyId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(agencyId + " is not found");
    }


    protected org.osid.authentication.AgencyNode findAgency(org.osid.authentication.AgencyNode node, 
                                                            org.osid.id.Id agencyId)
        throws org.osid.OperationFailedException {

        try (org.osid.authentication.AgencyNodeList children = node.getChildAgencyNodes()) {
            while (children.hasNext()) {
                org.osid.authentication.AgencyNode agency = children.getNextAgencyNode();
                if (agency.getId().equals(agencyId)) {
                    return (agency);
                }
                
                agency = findAgency(agency, agencyId);
                if (agency != null) {
                    return (agency);
                }
            }
        }

        return (null);
    }
}
