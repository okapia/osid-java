//
// AbstractAssemblyRouteQuery.java
//
//     A RouteQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.route.route.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RouteQuery that stores terms.
 */

public abstract class AbstractAssemblyRouteQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.mapping.route.RouteQuery,
               org.osid.mapping.route.RouteQueryInspector,
               org.osid.mapping.route.RouteSearchOrder {

    private final java.util.Collection<org.osid.mapping.route.records.RouteQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.route.records.RouteQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.route.records.RouteSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRouteQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRouteQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the location <code> Id </code> for this query to match routes 
     *  with a starting location. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStartingLocationId(org.osid.id.Id locationId, 
                                        boolean match) {
        getAssembler().addIdTerm(getStartingLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the starting location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStartingLocationIdTerms() {
        getAssembler().clearTerms(getStartingLocationIdColumn());
        return;
    }


    /**
     *  Gets the starting location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStartingLocationIdTerms() {
        return (getAssembler().getIdTerms(getStartingLocationIdColumn()));
    }


    /**
     *  Gets the StartingLocationId column name.
     *
     * @return the column name
     */

    protected String getStartingLocationIdColumn() {
        return ("starting_location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for a starting 
     *  location. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a starting location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartingLocationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getStartingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsStartingLocationQuery() is false");
    }


    /**
     *  Clears the starting location query terms. 
     */

    @OSID @Override
    public void clearStartingLocationTerms() {
        getAssembler().clearTerms(getStartingLocationColumn());
        return;
    }


    /**
     *  Gets the starting location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getStartingLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the StartingLocation column name.
     *
     * @return the column name
     */

    protected String getStartingLocationColumn() {
        return ("starting_location");
    }


    /**
     *  Sets the location <code> Id </code> for this query to match routes 
     *  with an ending location. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEndingLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getEndingLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the ending location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEndingLocationIdTerms() {
        getAssembler().clearTerms(getEndingLocationIdColumn());
        return;
    }


    /**
     *  Gets the ending location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndingLocationIdTerms() {
        return (getAssembler().getIdTerms(getEndingLocationIdColumn()));
    }


    /**
     *  Gets the EndingLocationId column name.
     *
     * @return the column name
     */

    protected String getEndingLocationIdColumn() {
        return ("ending_location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for an ending 
     *  location. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ending location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingLocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getEndingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsEndingLocationQuery() is false");
    }


    /**
     *  Clears the ending location query terms. 
     */

    @OSID @Override
    public void clearEndingLocationTerms() {
        getAssembler().clearTerms(getEndingLocationColumn());
        return;
    }


    /**
     *  Gets the ending location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getEndingLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the EndingLocation column name.
     *
     * @return the column name
     */

    protected String getEndingLocationColumn() {
        return ("ending_location");
    }


    /**
     *  Sets the location <code> Ids </code> for this query to match routes 
     *  along all the given locations. 
     *
     *  @param  locationIds the location <code> Ids </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationIds </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAlongLocationIds(org.osid.id.Id[] locationIds, 
                                     boolean match) {
        getAssembler().addIdSetTerm(getAlongLocationIdsColumn(), locationIds, match);
        return;
    }


    /**
     *  Clears the along location <code> Ids </code> query terms. 
     */

    @OSID @Override
    public void clearAlongLocationIdsTerms() {
        getAssembler().clearTerms(getAlongLocationIdsColumn());
        return;
    }


    /**
     *  Gets the along location <code> Ids </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdSetTerm[] getAlongLocationIdsTerms() {
        return (getAssembler().getIdSetTerms(getAlongLocationIdsColumn()));
    }


    /**
     *  Gets the Along Location Ids column name.
     *
     *  @return the column name
     */

    protected String getAlongLocationIdsColumn() {
        return ("along_location_ids");
    }


    /**
     *  Matches routes that have distances within the specified range 
     *  inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchDistance(org.osid.mapping.Distance from, 
                              org.osid.mapping.Distance to, boolean match) {
        getAssembler().addDistanceRangeTerm(getDistanceColumn(), from, to, match);
        return;
    }


    /**
     *  Matches routes that has any distance assigned or calculated.. 
     *
     *  @param  match <code> true </code> to match routes with any distance, 
     *          <code> false </code> to match routes with no distance 
     */

    @OSID @Override
    public void matchAnyDistance(boolean match) {
        getAssembler().addDistanceRangeWildcardTerm(getDistanceColumn(), match);
        return;
    }


    /**
     *  Clears the distance query terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        getAssembler().clearTerms(getDistanceColumn());
        return;
    }


    /**
     *  Gets the distance query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DistanceRangeTerm[] getDistanceTerms() {
        return (getAssembler().getDistanceRangeTerms(getDistanceColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the route 
     *  distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistanceColumn(), style);
        return;
    }


    /**
     *  Gets the Distance column name.
     *
     * @return the column name
     */

    protected String getDistanceColumn() {
        return ("distance");
    }


    /**
     *  Matches routes that have the specified estimated travel time 
     *  inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchETA(org.osid.calendaring.Duration from, 
                         org.osid.calendaring.Duration to, boolean match) {
        getAssembler().addDurationRangeTerm(getETAColumn(), from, to, match);
        return;
    }


    /**
     *  Matches routes that has any estimated time assigned or calculated. 
     *
     *  @param  match <code> true </code> to match routes with any eta, <code> 
     *          false </code> to match routes with no eta 
     */

    @OSID @Override
    public void matchAnyETA(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getETAColumn(), match);
        return;
    }


    /**
     *  Clears the ETA query terms. 
     */

    @OSID @Override
    public void clearETATerms() {
        getAssembler().clearTerms(getETAColumn());
        return;
    }


    /**
     *  Gets the ETA query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getETATerms() {
        return (getAssembler().getDurationRangeTerms(getETAColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the route travel 
     *  time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByETA(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getETAColumn(), style);
        return;
    }


    /**
     *  Gets the ETA column name.
     *
     * @return the column name
     */

    protected String getETAColumn() {
        return ("e_ta");
    }


    /**
     *  Sets the location <code> Id </code> for this query to match routes 
     *  through a location. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches routes that has any location along the route, exclusive of the 
     *  endpoints. 
     *
     *  @param  match <code> true </code> to match routes with any location, 
     *          <code> false </code> to match routes with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location query terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match routes using a 
     *  designated path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        getAssembler().clearTerms(getPathIdColumn());
        return;
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (getAssembler().getIdTerms(getPathIdColumn()));
    }


    /**
     *  Gets the PathId column name.
     *
     * @return the column name
     */

    protected String getPathIdColumn() {
        return ("path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches routes using any designated path. 
     *
     *  @param  match <code> true </code> to match routes with any path <code> 
     *          false </code> to match routes with no path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        getAssembler().addIdWildcardTerm(getPathColumn(), match);
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the Path column name.
     *
     * @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Sets the map <code> Id </code> for this query to match routes assigned 
     *  to maps. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this route supports the given record
     *  <code>Type</code>.
     *
     *  @param  routeRecordType a route record type 
     *  @return <code>true</code> if the routeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type routeRecordType) {
        for (org.osid.mapping.route.records.RouteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(routeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  routeRecordType the route record type 
     *  @return the route query record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteQueryRecord getRouteQueryRecord(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(routeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  routeRecordType the route record type 
     *  @return the route query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteQueryInspectorRecord getRouteQueryInspectorRecord(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(routeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param routeRecordType the route record type
     *  @return the route search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSearchOrderRecord getRouteSearchOrderRecord(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(routeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this route. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param routeQueryRecord the route query record
     *  @param routeQueryInspectorRecord the route query inspector
     *         record
     *  @param routeSearchOrderRecord the route search order record
     *  @param routeRecordType route record type
     *  @throws org.osid.NullArgumentException
     *          <code>routeQueryRecord</code>,
     *          <code>routeQueryInspectorRecord</code>,
     *          <code>routeSearchOrderRecord</code> or
     *          <code>routeRecordTyperoute</code> is
     *          <code>null</code>
     */
            
    protected void addRouteRecords(org.osid.mapping.route.records.RouteQueryRecord routeQueryRecord, 
                                      org.osid.mapping.route.records.RouteQueryInspectorRecord routeQueryInspectorRecord, 
                                      org.osid.mapping.route.records.RouteSearchOrderRecord routeSearchOrderRecord, 
                                      org.osid.type.Type routeRecordType) {

        addRecordType(routeRecordType);

        nullarg(routeQueryRecord, "route query record");
        nullarg(routeQueryInspectorRecord, "route query inspector record");
        nullarg(routeSearchOrderRecord, "route search odrer record");

        this.queryRecords.add(routeQueryRecord);
        this.queryInspectorRecords.add(routeQueryInspectorRecord);
        this.searchOrderRecords.add(routeSearchOrderRecord);
        
        return;
    }
}
