//
// AbstractCourseOfferingSearch.java
//
//     A template for making a CourseOffering Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing course offering searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCourseOfferingSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.CourseOfferingSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseOfferingSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.CourseOfferingSearchOrder courseOfferingSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of course offerings. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  courseOfferingIds list of course offerings
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCourseOfferings(org.osid.id.IdList courseOfferingIds) {
        while (courseOfferingIds.hasNext()) {
            try {
                this.ids.add(courseOfferingIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCourseOfferings</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of course offering Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCourseOfferingIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  courseOfferingSearchOrder course offering search order 
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>courseOfferingSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCourseOfferingResults(org.osid.course.CourseOfferingSearchOrder courseOfferingSearchOrder) {
	this.courseOfferingSearchOrder = courseOfferingSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.CourseOfferingSearchOrder getCourseOfferingSearchOrder() {
	return (this.courseOfferingSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given course offering search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a course offering implementing the requested record.
     *
     *  @param courseOfferingSearchRecordType a course offering search record
     *         type
     *  @return the course offering search record
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseOfferingSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingSearchRecord getCourseOfferingSearchRecord(org.osid.type.Type courseOfferingSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.records.CourseOfferingSearchRecord record : this.records) {
            if (record.implementsRecordType(courseOfferingSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseOfferingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course offering search. 
     *
     *  @param courseOfferingSearchRecord course offering search record
     *  @param courseOfferingSearchRecordType courseOffering search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseOfferingSearchRecord(org.osid.course.records.CourseOfferingSearchRecord courseOfferingSearchRecord, 
                                           org.osid.type.Type courseOfferingSearchRecordType) {

        addRecordType(courseOfferingSearchRecordType);
        this.records.add(courseOfferingSearchRecord);        
        return;
    }
}
