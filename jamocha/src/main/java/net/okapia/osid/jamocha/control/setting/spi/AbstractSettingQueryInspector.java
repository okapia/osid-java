//
// AbstractSettingQueryInspector.java
//
//     A template for making a SettingQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.setting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for settings.
 */

public abstract class AbstractSettingQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.control.SettingQueryInspector {

    private final java.util.Collection<org.osid.control.records.SettingQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Gets the on query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOnTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the off query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOffTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the variable percentage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariablePercentageTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the variable amount query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariableAmountTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the discreet state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDiscreetStateIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the discreet state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getDiscreetStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the ramp rate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRampRateTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given setting query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a setting implementing the requested record.
     *
     *  @param settingRecordType a setting record type
     *  @return the setting query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(settingRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingQueryInspectorRecord getSettingQueryInspectorRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SettingQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(settingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this setting query. 
     *
     *  @param settingQueryInspectorRecord setting query inspector
     *         record
     *  @param settingRecordType setting record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSettingQueryInspectorRecord(org.osid.control.records.SettingQueryInspectorRecord settingQueryInspectorRecord, 
                                                   org.osid.type.Type settingRecordType) {

        addRecordType(settingRecordType);
        nullarg(settingRecordType, "setting record type");
        this.records.add(settingQueryInspectorRecord);        
        return;
    }
}
