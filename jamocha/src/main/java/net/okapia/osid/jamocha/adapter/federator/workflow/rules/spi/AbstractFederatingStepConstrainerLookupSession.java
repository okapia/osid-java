//
// AbstractFederatingStepConstrainerLookupSession.java
//
//     An abstract federating adapter for a StepConstrainerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  StepConstrainerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingStepConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.workflow.rules.StepConstrainerLookupSession>
    implements org.osid.workflow.rules.StepConstrainerLookupSession {

    private boolean parallel = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();


    /**
     *  Constructs a new
     *  <code>AbstractFederatingStepConstrainerLookupSession</code>.
     */

    protected AbstractFederatingStepConstrainerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.workflow.rules.StepConstrainerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>StepConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStepConstrainers() {
        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            if (session.canLookupStepConstrainers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>StepConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepConstrainerView() {
        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            session.useComparativeStepConstrainerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>StepConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepConstrainerView() {
        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            session.usePlenaryStepConstrainerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            session.useFederatedOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            session.useIsolatedOfficeView();
        }

        return;
    }


    /**
     *  Only active step constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepConstrainerView() {
        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            session.useActiveStepConstrainerView();
        }

        return;
    }


    /**
     *  Active and inactive step constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepConstrainerView() {
        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            session.useAnyStatusStepConstrainerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>StepConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>StepConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>StepConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @param  stepConstrainerId <code>Id</code> of the
     *          <code>StepConstrainer</code>
     *  @return the step constrainer
     *  @throws org.osid.NotFoundException <code>stepConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainer getStepConstrainer(org.osid.id.Id stepConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            try {
                return (session.getStepConstrainer(stepConstrainerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(stepConstrainerId + " not found");
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stepConstrainers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>StepConstrainers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param  stepConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByIds(org.osid.id.IdList stepConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.workflow.rules.stepconstrainer.MutableStepConstrainerList ret = new net.okapia.osid.jamocha.workflow.rules.stepconstrainer.MutableStepConstrainerList();

        try (org.osid.id.IdList ids = stepConstrainerIds) {
            while (ids.hasNext()) {
                ret.addStepConstrainer(getStepConstrainer(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the
     *  given step constrainer genus <code>Type</code> which does not
     *  include step constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepconstrainer.FederatingStepConstrainerList ret = getStepConstrainerList();

        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            ret.addStepConstrainerList(session.getStepConstrainersByGenusType(stepConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the
     *  given step constrainer genus <code>Type</code> and include any
     *  additional step constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByParentGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepconstrainer.FederatingStepConstrainerList ret = getStepConstrainerList();

        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            ret.addStepConstrainerList(session.getStepConstrainersByParentGenusType(stepConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StepConstrainerList</code> containing the given
     *  step constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known step
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param  stepConstrainerRecordType a stepConstrainer record type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByRecordType(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepconstrainer.FederatingStepConstrainerList ret = getStepConstrainerList();

        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            ret.addStepConstrainerList(session.getStepConstrainersByRecordType(stepConstrainerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>StepConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @return a list of <code>StepConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepconstrainer.FederatingStepConstrainerList ret = getStepConstrainerList();

        for (org.osid.workflow.rules.StepConstrainerLookupSession session : getSessions()) {
            ret.addStepConstrainerList(session.getStepConstrainers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepconstrainer.FederatingStepConstrainerList getStepConstrainerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepconstrainer.ParallelStepConstrainerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepconstrainer.CompositeStepConstrainerList());
        }
    }
}
