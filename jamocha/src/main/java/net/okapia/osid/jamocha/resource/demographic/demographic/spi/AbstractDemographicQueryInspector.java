//
// AbstractDemographicQueryInspector.java
//
//     A template for making a DemographicQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for demographics.
 */

public abstract class AbstractDemographicQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.resource.demographic.DemographicQueryInspector {

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the included demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedDemographicIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the included demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getIncludedDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the intersecting demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedIntersectingDemographicIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the intersecting demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getIncludedIntersectingDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the exclsuive demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedExclusiveDemographicIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the exclsuive demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getIncludedExclusiveDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the excluded demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExcludedDemographicIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the excluded demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getExcludedDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the included resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIncludedResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the included resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getIncludedResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the excluded resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExcludedResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the excluded resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getExcludedResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the resulting resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultingResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resulting resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getResultingResourceTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given demographic query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a demographic implementing the requested record.
     *
     *  @param demographicRecordType a demographic record type
     *  @return the demographic query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicQueryInspectorRecord getDemographicQueryInspectorRecord(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicRecordType + " is not supported");
    }


    /**
     *  Adds a record to this demographic query. 
     *
     *  @param demographicQueryInspectorRecord demographic query inspector
     *         record
     *  @param demographicRecordType demographic record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDemographicQueryInspectorRecord(org.osid.resource.demographic.records.DemographicQueryInspectorRecord demographicQueryInspectorRecord, 
                                                   org.osid.type.Type demographicRecordType) {

        addRecordType(demographicRecordType);
        nullarg(demographicRecordType, "demographic record type");
        this.records.add(demographicQueryInspectorRecord);        
        return;
    }
}
