//
// AbstractControllerLookupSession.java
//
//    A starter implementation framework for providing a Controller
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Controller
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getControllers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractControllerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.ControllerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();
    

    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Controller</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupControllers() {
        return (true);
    }


    /**
     *  A complete view of the <code>Controller</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeControllerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Controller</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryControllerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include controllers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active controllers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveControllerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive controllers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusControllerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Controller</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Controller</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Controller</code> and
     *  retained for compatibility.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  controllerId <code>Id</code> of the
     *          <code>Controller</code>
     *  @return the controller
     *  @throws org.osid.NotFoundException <code>controllerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>controllerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Controller getController(org.osid.id.Id controllerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.ControllerList controllers = getControllers()) {
            while (controllers.hasNext()) {
                org.osid.control.Controller controller = controllers.getNextController();
                if (controller.getId().equals(controllerId)) {
                    return (controller);
                }
            }
        } 

        throw new org.osid.NotFoundException(controllerId + " not found");
    }


    /**
     *  Gets a <code>ControllerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  controllers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Controllers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>controllerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByIds(org.osid.id.IdList controllerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.control.Controller> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = controllerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getController(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("controller " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.control.controller.LinkedControllerList(ret));
    }


    /**
     *  Gets a <code>ControllerList</code> corresponding to the given
     *  controller genus <code>Type</code> which does not include
     *  controllers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerGenusType a controller genus type 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByGenusType(org.osid.type.Type controllerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.controller.ControllerGenusFilterList(getControllers(), controllerGenusType));
    }


    /**
     *  Gets a <code>ControllerList</code> corresponding to the given
     *  controller genus <code>Type</code> and include any additional
     *  controllers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerGenusType a controller genus type 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByParentGenusType(org.osid.type.Type controllerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getControllersByGenusType(controllerGenusType));
    }


    /**
     *  Gets a <code>ControllerList</code> containing the given
     *  controller record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerRecordType a controller record type 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByRecordType(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.controller.ControllerRecordFilterList(getControllers(), controllerRecordType));
    }


    /**
     *  Gets a <code> ControllerList </code> with the given
     *  address.
     *
     *   In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  address a controller address
     *  @return the returned <code> Controller </code> list
     *  @throws org.osid.NullArgumentException <code> address </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByAddress(String address)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.controller.ControllerFilterList(new AddressFilter(address), getControllers()));
    }



    /**
     *  Gets all <code>Controllers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @return a list of <code>Controllers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.control.ControllerList getControllers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the controller list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of controllers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.control.ControllerList filterControllersOnViews(org.osid.control.ControllerList list)
        throws org.osid.OperationFailedException {

        org.osid.control.ControllerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.control.controller.ActiveControllerFilterList(ret);
        }

        return (ret);
    }


    public static class AddressFilter
        implements net.okapia.osid.jamocha.inline.filter.control.controller.ControllerFilter {
         
        private final String address;
         
         
        /**
         *  Constructs a new <code>AddressFilter</code>.
         *
         *  @param address the address to filter
         *  @throws org.osid.NullArgumentException
         *          <code>address</code> is <code>null</code>
         */
        
        public AddressFilter(String address) {
            nullarg(address, "address");
            this.address = address;
            return;
        }

         
        /**
         *  Used by the ControllerFilterList to filter the controller
         *  list based on address.
         *
         *  @param controller the controller
         *  @return <code>true</code> to pass the controller,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.control.Controller controller) {
            return (controller.getAddress().equals(this.address));
        }
    }        
}
