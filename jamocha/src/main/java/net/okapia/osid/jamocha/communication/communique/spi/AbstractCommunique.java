//
// AbstractCommunique.java
//
//     Defines a Communique.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.communication.communique.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Communique</code>.
 */

public abstract class AbstractCommunique
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.communication.Communique {

    private org.osid.locale.DisplayText message;
    private org.osid.communication.CommuniqueLevel level;
    private final java.util.Collection<org.osid.communication.ResponseOption> responseOptions = new java.util.LinkedHashSet<>();

    private boolean responseRequired;
    private boolean respondViaForm;

    private final java.util.Collection<org.osid.communication.records.CommuniqueRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the message. 
     *
     *  @return the message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.message);
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    protected void setMessage(org.osid.locale.DisplayText message) {
        nullarg(message, "message");
        this.message = message;
        return;
    }


    /**
     *  Gets the message level for this communique. 
     *
     *  @return the message level 
     */

    @OSID @Override
    public org.osid.communication.CommuniqueLevel getLevel() {
        return (this.level);
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    protected void setLevel(org.osid.communication.CommuniqueLevel level) {
        nullarg(level, "level");
        this.level = level;
        return;
    }


    /**
     *  Tests if the provider is blocking for a response. A response
     *  may take the form of s simple acknowledgement, a selection
     *  among a list of options, or a form input.
     *
     *  @return <code> true </code> if the provider is blocking for a
     *          response <code> false </code> if no response is
     *          required
     */

    @OSID @Override
    public boolean isResponseRequired() {
        return (this.responseRequired);
    }


    /**
     *  Sets the response required flag.
     *
     *  @param responseRequired {@code true} if a response is
     *         required, {@code false} otherwise
     */

    protected void setResponseRequired(boolean responseRequired) {
        this.responseRequired = responseRequired;
        return;
    }


    /**
     *  Tests if the provider is blocking on a choice selection. If
     *  <code> respondViaForm() </code> is <code> true, </code> then
     *  this method must return <code> false. </code> If <code>
     *  isResponseRequired() </code> is <code> false </code> and
     *  <code> respondViaOption() </code> is <code> true, </code> then
     *  a response is optional.
     *
     *  @return <code> true </code> if the provider accepts a
     *          selection input, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean respondViaOption() {
        return (this.responseOptions.size() > 0);
    }


    /**
     *  Gets a list of possible response choices to this communique. 
     *
     *  @return a list of possible responses 
     *  @throws org.osid.IllegalStateException <code> respondViaOption() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.communication.ResponseOption[] getResponseOptions() {
        if (!respondViaOption()) {
            throw new org.osid.IllegalStateException("respondViaOption() is false");
        }

        return (this.responseOptions.toArray(new org.osid.communication.ResponseOption[this.responseOptions.size()]));
    }


    /**
     *  Sets the response options.
     *
     *  @param responseOptions the response options
     *  @throws org.osid.NullArgumentException
     *          <code>responseOptions</code> is <code>null</code>
     */

    protected void setResponseOptions(java.util.Collection<org.osid.communication.ResponseOption> responseOptions) {
        nullarg(responseOptions, "response options");

        this.responseOptions.clear();
        this.respondViaForm = false;
        this.responseOptions.addAll(responseOptions);

        return;
    }


    /**
     *  Adds a response option.
     *
     *  @param responseOption a response option
     *  @throws org.osid.NullArgumentException
     *          <code>responseOption</code> is <code>null</code>
     */

    protected void addResponseOption(org.osid.communication.ResponseOption responseOption) {
        nullarg(responseOption, "response option");

        this.responseOptions.add(responseOption);
        this.respondViaForm = false;

        return;
    }


    /**
     *  Clears the response options.
     */

    protected void clearResponseOptions() {
        this.responseOptions.clear();
        return;
    }


    /**
     *  Tests if the provider is blocking on a form input. If <code>
     *  respondViaOption() </code> is <code> true, </code> then this
     *  method must return <code> false </code> . If <code>
     *  isResponseRequired() </code> is false and <code>
     *  respondViaForm() </code> is <code> true </code> , then a
     *  response is optional.
     *
     *  @return <code> true </code> if the provider accepts form
     *          input, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean respondViaForm() {
        return (this.respondViaForm);
    }


    /**
     *  Sets the respond via form flag.
     *
     *  @param respondViaForm {@code true} if the response should use
     *         a form, @code false} otherwise
     */

    protected void setRespondViaForm(boolean respondViaForm) {
        this.respondViaForm = respondViaForm;
        this.responseOptions.clear();
        return;
    }


    /**
     *  Tests if this communique supports the given record
     *  <code>Type</code>.
     *
     *  @param  communiqueRecordType a communique record type 
     *  @return <code>true</code> if the communiqueRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>communiqueRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type communiqueRecordType) {
        for (org.osid.communication.records.CommuniqueRecord record : this.records) {
            if (record.implementsRecordType(communiqueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  communiqueRecordType the communique record type 
     *  @return the communique record 
     *  @throws org.osid.NullArgumentException
     *          <code>communiqueRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(communiqueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.communication.records.CommuniqueRecord getCommuniqueRecord(org.osid.type.Type communiqueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.communication.records.CommuniqueRecord record : this.records) {
            if (record.implementsRecordType(communiqueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(communiqueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this communique. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param communiqueRecord the communique record
     *  @param communiqueRecordType communique record type
     *  @throws org.osid.NullArgumentException
     *          <code>communiqueRecord</code> or
     *          <code>communiqueRecordTypecommunique</code> is
     *          <code>null</code>
     */
            
    protected void addCommuniqueRecord(org.osid.communication.records.CommuniqueRecord communiqueRecord, 
                                       org.osid.type.Type communiqueRecordType) {

        addRecordType(communiqueRecordType);
        this.records.add(communiqueRecord);
        
        return;
    }
}
