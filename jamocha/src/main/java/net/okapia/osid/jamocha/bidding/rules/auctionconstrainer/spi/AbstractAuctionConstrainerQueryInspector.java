//
// AbstractAuctionConstrainerQueryInspector.java
//
//     A template for making an AuctionConstrainerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for auction constrainers.
 */

public abstract class AbstractAuctionConstrainerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQueryInspector
    implements org.osid.bidding.rules.AuctionConstrainerQueryInspector {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the auction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuctionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the auction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQueryInspector[] getRuledAuctionTerms() {
        return (new org.osid.bidding.AuctionQueryInspector[0]);
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given auction constrainer query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an auction constrainer implementing the requested record.
     *
     *  @param auctionConstrainerRecordType an auction constrainer record type
     *  @return the auction constrainer query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord getAuctionConstrainerQueryInspectorRecord(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(auctionConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction constrainer query. 
     *
     *  @param auctionConstrainerQueryInspectorRecord auction constrainer query inspector
     *         record
     *  @param auctionConstrainerRecordType auctionConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionConstrainerQueryInspectorRecord(org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord auctionConstrainerQueryInspectorRecord, 
                                                   org.osid.type.Type auctionConstrainerRecordType) {

        addRecordType(auctionConstrainerRecordType);
        nullarg(auctionConstrainerRecordType, "auction constrainer record type");
        this.records.add(auctionConstrainerQueryInspectorRecord);        
        return;
    }
}
