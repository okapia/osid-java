//
// InvariantMapProxyQueueConstrainerLookupSession
//
//    Implements a QueueConstrainer lookup service backed by a fixed
//    collection of queueConstrainers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a QueueConstrainer lookup service backed by a fixed
 *  collection of queue constrainers. The queue constrainers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyQueueConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapQueueConstrainerLookupSession
    implements org.osid.provisioning.rules.QueueConstrainerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyQueueConstrainerLookupSession} with no
     *  queue constrainers.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyQueueConstrainerLookupSession} with a single
     *  queue constrainer.
     *
     *  @param distributor the distributor
     *  @param queueConstrainer a single queue constrainer
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code queueConstrainer} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.rules.QueueConstrainer queueConstrainer, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putQueueConstrainer(queueConstrainer);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyQueueConstrainerLookupSession} using
     *  an array of queue constrainers.
     *
     *  @param distributor the distributor
     *  @param queueConstrainers an array of queue constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code queueConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.rules.QueueConstrainer[] queueConstrainers, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putQueueConstrainers(queueConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyQueueConstrainerLookupSession} using a
     *  collection of queue constrainers.
     *
     *  @param distributor the distributor
     *  @param queueConstrainers a collection of queue constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code queueConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  java.util.Collection<? extends org.osid.provisioning.rules.QueueConstrainer> queueConstrainers,
                                                  org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putQueueConstrainers(queueConstrainers);
        return;
    }
}
