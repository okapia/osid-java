//
// AbstractAdapterDirectoryLookupSession.java
//
//    A Directory lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.filing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Directory lookup session adapter.
 */

public abstract class AbstractAdapterDirectoryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.filing.DirectoryLookupSession {

    private final org.osid.filing.DirectoryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDirectoryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDirectoryLookupSession(org.osid.filing.DirectoryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the <code> Id </code> of this directory. 
     *
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.id.Id getDirectoryId() {
        return (this.session.getDirectoryId());
    }


    /**
     *  Gets the directory associated with this session. 
     *
     *  @return the directory associated with this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectory());
    }


    /**
     *  Tests if this user can perform {@code Directory} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDirectories() {
        return (this.session.canLookupDirectories());
    }


    /**
     *  A complete view of the {@code Directory} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDirectoryView() {
        this.session.useComparativeDirectoryView();
        return;
    }


    /**
     *  A complete view of the {@code Directory} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDirectoryView() {
        this.session.usePlenaryDirectoryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in directories which are children of
     *  this directory.
     */

    @OSID @Override
    public void useFederatedDirectoryView() {
        this.session.useFederatedDirectoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this directory only.
     */

    @OSID @Override
    public void useIsolatedDirectoryView() {
        this.session.useIsolatedDirectoryView();
        return;
    }


    /**
     *  Gets the {@code Directory} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Directory} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Directory} and
     *  retained for compatibility.
     *
     *  @param directoryId {@code Id} of the {@code Directory}
     *  @return the directory
     *  @throws org.osid.NotFoundException {@code directoryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code directoryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectory(directoryId));
    }


    /**
     *  Gets a {@code DirectoryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  directories specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Directories} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  directoryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Directory} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code directoryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByIds(org.osid.id.IdList directoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectoriesByIds(directoryIds));
    }


    /**
     *  Gets a {@code DirectoryList} corresponding to the given
     *  directory genus {@code Type} which does not include
     *  directories of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned {@code Directory} list
     *  @throws org.osid.NullArgumentException
     *          {@code directoryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectoriesByGenusType(directoryGenusType));
    }


    /**
     *  Gets a {@code DirectoryList} corresponding to the given
     *  directory genus {@code Type} and include any additional
     *  directories with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned {@code Directory} list
     *  @throws org.osid.NullArgumentException
     *          {@code directoryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByParentGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectoriesByParentGenusType(directoryGenusType));
    }


    /**
     *  Gets a {@code DirectoryList} containing the given
     *  directory record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directoryRecordType a directory record type 
     *  @return the returned {@code Directory} list
     *  @throws org.osid.NullArgumentException
     *          {@code directoryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByRecordType(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectoriesByRecordType(directoryRecordType));
    }


    /**
     *  Gets a {@code DirectoryList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Directory} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectoriesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Directories}. 
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Directories} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectories());
    }
}
