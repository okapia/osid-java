//
// AbstractCourseRequisiteProxyManager.java
//
//     An adapter for a CourseRequisiteProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseRequisiteProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseRequisiteProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.course.requisite.CourseRequisiteProxyManager>
    implements org.osid.course.requisite.CourseRequisiteProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseRequisiteProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseRequisiteProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseRequisiteProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseRequisiteProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up requisites is supported. 
     *
     *  @return <code> true </code> if requisite lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteLookup() {
        return (getAdapteeManager().supportsRequisiteLookup());
    }


    /**
     *  Tests if querying requisites is supported. 
     *
     *  @return <code> true </code> if requisite query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteQuery() {
        return (getAdapteeManager().supportsRequisiteQuery());
    }


    /**
     *  Tests if searching requisites is supported. 
     *
     *  @return <code> true </code> if requisite search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteSearch() {
        return (getAdapteeManager().supportsRequisiteSearch());
    }


    /**
     *  Tests if requisite administrative service is supported. 
     *
     *  @return <code> true </code> if requisite administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteAdmin() {
        return (getAdapteeManager().supportsRequisiteAdmin());
    }


    /**
     *  Tests if a requisite <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if requisite notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteNotification() {
        return (getAdapteeManager().supportsRequisiteNotification());
    }


    /**
     *  Tests if a requisite cataloging service is supported. 
     *
     *  @return <code> true </code> if requisite cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteCourseCatalog() {
        return (getAdapteeManager().supportsRequisiteCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps requisites to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteCourseCatalogAssignment() {
        return (getAdapteeManager().supportsRequisiteCourseCatalogAssignment());
    }


    /**
     *  Tests if a requisite smart course catalog session is available. 
     *
     *  @return <code> true </code> if a requisite smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteSmartCourseCatalog() {
        return (getAdapteeManager().supportsRequisiteSmartCourseCatalog());
    }


    /**
     *  Gets the supported <code> Requisite </code> record types. 
     *
     *  @return a list containing the supported <code> Requisite </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequisiteRecordTypes() {
        return (getAdapteeManager().getRequisiteRecordTypes());
    }


    /**
     *  Tests if the given <code> Requisite </code> record type is supported. 
     *
     *  @param  requisiteRecordType a <code> Type </code> indicating a <code> 
     *          Requisite </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requisiteRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequisiteRecordType(org.osid.type.Type requisiteRecordType) {
        return (getAdapteeManager().supportsRequisiteRecordType(requisiteRecordType));
    }


    /**
     *  Gets the supported <code> Requisite </code> search record types. 
     *
     *  @return a list containing the supported <code> Requisite </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequisiteSearchRecordTypes() {
        return (getAdapteeManager().getRequisiteSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Requisite </code> search record type is 
     *  supported. 
     *
     *  @param  requisiteSearchRecordType a <code> Type </code> indicating a 
     *          <code> Requisite </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          requisiteSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequisiteSearchRecordType(org.osid.type.Type requisiteSearchRecordType) {
        return (getAdapteeManager().supportsRequisiteSearchRecordType(requisiteSearchRecordType));
    }


    /**
     *  Gets the supported <code> CourseRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> CourseRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseRequirementRecordTypes() {
        return (getAdapteeManager().getCourseRequirementRecordTypes());
    }


    /**
     *  Tests if the given <code> CourseRequirement </code> record type is 
     *  supported. 
     *
     *  @param  courseRequirementRecordType a <code> Type </code> indicating a 
     *          <code> CourseRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseRequirementRecordType(org.osid.type.Type courseRequirementRecordType) {
        return (getAdapteeManager().supportsCourseRequirementRecordType(courseRequirementRecordType));
    }


    /**
     *  Gets the supported <code> ProgramRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> ProgramRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramRequirementRecordTypes() {
        return (getAdapteeManager().getProgramRequirementRecordTypes());
    }


    /**
     *  Tests if the given <code> ProgramRequirement </code> record type is 
     *  supported. 
     *
     *  @param  programRequirementRecordType a <code> Type </code> indicating 
     *          a <code> ProgramRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramRequirementRecordType(org.osid.type.Type programRequirementRecordType) {
        return (getAdapteeManager().supportsProgramRequirementRecordType(programRequirementRecordType));
    }


    /**
     *  Gets the supported <code> CredentialRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> CredentialRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialRequirementRecordTypes() {
        return (getAdapteeManager().getCredentialRequirementRecordTypes());
    }


    /**
     *  Tests if the given <code> CredentialRequirement </code> record type is 
     *  supported. 
     *
     *  @param  credentialRequirementRecordType a <code> Type </code> 
     *          indicating a <code> CredentialRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialRequirementRecordType(org.osid.type.Type credentialRequirementRecordType) {
        return (getAdapteeManager().supportsCredentialRequirementRecordType(credentialRequirementRecordType));
    }


    /**
     *  Gets the supported <code> LearningObjectiveRequirement </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          LearningObjectiveRequirement </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLearningObjectiveRequirementRecordTypes() {
        return (getAdapteeManager().getLearningObjectiveRequirementRecordTypes());
    }


    /**
     *  Tests if the given <code> LearningObjectiveRequirement </code> record 
     *  type is supported. 
     *
     *  @param  learningObjectiveRequirementRecordType a <code> Type </code> 
     *          indicating a <code> LearningObjectivelRequirement </code> 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          learningObjectiveRequirementRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveRequirementRecordType(org.osid.type.Type learningObjectiveRequirementRecordType) {
        return (getAdapteeManager().supportsLearningObjectiveRequirementRecordType(learningObjectiveRequirementRecordType));
    }


    /**
     *  Gets the supported <code> AsessmentRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentRequirementRecordTypes() {
        return (getAdapteeManager().getAssessmentRequirementRecordTypes());
    }


    /**
     *  Tests if the given <code> AsessmentRequirement </code> record type is 
     *  supported. 
     *
     *  @param  assessmentRequirementRecordType a <code> Type </code> 
     *          indicating an <code> AssessmentRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentRequirementRecordType(org.osid.type.Type assessmentRequirementRecordType) {
        return (getAdapteeManager().supportsAssessmentRequirementRecordType(assessmentRequirementRecordType));
    }


    /**
     *  Gets the supported <code> AsessmentRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> AwardRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardRequirementRecordTypes() {
        return (getAdapteeManager().getAwardRequirementRecordTypes());
    }


    /**
     *  Tests if the given <code> AsessmentRequirement </code> record type is 
     *  supported. 
     *
     *  @param  awardRequirementRecordType a <code> Type </code> indicating an 
     *          <code> AwardRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          awardRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardRequirementRecordType(org.osid.type.Type awardRequirementRecordType) {
        return (getAdapteeManager().supportsAwardRequirementRecordType(awardRequirementRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteLookupSession getRequisiteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteLookupSession getRequisiteLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuerySession getRequisiteQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuerySession getRequisiteQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSearchSession getRequisiteSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSearchSession getRequisiteSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteSearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteAdminSession getRequisiteAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteAdminSession getRequisiteAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  notification service. 
     *
     *  @param  requisiteReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> requisiteReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteNotificationSession getRequisiteNotificationSession(org.osid.course.requisite.RequisiteReceiver requisiteReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteNotificationSession(requisiteReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  notification service for the given course catalog. 
     *
     *  @param  requisiteReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> requisiteReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteNotificationSession getRequisiteNotificationSessionForCourseCatalog(org.osid.course.requisite.RequisiteReceiver requisiteReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteNotificationSessionForCourseCatalog(requisiteReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup requisite/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteCourseCatalogSession getRequisiteCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteCourseCatalogSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  requisites to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteCourseCatalogAssignmentSession getRequisiteCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSmartCourseCatalogSession getRequisiteSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequisiteSmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
