//
// AbstractDynamicOrchestrationManager.java
//
//     A dynamic Java adapter for a Orchestration OSID Provider.
//
//
// Tom Coppeto
// Okapia
// 30 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.orchestration.spi;


/**
 *  A dynamic Java adapter template for a Orchestration OSID Provider. A
 *  single Orchestration OSID Provider is adapted through overriding the
 *  {@code invoke()} method and supplying the name of the adaptee
 *  through {@code initialize()}.
 */

public abstract class AbstractDynamicOrchestrationManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractDynamicOsidManager
    implements java.lang.reflect.InvocationHandler {


    /**
     *  Constructs a new {@code AbstractDynamicOrchestrationManager}.
     *
     *  @param provider the provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractDynamicOrchestrationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Starts this adapter. This method is called from the {@code
     *  initialize()} method of a subclass.
     *
     *  @param  implementation the name of the Orchestration OSID Provider
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized
     *  @throws org.osid.NotFoundException {@code implentation} not found
     *  @throws org.osid.NullArgumentException {@code implementation}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected void start(String implementation)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        super.start(org.osid.OSID.ORCHESTRATION, implementation);
        return;
    }


    /**
     *  Starts this adapter for proxy managers. This method is called
     *  from the {@code initialize()} method of a subclass.
     *
     *  @param  implementation the name of the Orchestration OSID Provider
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.NullArgumentException {@code implementation}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected void startProxy(String implementation)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        super.startProxy(org.osid.OSID.ORCHESTRATION, implementation);
        return;
    }
}
