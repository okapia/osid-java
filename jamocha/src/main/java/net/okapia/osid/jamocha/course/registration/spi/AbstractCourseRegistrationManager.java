//
// AbstractCourseRegistrationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCourseRegistrationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.registration.CourseRegistrationManager,
               org.osid.course.registration.CourseRegistrationProxyManager {

    private final Types activityBundleRecordTypes          = new TypeRefSet();
    private final Types activityBundleSearchRecordTypes    = new TypeRefSet();

    private final Types registrationRecordTypes            = new TypeRefSet();
    private final Types registrationSearchRecordTypes      = new TypeRefSet();

    private final Types activityRegistrationRecordTypes    = new TypeRefSet();
    private final Types activityRegistrationSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractCourseRegistrationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCourseRegistrationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up activity bundles is supported. 
     *
     *  @return <code> true </code> if activity bundle lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleLookup() {
        return (false);
    }


    /**
     *  Tests if querying activity bundles is supported. 
     *
     *  @return <code> true </code> if activity bundle query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleQuery() {
        return (false);
    }


    /**
     *  Tests if searching activity bundles is supported. 
     *
     *  @return <code> true </code> if activity bundle search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleSearch() {
        return (false);
    }


    /**
     *  Tests if an activity bundle <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity bundle administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleAdmin() {
        return (false);
    }


    /**
     *  Tests if an activity bundle <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity bundle notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleNotification() {
        return (false);
    }


    /**
     *  Tests if an activity bundle cataloging service is supported. 
     *
     *  @return <code> true </code> if activity bundle catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if an activity bundle cataloging service is supported. A 
     *  cataloging service maps activity bundles to catalogs. 
     *
     *  @return <code> true </code> if activity bundle cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if an activity bundle smart course catalog session is available. 
     *
     *  @return <code> true </code> if an activity bundle smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up registrations is supported. 
     *
     *  @return <code> true </code> if registration lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationLookup() {
        return (false);
    }


    /**
     *  Tests if querying registrations is supported. 
     *
     *  @return <code> true </code> if registration query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationQuery() {
        return (false);
    }


    /**
     *  Tests if searching registrations is supported. 
     *
     *  @return <code> true </code> if registration search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationSearch() {
        return (false);
    }


    /**
     *  Tests if course <code> </code> offering <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if registration administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationAdmin() {
        return (false);
    }


    /**
     *  Tests if a registration <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if registration notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationNotification() {
        return (false);
    }


    /**
     *  Tests if a registration cataloging service is supported. 
     *
     *  @return <code> true </code> if registration catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a registration cataloging service is supported. A cataloging 
     *  service maps registrations to catalogs. 
     *
     *  @return <code> true </code> if registration cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a registration smart course catalog session is available. 
     *
     *  @return <code> true </code> if a registration smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up activity registrations is supported. 
     *
     *  @return <code> true </code> if activity registration lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationLookup() {
        return (false);
    }


    /**
     *  Tests if querying activity registrations is supported. 
     *
     *  @return <code> true </code> if activity registration query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationQuery() {
        return (false);
    }


    /**
     *  Tests if searching activity registrations is supported. 
     *
     *  @return <code> true </code> if activity registration search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationSearch() {
        return (false);
    }


    /**
     *  Tests if an activity registration <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if activity registration administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationAdmin() {
        return (false);
    }


    /**
     *  Tests if an activity registration <code> </code> notification service 
     *  is supported. 
     *
     *  @return <code> true </code> if activity registration notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationNotification() {
        return (false);
    }


    /**
     *  Tests if an activity registration cataloging service is supported. 
     *
     *  @return <code> true </code> if activity registration catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if an activity registration cataloging service is supported. A 
     *  cataloging service maps activity registrations to catalogs. 
     *
     *  @return <code> true </code> if activity registration cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if an activity registration smart course catalog session is 
     *  available. 
     *
     *  @return <code> true </code> if an activity registration smart course 
     *          catalog session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course registration batch service is available. 
     *
     *  @return <code> true </code> if a course registration service session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseRegistrationBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> ActivityBundle </code> record types. 
     *
     *  @return a list containing the supported <code> ActivityBundle </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityBundleRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityBundleRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActivityBundle </code> record type is 
     *  supported. 
     *
     *  @param  activityBundleRecordType a <code> Type </code> indicating an 
     *          <code> ActivityBundle </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityBundleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityBundleRecordType(org.osid.type.Type activityBundleRecordType) {
        return (this.activityBundleRecordTypes.contains(activityBundleRecordType));
    }


    /**
     *  Adds support for an activity bundle record type.
     *
     *  @param activityBundleRecordType an activity bundle record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityBundleRecordType</code> is <code>null</code>
     */

    protected void addActivityBundleRecordType(org.osid.type.Type activityBundleRecordType) {
        this.activityBundleRecordTypes.add(activityBundleRecordType);
        return;
    }


    /**
     *  Removes support for an activity bundle record type.
     *
     *  @param activityBundleRecordType an activity bundle record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityBundleRecordType</code> is <code>null</code>
     */

    protected void removeActivityBundleRecordType(org.osid.type.Type activityBundleRecordType) {
        this.activityBundleRecordTypes.remove(activityBundleRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActivityBundle </code> search record types. 
     *
     *  @return a list containing the supported <code> ActivityBundle </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityBundleSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityBundleSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActivityBundle </code> search record type is 
     *  supported. 
     *
     *  @param  activityBundleSearchRecordType a <code> Type </code> 
     *          indicating an <code> ActivityBundle </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityBundleSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityBundleSearchRecordType(org.osid.type.Type activityBundleSearchRecordType) {
        return (this.activityBundleSearchRecordTypes.contains(activityBundleSearchRecordType));
    }


    /**
     *  Adds support for an activity bundle search record type.
     *
     *  @param activityBundleSearchRecordType an activity bundle search record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityBundleSearchRecordType</code> is <code>null</code>
     */

    protected void addActivityBundleSearchRecordType(org.osid.type.Type activityBundleSearchRecordType) {
        this.activityBundleSearchRecordTypes.add(activityBundleSearchRecordType);
        return;
    }


    /**
     *  Removes support for an activity bundle search record type.
     *
     *  @param activityBundleSearchRecordType an activity bundle search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityBundleSearchRecordType</code> is <code>null</code>
     */

    protected void removeActivityBundleSearchRecordType(org.osid.type.Type activityBundleSearchRecordType) {
        this.activityBundleSearchRecordTypes.remove(activityBundleSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Registration </code> record types. 
     *
     *  @return a list containing the supported <code> Registration </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRegistrationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.registrationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Registration </code> record type is 
     *  supported. 
     *
     *  @param  registrationRecordType a <code> Type </code> indicating an 
     *          <code> Registration </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> registrationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRegistrationRecordType(org.osid.type.Type registrationRecordType) {
        return (this.registrationRecordTypes.contains(registrationRecordType));
    }


    /**
     *  Adds support for a registration record type.
     *
     *  @param registrationRecordType a registration record type
     *  @throws org.osid.NullArgumentException
     *  <code>registrationRecordType</code> is <code>null</code>
     */

    protected void addRegistrationRecordType(org.osid.type.Type registrationRecordType) {
        this.registrationRecordTypes.add(registrationRecordType);
        return;
    }


    /**
     *  Removes support for a registration record type.
     *
     *  @param registrationRecordType a registration record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>registrationRecordType</code> is <code>null</code>
     */

    protected void removeRegistrationRecordType(org.osid.type.Type registrationRecordType) {
        this.registrationRecordTypes.remove(registrationRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Registration </code> search record types. 
     *
     *  @return a list containing the supported <code> Registration </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRegistrationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.registrationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Registration </code> search record type is 
     *  supported. 
     *
     *  @param  registrationSearchRecordType a <code> Type </code> indicating 
     *          an <code> Registration </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          registrationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRegistrationSearchRecordType(org.osid.type.Type registrationSearchRecordType) {
        return (this.registrationSearchRecordTypes.contains(registrationSearchRecordType));
    }


    /**
     *  Adds support for a registration search record type.
     *
     *  @param registrationSearchRecordType a registration search record type
     *  @throws org.osid.NullArgumentException
     *  <code>registrationSearchRecordType</code> is <code>null</code>
     */

    protected void addRegistrationSearchRecordType(org.osid.type.Type registrationSearchRecordType) {
        this.registrationSearchRecordTypes.add(registrationSearchRecordType);
        return;
    }


    /**
     *  Removes support for a registration search record type.
     *
     *  @param registrationSearchRecordType a registration search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>registrationSearchRecordType</code> is <code>null</code>
     */

    protected void removeRegistrationSearchRecordType(org.osid.type.Type registrationSearchRecordType) {
        this.registrationSearchRecordTypes.remove(registrationSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActivityRegistration </code> record types. 
     *
     *  @return a list containing the supported <code> ActivityRegistration 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRegistrationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityRegistrationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActivityRegistration </code> record type is 
     *  supported. 
     *
     *  @param  activityRegistrationRecordType a <code> Type </code> 
     *          indicating an <code> ActivityRegistration </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationRecordType(org.osid.type.Type activityRegistrationRecordType) {
        return (this.activityRegistrationRecordTypes.contains(activityRegistrationRecordType));
    }


    /**
     *  Adds support for an activity registration record type.
     *
     *  @param activityRegistrationRecordType an activity registration record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityRegistrationRecordType</code> is <code>null</code>
     */

    protected void addActivityRegistrationRecordType(org.osid.type.Type activityRegistrationRecordType) {
        this.activityRegistrationRecordTypes.add(activityRegistrationRecordType);
        return;
    }


    /**
     *  Removes support for an activity registration record type.
     *
     *  @param activityRegistrationRecordType an activity registration record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityRegistrationRecordType</code> is <code>null</code>
     */

    protected void removeActivityRegistrationRecordType(org.osid.type.Type activityRegistrationRecordType) {
        this.activityRegistrationRecordTypes.remove(activityRegistrationRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActivityRegistration </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> ActivityRegistration 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRegistrationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityRegistrationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActivityRegistration </code> search record 
     *  type is supported. 
     *
     *  @param  activityRegistrationSearchRecordType a <code> Type </code> 
     *          indicating an <code> ActivityRegistration </code> search 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationSearchRecordType(org.osid.type.Type activityRegistrationSearchRecordType) {
        return (this.activityRegistrationSearchRecordTypes.contains(activityRegistrationSearchRecordType));
    }


    /**
     *  Adds support for an activity registration search record type.
     *
     *  @param activityRegistrationSearchRecordType an activity registration search record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityRegistrationSearchRecordType</code> is <code>null</code>
     */

    protected void addActivityRegistrationSearchRecordType(org.osid.type.Type activityRegistrationSearchRecordType) {
        this.activityRegistrationSearchRecordTypes.add(activityRegistrationSearchRecordType);
        return;
    }


    /**
     *  Removes support for an activity registration search record type.
     *
     *  @param activityRegistrationSearchRecordType an activity registration search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityRegistrationSearchRecordType</code> is <code>null</code>
     */

    protected void removeActivityRegistrationSearchRecordType(org.osid.type.Type activityRegistrationSearchRecordType) {
        this.activityRegistrationSearchRecordTypes.remove(activityRegistrationSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle lookup service. 
     *
     *  @return an <code> ActivityBundleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleLookupSession getActivityBundleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleLookupSession getActivityBundleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityBundleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleLookupSession getActivityBundleLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleLookupSession getActivityBundleLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle query service. 
     *
     *  @return an <code> ActivityBundleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuerySession getActivityBundleQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuerySession getActivityBundleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuerySession getActivityBundleQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuerySession getActivityBundleQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle search service. 
     *
     *  @return an <code> ActivityBundleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSearchSession getActivityBundleSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSearchSession getActivityBundleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSearchSession getActivityBundleSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSearchSession getActivityBundleSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle administration service. 
     *
     *  @return an <code> ActivityBundleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleAdminSession getActivityBundleAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleAdminSession getActivityBundleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleAdminSession getActivityBundleAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleAdminSession getActivityBundleAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle notification service. 
     *
     *  @param  activityBundleReceiver the notification callback 
     *  @return an <code> ActivityBundleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityBundleReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleNotificationSession getActivityBundleNotificationSession(org.osid.course.registration.ActivityBundleReceiver activityBundleReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle notification service. 
     *
     *  @param  activityBundleReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityBundleReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleNotificationSession getActivityBundleNotificationSession(org.osid.course.registration.ActivityBundleReceiver activityBundleReceiver, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle notification service for the given course catalog. 
     *
     *  @param  activityBundleReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityBundleReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleNotificationSession getActivityBundleNotificationSessionForCourseCatalog(org.osid.course.registration.ActivityBundleReceiver activityBundleReceiver, 
                                                                                                                               org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle notification service for the given course catalog. 
     *
     *  @param  activityBundleReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityBundleReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleNotificationSession getActivityBundleNotificationSessionForCourseCatalog(org.osid.course.registration.ActivityBundleReceiver activityBundleReceiver, 
                                                                                                                               org.osid.id.Id courseCatalogId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity bundle/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityBundleCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleCourseCatalogSession getActivityBundleCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity bundle/course 
     *  catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleCourseCatalogSession getActivityBundleCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  bundles to course catalogs. 
     *
     *  @return an <code> ActivityBundleCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleCourseCatalogAssignmentSession getActivityBundleCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  bundles to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleCourseCatalogAssignmentSession getActivityBundleCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSmartCourseCatalogSession getActivityBundleSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityBundleSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityBundleSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSmartCourseCatalogSession getActivityBundleSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityBundleSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  lookup service. 
     *
     *  @return a <code> RegistrationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationLookupSession getRegistrationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RegistrationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationLookupSession getRegistrationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> RegistrationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationLookupSession getRegistrationLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> RegistrationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationLookupSession getRegistrationLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  query service. 
     *
     *  @return a <code> RegistrationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuerySession getRegistrationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RegistrationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuerySession getRegistrationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuerySession getRegistrationQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RegistrationQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuerySession getRegistrationQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  search service. 
     *
     *  @return a <code> RegistrationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchSession getRegistrationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RegistrationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchSession getRegistrationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchSession getRegistrationSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RegistrationSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchSession getRegistrationSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  administration service. 
     *
     *  @return a <code> RegistrationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationAdminSession getRegistrationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RegistrationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationAdminSession getRegistrationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationAdminSession getRegistrationAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RegistrationAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationAdminSession getRegistrationAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  notification service. 
     *
     *  @param  registrationReceiver the notification callback 
     *  @return a <code> RegistrationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> registrationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationNotificationSession getRegistrationNotificationSession(org.osid.course.registration.RegistrationReceiver registrationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  notification service. 
     *
     *  @param  registrationReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> RegistrationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> registrationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationNotificationSession getRegistrationNotificationSession(org.osid.course.registration.RegistrationReceiver registrationReceiver, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  notification service for the given course catalog. 
     *
     *  @param  registrationReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> registrationReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationNotificationSession getRegistrationNotificationSessionForCourseCatalog(org.osid.course.registration.RegistrationReceiver registrationReceiver, 
                                                                                                                           org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  notification service for the given course catalog. 
     *
     *  @param  registrationReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RegistrationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> registrationReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationNotificationSession getRegistrationNotificationSessionForCourseCatalog(org.osid.course.registration.RegistrationReceiver registrationReceiver, 
                                                                                                                           org.osid.id.Id courseCatalogId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup registration/catalog 
     *  mappings. 
     *
     *  @return a <code> RegistrationCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationCourseCatalogSession getRegistrationCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup registration/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RegistrationCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationCourseCatalogSession getRegistrationCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  registrations to course catalogs. 
     *
     *  @return a <code> RegistrationCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationCourseCatalogAssignmentSession getRegistrationCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  registrations to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RegistrationCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationCourseCatalogAssignmentSession getRegistrationCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSmartCourseCatalogSession getRegistrationSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getRegistrationSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RegistrationSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSmartCourseCatalogSession getRegistrationSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getRegistrationSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration lookup service. 
     *
     *  @return an <code> ActivityRegistrationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationLookupSession getActivityRegistrationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationLookupSession getActivityRegistrationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityRegistrationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationLookupSession getActivityRegistrationLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationLookupSession getActivityRegistrationLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration query service. 
     *
     *  @return an <code> ActivityRegistrationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationQuerySession getActivityRegistrationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationQuerySession getActivityRegistrationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationQuerySession getActivityRegistrationQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationQuerySession getActivityRegistrationQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration search service. 
     *
     *  @return an <code> ActivityRegistrationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSearchSession getActivityRegistrationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSearchSession getActivityRegistrationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSearchSession getActivityRegistrationSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSearchSession getActivityRegistrationSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration administration service. 
     *
     *  @return an <code> ActivityRegistrationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationAdminSession getActivityRegistrationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationAdminSession getActivityRegistrationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationAdminSession getActivityRegistrationAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationAdminSession getActivityRegistrationAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration notification service. 
     *
     *  @param  activityRegistrationReceiver the notification callback 
     *  @return an <code> ActivityRegistrationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationNotificationSession getActivityRegistrationNotificationSession(org.osid.course.registration.ActivityRegistrationReceiver activityRegistrationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration notification service. 
     *
     *  @param  activityRegistrationReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationNotificationSession getActivityRegistrationNotificationSession(org.osid.course.registration.ActivityRegistrationReceiver activityRegistrationReceiver, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration notification service for the given course catalog. 
     *
     *  @param  activityRegistrationReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationReceiver </code> or <code> courseCatalogId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationNotificationSession getActivityRegistrationNotificationSessionForCourseCatalog(org.osid.course.registration.ActivityRegistrationReceiver activityRegistrationReceiver, 
                                                                                                                                           org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration notification service for the given course catalog. 
     *
     *  @param  activityRegistrationReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationReceiver, courseCatalogId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationNotificationSession getActivityRegistrationNotificationSessionForCourseCatalog(org.osid.course.registration.ActivityRegistrationReceiver activityRegistrationReceiver, 
                                                                                                                                           org.osid.id.Id courseCatalogId, 
                                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity 
     *  registration/catalog mappings. 
     *
     *  @return an <code> ActivityRegistrationCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationCourseCatalog() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationCourseCatalogSession getActivityRegistrationCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity 
     *  registration/course catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationCourseCatalog() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationCourseCatalogSession getActivityRegistrationCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  registrations to course catalogs. 
     *
     *  @return an <code> ActivityRegistrationCourseCatalogAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationCourseCatalogAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationCourseCatalogAssignmentSession getActivityRegistrationCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  registrations to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationCourseCatalogAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationCourseCatalogAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationCourseCatalogAssignmentSession getActivityRegistrationCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationSmartCourseCatalogSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSmartCourseCatalog() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSmartCourseCatalogSession getActivityRegistrationSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getActivityRegistrationSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityRegistrationSmartCourseCatalogSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSmartCourseCatalog() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSmartCourseCatalogSession getActivityRegistrationSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getActivityRegistrationSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets a <code> CourseRegistrationBatchManager. </code> 
     *
     *  @return a <code> CourseRegistrationBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRegistrationBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.CourseRegistrationBatchManager getCourseRegistrationBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationManager.getCourseRegistrationBatchManager not implemented");
    }


    /**
     *  Gets a <code> CourseRegistrationBatchProxyManager. </code> 
     *
     *  @return a <code> CourseRegistrationBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRegistrationBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.CourseRegistrationBatchProxyManager getCourseRegistrationBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.registration.CourseRegistrationProxyManager.getCourseRegistrationBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.activityBundleRecordTypes.clear();
        this.activityBundleRecordTypes.clear();

        this.activityBundleSearchRecordTypes.clear();
        this.activityBundleSearchRecordTypes.clear();

        this.registrationRecordTypes.clear();
        this.registrationRecordTypes.clear();

        this.registrationSearchRecordTypes.clear();
        this.registrationSearchRecordTypes.clear();

        this.activityRegistrationRecordTypes.clear();
        this.activityRegistrationRecordTypes.clear();

        this.activityRegistrationSearchRecordTypes.clear();
        this.activityRegistrationSearchRecordTypes.clear();

        return;
    }
}
