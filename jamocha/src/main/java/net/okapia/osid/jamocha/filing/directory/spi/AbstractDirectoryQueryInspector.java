//
// AbstractDirectoryQueryInspector.java
//
//     A template for making a DirectoryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for directories.
 */

public abstract class AbstractDirectoryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.filing.DirectoryQueryInspector {

    private final java.util.Collection<org.osid.filing.records.DirectoryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the name query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPathTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the directory query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQueryInspector[] getDirectoryTerms() {
        return (new org.osid.filing.DirectoryQueryInspector[0]);
    }


    /**
     *  Gets the aliases query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAliasesTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the owner <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOwnerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the owner query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getOwnerTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the created time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the modified time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getModifiedTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the last access time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getLastAccessTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }

    
    /**
     *  Gets the file name query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getFileNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the file query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.filing.FileQueryInspector[] getFileTerms() {
        return (new org.osid.filing.FileQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given directory query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a directory implementing the requested record.
     *
     *  @param directoryRecordType a directory record type
     *  @return the directory query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directoryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectoryQueryInspectorRecord getDirectoryQueryInspectorRecord(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.DirectoryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this directory query. 
     *
     *  @param directoryQueryInspectorRecord directory query inspector
     *         record
     *  @param directoryRecordType directory record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDirectoryQueryInspectorRecord(org.osid.filing.records.DirectoryQueryInspectorRecord directoryQueryInspectorRecord, 
                                                   org.osid.type.Type directoryRecordType) {

        addRecordType(directoryRecordType);
        nullarg(directoryRecordType, "directory record type");
        this.records.add(directoryQueryInspectorRecord);        
        return;
    }
}
