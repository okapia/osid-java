//
// MutableMapCourseOfferingLookupSession
//
//    Implements a CourseOffering lookup service backed by a collection of
//    courseOfferings that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a CourseOffering lookup service backed by a collection of
 *  course offerings. The course offerings are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of course offerings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCourseOfferingLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractMapCourseOfferingLookupSession
    implements org.osid.course.CourseOfferingLookupSession {


    /**
     *  Constructs a new {@code MutableMapCourseOfferingLookupSession}
     *  with no course offerings.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog} is
     *          {@code null}
     */

      public MutableMapCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCourseOfferingLookupSession} with a
     *  single courseOffering.
     *
     *  @param courseCatalog the course catalog  
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courseOffering} is {@code null}
     */

    public MutableMapCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.CourseOffering courseOffering) {
        this(courseCatalog);
        putCourseOffering(courseOffering);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCourseOfferingLookupSession}
     *  using an array of course offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param courseOfferings an array of course offerings
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courseOfferings} is {@code null}
     */

    public MutableMapCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.CourseOffering[] courseOfferings) {
        this(courseCatalog);
        putCourseOfferings(courseOfferings);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCourseOfferingLookupSession}
     *  using a collection of course offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param courseOfferings a collection of course offerings
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courseOfferings} is {@code null}
     */

    public MutableMapCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           java.util.Collection<? extends org.osid.course.CourseOffering> courseOfferings) {

        this(courseCatalog);
        putCourseOfferings(courseOfferings);
        return;
    }

    
    /**
     *  Makes a {@code CourseOffering} available in this session.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException {@code courseOffering{@code  is
     *          {@code null}
     */

    @Override
    public void putCourseOffering(org.osid.course.CourseOffering courseOffering) {
        super.putCourseOffering(courseOffering);
        return;
    }


    /**
     *  Makes an array of course offerings available in this session.
     *
     *  @param courseOfferings an array of course offerings
     *  @throws org.osid.NullArgumentException {@code courseOfferings{@code 
     *          is {@code null}
     */

    @Override
    public void putCourseOfferings(org.osid.course.CourseOffering[] courseOfferings) {
        super.putCourseOfferings(courseOfferings);
        return;
    }


    /**
     *  Makes collection of course offerings available in this session.
     *
     *  @param courseOfferings a collection of course offerings
     *  @throws org.osid.NullArgumentException {@code courseOfferings{@code  is
     *          {@code null}
     */

    @Override
    public void putCourseOfferings(java.util.Collection<? extends org.osid.course.CourseOffering> courseOfferings) {
        super.putCourseOfferings(courseOfferings);
        return;
    }


    /**
     *  Removes a CourseOffering from this session.
     *
     *  @param courseOfferingId the {@code Id} of the course offering
     *  @throws org.osid.NullArgumentException {@code courseOfferingId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCourseOffering(org.osid.id.Id courseOfferingId) {
        super.removeCourseOffering(courseOfferingId);
        return;
    }    
}
