//
// AbstractMutableRequisite.java
//
//     Defines a mutable Requisite.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Requisite</code>.
 */

public abstract class AbstractMutableRequisite
    extends net.okapia.osid.jamocha.course.requisite.requisite.spi.AbstractRequisite
    implements org.osid.course.requisite.Requisite,
               net.okapia.osid.jamocha.builder.course.requisite.requisite.RequisiteMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    @Override
    public void setSequestered(boolean sequestered) {
        super.setSequestered(sequestered);
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this requisite. 
     *
     *  @param record requisite record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addRequisiteRecord(org.osid.course.requisite.records.RequisiteRecord record, org.osid.type.Type recordType) {
        super.addRequisiteRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this requisite is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this requisite ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override    
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Enables this requisite. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this requisite. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this requisite.
     *
     *  @param displayName the name for this requisite
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this requisite.
     *
     *  @param description the description of this requisite
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the schedule rule for this requisite.
     *
     *  @param schedule the schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */
    
    @Override
    public void setSchedule(org.osid.calendaring.Schedule schedule) {
        super.setSchedule(schedule);
        return;
    }


    /**
     *  Sets the event and makes this requisite 
     *  effective by event.
     *
     *  @param event the event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEvent(org.osid.calendaring.Event event) {
        super.setEvent(event);
        return;
    }


    /**
     *  Sets the cyclic event and makes this requisite 
     *  effective by cyclic event.
     *
     *  @param cyclicEvent the cyclic event
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEvent</code> is <code>null</code>
     */
    
    @Override
    public void setCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        super.setCyclicEvent(cyclicEvent);
        return;
    }


    /**
     *  Sets the demographic and apply this requisite
     *  to a population.
     *
     *  @param resource the demographic resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */
    
    @Override
    public void setDemographic(org.osid.resource.Resource resource) {
        super.setDemographic(resource);
        return;
    }


    /**
     *  Adds a requisite option.
     *
     *  @param option a requisite option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    @Override
    public void addRequisiteOption(org.osid.course.requisite.Requisite option) {
        super.addRequisiteOption(option);
        return;
    }


    /**
     *  Sets the requisite options.
     *
     *  @param options a collection of requisite options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    @Override
    public void setRequisiteOptions(java.util.Collection<org.osid.course.requisite.Requisite> options) {
        super.setRequisiteOptions(options);
        return;
    }


    /**
     *  Adds a course requirement.
     *
     *  @param requirement a course requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    @Override
    public void addCourseRequirement(org.osid.course.requisite.CourseRequirement requirement) {
        super.addCourseRequirement(requirement);
        return;
    }


    /**
     *  Sets the course requirements.
     *
     *  @param requirements a collection of course requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    @Override
    public void setCourseRequirements(java.util.Collection<org.osid.course.requisite.CourseRequirement> requirements) {
        super.setCourseRequirements(requirements);
        return;
    }


    /**
     *  Adds a program requirement.
     *
     *  @param requirement a program requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    @Override
    public void addProgramRequirement(org.osid.course.requisite.ProgramRequirement requirement) {
        super.addProgramRequirement(requirement);
        return;
    }


    /**
     *  Sets the program requirements.
     *
     *  @param requirements a collection of program requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setProgramRequirements(java.util.Collection<org.osid.course.requisite.ProgramRequirement> requirements) {
        super.setProgramRequirements(requirements);
        return;
    }


    /**
     *  Adds a credential requirement.
     *
     *  @param requirement a credential requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    @Override
    public void addCredentialRequirement(org.osid.course.requisite.CredentialRequirement requirement) {
        super.addCredentialRequirement(requirement);
        return;
    }


    /**
     *  Sets the credential requirements.
     *
     *  @param requirements a collection of credential requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setCredentialRequirements(java.util.Collection<org.osid.course.requisite.CredentialRequirement> requirements) {
        super.setCredentialRequirements(requirements);
        return;
    }


    /**
     *  Adds a learning objective requirement.
     *
     *  @param requirement a learning objective requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    @Override
    public void addLearningObjectiveRequirement(org.osid.course.requisite.LearningObjectiveRequirement requirement) {
        super.addLearningObjectiveRequirement(requirement);
        return;
    }


    /**
     *  Sets the learning objective requirements.
     *
     *  @param requirements a collection of learning objective requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setLearningObjectiveRequirements(java.util.Collection<org.osid.course.requisite.LearningObjectiveRequirement> requirements) {
        super.setLearningObjectiveRequirements(requirements);
        return;
    }


    /**
     *  Adds an assessment requirement.
     *
     *  @param requirement a assessment requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    @Override
    public void addAssessmentRequirement(org.osid.course.requisite.AssessmentRequirement requirement) {
        super.addAssessmentRequirement(requirement);
        return;
    }


    /**
     *  Sets the assessment requirements.
     *
     *  @param requirements a collection assessment requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setAssessmentRequirements(java.util.Collection<org.osid.course.requisite.AssessmentRequirement> requirements) {
        super.setAssessmentRequirements(requirements);
        return;
    }


    /**
     *  Adds an award requirement.
     *
     *  @param requirement a award requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    @Override
    public void addAwardRequirement(org.osid.course.requisite.AwardRequirement requirement) {
        super.addAwardRequirement(requirement);
        return;
    }


    /**
     *  Sets the award requirements.
     *
     *  @param requirements a collection of award requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setAwardRequirements(java.util.Collection<org.osid.course.requisite.AwardRequirement> requirements) {
        super.setAwardRequirements(requirements);
        return;
    }
}

