//
// InvariantIndexedMapProxyActivityBundleLookupSession
//
//    Implements an ActivityBundle lookup service backed by a fixed
//    collection of activityBundles indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration;


/**
 *  Implements a ActivityBundle lookup service backed by a fixed
 *  collection of activityBundles. The activityBundles are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some activityBundles may be compatible
 *  with more types than are indicated through these activityBundle
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyActivityBundleLookupSession
    extends net.okapia.osid.jamocha.core.course.registration.spi.AbstractIndexedMapActivityBundleLookupSession
    implements org.osid.course.registration.ActivityBundleLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyActivityBundleLookupSession}
     *  using an array of activity bundles.
     *
     *  @param courseCatalog the course catalog
     *  @param activityBundles an array of activity bundles
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityBundles} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                         org.osid.course.registration.ActivityBundle[] activityBundles, 
                                                         org.osid.proxy.Proxy proxy) {

        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        putActivityBundles(activityBundles);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyActivityBundleLookupSession}
     *  using a collection of activity bundles.
     *
     *  @param courseCatalog the course catalog
     *  @param activityBundles a collection of activity bundles
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityBundles} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                         java.util.Collection<? extends org.osid.course.registration.ActivityBundle> activityBundles,
                                                         org.osid.proxy.Proxy proxy) {

        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        putActivityBundles(activityBundles);

        return;
    }
}
