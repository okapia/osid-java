//
// AbstractMapJobProcessorEnablerLookupSession
//
//    A simple framework for providing a JobProcessorEnabler lookup service
//    backed by a fixed collection of job processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a JobProcessorEnabler lookup service backed by a
 *  fixed collection of job processor enablers. The job processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JobProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapJobProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.resourcing.rules.spi.AbstractJobProcessorEnablerLookupSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.rules.JobProcessorEnabler>());


    /**
     *  Makes a <code>JobProcessorEnabler</code> available in this session.
     *
     *  @param  jobProcessorEnabler a job processor enabler
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putJobProcessorEnabler(org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler) {
        this.jobProcessorEnablers.put(jobProcessorEnabler.getId(), jobProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of job processor enablers available in this session.
     *
     *  @param  jobProcessorEnablers an array of job processor enablers
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putJobProcessorEnablers(org.osid.resourcing.rules.JobProcessorEnabler[] jobProcessorEnablers) {
        putJobProcessorEnablers(java.util.Arrays.asList(jobProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of job processor enablers available in this session.
     *
     *  @param  jobProcessorEnablers a collection of job processor enablers
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putJobProcessorEnablers(java.util.Collection<? extends org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablers) {
        for (org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler : jobProcessorEnablers) {
            this.jobProcessorEnablers.put(jobProcessorEnabler.getId(), jobProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a JobProcessorEnabler from this session.
     *
     *  @param  jobProcessorEnablerId the <code>Id</code> of the job processor enabler
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId) {
        this.jobProcessorEnablers.remove(jobProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>JobProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  jobProcessorEnablerId <code>Id</code> of the <code>JobProcessorEnabler</code>
     *  @return the jobProcessorEnabler
     *  @throws org.osid.NotFoundException <code>jobProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnabler getJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler = this.jobProcessorEnablers.get(jobProcessorEnablerId);
        if (jobProcessorEnabler == null) {
            throw new org.osid.NotFoundException("jobProcessorEnabler not found: " + jobProcessorEnablerId);
        }

        return (jobProcessorEnabler);
    }


    /**
     *  Gets all <code>JobProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known jobProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  jobProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>JobProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.ArrayJobProcessorEnablerList(this.jobProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobProcessorEnablers.clear();
        super.close();
        return;
    }
}
