//
// AbstractMapOfferingConstrainerLookupSession
//
//    A simple framework for providing an OfferingConstrainer lookup service
//    backed by a fixed collection of offering constrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an OfferingConstrainer lookup service backed by a
 *  fixed collection of offering constrainers. The offering constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OfferingConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOfferingConstrainerLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractOfferingConstrainerLookupSession
    implements org.osid.offering.rules.OfferingConstrainerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.rules.OfferingConstrainer> offeringConstrainers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.rules.OfferingConstrainer>());


    /**
     *  Makes an <code>OfferingConstrainer</code> available in this session.
     *
     *  @param  offeringConstrainer an offering constrainer
     *  @throws org.osid.NullArgumentException <code>offeringConstrainer<code>
     *          is <code>null</code>
     */

    protected void putOfferingConstrainer(org.osid.offering.rules.OfferingConstrainer offeringConstrainer) {
        this.offeringConstrainers.put(offeringConstrainer.getId(), offeringConstrainer);
        return;
    }


    /**
     *  Makes an array of offering constrainers available in this session.
     *
     *  @param  offeringConstrainers an array of offering constrainers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainers<code>
     *          is <code>null</code>
     */

    protected void putOfferingConstrainers(org.osid.offering.rules.OfferingConstrainer[] offeringConstrainers) {
        putOfferingConstrainers(java.util.Arrays.asList(offeringConstrainers));
        return;
    }


    /**
     *  Makes a collection of offering constrainers available in this session.
     *
     *  @param  offeringConstrainers a collection of offering constrainers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainers<code>
     *          is <code>null</code>
     */

    protected void putOfferingConstrainers(java.util.Collection<? extends org.osid.offering.rules.OfferingConstrainer> offeringConstrainers) {
        for (org.osid.offering.rules.OfferingConstrainer offeringConstrainer : offeringConstrainers) {
            this.offeringConstrainers.put(offeringConstrainer.getId(), offeringConstrainer);
        }

        return;
    }


    /**
     *  Removes an OfferingConstrainer from this session.
     *
     *  @param  offeringConstrainerId the <code>Id</code> of the offering constrainer
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerId<code> is
     *          <code>null</code>
     */

    protected void removeOfferingConstrainer(org.osid.id.Id offeringConstrainerId) {
        this.offeringConstrainers.remove(offeringConstrainerId);
        return;
    }


    /**
     *  Gets the <code>OfferingConstrainer</code> specified by its <code>Id</code>.
     *
     *  @param  offeringConstrainerId <code>Id</code> of the <code>OfferingConstrainer</code>
     *  @return the offeringConstrainer
     *  @throws org.osid.NotFoundException <code>offeringConstrainerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainer getOfferingConstrainer(org.osid.id.Id offeringConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.rules.OfferingConstrainer offeringConstrainer = this.offeringConstrainers.get(offeringConstrainerId);
        if (offeringConstrainer == null) {
            throw new org.osid.NotFoundException("offeringConstrainer not found: " + offeringConstrainerId);
        }

        return (offeringConstrainer);
    }


    /**
     *  Gets all <code>OfferingConstrainers</code>. In plenary mode, the returned
     *  list contains all known offeringConstrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  offeringConstrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>OfferingConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.offeringconstrainer.ArrayOfferingConstrainerList(this.offeringConstrainers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offeringConstrainers.clear();
        super.close();
        return;
    }
}
