//
// AbstractQueryParameterProcessorLookupSession.java
//
//    An inline adapter that maps a ParameterProcessorLookupSession to
//    a ParameterProcessorQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ParameterProcessorLookupSession to
 *  a ParameterProcessorQuerySession.
 */

public abstract class AbstractQueryParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.configuration.rules.spi.AbstractParameterProcessorLookupSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {

      private boolean activeonly    = false;

    private final org.osid.configuration.rules.ParameterProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryParameterProcessorLookupSession.
     *
     *  @param querySession the underlying parameter processor query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryParameterProcessorLookupSession(org.osid.configuration.rules.ParameterProcessorQuerySession querySession) {
        nullarg(querySession, "parameter processor query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Configuration</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform <code>ParameterProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParameterProcessors() {
        return (this.session.canSearchParameterProcessors());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameter processors in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    

    /**
     *  Only active parameter processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive parameter processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ParameterProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ParameterProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ParameterProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorId <code>Id</code> of the
     *          <code>ParameterProcessor</code>
     *  @return the parameter processor
     *  @throws org.osid.NotFoundException <code>parameterProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>parameterProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessor getParameterProcessor(org.osid.id.Id parameterProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.rules.ParameterProcessorQuery query = getQuery();
        query.matchId(parameterProcessorId, true);
        org.osid.configuration.rules.ParameterProcessorList parameterProcessors = this.session.getParameterProcessorsByQuery(query);
        if (parameterProcessors.hasNext()) {
            return (parameterProcessors.getNextParameterProcessor());
        } 
        
        throw new org.osid.NotFoundException(parameterProcessorId + " not found");
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameterProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ParameterProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByIds(org.osid.id.IdList parameterProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.rules.ParameterProcessorQuery query = getQuery();

        try (org.osid.id.IdList ids = parameterProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getParameterProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  parameter processor genus <code>Type</code> which does not include
     *  parameter processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.rules.ParameterProcessorQuery query = getQuery();
        query.matchGenusType(parameterProcessorGenusType, true);
        return (this.session.getParameterProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  parameter processor genus <code>Type</code> and include any additional
     *  parameter processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByParentGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.rules.ParameterProcessorQuery query = getQuery();
        query.matchParentGenusType(parameterProcessorGenusType, true);
        return (this.session.getParameterProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> containing the given
     *  parameter processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorRecordType a parameterProcessor record type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByRecordType(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.rules.ParameterProcessorQuery query = getQuery();
        query.matchRecordType(parameterProcessorRecordType, true);
        return (this.session.getParameterProcessorsByQuery(query));
    }

    
    /**
     *  Gets all <code>ParameterProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @return a list of <code>ParameterProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.configuration.rules.ParameterProcessorQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getParameterProcessorsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.configuration.rules.ParameterProcessorQuery getQuery() {
        org.osid.configuration.rules.ParameterProcessorQuery query = this.session.getParameterProcessorQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
