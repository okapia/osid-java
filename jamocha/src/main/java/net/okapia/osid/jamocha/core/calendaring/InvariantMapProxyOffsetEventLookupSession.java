//
// InvariantMapProxyOffsetEventLookupSession
//
//    Implements an OffsetEvent lookup service backed by a fixed
//    collection of offsetEvents. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements an OffsetEvent lookup service backed by a fixed
 *  collection of offset events. The offset events are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyOffsetEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapOffsetEventLookupSession
    implements org.osid.calendaring.OffsetEventLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOffsetEventLookupSession} with no
     *  offset events.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyOffsetEventLookupSession} with a single
     *  offset event.
     *
     *  @param calendar the calendar
     *  @param offsetEvent an single offset event
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEvent} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.OffsetEvent offsetEvent, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putOffsetEvent(offsetEvent);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyOffsetEventLookupSession} using
     *  an array of offset events.
     *
     *  @param calendar the calendar
     *  @param offsetEvents an array of offset events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEvents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.OffsetEvent[] offsetEvents, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putOffsetEvents(offsetEvents);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOffsetEventLookupSession} using a
     *  collection of offset events.
     *
     *  @param calendar the calendar
     *  @param offsetEvents a collection of offset events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEvents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.OffsetEvent> offsetEvents,
                                                  org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putOffsetEvents(offsetEvents);
        return;
    }
}
