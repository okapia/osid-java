//
// FederatingStepProcessorEnablerList.java
//
//     Interface for federating lists.
//
//
// Tom Coppeto
// Okapia
// 17 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessorenabler;


/**
 *  Federating stepprocessorenabler lists.
 */

public interface FederatingStepProcessorEnablerList
    extends org.osid.workflow.rules.StepProcessorEnablerList {


    /**
     *  Adds a <code>StepProcessorEnablerList</code> to this
     *  <code>StepProcessorEnablerList</code> using the default buffer size.
     *
     *  @param stepProcessorEnablerList the <code>StepProcessorEnablerList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerList</code> is <code>null</code>
     */

    public void addStepProcessorEnablerList(org.osid.workflow.rules.StepProcessorEnablerList stepProcessorEnablerList);


    /**
     *  Adds a collection of <code>StepProcessorEnablerLists</code> to this
     *  <code>StepProcessorEnablerList</code>.
     *
     *  @param stepProcessorEnablerLists the <code>StepProcessorEnablerList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerLists</code> is <code>null</code>
     */

    public void addStepProcessorEnablerLists(java.util.Collection<org.osid.workflow.rules.StepProcessorEnablerList> stepProcessorEnablerLists);

        
    /**
     *  No more. Invoked when no more lists are to be added.
     */

    public void noMore();


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational();


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty();
}
