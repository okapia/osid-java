//
// AbstractResponseLookupSession.java
//
//    A starter implementation framework for providing a Response
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Response
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getResponses(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractResponseLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inquiry.ResponseLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();
    

    /**
     *  Gets the <code>Inquest/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Inquest Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.inquest.getId());
    }


    /**
     *  Gets the <code>Inquest</code> associated with this 
     *  session.
     *
     *  @return the <code>Inquest</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.inquest);
    }


    /**
     *  Sets the <code>Inquest</code>.
     *
     *  @param  inquest the inquest for this session
     *  @throws org.osid.NullArgumentException <code>inquest</code>
     *          is <code>null</code>
     */

    protected void setInquest(org.osid.inquiry.Inquest inquest) {
        nullarg(inquest, "inquest");
        this.inquest = inquest;
        return;
    }


    /**
     *  Tests if this user can perform <code>Response</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResponses() {
        return (true);
    }


    /**
     *  A complete view of the <code>Response</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResponseView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Response</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResponseView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include responses in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only responses whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResponseView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All responses of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResponseView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Response</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Response</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Response</code> and
     *  retained for compatibility.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  responseId <code>Id</code> of the
     *          <code>Response</code>
     *  @return the response
     *  @throws org.osid.NotFoundException <code>responseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>responseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Response getResponse(org.osid.id.Id responseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inquiry.ResponseList responses = getResponses()) {
            while (responses.hasNext()) {
                org.osid.inquiry.Response response = responses.getNextResponse();
                if (response.getId().equals(responseId)) {
                    return (response);
                }
            }
        } 

        throw new org.osid.NotFoundException(responseId + " not found");
    }


    /**
     *  Gets a <code>ResponseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  responses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Responses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getResponses()</code>.
     *
     *  @param  responseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>responseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByIds(org.osid.id.IdList responseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inquiry.Response> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = responseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getResponse(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("response " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inquiry.response.LinkedResponseList(ret));
    }


    /**
     *  Gets a <code>ResponseList</code> corresponding to the given
     *  response genus <code>Type</code> which does not include
     *  responses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getResponses()</code>.
     *
     *  @param  responseGenusType a response genus type 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByGenusType(org.osid.type.Type responseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.ResponseGenusFilterList(getResponses(), responseGenusType));
    }


    /**
     *  Gets a <code>ResponseList</code> corresponding to the given
     *  response genus <code>Type</code> and include any additional
     *  responses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResponses()</code>.
     *
     *  @param  responseGenusType a response genus type 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByParentGenusType(org.osid.type.Type responseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getResponsesByGenusType(responseGenusType));
    }


    /**
     *  Gets a <code>ResponseList</code> containing the given
     *  response record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getResponses()</code>.
     *
     *  @param  responseRecordType a response record type 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByRecordType(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.ResponseRecordFilterList(getResponses(), responseRecordType));
    }


    /**
     *  Gets a <code>ResponseList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *  
     *  In active mode, responses are returned that are currently
     *  active. In any status mode, active and inactive responses
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Response</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.TemporalResponseFilterList(getResponses(), from, to));
    }
        

    /**
     *  Gets a list of responses corresponding to an inquiry
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  inquiryId the <code>Id</code> of the inquiry
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.inquiry.ResponseList getResponsesForInquiry(org.osid.id.Id inquiryId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.ResponseFilterList(new InquiryFilter(inquiryId), getResponses()));
    }


    /**
     *  Gets a list of responses corresponding to an inquiry
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  inquiryId the <code>Id</code> of the inquiry
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryOnDate(org.osid.id.Id inquiryId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.TemporalResponseFilterList(getResponsesForInquiry(inquiryId), from, to));
    }


    /**
     *  Gets a list of responses corresponding to a responder
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the responder
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.inquiry.ResponseList getResponsesForResponder(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.ResponseFilterList(new ResponderFilter(resourceId), getResponses()));
    }


    /**
     *  Gets a list of responses corresponding to a responder
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the responder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForResponderOnDate(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.TemporalResponseFilterList(getResponsesForResponder(resourceId), from, to));
    }


    /**
     *  Gets a list of responses corresponding to inquiry and responder
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  inquiryId the <code>Id</code> of the inquiry
     *  @param  resourceId the <code>Id</code> of the responder
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryAndResponder(org.osid.id.Id inquiryId,
                                                                        org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.ResponseFilterList(new ResponderFilter(resourceId), getResponsesForInquiry(inquiryId)));
    }


    /**
     *  Gets a list of responses corresponding to inquiry and responder
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible
     *  through this session.
     *
     *  In effective mode, responses are returned that are
     *  currently effective.  In any effective mode, effective
     *  responses and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the responder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ResponseList</code>
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryAndResponderOnDate(org.osid.id.Id inquiryId,
                                                                                  org.osid.id.Id resourceId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.response.TemporalResponseFilterList(getResponsesForInquiryAndResponder(inquiryId, resourceId), from, to));
    }


    /**
     *  Gets all <code>Responses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Responses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inquiry.ResponseList getResponses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the response list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of responses
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inquiry.ResponseList filterResponsesOnViews(org.osid.inquiry.ResponseList list)
        throws org.osid.OperationFailedException {

        org.osid.inquiry.ResponseList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.inquiry.response.EffectiveResponseFilterList(ret);
        }

        return (ret);
    }


    public static class InquiryFilter
        implements net.okapia.osid.jamocha.inline.filter.inquiry.response.ResponseFilter {
         
        private final org.osid.id.Id inquiryId;
         
         
        /**
         *  Constructs a new <code>InquiryFilter</code>.
         *
         *  @param inquiryId the inquiry to filter
         *  @throws org.osid.NullArgumentException
         *          <code>inquiryId</code> is <code>null</code>
         */
        
        public InquiryFilter(org.osid.id.Id inquiryId) {
            nullarg(inquiryId, "inquiry Id");
            this.inquiryId = inquiryId;
            return;
        }

         
        /**
         *  Used by the ResponseFilterList to filter the 
         *  response list based on inquiry.
         *
         *  @param response the response
         *  @return <code>true</code> to pass the response,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inquiry.Response response) {
            return (response.getInquiryId().equals(this.inquiryId));
        }
    }


    public static class ResponderFilter
        implements net.okapia.osid.jamocha.inline.filter.inquiry.response.ResponseFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResponderFilter</code>.
         *
         *  @param resourceId the responder to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResponderFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "responder Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ResponseFilterList to filter the 
         *  response list based on responder.
         *
         *  @param response the response
         *  @return <code>true</code> to pass the response,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inquiry.Response response) {
            return (response.getResponderId().equals(this.resourceId));
        }
    }
}
