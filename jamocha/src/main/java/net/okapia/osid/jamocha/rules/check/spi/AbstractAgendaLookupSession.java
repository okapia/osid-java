//
// AbstractAgendaLookupSession.java
//
//    A starter implementation framework for providing an Agenda
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Agenda lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAgendas(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAgendaLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.rules.check.AgendaLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();
    

    /**
     *  Gets the <code>Engine/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this session.
     *
     *  @return the <code>Engine</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the <code>Engine</code>.
     *
     *  @param  engine the engine for this session
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can perform <code>Agenda</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAgendas() {
        return (true);
    }


    /**
     *  A complete view of the <code>Agenda</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAgendaView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Agenda</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAgendaView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agendas in engines which are children of
     *  this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Agenda</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Agenda</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Agenda</code> and retained for
     *  compatibility.
     *
     *  @param  agendaId <code>Id</code> of the
     *          <code>Agenda</code>
     *  @return the agenda
     *  @throws org.osid.NotFoundException <code>agendaId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>agendaId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getAgenda(org.osid.id.Id agendaId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.rules.check.AgendaList agendas = getAgendas()) {
            while (agendas.hasNext()) {
                org.osid.rules.check.Agenda agenda = agendas.getNextAgenda();
                if (agenda.getId().equals(agendaId)) {
                    return (agenda);
                }
            }
        } 

        throw new org.osid.NotFoundException(agendaId + " not found");
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the agendas
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Agendas</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAgendas()</code>.
     *
     *  @param  agendaIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>agendaIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByIds(org.osid.id.IdList agendaIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.rules.check.Agenda> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = agendaIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAgenda(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("agenda " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.rules.check.agenda.LinkedAgendaList(ret));
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  agenda genus <code>Type</code> which does not include agendas
     *  of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known agendas
     *  or an error results. Otherwise, the returned list may contain
     *  only those agendas that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAgendas()</code>.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.agenda.AgendaGenusFilterList(getAgendas(), agendaGenusType));
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  agenda genus <code>Type</code> and include any additional
     *  agendas with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known agendas
     *  or an error results. Otherwise, the returned list may contain
     *  only those agendas that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAgendas()</code>.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByParentGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAgendasByGenusType(agendaGenusType));
    }


    /**
     *  Gets an <code>AgendaList</code> containing the given agenda
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known agendas
     *  or an error results. Otherwise, the returned list may contain
     *  only those agendas that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAgendas()</code>.
     *
     *  @param  agendaRecordType an agenda record type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByRecordType(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.agenda.AgendaRecordFilterList(getAgendas(), agendaRecordType));
    }


    /**
     *  Gets all <code>Agendas</code>.
     *
     *  In plenary mode, the returned list contains all known agendas
     *  or an error results. Otherwise, the returned list may contain
     *  only those agendas that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Agendas</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.rules.check.AgendaList getAgendas()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the agenda list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of agendas
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.rules.check.AgendaList filterAgendasOnViews(org.osid.rules.check.AgendaList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
