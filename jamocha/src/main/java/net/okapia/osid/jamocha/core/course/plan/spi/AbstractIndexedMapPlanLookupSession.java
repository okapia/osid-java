//
// AbstractIndexedMapPlanLookupSession.java
//
//    A simple framework for providing a Plan lookup service
//    backed by a fixed collection of plans with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Plan lookup service backed by a
 *  fixed collection of plans. The plans are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some plans may be compatible
 *  with more types than are indicated through these plan
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Plans</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPlanLookupSession
    extends AbstractMapPlanLookupSession
    implements org.osid.course.plan.PlanLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.plan.Plan> plansByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.plan.Plan>());
    private final MultiMap<org.osid.type.Type, org.osid.course.plan.Plan> plansByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.plan.Plan>());


    /**
     *  Makes a <code>Plan</code> available in this session.
     *
     *  @param  plan a plan
     *  @throws org.osid.NullArgumentException <code>plan<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPlan(org.osid.course.plan.Plan plan) {
        super.putPlan(plan);

        this.plansByGenus.put(plan.getGenusType(), plan);
        
        try (org.osid.type.TypeList types = plan.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.plansByRecord.put(types.getNextType(), plan);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a plan from this session.
     *
     *  @param planId the <code>Id</code> of the plan
     *  @throws org.osid.NullArgumentException <code>planId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePlan(org.osid.id.Id planId) {
        org.osid.course.plan.Plan plan;
        try {
            plan = getPlan(planId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.plansByGenus.remove(plan.getGenusType());

        try (org.osid.type.TypeList types = plan.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.plansByRecord.remove(types.getNextType(), plan);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePlan(planId);
        return;
    }


    /**
     *  Gets a <code>PlanList</code> corresponding to the given
     *  plan genus <code>Type</code> which does not include
     *  plans of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known plans or an error results. Otherwise,
     *  the returned list may contain only those plans that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  planGenusType a plan genus type 
     *  @return the returned <code>Plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByGenusType(org.osid.type.Type planGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.plan.plan.ArrayPlanList(this.plansByGenus.get(planGenusType)));
    }


    /**
     *  Gets a <code>PlanList</code> containing the given
     *  plan record <code>Type</code>. In plenary mode, the
     *  returned list contains all known plans or an error
     *  results. Otherwise, the returned list may contain only those
     *  plans that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  planRecordType a plan record type 
     *  @return the returned <code>plan</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByRecordType(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.plan.plan.ArrayPlanList(this.plansByRecord.get(planRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.plansByGenus.clear();
        this.plansByRecord.clear();

        super.close();

        return;
    }
}
