//
// DispatchElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.dispatch.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class DispatchElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the DispatchElement Id.
     *
     *  @return the dispatch element Id
     */

    public static org.osid.id.Id getDispatchEntityId() {
        return (makeEntityId("osid.subscription.Dispatch"));
    }


    /**
     *  Gets the AddressGenusTypes element Id.
     *
     *  @return the AddressGenusTypes element Id
     */

    public static org.osid.id.Id getAddressGenusTypes() {
        return (makeElementId("osid.subscription.dispatch.AddressGenusTypes"));
    }


    /**
     *  Gets the SubscriptionId element Id.
     *
     *  @return the SubscriptionId element Id
     */

    public static org.osid.id.Id getSubscriptionId() {
        return (makeQueryElementId("osid.subscription.dispatch.SubscriptionId"));
    }


    /**
     *  Gets the Subscription element Id.
     *
     *  @return the Subscription element Id
     */

    public static org.osid.id.Id getSubscription() {
        return (makeQueryElementId("osid.subscription.dispatch.Subscription"));
    }


    /**
     *  Gets the PublisherId element Id.
     *
     *  @return the PublisherId element Id
     */

    public static org.osid.id.Id getPublisherId() {
        return (makeQueryElementId("osid.subscription.dispatch.PublisherId"));
    }


    /**
     *  Gets the Publisher element Id.
     *
     *  @return the Publisher element Id
     */

    public static org.osid.id.Id getPublisher() {
        return (makeQueryElementId("osid.subscription.dispatch.Publisher"));
    }
}
