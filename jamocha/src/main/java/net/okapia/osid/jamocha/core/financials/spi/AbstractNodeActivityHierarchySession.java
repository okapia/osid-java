//
// AbstractNodeActivityHierarchySession.java
//
//     Defines an Activity hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an activity hierarchy session for delivering a hierarchy
 *  of activities using the ActivityNode interface.
 */

public abstract class AbstractNodeActivityHierarchySession
    extends net.okapia.osid.jamocha.financials.spi.AbstractActivityHierarchySession
    implements org.osid.financials.ActivityHierarchySession {

    private java.util.Collection<org.osid.financials.ActivityNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root activity <code> Ids </code> in this hierarchy.
     *
     *  @return the root activity <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootActivityIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.financials.activitynode.ActivityNodeToIdList(this.roots));
    }


    /**
     *  Gets the root activities in the activity hierarchy. A node
     *  with no parents is an orphan. While all activity <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root activities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getRootActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.activitynode.ActivityNodeToActivityList(new net.okapia.osid.jamocha.financials.activitynode.ArrayActivityNodeList(this.roots)));
    }


    /**
     *  Adds a root activity node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootActivity(org.osid.financials.ActivityNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root activity nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootActivities(java.util.Collection<org.osid.financials.ActivityNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root activity node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootActivity(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.financials.ActivityNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Activity </code> has any parents. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return <code> true </code> if the activity has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentActivities(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getActivityNode(activityId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  activity.
     *
     *  @param  id an <code> Id </code> 
     *  @param  activityId the <code> Id </code> of an activity 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> activityId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> activityId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfActivity(org.osid.id.Id id, org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.ActivityNodeList parents = getActivityNode(activityId).getParentActivityNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextActivityNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given activity. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return the parent <code> Ids </code> of the activity 
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentActivityIds(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.activity.ActivityToIdList(getParentActivities(activityId)));
    }


    /**
     *  Gets the parents of the given activity. 
     *
     *  @param  activityId the <code> Id </code> to query 
     *  @return the parents of the activity 
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getParentActivities(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.activitynode.ActivityNodeToActivityList(getActivityNode(activityId).getParentActivityNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  activity.
     *
     *  @param  id an <code> Id </code> 
     *  @param  activityId the Id of an activity 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> activityId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfActivity(org.osid.id.Id id, org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfActivity(id, activityId)) {
            return (true);
        }

        try (org.osid.financials.ActivityList parents = getParentActivities(activityId)) {
            while (parents.hasNext()) {
                if (isAncestorOfActivity(id, parents.getNextActivity().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an activity has any children. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return <code> true </code> if the <code> activityId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildActivities(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivityNode(activityId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  activity.
     *
     *  @param  id an <code> Id </code> 
     *  @param activityId the <code> Id </code> of an 
     *         activity
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> activityId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> activityId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfActivity(org.osid.id.Id id, org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfActivity(activityId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  activity.
     *
     *  @param  activityId the <code> Id </code> to query 
     *  @return the children of the activity 
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildActivityIds(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.activity.ActivityToIdList(getChildActivities(activityId)));
    }


    /**
     *  Gets the children of the given activity. 
     *
     *  @param  activityId the <code> Id </code> to query 
     *  @return the children of the activity 
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getChildActivities(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.activitynode.ActivityNodeToActivityList(getActivityNode(activityId).getChildActivityNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  activity.
     *
     *  @param  id an <code> Id </code> 
     *  @param activityId the <code> Id </code> of an 
     *         activity
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> activityId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfActivity(org.osid.id.Id id, org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfActivity(activityId, id)) {
            return (true);
        }

        try (org.osid.financials.ActivityList children = getChildActivities(activityId)) {
            while (children.hasNext()) {
                if (isDescendantOfActivity(id, children.getNextActivity().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  activity.
     *
     *  @param  activityId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified activity node 
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getActivityNodeIds(org.osid.id.Id activityId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.financials.activitynode.ActivityNodeToNode(getActivityNode(activityId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given activity.
     *
     *  @param  activityId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified activity node 
     *  @throws org.osid.NotFoundException <code> activityId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityNode getActivityNodes(org.osid.id.Id activityId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivityNode(activityId));
    }


    /**
     *  Closes this <code>ActivityHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an activity node.
     *
     *  @param activityId the id of the activity node
     *  @throws org.osid.NotFoundException <code>activityId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.financials.ActivityNode getActivityNode(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(activityId, "activity Id");
        for (org.osid.financials.ActivityNode activity : this.roots) {
            if (activity.getId().equals(activityId)) {
                return (activity);
            }

            org.osid.financials.ActivityNode r = findActivity(activity, activityId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(activityId + " is not found");
    }


    protected org.osid.financials.ActivityNode findActivity(org.osid.financials.ActivityNode node, 
                                                            org.osid.id.Id activityId) 
	throws org.osid.OperationFailedException {

        try (org.osid.financials.ActivityNodeList children = node.getChildActivityNodes()) {
            while (children.hasNext()) {
                org.osid.financials.ActivityNode activity = children.getNextActivityNode();
                if (activity.getId().equals(activityId)) {
                    return (activity);
                }
                
                activity = findActivity(activity, activityId);
                if (activity != null) {
                    return (activity);
                }
            }
        }

        return (null);
    }
}
