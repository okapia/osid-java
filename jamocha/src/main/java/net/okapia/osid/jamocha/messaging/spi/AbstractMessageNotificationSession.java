//
// AbstractMessageNotificationSession.java
//
//     A template for making MessageNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Message} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Message} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for message entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractMessageNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.messaging.MessageNotificationSession {

    private boolean federated = false;
    private org.osid.messaging.Mailbox mailbox = new net.okapia.osid.jamocha.nil.messaging.mailbox.UnknownMailbox();


    /**
     *  Gets the {@code Mailbox/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Mailbox Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.mailbox.getId());
    }

    
    /**
     *  Gets the {@code Mailbox} associated with this session.
     *
     *  @return the {@code Mailbox} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.mailbox);
    }


    /**
     *  Sets the {@code Mailbox}.
     *
     *  @param mailbox the mailbox for this session
     *  @throws org.osid.NullArgumentException {@code mailbox}
     *          is {@code null}
     */

    protected void setMailbox(org.osid.messaging.Mailbox mailbox) {
        nullarg(mailbox, "mailbox");
        this.mailbox = mailbox;
        return;
    }


    /**
     *  Tests if this user can register for {@code Message}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForMessageNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include messages in mailboxes which are children of
     *  this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new messages. {@code
     *  MessageReceiver.newMessage()} is invoked when a new {@code
     *  Message} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new messages from sender. {@code
     *  MessageReceiver.newMessage()} is invoked when a new {@code
     *  Message} is appears in this mailbox.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewMessagesFromSender(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new messages for a
     *  recipient. {@code MessageReceiver.newMessage()} is invoked
     *  when a new {@code Message} is appears in this mailbox.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewMessagesForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated messages. {@code
     *  MessageReceiver.changedMessage()} is invoked when a message is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated messages from
     *  sender. {@code MessageReceiver.updatedMessage()} is invoked
     *  when a {@code Message} in this mailbox is changed.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedMessagesFromSender(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated messages for a
     *  recipient. {@code MessageReceiver.updatedMessage()} is invoked
     *  when a {@code Message} in this mailbox is changed.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedMessagesForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated message. {@code
     *  MessageReceiver.changedMessage()} is invoked when the
     *  specified message is changed.
     *
     *  @param messageId the {@code Id} of the {@code Message} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code messageId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedMessage(org.osid.id.Id messageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted messages. {@code
     *  MessageReceiver.deletedMessage()} is invoked when a message is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted messages from
     *  sender. {@code MessageReceiver.deletedMessage()} is invoked
     *  when a {@code Message} is deleted or removed from this
     *  mailbox.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code resourceId } is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedMessagesFromSender(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted messages for a
     *  recipient. {@code MessageReceiver.deletedMessage()} is invoked
     *  when a {@code Message} is deleted or removed from this
     *  mailbox.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedMessagesForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted message. {@code
     *  MessageReceiver.deletedMessage()} is invoked when the
     *  specified message is deleted.
     *
     *  @param messageId the {@code Id} of the
     *          {@code Message} to monitor
     *  @throws org.osid.NullArgumentException {@code messageId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedMessage(org.osid.id.Id messageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
