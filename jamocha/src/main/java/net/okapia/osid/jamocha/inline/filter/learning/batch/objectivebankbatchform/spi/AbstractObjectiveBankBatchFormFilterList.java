//
// AbstractObjectiveBankBatchFormList
//
//     Implements a filter for an ObjectiveBankBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.learning.batch.objectivebankbatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an ObjectiveBankBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedObjectiveBankBatchFormList
 *  to improve performance.
 */

public abstract class AbstractObjectiveBankBatchFormFilterList
    extends net.okapia.osid.jamocha.learning.batch.objectivebankbatchform.spi.AbstractObjectiveBankBatchFormList
    implements org.osid.learning.batch.ObjectiveBankBatchFormList,
               net.okapia.osid.jamocha.inline.filter.learning.batch.objectivebankbatchform.ObjectiveBankBatchFormFilter {

    private org.osid.learning.batch.ObjectiveBankBatchForm objectiveBankBatchForm;
    private final org.osid.learning.batch.ObjectiveBankBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractObjectiveBankBatchFormFilterList</code>.
     *
     *  @param objectiveBankBatchFormList an <code>ObjectiveBankBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankBatchFormList</code> is <code>null</code>
     */

    protected AbstractObjectiveBankBatchFormFilterList(org.osid.learning.batch.ObjectiveBankBatchFormList objectiveBankBatchFormList) {
        nullarg(objectiveBankBatchFormList, "objective bank batch form list");
        this.list = objectiveBankBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.objectiveBankBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ObjectiveBankBatchForm </code> in this list. 
     *
     *  @return the next <code> ObjectiveBankBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ObjectiveBankBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.batch.ObjectiveBankBatchForm getNextObjectiveBankBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.learning.batch.ObjectiveBankBatchForm objectiveBankBatchForm = this.objectiveBankBatchForm;
            this.objectiveBankBatchForm = null;
            return (objectiveBankBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in objective bank batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.objectiveBankBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ObjectiveBankBatchForms.
     *
     *  @param objectiveBankBatchForm the objective bank batch form to filter
     *  @return <code>true</code> if the objective bank batch form passes the filter,
     *          <code>false</code> if the objective bank batch form should be filtered
     */

    public abstract boolean pass(org.osid.learning.batch.ObjectiveBankBatchForm objectiveBankBatchForm);


    protected void prime() {
        if (this.objectiveBankBatchForm != null) {
            return;
        }

        org.osid.learning.batch.ObjectiveBankBatchForm objectiveBankBatchForm = null;

        while (this.list.hasNext()) {
            try {
                objectiveBankBatchForm = this.list.getNextObjectiveBankBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(objectiveBankBatchForm)) {
                this.objectiveBankBatchForm = objectiveBankBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
