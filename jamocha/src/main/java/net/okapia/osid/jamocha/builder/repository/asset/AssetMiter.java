//
// AssetMiter.java
//
//     Defines an Asset miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.asset;


/**
 *  Defines an <code>Asset</code> miter for use with the builders.
 */

public interface AssetMiter
    extends net.okapia.osid.jamocha.builder.spi.SourceableOsidObjectMiter,
            org.osid.repository.Asset {


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the copyright status known flag.
     *
     *  @param status <code> true </code> if the copyright status of
     *         this asset is known, <code> false </code> otherwise. If
     *         <code> false, isPublicDomain(), </code> <code>
     *         canDistributeVerbatim(), canDistributeAlterations() and
     *         canDistributeCompositions() </code> may also be <code>
     *         false. </code>
     */

    public void setCopyrightStatusKnown(boolean status);


    /**
     *  Sets the public domain flag and enables all the distribution
     *  rights if <code>true</code>.
     *
     *  @param publicDomain <code> true </code> if this asset is in
     *         the public domain, <code> false </code> otherwise. If
     *         <code> true, </code> <code> canDistributeVerbatim(),
     *         canDistributeAlterations() and
     *         canDistributeCompositions() </code> must also be <code>
     *         true.  </code>
     */

    public void setPublicDomain(boolean publicDomain);


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    public void setCopyright(org.osid.locale.DisplayText copyright);


    /**
     *  Sets the copyright registration.
     *
     *  @param registration a copyright registration
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    public void setCopyrightRegistration(String registration);


    /**
     *  Sets the can distribute verbatim flag.
     *
     *  @param status <code> true </code> if the asset can be
     *         distributed verbatim, <code> false </code> otherwise.
     */

    public void setCanDistributeVerbatim(boolean status);


    /**
     *  Sets the can distribute alterations flag.
     *
     *  @param status <code> true </code> if the asset can be
     *         modified, <code> false </code> otherwise. If <code>
     *         true, </code> <code> canDistributeVerbatim() </code>
     *         must also be <code> true.  </code>
     */

    public void setCanDistributeAlterations(boolean status);


    /**
     *  Sets the can distribute compositions flag.
     *
     *  @param status <code> true </code> if the asset can be part of
     *         a larger composition <code> false </code> otherwise. If
     *         <code> true, </code> <code> canDistributeVerbatim()
     *         </code> must also be <code> true. </code>
     */

    public void setCanDistributeCompositions(boolean status);


    /**
     *  Sets all distribution rights.
     * 
     *  @param canDistributeVerbatim <code> true </code> if the asset
     *         can be distributed verbatim, <code> false </code>
     *         otherwise.
     *  @param canDistributeAlterations <code> true </code> if the
     *         asset can be modified, <code> false </code>
     *         otherwise. If <code> true, </code> <code>
     *         canDistributeVerbatim() </code> must also be <code>
     *         true.  </code>
     *  @param canDistributeCompositions status <code> true </code> if
     *         the asset can be part of a larger composition <code>
     *         false </code> otherwise. If <code> true, </code> <code>
     *         canDistributeVerbatim() </code> must also be <code>
     *         true. </code>
     *  @throws org.osid.BadLogicException 
     */

    public void setDistributionRights(boolean canDistributeVerbatim,
                                      boolean canDistributeAlterations,
                                      boolean canDistributeCompositions);

    /**
     *  Sets the source.
     *
     *  @param source a source
     *  @throws org.osid.NullArgumentException <code>source</code> is
     *          <code>null</code>
     */

    public void setSource(org.osid.resource.Resource source);


    /**
     *  Adds a provider link.
     *
     *  @param provider a provider link
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    public void addProviderLink(org.osid.resource.Resource provider);


    /**
     *  Sets all the provider links.
     *
     *  @param providers a collection of provider links
     *  @throws org.osid.NullArgumentException <code>providers</code>
     *          is <code>null</code>
     */

    public void setProviderLinks(java.util.Collection<org.osid.resource.Resource> providers);


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setCreatedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the published date.
     *
     *  @param date a published date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    public void setPublishedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the principal credit string.
     *
     *  @param credits a principal credit string
     *  @throws org.osid.NullArgumentException
     *          <code>credits</code> is <code>null</code>
     */

    public void setPrincipalCreditString(org.osid.locale.DisplayText credits);


    /**
     *  Adds an asset content.
     *
     *  @param assetContent an asset content
     *  @throws org.osid.NullArgumentException
     *          <code>assetContent</code> is <code>null</code>
     */

    public void addAssetContent(org.osid.repository.AssetContent assetContent);


    /**
     *  Sets all the asset contents.
     *
     *  @param assetContents a collection of asset contents
     *  @throws org.osid.NullArgumentException
     *          <code>assetContents</code> is <code>null</code>
     */

    public void setAssetContents(java.util.Collection<org.osid.repository.AssetContent> assetContents);


    /**
     *  Sets the composition.
     *
     *  @param composition a composition
     *  @throws org.osid.NullArgumentException
     *          <code>composition</code> is <code>null</code>
     */

    public void setComposition(org.osid.repository.Composition composition);


    /**
     *  Adds an Asset record.
     *
     *  @param record an asset record
     *  @param recordType the type of asset record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssetRecord(org.osid.repository.records.AssetRecord record, org.osid.type.Type recordType);
}       


