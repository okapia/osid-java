//
// AbstractMapCalendarLookupSession
//
//    A simple framework for providing a Calendar lookup service
//    backed by a fixed collection of calendars.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Calendar lookup service backed by a
 *  fixed collection of calendars. The calendars are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Calendars</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCalendarLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCalendarLookupSession
    implements org.osid.calendaring.CalendarLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.Calendar> calendars = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.Calendar>());


    /**
     *  Makes a <code>Calendar</code> available in this session.
     *
     *  @param  calendar a calendar
     *  @throws org.osid.NullArgumentException <code>calendar<code>
     *          is <code>null</code>
     */

    protected void putCalendar(org.osid.calendaring.Calendar calendar) {
        this.calendars.put(calendar.getId(), calendar);
        return;
    }


    /**
     *  Makes an array of calendars available in this session.
     *
     *  @param  calendars an array of calendars
     *  @throws org.osid.NullArgumentException <code>calendars<code>
     *          is <code>null</code>
     */

    protected void putCalendars(org.osid.calendaring.Calendar[] calendars) {
        putCalendars(java.util.Arrays.asList(calendars));
        return;
    }


    /**
     *  Makes a collection of calendars available in this session.
     *
     *  @param  calendars a collection of calendars
     *  @throws org.osid.NullArgumentException <code>calendars<code>
     *          is <code>null</code>
     */

    protected void putCalendars(java.util.Collection<? extends org.osid.calendaring.Calendar> calendars) {
        for (org.osid.calendaring.Calendar calendar : calendars) {
            this.calendars.put(calendar.getId(), calendar);
        }

        return;
    }


    /**
     *  Removes a Calendar from this session.
     *
     *  @param  calendarId the <code>Id</code> of the calendar
     *  @throws org.osid.NullArgumentException <code>calendarId<code> is
     *          <code>null</code>
     */

    protected void removeCalendar(org.osid.id.Id calendarId) {
        this.calendars.remove(calendarId);
        return;
    }


    /**
     *  Gets the <code>Calendar</code> specified by its <code>Id</code>.
     *
     *  @param  calendarId <code>Id</code> of the <code>Calendar</code>
     *  @return the calendar
     *  @throws org.osid.NotFoundException <code>calendarId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>calendarId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.Calendar calendar = this.calendars.get(calendarId);
        if (calendar == null) {
            throw new org.osid.NotFoundException("calendar not found: " + calendarId);
        }

        return (calendar);
    }


    /**
     *  Gets all <code>Calendars</code>. In plenary mode, the returned
     *  list contains all known calendars or an error
     *  results. Otherwise, the returned list may contain only those
     *  calendars that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Calendars</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendars()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.calendar.ArrayCalendarList(this.calendars.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.calendars.clear();
        super.close();
        return;
    }
}
