//
// AbstractTriggerEnablerQueryInspector.java
//
//     A template for making a TriggerEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.triggerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for trigger enablers.
 */

public abstract class AbstractTriggerEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.control.rules.TriggerEnablerQueryInspector {

    private final java.util.Collection<org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the trigger <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledTriggerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the trigger query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.TriggerQueryInspector[] getRuledTriggerTerms() {
        return (new org.osid.control.TriggerQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given trigger enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a trigger enabler implementing the requested record.
     *
     *  @param triggerEnablerRecordType a trigger enabler record type
     *  @return the trigger enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord getTriggerEnablerQueryInspectorRecord(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(triggerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this trigger enabler query. 
     *
     *  @param triggerEnablerQueryInspectorRecord trigger enabler query inspector
     *         record
     *  @param triggerEnablerRecordType triggerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTriggerEnablerQueryInspectorRecord(org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord triggerEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type triggerEnablerRecordType) {

        addRecordType(triggerEnablerRecordType);
        nullarg(triggerEnablerRecordType, "trigger enabler record type");
        this.records.add(triggerEnablerQueryInspectorRecord);        
        return;
    }
}
