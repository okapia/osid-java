//
// AbstractQueryRouteLookupSession.java
//
//    An inline adapter that maps a RouteLookupSession to
//    a RouteQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RouteLookupSession to
 *  a RouteQuerySession.
 */

public abstract class AbstractQueryRouteLookupSession
    extends net.okapia.osid.jamocha.mapping.route.spi.AbstractRouteLookupSession
    implements org.osid.mapping.route.RouteLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.mapping.route.RouteQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRouteLookupSession.
     *
     *  @param querySession the underlying route query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRouteLookupSession(org.osid.mapping.route.RouteQuerySession querySession) {
        nullarg(querySession, "route query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Map</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform <code>Route</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRoutes() {
        return (this.session.canSearchRoutes());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include routes in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only routes whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRouteView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All routes of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRouteView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Route</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Route</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Route</code> and
     *  retained for compatibility.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and
     *  those currently expired are returned.
     *
     *  @param  routeId <code>Id</code> of the
     *          <code>Route</code>
     *  @return the route
     *  @throws org.osid.NotFoundException <code>routeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>routeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.Route getRoute(org.osid.id.Id routeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchId(routeId, true);
        org.osid.mapping.route.RouteList routes = this.session.getRoutesByQuery(query);
        if (routes.hasNext()) {
            return (routes.getNextRoute());
        } 
        
        throw new org.osid.NotFoundException(routeId + " not found");
    }


    /**
     *  Gets a <code>RouteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  routes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Routes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, routes are returned that are currently effective.
     *  In any effective mode, effective routes and those currently expired
     *  are returned.
     *
     *  @param  routeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>routeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByIds(org.osid.id.IdList routeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();

        try (org.osid.id.IdList ids = routeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a <code>RouteList</code> corresponding to the given
     *  route genus <code>Type</code> which does not include
     *  routes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, routes are returned that are currently effective.
     *  In any effective mode, effective routes and those currently expired
     *  are returned.
     *
     *  @param  routeGenusType a route genus type 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByGenusType(org.osid.type.Type routeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchGenusType(routeGenusType, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a <code>RouteList</code> corresponding to the given route
     *  genus <code>Type</code> and include any additional routes with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  routeGenusType a route genus type 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByParentGenusType(org.osid.type.Type routeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchParentGenusType(routeGenusType, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a <code>RouteList</code> containing the given route
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and
     *  those currently expired are returned.
     *
     *  @param  routeRecordType a route record type 
     *  @return the returned <code>Route</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByRecordType(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchRecordType(routeRecordType, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a <code>RouteList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *  
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Route</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRoutesByQuery(query));
    }
        

    /**
     *  Gets a list of routes corresponding to a starting location
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param startingLocationId the <code>Id</code> of the starting
     *         location
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingLocationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.mapping.route.RouteList getRoutesForStartingLocation(org.osid.id.Id startingLocationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchStartingLocationId(startingLocationId, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a list of routes corresponding to a starting location
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible
     *  through this session.
     *
     *  In effective mode, routes are returned that are
     *  currently effective.  In any effective mode, effective
     *  routes and those currently expired are returned.
     *
     *  @param startingLocationId the <code>Id</code> of the starting
     *         location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingLocationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingLocationOnDate(org.osid.id.Id startingLocationId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchStartingLocationId(startingLocationId, true);
        query.matchDate(from, to, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a list of routes corresponding to a ending location
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are
     *  currently effective.  In any effective mode, effective
     *  routes and those currently expired are returned.
     *
     *  @param  endingLocationId the <code>Id</code> of the ending location
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingLocationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.mapping.route.RouteList getRoutesForEndingLocation(org.osid.id.Id endingLocationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchEndingLocationId(endingLocationId, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a list of routes corresponding to a ending location
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param endingLocationId the <code>Id</code> of the ending
     *        location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingLocationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForEndingLocationOnDate(org.osid.id.Id endingLocationId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchEndingLocationId(endingLocationId, true);
        query.matchDate(from, to, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a list of routes corresponding to starting location and
     *  ending location <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param startingLocationId the <code>Id</code> of the starting
     *         location
     *  @param endingLocationId the <code>Id</code> of the ending
     *         location
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingLocationId</code>,
     *          <code>endingLocationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingAndEndingLocation(org.osid.id.Id startingLocationId,
                                                                                  org.osid.id.Id endingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchStartingLocationId(startingLocationId, true);
        query.matchEndingLocationId(endingLocationId, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a list of routes corresponding to starting location and
     *  ending location <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible
     *  through this session.
     *
     *  In effective mode, routes are returned that are
     *  currently effective.  In any effective mode, effective
     *  routes and those currently expired are returned.
     *
     *  @param startingLocationId the <code>Id</code> of the starting
     *         location
     *  @param endingLocationId the <code>Id</code> of the ending
     *         location
     *  @param from from date
     *  @param to to date
     *  @return the returned <code>RouteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingLocationId</code>,
     *          <code>endingLocationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingAndEndingLocationOnDate(org.osid.id.Id startingLocationId,
                                                                                        org.osid.id.Id endingLocationId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchStartingLocationId(startingLocationId, true);
        query.matchEndingLocationId(endingLocationId, true);
        query.matchDate(from, to, true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets a <code> RouteList </code> connected to all the given
     *  <code> Locations. </code>
     *  
     *  In plenary mode, the returned list contains all of the routes
     *  along the locations, or an error results if a route connected
     *  to the location is not found or inaccessible. Otherwise,
     *  inaccessible <code> Routes </code> may be omitted from the
     *  list.
     *  
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  locationIds the list of <code> Ids </code> to retrieve 
     *  @return the returned <code> Route </code> list 
     *  @throws org.osid.NullArgumentException <code> locationIds </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesAlongLocations(org.osid.id.IdList locationIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.nil.mapping.route.route.EmptyRouteList());
    }


    /**
     *  Gets all <code>Routes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Routes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.route.RouteQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRoutesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.mapping.route.RouteQuery getQuery() {
        org.osid.mapping.route.RouteQuery query = this.session.getRouteQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
