//
// AbstractOsidBrowsableForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A basic OsidBrowsableForm.
 */

public abstract class AbstractOsidBrowsableForm
    extends AbstractOsidExtensibleForm
    implements org.osid.OsidBrowsableForm {
    

    /** 
     *  Constructs a new {@code AbstractOsidBrowsableForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidBrowsableForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }
}
