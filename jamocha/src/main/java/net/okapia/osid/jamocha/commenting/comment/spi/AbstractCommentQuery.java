//
// AbstractCommentQuery.java
//
//     A template for making a Comment Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for comments.
 */

public abstract class AbstractCommentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.commenting.CommentQuery {

    private final java.util.Collection<org.osid.commenting.records.CommentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets reference <code> Id. </code> 
     *
     *  @param  sourceId a source <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id sourceId, boolean match) {
        return;
    }


    /**
     *  Clears the reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        return;
    }


    /**
     *  Sets a resource <code> Id </code> to match a commentor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommentorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommentorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCommentorQuery() {
        throw new org.osid.UnimplementedException("supportsCommentorQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearCommentorTerms() {
        return;
    }


    /**
     *  Sets an agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommentingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommentingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getCommentingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsCommentingAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearCommentingAgentTerms() {
        return;
    }


    /**
     *  Matches text. 
     *
     *  @param  text the text 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches a comment that has any text assigned. 
     *
     *  @param  match <code> true </code> to match comments with any text, 
     *          <code> false </code> to match comments with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        return;
    }


    /**
     *  Sets a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRatingId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears the rating <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRatingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a rating query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRatingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rating query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the rating query 
     *  @throws org.osid.UnimplementedException <code> supportsRatingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getRatingQuery() {
        throw new org.osid.UnimplementedException("supportsRatingQuery() is false");
    }


    /**
     *  Matches books with any rating. 
     *
     *  @param  match <code> true </code> to match comments with any rating, 
     *          <code> false </code> to match comments with no ratings 
     */

    @OSID @Override
    public void matchAnyRating(boolean match) {
        return;
    }


    /**
     *  Clears the rating terms. 
     */

    @OSID @Override
    public void clearRatingTerms() {
        return;
    }


    /**
     *  Sets the book <code> Id </code> for this query to match comments 
     *  assigned to books. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBookId(org.osid.id.Id bookId, boolean match) {
        return;
    }


    /**
     *  Clears the book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BookQuery </code> is available. 
     *
     *  @return <code> true </code> if a book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a book query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the book query 
     *  @throws org.osid.UnimplementedException <code> supportsBookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuery getBookQuery() {
        throw new org.osid.UnimplementedException("supportsBookQuery() is false");
    }


    /**
     *  Clears the book terms. 
     */

    @OSID @Override
    public void clearBookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given comment query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a comment implementing the requested record.
     *
     *  @param commentRecordType a comment record type
     *  @return the comment query record
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentQueryRecord getCommentQueryRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.CommentQueryRecord record : this.records) {
            if (record.implementsRecordType(commentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this comment query. 
     *
     *  @param commentQueryRecord comment query record
     *  @param commentRecordType comment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommentQueryRecord(org.osid.commenting.records.CommentQueryRecord commentQueryRecord, 
                                          org.osid.type.Type commentRecordType) {

        addRecordType(commentRecordType);
        nullarg(commentQueryRecord, "comment query record");
        this.records.add(commentQueryRecord);        
        return;
    }
}
