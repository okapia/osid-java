//
// AbstractQueryAccountLookupSession.java
//
//    An inline adapter that maps an AccountLookupSession to
//    an AccountQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AccountLookupSession to
 *  an AccountQuerySession.
 */

public abstract class AbstractQueryAccountLookupSession
    extends net.okapia.osid.jamocha.financials.spi.AbstractAccountLookupSession
    implements org.osid.financials.AccountLookupSession {

    private final org.osid.financials.AccountQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAccountLookupSession.
     *
     *  @param querySession the underlying account query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAccountLookupSession(org.osid.financials.AccountQuerySession querySession) {
        nullarg(querySession, "account query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Account</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAccounts() {
        return (this.session.canSearchAccounts());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include accounts in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the <code>Account</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Account</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Account</code> and
     *  retained for compatibility.
     *
     *  @param  accountId <code>Id</code> of the
     *          <code>Account</code>
     *  @return the account
     *  @throws org.osid.NotFoundException <code>accountId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.AccountQuery query = getQuery();
        query.matchId(accountId, true);
        org.osid.financials.AccountList accounts = this.session.getAccountsByQuery(query);
        if (accounts.hasNext()) {
            return (accounts.getNextAccount());
        } 
        
        throw new org.osid.NotFoundException(accountId + " not found");
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  accounts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Accounts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  accountIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>accountIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByIds(org.osid.id.IdList accountIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.AccountQuery query = getQuery();

        try (org.osid.id.IdList ids = accountIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAccountsByQuery(query));
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  account genus <code>Type</code> which does not include
     *  accounts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.AccountQuery query = getQuery();
        query.matchGenusType(accountGenusType, true);
        return (this.session.getAccountsByQuery(query));
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  account genus <code>Type</code> and include any additional
     *  accounts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByParentGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.AccountQuery query = getQuery();
        query.matchParentGenusType(accountGenusType, true);
        return (this.session.getAccountsByQuery(query));
    }


    /**
     *  Gets an <code>AccountList</code> containing the given
     *  account record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountRecordType an account record type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByRecordType(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.AccountQuery query = getQuery();
        query.matchRecordType(accountRecordType, true);
        return (this.session.getAccountsByQuery(query));
    }


    /**
     *  Gets an <code> AccountList </code> associated with the given
     *  code. In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list may
     *  contain only those accounts that are accessible through this
     *  session.
     *
     *  @param  code n account code 
     *  @return the returned <code> Account </code> list 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.AccountQuery query = getQuery();
        query.matchCode(code, getStringMatchType(), true);
        return (this.session.getAccountsByQuery(query));
    }


    /**
     *  Gets all <code>Accounts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Accounts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccounts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.AccountQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAccountsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.financials.AccountQuery getQuery() {
        org.osid.financials.AccountQuery query = this.session.getAccountQuery();
        return (query);
    }


    /**
     *  Gets the string match type for use in string queries.
     *
     *  @return a type
     */

    protected org.osid.type.Type getStringMatchType() {
        return (net.okapia.osid.primordium.types.search.StringMatchTypes.EXACT.getType());
    }
}
