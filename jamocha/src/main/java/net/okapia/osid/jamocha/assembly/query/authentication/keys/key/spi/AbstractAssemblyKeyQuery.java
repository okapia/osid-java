//
// AbstractAssemblyKeyQuery.java
//
//     A KeyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A KeyQuery that stores terms.
 */

public abstract class AbstractAssemblyKeyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.authentication.keys.KeyQuery,
               org.osid.authentication.keys.KeyQueryInspector,
               org.osid.authentication.keys.KeySearchOrder {

    private final java.util.Collection<org.osid.authentication.keys.records.KeyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authentication.keys.records.KeyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authentication.keys.records.KeySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyKeyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyKeyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Includes an agent query for making relations with <code> Agents. 
     *  </code> Multiple retrievals return separate query terms nested inside 
     *  this query term, each which are treated as a boolean <code> OR. 
     *  </code> 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an <code> AgentSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an agent search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Tests if this key supports the given record
     *  <code>Type</code>.
     *
     *  @param  keyRecordType a key record type 
     *  @return <code>true</code> if the keyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type keyRecordType) {
        for (org.osid.authentication.keys.records.KeyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(keyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  keyRecordType the key record type 
     *  @return the key query record 
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(keyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeyQueryRecord getKeyQueryRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.keys.records.KeyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(keyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  keyRecordType the key record type 
     *  @return the key query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(keyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeyQueryInspectorRecord getKeyQueryInspectorRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.keys.records.KeyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(keyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param keyRecordType the key record type
     *  @return the key search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(keyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeySearchOrderRecord getKeySearchOrderRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.keys.records.KeySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(keyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this key. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param keyQueryRecord the key query record
     *  @param keyQueryInspectorRecord the key query inspector
     *         record
     *  @param keySearchOrderRecord the key search order record
     *  @param keyRecordType key record type
     *  @throws org.osid.NullArgumentException
     *          <code>keyQueryRecord</code>,
     *          <code>keyQueryInspectorRecord</code>,
     *          <code>keySearchOrderRecord</code> or
     *          <code>keyRecordTypekey</code> is
     *          <code>null</code>
     */
            
    protected void addKeyRecords(org.osid.authentication.keys.records.KeyQueryRecord keyQueryRecord, 
                                      org.osid.authentication.keys.records.KeyQueryInspectorRecord keyQueryInspectorRecord, 
                                      org.osid.authentication.keys.records.KeySearchOrderRecord keySearchOrderRecord, 
                                      org.osid.type.Type keyRecordType) {

        addRecordType(keyRecordType);

        nullarg(keyQueryRecord, "key query record");
        nullarg(keyQueryInspectorRecord, "key query inspector record");
        nullarg(keySearchOrderRecord, "key search odrer record");

        this.queryRecords.add(keyQueryRecord);
        this.queryInspectorRecords.add(keyQueryInspectorRecord);
        this.searchOrderRecords.add(keySearchOrderRecord);
        
        return;
    }
}
