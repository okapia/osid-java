//
// IssueSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.issue;


/**
 *  A template for implementing a search results.
 */

public final class IssueSearchResults
    extends net.okapia.osid.jamocha.tracking.issue.spi.AbstractIssueSearchResults
    implements org.osid.tracking.IssueSearchResults {


    /**
     *  Constructs a new <code>IssueSearchResults.
     *
     *  @param issues the result set
     *  @param issueQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>issues</code>
     *          or <code>issueQueryInspector</code> is
     *          <code>null</code>
     */

    public IssueSearchResults(org.osid.tracking.IssueList issues,
                                 org.osid.tracking.IssueQueryInspector issueQueryInspector) {
        super(issues, issueQueryInspector);
        return;
    }
}
