//
// AbstractAssemblyCommissionEnablerQuery.java
//
//     A CommissionEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.rules.commissionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CommissionEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyCommissionEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.resourcing.rules.CommissionEnablerQuery,
               org.osid.resourcing.rules.CommissionEnablerQueryInspector,
               org.osid.resourcing.rules.CommissionEnablerSearchOrder {

    private final java.util.Collection<org.osid.resourcing.rules.records.CommissionEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.CommissionEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCommissionEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCommissionEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to a commission. 
     *
     *  @param  foundryId the commission <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCommissionId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getRuledCommissionIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the commission <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCommissionIdTerms() {
        getAssembler().clearTerms(getRuledCommissionIdColumn());
        return;
    }


    /**
     *  Gets the commission <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCommissionIdTerms() {
        return (getAssembler().getIdTerms(getRuledCommissionIdColumn()));
    }


    /**
     *  Gets the RuledCommissionId column name.
     *
     * @return the column name
     */

    protected String getRuledCommissionIdColumn() {
        return ("ruled_commission_id");
    }


    /**
     *  Tests if an <code> CommissionQuery </code> is available. 
     *
     *  @return <code> true </code> if a commission query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCommissionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commission. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commission query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuery getRuledCommissionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCommissionQuery() is false");
    }


    /**
     *  Matches mapped to any commission. 
     *
     *  @param  match <code> true </code> for mapped to any commission, <code> 
     *          false </code> to match mapped to no commissions 
     */

    @OSID @Override
    public void matchAnyRuledCommission(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledCommissionColumn(), match);
        return;
    }


    /**
     *  Clears the commission query terms. 
     */

    @OSID @Override
    public void clearRuledCommissionTerms() {
        getAssembler().clearTerms(getRuledCommissionColumn());
        return;
    }


    /**
     *  Gets the commission query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQueryInspector[] getRuledCommissionTerms() {
        return (new org.osid.resourcing.CommissionQueryInspector[0]);
    }


    /**
     *  Gets the RuledCommission column name.
     *
     * @return the column name
     */

    protected String getRuledCommissionColumn() {
        return ("ruled_commission");
    }


    /**
     *  Matches mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this commissionEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  commissionEnablerRecordType a commission enabler record type 
     *  @return <code>true</code> if the commissionEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commissionEnablerRecordType) {
        for (org.osid.resourcing.rules.records.CommissionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commissionEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  commissionEnablerRecordType the commission enabler record type 
     *  @return the commission enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.CommissionEnablerQueryRecord getCommissionEnablerQueryRecord(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.CommissionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commissionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  commissionEnablerRecordType the commission enabler record type 
     *  @return the commission enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord getCommissionEnablerQueryInspectorRecord(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(commissionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param commissionEnablerRecordType the commission enabler record type
     *  @return the commission enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.CommissionEnablerSearchOrderRecord getCommissionEnablerSearchOrderRecord(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.CommissionEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(commissionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this commission enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commissionEnablerQueryRecord the commission enabler query record
     *  @param commissionEnablerQueryInspectorRecord the commission enabler query inspector
     *         record
     *  @param commissionEnablerSearchOrderRecord the commission enabler search order record
     *  @param commissionEnablerRecordType commission enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerQueryRecord</code>,
     *          <code>commissionEnablerQueryInspectorRecord</code>,
     *          <code>commissionEnablerSearchOrderRecord</code> or
     *          <code>commissionEnablerRecordTypecommissionEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addCommissionEnablerRecords(org.osid.resourcing.rules.records.CommissionEnablerQueryRecord commissionEnablerQueryRecord, 
                                      org.osid.resourcing.rules.records.CommissionEnablerQueryInspectorRecord commissionEnablerQueryInspectorRecord, 
                                      org.osid.resourcing.rules.records.CommissionEnablerSearchOrderRecord commissionEnablerSearchOrderRecord, 
                                      org.osid.type.Type commissionEnablerRecordType) {

        addRecordType(commissionEnablerRecordType);

        nullarg(commissionEnablerQueryRecord, "commission enabler query record");
        nullarg(commissionEnablerQueryInspectorRecord, "commission enabler query inspector record");
        nullarg(commissionEnablerSearchOrderRecord, "commission enabler search odrer record");

        this.queryRecords.add(commissionEnablerQueryRecord);
        this.queryInspectorRecords.add(commissionEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(commissionEnablerSearchOrderRecord);
        
        return;
    }
}
