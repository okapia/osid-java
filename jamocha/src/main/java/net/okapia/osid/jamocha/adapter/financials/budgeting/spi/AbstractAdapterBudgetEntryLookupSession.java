//
// AbstractAdapterBudgetEntryLookupSession.java
//
//    A BudgetEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A BudgetEntry lookup session adapter.
 */

public abstract class AbstractAdapterBudgetEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.financials.budgeting.BudgetEntryLookupSession {

    private final org.osid.financials.budgeting.BudgetEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBudgetEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBudgetEntryLookupSession(org.osid.financials.budgeting.BudgetEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code BudgetEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBudgetEntries() {
        return (this.session.canLookupBudgetEntries());
    }


    /**
     *  A complete view of the {@code BudgetEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBudgetEntryView() {
        this.session.useComparativeBudgetEntryView();
        return;
    }


    /**
     *  A complete view of the {@code BudgetEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBudgetEntryView() {
        this.session.usePlenaryBudgetEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include budget entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only budget entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveBudgetEntryView() {
        this.session.useEffectiveBudgetEntryView();
        return;
    }
    

    /**
     *  All budget entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveBudgetEntryView() {
        this.session.useAnyEffectiveBudgetEntryView();
        return;
    }

     
    /**
     *  Gets the {@code BudgetEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code BudgetEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code BudgetEntry} and
     *  retained for compatibility.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param budgetEntryId {@code Id} of the {@code BudgetEntry}
     *  @return the budget entry
     *  @throws org.osid.NotFoundException {@code budgetEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code budgetEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntry getBudgetEntry(org.osid.id.Id budgetEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntry(budgetEntryId));
    }


    /**
     *  Gets a {@code BudgetEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  budgetEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code BudgetEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  budgetEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code BudgetEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code budgetEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByIds(org.osid.id.IdList budgetEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesByIds(budgetEntryIds));
    }


    /**
     *  Gets a {@code BudgetEntryList} corresponding to the given
     *  budget entry genus {@code Type} which does not include
     *  budget entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  budgetEntryGenusType a budgetEntry genus type 
     *  @return the returned {@code BudgetEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code budgetEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByGenusType(org.osid.type.Type budgetEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesByGenusType(budgetEntryGenusType));
    }


    /**
     *  Gets a {@code BudgetEntryList} corresponding to the given
     *  budget entry genus {@code Type} and include any additional
     *  budget entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  budgetEntryGenusType a budgetEntry genus type 
     *  @return the returned {@code BudgetEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code budgetEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByParentGenusType(org.osid.type.Type budgetEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesByParentGenusType(budgetEntryGenusType));
    }


    /**
     *  Gets a {@code BudgetEntryList} containing the given
     *  budget entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  budgetEntryRecordType a budgetEntry record type 
     *  @return the returned {@code BudgetEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code budgetEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByRecordType(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesByRecordType(budgetEntryRecordType));
    }


    /**
     *  Gets a {@code BudgetEntryList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *  
     *  In active mode, budget entries are returned that are currently
     *  active. In any status mode, active and inactive budget entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code BudgetEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of budget entries corresponding to a budget
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the {@code Id} of the budget
     *  @return the returned {@code BudgetEntryList}
     *  @throws org.osid.NullArgumentException {@code budgetId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudget(org.osid.id.Id budgetId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesForBudget(budgetId));
    }


    /**
     *  Gets a list of budget entries corresponding to a budget
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the {@code Id} of the budget
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code BudgetEntryList}
     *  @throws org.osid.NullArgumentException {@code budgetId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetOnDate(org.osid.id.Id budgetId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesForBudgetOnDate(budgetId, from, to));
    }


    /**
     *  Gets a list of budget entries corresponding to an account
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  accountId the {@code Id} of the account
     *  @return the returned {@code BudgetEntryList}
     *  @throws org.osid.NullArgumentException {@code accountId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForAccount(org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesForAccount(accountId));
    }


    /**
     *  Gets a list of budget entries corresponding to an account
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  accountId the {@code Id} of the account
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code BudgetEntryList}
     *  @throws org.osid.NullArgumentException {@code accountId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForAccountOnDate(org.osid.id.Id accountId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesForAccountOnDate(accountId, from, to));
    }


    /**
     *  Gets a list of budget entries corresponding to budget and account
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the {@code Id} of the budget
     *  @param  accountId the {@code Id} of the account
     *  @return the returned {@code BudgetEntryList}
     *  @throws org.osid.NullArgumentException {@code budgetId},
     *          {@code accountId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetAndAccount(org.osid.id.Id budgetId,
                                                                                             org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesForBudgetAndAccount(budgetId, accountId));
    }


    /**
     *  Gets a list of budget entries corresponding to budget and account
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective. In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  accountId the {@code Id} of the account
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code BudgetEntryList}
     *  @throws org.osid.NullArgumentException {@code budgetId},
     *          {@code accountId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetAndAccountOnDate(org.osid.id.Id budgetId,
                                                                                                   org.osid.id.Id accountId,
                                                                                                   org.osid.calendaring.DateTime from,
                                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntriesForBudgetAndAccountOnDate(budgetId, accountId, from, to));
    }


    /**
     *  Gets all {@code BudgetEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code BudgetEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetEntries());
    }
}
