//
// AbstractImmutableIssue.java
//
//     Wraps a mutable Issue to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.issue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Issue</code> to hide modifiers. This
 *  wrapper provides an immutized Issue from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying issue whose state changes are visible.
 */

public abstract class AbstractImmutableIssue
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.tracking.Issue {

    private final org.osid.tracking.Issue issue;


    /**
     *  Constructs a new <code>AbstractImmutableIssue</code>.
     *
     *  @param issue the issue to immutablize
     *  @throws org.osid.NullArgumentException <code>issue</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableIssue(org.osid.tracking.Issue issue) {
        super(issue);
        this.issue = issue;
        return;
    }


    /**
     *  Gets the queue <code> Id. </code> 
     *
     *  @return the queue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQueueId() {
        return (this.issue.getQueueId());
    }


    /**
     *  Gets the queue. 
     *
     *  @return the queue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Queue getQueue()
        throws org.osid.OperationFailedException {

        return (this.issue.getQueue());
    }


    /**
     *  Gets the customer <code> Id. </code> 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.issue.getCustomerId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCustomer()
        throws org.osid.OperationFailedException {

        return (this.issue.getCustomer());
    }


    /**
     *  Gets the topic <code> Id. </code> 
     *
     *  @return the topic <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTopicId() {
        return (this.issue.getTopicId());
    }


    /**
     *  Gets the topic. 
     *
     *  @return the topic 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ontology.Subject getTopic()
        throws org.osid.OperationFailedException {

        return (this.issue.getTopic());
    }


    /**
     *  Tests if this issue is a subtask of another issue. 
     *
     *  @return <code> true </code> if this issue is a subtask, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isSubTask() {
        return (this.issue.isSubTask());
    }


    /**
     *  Gets the master issue <code> Id </code> . If <code> isSubtask() 
     *  </code> is <code> false </code> then the <code> Id </code> of this 
     *  <code> Issue </code> is returned. 
     *
     *  @return the master issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMasterIssueId() {
        return (this.issue.getMasterIssueId());
    }


    /**
     *  Gets the master issue. If <code> isSubtask() </code> is <code> false 
     *  </code> then this <code> Issue </code> is returned. 
     *
     *  @return the master issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Issue getMasterIssue()
        throws org.osid.OperationFailedException {

        return (this.issue.getMasterIssue());
    }


    /**
     *  Tests if this issue is a duplicate of another issue. 
     *
     *  @return <code> true </code> if this issue is a duplicate, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isDuplicate() {
        return (this.issue.isDuplicate());
    }


    /**
     *  Gets the duplicate issue <code> Ids. </code> 
     *
     *  @return the duplicate issue <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isDuplicate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getDuplicateIssueIds() {
        return (this.issue.getDuplicateIssueIds());
    }


    /**
     *  Gets the duplicate issues. 
     *
     *  @return the duplicate issues 
     *  @throws org.osid.IllegalStateException <code> isDuplicate() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getDuplicateIssues()
        throws org.osid.OperationFailedException {

        return (this.issue.getDuplicateIssues());
    }


    /**
     *  Tests if this issue is a branch of another issue. 
     *
     *  @return <code> true </code> if this issue is a branch, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isBranched() {
        return (this.issue.isBranched());
    }


    /**
     *  Gets the branched issue <code> Id. </code> 
     *
     *  @return the branched issue 
     *  @throws org.osid.IllegalStateException <code> isBranched() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBranchedIssueId() {
        return (this.issue.getBranchedIssueId());
    }


    /**
     *  Gets the branched issue. 
     *
     *  @return the branched issue 
     *  @throws org.osid.IllegalStateException <code> isBranched() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Issue getBranchedIssue()
        throws org.osid.OperationFailedException {

        return (this.issue.getBranchedIssue());
    }


    /**
     *  Gets the root issue <code> Id </code> . If <code> isBranched() </code> 
     *  is <code> false </code> then the <code> Id </code> of this <code> 
     *  Issue </code> is returned. 
     *
     *  @return the root issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRootIssueId() {
        return (this.issue.getRootIssueId());
    }


    /**
     *  Gets the root issue. If <code> isBranched() </code> is <code> false 
     *  </code> then this <code> Issue </code> is returned. 
     *
     *  @return the root issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Issue getRootIssue()
        throws org.osid.OperationFailedException {

        return (this.issue.getRootIssue());
    }


    /**
     *  Gets the priority type of this issue. 
     *
     *  @return the priority type 
     */

    @OSID @Override
    public org.osid.type.Type getPriorityType() {
        return (this.issue.getPriorityType());
    }


    /**
     *  Gets the creator <code> Id. </code> 
     *
     *  @return the creator <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreatorId() {
        return (this.issue.getCreatorId());
    }


    /**
     *  Gets the creator of this issue. 
     *
     *  @return the creator 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCreator()
        throws org.osid.OperationFailedException {

        return (this.issue.getCreator());
    }


    /**
     *  Gets the creating agent <code> Id. </code> 
     *
     *  @return the creatoing agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreatingAgentId() {
        return (this.issue.getCreatingAgentId());
    }


    /**
     *  Gets the creating agent of this issue. 
     *
     *  @return the creating agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getCreatingAgent()
        throws org.osid.OperationFailedException {

        return (this.issue.getCreatingAgent());
    }


    /**
     *  Gets the created date. 
     *
     *  @return the created date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedDate() {
        return (this.issue.getCreatedDate());
    }


    /**
     *  Tests if this issue has been resurrected after a close. A reopened 
     *  <code> Issue </code> extends the effective dates through to when it is 
     *  closed again. 
     *
     *  @return <code> true </code> if this issue is reopened, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isReopened() {
        return (this.issue.isReopened());
    }


    /**
     *  Gets the reopener <code> Id. </code> If reoepned more than once, this 
     *  method returns the last reopened event. 
     *
     *  @return the reopener <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isReopened() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReopenerId() {
        return (this.issue.getReopenerId());
    }


    /**
     *  Gets the reopener of this issue. If reoepned more than once, this 
     *  method returns the last reopened event. 
     *
     *  @return the reopener 
     *  @throws org.osid.IllegalStateException <code> isReopened() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getReopener()
        throws org.osid.OperationFailedException {

        return (this.issue.getReopener());
    }


    /**
     *  Gets the reopening agent <code> Id. </code> If reoepned more than 
     *  once, this method returns the last reopened event. 
     *
     *  @return the reopening agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isReopened() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReopeningAgentId() {
        return (this.issue.getReopeningAgentId());
    }


    /**
     *  Gets the reopening agent of this issue. If reoepned more than once, 
     *  this method returns the last reopened event. 
     *
     *  @return the reopening agent 
     *  @throws org.osid.IllegalStateException <code> isReopened() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getReopeningAgent()
        throws org.osid.OperationFailedException {

        return (this.issue.getReopeningAgent());
    }


    /**
     *  Gets the last reopened date. If reoepned more than once, this method 
     *  returns the last reopened event. 
     *
     *  @return the last reopened date 
     *  @throws org.osid.IllegalStateException <code> hasReopened() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReopenedDate() {
        return (this.issue.getReopenedDate());
    }


    /**
     *  Tests if this issue has a due date. 
     *
     *  @return <code> true </code> if this issue has a due date, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasDueDate() {
        return (this.issue.hasDueDate());
    }


    /**
     *  Gets the due date. 
     *
     *  @return the due date 
     *  @throws org.osid.IllegalStateException <code> hasDueDate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        return (this.issue.getDueDate());
    }


    /**
     *  Tests if this issue is pending a response from the customer. 
     *
     *  @return <code> true </code> if this issue is pending, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isPending() {
        return (this.issue.isPending());
    }


    /**
     *  Tests if this issue is blocked on another issue. 
     *
     *  @return <code> true </code> if this issue is blocked, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isBlocked() {
        return (this.issue.isBlocked());
    }


    /**
     *  Gets the blocker issue <code> Ids. </code> 
     *
     *  @return the blocking issue <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isBlocked() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getBlockerIds() {
        return (this.issue.getBlockerIds());
    }


    /**
     *  Gets the blocking issues. 
     *
     *  @return the blocking issues 
     *  @throws org.osid.IllegalStateException <code> isBlocked() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getBlockers()
        throws org.osid.OperationFailedException {

        return (this.issue.getBlockers());
    }


    /**
     *  Tests if this issue is resolved. If the issue was reopened, it is no 
     *  longer resolved. 
     *
     *  @return <code> true </code> if this issue is resolved, <code> false 
     *          </code> if unresolved 
     */

    @OSID @Override
    public boolean isResolved() {
        return (this.issue.isResolved());
    }


    /**
     *  Gets the resolver <code> Id. </code> 
     *
     *  @return the reopener <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isResolved() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResolverId() {
        return (this.issue.getResolverId());
    }


    /**
     *  Gets the resolver of this issue. 
     *
     *  @return the resolver 
     *  @throws org.osid.IllegalStateException <code> isResolved() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResolver()
        throws org.osid.OperationFailedException {

        return (this.issue.getResolver());
    }


    /**
     *  Gets the resolving agent <code> Id. </code> 
     *
     *  @return the resolving agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isResolved() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResolvingAgentId() {
        return (this.issue.getResolvingAgentId());
    }


    /**
     *  Gets the resolving agent of this issue. 
     *
     *  @return the resolving agent 
     *  @throws org.osid.IllegalStateException <code> isResolved() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getResolvingAgent()
        throws org.osid.OperationFailedException {

        return (this.issue.getResolvingAgent());
    }


    /**
     *  Gets the resolved date. A resolved issue is still open until it is 
     *  closed. 
     *
     *  @return the resolved date 
     *  @throws org.osid.IllegalStateException <code> isResolved() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getResolvedDate() {
        return (this.issue.getResolvedDate());
    }


    /**
     *  Gets a type indicating the resolution; "fixed," "canceled", "cannot 
     *  reproduce." 
     *
     *  @return the resolution type 
     *  @throws org.osid.IllegalStateException <code> isResolved() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.Type getResolutionType() {
        return (this.issue.getResolutionType());
    }


    /**
     *  Tests if this issue is closed. An issue may be left opened after being 
     *  resolved for acknowledgement or review. 
     *
     *  @return <code> true </code> if this issue is closed, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isClosed() {
        return (this.issue.isClosed());
    }


    /**
     *  Gets the resource <code> Id </code> of the closer. 
     *
     *  @return the closing resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCloserId() {
        return (this.issue.getCloserId());
    }


    /**
     *  Gets the resource of the closer. 
     *
     *  @return the closing resource 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCloser()
        throws org.osid.OperationFailedException {

        return (this.issue.getCloser());
    }


    /**
     *  Gets the agent <code> Id </code> of the closer. 
     *
     *  @return the closing agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getClosingAgentId() {
        return (this.issue.getClosingAgentId());
    }


    /**
     *  Gets the agent of the closer. 
     *
     *  @return the closing agent 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getClosingAgent()
        throws org.osid.OperationFailedException {

        return (this.issue.getClosingAgent());
    }


    /**
     *  Gets the closed date. 
     *
     *  @return the closed date 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClosedDate() {
        return (this.issue.getClosedDate());
    }


    /**
     *  Tests if this issue is assigned. 
     *
     *  @return <code> true </code> if this issue is assigned, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isAssigned() {
        return (this.issue.isAssigned());
    }


    /**
     *  Gets the assigned resource <code> Id. </code> 
     *
     *  @return the assigned resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isAssigned() </code> is 
     *          <code> false </code> or <code> isClosed() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssignedResourceId() {
        return (this.issue.getAssignedResourceId());
    }


    /**
     *  Gets the assigned resource. 
     *
     *  @return the assigned resource 
     *  @throws org.osid.IllegalStateException <code> isAssigned() </code> is 
     *          <code> false </code> or <code> isClosed() </code> is <code> 
     *          true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getAssignedResource()
        throws org.osid.OperationFailedException {

        return (this.issue.getAssignedResource());
    }


    /**
     *  Gets the issue record corresponding to the given <code> Issue </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> issueRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(issueRecordType) </code> is <code> true </code> . 
     *
     *  @param  issueRecordType the type of issue record to retrieve 
     *  @return the issue record 
     *  @throws org.osid.NullArgumentException <code> issueRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(issueRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.records.IssueRecord getIssueRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        return (this.issue.getIssueRecord(issueRecordType));
    }
}

