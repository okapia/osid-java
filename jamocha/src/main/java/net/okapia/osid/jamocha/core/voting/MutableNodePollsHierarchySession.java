//
// MutableNodePollsHierarchySession.java
//
//     Defines a Polls hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Defines a polls hierarchy session for delivering a hierarchy
 *  of polls using the PollsNode interface.
 */

public final class MutableNodePollsHierarchySession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractNodePollsHierarchySession
    implements org.osid.voting.PollsHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodePollsHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodePollsHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodePollsHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodePollsHierarchySession(org.osid.voting.PollsNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getPolls().getDisplayName())
                     .description(root.getPolls().getDescription())
                     .build());

        addRootPolls(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodePollsHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodePollsHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.voting.PollsNode root) {
        setHierarchy(hierarchy);
        addRootPolls(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodePollsHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodePollsHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                            java.util.Collection<org.osid.voting.PollsNode> roots) {
        setHierarchy(hierarchy);
        addRootPolls(roots);
        return;
    }


    /**
     *  Adds a root polls node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootPolls(org.osid.voting.PollsNode root) {
        super.addRootPolls(root);
        return;
    }


    /**
     *  Adds a collection of root polls nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootPolls(java.util.Collection<org.osid.voting.PollsNode> roots) {
        super.addRootPolls(roots);
        return;
    }


    /**
     *  Removes a root polls node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootPolls(org.osid.id.Id rootId) {
        super.removeRootPolls(rootId);
        return;
    }
}
