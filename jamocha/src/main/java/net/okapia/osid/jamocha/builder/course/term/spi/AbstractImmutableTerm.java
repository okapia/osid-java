//
// AbstractImmutableTerm.java
//
//     Wraps a mutable Term to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.term.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Term</code> to hide modifiers. This
 *  wrapper provides an immutized Term from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying term whose state changes are visible.
 */

public abstract class AbstractImmutableTerm
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.course.Term {

    private final org.osid.course.Term term;


    /**
     *  Constructs a new <code>AbstractImmutableTerm</code>.
     *
     *  @param term the term to immutablize
     *  @throws org.osid.NullArgumentException <code>term</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableTerm(org.osid.course.Term term) {
        super(term);
        this.term = term;
        return;
    }


    /**
     *  Gets a display label for this term which may be less formal than the 
     *  display name. 
     *
     *  @return the term label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.term.getDisplayLabel());
    }


    /**
     *  Tests if this term has an open date when published offerings are 
     *  finalized. 
     *
     *  @return <code> true </code> if there is an open date associated with 
     *          this term, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasOpenDate() {
        return (this.term.hasOpenDate());
    }


    /**
     *  Gets the open date when published offerings are finalized. 
     *
     *  @return the open date 
     *  @throws org.osid.IllegalStateException <code> hasOpenDate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getOpenDate() {
        return (this.term.getOpenDate());
    }


    /**
     *  Tests if this term has a registration period. 
     *
     *  @return <code> true </code> if there is a registration period 
     *          associated with this term, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRegistrationPeriod() {
        return (this.term.hasRegistrationPeriod());
    }


    /**
     *  Gets the start of the registration period. 
     *
     *  @return the start date 
     *  @throws org.osid.IllegalStateException <code> hasRegistrationPeriod() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getRegistrationStart() {
        return (this.term.getRegistrationStart());
    }


    /**
     *  Gets the end of the registration period. 
     *
     *  @return the end date 
     *  @throws org.osid.IllegalStateException <code> hasRegistrationPeriod() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getRegistrationEnd() {
        return (this.term.getRegistrationEnd());
    }


    /**
     *  Gets the start of classes. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClassesStart() {
        return (this.term.getClassesStart());
    }


    /**
     *  Gets the end of classes. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClassesEnd() {
        return (this.term.getClassesEnd());
    }


    /**
     *  Tests if this term has an add/drop date. 
     *
     *  @return <code> true </code> if there is an add/drop date associated 
     *          with this term, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAddDropDate() {
        return (this.term.hasAddDropDate());
    }


    /**
     *  Gets the add date. 
     *
     *  @return the add date 
     *  @throws org.osid.IllegalStateException <code> hasAddDropDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getAddDate() {
        return (this.term.getAddDate());
    }


    /**
     *  Gets the drop date. 
     *
     *  @return the drop date 
     *  @throws org.osid.IllegalStateException <code> hasAddDropDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDropDate() {
        return (this.term.getDropDate());
    }


    /**
     *  Tests if this term has a final eam period. 
     *
     *  @return <code> true </code> if there is a final exam period associated 
     *          with this term, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasFinalExamPeriod() {
        return (this.term.hasFinalExamPeriod());
    }


    /**
     *  Gets the start of the final exam period. 
     *
     *  @return the start date 
     *  @throws org.osid.IllegalStateException <code> hasFinalExamPeriod() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getFinalExamStart() {
        return (this.term.getFinalExamStart());
    }


    /**
     *  Gets the end of the final exam period. 
     *
     *  @return the end date 
     *  @throws org.osid.IllegalStateException <code> hasFinalExamPeriod() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getFinalExamEnd() {
        return (this.term.getFinalExamEnd());
    }


    /**
     *  Tests if this term has a close date when results from course offerings 
     *  are finalized. 
     *
     *  @return <code> true </code> if there is a close date associated with 
     *          this term, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCloseDate() {
        return (this.term.hasCloseDate());
    }


    /**
     *  Gets the close date when results of course offerings are finalized. 
     *
     *  @return the close date 
     *  @throws org.osid.IllegalStateException <code> hasCloseDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCloseDate() {
        return (this.term.getCloseDate());
    }


    /**
     *  Gets the term record corresponding to the given <code> Term </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> termRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(termRecordType) </code> is <code> true </code> . 
     *
     *  @param  termRecordType the type of term record to retrieve 
     *  @return the term record 
     *  @throws org.osid.NullArgumentException <code> termRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(termRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.records.TermRecord getTermRecord(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException {

        return (this.term.getTermRecord(termRecordType));
    }
}

