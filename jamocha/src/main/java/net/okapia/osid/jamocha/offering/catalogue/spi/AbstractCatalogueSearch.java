//
// AbstractCatalogueSearch.java
//
//     A template for making a Catalogue Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.catalogue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing catalogue searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCatalogueSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.CatalogueSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.records.CatalogueSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.CatalogueSearchOrder catalogueSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of catalogues. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  catalogueIds list of catalogues
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCatalogues(org.osid.id.IdList catalogueIds) {
        while (catalogueIds.hasNext()) {
            try {
                this.ids.add(catalogueIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCatalogues</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of catalogue Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCatalogueIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  catalogueSearchOrder catalogue search order 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>catalogueSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCatalogueResults(org.osid.offering.CatalogueSearchOrder catalogueSearchOrder) {
	this.catalogueSearchOrder = catalogueSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.CatalogueSearchOrder getCatalogueSearchOrder() {
	return (this.catalogueSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given catalogue search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a catalogue implementing the requested record.
     *
     *  @param catalogueSearchRecordType a catalogue search record
     *         type
     *  @return the catalogue search record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogueSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CatalogueSearchRecord getCatalogueSearchRecord(org.osid.type.Type catalogueSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.records.CatalogueSearchRecord record : this.records) {
            if (record.implementsRecordType(catalogueSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogueSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalogue search. 
     *
     *  @param catalogueSearchRecord catalogue search record
     *  @param catalogueSearchRecordType catalogue search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogueSearchRecord(org.osid.offering.records.CatalogueSearchRecord catalogueSearchRecord, 
                                           org.osid.type.Type catalogueSearchRecordType) {

        addRecordType(catalogueSearchRecordType);
        this.records.add(catalogueSearchRecord);        
        return;
    }
}
