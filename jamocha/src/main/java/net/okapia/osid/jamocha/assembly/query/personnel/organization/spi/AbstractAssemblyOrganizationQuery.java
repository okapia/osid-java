//
// AbstractAssemblyOrganizationQuery.java
//
//     An OrganizationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.personnel.organization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OrganizationQuery that stores terms.
 */

public abstract class AbstractAssemblyOrganizationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.personnel.OrganizationQuery,
               org.osid.personnel.OrganizationQueryInspector,
               org.osid.personnel.OrganizationSearchOrder {

    private final java.util.Collection<org.osid.personnel.records.OrganizationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.OrganizationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.OrganizationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOrganizationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOrganizationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a display label. 
     *
     *  @param  label a display label 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getDisplayLabelColumn(), label, stringMatchType, match);
        return;
    }


    /**
     *  Matches organizations with any display label. 
     *
     *  @param  match <code> true </code> to match organizations with any 
     *          display label, <code> false </code> to match organizations 
     *          with no display label 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        getAssembler().addStringWildcardTerm(getDisplayLabelColumn(), match);
        return;
    }


    /**
     *  Clears all display label terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        getAssembler().clearTerms(getDisplayLabelColumn());
        return;
    }


    /**
     *  Gets the display label query terms. 
     *
     *  @return the display label terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (getAssembler().getStringTerms(getDisplayLabelColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the display 
     *  label. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayLabel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDisplayLabelColumn(), style);
        return;
    }


    /**
     *  Gets the DisplayLabel column name.
     *
     * @return the column name
     */

    protected String getDisplayLabelColumn() {
        return ("display_label");
    }


    /**
     *  Sets the organization <code> Id </code> for this query to match 
     *  organizations that have the specified organization as an ancestor. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOrganizationId(org.osid.id.Id organizationId, 
                                            boolean match) {
        getAssembler().addIdTerm(getAncestorOrganizationIdColumn(), organizationId, match);
        return;
    }


    /**
     *  Clears the ancestor organization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorOrganizationIdTerms() {
        getAssembler().clearTerms(getAncestorOrganizationIdColumn());
        return;
    }


    /**
     *  Gets the ancestor organization <code> Id </code> query terms. 
     *
     *  @return the ancestor organization <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOrganizationIdTerms() {
        return (getAssembler().getIdTerms(getAncestorOrganizationIdColumn()));
    }


    /**
     *  Gets the AncestorOrganizationId column name.
     *
     * @return the column name
     */

    protected String getAncestorOrganizationIdColumn() {
        return ("ancestor_organization_id");
    }


    /**
     *  Tests if an <code> OrganizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOrganizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getAncestorOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOrganizationQuery() is false");
    }


    /**
     *  Matches agencies with any organization ancestor. 
     *
     *  @param  match <code> true </code> to match organizations with any 
     *          ancestor, <code> false </code> to match root organizations 
     */

    @OSID @Override
    public void matchAnyAncestorOrganization(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorOrganizationColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor organization query terms. 
     */

    @OSID @Override
    public void clearAncestorOrganizationTerms() {
        getAssembler().clearTerms(getAncestorOrganizationColumn());
        return;
    }


    /**
     *  Gets the ancestor organization query terms. 
     *
     *  @return the ancestor organization terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getAncestorOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Gets the AncestorOrganization column name.
     *
     * @return the column name
     */

    protected String getAncestorOrganizationColumn() {
        return ("ancestor_organization");
    }


    /**
     *  Sets the organization <code> Id </code> for this query to match 
     *  organizations that have the specified organization as a descendant. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOrganizationId(org.osid.id.Id organizationId, 
                                              boolean match) {
        getAssembler().addIdTerm(getDescendantOrganizationIdColumn(), organizationId, match);
        return;
    }


    /**
     *  Clears the descendant organization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantOrganizationIdTerms() {
        getAssembler().clearTerms(getDescendantOrganizationIdColumn());
        return;
    }


    /**
     *  Gets the descendant organization <code> Id </code> query terms. 
     *
     *  @return the descendant organization <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOrganizationIdTerms() {
        return (getAssembler().getIdTerms(getDescendantOrganizationIdColumn()));
    }


    /**
     *  Gets the DescendantOrganizationId column name.
     *
     * @return the column name
     */

    protected String getDescendantOrganizationIdColumn() {
        return ("descendant_organization_id");
    }


    /**
     *  Tests if an <code> OrganizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOrganizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getDescendantOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOrganizationQuery() is false");
    }


    /**
     *  Matches organizations with any organization descendant. 
     *
     *  @param  match <code> true </code> to match organizations with any 
     *          descendant, <code> false </code> to match leaf organizations 
     */

    @OSID @Override
    public void matchAnyDescendantOrganization(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantOrganizationColumn(), match);
        return;
    }


    /**
     *  Clears the descendant organization query terms. 
     */

    @OSID @Override
    public void clearDescendantOrganizationTerms() {
        getAssembler().clearTerms(getDescendantOrganizationColumn());
        return;
    }


    /**
     *  Gets the descendant organization query terms. 
     *
     *  @return the descendant organization terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getDescendantOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Gets the DescendantOrganization column name.
     *
     * @return the column name
     */

    protected String getDescendantOrganizationColumn() {
        return ("descendant_organization");
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match organizations 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        getAssembler().addIdTerm(getRealmIdColumn(), realmId, match);
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        getAssembler().clearTerms(getRealmIdColumn());
        return;
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (getAssembler().getIdTerms(getRealmIdColumn()));
    }


    /**
     *  Gets the RealmId column name.
     *
     * @return the column name
     */

    protected String getRealmIdColumn() {
        return ("realm_id");
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        getAssembler().clearTerms(getRealmColumn());
        return;
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }


    /**
     *  Gets the Realm column name.
     *
     * @return the column name
     */

    protected String getRealmColumn() {
        return ("realm");
    }


    /**
     *  Tests if this organization supports the given record
     *  <code>Type</code>.
     *
     *  @param  organizationRecordType an organization record type 
     *  @return <code>true</code> if the organizationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type organizationRecordType) {
        for (org.osid.personnel.records.OrganizationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  organizationRecordType the organization record type 
     *  @return the organization query record 
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(organizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationQueryRecord getOrganizationQueryRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.OrganizationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  organizationRecordType the organization record type 
     *  @return the organization query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(organizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationQueryInspectorRecord getOrganizationQueryInspectorRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.OrganizationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param organizationRecordType the organization record type
     *  @return the organization search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(organizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationSearchOrderRecord getOrganizationSearchOrderRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.OrganizationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this organization. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param organizationQueryRecord the organization query record
     *  @param organizationQueryInspectorRecord the organization query inspector
     *         record
     *  @param organizationSearchOrderRecord the organization search order record
     *  @param organizationRecordType organization record type
     *  @throws org.osid.NullArgumentException
     *          <code>organizationQueryRecord</code>,
     *          <code>organizationQueryInspectorRecord</code>,
     *          <code>organizationSearchOrderRecord</code> or
     *          <code>organizationRecordTypeorganization</code> is
     *          <code>null</code>
     */
            
    protected void addOrganizationRecords(org.osid.personnel.records.OrganizationQueryRecord organizationQueryRecord, 
                                      org.osid.personnel.records.OrganizationQueryInspectorRecord organizationQueryInspectorRecord, 
                                      org.osid.personnel.records.OrganizationSearchOrderRecord organizationSearchOrderRecord, 
                                      org.osid.type.Type organizationRecordType) {

        addRecordType(organizationRecordType);

        nullarg(organizationQueryRecord, "organization query record");
        nullarg(organizationQueryInspectorRecord, "organization query inspector record");
        nullarg(organizationSearchOrderRecord, "organization search odrer record");

        this.queryRecords.add(organizationQueryRecord);
        this.queryInspectorRecords.add(organizationQueryInspectorRecord);
        this.searchOrderRecords.add(organizationSearchOrderRecord);
        
        return;
    }
}
