//
// AbstractAntimatroidSearch.java
//
//     A template for making an Antimatroid Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.antimatroid.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing antimatroid searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAntimatroidSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.sequencing.AntimatroidSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.sequencing.records.AntimatroidSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.sequencing.AntimatroidSearchOrder antimatroidSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of antimatroids. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  antimatroidIds list of antimatroids
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAntimatroids(org.osid.id.IdList antimatroidIds) {
        while (antimatroidIds.hasNext()) {
            try {
                this.ids.add(antimatroidIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAntimatroids</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of antimatroid Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAntimatroidIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  antimatroidSearchOrder antimatroid search order 
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>antimatroidSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAntimatroidResults(org.osid.sequencing.AntimatroidSearchOrder antimatroidSearchOrder) {
	this.antimatroidSearchOrder = antimatroidSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.sequencing.AntimatroidSearchOrder getAntimatroidSearchOrder() {
	return (this.antimatroidSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given antimatroid search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an antimatroid implementing the requested record.
     *
     *  @param antimatroidSearchRecordType an antimatroid search record
     *         type
     *  @return the antimatroid search record
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(antimatroidSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidSearchRecord getAntimatroidSearchRecord(org.osid.type.Type antimatroidSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.sequencing.records.AntimatroidSearchRecord record : this.records) {
            if (record.implementsRecordType(antimatroidSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(antimatroidSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this antimatroid search. 
     *
     *  @param antimatroidSearchRecord antimatroid search record
     *  @param antimatroidSearchRecordType antimatroid search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAntimatroidSearchRecord(org.osid.sequencing.records.AntimatroidSearchRecord antimatroidSearchRecord, 
                                           org.osid.type.Type antimatroidSearchRecordType) {

        addRecordType(antimatroidSearchRecordType);
        this.records.add(antimatroidSearchRecord);        
        return;
    }
}
