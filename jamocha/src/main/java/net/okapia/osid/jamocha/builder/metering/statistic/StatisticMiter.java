//
// StatisticMiter.java
//
//     Defines a Statistic miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.statistic;


/**
 *  Defines a <code>Statistic</code> miter for use with the builders.
 */

public interface StatisticMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidCompendiumMiter,
            org.osid.metering.Statistic {


    /**
     *  Sets the meter.
     *
     *  @param meter a meter
     *  @throws org.osid.NullArgumentException <code>meter</code> is
     *          <code>null</code>
     */

    public void setMeter(org.osid.metering.Meter meter);


    /**
     *  Sets the metered object id.
     *
     *  @param objectId a metered object id
     *  @throws org.osid.NullArgumentException <code>objectId</code>
     *          is <code>null</code>
     */

    public void setMeteredObjectId(org.osid.id.Id objectId);


    /**
     *  Sets the sum.
     *
     *  @param sum a sum
     *  @throws org.osid.NullArgumentException <code>sum</code> is
     *          <code>null</code>
     */

    public void setSum(java.math.BigDecimal sum);


    /**
     *  Sets the mean.
     *
     *  @param mean a mean
     *  @throws org.osid.NullArgumentException <code>mean</code> is
     *          <code>null</code>
     */

    public void setMean(java.math.BigDecimal mean);


    /**
     *  Sets the median.
     *
     *  @param median a median
     *  @throws org.osid.NullArgumentException <code>median</code> is
     *          <code>null</code>
     */

    public void setMedian(java.math.BigDecimal median);


    /**
     *  Sets the mode.
     *
     *  @param mode a mode
     *  @throws org.osid.NullArgumentException <code>mode</code> is
     *          <code>null</code>
     */

    public void setMode(java.math.BigDecimal mode);


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation a standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    public void setStandardDeviation(java.math.BigDecimal standardDeviation);


    /**
     *  Sets the root mean square.
     *
     *  @param rms the rms
     *  @throws org.osid.NullArgumentException <code>rms</code> is
     *          <code>null</code>
     */

    public void setRMS(java.math.BigDecimal rms);


    /**
     *  Sets the delta.
     *
     *  @param delta a delta
     *  @throws org.osid.NullArgumentException <code>delta</code> is
     *          <code>null</code>
     */

    public void setDelta(java.math.BigDecimal delta);


    /**
     *  Sets the percent change.
     *
     *  @param percent the percent change
     *  @throws org.osid.NullArgumentException <code>percent</code> is
     *          <code>null</code>
     */

    public void setPercentChange(java.math.BigDecimal percent);


    /**
     *  Sets the average rate.
     *
     *  @param rate an average rate
     *  @param units the time units
     *  @throws org.osid.NullArgumentException <code>rate</code> or
     *          <code>units</code> is <code>null</code>
     */

    public void setAverageRate(java.math.BigDecimal rate, org.osid.calendaring.DateTimeResolution units);


    /**
     *  Adds a Statistic record.
     *
     *  @param record a statistic record
     *  @param recordType the type of statistic record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addStatisticRecord(org.osid.metering.records.StatisticRecord record, org.osid.type.Type recordType);
}       


