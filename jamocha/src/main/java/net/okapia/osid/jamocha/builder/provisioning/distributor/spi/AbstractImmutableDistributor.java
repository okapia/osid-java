//
// AbstractImmutableDistributor.java
//
//     Wraps a mutable Distributor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.distributor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Distributor</code> to hide modifiers. This
 *  wrapper provides an immutized Distributor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying distributor whose state changes are visible.
 */

public abstract class AbstractImmutableDistributor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.provisioning.Distributor {

    private final org.osid.provisioning.Distributor distributor;


    /**
     *  Constructs a new <code>AbstractImmutableDistributor</code>.
     *
     *  @param distributor the distributor to immutablize
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDistributor(org.osid.provisioning.Distributor distributor) {
        super(distributor);
        this.distributor = distributor;
        return;
    }


    /**
     *  Gets the distributor record corresponding to the given <code> 
     *  Distributor </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  distributorRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(distributorRecordType) </code> is <code> true </code> . 
     *
     *  @param  distributorRecordType the type of distributor record to 
     *          retrieve 
     *  @return the distributor record 
     *  @throws org.osid.NullArgumentException <code> distributorRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(distributorRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorRecord getDistributorRecord(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException {

        return (this.distributor.getDistributorRecord(distributorRecordType));
    }
}

