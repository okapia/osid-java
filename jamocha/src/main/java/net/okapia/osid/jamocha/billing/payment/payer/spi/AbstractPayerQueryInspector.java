//
// AbstractPayerQueryInspector.java
//
//     A template for making a PayerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for payers.
 */

public abstract class AbstractPayerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.billing.payment.PayerQueryInspector {

    private final java.util.Collection<org.osid.billing.payment.records.PayerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the customer <code> Id </code> query terms. 
     *
     *  @return the customer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the customer query terms. 
     *
     *  @return the customer query terms 
     */

    @OSID @Override
    public org.osid.billing.CustomerQueryInspector[] getCustomerTerms() {
        return (new org.osid.billing.CustomerQueryInspector[0]);
    }


    /**
     *  Gets the activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getUsesActivityTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the cash terms. 
     *
     *  @return the cash terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getUsesCashTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the credit card number terms. 
     *
     *  @return the credit card number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCreditCardNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the credit card expiration terms. 
     *
     *  @return the credit card expiration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreditCardExpirationTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the credit card code terms. 
     *
     *  @return the credit card code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCreditCardCodeTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bank routing number terms. 
     *
     *  @return the bank routing number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getBankRoutingNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the bank account number terms. 
     *
     *  @return the bank account number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getBankAccountNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given payer query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a payer implementing the requested record.
     *
     *  @param payerRecordType a payer record type
     *  @return the payer query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(payerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerQueryInspectorRecord getPayerQueryInspectorRecord(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PayerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(payerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(payerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payer query. 
     *
     *  @param payerQueryInspectorRecord payer query inspector
     *         record
     *  @param payerRecordType payer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPayerQueryInspectorRecord(org.osid.billing.payment.records.PayerQueryInspectorRecord payerQueryInspectorRecord, 
                                                   org.osid.type.Type payerRecordType) {

        addRecordType(payerRecordType);
        nullarg(payerRecordType, "payer record type");
        this.records.add(payerQueryInspectorRecord);        
        return;
    }
}
