//
// AbstractSummary.java
//
//     Defines a Summary builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.summary.spi;


/**
 *  Defines a <code>Summary</code> builder.
 */

public abstract class AbstractSummaryBuilder<T extends AbstractSummaryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCompendiumBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.billing.payment.summary.SummaryMiter summary;


    /**
     *  Constructs a new <code>AbstractSummaryBuilder</code>.
     *
     *  @param summary the summary to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSummaryBuilder(net.okapia.osid.jamocha.builder.billing.payment.summary.SummaryMiter summary) {
        super(summary);
        this.summary = summary;
        return;
    }


    /**
     *  Builds the summary.
     *
     *  @return the new summary
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>summary</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.billing.payment.Summary build() {
        (new net.okapia.osid.jamocha.builder.validator.billing.payment.summary.SummaryValidator(getValidations())).validate(this.summary);
        return (new net.okapia.osid.jamocha.builder.billing.payment.summary.ImmutableSummary(this.summary));
    }


    /**
     *  Gets the summary. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new summary
     */

    @Override
    public net.okapia.osid.jamocha.builder.billing.payment.summary.SummaryMiter getMiter() {
        return (this.summary);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public T customer(org.osid.billing.Customer customer) {
        getMiter().setCustomer(customer);
        return (self());
    }


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException <code>period</code>
     *          is <code>null</code>
     */

    public T period(org.osid.billing.Period period) {
        getMiter().setPeriod(period);
        return (self());
    }


    /**
     *  Sets the credit balance flag.
     *
     *  @param credit <code>true</code> if a credit,
     *         <code>false</code> if a debit
     */

    public T creditBalance(boolean credit) {
        getMiter().setCreditBalance(credit);
        return (self());
    }


    /**
     *  Sets the balance.
     * 
     *  @param balance the balance amount
     *  @throws org.osid.NullArgumentException <code>balance</code> is
     *          <code>null</code>
     */

    public T balance(org.osid.financials.Currency balance) {
        getMiter().setBalance(balance);
        return (self());
    }


    /**
     *  Sets the last payment date.
     *
     *  @param date the last payment date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T lastPaymentDate(org.osid.calendaring.DateTime date) {
        getMiter().setLastPaymentDate(date);
        return (self());
    }


    /**
     *  Sets the last payment amount.
     *
     *  @param amount the last payment amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T lastPaymentAmount(org.osid.financials.Currency amount) {
        getMiter().setLastPaymentAmount(amount);
        return (self());
    }


    /**
     *  Sets the due date.
     *
     *  @param date due date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    public T paymentDueDate(org.osid.calendaring.DateTime date) {
        getMiter().setPaymentDueDate(date);
        return (self());
    }


    /**
     *  Adds a Summary record.
     *
     *  @param record a summary record
     *  @param recordType the type of summary record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.billing.payment.records.SummaryRecord record, org.osid.type.Type recordType) {
        getMiter().addSummaryRecord(record, recordType);
        return (self());
    }
}       


