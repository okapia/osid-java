//
// AbstractQueryCyclicEventLookupSession.java
//
//    An inline adapter that maps a CyclicEventLookupSession to
//    a CyclicEventQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CyclicEventLookupSession to
 *  a CyclicEventQuerySession.
 */

public abstract class AbstractQueryCyclicEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.cycle.spi.AbstractCyclicEventLookupSession
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {

    private final org.osid.calendaring.cycle.CyclicEventQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCyclicEventLookupSession.
     *
     *  @param querySession the underlying cyclic event query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCyclicEventLookupSession(org.osid.calendaring.cycle.CyclicEventQuerySession querySession) {
        nullarg(querySession, "cyclic event query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>CyclicEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCyclicEvents() {
        return (this.session.canSearchCyclicEvents());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the <code>CyclicEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CyclicEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CyclicEvent</code> and
     *  retained for compatibility.
     *
     *  @param  cyclicEventId <code>Id</code> of the
     *          <code>CyclicEvent</code>
     *  @return the cyclic event
     *  @throws org.osid.NotFoundException <code>cyclicEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>cyclicEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent(org.osid.id.Id cyclicEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicEventQuery query = getQuery();
        query.matchId(cyclicEventId, true);
        org.osid.calendaring.cycle.CyclicEventList cyclicEvents = this.session.getCyclicEventsByQuery(query);
        if (cyclicEvents.hasNext()) {
            return (cyclicEvents.getNextCyclicEvent());
        } 
        
        throw new org.osid.NotFoundException(cyclicEventId + " not found");
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CyclicEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  cyclicEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByIds(org.osid.id.IdList cyclicEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicEventQuery query = getQuery();

        try (org.osid.id.IdList ids = cyclicEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCyclicEventsByQuery(query));
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  cyclic event genus <code>Type</code> which does not include
     *  cyclic events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicEventQuery query = getQuery();
        query.matchGenusType(cyclicEventGenusType, true);
        return (this.session.getCyclicEventsByQuery(query));
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  cyclic event genus <code>Type</code> and include any additional
     *  cyclic events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByParentGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicEventQuery query = getQuery();
        query.matchParentGenusType(cyclicEventGenusType, true);
        return (this.session.getCyclicEventsByQuery(query));
    }


    /**
     *  Gets a <code>CyclicEventList</code> containing the given
     *  cyclic event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventRecordType a cyclicEvent record type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByRecordType(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicEventQuery query = getQuery();
        query.matchRecordType(cyclicEventRecordType, true);
        return (this.session.getCyclicEventsByQuery(query));
    }

    
    /**
     *  Gets all <code>CyclicEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>CyclicEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicEventQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCyclicEventsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.cycle.CyclicEventQuery getQuery() {
        org.osid.calendaring.cycle.CyclicEventQuery query = this.session.getCyclicEventQuery();
        return (query);
    }
}
