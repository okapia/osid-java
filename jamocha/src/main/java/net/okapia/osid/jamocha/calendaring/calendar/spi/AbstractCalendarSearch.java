//
// AbstractCalendarSearch.java
//
//     A template for making a Calendar Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.calendar.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing calendar searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCalendarSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.CalendarSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.CalendarSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.CalendarSearchOrder calendarSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of calendars. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  calendarIds list of calendars
     *  @throws org.osid.NullArgumentException
     *          <code>calendarIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCalendars(org.osid.id.IdList calendarIds) {
        while (calendarIds.hasNext()) {
            try {
                this.ids.add(calendarIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCalendars</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of calendar Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCalendarIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  calendarSearchOrder calendar search order 
     *  @throws org.osid.NullArgumentException
     *          <code>calendarSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>calendarSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCalendarResults(org.osid.calendaring.CalendarSearchOrder calendarSearchOrder) {
	this.calendarSearchOrder = calendarSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.CalendarSearchOrder getCalendarSearchOrder() {
	return (this.calendarSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given calendar search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a calendar implementing the requested record.
     *
     *  @param calendarSearchRecordType a calendar search record
     *         type
     *  @return the calendar search record
     *  @throws org.osid.NullArgumentException
     *          <code>calendarSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(calendarSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CalendarSearchRecord getCalendarSearchRecord(org.osid.type.Type calendarSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.CalendarSearchRecord record : this.records) {
            if (record.implementsRecordType(calendarSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(calendarSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this calendar search. 
     *
     *  @param calendarSearchRecord calendar search record
     *  @param calendarSearchRecordType calendar search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCalendarSearchRecord(org.osid.calendaring.records.CalendarSearchRecord calendarSearchRecord, 
                                           org.osid.type.Type calendarSearchRecordType) {

        addRecordType(calendarSearchRecordType);
        this.records.add(calendarSearchRecord);        
        return;
    }
}
