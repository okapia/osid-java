//
// AbstractQueryProcessLookupSession.java
//
//    An inline adapter that maps a ProcessLookupSession to
//    a ProcessQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProcessLookupSession to
 *  a ProcessQuerySession.
 */

public abstract class AbstractQueryProcessLookupSession
    extends net.okapia.osid.jamocha.workflow.spi.AbstractProcessLookupSession
    implements org.osid.workflow.ProcessLookupSession {

    private boolean activeonly    = false;
    private final org.osid.workflow.ProcessQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProcessLookupSession.
     *
     *  @param querySession the underlying process query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProcessLookupSession(org.osid.workflow.ProcessQuerySession querySession) {
        nullarg(querySession, "process query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Office</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform <code>Process</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProcesses() {
        return (this.session.canSearchProcesses());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include processes in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active processes are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveProcessView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive processes are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusProcessView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Process</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Process</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Process</code> and
     *  retained for compatibility.
     *
     *  In active mode, processes are returned that are currently
     *  active. In any status mode, active and inactive processes
     *  are returned.
     *
     *  @param  processId <code>Id</code> of the
     *          <code>Process</code>
     *  @return the process
     *  @throws org.osid.NotFoundException <code>processId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>processId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Process getProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.ProcessQuery query = getQuery();
        query.matchId(processId, true);
        org.osid.workflow.ProcessList processes = this.session.getProcessesByQuery(query);
        if (processes.hasNext()) {
            return (processes.getNextProcess());
        } 
        
        throw new org.osid.NotFoundException(processId + " not found");
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  processes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Processes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, processes are returned that are currently
     *  active. In any status mode, active and inactive processes
     *  are returned.
     *
     *  @param  processIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>processIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.ProcessList getProcessesByIds(org.osid.id.IdList processIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.ProcessQuery query = getQuery();

        try (org.osid.id.IdList ids = processIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProcessesByQuery(query));
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  process genus <code>Type</code> which does not include
     *  processes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, processes are returned that are currently
     *  active. In any status mode, active and inactive processes
     *  are returned.
     *
     *  @param  processGenusType a process genus type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.ProcessList getProcessesByGenusType(org.osid.type.Type processGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.ProcessQuery query = getQuery();
        query.matchGenusType(processGenusType, true);
        return (this.session.getProcessesByQuery(query));
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  process genus <code>Type</code> and include any additional
     *  processes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, processes are returned that are currently
     *  active. In any status mode, active and inactive processes
     *  are returned.
     *
     *  @param  processGenusType a process genus type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.ProcessList getProcessesByParentGenusType(org.osid.type.Type processGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.ProcessQuery query = getQuery();
        query.matchParentGenusType(processGenusType, true);
        return (this.session.getProcessesByQuery(query));
    }


    /**
     *  Gets a <code>ProcessList</code> containing the given
     *  process record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, processes are returned that are currently
     *  active. In any status mode, active and inactive processes
     *  are returned.
     *
     *  @param  processRecordType a process record type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.ProcessList getProcessesByRecordType(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.ProcessQuery query = getQuery();
        query.matchRecordType(processRecordType, true);
        return (this.session.getProcessesByQuery(query));
    }


    /**
     *  Gets a <code>ProcessList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known processes or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  processes that are accessible through this session. 
     *
     *  In active mode, processes are returned that are currently
     *  active. In any status mode, active and inactive processes
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Process</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.ProcessList getProcessesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.ProcessQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getProcessesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Processes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, processes are returned that are currently
     *  active. In any status mode, active and inactive processes
     *  are returned.
     *
     *  @return a list of <code>Processes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.ProcessList getProcesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.ProcessQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProcessesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.workflow.ProcessQuery getQuery() {
        org.osid.workflow.ProcessQuery query = this.session.getProcessQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
