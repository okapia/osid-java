//
// AbstractAdapterOsidProfile.java
//
//     A basic adapter for OsidProfiles.
//
//
// Tom Coppeto
// Okapia
// 27 January 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This is a abstract class for implementing a basic OsidProfile
 *  adapter.
 */

public abstract class AbstractAdapterOsidProfile<T extends org.osid.OsidProfile>
    implements org.osid.OsidProfile {

    private final net.okapia.osid.provider.ServiceProvider provider;
    private T profile;


    /**
     *  Constructs a new {@code AbstractAdapterOsidProfile} using the
     *  underlying provider.
     */

    protected AbstractAdapterOsidProfile() {
        this.provider = null;
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOsidProfile}.
     *
     *  @param provider the provider identity for the adapter
     *  @throws org.osid.NullArgumentException {@code profile} is
     *          {@code null}
     */

    protected AbstractAdapterOsidProfile(net.okapia.osid.provider.ServiceProvider provider) {
        this.provider    = provider;
        return;
    }


    /**
     *  Gets the underlying manager.
     *
     *  @return the manager
     *  @throws org.osid.IllegalStateException manager not set
     */

    protected T getAdapteeManager() {
        if (this.profile == null) {
            throw new org.osid.IllegalStateException("manager not set");
        }

        return (this.profile);
    }


    /**
     *  Sets the underlying manager.
     *
     *  @param manager the manager
     *  @throws org.osid.NullArgumentException {@code manager} is
     *          {@code null}
     */

    protected void setAdapteeManager(T manager) {
        nullarg(manager, "manager");
        this.profile = manager;
        return;
    }


    /**
     *  Gets an identifier for this service implementation. The
     *  identifier is unique among services but multiple
     *  instantiations of the same service use the same {@code
     *  Id}. This identifier is the same identifier used in managing
     *  OSID installations.
     *
     *  @return the {@code Id} 
     */

    @OSID @Override
    public org.osid.id.Id getId() {
        if (this.provider == null) {
            return (this.profile.getId());
        } else {
            return (this.provider.getServiceId());
        }
    }        


    /**
     *  Gets a display name for this service implementation. 
     *
     *  @return a display name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayName() {
        if (this.provider == null) {
            return (this.profile.getDisplayName());
        } else {
            return (this.provider.getServiceName());
        }            
    }


    /**
     *  Gets a description of this service implementation. 
     *
     *  @return a description 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDescription() {
        if (this.provider == null) {
            return (this.profile.getDescription());
        } else {
            return (this.provider.getServiceDescription());
        }
    }


    /**
     *  Gets the version of this service implementation. 
     *
     *  @return the service implementation version 
     */

    @OSID @Override
    public org.osid.installation.Version getVersion() {
        if (this.provider == null) {
            return (this.profile.getVersion());
        } else {
            return (this.provider.getImplementationVersion());
        }            
    }


    /**
     *  Gets the date this service implementation was released. 
     *
     *  @return the release date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReleaseDate() {
        if (this.provider == null) {
            return (this.profile.getReleaseDate());
        } else {
            return (this.provider.getReleaseDate());
        }
    }


    /**
     *  Test for support of an OSID specification version. 
     *
     *  @param  version the specification version to test 
     *  @return {@code true} if this manager supports the given OSID 
     *          version, {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsOSIDVersion(org.osid.installation.Version version) {
        return (this.profile.supportsOSIDVersion(version));
    }


    /**
     *  Gets the locales supported in this service. 
     *
     *  @return list of locales supported 
     */

    @OSID @Override
    public org.osid.locale.LocaleList getLocales() {
        return (this.profile.getLocales());
    }


    /**
     *  Test for support of a journaling rollback service. 
     *
     *  @return {@code true} if this manager supports the journal 
     *          rollback, {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsJournalRollback() {
        return (this.profile.supportsJournalRollback());
    }


    /**
     *  Test for support of a journal branching service. 
     *
     *  @return {@code true} if this manager supports the journal 
     *          branching, {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsJournalBranching() {
        return (this.profile.supportsJournalBranching());
    }


    /**
     *  Gets the {@code Branch Id} representing this service branch. 
     *
     *  @return the branch {@code Id} 
     *  @throws org.osid.UnimplementedException {@code 
     *          supportsJournalBranching()} is {@code false} {
     */

    @OSID @Override
    public org.osid.id.Id getBranchId() {
        return (this.profile.getBranchId());
    }


    /**
     *  Gets this service branch. 
     *
     *  @return the service branch 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException {@code 
     *          supportsJournalBranching()} is {@code false} 
     */

    @OSID @Override
    public org.osid.journaling.Branch getBranch()
        throws org.osid.OperationFailedException {

        return (this.profile.getBranch());
    }


    /**
     *  Gets the proxy record {@code Types} supported in this service. 
     *  If no proxy manager is available, an empty list is returned. 
     *
     *  @return list of proxy record types supported 
     */

    @OSID @Override
    public org.osid.type.TypeList getProxyRecordTypes() {
        return (this.profile.getProxyRecordTypes());
    }


    /**
     *  Test for support of a proxy type. 
     *
     *  @param  proxyRecordType a proxy record type 
     *  @return {@code true} if this service supports the given proxy 
     *          record type, {@code false} otherwise 
     *  @throws org.osid.NullArgumentException {@code proxyRecordType} 
     *          is {@code null} 
     */

    @OSID @Override
    public boolean supportsProxyRecordType(org.osid.type.Type proxyRecordType) {
        return (this.profile.supportsProxyRecordType(proxyRecordType));
    }


    /**
     *  Gets the {@code Id} of the provider. 
     *
     *  @return the provider {@code Id} 
     */

    @OSID @Override
    public org.osid.id.Id getProviderId() {
        if (this.provider == null) {            
            return (this.profile.getProviderId());
        } else {
            return (this.provider.getProvider().getResource().getId());
        }
    }


    /**
     *  Gets the {@code Resource} representing the provider. 
     *
     *  @return the provider 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getProvider()
        throws org.osid.OperationFailedException {
        
        if (this.provider == null) {
            return (this.profile.getProvider());
        } else {
            return (this.provider.getProvider().getResource());
        }
    }


    /**
     *  Gets the branding asset {@code Ids}.
     *
     *  @return a list of asset {@code Ids} 
     */

    @OSID @Override
    public org.osid.id.IdList getBrandingIds() {
        if (this.provider == null) {
            return (this.profile.getBrandingIds());
        } else {
            try {
                return (new net.okapia.osid.jamocha.adapter.converter.repository.asset.AssetToIdList(getBranding()));
            } catch (org.osid.OperationFailedException ofe) {
                return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
            }
        }
    }


    /**
     *  Gets a branding, such as an image or logo, expressed using the
     *  {@code Asset} interface.
     *
     *  @return a list of assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getBranding()
        throws org.osid.OperationFailedException {

        if (this.provider == null) {
            return (this.profile.getBranding());
        } else {
            return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.provider.getProvider().getBranding()));
        }
    }


    /**
     *  Gets the terms of usage. An empty license means the terms are
     *  unknown.
     *
     *  @return the license 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLicense() {
        if (this.provider == null) {
            return (this.profile.getLicense());
        } else {
            return (this.provider.getProvider().getLicense());
        }
    }
}

