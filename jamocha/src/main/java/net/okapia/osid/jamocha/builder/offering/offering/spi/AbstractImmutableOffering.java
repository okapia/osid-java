//
// AbstractImmutableOffering.java
//
//     Wraps a mutable Offering to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Offering</code> to hide modifiers. This
 *  wrapper provides an immutized Offering from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying offering whose state changes are visible.
 */

public abstract class AbstractImmutableOffering
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.offering.Offering {

    private final org.osid.offering.Offering offering;


    /**
     *  Constructs a new <code>AbstractImmutableOffering</code>.
     *
     *  @param offering the offering to immutablize
     *  @throws org.osid.NullArgumentException <code>offering</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOffering(org.osid.offering.Offering offering) {
        super(offering);
        this.offering = offering;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the canonical unit. 
     *
     *  @return the canonical unit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCanonicalUnitId() {
        return (this.offering.getCanonicalUnitId());
    }


    /**
     *  Gets the canonical of this offering. 
     *
     *  @return the canonical unit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnit getCanonicalUnit()
        throws org.osid.OperationFailedException {

        return (this.offering.getCanonicalUnit());
    }


    /**
     *  Gets the <code> Id </code> of the time period. 
     *
     *  @return the time period <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        return (this.offering.getTimePeriodId());
    }


    /**
     *  Gets the time period of this offering 
     *
     *  @return the time period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        return (this.offering.getTimePeriod());
    }


    /**
     *  Gets the title for this offering. 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.offering.getTitle());
    }


    /**
     *  Gets the code for this offering. 
     *
     *  @return the code 
     */

    @OSID @Override
    public String getCode() {
        return (this.offering.getCode());
    }


    /**
     *  Tests if this offering has results when offered. 
     *
     *  @return <code> true </code> if this offering has results,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasResults() {
        return (this.offering.hasResults());
    }


    /**
     *  Gets the various result option <code> Ids </code> allowed for
     *  results.
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>hasResults()</code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getResultOptionIds() {
        return (this.offering.getResultOptionIds());
    }


    /**
     *  Gets the various result options allowed for this offering. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code>hasResults()
     *          /code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getResultOptions()
        throws org.osid.OperationFailedException {

        return (this.offering.getResultOptions());
    }


    /**
     *  Tests if this offering has sponsors. 
     *
     *  @return <code> true </code> if this offering has sponsors, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.offering.hasSponsors());
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.offering.getSponsorIds());
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.offering.getSponsors());
    }


    /**
     *  Gets the schedule <code> Ids </code> associated with this offering. 
     *
     *  @return the schedule <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleIds() {
        return (this.offering.getScheduleIds());
    }


    /**
     *  Gets the schedules associated with this offering. 
     *
     *  @return the schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException {

        return (this.offering.getSchedules());
    }


    /**
     *  Gets the record corresponding to the given <code> Offering </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> offeringtRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(offeringRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  offeringRecordType the type of offering record to retrieve 
     *  @return the offering record 
     *  @throws org.osid.NullArgumentException <code> offeringRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(offeringRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.records.OfferingRecord getOfferingRecord(org.osid.type.Type offeringRecordType)
        throws org.osid.OperationFailedException {

        return (this.offering.getOfferingRecord(offeringRecordType));
    }
}

