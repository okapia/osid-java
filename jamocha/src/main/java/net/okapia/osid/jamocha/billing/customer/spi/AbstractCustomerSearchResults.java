//
// AbstractCustomerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.customer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCustomerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.billing.CustomerSearchResults {

    private org.osid.billing.CustomerList customers;
    private final org.osid.billing.CustomerQueryInspector inspector;
    private final java.util.Collection<org.osid.billing.records.CustomerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCustomerSearchResults.
     *
     *  @param customers the result set
     *  @param customerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>customers</code>
     *          or <code>customerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCustomerSearchResults(org.osid.billing.CustomerList customers,
                                            org.osid.billing.CustomerQueryInspector customerQueryInspector) {
        nullarg(customers, "customers");
        nullarg(customerQueryInspector, "customer query inspectpr");

        this.customers = customers;
        this.inspector = customerQueryInspector;

        return;
    }


    /**
     *  Gets the customer list resulting from a search.
     *
     *  @return a customer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomers() {
        if (this.customers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.billing.CustomerList customers = this.customers;
        this.customers = null;
	return (customers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.billing.CustomerQueryInspector getCustomerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  customer search record <code> Type. </code> This method must
     *  be used to retrieve a customer implementing the requested
     *  record.
     *
     *  @param customerSearchRecordType a customer search 
     *         record type 
     *  @return the customer search
     *  @throws org.osid.NullArgumentException
     *          <code>customerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(customerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CustomerSearchResultsRecord getCustomerSearchResultsRecord(org.osid.type.Type customerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.billing.records.CustomerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(customerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(customerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record customer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCustomerRecord(org.osid.billing.records.CustomerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "customer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
