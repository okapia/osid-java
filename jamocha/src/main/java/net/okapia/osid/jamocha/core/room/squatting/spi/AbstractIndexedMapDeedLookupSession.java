//
// AbstractIndexedMapDeedLookupSession.java
//
//    A simple framework for providing a Deed lookup service
//    backed by a fixed collection of deeds with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Deed lookup service backed by a
 *  fixed collection of deeds. The deeds are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some deeds may be compatible
 *  with more types than are indicated through these deed
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Deeds</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDeedLookupSession
    extends AbstractMapDeedLookupSession
    implements org.osid.room.squatting.DeedLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.squatting.Deed> deedsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.squatting.Deed>());
    private final MultiMap<org.osid.type.Type, org.osid.room.squatting.Deed> deedsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.squatting.Deed>());


    /**
     *  Makes a <code>Deed</code> available in this session.
     *
     *  @param  deed a deed
     *  @throws org.osid.NullArgumentException <code>deed<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDeed(org.osid.room.squatting.Deed deed) {
        super.putDeed(deed);

        this.deedsByGenus.put(deed.getGenusType(), deed);
        
        try (org.osid.type.TypeList types = deed.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.deedsByRecord.put(types.getNextType(), deed);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a deed from this session.
     *
     *  @param deedId the <code>Id</code> of the deed
     *  @throws org.osid.NullArgumentException <code>deedId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDeed(org.osid.id.Id deedId) {
        org.osid.room.squatting.Deed deed;
        try {
            deed = getDeed(deedId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.deedsByGenus.remove(deed.getGenusType());

        try (org.osid.type.TypeList types = deed.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.deedsByRecord.remove(types.getNextType(), deed);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDeed(deedId);
        return;
    }


    /**
     *  Gets a <code>DeedList</code> corresponding to the given
     *  deed genus <code>Type</code> which does not include
     *  deeds of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known deeds or an error results. Otherwise,
     *  the returned list may contain only those deeds that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusType(org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.squatting.deed.ArrayDeedList(this.deedsByGenus.get(deedGenusType)));
    }


    /**
     *  Gets a <code>DeedList</code> containing the given
     *  deed record <code>Type</code>. In plenary mode, the
     *  returned list contains all known deeds or an error
     *  results. Otherwise, the returned list may contain only those
     *  deeds that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  deedRecordType a deed record type 
     *  @return the returned <code>deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByRecordType(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.squatting.deed.ArrayDeedList(this.deedsByRecord.get(deedRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.deedsByGenus.clear();
        this.deedsByRecord.clear();

        super.close();

        return;
    }
}
