//
// AbstractRelevancy.java
//
//     Defines a Relevancy builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ontology.relevancy.spi;


/**
 *  Defines a <code>Relevancy</code> builder.
 */

public abstract class AbstractRelevancyBuilder<T extends AbstractRelevancyBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ontology.relevancy.RelevancyMiter relevancy;


    /**
     *  Constructs a new <code>AbstractRelevancyBuilder</code>.
     *
     *  @param relevancy the relevancy to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRelevancyBuilder(net.okapia.osid.jamocha.builder.ontology.relevancy.RelevancyMiter relevancy) {
        super(relevancy);
        this.relevancy = relevancy;
        return;
    }


    /**
     *  Builds the relevancy.
     *
     *  @return the new relevancy
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ontology.Relevancy build() {
        (new net.okapia.osid.jamocha.builder.validator.ontology.relevancy.RelevancyValidator(getValidations())).validate(this.relevancy);
        return (new net.okapia.osid.jamocha.builder.ontology.relevancy.ImmutableRelevancy(this.relevancy));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the relevancy miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ontology.relevancy.RelevancyMiter getMiter() {
        return (this.relevancy);
    }


    /**
     *  Sets the subject.
     *
     *  @param subject a subject
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subject</code> is <code>null</code>
     */

    public T subject(org.osid.ontology.Subject subject) {
        getMiter().setSubject(subject);
        return (self());
    }


    /**
     *  Sets the mapped id.
     *
     *  @param mappedId a mapped id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>mappedId</code> is <code>null</code>
     */

    public T mappedId(org.osid.id.Id mappedId) {
        getMiter().setMappedId(mappedId);
        return (self());
    }


    /**
     *  Adds a Relevancy record.
     *
     *  @param record a relevancy record
     *  @param recordType the type of relevancy record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ontology.records.RelevancyRecord record, org.osid.type.Type recordType) {
        getMiter().addRelevancyRecord(record, recordType);
        return (self());
    }
}       


