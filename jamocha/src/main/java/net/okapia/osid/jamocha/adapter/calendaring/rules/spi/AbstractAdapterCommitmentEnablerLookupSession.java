//
// AbstractAdapterCommitmentEnablerLookupSession.java
//
//    A CommitmentEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CommitmentEnabler lookup session adapter.
 */

public abstract class AbstractAdapterCommitmentEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.rules.CommitmentEnablerLookupSession {

    private final org.osid.calendaring.rules.CommitmentEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCommitmentEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCommitmentEnablerLookupSession(org.osid.calendaring.rules.CommitmentEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code CommitmentEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCommitmentEnablers() {
        return (this.session.canLookupCommitmentEnablers());
    }


    /**
     *  A complete view of the {@code CommitmentEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommitmentEnablerView() {
        this.session.useComparativeCommitmentEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code CommitmentEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommitmentEnablerView() {
        this.session.usePlenaryCommitmentEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commitment enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active commitment enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCommitmentEnablerView() {
        this.session.useActiveCommitmentEnablerView();
        return;
    }


    /**
     *  Active and inactive commitment enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCommitmentEnablerView() {
        this.session.useAnyStatusCommitmentEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code CommitmentEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CommitmentEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CommitmentEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param commitmentEnablerId {@code Id} of the {@code CommitmentEnabler}
     *  @return the commitment enabler
     *  @throws org.osid.NotFoundException {@code commitmentEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code commitmentEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnabler getCommitmentEnabler(org.osid.id.Id commitmentEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommitmentEnabler(commitmentEnablerId));
    }


    /**
     *  Gets a {@code CommitmentEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commitmentEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CommitmentEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CommitmentEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code commitmentEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByIds(org.osid.id.IdList commitmentEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommitmentEnablersByIds(commitmentEnablerIds));
    }


    /**
     *  Gets a {@code CommitmentEnablerList} corresponding to the given
     *  commitment enabler genus {@code Type} which does not include
     *  commitment enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerGenusType a commitmentEnabler genus type 
     *  @return the returned {@code CommitmentEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code commitmentEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByGenusType(org.osid.type.Type commitmentEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommitmentEnablersByGenusType(commitmentEnablerGenusType));
    }


    /**
     *  Gets a {@code CommitmentEnablerList} corresponding to the given
     *  commitment enabler genus {@code Type} and include any additional
     *  commitment enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerGenusType a commitmentEnabler genus type 
     *  @return the returned {@code CommitmentEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code commitmentEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByParentGenusType(org.osid.type.Type commitmentEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommitmentEnablersByParentGenusType(commitmentEnablerGenusType));
    }


    /**
     *  Gets a {@code CommitmentEnablerList} containing the given
     *  commitment enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerRecordType a commitmentEnabler record type 
     *  @return the returned {@code CommitmentEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code commitmentEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByRecordType(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommitmentEnablersByRecordType(commitmentEnablerRecordType));
    }


    /**
     *  Gets a {@code CommitmentEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible
     *  through this session.
     *  
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CommitmentEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommitmentEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code CommitmentEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible
     *  through this session.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code CommitmentEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getCommitmentEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code CommitmentEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @return a list of {@code CommitmentEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommitmentEnablers());
    }
}
