//
// AbstractQueryBlogLookupSession.java
//
//    An inline adapter that maps a BlogLookupSession to
//    a BlogQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BlogLookupSession to
 *  a BlogQuerySession.
 */

public abstract class AbstractQueryBlogLookupSession
    extends net.okapia.osid.jamocha.blogging.spi.AbstractBlogLookupSession
    implements org.osid.blogging.BlogLookupSession {

    private final org.osid.blogging.BlogQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBlogLookupSession.
     *
     *  @param querySession the underlying blog query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBlogLookupSession(org.osid.blogging.BlogQuerySession querySession) {
        nullarg(querySession, "blog query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Blog</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBlogs() {
        return (this.session.canSearchBlogs());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Blog</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Blog</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Blog</code> and
     *  retained for compatibility.
     *
     *  @param  blogId <code>Id</code> of the
     *          <code>Blog</code>
     *  @return the blog
     *  @throws org.osid.NotFoundException <code>blogId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>blogId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.BlogQuery query = getQuery();
        query.matchId(blogId, true);
        org.osid.blogging.BlogList blogs = this.session.getBlogsByQuery(query);
        if (blogs.hasNext()) {
            return (blogs.getNextBlog());
        } 
        
        throw new org.osid.NotFoundException(blogId + " not found");
    }


    /**
     *  Gets a <code>BlogList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  blogs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Blogs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  blogIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>blogIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByIds(org.osid.id.IdList blogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.BlogQuery query = getQuery();

        try (org.osid.id.IdList ids = blogIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBlogsByQuery(query));
    }


    /**
     *  Gets a <code>BlogList</code> corresponding to the given
     *  blog genus <code>Type</code> which does not include
     *  blogs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blogGenusType a blog genus type 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByGenusType(org.osid.type.Type blogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.BlogQuery query = getQuery();
        query.matchGenusType(blogGenusType, true);
        return (this.session.getBlogsByQuery(query));
    }


    /**
     *  Gets a <code>BlogList</code> corresponding to the given
     *  blog genus <code>Type</code> and include any additional
     *  blogs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blogGenusType a blog genus type 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByParentGenusType(org.osid.type.Type blogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.BlogQuery query = getQuery();
        query.matchParentGenusType(blogGenusType, true);
        return (this.session.getBlogsByQuery(query));
    }


    /**
     *  Gets a <code>BlogList</code> containing the given
     *  blog record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blogRecordType a blog record type 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByRecordType(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.BlogQuery query = getQuery();
        query.matchRecordType(blogRecordType, true);
        return (this.session.getBlogsByQuery(query));
    }


    /**
     *  Gets a <code>BlogList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known blogs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  blogs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Blog</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.BlogQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getBlogsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Blogs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Blogs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.BlogQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBlogsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.blogging.BlogQuery getQuery() {
        org.osid.blogging.BlogQuery query = this.session.getBlogQuery();
        return (query);
    }
}
