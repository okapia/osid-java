//
// MutableIndexedMapProxyActivityBundleLookupSession
//
//    Implements an ActivityBundle lookup service backed by a collection of
//    activityBundles indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration;


/**
 *  Implements an ActivityBundle lookup service backed by a collection of
 *  activityBundles. The activity bundles are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some activityBundles may be compatible
 *  with more types than are indicated through these activityBundle
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of activity bundles can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyActivityBundleLookupSession
    extends net.okapia.osid.jamocha.core.course.registration.spi.AbstractIndexedMapActivityBundleLookupSession
    implements org.osid.course.registration.ActivityBundleLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityBundleLookupSession} with
     *  no activity bundle.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityBundleLookupSession} with
     *  a single activity bundle.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityBundle an activity bundle
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityBundle}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.registration.ActivityBundle activityBundle, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putActivityBundle(activityBundle);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityBundleLookupSession} using
     *  an array of activity bundles.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityBundles an array of activity bundles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityBundles}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.registration.ActivityBundle[] activityBundles, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putActivityBundles(activityBundles);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityBundleLookupSession} using
     *  a collection of activity bundles.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityBundles a collection of activity bundles
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityBundles}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       java.util.Collection<? extends org.osid.course.registration.ActivityBundle> activityBundles,
                                                       org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putActivityBundles(activityBundles);
        return;
    }

    
    /**
     *  Makes an {@code ActivityBundle} available in this session.
     *
     *  @param  activityBundle an activity bundle
     *  @throws org.osid.NullArgumentException {@code activityBundle{@code 
     *          is {@code null}
     */

    @Override
    public void putActivityBundle(org.osid.course.registration.ActivityBundle activityBundle) {
        super.putActivityBundle(activityBundle);
        return;
    }


    /**
     *  Makes an array of activity bundles available in this session.
     *
     *  @param  activityBundles an array of activity bundles
     *  @throws org.osid.NullArgumentException {@code activityBundles{@code 
     *          is {@code null}
     */

    @Override
    public void putActivityBundles(org.osid.course.registration.ActivityBundle[] activityBundles) {
        super.putActivityBundles(activityBundles);
        return;
    }


    /**
     *  Makes collection of activity bundles available in this session.
     *
     *  @param  activityBundles a collection of activity bundles
     *  @throws org.osid.NullArgumentException {@code activityBundle{@code 
     *          is {@code null}
     */

    @Override
    public void putActivityBundles(java.util.Collection<? extends org.osid.course.registration.ActivityBundle> activityBundles) {
        super.putActivityBundles(activityBundles);
        return;
    }


    /**
     *  Removes an ActivityBundle from this session.
     *
     *  @param activityBundleId the {@code Id} of the activity bundle
     *  @throws org.osid.NullArgumentException {@code activityBundleId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActivityBundle(org.osid.id.Id activityBundleId) {
        super.removeActivityBundle(activityBundleId);
        return;
    }    
}
