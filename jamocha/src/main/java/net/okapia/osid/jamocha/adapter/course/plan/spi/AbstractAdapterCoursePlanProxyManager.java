//
// AbstractCoursePlanProxyManager.java
//
//     An adapter for a CoursePlanProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CoursePlanProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCoursePlanProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.course.plan.CoursePlanProxyManager>
    implements org.osid.course.plan.CoursePlanProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCoursePlanProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCoursePlanProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCoursePlanProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCoursePlanProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any lesson federation is exposed. Federation is exposed when 
     *  a specific lesson may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  lessonsappears as a single lesson. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a plan lookup service. 
     *
     *  @return <code> true </code> if plan lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanLookup() {
        return (getAdapteeManager().supportsPlanLookup());
    }


    /**
     *  Tests if querying plans is available. 
     *
     *  @return <code> true </code> if plan query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanQuery() {
        return (getAdapteeManager().supportsPlanQuery());
    }


    /**
     *  Tests if searching for plans is available. 
     *
     *  @return <code> true </code> if plan search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanSearch() {
        return (getAdapteeManager().supportsPlanSearch());
    }


    /**
     *  Tests if searching for plans is available. 
     *
     *  @return <code> true </code> if plan search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanAdmin() {
        return (getAdapteeManager().supportsPlanAdmin());
    }


    /**
     *  Tests if plan notification is available. 
     *
     *  @return <code> true </code> if plan notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanNotification() {
        return (getAdapteeManager().supportsPlanNotification());
    }


    /**
     *  Tests if a plan to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if plan course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanCourseCatalog() {
        return (getAdapteeManager().supportsPlanCourseCatalog());
    }


    /**
     *  Tests if a plan to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if plan course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanCourseCatalogAssignment() {
        return (getAdapteeManager().supportsPlanCourseCatalogAssignment());
    }


    /**
     *  Tests if a plan smart course catalog session is available. 
     *
     *  @return <code> true </code> if plan smart course catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanSmartCourseCatalog() {
        return (getAdapteeManager().supportsPlanSmartCourseCatalog());
    }


    /**
     *  Tests for the availability of a lesson lookup service. 
     *
     *  @return <code> true </code> if lesson lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonLookup() {
        return (getAdapteeManager().supportsLessonLookup());
    }


    /**
     *  Tests if querying lessonsis available. 
     *
     *  @return <code> true </code> if lesson query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonQuery() {
        return (getAdapteeManager().supportsLessonQuery());
    }


    /**
     *  Tests if searching for lessons is available. 
     *
     *  @return <code> true </code> if lesson search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonSearch() {
        return (getAdapteeManager().supportsLessonSearch());
    }


    /**
     *  Tests for the availability of a lesson administrative service for 
     *  creating and deleting lessons. 
     *
     *  @return <code> true </code> if lesson administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonAdmin() {
        return (getAdapteeManager().supportsLessonAdmin());
    }


    /**
     *  Tests for the availability of a lesson notification service. 
     *
     *  @return <code> true </code> if lesson notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonNotification() {
        return (getAdapteeManager().supportsLessonNotification());
    }


    /**
     *  Tests if a lesson to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if lesson course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonCourseCatalog() {
        return (getAdapteeManager().supportsLessonCourseCatalog());
    }


    /**
     *  Tests if a lesson to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if lesson course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonCourseCatalogAssignment() {
        return (getAdapteeManager().supportsLessonCourseCatalogAssignment());
    }


    /**
     *  Tests if a lesson smart course catalog session is available. 
     *
     *  @return <code> true </code> if lesson smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonSmartCourseCatalog() {
        return (getAdapteeManager().supportsLessonSmartCourseCatalog());
    }


    /**
     *  Tests if looking at lesson conflicts available. 
     *
     *  @return <code> true </code> if lesson conflict is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonConflict() {
        return (getAdapteeManager().supportsLessonConflict());
    }


    /**
     *  Tests if anchoring lessons is available. 
     *
     *  @return <code> true </code> if lesson anchoring is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonAnchoring() {
        return (getAdapteeManager().supportsLessonAnchoring());
    }


    /**
     *  Gets the supported <code> Syllabus </code> record types. 
     *
     *  @return a list containing the supported syllabus record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusRecordTypes() {
        return (getAdapteeManager().getSyllabusRecordTypes());
    }


    /**
     *  Tests if the given <code> Syllabus </code> record type is supported. 
     *
     *  @param  syllabusRecordType a <code> Type </code> indicating a <code> 
     *          Syllabus </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        return (getAdapteeManager().supportsSyllabusRecordType(syllabusRecordType));
    }


    /**
     *  Gets the supported syllabus search record types. 
     *
     *  @return a list containing the supported syllabus search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusSearchRecordTypes() {
        return (getAdapteeManager().getSyllabusSearchRecordTypes());
    }


    /**
     *  Tests if the given syllabus search record type is supported. 
     *
     *  @param  syllabusSearchRecordType a <code> Type </code> indicating a 
     *          syllabus record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        return (getAdapteeManager().supportsSyllabusSearchRecordType(syllabusSearchRecordType));
    }


    /**
     *  Gets the supported <code> Plan </code> record types. 
     *
     *  @return a list containing the supported plan record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPlanRecordTypes() {
        return (getAdapteeManager().getPlanRecordTypes());
    }


    /**
     *  Tests if the given <code> Plan </code> record type is supported. 
     *
     *  @param  planRecordType a <code> Type </code> indicating a <code> Plan 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> planRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPlanRecordType(org.osid.type.Type planRecordType) {
        return (getAdapteeManager().supportsPlanRecordType(planRecordType));
    }


    /**
     *  Gets the supported plan search record types. 
     *
     *  @return a list containing the supported plan search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPlanSearchRecordTypes() {
        return (getAdapteeManager().getPlanSearchRecordTypes());
    }


    /**
     *  Tests if the given plan search record type is supported. 
     *
     *  @param  planSearchRecordType a <code> Type </code> indicating a plan 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> planSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPlanSearchRecordType(org.osid.type.Type planSearchRecordType) {
        return (getAdapteeManager().supportsPlanSearchRecordType(planSearchRecordType));
    }


    /**
     *  Gets the supported <code> Lesson </code> record types. 
     *
     *  @return a list containing the supported lesson record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLessonRecordTypes() {
        return (getAdapteeManager().getLessonRecordTypes());
    }


    /**
     *  Tests if the given <code> Lesson </code> record type is supported. 
     *
     *  @param  lessonRecordType a <code> Type </code> indicating a <code> 
     *          Lesson </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> lessonRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLessonRecordType(org.osid.type.Type lessonRecordType) {
        return (getAdapteeManager().supportsLessonRecordType(lessonRecordType));
    }


    /**
     *  Gets the supported lesson search record types. 
     *
     *  @return a list containing the supported lesson search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLessonSearchRecordTypes() {
        return (getAdapteeManager().getLessonSearchRecordTypes());
    }


    /**
     *  Tests if the given lesson search record type is supported. 
     *
     *  @param  lessonSearchRecordType a <code> Type </code> indicating a 
     *          lesson record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> lessonSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLessonSearchRecordType(org.osid.type.Type lessonSearchRecordType) {
        return (getAdapteeManager().supportsLessonSearchRecordType(lessonSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanLookupSession getPlanLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanLookupSession getPlanLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuerySession getPlanQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuerySession getPlanQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchSession getPlanSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchSession getPlanSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanSearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanAdminSession getPlanAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanAdminSession getPlanAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  notification service. 
     *
     *  @param  planReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PlanNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> planReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanNotificationSession getPlanNotificationSession(org.osid.course.plan.PlanReceiver planReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanNotificationSession(planReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  notification service for the given course catalog. 
     *
     *  @param  planReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> planReceiver, 
     *          courseCatalogId, </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanNotificationSession getPlanNotificationSessionForCourseCatalog(org.osid.course.plan.PlanReceiver planReceiver, 
                                                                                                   org.osid.id.Id courseCatalogId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanNotificationSessionForCourseCatalog(planReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the session for retrieving plan to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanCourseCatalogSession getPlanCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanCourseCatalogSession(proxy));
    }


    /**
     *  Gets the session for assigning plan to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanCourseCatalogAssignmentSession getPlanCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the plan smart course catalog for the 
     *  given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> PlanSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSmartCourseCatalogSession getPlanSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPlanSmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonLookupSession getLessonLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonLookupSession getLessonLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuerySession getLessonQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuerySession getLessonQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSearchSession getLessonSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSearchSession getLessonSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonSearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonAdminSession getLessonAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonAdminSession getLessonAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  notification service. 
     *
     *  @param  lessonReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> LessonNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> lessonReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonNotificationSession getLessonNotificationSession(org.osid.course.plan.LessonReceiver lessonReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonNotificationSession(lessonReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  notification service for the given course catalog. 
     *
     *  @param  lessonReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> lessonReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonNotificationSession getLessonNotificationSessionForCourseCatalog(org.osid.course.plan.LessonReceiver lessonReceiver, 
                                                                                                       org.osid.id.Id courseCatalogId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonNotificationSessionForCourseCatalog(lessonReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the session for retrieving lesson to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonCourseCatalogSession getLessonCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonCourseCatalogSession(proxy));
    }


    /**
     *  Gets the session for assigning lesson to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonCourseCatalogAssignmentSession getLessonCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic lesson course catalogs for the 
     *  given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of a course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> LessonSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSmartCourseCatalogSession getLessonSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonSmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonConflictSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonConflictSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonConflictSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonConflictSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonAnchoringSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonAnchoringSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  anchoring service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> LessonAnchoringSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonAnchoring() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonAnchoringSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLessonAnchoringSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
