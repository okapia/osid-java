//
// AbstractImmutableSetting.java
//
//     Wraps a mutable Setting to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.setting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Setting</code> to hide modifiers. This
 *  wrapper provides an immutized Setting from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying setting whose state changes are visible.
 */

public abstract class AbstractImmutableSetting
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.control.Setting {

    private final org.osid.control.Setting setting;


    /**
     *  Constructs a new <code>AbstractImmutableSetting</code>.
     *
     *  @param setting the setting to immutablize
     *  @throws org.osid.NullArgumentException <code>setting</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSetting(org.osid.control.Setting setting) {
        super(setting);
        this.setting = setting;
        return;
    }


    /**
     *  Gets the controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getControllerId() {
        return (this.setting.getControllerId());
    }


    /**
     *  Gets the controller. 
     *
     *  @return the controller 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getController()
        throws org.osid.OperationFailedException {

        return (this.setting.getController());
    }


    /**
     *  Tests if the toggleable controller is on. 
     *
     *  @return <code> true </code> if the controller is on, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.isToggleable() </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean isOn() {
        return (this.setting.isOn());
    }


    /**
     *  Tests if the toggleable controller is off. 
     *
     *  @return <code> true </code> if the controller is iff, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.isToggleable() </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean isOff() {
        return (this.setting.isOff());
    }


    /**
     *  Gets the level amount on a fixed scale. 
     *
     *  @return the level amount 
     *  @throws org.osid.IllegalStateException <code> Controller.isVariable() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getVariableAmount() {
        return (this.setting.getVariableAmount());
    }


    /**
     *  Gets the discreet <code> State </code> <code> Id. </code> 
     *
     *  @return the state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.hasDiscreetStates() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDiscreetStateId() {
        return (this.setting.getDiscreetStateId());
    }


    /**
     *  Gets the discreet <code> State. </code> 
     *
     *  @return the state 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.hasDiscreetStates() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getDiscreetState()
        throws org.osid.OperationFailedException {

        return (this.setting.getDiscreetState());
    }


    /**
     *  Gets the ramp rate from off to on to use for the transition for this 
     *  setting. If the ramp rate is 10 seconds and the variable percentage is 
     *  50%, then the actual transition duration from off would be 5 seconds 
     *  for a linear controller.. 
     *
     *  @return the ramp rate 
     *  @throws org.osid.IllegalStateException <code> Controller.isRampable() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRampRate() {
        return (this.setting.getRampRate());
    }


    /**
     *  Gets the setting record corresponding to the given <code>
     *  Setting </code> record <code> Type. </code> This method is
     *  used to retrieve an object implementing the requested
     *  record. The <code> settingRecordType </code> may be the <code>
     *  Type </code> returned in <code> getRecordTypes() </code> or
     *  any of its parents in a <code> Type </code> hierarchy where
     *  <code> hasRecordType(settingRecordType) </code> is <code> true
     *  </code>.
     *
     *  @param  settingRecordType the type of setting record to retrieve 
     *  @return the setting record 
     *  @throws org.osid.NullArgumentException <code> settingRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(settingRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.records.SettingRecord getSettingRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        return (this.setting.getSettingRecord(settingRecordType));
    }
}

