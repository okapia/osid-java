//
// AbstractNodeBinHierarchySession.java
//
//     Defines a Bin hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a bin hierarchy session for delivering a hierarchy
 *  of bins using the BinNode interface.
 */

public abstract class AbstractNodeBinHierarchySession
    extends net.okapia.osid.jamocha.resource.spi.AbstractBinHierarchySession
    implements org.osid.resource.BinHierarchySession {

    private java.util.Collection<org.osid.resource.BinNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root bin <code> Ids </code> in this hierarchy.
     *
     *  @return the root bin <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootBinIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.resource.binnode.BinNodeToIdList(this.roots));
    }


    /**
     *  Gets the root bins in the bin hierarchy. A node
     *  with no parents is an orphan. While all bin <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root bins 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getRootBins()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resource.binnode.BinNodeToBinList(new net.okapia.osid.jamocha.resource.binnode.ArrayBinNodeList(this.roots)));
    }


    /**
     *  Adds a root bin node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootBin(org.osid.resource.BinNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root bin nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootBins(java.util.Collection<org.osid.resource.BinNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root bin node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootBin(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.resource.BinNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Bin </code> has any parents. 
     *
     *  @param  binId a bin <code> Id </code> 
     *  @return <code> true </code> if the bin has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentBins(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getBinNode(binId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  bin.
     *
     *  @param  id an <code> Id </code> 
     *  @param  binId the <code> Id </code> of a bin 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> binId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> binId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfBin(org.osid.id.Id id, org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resource.BinNodeList parents = getBinNode(binId).getParentBinNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextBinNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given bin. 
     *
     *  @param  binId a bin <code> Id </code> 
     *  @return the parent <code> Ids </code> of the bin 
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentBinIds(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resource.bin.BinToIdList(getParentBins(binId)));
    }


    /**
     *  Gets the parents of the given bin. 
     *
     *  @param  binId the <code> Id </code> to query 
     *  @return the parents of the bin 
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getParentBins(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resource.binnode.BinNodeToBinList(getBinNode(binId).getParentBinNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  bin.
     *
     *  @param  id an <code> Id </code> 
     *  @param  binId the Id of a bin 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> binId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfBin(org.osid.id.Id id, org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBin(id, binId)) {
            return (true);
        }

        try (org.osid.resource.BinList parents = getParentBins(binId)) {
            while (parents.hasNext()) {
                if (isAncestorOfBin(id, parents.getNextBin().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a bin has any children. 
     *
     *  @param  binId a bin <code> Id </code> 
     *  @return <code> true </code> if the <code> binId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildBins(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBinNode(binId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  bin.
     *
     *  @param  id an <code> Id </code> 
     *  @param binId the <code> Id </code> of a 
     *         bin
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> binId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> binId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfBin(org.osid.id.Id id, org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfBin(binId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  bin.
     *
     *  @param  binId the <code> Id </code> to query 
     *  @return the children of the bin 
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildBinIds(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resource.bin.BinToIdList(getChildBins(binId)));
    }


    /**
     *  Gets the children of the given bin. 
     *
     *  @param  binId the <code> Id </code> to query 
     *  @return the children of the bin 
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getChildBins(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.resource.binnode.BinNodeToBinList(getBinNode(binId).getChildBinNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  bin.
     *
     *  @param  id an <code> Id </code> 
     *  @param binId the <code> Id </code> of a 
     *         bin
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> binId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfBin(org.osid.id.Id id, org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBin(binId, id)) {
            return (true);
        }

        try (org.osid.resource.BinList children = getChildBins(binId)) {
            while (children.hasNext()) {
                if (isDescendantOfBin(id, children.getNextBin().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  bin.
     *
     *  @param  binId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified bin node 
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getBinNodeIds(org.osid.id.Id binId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.resource.binnode.BinNodeToNode(getBinNode(binId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given bin.
     *
     *  @param  binId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified bin node 
     *  @throws org.osid.NotFoundException <code> binId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> binId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinNode getBinNodes(org.osid.id.Id binId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBinNode(binId));
    }


    /**
     *  Closes this <code>BinHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a bin node.
     *
     *  @param binId the id of the bin node
     *  @throws org.osid.NotFoundException <code>binId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>binId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.resource.BinNode getBinNode(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(binId, "bin Id");
        for (org.osid.resource.BinNode bin : this.roots) {
            if (bin.getId().equals(binId)) {
                return (bin);
            }

            org.osid.resource.BinNode r = findBin(bin, binId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(binId + " is not found");
    }


    protected org.osid.resource.BinNode findBin(org.osid.resource.BinNode node, 
                                                org.osid.id.Id binId) 
	throws org.osid.OperationFailedException {

        try (org.osid.resource.BinNodeList children = node.getChildBinNodes()) {
            while (children.hasNext()) {
                org.osid.resource.BinNode bin = children.getNextBinNode();
                if (bin.getId().equals(binId)) {
                    return (bin);
                }
                
                bin = findBin(bin, binId);
                if (bin != null) {
                    return (bin);
                }
            }
        }

        return (null);
    }
}
