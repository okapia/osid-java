//
// AbstractQueryCanonicalUnitProcessorLookupSession.java
//
//    An inline adapter that maps a CanonicalUnitProcessorLookupSession to
//    a CanonicalUnitProcessorQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CanonicalUnitProcessorLookupSession to
 *  a CanonicalUnitProcessorQuerySession.
 */

public abstract class AbstractQueryCanonicalUnitProcessorLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractCanonicalUnitProcessorLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {

      private boolean activeonly    = false;

    private final org.osid.offering.rules.CanonicalUnitProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCanonicalUnitProcessorLookupSession.
     *
     *  @param querySession the underlying canonical unit processor query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCanonicalUnitProcessorLookupSession(org.osid.offering.rules.CanonicalUnitProcessorQuerySession querySession) {
        nullarg(querySession, "canonical unit processor query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnitProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitProcessors() {
        return (this.session.canSearchCanonicalUnitProcessors());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit processors in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active canonical unit processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive canonical unit processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CanonicalUnitProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnitProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CanonicalUnitProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorId <code>Id</code> of the
     *          <code>CanonicalUnitProcessor</code>
     *  @return the canonical unit processor
     *  @throws org.osid.NotFoundException <code>canonicalUnitProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessor getCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorQuery query = getQuery();
        query.matchId(canonicalUnitProcessorId, true);
        org.osid.offering.rules.CanonicalUnitProcessorList canonicalUnitProcessors = this.session.getCanonicalUnitProcessorsByQuery(query);
        if (canonicalUnitProcessors.hasNext()) {
            return (canonicalUnitProcessors.getNextCanonicalUnitProcessor());
        } 
        
        throw new org.osid.NotFoundException(canonicalUnitProcessorId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CanonicalUnitProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByIds(org.osid.id.IdList canonicalUnitProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorQuery query = getQuery();

        try (org.osid.id.IdList ids = canonicalUnitProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCanonicalUnitProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> corresponding to the given
     *  canonical unit processor genus <code>Type</code> which does not include
     *  canonical unit processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorGenusType a canonicalUnitProcessor genus type 
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByGenusType(org.osid.type.Type canonicalUnitProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorQuery query = getQuery();
        query.matchGenusType(canonicalUnitProcessorGenusType, true);
        return (this.session.getCanonicalUnitProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> corresponding to the given
     *  canonical unit processor genus <code>Type</code> and include any additional
     *  canonical unit processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorGenusType a canonicalUnitProcessor genus type 
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByParentGenusType(org.osid.type.Type canonicalUnitProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorQuery query = getQuery();
        query.matchParentGenusType(canonicalUnitProcessorGenusType, true);
        return (this.session.getCanonicalUnitProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorList</code> containing the given
     *  canonical unit processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorRecordType a canonicalUnitProcessor record type 
     *  @return the returned <code>CanonicalUnitProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByRecordType(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitProcessorQuery query = getQuery();
        query.matchRecordType(canonicalUnitProcessorRecordType, true);
        return (this.session.getCanonicalUnitProcessorsByQuery(query));
    }

    
    /**
     *  Gets all <code>CanonicalUnitProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @return a list of <code>CanonicalUnitProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.rules.CanonicalUnitProcessorQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCanonicalUnitProcessorsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.rules.CanonicalUnitProcessorQuery getQuery() {
        org.osid.offering.rules.CanonicalUnitProcessorQuery query = this.session.getCanonicalUnitProcessorQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
