//
// AbstractQueryProfileLookupSession.java
//
//    An inline adapter that maps a ProfileLookupSession to
//    a ProfileQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProfileLookupSession to
 *  a ProfileQuerySession.
 */

public abstract class AbstractQueryProfileLookupSession
    extends net.okapia.osid.jamocha.profile.spi.AbstractProfileLookupSession
    implements org.osid.profile.ProfileLookupSession {

    private final org.osid.profile.ProfileQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProfileLookupSession.
     *
     *  @param querySession the underlying profile query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProfileLookupSession(org.osid.profile.ProfileQuerySession querySession) {
        nullarg(querySession, "profile query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Profile</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProfiles() {
        return (this.session.canSearchProfiles());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Profile</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Profile</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Profile</code> and
     *  retained for compatibility.
     *
     *  @param  profileId <code>Id</code> of the
     *          <code>Profile</code>
     *  @return the profile
     *  @throws org.osid.NotFoundException <code>profileId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>profileId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileQuery query = getQuery();
        query.matchId(profileId, true);
        org.osid.profile.ProfileList profiles = this.session.getProfilesByQuery(query);
        if (profiles.hasNext()) {
            return (profiles.getNextProfile());
        } 
        
        throw new org.osid.NotFoundException(profileId + " not found");
    }


    /**
     *  Gets a <code>ProfileList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profiles specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Profiles</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  profileIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Profile</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>profileIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByIds(org.osid.id.IdList profileIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileQuery query = getQuery();

        try (org.osid.id.IdList ids = profileIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProfilesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileList</code> corresponding to the given
     *  profile genus <code>Type</code> which does not include
     *  profiles of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileGenusType a profile genus type 
     *  @return the returned <code>Profile</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByGenusType(org.osid.type.Type profileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileQuery query = getQuery();
        query.matchGenusType(profileGenusType, true);
        return (this.session.getProfilesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileList</code> corresponding to the given
     *  profile genus <code>Type</code> and include any additional
     *  profiles with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileGenusType a profile genus type 
     *  @return the returned <code>Profile</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByParentGenusType(org.osid.type.Type profileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileQuery query = getQuery();
        query.matchParentGenusType(profileGenusType, true);
        return (this.session.getProfilesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileList</code> containing the given
     *  profile record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileRecordType a profile record type 
     *  @return the returned <code>Profile</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByRecordType(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileQuery query = getQuery();
        query.matchRecordType(profileRecordType, true);
        return (this.session.getProfilesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known profiles or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  profiles that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Profile</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getProfilesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Profiles</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Profiles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfiles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProfilesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.profile.ProfileQuery getQuery() {
        org.osid.profile.ProfileQuery query = this.session.getProfileQuery();
        return (query);
    }
}
