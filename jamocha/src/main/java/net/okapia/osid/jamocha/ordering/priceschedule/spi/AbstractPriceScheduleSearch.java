//
// AbstractPriceScheduleSearch.java
//
//     A template for making a PriceSchedule Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.priceschedule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing price schedule searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPriceScheduleSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ordering.PriceScheduleSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ordering.records.PriceScheduleSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ordering.PriceScheduleSearchOrder priceScheduleSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of price schedules. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  priceScheduleIds list of price schedules
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPriceSchedules(org.osid.id.IdList priceScheduleIds) {
        while (priceScheduleIds.hasNext()) {
            try {
                this.ids.add(priceScheduleIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPriceSchedules</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of price schedule Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPriceScheduleIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  priceScheduleSearchOrder price schedule search order 
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>priceScheduleSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPriceScheduleResults(org.osid.ordering.PriceScheduleSearchOrder priceScheduleSearchOrder) {
	this.priceScheduleSearchOrder = priceScheduleSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ordering.PriceScheduleSearchOrder getPriceScheduleSearchOrder() {
	return (this.priceScheduleSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given price schedule search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a price schedule implementing the requested record.
     *
     *  @param priceScheduleSearchRecordType a price schedule search record
     *         type
     *  @return the price schedule search record
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceScheduleSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleSearchRecord getPriceScheduleSearchRecord(org.osid.type.Type priceScheduleSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ordering.records.PriceScheduleSearchRecord record : this.records) {
            if (record.implementsRecordType(priceScheduleSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceScheduleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price schedule search. 
     *
     *  @param priceScheduleSearchRecord price schedule search record
     *  @param priceScheduleSearchRecordType priceSchedule search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceScheduleSearchRecord(org.osid.ordering.records.PriceScheduleSearchRecord priceScheduleSearchRecord, 
                                           org.osid.type.Type priceScheduleSearchRecordType) {

        addRecordType(priceScheduleSearchRecordType);
        this.records.add(priceScheduleSearchRecord);        
        return;
    }
}
