//
// AbstractPositionQueryInspector.java
//
//     A template for making a PositionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for positions.
 */

public abstract class AbstractPositionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.personnel.PositionQueryInspector {

    private final java.util.Collection<org.osid.personnel.records.PositionQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the organization <code> Id </code> query terms. 
     *
     *  @return the organization <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrganizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the organization query terms. 
     *
     *  @return the organization terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the grade level <code> Id </code> query terms. 
     *
     *  @return the grade level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade level query terms. 
     *
     *  @return the grade level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the qualification <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualificationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the qualification query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getQualificationTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the target appointments query terms. 
     *
     *  @return the target appointments terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getTargetAppointmentsTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the required commitment query terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getRequiredCommitmentTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the low salary query terms. 
     *
     *  @return the low salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getLowSalaryRangeTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the midpoint salary query terms. 
     *
     *  @return the midpoint salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getMidpointSalaryRangeTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the high salary query terms. 
     *
     *  @return the high salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getHighSalaryRangeTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the compensation frequency terms. 
     *
     *  @return the frequency terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getCompensationFrequencyTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the exempt terms. 
     *
     *  @return the exempt terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getExemptTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the benefits type terms. 
     *
     *  @return the benefit type terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getBenefitsTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given position query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a position implementing the requested record.
     *
     *  @param positionRecordType a position record type
     *  @return the position query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(positionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PositionQueryInspectorRecord getPositionQueryInspectorRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PositionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(positionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this position query. 
     *
     *  @param positionQueryInspectorRecord position query inspector
     *         record
     *  @param positionRecordType position record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPositionQueryInspectorRecord(org.osid.personnel.records.PositionQueryInspectorRecord positionQueryInspectorRecord, 
                                                   org.osid.type.Type positionRecordType) {

        addRecordType(positionRecordType);
        nullarg(positionRecordType, "position record type");
        this.records.add(positionQueryInspectorRecord);        
        return;
    }
}
