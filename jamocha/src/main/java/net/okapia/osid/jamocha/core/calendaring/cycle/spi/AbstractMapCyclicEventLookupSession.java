//
// AbstractMapCyclicEventLookupSession
//
//    A simple framework for providing a CyclicEvent lookup service
//    backed by a fixed collection of cyclic events.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CyclicEvent lookup service backed by a
 *  fixed collection of cyclic events. The cyclic events are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CyclicEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCyclicEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.cycle.spi.AbstractCyclicEventLookupSession
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.cycle.CyclicEvent> cyclicEvents = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.cycle.CyclicEvent>());


    /**
     *  Makes a <code>CyclicEvent</code> available in this session.
     *
     *  @param  cyclicEvent a cyclic event
     *  @throws org.osid.NullArgumentException <code>cyclicEvent<code>
     *          is <code>null</code>
     */

    protected void putCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        this.cyclicEvents.put(cyclicEvent.getId(), cyclicEvent);
        return;
    }


    /**
     *  Makes an array of cyclic events available in this session.
     *
     *  @param  cyclicEvents an array of cyclic events
     *  @throws org.osid.NullArgumentException <code>cyclicEvents<code>
     *          is <code>null</code>
     */

    protected void putCyclicEvents(org.osid.calendaring.cycle.CyclicEvent[] cyclicEvents) {
        putCyclicEvents(java.util.Arrays.asList(cyclicEvents));
        return;
    }


    /**
     *  Makes a collection of cyclic events available in this session.
     *
     *  @param  cyclicEvents a collection of cyclic events
     *  @throws org.osid.NullArgumentException <code>cyclicEvents<code>
     *          is <code>null</code>
     */

    protected void putCyclicEvents(java.util.Collection<? extends org.osid.calendaring.cycle.CyclicEvent> cyclicEvents) {
        for (org.osid.calendaring.cycle.CyclicEvent cyclicEvent : cyclicEvents) {
            this.cyclicEvents.put(cyclicEvent.getId(), cyclicEvent);
        }

        return;
    }


    /**
     *  Removes a CyclicEvent from this session.
     *
     *  @param  cyclicEventId the <code>Id</code> of the cyclic event
     *  @throws org.osid.NullArgumentException <code>cyclicEventId<code> is
     *          <code>null</code>
     */

    protected void removeCyclicEvent(org.osid.id.Id cyclicEventId) {
        this.cyclicEvents.remove(cyclicEventId);
        return;
    }


    /**
     *  Gets the <code>CyclicEvent</code> specified by its <code>Id</code>.
     *
     *  @param  cyclicEventId <code>Id</code> of the <code>CyclicEvent</code>
     *  @return the cyclicEvent
     *  @throws org.osid.NotFoundException <code>cyclicEventId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>cyclicEventId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent(org.osid.id.Id cyclicEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.cycle.CyclicEvent cyclicEvent = this.cyclicEvents.get(cyclicEventId);
        if (cyclicEvent == null) {
            throw new org.osid.NotFoundException("cyclicEvent not found: " + cyclicEventId);
        }

        return (cyclicEvent);
    }


    /**
     *  Gets all <code>CyclicEvents</code>. In plenary mode, the returned
     *  list contains all known cyclicEvents or an error
     *  results. Otherwise, the returned list may contain only those
     *  cyclicEvents that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CyclicEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.ArrayCyclicEventList(this.cyclicEvents.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.cyclicEvents.clear();
        super.close();
        return;
    }
}
