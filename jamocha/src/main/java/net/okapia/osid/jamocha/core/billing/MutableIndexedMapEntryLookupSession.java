//
// MutableIndexedMapEntryLookupSession
//
//    Implements an Entry lookup service backed by a collection of
//    entries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements an Entry lookup service backed by a collection of
 *  entries. The entries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some entries may be compatible
 *  with more types than are indicated through these entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapEntryLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractIndexedMapEntryLookupSession
    implements org.osid.billing.EntryLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapEntryLookupSession} with no entries.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business}
     *          is {@code null}
     */

      public MutableIndexedMapEntryLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEntryLookupSession} with a
     *  single entry.
     *  
     *  @param business the business
     *  @param  entry an single entry
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code entry} is {@code null}
     */

    public MutableIndexedMapEntryLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.Entry entry) {
        this(business);
        putEntry(entry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEntryLookupSession} using an
     *  array of entries.
     *
     *  @param business the business
     *  @param  entries an array of entries
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code entries} is {@code null}
     */

    public MutableIndexedMapEntryLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.Entry[] entries) {
        this(business);
        putEntries(entries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEntryLookupSession} using a
     *  collection of entries.
     *
     *  @param business the business
     *  @param  entries a collection of entries
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code entries} is {@code null}
     */

    public MutableIndexedMapEntryLookupSession(org.osid.billing.Business business,
                                                  java.util.Collection<? extends org.osid.billing.Entry> entries) {

        this(business);
        putEntries(entries);
        return;
    }
    

    /**
     *  Makes an {@code Entry} available in this session.
     *
     *  @param  entry an entry
     *  @throws org.osid.NullArgumentException {@code entry{@code  is
     *          {@code null}
     */

    @Override
    public void putEntry(org.osid.billing.Entry entry) {
        super.putEntry(entry);
        return;
    }


    /**
     *  Makes an array of entries available in this session.
     *
     *  @param  entries an array of entries
     *  @throws org.osid.NullArgumentException {@code entries{@code 
     *          is {@code null}
     */

    @Override
    public void putEntries(org.osid.billing.Entry[] entries) {
        super.putEntries(entries);
        return;
    }


    /**
     *  Makes collection of entries available in this session.
     *
     *  @param  entries a collection of entries
     *  @throws org.osid.NullArgumentException {@code entry{@code  is
     *          {@code null}
     */

    @Override
    public void putEntries(java.util.Collection<? extends org.osid.billing.Entry> entries) {
        super.putEntries(entries);
        return;
    }


    /**
     *  Removes an Entry from this session.
     *
     *  @param entryId the {@code Id} of the entry
     *  @throws org.osid.NullArgumentException {@code entryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEntry(org.osid.id.Id entryId) {
        super.removeEntry(entryId);
        return;
    }    
}
