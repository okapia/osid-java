//
// AbstractProfileEntryEnablerQuery.java
//
//     A template for making a ProfileEntryEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.rules.profileentryenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for profile entry enablers.
 */

public abstract class AbstractProfileEntryEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.profile.rules.ProfileEntryEnablerQuery {

    private final java.util.Collection<org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the profile entry. 
     *
     *  @param  profileEntryId the profile entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledProfileEntryId(org.osid.id.Id profileEntryId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledProfileEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledProfileEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getRuledProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsRuledProfileEntryQuery() is false");
    }


    /**
     *  Matches enablers mapped to any profile entry. 
     *
     *  @param  match <code> true </code> for enablers mapped to any profile 
     *          entry, <code> false </code> to match enablers mapped to no 
     *          profile entry 
     */

    @OSID @Override
    public void matchAnyRuledProfileEntry(boolean match) {
        return;
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearRuledProfileEntryTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the profile. 
     *
     *  @param  profileId the profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileId(org.osid.id.Id profileId, boolean match) {
        return;
    }


    /**
     *  Clears the profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getProfileQuery() {
        throw new org.osid.UnimplementedException("supportsProfileQuery() is false");
    }


    /**
     *  Clears the profile query terms. 
     */

    @OSID @Override
    public void clearProfileTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given profile entry enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a profile entry enabler implementing the requested record.
     *
     *  @param profileEntryEnablerRecordType a profile entry enabler record type
     *  @return the profile entry enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord getProfileEntryEnablerQueryRecord(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(profileEntryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile entry enabler query. 
     *
     *  @param profileEntryEnablerQueryRecord profile entry enabler query record
     *  @param profileEntryEnablerRecordType profileEntryEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileEntryEnablerQueryRecord(org.osid.profile.rules.records.ProfileEntryEnablerQueryRecord profileEntryEnablerQueryRecord, 
                                          org.osid.type.Type profileEntryEnablerRecordType) {

        addRecordType(profileEntryEnablerRecordType);
        nullarg(profileEntryEnablerQueryRecord, "profile entry enabler query record");
        this.records.add(profileEntryEnablerQueryRecord);        
        return;
    }
}
