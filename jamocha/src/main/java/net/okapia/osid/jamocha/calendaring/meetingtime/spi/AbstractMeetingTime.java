//
// AbstractMeetingTime.java
//
//     Defines a MeetingTime.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.meetingtime.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>MeetingTime</code>.
 */

public abstract class AbstractMeetingTime
    implements org.osid.calendaring.MeetingTime {

    private org.osid.calendaring.DateTime date;
    private org.osid.locale.DisplayText locationDesc;
    private org.osid.mapping.Location location;

    
    /**
     *  Gets the date. 
     *
     *  @return the date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.date);
    }

    
    /**
     *  Sets the date.
     * 
     *  @param date the date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.date = date;
        return;
    }


    /**
     *  Gets a descriptive text for the location. 
     *
     *  @return a location description 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        if (!hasLocation() && (this.locationDesc == null)) {
            return (this.location.getDisplayName());
        }

        return (this.locationDesc);
    }


    /**
     *  Sets the location descriptive label.
     *
     *  @param location descriptive text for a location
     *  @throws org.osid.NullArgumentException <code>location/code> is
     *          <code>null</code>
     */

    protected void setLocationDescription(org.osid.locale.DisplayText location) {
        nullarg(location, "location description");
        this.locationDesc = location;
        return;
    }


    /**
     *  Tests if a location is available. 
     *
     *  @return <code> true </code> if a location is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        if (this.location == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the location <code>Id</code>. 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location.getId());
    }


    /**
     *  Gets the <code>Location</code>.
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location);
    }


    /**
     *  Sets the location.
     *
     *  @param location thelocation
     *  @throws org.osid.NullArgumentException <code>location/code> is
     *          <code>null</code>
     */

    protected void setLocation(org.osid.mapping.Location location) {
        nullarg(location, "location");
        this.location = location;
        return;
    }
}