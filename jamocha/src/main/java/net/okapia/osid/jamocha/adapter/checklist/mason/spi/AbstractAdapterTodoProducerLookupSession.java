//
// AbstractAdapterTodoProducerLookupSession.java
//
//    A TodoProducer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A TodoProducer lookup session adapter.
 */

public abstract class AbstractAdapterTodoProducerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.checklist.mason.TodoProducerLookupSession {

    private final org.osid.checklist.mason.TodoProducerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTodoProducerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTodoProducerLookupSession(org.osid.checklist.mason.TodoProducerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Checklist/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Checklist Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.session.getChecklistId());
    }


    /**
     *  Gets the {@code Checklist} associated with this session.
     *
     *  @return the {@code Checklist} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getChecklist());
    }


    /**
     *  Tests if this user can perform {@code TodoProducer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupTodoProducers() {
        return (this.session.canLookupTodoProducers());
    }


    /**
     *  A complete view of the {@code TodoProducer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTodoProducerView() {
        this.session.useComparativeTodoProducerView();
        return;
    }


    /**
     *  A complete view of the {@code TodoProducer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTodoProducerView() {
        this.session.usePlenaryTodoProducerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todo producers in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.session.useFederatedChecklistView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        this.session.useIsolatedChecklistView();
        return;
    }
    

    /**
     *  Only active todo producers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTodoProducerView() {
        this.session.useActiveTodoProducerView();
        return;
    }


    /**
     *  Active and inactive todo producers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTodoProducerView() {
        this.session.useAnyStatusTodoProducerView();
        return;
    }
    
     
    /**
     *  Gets the {@code TodoProducer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code TodoProducer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code TodoProducer} and
     *  retained for compatibility.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param todoProducerId {@code Id} of the {@code TodoProducer}
     *  @return the todo producer
     *  @throws org.osid.NotFoundException {@code todoProducerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code todoProducerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducer(org.osid.id.Id todoProducerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodoProducer(todoProducerId));
    }


    /**
     *  Gets a {@code TodoProducerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  todoProducers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code TodoProducers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code TodoProducer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code todoProducerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByIds(org.osid.id.IdList todoProducerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodoProducersByIds(todoProducerIds));
    }


    /**
     *  Gets a {@code TodoProducerList} corresponding to the given
     *  todo producer genus {@code Type} which does not include
     *  todo producers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned {@code TodoProducer} list
     *  @throws org.osid.NullArgumentException
     *          {@code todoProducerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodoProducersByGenusType(todoProducerGenusType));
    }


    /**
     *  Gets a {@code TodoProducerList} corresponding to the given
     *  todo producer genus {@code Type} and include any additional
     *  todo producers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned {@code TodoProducer} list
     *  @throws org.osid.NullArgumentException
     *          {@code todoProducerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByParentGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodoProducersByParentGenusType(todoProducerGenusType));
    }


    /**
     *  Gets a {@code TodoProducerList} containing the given
     *  todo producer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerRecordType a todoProducer record type 
     *  @return the returned {@code TodoProducer} list
     *  @throws org.osid.NullArgumentException
     *          {@code todoProducerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByRecordType(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodoProducersByRecordType(todoProducerRecordType));
    }


    /**
     *  Gets a {@code TodoProducer} by {@code Todo.} In plenary mode,
     *  the returned list contains all known todo producers or an
     *  error results. Otherwise, the returned list may contain only
     *  those todo producers that are accessible through this session.
     *
     *  @param  todoId a todo {@code Id} 
     *  @return the returned {@code TodoProducer} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NotFoundException {@code todoId} is not found 
     *          or has no producer 
     *  @throws org.osid.NullArgumentException {@code from} or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducerByTodo(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getTodoProducerByTodo(todoId));
    }


    /**
     *  Gets all {@code TodoProducers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @return a list of {@code TodoProducers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodoProducers());
    }
}
