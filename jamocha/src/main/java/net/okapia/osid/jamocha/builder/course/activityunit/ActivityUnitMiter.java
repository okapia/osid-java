//
// ActivityUnitMiter.java
//
//     Defines an ActivityUnit miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activityunit;


/**
 *  Defines an <code>ActivityUnit</code> miter for use with the builders.
 */

public interface ActivityUnitMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            org.osid.course.ActivityUnit {


    /**
     *  Sets the course.
     *
     *  @param course a course 
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public void setCourse(org.osid.course.Course course);


    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setTotalTargetEffort(org.osid.calendaring.Duration effort);


    /**
     *  Sets this is a contact activity. 
     *
     *  @param contact <code> true </code> if this is a contact activity, <code> 
     *         false </code> if an independent activity 
     */

    public void setContact(boolean contact);


    /**
     *  Sets the total target contact time.
     *
     *  @param duration a total target contact time
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setTotalTargetContactTime(org.osid.calendaring.Duration duration);


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setTotalTargetIndividualEffort(org.osid.calendaring.Duration effort);


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setWeeklyEffort(org.osid.calendaring.Duration effort);


    /**
     *  Sets the weekly contact time.
     *
     *  @param time a weekly contact time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setWeeklyContactTime(org.osid.calendaring.Duration time);


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setWeeklyIndividualEffort(org.osid.calendaring.Duration effort);


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public void addLearningObjective(org.osid.learning.Objective objective);


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    public void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives);


    /**
     *  Adds an ActivityUnit record.
     *
     *  @param record an activityUnit record
     *  @param recordType the type of activityUnit record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addActivityUnitRecord(org.osid.course.records.ActivityUnitRecord record, org.osid.type.Type recordType);
}       


