//
// ShipmentMiter.java
//
//     Defines a Shipment miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.shipment.shipment;


/**
 *  Defines a <code>Shipment</code> miter for use with the builders.
 */

public interface ShipmentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.inventory.shipment.Shipment {


    /**
     *  Sets the source.
     *
     *  @param source a source
     *  @throws org.osid.NullArgumentException <code>source</code> is
     *          <code>null</code>
     */

    public void setSource(org.osid.resource.Resource source);


    /**
     *  Sets the order.
     *
     *  @param order an order
     *  @throws org.osid.NullArgumentException <code>order</code> is
     *          <code>null</code>
     */

    public void setOrder(org.osid.ordering.Order order);


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds an entry.
     *
     *  @param entry an entry
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public void addEntry(org.osid.inventory.shipment.Entry entry);


    /**
     *  Sets all the entries.
     *
     *  @param entries a collection of entries
     *  @throws org.osid.NullArgumentException <code>entries</code> is
     *          <code>null</code>
     */

    public void setEntries(java.util.Collection<org.osid.inventory.shipment.Entry> entries);


    /**
     *  Adds a Shipment record.
     *
     *  @param record a shipment record
     *  @param recordType the type of shipment record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addShipmentRecord(org.osid.inventory.shipment.records.ShipmentRecord record, org.osid.type.Type recordType);
}       


