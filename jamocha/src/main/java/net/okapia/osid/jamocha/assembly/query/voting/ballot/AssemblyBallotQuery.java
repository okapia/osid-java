//
// AssemblyBallotQuery.java
//
//     A BallotQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A BallotQuery that stores terms.
 */

public final class AssemblyBallotQuery
    extends net.okapia.osid.jamocha.assembly.query.voting.ballot.spi.AbstractAssemblyBallotQuery
    implements org.osid.voting.BallotQuery,
               org.osid.voting.BallotQueryInspector,
               org.osid.voting.BallotSearchOrder {


    private final AssemblyOsidTemporalQuery query;


    /** 
     *  Constructs a new <code>AssemblyBallotQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    public AssemblyBallotQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidTemporalQuery(assembler);
        return;
    }



    /**
     *  Match effective objects where the current date falls within
     *  the start and end dates inclusive.
     *
     *  @param  match <code> true </code> to match any effective, <code> false 
     *          </code> to match ineffective 
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        this.query.matchEffective(match);
        return;
    }


    /**
     *  Clears the effective query terms. 
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        this.query.clearEffectiveTerms();
        return;
    }


    /**
     *  Gets the effective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (this.query.getEffectiveTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the effective 
     *  status. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEffective(org.osid.SearchOrderStyle style) {
        this.query.orderByEffective(style);
        return;
    }


    /**
     *  Gets the column name for the effective field.
     *
     *  @return the column name
     */

    protected String getEffectiveColumn() {
        return (this.query.getEffectiveColumn());
    }    


    /**
     *  Matches temporals whose start date falls in between the given
     *  dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        this.query.matchStartDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        this.query.matchAnyStartDate(match);
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        this.query.clearStartDateTerms();
        return;
    }


    /**
     *  Gets the start date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (this.query.getStartDateTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the start date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        this.query.orderByStartDate(style);
        return;
    }


    /**
     *  Gets the column name for the start date field.
     *
     *  @return the column name
     */

    protected String getStartDateColumn() {
        return (this.query.getStartDateColumn());
    }    


    /**
     *  Matches temporals whose effective end date falls in between
     *  the given dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param match <code> true </code> if a positive match, <code>
     *          false </code> for negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code>
     *          is less than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end, boolean match) {

        this.query.matchEndDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any end date set. 
     *
     *  @param  match <code> true </code> to match any end date, <code> false 
     *          </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        this.query.matchAnyEndDate(match);
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        this.query.clearEndDateTerms();
        return;
    }


    /**
     *  Gets the end date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (this.query.getEndDateTerms());
    }


    
    /**
     *  Specifies a preference for ordering the result set by the end date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        this.query.orderByEndDate(style);
        return;
    }


    /**
     *  Gets the column name for the end date field.
     *
     *  @return the column name
     */

    protected String getEndDateColumn() {
        return (this.query.getEndDateColumn());
    }    


    /**
     *  Matches temporals where the given date range falls entirely between 
     *  the start and end dates inclusive. 
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is less 
     *          than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {

        this.query.matchDate(from, to, match);
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        this.query.clearDateTerms();
        return;
    }

    
    /**
     *  Gets the date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (this.query.getDateTerms());
    }


    /**
     *  Gets the column name for the date field.
     *
     *  @return the column name
     */

    protected String getDateColumn() {
        return (this.query.getDateColumn());
    }    


    /**
     *  Gets the query assembler.
     *
     *  @return the query assembler
     */

    public net.okapia.osid.jamocha.assembly.query.QueryAssembler getAssembler() {
        return (super.getAssembler());
    }


    protected class AssemblyOsidTemporalQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidTemporalQuery
        implements org.osid.OsidTemporalQuery,
                   org.osid.OsidTemporalQueryInspector,
                   org.osid.OsidTemporalSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidTemporalQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidTemporalQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the effective field.
         *
         *  @return the column name
         */
        
        protected String getEffectiveColumn() {
            return (super.getEffectiveColumn());
        }    


        /**
         *  Gets the column name for the start date field.
         *
         *  @return the column name
         */
        
        protected String getStartDateColumn() {
            return (super.getStartDateColumn());
        }    


        /**
         *  Gets the column name for the end date field.
         *
         *  @return the column name
         */
        
        protected String getEndDateColumn() {
            return (super.getEndDateColumn());
        }    


        /**
         *  Gets the column name for the date field.
         *
         *  @return the column name
         */
        
        protected String getDateColumn() {
            return (super.getDateColumn());
        }    
    }
}
