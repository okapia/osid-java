//
// AbstractIndexedMapStepProcessorEnablerLookupSession.java
//
//    A simple framework for providing a StepProcessorEnabler lookup service
//    backed by a fixed collection of step processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a StepProcessorEnabler lookup service backed by a
 *  fixed collection of step processor enablers. The step processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some step processor enablers may be compatible
 *  with more types than are indicated through these step processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>StepProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStepProcessorEnablerLookupSession
    extends AbstractMapStepProcessorEnablerLookupSession
    implements org.osid.workflow.rules.StepProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepProcessorEnabler> stepProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepProcessorEnabler> stepProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepProcessorEnabler>());


    /**
     *  Makes a <code>StepProcessorEnabler</code> available in this session.
     *
     *  @param  stepProcessorEnabler a step processor enabler
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putStepProcessorEnabler(org.osid.workflow.rules.StepProcessorEnabler stepProcessorEnabler) {
        super.putStepProcessorEnabler(stepProcessorEnabler);

        this.stepProcessorEnablersByGenus.put(stepProcessorEnabler.getGenusType(), stepProcessorEnabler);
        
        try (org.osid.type.TypeList types = stepProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepProcessorEnablersByRecord.put(types.getNextType(), stepProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a step processor enabler from this session.
     *
     *  @param stepProcessorEnablerId the <code>Id</code> of the step processor enabler
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeStepProcessorEnabler(org.osid.id.Id stepProcessorEnablerId) {
        org.osid.workflow.rules.StepProcessorEnabler stepProcessorEnabler;
        try {
            stepProcessorEnabler = getStepProcessorEnabler(stepProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.stepProcessorEnablersByGenus.remove(stepProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = stepProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepProcessorEnablersByRecord.remove(types.getNextType(), stepProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeStepProcessorEnabler(stepProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>StepProcessorEnablerList</code> corresponding to the given
     *  step processor enabler genus <code>Type</code> which does not include
     *  step processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known step processor enablers or an error results. Otherwise,
     *  the returned list may contain only those step processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  stepProcessorEnablerGenusType a step processor enabler genus type 
     *  @return the returned <code>StepProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerList getStepProcessorEnablersByGenusType(org.osid.type.Type stepProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepprocessorenabler.ArrayStepProcessorEnablerList(this.stepProcessorEnablersByGenus.get(stepProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>StepProcessorEnablerList</code> containing the given
     *  step processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known step processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  step processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  stepProcessorEnablerRecordType a step processor enabler record type 
     *  @return the returned <code>stepProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerList getStepProcessorEnablersByRecordType(org.osid.type.Type stepProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepprocessorenabler.ArrayStepProcessorEnablerList(this.stepProcessorEnablersByRecord.get(stepProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepProcessorEnablersByGenus.clear();
        this.stepProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
