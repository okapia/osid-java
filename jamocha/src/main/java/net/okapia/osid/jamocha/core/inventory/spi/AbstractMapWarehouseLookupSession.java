//
// AbstractMapWarehouseLookupSession
//
//    A simple framework for providing a Warehouse lookup service
//    backed by a fixed collection of warehouses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Warehouse lookup service backed by a
 *  fixed collection of warehouses. The warehouses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Warehouses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapWarehouseLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractWarehouseLookupSession
    implements org.osid.inventory.WarehouseLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inventory.Warehouse> warehouses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inventory.Warehouse>());


    /**
     *  Makes a <code>Warehouse</code> available in this session.
     *
     *  @param  warehouse a warehouse
     *  @throws org.osid.NullArgumentException <code>warehouse<code>
     *          is <code>null</code>
     */

    protected void putWarehouse(org.osid.inventory.Warehouse warehouse) {
        this.warehouses.put(warehouse.getId(), warehouse);
        return;
    }


    /**
     *  Makes an array of warehouses available in this session.
     *
     *  @param  warehouses an array of warehouses
     *  @throws org.osid.NullArgumentException <code>warehouses<code>
     *          is <code>null</code>
     */

    protected void putWarehouses(org.osid.inventory.Warehouse[] warehouses) {
        putWarehouses(java.util.Arrays.asList(warehouses));
        return;
    }


    /**
     *  Makes a collection of warehouses available in this session.
     *
     *  @param  warehouses a collection of warehouses
     *  @throws org.osid.NullArgumentException <code>warehouses<code>
     *          is <code>null</code>
     */

    protected void putWarehouses(java.util.Collection<? extends org.osid.inventory.Warehouse> warehouses) {
        for (org.osid.inventory.Warehouse warehouse : warehouses) {
            this.warehouses.put(warehouse.getId(), warehouse);
        }

        return;
    }


    /**
     *  Removes a Warehouse from this session.
     *
     *  @param  warehouseId the <code>Id</code> of the warehouse
     *  @throws org.osid.NullArgumentException <code>warehouseId<code> is
     *          <code>null</code>
     */

    protected void removeWarehouse(org.osid.id.Id warehouseId) {
        this.warehouses.remove(warehouseId);
        return;
    }


    /**
     *  Gets the <code>Warehouse</code> specified by its <code>Id</code>.
     *
     *  @param  warehouseId <code>Id</code> of the <code>Warehouse</code>
     *  @return the warehouse
     *  @throws org.osid.NotFoundException <code>warehouseId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>warehouseId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inventory.Warehouse warehouse = this.warehouses.get(warehouseId);
        if (warehouse == null) {
            throw new org.osid.NotFoundException("warehouse not found: " + warehouseId);
        }

        return (warehouse);
    }


    /**
     *  Gets all <code>Warehouses</code>. In plenary mode, the returned
     *  list contains all known warehouses or an error
     *  results. Otherwise, the returned list may contain only those
     *  warehouses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Warehouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.warehouse.ArrayWarehouseList(this.warehouses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.warehouses.clear();
        super.close();
        return;
    }
}
