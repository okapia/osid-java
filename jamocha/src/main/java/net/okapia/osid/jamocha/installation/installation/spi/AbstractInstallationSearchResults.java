//
// AbstractInstallationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractInstallationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.installation.InstallationSearchResults {

    private org.osid.installation.InstallationList installations;
    private final org.osid.installation.InstallationQueryInspector inspector;
    private final java.util.Collection<org.osid.installation.records.InstallationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractInstallationSearchResults.
     *
     *  @param installations the result set
     *  @param installationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>installations</code>
     *          or <code>installationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractInstallationSearchResults(org.osid.installation.InstallationList installations,
                                            org.osid.installation.InstallationQueryInspector installationQueryInspector) {
        nullarg(installations, "installations");
        nullarg(installationQueryInspector, "installation query inspectpr");

        this.installations = installations;
        this.inspector = installationQueryInspector;

        return;
    }


    /**
     *  Gets the installation list resulting from a search.
     *
     *  @return an installation list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallations() {
        if (this.installations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.installation.InstallationList installations = this.installations;
        this.installations = null;
	return (installations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.installation.InstallationQueryInspector getInstallationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  installation search record <code> Type. </code> This method must
     *  be used to retrieve an installation implementing the requested
     *  record.
     *
     *  @param installationSearchRecordType an installation search 
     *         record type 
     *  @return the installation search
     *  @throws org.osid.NullArgumentException
     *          <code>installationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(installationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationSearchResultsRecord getInstallationSearchResultsRecord(org.osid.type.Type installationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.installation.records.InstallationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(installationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(installationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record installation search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addInstallationRecord(org.osid.installation.records.InstallationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "installation record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
