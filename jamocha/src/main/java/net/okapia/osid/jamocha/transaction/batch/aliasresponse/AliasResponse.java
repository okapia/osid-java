//
// AliasResponse.java
//
//     Defines an AliasResponse.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transaction.batch.aliasresponse;


/**
 *  Defines an <code>AliasResponse</code>.
 */

public final class AliasResponse
    extends net.okapia.osid.jamocha.transaction.batch.aliasresponse.spi.AbstractAliasResponse
    implements org.osid.transaction.batch.AliasResponse {


    /**
     *  Constructs a new successful <code>AliasResponse</code>.
     *
     *  @param referenceId the reference Id
     *  @param aliasId the alias Id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> or <code>aliasId</code> is
     *          <code>null</code>
     */

    public AliasResponse(org.osid.id.Id referenceId, org.osid.id.Id aliasId) {
        setReferenceId(referenceId);
        setAliasId(aliasId);
        return;
    }


    /**
     *  Constructs a new unsuccessful <code>AliasResponse</code>.
     *
     *  @param referenceId the reference Id
     *  @param aliasId the alias Id
     *  @param message the error message
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code>, <code>aliasId</code>, or
     *          <code>message</code> is <code>null</code>
     */

    public AliasResponse(org.osid.id.Id referenceId, org.osid.id.Id aliasId, 
                         org.osid.locale.DisplayText message) {

        this(referenceId, aliasId);
        setErrorMessage(message);
        return;
    }
}
