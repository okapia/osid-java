//
// InvariantMapProxyRealmLookupSession
//
//    Implements a Realm lookup service backed by a fixed
//    collection of realms. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements a Realm lookup service backed by a fixed
 *  collection of realms. The realms are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyRealmLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapRealmLookupSession
    implements org.osid.personnel.RealmLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRealmLookupSession} with no
     *  realms.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyRealmLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRealmLookupSession} with a
     *  single realm.
     *
     *  @param realm a single realm
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRealmLookupSession(org.osid.personnel.Realm realm, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRealm(realm);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRealmLookupSession} using
     *  an array of realms.
     *
     *  @param realms an array of realms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realms} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRealmLookupSession(org.osid.personnel.Realm[] realms, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRealms(realms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRealmLookupSession} using a
     *  collection of realms.
     *
     *  @param realms a collection of realms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realms} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRealmLookupSession(java.util.Collection<? extends org.osid.personnel.Realm> realms,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRealms(realms);
        return;
    }
}
