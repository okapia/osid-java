//
// AbstractGraphNodeList
//
//     Implements a filter for a GraphNodeList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.topology.graphnode.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a GraphNodeList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedGraphNodeList
 *  to improve performance.
 */

public abstract class AbstractGraphNodeFilterList
    extends net.okapia.osid.jamocha.topology.graphnode.spi.AbstractGraphNodeList
    implements org.osid.topology.GraphNodeList,
               net.okapia.osid.jamocha.inline.filter.topology.graphnode.GraphNodeFilter {

    private org.osid.topology.GraphNode graphNode;
    private final org.osid.topology.GraphNodeList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractGraphNodeFilterList</code>.
     *
     *  @param graphNodeList a <code>GraphNodeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>graphNodeList</code> is <code>null</code>
     */

    protected AbstractGraphNodeFilterList(org.osid.topology.GraphNodeList graphNodeList) {
        nullarg(graphNodeList, "graph node list");
        this.list = graphNodeList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.graphNode == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> GraphNode </code> in this list. 
     *
     *  @return the next <code> GraphNode </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> GraphNode </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.GraphNode getNextGraphNode()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.topology.GraphNode graphNode = this.graphNode;
            this.graphNode = null;
            return (graphNode);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in graph node list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.graphNode = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters GraphNodes.
     *
     *  @param graphNode the graph node to filter
     *  @return <code>true</code> if the graph node passes the filter,
     *          <code>false</code> if the graph node should be filtered
     */

    public abstract boolean pass(org.osid.topology.GraphNode graphNode);


    protected void prime() {
        if (this.graphNode != null) {
            return;
        }

        org.osid.topology.GraphNode graphNode = null;

        while (this.list.hasNext()) {
            try {
                graphNode = this.list.getNextGraphNode();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(graphNode)) {
                this.graphNode = graphNode;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
