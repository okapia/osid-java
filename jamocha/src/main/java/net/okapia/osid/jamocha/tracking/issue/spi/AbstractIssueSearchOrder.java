//
// AbstractIssueSearchOdrer.java
//
//     Defines an IssueSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.issue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code IssueSearchOrder}.
 */

public abstract class AbstractIssueSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.tracking.IssueSearchOrder {

    private final java.util.Collection<org.osid.tracking.records.IssueSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by queue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQueue(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a queue search order is available. 
     *
     *  @return <code> true </code> if a queue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the queue search order. 
     *
     *  @return the queue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsQueueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchOrder getQueueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQueueSearchOrder() is false");
    }


    /**
     *  Orders the results by the customer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Orders the results by the topic. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTopic(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a subject search order is available. 
     *
     *  @return <code> true </code> if a subject search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopicSearchOrder() {
        return (false);
    }


    /**
     *  Gets the topic search order. 
     *
     *  @return the subject search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsTopicSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchOrder getTopicSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTopicSearchOrder() is false");
    }


    /**
     *  Orders the results by the master issue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMasterIssue(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an issue search order is available. 
     *
     *  @return <code> true </code> if an issue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMasterIssueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the issue search order. 
     *
     *  @return the issue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsMasterIssueSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchOrder getMasterIssueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsMasterIssueSearchOrder() is false");
    }


    /**
     *  Orders the results by the root issue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRootIssue(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an issue search order is available. 
     *
     *  @return <code> true </code> if an issue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRootIssueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the issue search order. 
     *
     *  @return the issue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRootIssueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchOrder getRootIssueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRootIssueSearchOrder() is false");
    }


    /**
     *  Orders the results by the creator. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreator(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCreatorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCreatorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreatorSearchOrder() is false");
    }


    /**
     *  Orders the results by the creator. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreatingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCreatingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getCreatingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreatingAgentSearchOrder() is false");
    }


    /**
     *  Orders the results by the created date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreatedDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the reopener. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReopener(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopenerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopenerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getReopenerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReopenerSearchOrder() is false");
    }


    /**
     *  Orders the results by the reopener. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReopeningAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopeningAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopeningAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getReopeningAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReopeningAgentSearchOrder() is false");
    }


    /**
     *  Orders the results by the reopened date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReopenedDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the due date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDueDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the pending state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPending(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the resolver. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolver(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolverSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopenerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResolverSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResolverSearchOrder() is false");
    }


    /**
     *  Orders the results by the resolving agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolvingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolvingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopeningAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getResolvingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResolvingAgentSearchOrder() is false");
    }


    /**
     *  Orders the results by the resolved date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolvedDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the resolution type. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolutionType(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCloser(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCloserSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCloserSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCloserSearchOrder() is false");
    }


    /**
     *  Orders the results by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsClosingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getClosingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsClosingAgentSearchOrder() is false");
    }


    /**
     *  Orders the results by the closed date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosedDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the assigned resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssignedResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssignedResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAssignedResourceSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getAssignedResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssignedResourceSearchOrder() is false");
    }


    /**
     *  Orders the results by the issue status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStatus(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  issueRecordType an issue record type 
     *  @return {@code true} if the issueRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type issueRecordType) {
        for (org.osid.tracking.records.IssueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  issueRecordType the issue record type 
     *  @return the issue search order record
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(issueRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.tracking.records.IssueSearchOrderRecord getIssueSearchOrderRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.IssueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this issue. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param issueRecord the issue search odrer record
     *  @param issueRecordType issue record type
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecord} or
     *          {@code issueRecordTypeissue} is
     *          {@code null}
     */
            
    protected void addIssueRecord(org.osid.tracking.records.IssueSearchOrderRecord issueSearchOrderRecord, 
                                     org.osid.type.Type issueRecordType) {

        addRecordType(issueRecordType);
        this.records.add(issueSearchOrderRecord);
        
        return;
    }
}
