//
// AbstractAssemblyCookbookQuery.java
//
//     A CookbookQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recipe.cookbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CookbookQuery that stores terms.
 */

public abstract class AbstractAssemblyCookbookQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.recipe.CookbookQuery,
               org.osid.recipe.CookbookQueryInspector,
               org.osid.recipe.CookbookSearchOrder {

    private final java.util.Collection<org.osid.recipe.records.CookbookQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.CookbookQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.CookbookSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCookbookQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCookbookQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the recipe <code> Id </code> for this query. 
     *
     *  @param  recipeId a recipe <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recipeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipeId(org.osid.id.Id recipeId, boolean match) {
        getAssembler().addIdTerm(getRecipeIdColumn(), recipeId, match);
        return;
    }


    /**
     *  Clears the recipe <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipeIdTerms() {
        getAssembler().clearTerms(getRecipeIdColumn());
        return;
    }


    /**
     *  Gets the recipe <code> Id </code> terms. 
     *
     *  @return the recipe <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipeIdTerms() {
        return (getAssembler().getIdTerms(getRecipeIdColumn()));
    }


    /**
     *  Gets the RecipeId column name.
     *
     * @return the column name
     */

    protected String getRecipeIdColumn() {
        return ("recipe_id");
    }


    /**
     *  Tests if a recipe query is available. 
     *
     *  @return <code> true </code> if a recipe query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipe. 
     *
     *  @return the recipe query 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuery getRecipeQuery() {
        throw new org.osid.UnimplementedException("supportsRecipeQuery() is false");
    }


    /**
     *  Matches cook books with any recipe. 
     *
     *  @param  match <code> true </code> to match cook books with any recipe, 
     *          <code> false </code> to match cook books with no recipes 
     */

    @OSID @Override
    public void matchAnyRecipe(boolean match) {
        getAssembler().addIdWildcardTerm(getRecipeColumn(), match);
        return;
    }


    /**
     *  Clears the procedure terms. 
     */

    @OSID @Override
    public void clearRecipeTerms() {
        getAssembler().clearTerms(getRecipeColumn());
        return;
    }


    /**
     *  Gets the recipe terms. 
     *
     *  @return the recipe terms 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQueryInspector[] getRecipeTerms() {
        return (new org.osid.recipe.RecipeQueryInspector[0]);
    }


    /**
     *  Gets the Recipe column name.
     *
     * @return the column name
     */

    protected String getRecipeColumn() {
        return ("recipe");
    }


    /**
     *  Sets the procedure <code> Id </code> for this query to match 
     *  procedures assigned to cook books. 
     *
     *  @param  procedureId a procedure <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> procedureId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcedureId(org.osid.id.Id procedureId, boolean match) {
        getAssembler().addIdTerm(getProcedureIdColumn(), procedureId, match);
        return;
    }


    /**
     *  Clears the procedure <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProcedureIdTerms() {
        getAssembler().clearTerms(getProcedureIdColumn());
        return;
    }


    /**
     *  Gets the procedure <code> Id </code> terms. 
     *
     *  @return the procedure <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcedureIdTerms() {
        return (getAssembler().getIdTerms(getProcedureIdColumn()));
    }


    /**
     *  Gets the ProcedureId column name.
     *
     * @return the column name
     */

    protected String getProcedureIdColumn() {
        return ("procedure_id");
    }


    /**
     *  Tests if a procedure query is available. 
     *
     *  @return <code> true </code> if a procedure query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureQuery() {
        return (false);
    }


    /**
     *  Gets the query for an procedure. 
     *
     *  @return the procedure query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuery getProcedureQuery() {
        throw new org.osid.UnimplementedException("supportsProcedureQuery() is false");
    }


    /**
     *  Matches cook books with any procedure. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          procedure, <code> false </code> to match cook books with no 
     *          procedures 
     */

    @OSID @Override
    public void matchAnyProcedure(boolean match) {
        getAssembler().addIdWildcardTerm(getProcedureColumn(), match);
        return;
    }


    /**
     *  Clears the procedure terms. 
     */

    @OSID @Override
    public void clearProcedureTerms() {
        getAssembler().clearTerms(getProcedureColumn());
        return;
    }


    /**
     *  Gets the procedure terms. 
     *
     *  @return the procedure terms 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQueryInspector[] getProcedureTerms() {
        return (new org.osid.recipe.ProcedureQueryInspector[0]);
    }


    /**
     *  Gets the Procedure column name.
     *
     * @return the column name
     */

    protected String getProcedureColumn() {
        return ("procedure");
    }


    /**
     *  Sets a direction <code> Id. </code> 
     *
     *  @param  directionId a direction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> directionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDirectionId(org.osid.id.Id directionId, boolean match) {
        getAssembler().addIdTerm(getDirectionIdColumn(), directionId, match);
        return;
    }


    /**
     *  Clears the direction <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDirectionIdTerms() {
        getAssembler().clearTerms(getDirectionIdColumn());
        return;
    }


    /**
     *  Gets the direction <code> Id </code> terms. 
     *
     *  @return the direction <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDirectionIdTerms() {
        return (getAssembler().getIdTerms(getDirectionIdColumn()));
    }


    /**
     *  Gets the DirectionId column name.
     *
     * @return the column name
     */

    protected String getDirectionIdColumn() {
        return ("direction_id");
    }


    /**
     *  Tests if a <code> DirectionQuery </code> is available. 
     *
     *  @return <code> true </code> if a direction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a direction query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the direction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuery getDirectionQuery() {
        throw new org.osid.UnimplementedException("supportsDirectionQuery() is false");
    }


    /**
     *  Matches any direction. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          direction, <code> false </code> to match cook books with no 
     *          directions 
     */

    @OSID @Override
    public void matchAnyDirection(boolean match) {
        getAssembler().addIdWildcardTerm(getDirectionColumn(), match);
        return;
    }


    /**
     *  Clears the direction terms. 
     */

    @OSID @Override
    public void clearDirectionTerms() {
        getAssembler().clearTerms(getDirectionColumn());
        return;
    }


    /**
     *  Gets the direction terms. 
     *
     *  @return the direction terms 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQueryInspector[] getDirectionTerms() {
        return (new org.osid.recipe.DirectionQueryInspector[0]);
    }


    /**
     *  Gets the Direction column name.
     *
     * @return the column name
     */

    protected String getDirectionColumn() {
        return ("direction");
    }


    /**
     *  Sets the cook book <code> Id </code> for this query to match cook 
     *  books that have the specified cook book as an ancestor. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCookbookId(org.osid.id.Id cookbookId, 
                                        boolean match) {
        getAssembler().addIdTerm(getAncestorCookbookIdColumn(), cookbookId, match);
        return;
    }


    /**
     *  Clears the ancestor cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCookbookIdTerms() {
        getAssembler().clearTerms(getAncestorCookbookIdColumn());
        return;
    }


    /**
     *  Gets the ancestor cook book <code> Id </code> terms. 
     *
     *  @return the ancestor cook book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCookbookIdTerms() {
        return (getAssembler().getIdTerms(getAncestorCookbookIdColumn()));
    }


    /**
     *  Gets the AncestorCookbookId column name.
     *
     * @return the column name
     */

    protected String getAncestorCookbookIdColumn() {
        return ("ancestor_cookbook_id");
    }


    /**
     *  Tests if an <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCookbookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getAncestorCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCookbookQuery() is false");
    }


    /**
     *  Matches cook books with any ancestor. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          ancestor, <code> false </code> to match root cook books 
     */

    @OSID @Override
    public void matchAnyAncestorCookbook(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorCookbookColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor cook book terms. 
     */

    @OSID @Override
    public void clearAncestorCookbookTerms() {
        getAssembler().clearTerms(getAncestorCookbookColumn());
        return;
    }


    /**
     *  Gets the ancestor cook book terms. 
     *
     *  @return the ancestor cook book terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getAncestorCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }


    /**
     *  Gets the AncestorCookbook column name.
     *
     * @return the column name
     */

    protected String getAncestorCookbookColumn() {
        return ("ancestor_cookbook");
    }


    /**
     *  Sets the cook book <code> Id </code> for this query to match cook 
     *  books that have the specified cook book as a descendant. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCookbookId(org.osid.id.Id cookbookId, 
                                          boolean match) {
        getAssembler().addIdTerm(getDescendantCookbookIdColumn(), cookbookId, match);
        return;
    }


    /**
     *  Clears the descendant cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCookbookIdTerms() {
        getAssembler().clearTerms(getDescendantCookbookIdColumn());
        return;
    }


    /**
     *  Gets the descendant cook book <code> Id </code> terms. 
     *
     *  @return the descendant cook book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCookbookIdTerms() {
        return (getAssembler().getIdTerms(getDescendantCookbookIdColumn()));
    }


    /**
     *  Gets the DescendantCookbookId column name.
     *
     * @return the column name
     */

    protected String getDescendantCookbookIdColumn() {
        return ("descendant_cookbook_id");
    }


    /**
     *  Tests if an <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCookbookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getDescendantCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCookbookQuery() is false");
    }


    /**
     *  Matches cook books with any descendant. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          descendant, <code> false </code> to match leaf cook books 
     */

    @OSID @Override
    public void matchAnyDescendantCookbook(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantCookbookColumn(), match);
        return;
    }


    /**
     *  Clears the descendant cook book terms. 
     */

    @OSID @Override
    public void clearDescendantCookbookTerms() {
        getAssembler().clearTerms(getDescendantCookbookColumn());
        return;
    }


    /**
     *  Gets the descendant cook book terms. 
     *
     *  @return the descendant cook book terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getDescendantCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }


    /**
     *  Gets the DescendantCookbook column name.
     *
     * @return the column name
     */

    protected String getDescendantCookbookColumn() {
        return ("descendant_cookbook");
    }


    /**
     *  Tests if this cookbook supports the given record
     *  <code>Type</code>.
     *
     *  @param  cookbookRecordType a cookbook record type 
     *  @return <code>true</code> if the cookbookRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type cookbookRecordType) {
        for (org.osid.recipe.records.CookbookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(cookbookRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  cookbookRecordType the cookbook record type 
     *  @return the cookbook query record 
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cookbookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.CookbookQueryRecord getCookbookQueryRecord(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.CookbookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(cookbookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cookbookRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  cookbookRecordType the cookbook record type 
     *  @return the cookbook query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cookbookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.CookbookQueryInspectorRecord getCookbookQueryInspectorRecord(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.CookbookQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(cookbookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cookbookRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param cookbookRecordType the cookbook record type
     *  @return the cookbook search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cookbookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.CookbookSearchOrderRecord getCookbookSearchOrderRecord(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.CookbookSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(cookbookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cookbookRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this cookbook. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param cookbookQueryRecord the cookbook query record
     *  @param cookbookQueryInspectorRecord the cookbook query inspector
     *         record
     *  @param cookbookSearchOrderRecord the cookbook search order record
     *  @param cookbookRecordType cookbook record type
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookQueryRecord</code>,
     *          <code>cookbookQueryInspectorRecord</code>,
     *          <code>cookbookSearchOrderRecord</code> or
     *          <code>cookbookRecordTypecookbook</code> is
     *          <code>null</code>
     */
            
    protected void addCookbookRecords(org.osid.recipe.records.CookbookQueryRecord cookbookQueryRecord, 
                                      org.osid.recipe.records.CookbookQueryInspectorRecord cookbookQueryInspectorRecord, 
                                      org.osid.recipe.records.CookbookSearchOrderRecord cookbookSearchOrderRecord, 
                                      org.osid.type.Type cookbookRecordType) {

        addRecordType(cookbookRecordType);

        nullarg(cookbookQueryRecord, "cookbook query record");
        nullarg(cookbookQueryInspectorRecord, "cookbook query inspector record");
        nullarg(cookbookSearchOrderRecord, "cookbook search odrer record");

        this.queryRecords.add(cookbookQueryRecord);
        this.queryInspectorRecords.add(cookbookQueryInspectorRecord);
        this.searchOrderRecords.add(cookbookSearchOrderRecord);
        
        return;
    }
}
