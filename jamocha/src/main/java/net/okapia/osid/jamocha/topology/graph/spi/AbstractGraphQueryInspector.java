//
// AbstractGraphQueryInspector.java
//
//     A template for making a GraphQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.graph.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for graphs.
 */

public abstract class AbstractGraphQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.topology.GraphQueryInspector {

    private final java.util.Collection<org.osid.topology.records.GraphQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the node <code> Id </code> terms. 
     *
     *  @return the node <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNodeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the node terms. 
     *
     *  @return the node terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the edge <code> Id </code> terms. 
     *
     *  @return the edge <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEdgeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the edge terms. 
     *
     *  @return the edge terms 
     */

    @OSID @Override
    public org.osid.topology.EdgeQueryInspector[] getEdgeTerms() {
        return (new org.osid.topology.EdgeQueryInspector[0]);
    }


    /**
     *  Gets the ancestor graph <code> Id </code> terms. 
     *
     *  @return the ancestor graph <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorGraphIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor graph terms. 
     *
     *  @return the ancestor graph terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getAncestorGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }


    /**
     *  Gets the descendant graph <code> Id </code> terms. 
     *
     *  @return the descendant graph <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantGraphIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant graph terms. 
     *
     *  @return the descendant graph terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getDescendantGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given graph query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a graph implementing the requested record.
     *
     *  @param graphRecordType a graph record type
     *  @return the graph query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(graphRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.GraphQueryInspectorRecord getGraphQueryInspectorRecord(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.GraphQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(graphRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(graphRecordType + " is not supported");
    }


    /**
     *  Adds a record to this graph query. 
     *
     *  @param graphQueryInspectorRecord graph query inspector
     *         record
     *  @param graphRecordType graph record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGraphQueryInspectorRecord(org.osid.topology.records.GraphQueryInspectorRecord graphQueryInspectorRecord, 
                                                   org.osid.type.Type graphRecordType) {

        addRecordType(graphRecordType);
        nullarg(graphRecordType, "graph record type");
        this.records.add(graphQueryInspectorRecord);        
        return;
    }
}
