//
// AbstractMapAuctionProcessorEnablerLookupSession
//
//    A simple framework for providing an AuctionProcessorEnabler lookup service
//    backed by a fixed collection of auction processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AuctionProcessorEnabler lookup service backed by a
 *  fixed collection of auction processor enablers. The auction processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuctionProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.bidding.rules.spi.AbstractAuctionProcessorEnablerLookupSession
    implements org.osid.bidding.rules.AuctionProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.bidding.rules.AuctionProcessorEnabler> auctionProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.bidding.rules.AuctionProcessorEnabler>());


    /**
     *  Makes an <code>AuctionProcessorEnabler</code> available in this session.
     *
     *  @param  auctionProcessorEnabler an auction processor enabler
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putAuctionProcessorEnabler(org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler) {
        this.auctionProcessorEnablers.put(auctionProcessorEnabler.getId(), auctionProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of auction processor enablers available in this session.
     *
     *  @param  auctionProcessorEnablers an array of auction processor enablers
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuctionProcessorEnablers(org.osid.bidding.rules.AuctionProcessorEnabler[] auctionProcessorEnablers) {
        putAuctionProcessorEnablers(java.util.Arrays.asList(auctionProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of auction processor enablers available in this session.
     *
     *  @param  auctionProcessorEnablers a collection of auction processor enablers
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuctionProcessorEnablers(java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessorEnabler> auctionProcessorEnablers) {
        for (org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler : auctionProcessorEnablers) {
            this.auctionProcessorEnablers.put(auctionProcessorEnabler.getId(), auctionProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes an AuctionProcessorEnabler from this session.
     *
     *  @param  auctionProcessorEnablerId the <code>Id</code> of the auction processor enabler
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId) {
        this.auctionProcessorEnablers.remove(auctionProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>AuctionProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  auctionProcessorEnablerId <code>Id</code> of the <code>AuctionProcessorEnabler</code>
     *  @return the auctionProcessorEnabler
     *  @throws org.osid.NotFoundException <code>auctionProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnabler getAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler = this.auctionProcessorEnablers.get(auctionProcessorEnablerId);
        if (auctionProcessorEnabler == null) {
            throw new org.osid.NotFoundException("auctionProcessorEnabler not found: " + auctionProcessorEnablerId);
        }

        return (auctionProcessorEnabler);
    }


    /**
     *  Gets all <code>AuctionProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known auctionProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  auctionProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AuctionProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.ArrayAuctionProcessorEnablerList(this.auctionProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionProcessorEnablers.clear();
        super.close();
        return;
    }
}
