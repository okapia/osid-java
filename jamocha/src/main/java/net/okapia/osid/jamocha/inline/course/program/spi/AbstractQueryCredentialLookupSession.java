//
// AbstractQueryCredentialLookupSession.java
//
//    An inline adapter that maps a CredentialLookupSession to
//    a CredentialQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CredentialLookupSession to
 *  a CredentialQuerySession.
 */

public abstract class AbstractQueryCredentialLookupSession
    extends net.okapia.osid.jamocha.course.program.spi.AbstractCredentialLookupSession
    implements org.osid.course.program.CredentialLookupSession {

    private final org.osid.course.program.CredentialQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCredentialLookupSession.
     *
     *  @param querySession the underlying credential query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCredentialLookupSession(org.osid.course.program.CredentialQuerySession querySession) {
        nullarg(querySession, "credential query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Credential</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCredentials() {
        return (this.session.canSearchCredentials());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credentials in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the <code>Credential</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Credential</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Credential</code> and
     *  retained for compatibility.
     *
     *  @param  credentialId <code>Id</code> of the
     *          <code>Credential</code>
     *  @return the credential
     *  @throws org.osid.NotFoundException <code>credentialId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>credentialId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Credential getCredential(org.osid.id.Id credentialId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.CredentialQuery query = getQuery();
        query.matchId(credentialId, true);
        org.osid.course.program.CredentialList credentials = this.session.getCredentialsByQuery(query);
        if (credentials.hasNext()) {
            return (credentials.getNextCredential());
        } 
        
        throw new org.osid.NotFoundException(credentialId + " not found");
    }


    /**
     *  Gets a <code>CredentialList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  credentials specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Credentials</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  credentialIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Credential</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>credentialIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByIds(org.osid.id.IdList credentialIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.CredentialQuery query = getQuery();

        try (org.osid.id.IdList ids = credentialIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCredentialsByQuery(query));
    }


    /**
     *  Gets a <code>CredentialList</code> corresponding to the given
     *  credential genus <code>Type</code> which does not include
     *  credentials of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  credentialGenusType a credential genus type 
     *  @return the returned <code>Credential</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByGenusType(org.osid.type.Type credentialGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.CredentialQuery query = getQuery();
        query.matchGenusType(credentialGenusType, true);
        return (this.session.getCredentialsByQuery(query));
    }


    /**
     *  Gets a <code>CredentialList</code> corresponding to the given
     *  credential genus <code>Type</code> and include any additional
     *  credentials with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  credentialGenusType a credential genus type 
     *  @return the returned <code>Credential</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByParentGenusType(org.osid.type.Type credentialGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.CredentialQuery query = getQuery();
        query.matchParentGenusType(credentialGenusType, true);
        return (this.session.getCredentialsByQuery(query));
    }


    /**
     *  Gets a <code>CredentialList</code> containing the given
     *  credential record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  credentialRecordType a credential record type 
     *  @return the returned <code>Credential</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByRecordType(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.CredentialQuery query = getQuery();
        query.matchRecordType(credentialRecordType, true);
        return (this.session.getCredentialsByQuery(query));
    }

    
    /**
     *  Gets all <code>Credentials</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Credentials</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.CredentialQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCredentialsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.program.CredentialQuery getQuery() {
        org.osid.course.program.CredentialQuery query = this.session.getCredentialQuery();
        
        return (query);
    }
}
