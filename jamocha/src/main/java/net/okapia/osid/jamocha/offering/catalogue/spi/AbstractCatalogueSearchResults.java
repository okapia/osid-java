//
// AbstractCatalogueSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.catalogue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCatalogueSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.CatalogueSearchResults {

    private org.osid.offering.CatalogueList catalogues;
    private final org.osid.offering.CatalogueQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.records.CatalogueSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCatalogueSearchResults.
     *
     *  @param catalogues the result set
     *  @param catalogueQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>catalogues</code>
     *          or <code>catalogueQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCatalogueSearchResults(org.osid.offering.CatalogueList catalogues,
                                            org.osid.offering.CatalogueQueryInspector catalogueQueryInspector) {
        nullarg(catalogues, "catalogues");
        nullarg(catalogueQueryInspector, "catalogue query inspectpr");

        this.catalogues = catalogues;
        this.inspector = catalogueQueryInspector;

        return;
    }


    /**
     *  Gets the catalogue list resulting from a search.
     *
     *  @return a catalogue list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCatalogues() {
        if (this.catalogues == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.CatalogueList catalogues = this.catalogues;
        this.catalogues = null;
	return (catalogues);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.CatalogueQueryInspector getCatalogueQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  catalogue search record <code> Type. </code> This method must
     *  be used to retrieve a catalogue implementing the requested
     *  record.
     *
     *  @param catalogueSearchRecordType a catalogue search 
     *         record type 
     *  @return the catalogue search
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(catalogueSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CatalogueSearchResultsRecord getCatalogueSearchResultsRecord(org.osid.type.Type catalogueSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.records.CatalogueSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(catalogueSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(catalogueSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record catalogue search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCatalogueRecord(org.osid.offering.records.CatalogueSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "catalogue record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
