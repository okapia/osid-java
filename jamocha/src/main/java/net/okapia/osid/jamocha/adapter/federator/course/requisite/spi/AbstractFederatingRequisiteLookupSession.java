//
// AbstractFederatingRequisiteLookupSession.java
//
//     An abstract federating adapter for a RequisiteLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RequisiteLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRequisiteLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.requisite.RequisiteLookupSession>
    implements org.osid.course.requisite.RequisiteLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingRequisiteLookupSession</code>.
     */

    protected AbstractFederatingRequisiteLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.requisite.RequisiteLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Requisite</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRequisites() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            if (session.canLookupRequisites()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Requisite</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequisiteView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.useComparativeRequisiteView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Requisite</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequisiteView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.usePlenaryRequisiteView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include requisites in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only active requisites are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRequisiteView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.useActiveRequisiteView();
        }

        return;
    }


    /**
     *  Active and inactive requisites are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRequisiteView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.useAnyStatusRequisiteView();
        }

        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  requisites.
     */

    @OSID @Override
    public void useSequesteredRequisiteView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.useSequesteredRequisiteView();
        }

        return;
    }


    /**
     *  All requisites are returned including sequestered requisites.
     */

    @OSID @Override
    public void useUnsequesteredRequisiteView() {
        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            session.useUnsequesteredRequisiteView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Requisite</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Requisite</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Requisite</code> and
     *  retained for compatibility.
     *
     *  In effective mode, requisites are returned that are currently
     *  effective.  In any effective mode, effective requisites and
     *  those currently expired are returned.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteId <code>Id</code> of the
     *          <code>Requisite</code>
     *  @return the requisite
     *  @throws org.osid.NotFoundException <code>requisiteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>requisiteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite getRequisite(org.osid.id.Id requisiteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            try {
                return (session.getRequisite(requisiteId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(requisiteId + " not found");
    }


    /**
     *  Gets a <code>RequisiteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  requisites specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Requisites</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, requisites are returned that are currently effective.
     *  In any effective mode, effective requisites and those currently expired
     *  are returned.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByIds(org.osid.id.IdList requisiteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.requisite.requisite.MutableRequisiteList ret = new net.okapia.osid.jamocha.course.requisite.requisite.MutableRequisiteList();

        try (org.osid.id.IdList ids = requisiteIds) {
            while (ids.hasNext()) {
                ret.addRequisite(getRequisite(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RequisiteList</code> corresponding to the given
     *  requisite genus <code>Type</code> which does not include
     *  requisites of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requisites are returned that are currently effective.
     *  In any effective mode, effective requisites and those currently expired
     *  are returned.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteGenusType a requisite genus type 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByGenusType(org.osid.type.Type requisiteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesByGenusType(requisiteGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequisiteList</code> corresponding to the given
     *  requisite genus <code>Type</code> and include any additional
     *  requisites with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requisites are returned that are currently
     *  effective.  In any effective mode, effective requisites and
     *  those currently expired are returned.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteGenusType a requisite genus type 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByParentGenusType(org.osid.type.Type requisiteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesByParentGenusType(requisiteGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequisiteList</code> containing the given
     *  requisite record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requisites are returned that are currently
     *  effective.  In any effective mode, effective requisites and
     *  those currently expired are returned.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteRecordType a requisite record type 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByRecordType(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesByRecordType(requisiteRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequisiteList</code> immediately containing the
     *  given requisite option.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites are
     *  returned.
     *
     *  In sequestered mode, no sequestered requisites are
     *  returned. In unsequestered mode, all requisites are returned.
     *
     *  @param  requisiteOptionId a requisite option <code>Id</code>
     *  @return the returned <code>RequisiteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteOptionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForRequisiteOption(org.osid.id.Id requisiteOptionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesForRequisiteOption(requisiteOptionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Requisites</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requisites are returned that are currently
     *  effective.  In any effective mode, effective requisites and
     *  those currently expired are returned.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisites());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList getRequisiteList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.ParallelRequisiteList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.CompositeRequisiteList());
        }
    }


    /**
     *  Gets the <code>CourseRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CourseRequirement</code> and retained for compatibility.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementId <code>Id</code> of the
     *          <code>CourseRequirement</code>
     *  @return the course requirement
     *  @throws org.osid.NotFoundException <code>courseRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirement getCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            try {
                return (session.getCourseRequirement(courseRequirementId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(courseRequirementId + " not found");
    }

    
    /**
     *  Gets a <code>CourseRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the course
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>CourseRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByIds(org.osid.id.IdList courseRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.requisite.courserequirement.MutableCourseRequirementList ret = new net.okapia.osid.jamocha.course.requisite.courserequirement.MutableCourseRequirementList();

        try (org.osid.id.IdList ids = courseRequirementIds) {
            while (ids.hasNext()) {
                ret.addCourseRequirement(getCourseRequirement(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }

    
    /**
     *  Gets a <code>CourseRequirementList</code> corresponding to the
     *  given course requirement genus <code>Type</code> which does
     *  not include course requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementGenusType a course requirement genus type
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByGenusType(org.osid.type.Type courseRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.FederatingCourseRequirementList ret = getCourseRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCourseRequirementList(session.getCourseRequirementsByGenusType(courseRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>CourseRequirementList</code> corresponding to the
     *  given course requirement genus <code>Type</code> and include
     *  any additional course requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementGenusType a course requirement genus type
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByParentGenusType(org.osid.type.Type courseRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.FederatingCourseRequirementList ret = getCourseRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCourseRequirementList(session.getCourseRequirementsByParentGenusType(courseRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseRequirementList</code> containing the given
     *  course requirement record <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementRecordType a course requirement record type
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByRecordType(org.osid.type.Type courseRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.FederatingCourseRequirementList ret = getCourseRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCourseRequirementList(session.getCourseRequirementsByRecordType(courseRequirementRecordType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseRequirementList</code> containing the given
     *  course.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseId a course <code>Id</code>
     *  @return the returned <code>CourseRequirementList</code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.FederatingCourseRequirementList ret = getCourseRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCourseRequirementList(session.getCourseRequirementsByCourse(courseId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseRequirementList</code> with the given
     *  <code>Requisite</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code>
     *  @return the returned <code>CourseRequirementList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.FederatingCourseRequirementList ret = getCourseRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCourseRequirementList(session.getCourseRequirementsByAltRequisite(requisiteId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     * Gets a <code>RequisiteList</code> immediately containing the
     *  given course requirement.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *
     *  In active mode, course requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  courseRequirementId a course requirement <code>Id</code>
     *  @return a list of <code>Requisites</code>
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesForCourseRequirement(courseRequirementId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CourseRequirements</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @return a list of <code>CourseRequirements</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.FederatingCourseRequirementList ret = getCourseRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCourseRequirementList(session.getCourseRequirements());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.FederatingCourseRequirementList getCourseRequirementList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.ParallelCourseRequirementList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.courserequirement.CompositeCourseRequirementList());
        }
    }


    /**
     *  Gets the <code>ProgramRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ProgramRequirement</code> and retained for compatibility.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementId <code>Id</code> of the
     *          <code>ProgramRequirement</code>
     *  @return the program requirement
     *  @throws org.osid.NotFoundException <code>programRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirement getProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            try {
                return (session.getProgramRequirement(programRequirementId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(programRequirementId + " not found");
    }

    
    /**
     *  Gets a <code>ProgramRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the program
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>ProgramRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByIds(org.osid.id.IdList programRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.requisite.programrequirement.MutableProgramRequirementList ret = new net.okapia.osid.jamocha.course.requisite.programrequirement.MutableProgramRequirementList();

        try (org.osid.id.IdList ids = programRequirementIds) {
            while (ids.hasNext()) {
                ret.addProgramRequirement(getProgramRequirement(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }

    
    /**
     *  Gets a <code>ProgramRequirementList</code> corresponding to the
     *  given program requirement genus <code>Type</code> which does
     *  not include program requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementGenusType a program requirement genus type
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByGenusType(org.osid.type.Type programRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.FederatingProgramRequirementList ret = getProgramRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addProgramRequirementList(session.getProgramRequirementsByGenusType(programRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>ProgramRequirementList</code> corresponding to the
     *  given program requirement genus <code>Type</code> and include
     *  any additional program requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementGenusType a program requirement genus type
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByParentGenusType(org.osid.type.Type programRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.FederatingProgramRequirementList ret = getProgramRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addProgramRequirementList(session.getProgramRequirementsByParentGenusType(programRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> containing the given
     *  program requirement record <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param programRequirementRecordType a program requirement
     *         record type
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByRecordType(org.osid.type.Type programRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.FederatingProgramRequirementList ret = getProgramRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addProgramRequirementList(session.getProgramRequirementsByRecordType(programRequirementRecordType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> containing the given
     *  program.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programId a program <code>Id</code>
     *  @return the returned <code>ProgramRequirementList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.FederatingProgramRequirementList ret = getProgramRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addProgramRequirementList(session.getProgramRequirementsByProgram(programId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> with the given
     *  <code>Requisite</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code>
     *  @return the returned <code>ProgramRequirementList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.FederatingProgramRequirementList ret = getProgramRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addProgramRequirementList(session.getProgramRequirementsByAltRequisite(requisiteId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     * Gets a <code>RequisiteList</code> immediately containing the
     *  given program requirement.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *
     *  In active mode, program requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  programRequirementId a program requirement <code>Id</code>
     *  @return a list of <code>Requisites</code>
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesForProgramRequirement(programRequirementId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ProgramRequirements</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @return a list of <code>ProgramRequirements</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.FederatingProgramRequirementList ret = getProgramRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addProgramRequirementList(session.getProgramRequirements());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.FederatingProgramRequirementList getProgramRequirementList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.ParallelProgramRequirementList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.programrequirement.CompositeProgramRequirementList());
        }
    }


    /**
     *  Gets the <code>CredentialRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CredentialRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CredentialRequirement</code> and retained for compatibility.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementId <code>Id</code> of the
     *          <code>CredentialRequirement</code>
     *  @return the credential requirement
     *  @throws org.osid.NotFoundException <code>credentialRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>credentialRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirement getCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            try {
                return (session.getCredentialRequirement(credentialRequirementId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(credentialRequirementId + " not found");
    }

    
    /**
     *  Gets a <code>CredentialRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the credential
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>CredentialRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByIds(org.osid.id.IdList credentialRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.requisite.credentialrequirement.MutableCredentialRequirementList ret = new net.okapia.osid.jamocha.course.requisite.credentialrequirement.MutableCredentialRequirementList();

        try (org.osid.id.IdList ids = credentialRequirementIds) {
            while (ids.hasNext()) {
                ret.addCredentialRequirement(getCredentialRequirement(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }

    
    /**
     *  Gets a <code>CredentialRequirementList</code> corresponding to the
     *  given credential requirement genus <code>Type</code> which does
     *  not include credential requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementGenusType a credential requirement genus type
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByGenusType(org.osid.type.Type credentialRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.FederatingCredentialRequirementList ret = getCredentialRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCredentialRequirementList(session.getCredentialRequirementsByGenusType(credentialRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>CredentialRequirementList</code> corresponding to the
     *  given credential requirement genus <code>Type</code> and include
     *  any additional credential requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementGenusType a credential requirement genus type
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByParentGenusType(org.osid.type.Type credentialRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.FederatingCredentialRequirementList ret = getCredentialRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCredentialRequirementList(session.getCredentialRequirementsByParentGenusType(credentialRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> containing the given
     *  credential requirement record <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementRecordType a credential requirement record type
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByRecordType(org.osid.type.Type credentialRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.FederatingCredentialRequirementList ret = getCredentialRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCredentialRequirementList(session.getCredentialRequirementsByRecordType(credentialRequirementRecordType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> containing the given
     *  credential.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialId a credential <code>Id</code>
     *  @return the returned <code>CredentialRequirementList</code>
     *  @throws org.osid.NullArgumentException <code>credentialId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByCredential(org.osid.id.Id credentialId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.FederatingCredentialRequirementList ret = getCredentialRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCredentialRequirementList(session.getCredentialRequirementsByCredential(credentialId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> with the given
     *  <code>Requisite</code>.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code>
     *  @return the returned <code>CredentialRequirementList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.FederatingCredentialRequirementList ret = getCredentialRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCredentialRequirementList(session.getCredentialRequirementsByAltRequisite(requisiteId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     * Gets a <code>RequisiteList</code> immediately containing the
     *  given credential requirement.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *
     *  In active mode, credential requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  credentialRequirementId a credential requirement <code>Id</code>
     *  @return a list of <code>Requisites</code>
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesForCredentialRequirement(credentialRequirementId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CredentialRequirements</code>.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @return a list of <code>CredentialRequirements</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.FederatingCredentialRequirementList ret = getCredentialRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addCredentialRequirementList(session.getCredentialRequirements());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.FederatingCredentialRequirementList getCredentialRequirementList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.ParallelCredentialRequirementList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.credentialrequirement.CompositeCredentialRequirementList());
        }
    }


    /**
     *  Gets the <code>LearningObjectiveRequirement</code> specified
     *  by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>LearningObjectiveRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>LearningObjectiveRequirement</code> and retained for
     *  compatibility.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementId <code>Id</code> of the
     *          <code>LearningObjectiveRequirement</code>
     *  @return the learning objective requirement
     *  @throws org.osid.NotFoundException <code>learningObjectiveRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>learningObjectiveRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirement getLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            try {
                return (session.getLearningObjectiveRequirement(learningObjectiveRequirementId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(learningObjectiveRequirementId + " not found");
    }

    
    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  corresponding to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  learning objective requirements specified in the
     *  <code>Id</code> list, in the order of the list, including
     *  duplicates, or an error results if an <code>Id</code> in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible <code>LearningObjectiveRequirements</code> may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByIds(org.osid.id.IdList learningObjectiveRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.requisite.learningobjectiverequirement.MutableLearningObjectiveRequirementList ret = new net.okapia.osid.jamocha.course.requisite.learningobjectiverequirement.MutableLearningObjectiveRequirementList();

        try (org.osid.id.IdList ids = learningObjectiveRequirementIds) {
            while (ids.hasNext()) {
                ret.addLearningObjectiveRequirement(getLearningObjectiveRequirement(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }

    
    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  corresponding to the given learning objective requirement
     *  genus <code>Type</code> which does not include learning
     *  objective requirements of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementGenusType a learning objective requirement genus type
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByGenusType(org.osid.type.Type learningObjectiveRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.FederatingLearningObjectiveRequirementList ret = getLearningObjectiveRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addLearningObjectiveRequirementList(session.getLearningObjectiveRequirementsByGenusType(learningObjectiveRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  corresponding to the given learning objective requirement
     *  genus <code>Type</code> and include any additional learning
     *  objective requirements with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementGenusType a learning objective requirement genus type
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByParentGenusType(org.osid.type.Type learningObjectiveRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.FederatingLearningObjectiveRequirementList ret = getLearningObjectiveRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addLearningObjectiveRequirementList(session.getLearningObjectiveRequirementsByParentGenusType(learningObjectiveRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  containing the given learning objective requirement record
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param learningObjectiveRequirementRecordType a learning
     *         objective requirement record type
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByRecordType(org.osid.type.Type learningObjectiveRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.FederatingLearningObjectiveRequirementList ret = getLearningObjectiveRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addLearningObjectiveRequirementList(session.getLearningObjectiveRequirementsByRecordType(learningObjectiveRequirementRecordType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  containing the given learning objective.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveId a learning objective <code>Id</code>
     *  @return the returned <code>LearningObjectiveRequirementList</code>
     *  @throws org.osid.NullArgumentException <code>learningObjectiveId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByObjective(org.osid.id.Id learningObjectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.FederatingLearningObjectiveRequirementList ret = getLearningObjectiveRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addLearningObjectiveRequirementList(session.getLearningObjectiveRequirementsByObjective(learningObjectiveId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code> with the
     *  given <code>Requisite</code>.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code>
     *  @return the returned <code>LearningObjectiveRequirementList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.FederatingLearningObjectiveRequirementList ret = getLearningObjectiveRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addLearningObjectiveRequirementList(session.getLearningObjectiveRequirementsByAltRequisite(requisiteId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     * Gets a <code>RequisiteList</code> immediately containing the
     *  given learning objective requirement.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *
     *  In active mode, learning objective requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  learningObjectiveRequirementId a learning objective requirement <code>Id</code>
     *  @return a list of <code>Requisites</code>
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesForLearningObjectiveRequirement(learningObjectiveRequirementId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>LearningObjectiveRequirements</code>.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @return a list of <code>LearningObjectiveRequirements</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.FederatingLearningObjectiveRequirementList ret = getLearningObjectiveRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addLearningObjectiveRequirementList(session.getLearningObjectiveRequirements());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.FederatingLearningObjectiveRequirementList getLearningObjectiveRequirementList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.ParallelLearningObjectiveRequirementList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.learningobjectiverequirement.CompositeLearningObjectiveRequirementList());
        }
    }


    /**
     *  Gets the <code>AssessmentRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AssessmentRequirement</code> and retained for compatibility.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementId <code>Id</code> of the
     *          <code>AssessmentRequirement</code>
     *  @return the assessment requirement
     *  @throws org.osid.NotFoundException <code>assessmentRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirement getAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            try {
                return (session.getAssessmentRequirement(assessmentRequirementId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(assessmentRequirementId + " not found");
    }

    
    /**
     *  Gets a <code>AssessmentRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the assessment
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>AssessmentRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByIds(org.osid.id.IdList assessmentRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.requisite.assessmentrequirement.MutableAssessmentRequirementList ret = new net.okapia.osid.jamocha.course.requisite.assessmentrequirement.MutableAssessmentRequirementList();

        try (org.osid.id.IdList ids = assessmentRequirementIds) {
            while (ids.hasNext()) {
                ret.addAssessmentRequirement(getAssessmentRequirement(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }

    
    /**
     *  Gets a <code>AssessmentRequirementList</code> corresponding to the
     *  given assessment requirement genus <code>Type</code> which does
     *  not include assessment requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param assessmentRequirementGenusType an assessment
     *         requirement genus type
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByGenusType(org.osid.type.Type assessmentRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.FederatingAssessmentRequirementList ret = getAssessmentRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAssessmentRequirementList(session.getAssessmentRequirementsByGenusType(assessmentRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>AssessmentRequirementList</code> corresponding to the
     *  given assessment requirement genus <code>Type</code> and include
     *  any additional assessment requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param assessmentRequirementGenusType an assessment requirement
     *         genus type
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByParentGenusType(org.osid.type.Type assessmentRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.FederatingAssessmentRequirementList ret = getAssessmentRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAssessmentRequirementList(session.getAssessmentRequirementsByParentGenusType(assessmentRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>AssessmentRequirementList</code> containing the given
     *  assessment requirement record <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param assessmentRequirementRecordType an assessment
     *         requirement record type
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByRecordType(org.osid.type.Type assessmentRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.FederatingAssessmentRequirementList ret = getAssessmentRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAssessmentRequirementList(session.getAssessmentRequirementsByRecordType(assessmentRequirementRecordType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>AssessmentRequirementList</code> containing the given
     *  assessment.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentId an assessment <code>Id</code>
     *  @return the returned <code>AssessmentRequirementList</code>
     *  @throws org.osid.NullArgumentException <code>assessmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.FederatingAssessmentRequirementList ret = getAssessmentRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAssessmentRequirementList(session.getAssessmentRequirementsByAssessment(assessmentId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>AssessmentRequirementList</code> with the given
     *  <code>Requisite</code>.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code>
     *  @return the returned <code>AssessmentRequirementList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.FederatingAssessmentRequirementList ret = getAssessmentRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAssessmentRequirementList(session.getAssessmentRequirementsByAltRequisite(requisiteId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     * Gets a <code>RequisiteList</code> immediately containing the
     *  given assessment requirement.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *
     *  In active mode, assessment requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param assessmentRequirementId an assessment requirement
     *         <code>Id</code>
     *  @return a list of <code>Requisites</code>
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesForAssessmentRequirement(assessmentRequirementId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AssessmentRequirements</code>.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @return a list of <code>AssessmentRequirements</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.FederatingAssessmentRequirementList ret = getAssessmentRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAssessmentRequirementList(session.getAssessmentRequirements());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.FederatingAssessmentRequirementList getAssessmentRequirementList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.ParallelAssessmentRequirementList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.assessmentrequirement.CompositeAssessmentRequirementList());
        }
    }


    /**
     *  Gets the <code>AwardRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AwardRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AwardRequirement</code> and retained for compatibility.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementId <code>Id</code> of the
     *          <code>AwardRequirement</code>
     *  @return the award requirement
     *  @throws org.osid.NotFoundException <code>awardRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirement getAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            try {
                return (session.getAwardRequirement(awardRequirementId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(awardRequirementId + " not found");
    }

    
    /**
     *  Gets a <code>AwardRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the award
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>AwardRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByIds(org.osid.id.IdList awardRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.requisite.awardrequirement.MutableAwardRequirementList ret = new net.okapia.osid.jamocha.course.requisite.awardrequirement.MutableAwardRequirementList();

        try (org.osid.id.IdList ids = awardRequirementIds) {
            while (ids.hasNext()) {
                ret.addAwardRequirement(getAwardRequirement(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }

    
    /**
     *  Gets a <code>AwardRequirementList</code> corresponding to the
     *  given award requirement genus <code>Type</code> which does
     *  not include award requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param awardRequirementGenusType an award
     *         requirement genus type
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByGenusType(org.osid.type.Type awardRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.FederatingAwardRequirementList ret = getAwardRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAwardRequirementList(session.getAwardRequirementsByGenusType(awardRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>AwardRequirementList</code> corresponding to the
     *  given award requirement genus <code>Type</code> and include
     *  any additional award requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param awardRequirementGenusType an award requirement
     *         genus type
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByParentGenusType(org.osid.type.Type awardRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.FederatingAwardRequirementList ret = getAwardRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAwardRequirementList(session.getAwardRequirementsByParentGenusType(awardRequirementGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>AwardRequirementList</code> containing the given
     *  award requirement record <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param awardRequirementRecordType an award
     *         requirement record type
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByRecordType(org.osid.type.Type awardRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.FederatingAwardRequirementList ret = getAwardRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAwardRequirementList(session.getAwardRequirementsByRecordType(awardRequirementRecordType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>AwardRequirementList</code> containing the given
     *  award.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardId an award <code>Id</code>
     *  @return the returned <code>AwardRequirementList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByAward(org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.FederatingAwardRequirementList ret = getAwardRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAwardRequirementList(session.getAwardRequirementsByAward(awardId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>AwardRequirementList</code> with the given
     *  <code>Requisite</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code>
     *  @return the returned <code>AwardRequirementList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.FederatingAwardRequirementList ret = getAwardRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAwardRequirementList(session.getAwardRequirementsByAltRequisite(requisiteId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     * Gets a <code>RequisiteList</code> immediately containing the
     *  given award requirement.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *
     *  In active mode, award requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param awardRequirementId an award requirement
     *         <code>Id</code>
     *  @return a list of <code>Requisites</code>
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.requisite.FederatingRequisiteList ret = getRequisiteList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addRequisiteList(session.getRequisitesForAwardRequirement(awardRequirementId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AwardRequirements</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @return a list of <code>AwardRequirements</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.FederatingAwardRequirementList ret = getAwardRequirementList();

        for (org.osid.course.requisite.RequisiteLookupSession session : getSessions()) {
            ret.addAwardRequirementList(session.getAwardRequirements());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.FederatingAwardRequirementList getAwardRequirementList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.ParallelAwardRequirementList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.requisite.awardrequirement.CompositeAwardRequirementList());
        }
    }
}
