//
// AbstractProgramOfferingLookupSession.java
//
//    A starter implementation framework for providing a ProgramOffering
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ProgramOffering
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProgramOfferings(), this other methods may need to be
 *  overridden for better performance.
 */

public abstract class AbstractProgramOfferingLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.program.ProgramOfferingLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private boolean effectiveonly = true;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProgramOffering</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProgramOfferings() {
        return (true);
    }


    /**
     *  A complete view of the <code>ProgramOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProgramOfferingView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ProgramOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProgramOfferingView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include program offerings in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only program offerings whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveProgramOfferingView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All program offerings of any effective dates are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProgramOfferingView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>ProgramOffering</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramOffering</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProgramOffering</code> and
     *  retained for compatibility.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingId <code>Id</code> of the
     *          <code>ProgramOffering</code>
     *  @return the program offering
     *  @throws org.osid.NotFoundException <code>programOfferingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOffering getProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.program.ProgramOfferingList programOfferings = getProgramOfferings()) {
            while (programOfferings.hasNext()) {
                org.osid.course.program.ProgramOffering programOffering = programOfferings.getNextProgramOffering();
                if (programOffering.getId().equals(programOfferingId)) {
                    return (programOffering);
                }
            }
        } 

        throw new org.osid.NotFoundException(programOfferingId + " not found");
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programOfferings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProgramOfferings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProgramOfferings()</code>.
     *
     *  @param  programOfferingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByIds(org.osid.id.IdList programOfferingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.program.ProgramOffering> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = programOfferingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProgramOffering(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("program offering " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.program.programoffering.LinkedProgramOfferingList(ret));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the
     *  given program offering genus <code>Type</code> which does not
     *  include program offerings of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProgramOfferings()</code>.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.ProgramOfferingGenusFilterList(getProgramOfferings(), programOfferingGenusType));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the
     *  given program offering genus <code>Type</code> and include any
     *  additional program offerings with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProgramOfferings()</code>.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByParentGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProgramOfferingsByGenusType(programOfferingGenusType));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> containing the given
     *  program offering record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProgramOfferings()</code>.
     *
     *  @param  programOfferingRecordType a programOffering record type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByRecordType(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.ProgramOfferingRecordFilterList(getProgramOfferings(), programOfferingRecordType));
    }


    /**
     *  Gets a <code> ProgramOfferingList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective. In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> ProgramOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsOnDate(org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.TemporalProgramOfferingFilterList(getProgramOfferings(), from, to));
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code> Program. </code> 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code> 
     *  @return a list of <code> ProgramOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
     
        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.ProgramOfferingFilterList(new ProgramFilter(programId), getProgramOfferings()));
    }


    /**
     *  Gets a <code> ProgramOfferingList </code> for the given
     *  program effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective. In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param programId the <code>Id</code> of a program
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> ProgramOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramOnDate(org.osid.id.Id programId, 
                                                                                           org.osid.calendaring.DateTime from,
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.TemporalProgramOfferingFilterList(getProgramOfferingsForProgram(programId), from, to));
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> ProgramOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.ProgramOfferingFilterList(new TermFilter(termId), getProgramOfferings()));
    }


    /**
     *  Gets a <code> ProgramOfferingList </code> for the given
     *  term effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective. In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param termId the <code>Id</code> of a term
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> ProgramOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTermOnDate(org.osid.id.Id termId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.TemporalProgramOfferingFilterList(getProgramOfferingsForTerm(termId), from, to));
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code>Program</code> and <code>Term</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> ProgramOfferings </code> 
     *  @throws org.osid.NullArgumentException <code>programId</code>
     *          or <code>termId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTerm(org.osid.id.Id programId, 
                                                                                            org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.ProgramOfferingFilterList(new TermFilter(termId), getProgramOfferingsForProgram(programId)));
    }


    /**
     *  Gets a <code> ProgramOfferingList </code> for the given
     *  program, term, and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective. In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param programId the <code>Id</code> of a program
     *  @param termId the <code>Id</code> of a term
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> ProgramOffering </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTermOnDate(org.osid.id.Id programId, 
                                                                                                  org.osid.id.Id termId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.TemporalProgramOfferingFilterList(getProgramOfferingsForProgramAndTerm(programId, termId), from, to));
    }


    /**
     *  Gets all <code>ProgramOfferings</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @return a list of <code>ProgramOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.program.ProgramOfferingList getProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the program offering list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of program offerings
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.program.ProgramOfferingList filterProgramOfferingsOnViews(org.osid.course.program.ProgramOfferingList list)
        throws org.osid.OperationFailedException {

        org.osid.course.program.ProgramOfferingList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.program.programoffering.EffectiveProgramOfferingFilterList(ret);
        }

        return (ret);
    }


    public static class ProgramFilter
        implements net.okapia.osid.jamocha.inline.filter.course.program.programoffering.ProgramOfferingFilter {

        private final org.osid.id.Id programId;
         
         
        /**
         *  Constructs a new <code>ProgramFilter</code>.
         *
         *  @param programId the program to filter
         *  @throws org.osid.NullArgumentException
         *          <code>programId</code> is <code>null</code>
         */
        
        public ProgramFilter(org.osid.id.Id programId) {
            nullarg(programId, "program Id");
            this.programId = programId;
            return;
        }

         
        /**
         *  Used by the ProgramOfferingFilterList to filter the
         *  program offering list based on program.
         *
         *  @param programOffering the program offering
         *  @return <code>true</code> to pass the program,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.program.ProgramOffering programOffering) {
            return (programOffering.getProgramId().equals(this.programId));
        }
    }


    public static class TermFilter
        implements net.okapia.osid.jamocha.inline.filter.course.program.programoffering.ProgramOfferingFilter {

        private final org.osid.id.Id termId;
         
         
        /**
         *  Constructs a new <code>TermFilter</code>.
         *
         *  @param termId the term to filter
         *  @throws org.osid.NullArgumentException
         *          <code>termId</code> is <code>null</code>
         */
        
        public TermFilter(org.osid.id.Id termId) {
            nullarg(termId, "term Id");
            this.termId = termId;
            return;
        }

         
        /**
         *  Used by the ProgramOfferingFilterList to filter the
         *  program offering list based on term.
         *
         *  @param programOffering the program offering
         *  @return <code>true</code> to pass the program offering,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.program.ProgramOffering programOffering) {
            return (programOffering.getTermId().equals(this.termId));
        }
    }
}
