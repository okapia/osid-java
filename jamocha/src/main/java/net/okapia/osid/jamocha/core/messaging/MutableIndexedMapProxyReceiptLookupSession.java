//
// MutableIndexedMapProxyReceiptLookupSession
//
//    Implements a Receipt lookup service backed by a collection of
//    receipts indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging;


/**
 *  Implements a Receipt lookup service backed by a collection of
 *  receipts. The receipts are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some receipts may be compatible
 *  with more types than are indicated through these receipt
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of receipts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyReceiptLookupSession
    extends net.okapia.osid.jamocha.core.messaging.spi.AbstractIndexedMapReceiptLookupSession
    implements org.osid.messaging.ReceiptLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyReceiptLookupSession} with
     *  no receipt.
     *
     *  @param mailbox the mailbox
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyReceiptLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       org.osid.proxy.Proxy proxy) {
        setMailbox(mailbox);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyReceiptLookupSession} with
     *  a single receipt.
     *
     *  @param mailbox the mailbox
     *  @param  receipt an receipt
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code receipt}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyReceiptLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       org.osid.messaging.Receipt receipt, org.osid.proxy.Proxy proxy) {

        this(mailbox, proxy);
        putReceipt(receipt);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyReceiptLookupSession} using
     *  an array of receipts.
     *
     *  @param mailbox the mailbox
     *  @param  receipts an array of receipts
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code receipts}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyReceiptLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       org.osid.messaging.Receipt[] receipts, org.osid.proxy.Proxy proxy) {

        this(mailbox, proxy);
        putReceipts(receipts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyReceiptLookupSession} using
     *  a collection of receipts.
     *
     *  @param mailbox the mailbox
     *  @param  receipts a collection of receipts
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code receipts}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyReceiptLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       java.util.Collection<? extends org.osid.messaging.Receipt> receipts,
                                                       org.osid.proxy.Proxy proxy) {
        this(mailbox, proxy);
        putReceipts(receipts);
        return;
    }

    
    /**
     *  Makes a {@code Receipt} available in this session.
     *
     *  @param  receipt a receipt
     *  @throws org.osid.NullArgumentException {@code receipt{@code 
     *          is {@code null}
     */

    @Override
    public void putReceipt(org.osid.messaging.Receipt receipt) {
        super.putReceipt(receipt);
        return;
    }


    /**
     *  Makes an array of receipts available in this session.
     *
     *  @param  receipts an array of receipts
     *  @throws org.osid.NullArgumentException {@code receipts{@code 
     *          is {@code null}
     */

    @Override
    public void putReceipts(org.osid.messaging.Receipt[] receipts) {
        super.putReceipts(receipts);
        return;
    }


    /**
     *  Makes collection of receipts available in this session.
     *
     *  @param  receipts a collection of receipts
     *  @throws org.osid.NullArgumentException {@code receipt{@code 
     *          is {@code null}
     */

    @Override
    public void putReceipts(java.util.Collection<? extends org.osid.messaging.Receipt> receipts) {
        super.putReceipts(receipts);
        return;
    }


    /**
     *  Removes a Receipt from this session.
     *
     *  @param receiptId the {@code Id} of the receipt
     *  @throws org.osid.NullArgumentException {@code receiptId{@code  is
     *          {@code null}
     */

    @Override
    public void removeReceipt(org.osid.id.Id receiptId) {
        super.removeReceipt(receiptId);
        return;
    }    
}
