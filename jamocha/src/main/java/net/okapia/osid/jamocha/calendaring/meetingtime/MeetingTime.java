//
// MeetingTime.java
//
//     Defines a MeetingTime.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.meetingtime;


/**
 *  Defines a <code>MeetingTime</code>.
 */

public final class MeetingTime
    extends net.okapia.osid.jamocha.calendaring.meetingtime.spi.AbstractMeetingTime
    implements org.osid.calendaring.MeetingTime {

    
    /**
     *  Constructs a new <code>MeetingTime</code>
     *
     *  @param date the date
     *  @param location the location
     *  @throws org.osid.NullArgumentException <code>date</code> or
     *          <code>location</code> is <code>null</code>
     */

    public MeetingTime(org.osid.calendaring.DateTime date, org.osid.mapping.Location location) {
        setDate(date);
        setLocation(location);
        return;
    }


    /**
     *  Constructs a new <code>MeetingTime</code>
     *
     *  @param date the date
     *  @param location a descrtive location label
     *  @throws org.osid.NullArgumentException <code>date</code> or
     *          <code>location</code> is <code>null</code>
     */

    public MeetingTime(org.osid.calendaring.DateTime date, org.osid.locale.DisplayText location) {
        setDate(date);
        setLocationDescription(location);
        return;
    }
}
