//
// MutableNodeAgencyHierarchySession.java
//
//     Defines an Agency hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication;


/**
 *  Defines an agency hierarchy session for delivering a hierarchy
 *  of agencies using the AgencyNode interface.
 */

public final class MutableNodeAgencyHierarchySession
    extends net.okapia.osid.jamocha.core.authentication.spi.AbstractNodeAgencyHierarchySession
    implements org.osid.authentication.AgencyHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodeAgencyHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodeAgencyHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeAgencyHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodeAgencyHierarchySession(org.osid.authentication.AgencyNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getAgency().getDisplayName())
                     .description(root.getAgency().getDescription())
                     .build());

        addRootAgency(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeAgencyHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodeAgencyHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.authentication.AgencyNode root) {
        setHierarchy(hierarchy);
        addRootAgency(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeAgencyHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodeAgencyHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               java.util.Collection<org.osid.authentication.AgencyNode> roots) {
        setHierarchy(hierarchy);
        addRootAgencies(roots);
        return;
    }


    /**
     *  Adds a root agency node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootAgency(org.osid.authentication.AgencyNode root) {
        super.addRootAgency(root);
        return;
    }


    /**
     *  Adds a collection of root agency nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootAgencies(java.util.Collection<org.osid.authentication.AgencyNode> roots) {
        super.addRootAgencies(roots);
        return;
    }


    /**
     *  Removes a root agency node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootAgency(org.osid.id.Id rootId) {
        super.removeRootAgency(rootId);
        return;
    }
}
