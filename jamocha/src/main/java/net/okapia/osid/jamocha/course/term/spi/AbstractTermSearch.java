//
// AbstractTermSearch.java
//
//     A template for making a Term Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.term.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing term searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractTermSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.TermSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.records.TermSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.TermSearchOrder termSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of terms. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  termIds list of terms
     *  @throws org.osid.NullArgumentException
     *          <code>termIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongTerms(org.osid.id.IdList termIds) {
        while (termIds.hasNext()) {
            try {
                this.ids.add(termIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongTerms</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of term Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getTermIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  termSearchOrder term search order 
     *  @throws org.osid.NullArgumentException
     *          <code>termSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>termSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderTermResults(org.osid.course.TermSearchOrder termSearchOrder) {
	this.termSearchOrder = termSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.TermSearchOrder getTermSearchOrder() {
	return (this.termSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given term search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a term implementing the requested record.
     *
     *  @param termSearchRecordType a term search record
     *         type
     *  @return the term search record
     *  @throws org.osid.NullArgumentException
     *          <code>termSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(termSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermSearchRecord getTermSearchRecord(org.osid.type.Type termSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.records.TermSearchRecord record : this.records) {
            if (record.implementsRecordType(termSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(termSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this term search. 
     *
     *  @param termSearchRecord term search record
     *  @param termSearchRecordType term search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTermSearchRecord(org.osid.course.records.TermSearchRecord termSearchRecord, 
                                           org.osid.type.Type termSearchRecordType) {

        addRecordType(termSearchRecordType);
        this.records.add(termSearchRecord);        
        return;
    }
}
