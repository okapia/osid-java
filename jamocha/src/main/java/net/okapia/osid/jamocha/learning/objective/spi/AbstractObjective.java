//
// AbstractObjective.java
//
//     Defines an Objective.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Objective</code>.
 */

public abstract class AbstractObjective
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.learning.Objective {

    private org.osid.assessment.Assessment assessment;
    private org.osid.grading.Grade knowledgeCategory;
    private org.osid.grading.Grade cognitiveProcess;

    private final java.util.Collection<org.osid.learning.records.ObjectiveRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if an assessment is associated with this objective. 
     *
     *  @return <code> true </code> if an assessment exists, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean hasAssessment() {
        return (this.assessment != null);
    }


    /**
     *  Gets the assessment <code> Id </code> associated with this learning 
     *  objective. 
     *
     *  @return the assessment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        if (!hasAssessment()) {
            throw new org.osid.IllegalStateException("hasAssessment() is false");
        }

        return (this.assessment.getId());
    }


    /**
     *  Gets the assessment associated with this learning objective. 
     *
     *  @return the assessment 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        if (!hasAssessment()) {
            throw new org.osid.IllegalStateException("hasAssessment() is false");
        }

        return (this.assessment);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    protected void setAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");
        this.assessment = assessment;
        return;
    }


    /**
     *  Tests if this objective has a knowledge dimension 
     *
     *  @return <code> true </code> if a knowledge category exists, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasKnowledgeCategory() {
        return (this.knowledgeCategory != null);
    }


    /**
     *  Gets the grade <code> Id </code> associated with the knowledge 
     *  dimension. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasKnowledgeCategory() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getKnowledgeCategoryId() {
        if (!hasKnowledgeCategory()) {
            throw new org.osid.IllegalStateException("hasKnowledgeCategory() is false");
        }

        return (this.knowledgeCategory.getId());
    }


    /**
     *  Gets the grade associated with the knowledge dimension. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasKnowledgeCategory() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getKnowledgeCategory()
        throws org.osid.OperationFailedException {

        if (!hasKnowledgeCategory()) {
            throw new org.osid.IllegalStateException("hasKnowledgeCategory() is false");
        }

        return (this.knowledgeCategory);
    }


    /**
     *  Sets the knowledge category.
     *
     *  @param category a knowledge category
     *  @throws org.osid.NullArgumentException <code>category</code>
     *          is <code>null</code>
     */

    protected void setKnowledgeCategory(org.osid.grading.Grade category) {
        nullarg(category, "knowledge category");
        this.knowledgeCategory = category;
        return;
    }


    /**
     *  Tests if this objective has a cognitive process type. 
     *
     *  @return <code> true </code> if a cognitive process exists, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCognitiveProcess() {
        return (this.cognitiveProcess != null);
    }


    /**
     *  Gets the grade <code> Id </code> associated with the cognitive 
     *  process. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCognitiveProcess() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCognitiveProcessId() {
        if (!hasCognitiveProcess()) {
            throw new org.osid.IllegalStateException("getCognitiveProcess() is false");
        }

        return (this.cognitiveProcess.getId());
    }


    /**
     *  Gets the grade associated with the cognitive process. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasCognitiveProcess() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getCognitiveProcess()
        throws org.osid.OperationFailedException {

        if (!hasCognitiveProcess()) {
            throw new org.osid.IllegalStateException("getCognitiveProcess() is false");
        }

        return (this.cognitiveProcess);
    }


    /**
     *  Sets the cognitive process.
     *
     *  @param process a cognitive process
     *  @throws org.osid.NullArgumentException
     *          <code>process</code> is <code>null</code>
     */

    protected void setCognitiveProcess(org.osid.grading.Grade process) {
        nullarg(process, "cognitive process");
        this.cognitiveProcess = process;
        return;
    }

    /**
     *  Tests if this objective supports the given record
     *  <code>Type</code>.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return <code>true</code> if the objectiveRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type objectiveRecordType) {
        for (org.osid.learning.records.ObjectiveRecord record : this.records) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Objective</code> record <code>Type</code>.
     *
     *  @param  objectiveRecordType the objective record type 
     *  @return the objective record 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveRecord getObjectiveRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveRecord record : this.records) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveRecordType + " is not supported");
    }


    /**
     *  Adds a record to this objective. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param objectiveRecord the objective record
     *  @param objectiveRecordType objective record type
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecord</code> or
     *          <code>objectiveRecordTypeobjective</code> is
     *          <code>null</code>
     */
            
    protected void addObjectiveRecord(org.osid.learning.records.ObjectiveRecord objectiveRecord, 
                                      org.osid.type.Type objectiveRecordType) {

        nullarg(objectiveRecord, "objective record");
        addRecordType(objectiveRecordType);
        this.records.add(objectiveRecord);
        
        return;
    }
}
