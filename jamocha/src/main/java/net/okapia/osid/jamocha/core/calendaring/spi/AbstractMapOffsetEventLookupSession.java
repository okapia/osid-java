//
// AbstractMapOffsetEventLookupSession
//
//    A simple framework for providing an OffsetEvent lookup service
//    backed by a fixed collection of offset events.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an OffsetEvent lookup service backed by a
 *  fixed collection of offset events. The offset events are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OffsetEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOffsetEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractOffsetEventLookupSession
    implements org.osid.calendaring.OffsetEventLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.OffsetEvent> offsetEvents = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.OffsetEvent>());


    /**
     *  Makes an <code>OffsetEvent</code> available in this session.
     *
     *  @param  offsetEvent an offset event
     *  @throws org.osid.NullArgumentException <code>offsetEvent<code>
     *          is <code>null</code>
     */

    protected void putOffsetEvent(org.osid.calendaring.OffsetEvent offsetEvent) {
        this.offsetEvents.put(offsetEvent.getId(), offsetEvent);
        return;
    }


    /**
     *  Makes an array of offset events available in this session.
     *
     *  @param  offsetEvents an array of offset events
     *  @throws org.osid.NullArgumentException <code>offsetEvents<code>
     *          is <code>null</code>
     */

    protected void putOffsetEvents(org.osid.calendaring.OffsetEvent[] offsetEvents) {
        putOffsetEvents(java.util.Arrays.asList(offsetEvents));
        return;
    }


    /**
     *  Makes a collection of offset events available in this session.
     *
     *  @param  offsetEvents a collection of offset events
     *  @throws org.osid.NullArgumentException <code>offsetEvents<code>
     *          is <code>null</code>
     */

    protected void putOffsetEvents(java.util.Collection<? extends org.osid.calendaring.OffsetEvent> offsetEvents) {
        for (org.osid.calendaring.OffsetEvent offsetEvent : offsetEvents) {
            this.offsetEvents.put(offsetEvent.getId(), offsetEvent);
        }

        return;
    }


    /**
     *  Removes an OffsetEvent from this session.
     *
     *  @param  offsetEventId the <code>Id</code> of the offset event
     *  @throws org.osid.NullArgumentException <code>offsetEventId<code> is
     *          <code>null</code>
     */

    protected void removeOffsetEvent(org.osid.id.Id offsetEventId) {
        this.offsetEvents.remove(offsetEventId);
        return;
    }


    /**
     *  Gets the <code>OffsetEvent</code> specified by its <code>Id</code>.
     *
     *  @param  offsetEventId <code>Id</code> of the <code>OffsetEvent</code>
     *  @return the offsetEvent
     *  @throws org.osid.NotFoundException <code>offsetEventId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>offsetEventId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEvent getOffsetEvent(org.osid.id.Id offsetEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.OffsetEvent offsetEvent = this.offsetEvents.get(offsetEventId);
        if (offsetEvent == null) {
            throw new org.osid.NotFoundException("offsetEvent not found: " + offsetEventId);
        }

        return (offsetEvent);
    }


    /**
     *  Gets all <code>OffsetEvents</code>. In plenary mode, the returned
     *  list contains all known offsetEvents or an error
     *  results. Otherwise, the returned list may contain only those
     *  offsetEvents that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>OffsetEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.offsetevent.ArrayOffsetEventList(this.offsetEvents.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offsetEvents.clear();
        super.close();
        return;
    }
}
