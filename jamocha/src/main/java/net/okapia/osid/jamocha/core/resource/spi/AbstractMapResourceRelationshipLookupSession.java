//
// AbstractMapResourceRelationshipLookupSession
//
//    A simple framework for providing a ResourceRelationship lookup service
//    backed by a fixed collection of resource relationships.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ResourceRelationship lookup service backed by a
 *  fixed collection of resource relationships. The resource relationships are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ResourceRelationships</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapResourceRelationshipLookupSession
    extends net.okapia.osid.jamocha.resource.spi.AbstractResourceRelationshipLookupSession
    implements org.osid.resource.ResourceRelationshipLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resource.ResourceRelationship> resourceRelationships = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resource.ResourceRelationship>());


    /**
     *  Makes a <code>ResourceRelationship</code> available in this session.
     *
     *  @param  resourceRelationship a resource relationship
     *  @throws org.osid.NullArgumentException <code>resourceRelationship<code>
     *          is <code>null</code>
     */

    protected void putResourceRelationship(org.osid.resource.ResourceRelationship resourceRelationship) {
        this.resourceRelationships.put(resourceRelationship.getId(), resourceRelationship);
        return;
    }


    /**
     *  Makes an array of resource relationships available in this session.
     *
     *  @param  resourceRelationships an array of resource relationships
     *  @throws org.osid.NullArgumentException <code>resourceRelationships<code>
     *          is <code>null</code>
     */

    protected void putResourceRelationships(org.osid.resource.ResourceRelationship[] resourceRelationships) {
        putResourceRelationships(java.util.Arrays.asList(resourceRelationships));
        return;
    }


    /**
     *  Makes a collection of resource relationships available in this session.
     *
     *  @param  resourceRelationships a collection of resource relationships
     *  @throws org.osid.NullArgumentException <code>resourceRelationships<code>
     *          is <code>null</code>
     */

    protected void putResourceRelationships(java.util.Collection<? extends org.osid.resource.ResourceRelationship> resourceRelationships) {
        for (org.osid.resource.ResourceRelationship resourceRelationship : resourceRelationships) {
            this.resourceRelationships.put(resourceRelationship.getId(), resourceRelationship);
        }

        return;
    }


    /**
     *  Removes a ResourceRelationship from this session.
     *
     *  @param  resourceRelationshipId the <code>Id</code> of the resource relationship
     *  @throws org.osid.NullArgumentException <code>resourceRelationshipId<code> is
     *          <code>null</code>
     */

    protected void removeResourceRelationship(org.osid.id.Id resourceRelationshipId) {
        this.resourceRelationships.remove(resourceRelationshipId);
        return;
    }


    /**
     *  Gets the <code>ResourceRelationship</code> specified by its <code>Id</code>.
     *
     *  @param  resourceRelationshipId <code>Id</code> of the <code>ResourceRelationship</code>
     *  @return the resourceRelationship
     *  @throws org.osid.NotFoundException <code>resourceRelationshipId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>resourceRelationshipId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationship getResourceRelationship(org.osid.id.Id resourceRelationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resource.ResourceRelationship resourceRelationship = this.resourceRelationships.get(resourceRelationshipId);
        if (resourceRelationship == null) {
            throw new org.osid.NotFoundException("resourceRelationship not found: " + resourceRelationshipId);
        }

        return (resourceRelationship);
    }


    /**
     *  Gets all <code>ResourceRelationships</code>. In plenary mode, the returned
     *  list contains all known resourceRelationships or an error
     *  results. Otherwise, the returned list may contain only those
     *  resourceRelationships that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ResourceRelationships</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.resourcerelationship.ArrayResourceRelationshipList(this.resourceRelationships.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.resourceRelationships.clear();
        super.close();
        return;
    }
}
