//
// InvariantMapProxyTermLookupSession
//
//    Implements a Term lookup service backed by a fixed
//    collection of terms. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a Term lookup service backed by a fixed
 *  collection of terms. The terms are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyTermLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractMapTermLookupSession
    implements org.osid.course.TermLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyTermLookupSession} with no
     *  terms.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyTermLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyTermLookupSession} with a single
     *  term.
     *
     *  @param courseCatalog the course catalog
     *  @param term a single term
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code term} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTermLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.Term term, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putTerm(term);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyTermLookupSession} using
     *  an array of terms.
     *
     *  @param courseCatalog the course catalog
     *  @param terms an array of terms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code terms} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTermLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.Term[] terms, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putTerms(terms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyTermLookupSession} using a
     *  collection of terms.
     *
     *  @param courseCatalog the course catalog
     *  @param terms a collection of terms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code terms} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTermLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.Term> terms,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putTerms(terms);
        return;
    }
}
