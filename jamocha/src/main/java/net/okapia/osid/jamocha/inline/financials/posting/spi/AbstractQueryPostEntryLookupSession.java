//
// AbstractQueryPostEntryLookupSession.java
//
//    An inline adapter that maps a PostEntryLookupSession to
//    a PostEntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PostEntryLookupSession to
 *  a PostEntryQuerySession.
 */

public abstract class AbstractQueryPostEntryLookupSession
    extends net.okapia.osid.jamocha.financials.posting.spi.AbstractPostEntryLookupSession
    implements org.osid.financials.posting.PostEntryLookupSession {

    private final org.osid.financials.posting.PostEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPostEntryLookupSession.
     *
     *  @param querySession the underlying post entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPostEntryLookupSession(org.osid.financials.posting.PostEntryQuerySession querySession) {
        nullarg(querySession, "post entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>PostEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPostEntries() {
        return (this.session.canSearchPostEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include post entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the <code>PostEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PostEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PostEntry</code> and
     *  retained for compatibility.
     *
     *  @param  postEntryId <code>Id</code> of the
     *          <code>PostEntry</code>
     *  @return the post entry
     *  @throws org.osid.NotFoundException <code>postEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>postEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntry getPostEntry(org.osid.id.Id postEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchId(postEntryId, true);
        org.osid.financials.posting.PostEntryList postEntries = this.session.getPostEntriesByQuery(query);
        if (postEntries.hasNext()) {
            return (postEntries.getNextPostEntry());
        } 
        
        throw new org.osid.NotFoundException(postEntryId + " not found");
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  postEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>PostEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  postEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByIds(org.osid.id.IdList postEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();

        try (org.osid.id.IdList ids = postEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPostEntriesByQuery(query));
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  post entry genus <code>Type</code> which does not include
     *  post entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchGenusType(postEntryGenusType, true);
        return (this.session.getPostEntriesByQuery(query));
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  post entry genus <code>Type</code> and include any additional
     *  post entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByParentGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchParentGenusType(postEntryGenusType, true);
        return (this.session.getPostEntriesByQuery(query));
    }


    /**
     *  Gets a <code>PostEntryList</code> containing the given
     *  post entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryRecordType a postEntry record type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByRecordType(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchRecordType(postEntryRecordType, true);
        return (this.session.getPostEntriesByQuery(query));
    }

    
    /**
     *  Gets a <code> PostEntryList </code> for the given post.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  postId a post <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesForPost(org.osid.id.Id postId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchPostId(postId, true);
        return (this.session.getPostEntriesByQuery(query));
    }

    
    /**
     *  Gets a <code> PostEntryList </code> in the given fiscal
     *  period.
     *  
     *  In plenary mode, the returned list contains all
     *  known post entries or an error results. Otherwise, the
     *  returned list may contain only those post entries that are
     *  accessible through this session.
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        if (query.supportsPostQuery()) {
            query.getPostQuery().matchFiscalPeriodId(fiscalPeriodId, true);
            return (this.session.getPostEntriesByQuery(query));
        }

        return (super.getPostEntriesByFiscalPeriod(fiscalPeriodId));
    }


    /**
     *  Gets a <code> PostEntryList </code> posted within given date
     *  range inclusive.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        if (query.supportsPostQuery()) {
            query.getPostQuery().matchDate(from, to, true);
            return (this.session.getPostEntriesByQuery(query));
        }

        return (super.getPostEntriesByDate(from, to));
    }


    /**
     *  Gets a <code> PostEntryList </code> for the given
     *  account.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccount(org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchAccountId(accountId, true);
        return (this.session.getPostEntriesByQuery(query));
    }


    /**
     *  Gets a <code> PostEntryList </code> for the given account in a
     *  fiscal period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> accountId </code> or 
     *          <code> fiscalPeriodId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndFiscalPeriod(org.osid.id.Id accountId, 
                                                                                            org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        if (query.supportsPostQuery()) {
            query.getPostQuery().matchFiscalPeriodId(fiscalPeriodId, true);
            query.matchAccountId(accountId, true);
            return (this.session.getPostEntriesByQuery(query));
        }

        return (super.getPostEntriesByAccountAndFiscalPeriod(accountId, fiscalPeriodId));
    }

    
    /**
     *  Gets a <code> PostEntryList </code> for the given
     *  activity.
     *  
     *  In plenary mode, the returned list contains all known 
     *  post entries or an error results. Otherwise, the returned list may 
     *  contain only those post entries that are accessible through this 
     *  session. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchActivityId(activityId,true);
        return (this.session.getPostEntriesByQuery(query));
    }


    /**
     *  Gets a <code> PostEntryList </code> for the given activity in
     *  a fiscal period.
     *  
     *  In plenary mode, the returned list contains all
     *  known post entries or an error results. Otherwise, the
     *  returned list may contain only those post entries that are
     *  accessible through this session.
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> or <code> fiscalPeriodId </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivityAndFiscalPeriod(org.osid.id.Id activityId, 
                                                                                             org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        if (query.supportsPostQuery()) {
            query.getPostQuery().matchFiscalPeriodId(fiscalPeriodId, true);
            query.matchActivityId(activityId,true);
            return (this.session.getPostEntriesByQuery(query));
        }

        return (super.getPostEntriesByActivityAndFiscalPeriod(activityId, fiscalPeriodId));
    }


    /**
     *  Gets a <code> PostEntryList </code> for the given activity and
     *  account.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  activityId an activity <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> or <code> activityId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivity(org.osid.id.Id accountId, 
                                                                                        org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchAccountId(accountId,true);
        query.matchActivityId(activityId,true);
        return (this.session.getPostEntriesByQuery(query));
    }


    /**
     *  Gets a <code> PostEntryList </code> for the given account in a
     *  fiscal period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  activityId an activity <code> Id </code> 
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @return the returned <code> PostEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> accountId,
     *          activityId, </code> or <code> fiscalPeriodId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivityAndFiscalPeriod(org.osid.id.Id accountId, 
                                                                                                       org.osid.id.Id activityId, 
                                                                                                       org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        if (query.supportsPostQuery()) {
            query.getPostQuery().matchFiscalPeriodId(fiscalPeriodId, true);
            query.matchAccountId(accountId,true);
            query.matchActivityId(activityId,true);
            return (this.session.getPostEntriesByQuery(query));
        }

        return (super.getPostEntriesByAccountAndActivityAndFiscalPeriod(accountId, activityId, fiscalPeriodId));
    }


    /**
     *  Gets all <code>PostEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>PostEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.posting.PostEntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPostEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.financials.posting.PostEntryQuery getQuery() {
        org.osid.financials.posting.PostEntryQuery query = this.session.getPostEntryQuery();
        return (query);
    }
}
