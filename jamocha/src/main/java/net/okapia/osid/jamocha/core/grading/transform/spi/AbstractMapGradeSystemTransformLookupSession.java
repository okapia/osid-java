//
// AbstractMapGradeSystemTransformLookupSession
//
//    A simple framework for providing a GradeSystemTransform lookup service
//    backed by a fixed collection of grade system transforms.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.transform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a GradeSystemTransform lookup service backed by a
 *  fixed collection of grade system transforms. The grade system transforms are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradeSystemTransforms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapGradeSystemTransformLookupSession
    extends net.okapia.osid.jamocha.grading.transform.spi.AbstractGradeSystemTransformLookupSession
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.grading.transform.GradeSystemTransform>());


    /**
     *  Makes a <code>GradeSystemTransform</code> available in this session.
     *
     *  @param  gradeSystemTransform a grade system transform
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransform<code>
     *          is <code>null</code>
     */

    protected void putGradeSystemTransform(org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        this.gradeSystemTransforms.put(gradeSystemTransform.getId(), gradeSystemTransform);
        return;
    }


    /**
     *  Makes an array of grade system transforms available in this session.
     *
     *  @param  gradeSystemTransforms an array of grade system transforms
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransforms<code>
     *          is <code>null</code>
     */

    protected void putGradeSystemTransforms(org.osid.grading.transform.GradeSystemTransform[] gradeSystemTransforms) {
        putGradeSystemTransforms(java.util.Arrays.asList(gradeSystemTransforms));
        return;
    }


    /**
     *  Makes a collection of grade system transforms available in this session.
     *
     *  @param  gradeSystemTransforms a collection of grade system transforms
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransforms<code>
     *          is <code>null</code>
     */

    protected void putGradeSystemTransforms(java.util.Collection<? extends org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms) {
        for (org.osid.grading.transform.GradeSystemTransform gradeSystemTransform : gradeSystemTransforms) {
            this.gradeSystemTransforms.put(gradeSystemTransform.getId(), gradeSystemTransform);
        }

        return;
    }


    /**
     *  Removes a GradeSystemTransform from this session.
     *
     *  @param  gradeSystemTransformId the <code>Id</code> of the grade system transform
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransformId<code> is
     *          <code>null</code>
     */

    protected void removeGradeSystemTransform(org.osid.id.Id gradeSystemTransformId) {
        this.gradeSystemTransforms.remove(gradeSystemTransformId);
        return;
    }


    /**
     *  Gets the <code>GradeSystemTransform</code> specified by its <code>Id</code>.
     *
     *  @param  gradeSystemTransformId <code>Id</code> of the <code>GradeSystemTransform</code>
     *  @return the gradeSystemTransform
     *  @throws org.osid.NotFoundException <code>gradeSystemTransformId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransformId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransform getGradeSystemTransform(org.osid.id.Id gradeSystemTransformId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.grading.transform.GradeSystemTransform gradeSystemTransform = this.gradeSystemTransforms.get(gradeSystemTransformId);
        if (gradeSystemTransform == null) {
            throw new org.osid.NotFoundException("gradeSystemTransform not found: " + gradeSystemTransformId);
        }

        return (gradeSystemTransform);
    }


    /**
     *  Gets all <code>GradeSystemTransforms</code>. In plenary mode, the returned
     *  list contains all known gradeSystemTransforms or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradeSystemTransforms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>GradeSystemTransforms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransforms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.transform.gradesystemtransform.ArrayGradeSystemTransformList(this.gradeSystemTransforms.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradeSystemTransforms.clear();
        super.close();
        return;
    }
}
