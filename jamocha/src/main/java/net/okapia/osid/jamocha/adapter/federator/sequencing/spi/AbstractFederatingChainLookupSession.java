//
// AbstractFederatingChainLookupSession.java
//
//     An abstract federating adapter for a ChainLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ChainLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingChainLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.sequencing.ChainLookupSession>
    implements org.osid.sequencing.ChainLookupSession {

    private boolean parallel = false;
    private org.osid.sequencing.Antimatroid antimatroid = new net.okapia.osid.jamocha.nil.sequencing.antimatroid.UnknownAntimatroid();


    /**
     *  Constructs a new <code>AbstractFederatingChainLookupSession</code>.
     */

    protected AbstractFederatingChainLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.sequencing.ChainLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Antimatroid/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Antimatroid Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAntimatroidId() {
        return (this.antimatroid.getId());
    }


    /**
     *  Gets the <code>Antimatroid</code> associated with this 
     *  session.
     *
     *  @return the <code>Antimatroid</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Antimatroid getAntimatroid()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.antimatroid);
    }


    /**
     *  Sets the <code>Antimatroid</code>.
     *
     *  @param  antimatroid the antimatroid for this session
     *  @throws org.osid.NullArgumentException <code>antimatroid</code>
     *          is <code>null</code>
     */

    protected void setAntimatroid(org.osid.sequencing.Antimatroid antimatroid) {
        nullarg(antimatroid, "antimatroid");
        this.antimatroid = antimatroid;
        return;
    }


    /**
     *  Tests if this user can perform <code>Chain</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupChains() {
        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            if (session.canLookupChains()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Chain</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeChainView() {
        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            session.useComparativeChainView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Chain</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryChainView() {
        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            session.usePlenaryChainView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include chains in antimatroids which are children
     *  of this antimatroid in the antimatroid hierarchy.
     */

    @OSID @Override
    public void useFederatedAntimatroidView() {
        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            session.useFederatedAntimatroidView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this antimatroid only.
     */

    @OSID @Override
    public void useIsolatedAntimatroidView() {
        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            session.useIsolatedAntimatroidView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Chain</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Chain</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Chain</code> and
     *  retained for compatibility.
     *
     *  @param  chainId <code>Id</code> of the
     *          <code>Chain</code>
     *  @return the chain
     *  @throws org.osid.NotFoundException <code>chainId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>chainId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Chain getChain(org.osid.id.Id chainId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            try {
                return (session.getChain(chainId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(chainId + " not found");
    }


    /**
     *  Gets a <code>ChainList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  chains specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Chains</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getChains()</code>.
     *
     *  @param  chainIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Chain</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>chainIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByIds(org.osid.id.IdList chainIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.sequencing.chain.MutableChainList ret = new net.okapia.osid.jamocha.sequencing.chain.MutableChainList();

        try (org.osid.id.IdList ids = chainIds) {
            while (ids.hasNext()) {
                ret.addChain(getChain(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ChainList</code> corresponding to the given
     *  chain genus <code>Type</code> which does not include
     *  chains of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getChains()</code>.
     *
     *  @param  chainGenusType a chain genus type 
     *  @return the returned <code>Chain</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>chainGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByGenusType(org.osid.type.Type chainGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.sequencing.chain.FederatingChainList ret = getChainList();

        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            ret.addChainList(session.getChainsByGenusType(chainGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ChainList</code> corresponding to the given
     *  chain genus <code>Type</code> and include any additional
     *  chains with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getChains()</code>.
     *
     *  @param  chainGenusType a chain genus type 
     *  @return the returned <code>Chain</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>chainGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByParentGenusType(org.osid.type.Type chainGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.sequencing.chain.FederatingChainList ret = getChainList();

        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            ret.addChainList(session.getChainsByParentGenusType(chainGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ChainList</code> containing the given
     *  chain record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getChains()</code>.
     *
     *  @param  chainRecordType a chain record type 
     *  @return the returned <code>Chain</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByRecordType(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.sequencing.chain.FederatingChainList ret = getChainList();

        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            ret.addChainList(session.getChainsByRecordType(chainRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Chains</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Chains</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChains()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.sequencing.chain.FederatingChainList ret = getChainList();

        for (org.osid.sequencing.ChainLookupSession session : getSessions()) {
            ret.addChainList(session.getChains());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.sequencing.chain.FederatingChainList getChainList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.sequencing.chain.ParallelChainList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.sequencing.chain.CompositeChainList());
        }
    }
}
