//
// AbstractProfileEntryEnablerLookupSession.java
//
//    A starter implementation framework for providing a ProfileEntryEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ProfileEntryEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProfileEntryEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProfileEntryEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.profile.rules.ProfileEntryEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.profile.Profile profile = new net.okapia.osid.jamocha.nil.profile.profile.UnknownProfile();
    

    /**
     *  Gets the <code>Profile/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Profile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.profile.getId());
    }


    /**
     *  Gets the <code>Profile</code> associated with this 
     *  session.
     *
     *  @return the <code>Profile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.profile);
    }


    /**
     *  Sets the <code>Profile</code>.
     *
     *  @param  profile the profile for this session
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>
     */

    protected void setProfile(org.osid.profile.Profile profile) {
        nullarg(profile, "profile");
        this.profile = profile;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProfileEntryEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProfileEntryEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>ProfileEntryEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileEntryEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ProfileEntryEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileEntryEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile entry enablers in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active profile entry enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveProfileEntryEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive profile entry enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusProfileEntryEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ProfileEntryEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProfileEntryEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProfileEntryEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, profile entry enablers are returned that are currently
     *  active. In any status mode, active and inactive profile entry enablers
     *  are returned.
     *
     *  @param  profileEntryEnablerId <code>Id</code> of the
     *          <code>ProfileEntryEnabler</code>
     *  @return the profile entry enabler
     *  @throws org.osid.NotFoundException <code>profileEntryEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>profileEntryEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnabler getProfileEntryEnabler(org.osid.id.Id profileEntryEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.profile.rules.ProfileEntryEnablerList profileEntryEnablers = getProfileEntryEnablers()) {
            while (profileEntryEnablers.hasNext()) {
                org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler = profileEntryEnablers.getNextProfileEntryEnabler();
                if (profileEntryEnabler.getId().equals(profileEntryEnablerId)) {
                    return (profileEntryEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(profileEntryEnablerId + " not found");
    }


    /**
     *  Gets a <code>ProfileEntryEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profileEntryEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProfileEntryEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, profile entry enablers are returned that are currently
     *  active. In any status mode, active and inactive profile entry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProfileEntryEnablers()</code>.
     *
     *  @param  profileEntryEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProfileEntryEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersByIds(org.osid.id.IdList profileEntryEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.profile.rules.ProfileEntryEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = profileEntryEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProfileEntryEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("profile entry enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.profile.rules.profileentryenabler.LinkedProfileEntryEnablerList(ret));
    }


    /**
     *  Gets a <code>ProfileEntryEnablerList</code> corresponding to the given
     *  profile entry enabler genus <code>Type</code> which does not include
     *  profile entry enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  profile entry enablers or an error results. Otherwise, the returned list
     *  may contain only those profile entry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, profile entry enablers are returned that are currently
     *  active. In any status mode, active and inactive profile entry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProfileEntryEnablers()</code>.
     *
     *  @param  profileEntryEnablerGenusType a profileEntryEnabler genus type 
     *  @return the returned <code>ProfileEntryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersByGenusType(org.osid.type.Type profileEntryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.rules.profileentryenabler.ProfileEntryEnablerGenusFilterList(getProfileEntryEnablers(), profileEntryEnablerGenusType));
    }


    /**
     *  Gets a <code>ProfileEntryEnablerList</code> corresponding to the given
     *  profile entry enabler genus <code>Type</code> and include any additional
     *  profile entry enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entry enablers or an error results. Otherwise, the returned list
     *  may contain only those profile entry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, profile entry enablers are returned that are currently
     *  active. In any status mode, active and inactive profile entry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProfileEntryEnablers()</code>.
     *
     *  @param  profileEntryEnablerGenusType a profileEntryEnabler genus type 
     *  @return the returned <code>ProfileEntryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersByParentGenusType(org.osid.type.Type profileEntryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProfileEntryEnablersByGenusType(profileEntryEnablerGenusType));
    }


    /**
     *  Gets a <code>ProfileEntryEnablerList</code> containing the given
     *  profile entry enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profile entry enablers or an error results. Otherwise, the returned list
     *  may contain only those profile entry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, profile entry enablers are returned that are currently
     *  active. In any status mode, active and inactive profile entry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProfileEntryEnablers()</code>.
     *
     *  @param  profileEntryEnablerRecordType a profileEntryEnabler record type 
     *  @return the returned <code>ProfileEntryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersByRecordType(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.rules.profileentryenabler.ProfileEntryEnablerRecordFilterList(getProfileEntryEnablers(), profileEntryEnablerRecordType));
    }


    /**
     *  Gets a <code>ProfileEntryEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  profile entry enablers or an error results. Otherwise, the returned list
     *  may contain only those profile entry enablers that are accessible
     *  through this session.
     *  
     *  In active mode, profile entry enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive profile entry enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProfileEntryEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.rules.profileentryenabler.TemporalProfileEntryEnablerFilterList(getProfileEntryEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>ProfileEntryEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entry enablers or an error results. Otherwise, the returned list
     *  may contain only those profile entry enablers that are accessible
     *  through this session.
     *
     *  In active mode, profile entry enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive profile entry enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ProfileEntryEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getProfileEntryEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>ProfileEntryEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  profile entry enablers or an error results. Otherwise, the returned list
     *  may contain only those profile entry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, profile entry enablers are returned that are currently
     *  active. In any status mode, active and inactive profile entry enablers
     *  are returned.
     *
     *  @return a list of <code>ProfileEntryEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the profile entry enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of profile entry enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.profile.rules.ProfileEntryEnablerList filterProfileEntryEnablersOnViews(org.osid.profile.rules.ProfileEntryEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.profile.rules.ProfileEntryEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.profile.rules.profileentryenabler.ActiveProfileEntryEnablerFilterList(ret);
        }

        return (ret);
    }
}
