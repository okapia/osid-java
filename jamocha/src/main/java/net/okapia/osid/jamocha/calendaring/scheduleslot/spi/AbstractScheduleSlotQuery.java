//
// AbstractScheduleSlotQuery.java
//
//     A template for making a ScheduleSlot Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for schedule slots.
 */

public abstract class AbstractScheduleSlotQuery    
    extends net.okapia.osid.jamocha.spi.AbstractContainableOsidObjectQuery
    implements org.osid.calendaring.ScheduleSlotQuery {

    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the schedule <code> Id </code> for this query for matching nested 
     *  schedule slots. 
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleSlotId(org.osid.id.Id scheduleSlotId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the schedule slot <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleSlotIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ScheduleSlotQuery </code> is available for querying 
     *  sechedule slots. 
     *
     *  @return <code> true </code> if a schedule slot query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedul slot. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the schedule slot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuery getScheduleSlotQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleSlotQuery() is false");
    }


    /**
     *  Matches a schedule that has any schedule slot assigned. 
     *
     *  @param  match <code> true </code> to match schedule with any schedule 
     *          slots, <code> false </code> to match schedules with no 
     *          schedule slots 
     */

    @OSID @Override
    public void matchAnyScheduleSlot(boolean match) {
        return;
    }


    /**
     *  Clears the schedule slot terms. 
     */

    @OSID @Override
    public void clearScheduleSlotTerms() {
        return;
    }


    /**
     *  Matches schedules that have the given weekday. 
     *
     *  @param  weekday a weekday 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchWeekday(long weekday, boolean match) {
        return;
    }


    /**
     *  Matches schedules with any weekday set. 
     *
     *  @param  match <code> true </code> to match schedules with any weekday, 
     *          <code> false </code> to match schedules with no weekday 
     */

    @OSID @Override
    public void matchAnyWeekday(boolean match) {
        return;
    }


    /**
     *  Clears the weekday terms. 
     */

    @OSID @Override
    public void clearWeekdayTerms() {
        return;
    }


    /**
     *  Matches schedules that have the given weekly interval in the given 
     *  range inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchWeeklyInterval(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches schedules with any weekly interval set. 
     *
     *  @param  match <code> true </code> to match schedules with any weekly 
     *          interval, <code> false </code> to match schedules with no 
     *          weekly interval 
     */

    @OSID @Override
    public void matchAnyWeeklyInterval(boolean match) {
        return;
    }


    /**
     *  Clears the weekly interval terms. 
     */

    @OSID @Override
    public void clearWeeklyIntervalTerms() {
        return;
    }


    /**
     *  Matches schedules that have a week of month in the given range 
     *  inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchWeekOfMonth(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches schedules with any month week set. 
     *
     *  @param  match <code> true </code> to match schedules with any week of 
     *          month, <code> false </code> to match schedules with no month 
     *          week 
     */

    @OSID @Override
    public void matchAnyWeekOfMonth(boolean match) {
        return;
    }


    /**
     *  Clears the week of month terms. 
     */

    @OSID @Override
    public void clearWeekOfMonthTerms() {
        return;
    }


    /**
     *  Matches schedules that have a weekday time in the given range 
     *  inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchWeekdayTime(org.osid.calendaring.Time from, 
                                 org.osid.calendaring.Time to, boolean match) {
        return;
    }


    /**
     *  Matches schedules with any weekday time. 
     *
     *  @param  match <code> true </code> to match schedules with any weekday 
     *          time, <code> false </code> to match schedules with no weekday 
     *          time 
     */

    @OSID @Override
    public void matchAnyWeekdayTime(boolean match) {
        return;
    }


    /**
     *  Clears the weekday time terms. 
     */

    @OSID @Override
    public void clearWeekdayTimeTerms() {
        return;
    }


    /**
     *  Matches schedules that have the given fixed interval in the given 
     *  range inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedInterval(org.osid.calendaring.Duration from, 
                                   org.osid.calendaring.Duration to, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches schedules with any fixed interval. 
     *
     *  @param  match <code> true </code> to match schedules with any fixed 
     *          interval, <code> false </code> to match schedules with no 
     *          fixed interval 
     */

    @OSID @Override
    public void matchAnyFixedInterval(boolean match) {
        return;
    }


    /**
     *  Clears the fixed interval terms. 
     */

    @OSID @Override
    public void clearFixedIntervalTerms() {
        return;
    }


    /**
     *  Matches the duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        return;
    }


    /**
     *  Matches a schedule slot that has any duration. 
     *
     *  @param  match <code> true </code> to match schedules with any 
     *          duration, <code> false </code> to match schedules with no 
     *          start time 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given schedule slot query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a schedule slot implementing the requested record.
     *
     *  @param scheduleSlotRecordType a schedule slot record type
     *  @return the schedule slot query record
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSlotRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotQueryRecord getScheduleSlotQueryRecord(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSlotQueryRecord record : this.records) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSlotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule slot query. 
     *
     *  @param scheduleSlotQueryRecord schedule slot query record
     *  @param scheduleSlotRecordType scheduleSlot record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addScheduleSlotQueryRecord(org.osid.calendaring.records.ScheduleSlotQueryRecord scheduleSlotQueryRecord, 
                                          org.osid.type.Type scheduleSlotRecordType) {

        addRecordType(scheduleSlotRecordType);
        nullarg(scheduleSlotQueryRecord, "schedule slot query record");
        this.records.add(scheduleSlotQueryRecord);        
        return;
    }
}
