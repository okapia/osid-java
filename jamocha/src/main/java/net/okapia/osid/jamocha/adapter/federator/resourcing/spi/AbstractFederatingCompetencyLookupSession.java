//
// AbstractFederatingCompetencyLookupSession.java
//
//     An abstract federating adapter for a CompetencyLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CompetencyLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCompetencyLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resourcing.CompetencyLookupSession>
    implements org.osid.resourcing.CompetencyLookupSession {

    private boolean parallel = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();


    /**
     *  Constructs a new <code>AbstractFederatingCompetencyLookupSession</code>.
     */

    protected AbstractFederatingCompetencyLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resourcing.CompetencyLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Competency</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCompetencies() {
        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            if (session.canLookupCompetencies()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Competency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCompetencyView() {
        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            session.useComparativeCompetencyView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Competency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCompetencyView() {
        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            session.usePlenaryCompetencyView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include competencies in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            session.useFederatedFoundryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            session.useIsolatedFoundryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Competency</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Competency</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Competency</code> and
     *  retained for compatibility.
     *
     *  @param  competencyId <code>Id</code> of the
     *          <code>Competency</code>
     *  @return the competency
     *  @throws org.osid.NotFoundException <code>competencyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>competencyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency(org.osid.id.Id competencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            try {
                return (session.getCompetency(competencyId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(competencyId + " not found");
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  competencies specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Competencies</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  @param  competencyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>competencyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByIds(org.osid.id.IdList competencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resourcing.competency.MutableCompetencyList ret = new net.okapia.osid.jamocha.resourcing.competency.MutableCompetencyList();

        try (org.osid.id.IdList ids = competencyIds) {
            while (ids.hasNext()) {
                ret.addCompetency(getCompetency(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  competency genus <code>Type</code> which does not include
     *  competencies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.competency.FederatingCompetencyList ret = getCompetencyList();

        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            ret.addCompetencyList(session.getCompetenciesByGenusType(competencyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  competency genus <code>Type</code> and include any additional
     *  competencies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByParentGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.competency.FederatingCompetencyList ret = getCompetencyList();

        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            ret.addCompetencyList(session.getCompetenciesByParentGenusType(competencyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CompetencyList</code> containing the given
     *  competency record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  @param  competencyRecordType a competency record type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByRecordType(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.competency.FederatingCompetencyList ret = getCompetencyList();

        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            ret.addCompetencyList(session.getCompetenciesByRecordType(competencyRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Competencies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Competencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.competency.FederatingCompetencyList ret = getCompetencyList();

        for (org.osid.resourcing.CompetencyLookupSession session : getSessions()) {
            ret.addCompetencyList(session.getCompetencies());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resourcing.competency.FederatingCompetencyList getCompetencyList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.competency.ParallelCompetencyList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.competency.CompositeCompetencyList());
        }
    }
}
