//
// AbstractInquirySearch.java
//
//     A template for making an Inquiry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing inquiry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractInquirySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inquiry.InquirySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.InquirySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inquiry.InquirySearchOrder inquirySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of inquiries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  inquiryIds list of inquiries
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongInquiries(org.osid.id.IdList inquiryIds) {
        while (inquiryIds.hasNext()) {
            try {
                this.ids.add(inquiryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongInquiries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of inquiry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getInquiryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  inquirySearchOrder inquiry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>inquirySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>inquirySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderInquiryResults(org.osid.inquiry.InquirySearchOrder inquirySearchOrder) {
	this.inquirySearchOrder = inquirySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inquiry.InquirySearchOrder getInquirySearchOrder() {
	return (this.inquirySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given inquiry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an inquiry implementing the requested record.
     *
     *  @param inquirySearchRecordType an inquiry search record
     *         type
     *  @return the inquiry search record
     *  @throws org.osid.NullArgumentException
     *          <code>inquirySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquirySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquirySearchRecord getInquirySearchRecord(org.osid.type.Type inquirySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inquiry.records.InquirySearchRecord record : this.records) {
            if (record.implementsRecordType(inquirySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquirySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inquiry search. 
     *
     *  @param inquirySearchRecord inquiry search record
     *  @param inquirySearchRecordType inquiry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInquirySearchRecord(org.osid.inquiry.records.InquirySearchRecord inquirySearchRecord, 
                                           org.osid.type.Type inquirySearchRecordType) {

        addRecordType(inquirySearchRecordType);
        this.records.add(inquirySearchRecord);        
        return;
    }
}
