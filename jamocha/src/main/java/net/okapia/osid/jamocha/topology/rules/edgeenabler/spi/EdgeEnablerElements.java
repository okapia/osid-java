//
// EdgeEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.edgeenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class EdgeEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the EdgeEnablerElement Id.
     *
     *  @return the edge enabler element Id
     */

    public static org.osid.id.Id getEdgeEnablerEntityId() {
        return (makeEntityId("osid.topology.rules.EdgeEnabler"));
    }


    /**
     *  Gets the RuledEdgeId element Id.
     *
     *  @return the RuledEdgeId element Id
     */

    public static org.osid.id.Id getRuledEdgeId() {
        return (makeQueryElementId("osid.topology.rules.edgeenabler.RuledEdgeId"));
    }


    /**
     *  Gets the RuledEdge element Id.
     *
     *  @return the RuledEdge element Id
     */

    public static org.osid.id.Id getRuledEdge() {
        return (makeQueryElementId("osid.topology.rules.edgeenabler.RuledEdge"));
    }


    /**
     *  Gets the GraphId element Id.
     *
     *  @return the GraphId element Id
     */

    public static org.osid.id.Id getGraphId() {
        return (makeQueryElementId("osid.topology.rules.edgeenabler.GraphId"));
    }


    /**
     *  Gets the Graph element Id.
     *
     *  @return the Graph element Id
     */

    public static org.osid.id.Id getGraph() {
        return (makeQueryElementId("osid.topology.rules.edgeenabler.Graph"));
    }
}
