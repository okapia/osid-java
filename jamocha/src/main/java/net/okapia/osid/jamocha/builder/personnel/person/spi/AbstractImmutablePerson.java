//
// AbstractImmutablePerson.java
//
//     Wraps a mutable Person to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.person.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Person</code> to hide modifiers. This
 *  wrapper provides an immutized Person from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying person whose state changes are visible.
 */

public abstract class AbstractImmutablePerson
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.personnel.Person {

    private final org.osid.personnel.Person person;


    /**
     *  Constructs a new <code>AbstractImmutablePerson</code>.
     *
     *  @param person the person to immutablize
     *  @throws org.osid.NullArgumentException <code>person</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePerson(org.osid.personnel.Person person) {
        super(person);
        this.person = person;
        return;
    }


    /**
     *  Gets the title for this person (Mr., Dr., Ms.). 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSalutation() {
        return (this.person.getSalutation());
    }


    /**
     *  Gets the given name of this person. 
     *
     *  @return the given name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getGivenName() {
        return (this.person.getGivenName());
    }


    /**
     *  Gets the preferred forename or mononym of this person. 
     *
     *  @return the preferred name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getPreferredName() {
        return (this.person.getPreferredName());
    }


    /**
     *  Gets additional forenames this person is or was known by. 
     *
     *  @return forname aliases 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getForenameAliases() {
        return (this.person.getForenameAliases());
    }


    /**
     *  Gets the middle names of this person. 
     *
     *  @return the middle names 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getMiddleNames() {
        return (this.person.getMiddleNames());
    }


    /**
     *  Gets the surname of this person. 
     *
     *  @return the surname 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSurname() {
        return (this.person.getSurname());
    }


    /**
     *  Gets additional surnames this person is or was known by. 
     *
     *  @return the surname aliases 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getSurnameAliases() {
        return (this.person.getSurnameAliases());
    }


    /**
     *  Gets the generation qualifier of this person. 
     *
     *  @return the generation qualifier 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getGenerationQualifier() {
        return (this.person.getGenerationQualifier());
    }


    /**
     *  Gets the qualification suffix of this person (MD, Phd). 
     *
     *  @return the generation qualifier 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getQualificationSuffix() {
        return (this.person.getQualificationSuffix());
    }


    /**
     *  Tests if a birth date is available. 
     *
     *  @return <code> true </code> if a birth date is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasBirthDate() {
        return (this.person.hasBirthDate());
    }


    /**
     *  Gets the date of birth for this person. 
     *
     *  @return the date of birth 
     *  @throws org.osid.IllegalStateException <code> hasBirthDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getBirthDate() {
        return (this.person.getBirthDate());
    }


    /**
     *  Tests if this person died. 
     *
     *  @return <code> true </code> if this person is dead, <code> false 
     *          </code> if still kicking 
     */

    @OSID @Override
    public boolean isDeceased() {
        return (this.person.isDeceased());
    }


    /**
     *  Gets the date of death for this person. 
     *
     *  @return the date of death 
     *  @throws org.osid.IllegalStateException <code> isDead() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDeathDate() {
        return (this.person.getDeathDate());
    }


    /**
     *  Gets the institutional identifier for this person. 
     *
     *  @return the institutional identifier 
     */

    @OSID @Override
    public String getInstitutionalIdentifier() {
        return (this.person.getInstitutionalIdentifier());
    }


    /**
     *  Gets the record corresponding to the given <code> Person </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> personRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(personRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  personRecordType the type of person record to retrieve 
     *  @return the person record 
     *  @throws org.osid.NullArgumentException <code> personRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(personRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.records.PersonRecord getPersonRecord(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException {

        return (this.person.getPersonRecord(personRecordType));
    }
}

