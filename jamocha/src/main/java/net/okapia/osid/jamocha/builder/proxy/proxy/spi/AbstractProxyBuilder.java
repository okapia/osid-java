//
// AbstractProxy.java
//
//     Defines a Proxy builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.proxy.proxy.spi;


/**
 *  Defines a <code>Proxy</code> builder.
 */

public abstract class AbstractProxyBuilder<T extends AbstractProxyBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidResultBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.proxy.proxy.ProxyMiter proxy;


    /**
     *  Constructs a new <code>AbstractProxyBuilder</code>.
     *
     *  @param proxy the proxy to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProxyBuilder(net.okapia.osid.jamocha.builder.proxy.proxy.ProxyMiter proxy) {
        super(proxy);
        this.proxy = proxy;
        return;
    }


    /**
     *  Builds the proxy.
     *
     *  @return the new proxy
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.proxy.Proxy build() {
        (new net.okapia.osid.jamocha.builder.validator.proxy.proxy.ProxyValidator(getValidations())).validate(this.proxy);
        return (new net.okapia.osid.jamocha.builder.proxy.proxy.ImmutableProxy(this.proxy));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the proxy miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.proxy.proxy.ProxyMiter getMiter() {
        return (this.proxy);
    }

    
    /**
     *  Sets the authentication.
     *
     *  @param authentication the authentication
     *  @throws org.osid.NullArgumentException
     *          <code>authentication</code> is <code>null</code.
     *  @return the builder
     */

    public T authentication(org.osid.authentication.process.Authentication authentication) {
        getMiter().setAuthentication(authentication);
        return (self());
    }


    /**
     *  Sets the effective agent.
     *
     *  @param agent the agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code.
     */

    public T effectiveAgent(org.osid.authentication.Agent agent) {
        getMiter().setEffectiveAgent(agent);
        return (self());
    }
    

    /**
     *  Sets the effective date. 
     *
     *  @param  date a date 
     *  @param  rate the rate at which the clock should tick from the given 
     *          effective date. 0 is a clock that is fixed 
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    public T effectiveDate(java.util.Date date, java.math.BigDecimal rate) {
        getMiter().setEffectiveDate(date, rate);
        return (self());
    }

    
    /**
     *  Sets the locale.
     *
     *  @param locale the locale
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>locale</code> is
     *          <code>null</code.
     */
    
    public T locale(org.osid.locale.Locale locale) {
        getMiter().setLocale(locale);
        return (self());
    }        


    /**
     *  Sets the <code> DisplayText </code> format <code>
     *  Type. </code>
     *
     *  @param formatType the format <code> Type </code>
     *  @throws org.osid.NullArgumentException <code>formatType</code>
     *          is <code>null</code>
     */

    public T formatType(org.osid.type.Type formatType) {
        getMiter().setFormatType(formatType);
        return (self());
    }
}       


