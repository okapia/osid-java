//
// AbstractStatisticSearchOdrer.java
//
//     Defines a StatisticSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.statistic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code StatisticSearchOrder}.
 */

public abstract class AbstractStatisticSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendiumSearchOrder
    implements org.osid.metering.StatisticSearchOrder {

    private final java.util.Collection<org.osid.metering.records.StatisticSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the metered object. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMeter(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> MeterSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a meter search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a meter. 
     *
     *  @return the meter search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchOrder getMeterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsMeterSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the metered object. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMeteredObject(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the sum. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySum(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the mean. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMean(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the median. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMedian(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the mode. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the standard deviation. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStandardDeviation(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the root mean square. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRMS(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the delta. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDelta(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the percent change. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPercentChange(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the average rate. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAverageRate(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  statisticRecordType a statistic record type 
     *  @return {@code true} if the statisticRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code statisticRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type statisticRecordType) {
        for (org.osid.metering.records.StatisticSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  statisticRecordType the statistic record type 
     *  @return the statistic search order record
     *  @throws org.osid.NullArgumentException
     *          {@code statisticRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(statisticRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.metering.records.StatisticSearchOrderRecord getStatisticSearchOrderRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.StatisticSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(statisticRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this statistic. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param statisticRecord the statistic search odrer record
     *  @param statisticRecordType statistic record type
     *  @throws org.osid.NullArgumentException
     *          {@code statisticRecord} or
     *          {@code statisticRecordTypestatistic} is
     *          {@code null}
     */
            
    protected void addStatisticRecord(org.osid.metering.records.StatisticSearchOrderRecord statisticSearchOrderRecord, 
                                     org.osid.type.Type statisticRecordType) {

        addRecordType(statisticRecordType);
        this.records.add(statisticSearchOrderRecord);
        
        return;
    }
}
