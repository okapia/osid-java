//
// AbstractOublietteSearch.java
//
//     A template for making an Oubliette Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.oubliette.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing oubliette searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOublietteSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.hold.OublietteSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.hold.records.OublietteSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.hold.OublietteSearchOrder oublietteSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of oubliettes. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  oublietteIds list of oubliettes
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOubliettes(org.osid.id.IdList oublietteIds) {
        while (oublietteIds.hasNext()) {
            try {
                this.ids.add(oublietteIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOubliettes</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of oubliette Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOublietteIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  oublietteSearchOrder oubliette search order 
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>oublietteSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOublietteResults(org.osid.hold.OublietteSearchOrder oublietteSearchOrder) {
	this.oublietteSearchOrder = oublietteSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.hold.OublietteSearchOrder getOublietteSearchOrder() {
	return (this.oublietteSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given oubliette search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an oubliette implementing the requested record.
     *
     *  @param oublietteSearchRecordType an oubliette search record
     *         type
     *  @return the oubliette search record
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(oublietteSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.OublietteSearchRecord getOublietteSearchRecord(org.osid.type.Type oublietteSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.hold.records.OublietteSearchRecord record : this.records) {
            if (record.implementsRecordType(oublietteSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(oublietteSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this oubliette search. 
     *
     *  @param oublietteSearchRecord oubliette search record
     *  @param oublietteSearchRecordType oubliette search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOublietteSearchRecord(org.osid.hold.records.OublietteSearchRecord oublietteSearchRecord, 
                                           org.osid.type.Type oublietteSearchRecordType) {

        addRecordType(oublietteSearchRecordType);
        this.records.add(oublietteSearchRecord);        
        return;
    }
}
