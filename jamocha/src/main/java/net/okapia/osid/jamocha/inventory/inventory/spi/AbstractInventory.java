//
// AbstractInventory.java
//
//     Defines an Inventory.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Inventory</code>.
 */

public abstract class AbstractInventory
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.inventory.Inventory {

    private org.osid.inventory.Stock stock;
    private org.osid.calendaring.DateTime date;
    private java.math.BigDecimal quantity;

    private final java.util.Collection<org.osid.inventory.records.InventoryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Stock. </code> 
     *
     *  @return the stock <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        return (this.stock.getId());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Stock. </code> 
     *
     *  @return the stock 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        return (this.stock);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @throws org.osid.NullArgumentException
     *          <code>stock</code> is <code>null</code>
     */

    protected void setStock(org.osid.inventory.Stock stock) {
        nullarg(stock, "stock");
        this.stock = stock;
        return;
    }


    /**
     *  Gets the inventory date. 
     *
     *  @return the inventory date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.date);
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.date = date;
        return;
    }


    /**
     *  Gets the quantity of the items in the stock. 
     *
     *  @return the quantity 
     */

    @OSID @Override
    public java.math.BigDecimal getQuantity() {
        return (this.quantity);
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.NullArgumentException
     *          <code>quantity</code> is <code>null</code>
     */

    protected void setQuantity(java.math.BigDecimal quantity) {
        nullarg(quantity, "quantity");
        this.quantity = quantity;
        return;
    }


    /**
     *  Tests if this inventory supports the given record
     *  <code>Type</code>.
     *
     *  @param  inventoryRecordType an inventory record type 
     *  @return <code>true</code> if the inventoryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inventoryRecordType) {
        for (org.osid.inventory.records.InventoryRecord record : this.records) {
            if (record.implementsRecordType(inventoryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Inventory</code> record <code>Type</code>.
     *
     *  @param  inventoryRecordType the inventory record type 
     *  @return the inventory record 
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inventoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.InventoryRecord getInventoryRecord(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.InventoryRecord record : this.records) {
            if (record.implementsRecordType(inventoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inventoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inventory. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inventoryRecord the inventory record
     *  @param inventoryRecordType inventory record type
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecord</code> or
     *          <code>inventoryRecordTypeinventory</code> is
     *          <code>null</code>
     */
            
    protected void addInventoryRecord(org.osid.inventory.records.InventoryRecord inventoryRecord, 
                                      org.osid.type.Type inventoryRecordType) {

        nullarg(inventoryRecord, "inventory record");
        addRecordType(inventoryRecordType);
        this.records.add(inventoryRecord);
        
        return;
    }
}
