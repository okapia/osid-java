//
// RealmNodeToIdList.java
//
//     Implements a RealmNode IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.personnel.realmnode;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying RealmNodeList to a
 *  list of Ids.
 */

public final class RealmNodeToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.personnel.RealmNodeList realmNodeList;


    /**
     *  Creates a new {@code RealmNodeToIdList}.
     *
     *  @param realmNodeList a {@code RealmNodeList}
     *  @throws org.osid.NullArgumentException {@code realmNodeList}
     *          is {@code null}
     */

    public RealmNodeToIdList(org.osid.personnel.RealmNodeList realmNodeList) {
        super(realmNodeList);
        this.realmNodeList = realmNodeList;
        return;
    }


    /**
     *  Creates a new {@code RealmNodeToIdList}.
     *
     *  @param realmNodes a collection of RealmNodes
     *  @throws org.osid.NullArgumentException {@code realmNodes}
     *          is {@code null}
     */

    public RealmNodeToIdList(java.util.Collection<org.osid.personnel.RealmNode> realmNodes) {
        this(new net.okapia.osid.jamocha.personnel.realmnode.ArrayRealmNodeList(realmNodes)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.realmNodeList.getNextRealmNode().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.realmNodeList.close();
        return;
    }
}
