//
// AbstractResultQuery.java
//
//     A template for making a Result Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.result.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for results.
 */

public abstract class AbstractResultQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.offering.ResultQuery {

    private final java.util.Collection<org.osid.offering.records.ResultQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a participant <code> Id. </code> 
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> participantId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParticipantId(org.osid.id.Id participantId, boolean match) {
        return;
    }


    /**
     *  Clears all participant <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParticipantIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ParticipantUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a participant query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantQuery() {
        return (false);
    }


    /**
     *  Gets the query for a participant query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the participant query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuery getParticipantQuery() {
        throw new org.osid.UnimplementedException("supportsParticipantQuery() is false");
    }


    /**
     *  Clears all participant terms. 
     */

    @OSID @Override
    public void clearParticipantTerms() {
        return;
    }


    /**
     *  Sets a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears all grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradetQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches results with any grade. 
     *
     *  @param  match <code> true </code> to match results with any grade, 
     *          <code> false </code> to match results with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        return;
    }


    /**
     *  Clears all grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        return;
    }


    /**
     *  Matches a value between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchValue(java.math.BigDecimal from, java.math.BigDecimal to, 
                           boolean match) {
        return;
    }


    /**
     *  Matches results with any value. 
     *
     *  @param  match <code> true </code> to match results with any value, 
     *          <code> false </code> to match results with no value 
     */

    @OSID @Override
    public void matchAnyValue(boolean match) {
        return;
    }


    /**
     *  Clears all value terms. 
     */

    @OSID @Override
    public void clearValueTerms() {
        return;
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match results 
     *  assigned to catalogues. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears all catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears all catalogue terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given result query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a result implementing the requested record.
     *
     *  @param resultRecordType a result record type
     *  @return the result query record
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resultRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ResultQueryRecord getResultQueryRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ResultQueryRecord record : this.records) {
            if (record.implementsRecordType(resultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resultRecordType + " is not supported");
    }


    /**
     *  Adds a record to this result query. 
     *
     *  @param resultQueryRecord result query record
     *  @param resultRecordType result record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResultQueryRecord(org.osid.offering.records.ResultQueryRecord resultQueryRecord, 
                                          org.osid.type.Type resultRecordType) {

        addRecordType(resultRecordType);
        nullarg(resultQueryRecord, "result query record");
        this.records.add(resultQueryRecord);        
        return;
    }
}
