//
// AbstractQueryBallotConstrainerLookupSession.java
//
//    An inline adapter that maps a BallotConstrainerLookupSession to
//    a BallotConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BallotConstrainerLookupSession to
 *  a BallotConstrainerQuerySession.
 */

public abstract class AbstractQueryBallotConstrainerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractBallotConstrainerLookupSession
    implements org.osid.voting.rules.BallotConstrainerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.voting.rules.BallotConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBallotConstrainerLookupSession.
     *
     *  @param querySession the underlying ballot constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBallotConstrainerLookupSession(org.osid.voting.rules.BallotConstrainerQuerySession querySession) {
        nullarg(querySession, "ballot constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this session.
     *
     *  @return the <code>Polls</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform <code>BallotConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBallotConstrainers() {
        return (this.session.canSearchBallotConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active ballot constrainers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveBallotConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive ballot constrainers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BallotConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BallotConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>BallotConstrainer</code> and retained for compatibility.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerId <code>Id</code> of the
     *          <code>BallotConstrainer</code>
     *  @return the ballot constrainer
     *  @throws org.osid.NotFoundException <code>ballotConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainer getBallotConstrainer(org.osid.id.Id ballotConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerQuery query = getQuery();
        query.matchId(ballotConstrainerId, true);
        org.osid.voting.rules.BallotConstrainerList ballotConstrainers = this.session.getBallotConstrainersByQuery(query);
        if (ballotConstrainers.hasNext()) {
            return (ballotConstrainers.getNextBallotConstrainer());
        } 
        
        throw new org.osid.NotFoundException(ballotConstrainerId + " not found");
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  ballotConstrainers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>BallotConstrainers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByIds(org.osid.id.IdList ballotConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = ballotConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBallotConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the
     *  given ballot constrainer genus <code>Type</code> which does
     *  not include ballot constrainers of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerQuery query = getQuery();
        query.matchGenusType(ballotConstrainerGenusType, true);
        return (this.session.getBallotConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the
     *  given ballot constrainer genus <code>Type</code> and include
     *  any additional ballot constrainers with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByParentGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerQuery query = getQuery();
        query.matchParentGenusType(ballotConstrainerGenusType, true);
        return (this.session.getBallotConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> containing the given
     *  ballot constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known ballot
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerRecordType a ballotConstrainer record type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByRecordType(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerQuery query = getQuery();
        query.matchRecordType(ballotConstrainerRecordType, true);
        return (this.session.getBallotConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>BallotConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @return a list of <code>BallotConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBallotConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.rules.BallotConstrainerQuery getQuery() {
        org.osid.voting.rules.BallotConstrainerQuery query = this.session.getBallotConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
