//
// MutableIndexedMapGradebookColumnCalculationLookupSession
//
//    Implements a GradebookColumnCalculation lookup service backed by a collection of
//    gradebookColumnCalculations indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.calculation;


/**
 *  Implements a GradebookColumnCalculation lookup service backed by a collection of
 *  gradebook column calculations. The gradebook column calculations are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some gradebook column calculations may be compatible
 *  with more types than are indicated through these gradebook column calculation
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of gradebook column calculations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapGradebookColumnCalculationLookupSession
    extends net.okapia.osid.jamocha.core.grading.calculation.spi.AbstractIndexedMapGradebookColumnCalculationLookupSession
    implements org.osid.grading.calculation.GradebookColumnCalculationLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapGradebookColumnCalculationLookupSession} with no gradebook column calculations.
     *
     *  @param gradebook the gradebook
     *  @throws org.osid.NullArgumentException {@code gradebook}
     *          is {@code null}
     */

      public MutableIndexedMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook) {
        setGradebook(gradebook);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGradebookColumnCalculationLookupSession} with a
     *  single gradebook column calculation.
     *  
     *  @param gradebook the gradebook
     *  @param  gradebookColumnCalculation a single gradebookColumnCalculation
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumnCalculation} is {@code null}
     */

    public MutableIndexedMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {
        this(gradebook);
        putGradebookColumnCalculation(gradebookColumnCalculation);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGradebookColumnCalculationLookupSession} using an
     *  array of gradebook column calculations.
     *
     *  @param gradebook the gradebook
     *  @param  gradebookColumnCalculations an array of gradebook column calculations
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumnCalculations} is {@code null}
     */

    public MutableIndexedMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.grading.calculation.GradebookColumnCalculation[] gradebookColumnCalculations) {
        this(gradebook);
        putGradebookColumnCalculations(gradebookColumnCalculations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGradebookColumnCalculationLookupSession} using a
     *  collection of gradebook column calculations.
     *
     *  @param gradebook the gradebook
     *  @param  gradebookColumnCalculations a collection of gradebook column calculations
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumnCalculations} is {@code null}
     */

    public MutableIndexedMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook,
                                                  java.util.Collection<? extends org.osid.grading.calculation.GradebookColumnCalculation> gradebookColumnCalculations) {

        this(gradebook);
        putGradebookColumnCalculations(gradebookColumnCalculations);
        return;
    }
    

    /**
     *  Makes a {@code GradebookColumnCalculation} available in this session.
     *
     *  @param  gradebookColumnCalculation a gradebook column calculation
     *  @throws org.osid.NullArgumentException {@code gradebookColumnCalculation{@code  is
     *          {@code null}
     */

    @Override
    public void putGradebookColumnCalculation(org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {
        super.putGradebookColumnCalculation(gradebookColumnCalculation);
        return;
    }


    /**
     *  Makes an array of gradebook column calculations available in this session.
     *
     *  @param  gradebookColumnCalculations an array of gradebook column calculations
     *  @throws org.osid.NullArgumentException {@code gradebookColumnCalculations{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebookColumnCalculations(org.osid.grading.calculation.GradebookColumnCalculation[] gradebookColumnCalculations) {
        super.putGradebookColumnCalculations(gradebookColumnCalculations);
        return;
    }


    /**
     *  Makes collection of gradebook column calculations available in this session.
     *
     *  @param  gradebookColumnCalculations a collection of gradebook column calculations
     *  @throws org.osid.NullArgumentException {@code gradebookColumnCalculation{@code  is
     *          {@code null}
     */

    @Override
    public void putGradebookColumnCalculations(java.util.Collection<? extends org.osid.grading.calculation.GradebookColumnCalculation> gradebookColumnCalculations) {
        super.putGradebookColumnCalculations(gradebookColumnCalculations);
        return;
    }


    /**
     *  Removes a GradebookColumnCalculation from this session.
     *
     *  @param gradebookColumnCalculationId the {@code Id} of the gradebook column calculation
     *  @throws org.osid.NullArgumentException {@code gradebookColumnCalculationId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId) {
        super.removeGradebookColumnCalculation(gradebookColumnCalculationId);
        return;
    }    
}
