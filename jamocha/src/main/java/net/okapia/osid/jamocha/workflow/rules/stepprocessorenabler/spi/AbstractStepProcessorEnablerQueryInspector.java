//
// AbstractStepProcessorEnablerQueryInspector.java
//
//     A template for making a StepProcessorEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for step processor enablers.
 */

public abstract class AbstractStepProcessorEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.workflow.rules.StepProcessorEnablerQueryInspector {

    private final java.util.Collection<org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the step processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledStepProcessorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the step processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQueryInspector[] getRuledStepProcessorTerms() {
        return (new org.osid.workflow.rules.StepProcessorQueryInspector[0]);
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given step processor enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a step processor enabler implementing the requested record.
     *
     *  @param stepProcessorEnablerRecordType a step processor enabler record type
     *  @return the step processor enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord getStepProcessorEnablerQueryInspectorRecord(org.osid.type.Type stepProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(stepProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step processor enabler query. 
     *
     *  @param stepProcessorEnablerQueryInspectorRecord step processor enabler query inspector
     *         record
     *  @param stepProcessorEnablerRecordType stepProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepProcessorEnablerQueryInspectorRecord(org.osid.workflow.rules.records.StepProcessorEnablerQueryInspectorRecord stepProcessorEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type stepProcessorEnablerRecordType) {

        addRecordType(stepProcessorEnablerRecordType);
        nullarg(stepProcessorEnablerRecordType, "step processor enabler record type");
        this.records.add(stepProcessorEnablerQueryInspectorRecord);        
        return;
    }
}
