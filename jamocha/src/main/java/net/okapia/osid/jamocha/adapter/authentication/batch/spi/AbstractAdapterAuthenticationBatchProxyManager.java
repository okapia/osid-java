//
// AbstractAuthenticationBatchProxyManager.java
//
//     An adapter for a AuthenticationBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AuthenticationBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAuthenticationBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.authentication.batch.AuthenticationBatchProxyManager>
    implements org.osid.authentication.batch.AuthenticationBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAuthenticationBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAuthenticationBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of agents is available. 
     *
     *  @return <code> true </code> if an agent bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentBatchAdmin() {
        return (getAdapteeManager().supportsAgentBatchAdmin());
    }


    /**
     *  Tests if bulk administration of agencies is available. 
     *
     *  @return <code> true </code> if an agency bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyBatchAdmin() {
        return (getAdapteeManager().supportsAgencyBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgentBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the <code> Agency </code> 
     *  @param  proxy a proxy 
     *  @return a <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Agency </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSessionForAgency(org.osid.id.Id agencyId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgentBatchAdminSessionForAgency(agencyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agency 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AgencyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgencyBatchAdminSession getAgencyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgencyBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
