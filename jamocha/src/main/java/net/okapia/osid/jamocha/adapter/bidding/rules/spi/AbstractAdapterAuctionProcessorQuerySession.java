//
// AbstractQueryAuctionProcessorLookupSession.java
//
//    An AuctionProcessorQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuctionProcessorQuerySession adapter.
 */

public abstract class AbstractAdapterAuctionProcessorQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.rules.AuctionProcessorQuerySession {

    private final org.osid.bidding.rules.AuctionProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAuctionProcessorQuerySession.
     *
     *  @param session the underlying auction processor query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionProcessorQuerySession(org.osid.bidding.rules.AuctionProcessorQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeAuctionHouse</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeAuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the {@codeAuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the {@codeAuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform {@codeAuctionProcessor</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAuctionProcessors() {
        return (this.session.canSearchAuctionProcessors());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processors in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this auction house only.
     */
    
    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    
      
    /**
     *  Gets an auction processor query. The returned query will not have an
     *  extension query.
     *
     *  @return the auction processor query 
     */
      
    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuery getAuctionProcessorQuery() {
        return (this.session.getAuctionProcessorQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  auctionProcessorQuery the auction processor query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code auctionProcessorQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code auctionProcessorQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByQuery(org.osid.bidding.rules.AuctionProcessorQuery auctionProcessorQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAuctionProcessorsByQuery(auctionProcessorQuery));
    }
}
