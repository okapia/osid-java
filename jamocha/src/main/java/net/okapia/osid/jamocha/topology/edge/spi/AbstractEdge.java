//
// AbstractEdge.java
//
//     Defines an Edge.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.edge.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Edge</code>.
 */

public abstract class AbstractEdge
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.topology.Edge {

    private boolean directional = false;
    private boolean biDirectional = false;
    private java.math.BigDecimal cost = new java.math.BigDecimal(1);
    private java.math.BigDecimal distance = new java.math.BigDecimal(1);
    private org.osid.topology.Node sourceNode;
    private org.osid.topology.Node destinationNode;

    private final java.util.Collection<org.osid.topology.records.EdgeRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this is a directional edge.
     *
     *  @return <code> true </code> if this edge is directional
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isDirectional() {
        return (this.directional);
    }


    /**
     *  Sets the directional flag.
     *
     *  @param directional <code>true</code> if this is a directional
     *         edge, <code>false</code> if non-directional
     */

    protected void setDirectional(boolean directional) {
        this.directional = directional;
        return;
    }


    /**
     *  Tests if this directional edge is bi-directional. 
     *
     *  @return <code> true </code> if this edge is directional and 
     *          bi-directional, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isBiDirectional() {
        return (this.biDirectional);
    }


    /**
     *  Sets the bidirectional flag.
     *
     *  @param biDirectional <code>true</code> if this is a
     *         bidirectional edge, <code>false</code> if a
     *         unidirectional edge
     */

    protected void setBiDirectional(boolean biDirectional) {
        this.biDirectional = biDirectional;
        return;
    }


    /**
     *  Gets the cost of this edge. 
     *
     *  @return a number representing the cost of this edge 
     */

    @OSID @Override
    public java.math.BigDecimal getCost() {
        return (this.cost);
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    protected void setCost(java.math.BigDecimal cost) {
        nullarg(cost, "cost");
        this.cost = cost;
        return;
    }


    /**
     *  Gets the distance of this edge. 
     *
     *  @return a number representing the distance of this edge 
     */

    @OSID @Override
    public java.math.BigDecimal getDistance() {
        return (this.distance);
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException
     *          <code>distance</code> is <code>null</code>
     */

    protected void setDistance(java.math.BigDecimal distance) {
        nullarg(distance, "distance");
        this.distance = distance;
        return;
    }


    /**
     *  Gets the source node of this edge. If the edge is
     *  uni-directional, the source node is the node at the beginning
     *  of the edge, otherwise it may be relative to the means of
     *  traversal of the graph.
     *
     *  @return the source node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceNodeId() {
        return (this.sourceNode.getId());
    }


    /**
     *  Gets the <code> Id </code> of the source node of this edge. If
     *  the edge is uni-directional, the source node is the node at
     *  the beginning of the edge, otherwise it may be relative to the
     *  means of traversal of the graph.
     *
     *  @return the source node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getSourceNode()
        throws org.osid.OperationFailedException {

        return (this.sourceNode);
    }


    /**
     *  Sets the source node.
     *
     *  @param sourceNode a source node
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNode</code> is <code>null</code>
     */

    protected void setSourceNode(org.osid.topology.Node sourceNode) {
        nullarg(sourceNode, "source node");
        this.sourceNode = sourceNode;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the destination node of this
     *  edge. If the edge is uni-directional, the destination node is
     *  the node at the end of the edge, otherwise it may be relative
     *  to the means of traversal of the graph.
     *
     *  @return the detsination node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDestinationNodeId() {
        return (this.destinationNode.getId());
    }


    /**
     *  Gets the destination node of this edge. If the edge is
     *  uni-directional, the destination node is the node at the end
     *  of the edge, otherwise it may be relative to the means of
     *  traversal of the graph.
     *
     *  @return the detsination node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getDestinationNode()
        throws org.osid.OperationFailedException {

        return (this.destinationNode);
    }


    /**
     *  Sets the destination node.
     *
     *  @param destinationNode a destination node
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNode</code> is <code>null</code>
     */

    protected void setDestinationNode(org.osid.topology.Node destinationNode) {
        nullarg(destinationNode, "destination node");
        this.destinationNode = destinationNode;
        return;
    }


    /**
     *  Tests if this edge supports the given record
     *  <code>Type</code>.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return <code>true</code> if the edgeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type edgeRecordType) {
        for (org.osid.topology.records.EdgeRecord record : this.records) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Edge</code>
     *  record <code>Type</code>.
     *
     *  @param  edgeRecordType the edge record type 
     *  @return the edge record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.EdgeRecord getEdgeRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.EdgeRecord record : this.records) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this edge. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param edgeRecord the edge record
     *  @param edgeRecordType edge record type
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecord</code> or
     *          <code>edgeRecordTypeedge</code> is
     *          <code>null</code>
     */
            
    protected void addEdgeRecord(org.osid.topology.records.EdgeRecord edgeRecord, 
                                 org.osid.type.Type edgeRecordType) {

        nullarg(edgeRecord, "edge record");
        addRecordType(edgeRecordType);
        this.records.add(edgeRecord);
        
        return;
    }
}
