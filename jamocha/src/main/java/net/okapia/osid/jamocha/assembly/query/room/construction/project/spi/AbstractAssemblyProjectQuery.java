//
// AbstractAssemblyProjectQuery.java
//
//     A ProjectQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProjectQuery that stores terms.
 */

public abstract class AbstractAssemblyProjectQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.room.construction.ProjectQuery,
               org.osid.room.construction.ProjectQueryInspector,
               org.osid.room.construction.ProjectSearchOrder {

    private final java.util.Collection<org.osid.room.construction.records.ProjectQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.construction.records.ProjectQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.construction.records.ProjectSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProjectQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProjectQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a building <code> Id. </code> 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        getAssembler().addIdTerm(getBuildingIdColumn(), buildingId, match);
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        getAssembler().clearTerms(getBuildingIdColumn());
        return;
    }


    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (getAssembler().getIdTerms(getBuildingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the building. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuilding(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBuildingColumn(), style);
        return;
    }


    /**
     *  Gets the BuildingId column name.
     *
     * @return the column name
     */

    protected String getBuildingIdColumn() {
        return ("building_id");
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        getAssembler().clearTerms(getBuildingColumn());
        return;
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Tests if a building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBuildingSearchOrder() is false");
    }


    /**
     *  Gets the Building column name.
     *
     * @return the column name
     */

    protected String getBuildingColumn() {
        return ("building");
    }


    /**
     *  Matches a cost within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCost(org.osid.financials.Currency low, 
                          org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getCostColumn(), low, high, match);
        return;
    }


    /**
     *  Matches any cost. 
     *
     *  @param  match <code> true </code> to match projects with any cost 
     *          assigned, <code> false </code> to match buildings with no cost 
     *          assigned 
     */

    @OSID @Override
    public void matchAnyCost(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getCostColumn(), match);
        return;
    }


    /**
     *  Clears the cost terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        getAssembler().clearTerms(getCostColumn());
        return;
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (getAssembler().getCurrencyRangeTerms(getCostColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCostColumn(), style);
        return;
    }


    /**
     *  Gets the Cost column name.
     *
     * @return the column name
     */

    protected String getCostColumn() {
        return ("cost");
    }


    /**
     *  Sets the project <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        getAssembler().clearTerms(getCampusIdColumn());
        return;
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (getAssembler().getIdTerms(getCampusIdColumn()));
    }


    /**
     *  Gets the CampusId column name.
     *
     * @return the column name
     */

    protected String getCampusIdColumn() {
        return ("campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        getAssembler().clearTerms(getCampusColumn());
        return;
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the Campus column name.
     *
     * @return the column name
     */

    protected String getCampusColumn() {
        return ("campus");
    }


    /**
     *  Tests if this project supports the given record
     *  <code>Type</code>.
     *
     *  @param  projectRecordType a project record type 
     *  @return <code>true</code> if the projectRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type projectRecordType) {
        for (org.osid.room.construction.records.ProjectQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(projectRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  projectRecordType the project record type 
     *  @return the project query record 
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(projectRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectQueryRecord getProjectQueryRecord(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.ProjectQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(projectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(projectRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  projectRecordType the project record type 
     *  @return the project query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(projectRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectQueryInspectorRecord getProjectQueryInspectorRecord(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.ProjectQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(projectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(projectRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param projectRecordType the project record type
     *  @return the project search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(projectRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectSearchOrderRecord getProjectSearchOrderRecord(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.ProjectSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(projectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(projectRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this project. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param projectQueryRecord the project query record
     *  @param projectQueryInspectorRecord the project query inspector
     *         record
     *  @param projectSearchOrderRecord the project search order record
     *  @param projectRecordType project record type
     *  @throws org.osid.NullArgumentException
     *          <code>projectQueryRecord</code>,
     *          <code>projectQueryInspectorRecord</code>,
     *          <code>projectSearchOrderRecord</code> or
     *          <code>projectRecordTypeproject</code> is
     *          <code>null</code>
     */
            
    protected void addProjectRecords(org.osid.room.construction.records.ProjectQueryRecord projectQueryRecord, 
                                      org.osid.room.construction.records.ProjectQueryInspectorRecord projectQueryInspectorRecord, 
                                      org.osid.room.construction.records.ProjectSearchOrderRecord projectSearchOrderRecord, 
                                      org.osid.type.Type projectRecordType) {

        addRecordType(projectRecordType);

        nullarg(projectQueryRecord, "project query record");
        nullarg(projectQueryInspectorRecord, "project query inspector record");
        nullarg(projectSearchOrderRecord, "project search odrer record");

        this.queryRecords.add(projectQueryRecord);
        this.queryInspectorRecords.add(projectQueryInspectorRecord);
        this.searchOrderRecords.add(projectSearchOrderRecord);
        
        return;
    }
}
