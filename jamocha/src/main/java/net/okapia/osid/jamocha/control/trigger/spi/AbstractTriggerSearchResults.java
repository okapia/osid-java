//
// AbstractTriggerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractTriggerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.TriggerSearchResults {

    private org.osid.control.TriggerList triggers;
    private final org.osid.control.TriggerQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.TriggerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractTriggerSearchResults.
     *
     *  @param triggers the result set
     *  @param triggerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>triggers</code>
     *          or <code>triggerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractTriggerSearchResults(org.osid.control.TriggerList triggers,
                                            org.osid.control.TriggerQueryInspector triggerQueryInspector) {
        nullarg(triggers, "triggers");
        nullarg(triggerQueryInspector, "trigger query inspectpr");

        this.triggers = triggers;
        this.inspector = triggerQueryInspector;

        return;
    }


    /**
     *  Gets the trigger list resulting from a search.
     *
     *  @return a trigger list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggers() {
        if (this.triggers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.TriggerList triggers = this.triggers;
        this.triggers = null;
	return (triggers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.TriggerQueryInspector getTriggerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  trigger search record <code> Type. </code> This method must
     *  be used to retrieve a trigger implementing the requested
     *  record.
     *
     *  @param triggerSearchRecordType a trigger search 
     *         record type 
     *  @return the trigger search
     *  @throws org.osid.NullArgumentException
     *          <code>triggerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(triggerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerSearchResultsRecord getTriggerSearchResultsRecord(org.osid.type.Type triggerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.TriggerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(triggerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(triggerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record trigger search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addTriggerRecord(org.osid.control.records.TriggerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "trigger record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
