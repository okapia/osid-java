//
// AbstractAvailabilityQueryInspector.java
//
//     A template for making an AvailabilityQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.availability.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for availabilities.
 */

public abstract class AbstractAvailabilityQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.resourcing.AvailabilityQueryInspector {

    private final java.util.Collection<org.osid.resourcing.records.AvailabilityQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJobIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Gets the percentage availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getPercentageTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given availability query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an availability implementing the requested record.
     *
     *  @param availabilityRecordType an availability record type
     *  @return the availability query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilityQueryInspectorRecord getAvailabilityQueryInspectorRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.AvailabilityQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this availability query. 
     *
     *  @param availabilityQueryInspectorRecord availability query inspector
     *         record
     *  @param availabilityRecordType availability record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAvailabilityQueryInspectorRecord(org.osid.resourcing.records.AvailabilityQueryInspectorRecord availabilityQueryInspectorRecord, 
                                                   org.osid.type.Type availabilityRecordType) {

        addRecordType(availabilityRecordType);
        nullarg(availabilityRecordType, "availability record type");
        this.records.add(availabilityQueryInspectorRecord);        
        return;
    }
}
