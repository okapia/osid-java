//
// RegistrationMiter.java
//
//     Defines a Registration miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.registration;


/**
 *  Defines a <code>Registration</code> miter for use with the builders.
 */

public interface RegistrationMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.registration.Registration {


    /**
     *  Sets the activity bundle.
     *
     *  @param activityBundle an activity bundle
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundle</code> is <code>null</code>
     */

    public void setActivityBundle(org.osid.course.registration.ActivityBundle activityBundle);


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public void setStudent(org.osid.resource.Resource student);


    /**
     *  Adds a credit option.
     *
     *  @param credits a credit option
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public void addCredits(java.math.BigDecimal credits);


    /**
     *  Sets all the credit options.
     *
     *  @param credits a collection of credit options
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public void setCredits(java.util.Collection<java.math.BigDecimal> credits);


    /**
     *  Sets the grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public void setGradingOption(org.osid.grading.GradeSystem option);


    /**
     *  Adds a Registration record.
     *
     *  @param record a registration record
     *  @param recordType the type of registration record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRegistrationRecord(org.osid.course.registration.records.RegistrationRecord record, org.osid.type.Type recordType);
}       


