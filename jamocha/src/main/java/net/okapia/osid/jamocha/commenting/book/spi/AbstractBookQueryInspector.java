//
// AbstractBookQueryInspector.java
//
//     A template for making a BookQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.book.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for books.
 */

public abstract class AbstractBookQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.commenting.BookQueryInspector {

    private final java.util.Collection<org.osid.commenting.records.BookQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the comment <code> Id </code> terms. 
     *
     *  @return the comment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the comment terms. 
     *
     *  @return the comment terms 
     */

    @OSID @Override
    public org.osid.commenting.CommentQueryInspector[] getCommentTerms() {
        return (new org.osid.commenting.CommentQueryInspector[0]);
    }


    /**
     *  Gets the ancestor book <code> Id </code> terms. 
     *
     *  @return the ancestor book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor book terms. 
     *
     *  @return the ancestor book terms 
     */

    @OSID @Override
    public org.osid.commenting.BookQueryInspector[] getAncestorBookTerms() {
        return (new org.osid.commenting.BookQueryInspector[0]);
    }


    /**
     *  Gets the descendant book <code> Id </code> terms. 
     *
     *  @return the descendant book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant book terms. 
     *
     *  @return the descendant book terms 
     */

    @OSID @Override
    public org.osid.commenting.BookQueryInspector[] getDescendantBookTerms() {
        return (new org.osid.commenting.BookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given book query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a book implementing the requested record.
     *
     *  @param bookRecordType a book record type
     *  @return the book query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.BookQueryInspectorRecord getBookQueryInspectorRecord(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.BookQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(bookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this book query. 
     *
     *  @param bookQueryInspectorRecord book query inspector
     *         record
     *  @param bookRecordType book record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBookQueryInspectorRecord(org.osid.commenting.records.BookQueryInspectorRecord bookQueryInspectorRecord, 
                                                   org.osid.type.Type bookRecordType) {

        addRecordType(bookRecordType);
        nullarg(bookRecordType, "book record type");
        this.records.add(bookQueryInspectorRecord);        
        return;
    }
}
