//
// AbstractInventoryManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInventoryManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.inventory.InventoryManager,
               org.osid.inventory.InventoryProxyManager {

    private final Types itemRecordTypes                    = new TypeRefSet();
    private final Types itemSearchRecordTypes              = new TypeRefSet();

    private final Types stockRecordTypes                   = new TypeRefSet();
    private final Types stockSearchRecordTypes             = new TypeRefSet();

    private final Types modelRecordTypes                   = new TypeRefSet();
    private final Types modelSearchRecordTypes             = new TypeRefSet();

    private final Types inventoryRecordTypes               = new TypeRefSet();
    private final Types inventorySearchRecordTypes         = new TypeRefSet();

    private final Types warehouseRecordTypes               = new TypeRefSet();
    private final Types warehouseSearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractInventoryManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInventoryManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up items is supported. 
     *
     *  @return <code> true </code> if item lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemLookup() {
        return (false);
    }


    /**
     *  Tests if querying items is supported. 
     *
     *  @return <code> true </code> if item query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Tests if searching items is supported. 
     *
     *  @return <code> true </code> if item search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearch() {
        return (false);
    }


    /**
     *  Tests if an item <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if item administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemAdmin() {
        return (false);
    }


    /**
     *  Tests if an item <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if item notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemNotification() {
        return (false);
    }


    /**
     *  Tests if an item cataloging service is supported. 
     *
     *  @return <code> true </code> if item catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemWarehouse() {
        return (false);
    }


    /**
     *  Tests if an item cataloging service is supported. A cataloging service 
     *  maps items to catalogs. 
     *
     *  @return <code> true </code> if item cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemWarehouseAssignment() {
        return (false);
    }


    /**
     *  Tests if an item smart warehouse session is available. 
     *
     *  @return <code> true </code> if an item smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSmartWarehouse() {
        return (false);
    }


    /**
     *  Tests if looking up stocks is supported. 
     *
     *  @return <code> true </code> if stock lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockLookup() {
        return (false);
    }


    /**
     *  Tests if querying stocks is supported. 
     *
     *  @return <code> true </code> if stock query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Tests if searching stocks is supported. 
     *
     *  @return <code> true </code> if stock search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSearch() {
        return (false);
    }


    /**
     *  Tests if stock administrative service is supported. 
     *
     *  @return <code> true </code> if stock administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockAdmin() {
        return (false);
    }


    /**
     *  Tests if a stock <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if stock notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockNotification() {
        return (false);
    }


    /**
     *  Tests if a stock cataloging service is supported. 
     *
     *  @return <code> true </code> if stock catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockWarehouse() {
        return (false);
    }


    /**
     *  Tests if a stock cataloging service is supported. A cataloging service 
     *  maps stocks to catalogs. 
     *
     *  @return <code> true </code> if stock cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockWarehouseAssignment() {
        return (false);
    }


    /**
     *  Tests if a stock smart warehouse session is available. 
     *
     *  @return <code> true </code> if a stock smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSmartWarehouse() {
        return (false);
    }


    /**
     *  Tests for the availability of a stock hierarchy traversal service. 
     *
     *  @return <code> true </code> if stock hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a stock hierarchy design service. 
     *
     *  @return <code> true </code> if stock hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if looking up models is supported. 
     *
     *  @return <code> true </code> if model lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelLookup() {
        return (false);
    }


    /**
     *  Tests if querying models is supported. 
     *
     *  @return <code> true </code> if model query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Tests if searching models is supported. 
     *
     *  @return <code> true </code> if model search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelSearch() {
        return (false);
    }


    /**
     *  Tests if a stock <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if model administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelAdmin() {
        return (false);
    }


    /**
     *  Tests if a model <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if model notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelNotification() {
        return (false);
    }


    /**
     *  Tests if a model cataloging service is supported. 
     *
     *  @return <code> true </code> if model catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelWarehouse() {
        return (false);
    }


    /**
     *  Tests if a model cataloging service is supported. A cataloging service 
     *  maps models to catalogs. 
     *
     *  @return <code> true </code> if model cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelWarehouseAssignment() {
        return (false);
    }


    /**
     *  Tests if a model smart warehouse session is available. 
     *
     *  @return <code> true </code> if a model smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelSmartWarehouse() {
        return (false);
    }


    /**
     *  Tests if looking up inventories is supported. 
     *
     *  @return <code> true </code> if inventory lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryLookup() {
        return (false);
    }


    /**
     *  Tests if querying inventories is supported. 
     *
     *  @return <code> true </code> if inventory query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryQuery() {
        return (false);
    }


    /**
     *  Tests if searching inventories is supported. 
     *
     *  @return <code> true </code> if inventory search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventorySearch() {
        return (false);
    }


    /**
     *  Tests if inventory administrative service is supported. 
     *
     *  @return <code> true </code> if inventory administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryAdmin() {
        return (false);
    }


    /**
     *  Tests if an inventory <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if inventory notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryNotification() {
        return (false);
    }


    /**
     *  Tests if an inventory cataloging service is supported. 
     *
     *  @return <code> true </code> if inventory catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryWarehouse() {
        return (false);
    }


    /**
     *  Tests if an inventory cataloging service is supported. A cataloging 
     *  service maps inventories to catalogs. 
     *
     *  @return <code> true </code> if inventory cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryWarehouseAssignment() {
        return (false);
    }


    /**
     *  Tests if an inventory smart warehouse session is available. 
     *
     *  @return <code> true </code> if an inventory smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventorySmartWarehouse() {
        return (false);
    }


    /**
     *  Tests if looking up warehouses is supported. 
     *
     *  @return <code> true </code> if warehouse lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseLookup() {
        return (false);
    }


    /**
     *  Tests if searching warehouses is supported. 
     *
     *  @return <code> true </code> if warehouse search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseSearch() {
        return (false);
    }


    /**
     *  Tests if querying warehouses is supported. 
     *
     *  @return <code> true </code> if warehouse query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Tests if warehouse administrative service is supported. 
     *
     *  @return <code> true </code> if warehouse administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseAdmin() {
        return (false);
    }


    /**
     *  Tests if a warehouse <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if warehouse notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a warehouse hierarchy traversal service. 
     *
     *  @return <code> true </code> if warehouse hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a warehouse hierarchy design service. 
     *
     *  @return <code> true </code> if warehouse hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a inventory batch service. 
     *
     *  @return <code> true </code> if a inventory batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a inventory shipment service. 
     *
     *  @return <code> true </code> if a inventory shipment service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryShipment() {
        return (false);
    }


    /**
     *  Gets the supported <code> Item </code> record types. 
     *
     *  @return a list containing the supported <code> Item </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.itemRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Item </code> record type is supported. 
     *
     *  @param  itemRecordType a <code> Type </code> indicating an <code> Item 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemRecordType(org.osid.type.Type itemRecordType) {
        return (this.itemRecordTypes.contains(itemRecordType));
    }


    /**
     *  Adds support for an item record type.
     *
     *  @param itemRecordType an item record type
     *  @throws org.osid.NullArgumentException
     *  <code>itemRecordType</code> is <code>null</code>
     */

    protected void addItemRecordType(org.osid.type.Type itemRecordType) {
        this.itemRecordTypes.add(itemRecordType);
        return;
    }


    /**
     *  Removes support for an item record type.
     *
     *  @param itemRecordType an item record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>itemRecordType</code> is <code>null</code>
     */

    protected void removeItemRecordType(org.osid.type.Type itemRecordType) {
        this.itemRecordTypes.remove(itemRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Item </code> search record types. 
     *
     *  @return a list containing the supported <code> Item </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.itemSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Item </code> search record type is 
     *  supported. 
     *
     *  @param  itemSearchRecordType a <code> Type </code> indicating an 
     *          <code> Item </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        return (this.itemSearchRecordTypes.contains(itemSearchRecordType));
    }


    /**
     *  Adds support for an item search record type.
     *
     *  @param itemSearchRecordType an item search record type
     *  @throws org.osid.NullArgumentException
     *  <code>itemSearchRecordType</code> is <code>null</code>
     */

    protected void addItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        this.itemSearchRecordTypes.add(itemSearchRecordType);
        return;
    }


    /**
     *  Removes support for an item search record type.
     *
     *  @param itemSearchRecordType an item search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>itemSearchRecordType</code> is <code>null</code>
     */

    protected void removeItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        this.itemSearchRecordTypes.remove(itemSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Stock </code> record types. 
     *
     *  @return a list containing the supported <code> Stock </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStockRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stockRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Stock </code> record type is supported. 
     *
     *  @param  stockRecordType a <code> Type </code> indicating an <code> 
     *          Stock </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stockRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStockRecordType(org.osid.type.Type stockRecordType) {
        return (this.stockRecordTypes.contains(stockRecordType));
    }


    /**
     *  Adds support for a stock record type.
     *
     *  @param stockRecordType a stock record type
     *  @throws org.osid.NullArgumentException
     *  <code>stockRecordType</code> is <code>null</code>
     */

    protected void addStockRecordType(org.osid.type.Type stockRecordType) {
        this.stockRecordTypes.add(stockRecordType);
        return;
    }


    /**
     *  Removes support for a stock record type.
     *
     *  @param stockRecordType a stock record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stockRecordType</code> is <code>null</code>
     */

    protected void removeStockRecordType(org.osid.type.Type stockRecordType) {
        this.stockRecordTypes.remove(stockRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Stock </code> search record types. 
     *
     *  @return a list containing the supported <code> Stock </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStockSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stockSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Stock </code> search record type is 
     *  supported. 
     *
     *  @param  stockSearchRecordType a <code> Type </code> indicating an 
     *          <code> Stock </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stockSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStockSearchRecordType(org.osid.type.Type stockSearchRecordType) {
        return (this.stockSearchRecordTypes.contains(stockSearchRecordType));
    }


    /**
     *  Adds support for a stock search record type.
     *
     *  @param stockSearchRecordType a stock search record type
     *  @throws org.osid.NullArgumentException
     *  <code>stockSearchRecordType</code> is <code>null</code>
     */

    protected void addStockSearchRecordType(org.osid.type.Type stockSearchRecordType) {
        this.stockSearchRecordTypes.add(stockSearchRecordType);
        return;
    }


    /**
     *  Removes support for a stock search record type.
     *
     *  @param stockSearchRecordType a stock search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stockSearchRecordType</code> is <code>null</code>
     */

    protected void removeStockSearchRecordType(org.osid.type.Type stockSearchRecordType) {
        this.stockSearchRecordTypes.remove(stockSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Model </code> record types. 
     *
     *  @return a list containing the supported <code> Model </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModelRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.modelRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Model </code> record type is supported. 
     *
     *  @param  modelRecordType a <code> Type </code> indicating an <code> 
     *          Model </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> modelRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModelRecordType(org.osid.type.Type modelRecordType) {
        return (this.modelRecordTypes.contains(modelRecordType));
    }


    /**
     *  Adds support for a model record type.
     *
     *  @param modelRecordType a model record type
     *  @throws org.osid.NullArgumentException
     *  <code>modelRecordType</code> is <code>null</code>
     */

    protected void addModelRecordType(org.osid.type.Type modelRecordType) {
        this.modelRecordTypes.add(modelRecordType);
        return;
    }


    /**
     *  Removes support for a model record type.
     *
     *  @param modelRecordType a model record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>modelRecordType</code> is <code>null</code>
     */

    protected void removeModelRecordType(org.osid.type.Type modelRecordType) {
        this.modelRecordTypes.remove(modelRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Model </code> search record types. 
     *
     *  @return a list containing the supported <code> Model </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModelSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.modelSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Model </code> search record type is 
     *  supported. 
     *
     *  @param  modelSearchRecordType a <code> Type </code> indicating an 
     *          <code> Model </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> modelSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModelSearchRecordType(org.osid.type.Type modelSearchRecordType) {
        return (this.modelSearchRecordTypes.contains(modelSearchRecordType));
    }


    /**
     *  Adds support for a model search record type.
     *
     *  @param modelSearchRecordType a model search record type
     *  @throws org.osid.NullArgumentException
     *  <code>modelSearchRecordType</code> is <code>null</code>
     */

    protected void addModelSearchRecordType(org.osid.type.Type modelSearchRecordType) {
        this.modelSearchRecordTypes.add(modelSearchRecordType);
        return;
    }


    /**
     *  Removes support for a model search record type.
     *
     *  @param modelSearchRecordType a model search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>modelSearchRecordType</code> is <code>null</code>
     */

    protected void removeModelSearchRecordType(org.osid.type.Type modelSearchRecordType) {
        this.modelSearchRecordTypes.remove(modelSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Inventory </code> record types. 
     *
     *  @return a list containing the supported <code> Inventory </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInventoryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inventoryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Inventory </code> record type is supported. 
     *
     *  @param  inventoryRecordType a <code> Type </code> indicating a <code> 
     *          Inventory </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inventoryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInventoryRecordType(org.osid.type.Type inventoryRecordType) {
        return (this.inventoryRecordTypes.contains(inventoryRecordType));
    }


    /**
     *  Adds support for an inventory record type.
     *
     *  @param inventoryRecordType an inventory record type
     *  @throws org.osid.NullArgumentException
     *  <code>inventoryRecordType</code> is <code>null</code>
     */

    protected void addInventoryRecordType(org.osid.type.Type inventoryRecordType) {
        this.inventoryRecordTypes.add(inventoryRecordType);
        return;
    }


    /**
     *  Removes support for an inventory record type.
     *
     *  @param inventoryRecordType an inventory record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inventoryRecordType</code> is <code>null</code>
     */

    protected void removeInventoryRecordType(org.osid.type.Type inventoryRecordType) {
        this.inventoryRecordTypes.remove(inventoryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Inventory </code> search record types. 
     *
     *  @return a list containing the supported <code> Inventory </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInventorySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inventorySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Inventory </code> search record type is 
     *  supported. 
     *
     *  @param  inventorySearchRecordType a <code> Type </code> indicating a 
     *          <code> Inventory </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          inventorySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInventorySearchRecordType(org.osid.type.Type inventorySearchRecordType) {
        return (this.inventorySearchRecordTypes.contains(inventorySearchRecordType));
    }


    /**
     *  Adds support for an inventory search record type.
     *
     *  @param inventorySearchRecordType an inventory search record type
     *  @throws org.osid.NullArgumentException
     *  <code>inventorySearchRecordType</code> is <code>null</code>
     */

    protected void addInventorySearchRecordType(org.osid.type.Type inventorySearchRecordType) {
        this.inventorySearchRecordTypes.add(inventorySearchRecordType);
        return;
    }


    /**
     *  Removes support for an inventory search record type.
     *
     *  @param inventorySearchRecordType an inventory search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inventorySearchRecordType</code> is <code>null</code>
     */

    protected void removeInventorySearchRecordType(org.osid.type.Type inventorySearchRecordType) {
        this.inventorySearchRecordTypes.remove(inventorySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Warehouse </code> record types. 
     *
     *  @return a list containing the supported <code> Warehouse </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWarehouseRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.warehouseRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Warehouse </code> record type is supported. 
     *
     *  @param  warehouseRecordType a <code> Type </code> indicating an <code> 
     *          Warehouse </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> warehouseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWarehouseRecordType(org.osid.type.Type warehouseRecordType) {
        return (this.warehouseRecordTypes.contains(warehouseRecordType));
    }


    /**
     *  Adds support for a warehouse record type.
     *
     *  @param warehouseRecordType a warehouse record type
     *  @throws org.osid.NullArgumentException
     *  <code>warehouseRecordType</code> is <code>null</code>
     */

    protected void addWarehouseRecordType(org.osid.type.Type warehouseRecordType) {
        this.warehouseRecordTypes.add(warehouseRecordType);
        return;
    }


    /**
     *  Removes support for a warehouse record type.
     *
     *  @param warehouseRecordType a warehouse record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>warehouseRecordType</code> is <code>null</code>
     */

    protected void removeWarehouseRecordType(org.osid.type.Type warehouseRecordType) {
        this.warehouseRecordTypes.remove(warehouseRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Warehouse </code> search record types. 
     *
     *  @return a list containing the supported <code> Warehouse </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWarehouseSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.warehouseSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Warehouse </code> search record type is 
     *  supported. 
     *
     *  @param  warehouseSearchRecordType a <code> Type </code> indicating an 
     *          <code> Warehouse </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          warehousesearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWarehouseSearchRecordType(org.osid.type.Type warehouseSearchRecordType) {
        return (this.warehouseSearchRecordTypes.contains(warehouseSearchRecordType));
    }


    /**
     *  Adds support for a warehouse search record type.
     *
     *  @param warehouseSearchRecordType a warehouse search record type
     *  @throws org.osid.NullArgumentException
     *  <code>warehouseSearchRecordType</code> is <code>null</code>
     */

    protected void addWarehouseSearchRecordType(org.osid.type.Type warehouseSearchRecordType) {
        this.warehouseSearchRecordTypes.add(warehouseSearchRecordType);
        return;
    }


    /**
     *  Removes support for a warehouse search record type.
     *
     *  @param warehouseSearchRecordType a warehouse search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>warehouseSearchRecordType</code> is <code>null</code>
     */

    protected void removeWarehouseSearchRecordType(org.osid.type.Type warehouseSearchRecordType) {
        this.warehouseSearchRecordTypes.remove(warehouseSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service. 
     *
     *  @return an <code> ItemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemLookupSession getItemLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemLookupSession getItemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemLookupSession getItemLookupSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemLookupSession getItemLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service. 
     *
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuerySession getItemQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuerySession getItemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuerySession getItemQuerySessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuerySession getItemQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service. 
     *
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchSession getItemSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchSession getItemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchSession getItemSearchSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchSession getItemSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service. 
     *
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemAdminSession getItemAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemAdminSession getItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemAdminSession getItemAdminSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemAdminSession getItemAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service. 
     *
     *  @param  itemReceiver the notification callback 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemNotificationSession getItemNotificationSession(org.osid.inventory.ItemReceiver itemReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemNotificationSession getItemNotificationSession(org.osid.inventory.ItemReceiver itemReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service for the given warehouse. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> or 
     *          <code> warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemNotificationSession getItemNotificationSessionForWarehouse(org.osid.inventory.ItemReceiver itemReceiver, 
                                                                                             org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service for the given warehouse. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemNotificationSession getItemNotificationSessionForWarehouse(org.osid.inventory.ItemReceiver itemReceiver, 
                                                                                             org.osid.id.Id warehouseId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup item/catalog mappings. 
     *
     *  @return an <code> ItemWarehouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemWarehouse() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemWarehouseSession getItemWarehouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup item/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemWarehouseSession getItemWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning items to 
     *  warehouses. 
     *
     *  @return an <code> ItemWarehouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemWarehouseAssignmentSession getItemWarehouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning items to 
     *  warehouses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemWarehouseAssignmentSession getItemWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> ItemSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSmartWarehouseSession getItemSmartWarehouseSession(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getItemSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSmartWarehouseSession getItemSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getItemSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock lookup 
     *  service. 
     *
     *  @return a <code> StockSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockLookupSession getStockLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockLookupSession getStockLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @return a <code> StockLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockLookupSession getStockLookupSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @param  proxy proxy 
     *  @return a <code> StockLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockLookupSession getStockLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock query 
     *  service. 
     *
     *  @return a <code> StockQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuerySession getStockQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuerySession getStockQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> StockQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuerySession getStockQuerySessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuerySession getStockQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock search 
     *  service. 
     *
     *  @return a <code> StockSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchSession getStockSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchSession getStockSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> StockSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchSession getStockSearchSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchSession getStockSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  administration service. 
     *
     *  @return a <code> StockAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockAdminSession getStockAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockAdminSession getStockAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> StockAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockAdminSession getStockAdminSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockAdminSession getStockAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  notification service. 
     *
     *  @param  stockReceiver the notification callback 
     *  @return a <code> StockNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stockReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockNotificationSession getStockNotificationSession(org.osid.inventory.StockReceiver stockReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  notification service. 
     *
     *  @param  stockReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> StockNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stockReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockNotificationSession getStockNotificationSession(org.osid.inventory.StockReceiver stockReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  notification service for the given warehouse. 
     *
     *  @param  stockReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> StockNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> stockReceiver </code> or 
     *          <code> warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockNotificationSession getStockNotificationSessionForWarehouse(org.osid.inventory.StockReceiver stockReceiver, 
                                                                                               org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  notification service for the given warehouse. 
     *
     *  @param  stockReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> stockReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockNotificationSession getStockNotificationSessionForWarehouse(org.osid.inventory.StockReceiver stockReceiver, 
                                                                                               org.osid.id.Id warehouseId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup stock/catalog mappings. 
     *
     *  @return a <code> StockWarehouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockWarehouseSession getStockWarehouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup stock/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockWarehouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockWarehouseSession getStockWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning stocks 
     *  to warehouses. 
     *
     *  @return a <code> StockWarehouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockWarehouseAssignmentSession getStockWarehouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning stocks 
     *  to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockWarehouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockWarehouseAssignmentSession getStockWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> StockSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSmartWarehouseSession getStockSmartWarehouseSession(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSmartWarehouseSession getStockSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the stock hierarchy traversal session. 
     *
     *  @return a <code> StockHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchySession getStockHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockHierarchySession not implemented");
    }


    /**
     *  Gets the stock hierarchy traversal session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchySession getStockHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockHierarchySession not implemented");
    }


    /**
     *  Gets the stock hierarchy traversal session for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> StockHierarchySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchySession getStockHierarchySessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockHierarchySessionForWarehouse not implemented");
    }


    /**
     *  Gets the stock hierarchy traversal session for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchySession getStockHierarchySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockHierarchySessionForWarehouse not implemented");
    }


    /**
     *  Gets the stock hierarchy design session. 
     *
     *  @return a <code> StockHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchyDesignSession getStockHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the stock hierarchy design session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchyDesignSession getStockHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the stock hierarchy design session for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> StockHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchyDesignSession getStockHierarchyDesignSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getStockHierarchyDesignSessionForWarehouse not implemented");
    }


    /**
     *  Gets the stock hierarchy design session for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchyDesignSession getStockHierarchyDesignSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getStockHierarchyDesignSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model lookup 
     *  service. 
     *
     *  @return a <code> ModelSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelLookupSession getModelLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelLookupSession getModelLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @return a <code> ModelLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelLookupSession getModelLookupSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @param  proxy proxy 
     *  @return a <code> ModelLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelLookupSession getModelLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model query 
     *  service. 
     *
     *  @return a <code> ModelQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuerySession getModelQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuerySession getModelQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ModelQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuerySession getModelQuerySessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuerySession getModelQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model search 
     *  service. 
     *
     *  @return a <code> ModelSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchSession getModelSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchSession getModelSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ModelSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchSession getModelSearchSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchSession getModelSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelSearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  administration service. 
     *
     *  @return a <code> ModelAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelAdminSession getModelAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelAdminSession getModelAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ModelAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelAdminSession getModelAdminSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelAdminSession getModelAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  notification service. 
     *
     *  @param  modelReceiver the notification callback 
     *  @return a <code> ModelNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> modelReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelNotificationSession getModelNotificationSession(org.osid.inventory.ModelReceiver modelReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  notification service. 
     *
     *  @param  modelReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ModelNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> modelReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelNotificationSession getModelNotificationSession(org.osid.inventory.ModelReceiver modelReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  notification service for the given warehouse. 
     *
     *  @param  modelReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ModelNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> modelReceiver </code> or 
     *          <code> warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelNotificationSession getModelNotificationSessionForWarehouse(org.osid.inventory.ModelReceiver modelReceiver, 
                                                                                               org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  notification service for the given warehouse. 
     *
     *  @param  modelReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> modelReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelNotificationSession getModelNotificationSessionForWarehouse(org.osid.inventory.ModelReceiver modelReceiver, 
                                                                                               org.osid.id.Id warehouseId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup model/catalog mappings. 
     *
     *  @return a <code> ModelWarehouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelWarehouseSession getModelWarehouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup model/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelWarehouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelWarehouseSession getModelWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning models 
     *  to warehouses. 
     *
     *  @return a <code> ModelWarehouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelWarehouseAssignmentSession getModelWarehouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning models 
     *  to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelWarehouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelWarehouseAssignmentSession getModelWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return a <code> ModelSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSmartWarehouseSession getModelSmartWarehouseSession(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getModelSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSmartWarehouseSession getModelSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getModelSmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  lookup service. 
     *
     *  @return an <code> InventoryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryLookupSession getInventoryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryLookupSession getInventoryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  lookup service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> InventoryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryLookupSession getInventoryLookupSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  lookup service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventoryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryLookupSession getInventoryLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryLookupSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  query service. 
     *
     *  @return an <code> InventoryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuerySession getInventoryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> InventoryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuerySession getInventoryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  query service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> InventoryQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuerySession getInventoryQuerySessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  query service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> InventoryQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuerySession getInventoryQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryQuerySessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  search service. 
     *
     *  @return an <code> InventorySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySearchSession getInventorySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventorySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventorySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySearchSession getInventorySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventorySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  search service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> InventorySearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySearchSession getInventorySearchSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventorySearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  search service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventorySearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySearchSession getInventorySearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventorySearchSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  administration service. 
     *
     *  @return an <code> InventoryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryAdminSession getInventoryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryAdminSession getInventoryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> InventoryAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryAdminSession getInventoryAdminSessionForWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventoryAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryAdminSession getInventoryAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryAdminSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  notification service. 
     *
     *  @param  inventoryReceiver the notification callback 
     *  @return an <code> InventoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryNotificationSession getInventoryNotificationSession(org.osid.inventory.InventoryReceiver inventoryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  notification service. 
     *
     *  @param  inventoryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> InventoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryNotificationSession getInventoryNotificationSession(org.osid.inventory.InventoryReceiver inventoryReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  notification service for the given warehouse. 
     *
     *  @param  inventoryReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> InventoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryReceiver 
     *          </code> or <code> warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryNotificationSession getInventoryNotificationSessionForWarehouse(org.osid.inventory.InventoryReceiver inventoryReceiver, 
                                                                                                       org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  notification service for the given warehouse. 
     *
     *  @param  inventoryReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryReceiver, 
     *          warehouseId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryNotificationSession getInventoryNotificationSessionForWarehouse(org.osid.inventory.InventoryReceiver inventoryReceiver, 
                                                                                                       org.osid.id.Id warehouseId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryNotificationSessionForWarehouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inventory/catalog 
     *  mappings. 
     *
     *  @return an <code> InventoryWarehouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryWarehouseSession getInventoryWarehouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inventory/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryWarehouseSession getInventoryWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  inventories to warehouses. 
     *
     *  @return an <code> InventoryWarehouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryWarehouseAssignmentSession getInventoryWarehouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  inventories to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryWarehouseAssignmentSession getInventoryWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryWarehouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  smart warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @return an <code> InventorySmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySmartWarehouseSession getInventorySmartWarehouseSession(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventorySmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  smart warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> InventorySmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySmartWarehouseSession getInventorySmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventorySmartWarehouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  lookup service. 
     *
     *  @return a <code> WarehouseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseLookupSession getWarehouseLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getWarehouseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseLookupSession getWarehouseLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getWarehouseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  query service. 
     *
     *  @return a <code> WarehouseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuerySession getWarehouseQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getWarehouseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuerySession getWarehouseQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getWarehouseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  search service. 
     *
     *  @return a <code> WarehouseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseSearchSession getWarehouseSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getWarehouseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseSearchSession getWarehouseSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getWarehouseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  administrative service. 
     *
     *  @return a <code> WarehouseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseAdminSession getWarehouseAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getWarehouseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  administrative service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseAdminSession getWarehouseAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getWarehouseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  notification service. 
     *
     *  @param  warehouseReceiver the notification callback 
     *  @return a <code> WarehouseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseNotificationSession getWarehouseNotificationSession(org.osid.inventory.WarehouseReceiver warehouseReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getWarehouseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  notification service. 
     *
     *  @param  warehouseReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> WarehouseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseNotificationSession getWarehouseNotificationSession(org.osid.inventory.WarehouseReceiver warehouseReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getWarehouseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  hierarchy service. 
     *
     *  @return a <code> WarehouseHierarchySession </code> for warehouses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseHierarchySession getWarehouseHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getWarehouseHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  hierarchy service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseHierarchySession </code> for warehouses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseHierarchySession getWarehouseHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getWarehouseHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for warehouses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseHierarchyDesignSession getWarehouseHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getWarehouseHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  hierarchy design service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> HierarchyDesignSession </code> for warehouses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseHierarchyDesignSession getWarehouseHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getWarehouseHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> InventoryBatchManager. </code> 
     *
     *  @return a <code> InventoryBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.InventoryBatchManager getInventoryBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryBatchManager not implemented");
    }


    /**
     *  Gets the <code> InventoryBatchProxyManager. </code> 
     *
     *  @return a <code> InventoryBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.InventoryBatchProxyManager getInventoryBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> InventoryShipmentManager. </code> 
     *
     *  @return a <code> InventoryShipmentManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryShipment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.InventoryShipmentManager getInventoryShipmentManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryManager.getInventoryShipmentManager not implemented");
    }


    /**
     *  Gets the <code> InventoryShipmentProxyManager. </code> 
     *
     *  @return a <code> InventoryShipmentProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryShipment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.InventoryShipmentProxyManager getInventoryShipmentProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.InventoryProxyManager.getInventoryShipmentProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.itemRecordTypes.clear();
        this.itemRecordTypes.clear();

        this.itemSearchRecordTypes.clear();
        this.itemSearchRecordTypes.clear();

        this.stockRecordTypes.clear();
        this.stockRecordTypes.clear();

        this.stockSearchRecordTypes.clear();
        this.stockSearchRecordTypes.clear();

        this.modelRecordTypes.clear();
        this.modelRecordTypes.clear();

        this.modelSearchRecordTypes.clear();
        this.modelSearchRecordTypes.clear();

        this.inventoryRecordTypes.clear();
        this.inventoryRecordTypes.clear();

        this.inventorySearchRecordTypes.clear();
        this.inventorySearchRecordTypes.clear();

        this.warehouseRecordTypes.clear();
        this.warehouseRecordTypes.clear();

        this.warehouseSearchRecordTypes.clear();
        this.warehouseSearchRecordTypes.clear();

        return;
    }
}
