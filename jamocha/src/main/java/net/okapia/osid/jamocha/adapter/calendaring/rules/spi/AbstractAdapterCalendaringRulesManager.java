//
// AbstractCalendaringRulesManager.java
//
//     An adapter for a CalendaringRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CalendaringRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCalendaringRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.calendaring.rules.CalendaringRulesManager>
    implements org.osid.calendaring.rules.CalendaringRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCalendaringRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCalendaringRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up recurring event enablers is supported. 
     *
     *  @return true if recurring event enabler lookup is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerLookup() {
        return (getAdapteeManager().supportsRecurringEventEnablerLookup());
    }


    /**
     *  Tests if querying recurring event enablers is supported. 
     *
     *  @return true if recurring event enabler query is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerQuery() {
        return (getAdapteeManager().supportsRecurringEventEnablerQuery());
    }


    /**
     *  Tests if searching recurring event enablers is supported. 
     *
     *  @return true if recurring event enabler search is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerSearch() {
        return (getAdapteeManager().supportsRecurringEventEnablerSearch());
    }


    /**
     *  Tests if an recurring event enabler administrative service is 
     *  supported. 
     *
     *  @return true if recurring event enabler administration is supported, 
     *          false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerAdmin() {
        return (getAdapteeManager().supportsRecurringEventEnablerAdmin());
    }


    /**
     *  Tests if an recurring event enabler notification service is supported. 
     *
     *  @return true if recurring event enabler notification is supported, 
     *          false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerNotification() {
        return (getAdapteeManager().supportsRecurringEventEnablerNotification());
    }


    /**
     *  Tests if an recurring event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return true if an recurring event enabler calendar lookup service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerCalendar() {
        return (getAdapteeManager().supportsRecurringEventEnablerCalendar());
    }


    /**
     *  Tests if an recurring event enabler calendar service is supported. 
     *
     *  @return true if recurring event enabler calendar assignment service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerCalendarAssignment() {
        return (getAdapteeManager().supportsRecurringEventEnablerCalendarAssignment());
    }


    /**
     *  Tests if an recurring event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return true if an recurring event enabler calendar service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerSmartCalendar() {
        return (getAdapteeManager().supportsRecurringEventEnablerSmartCalendar());
    }


    /**
     *  Tests if an recurring event enabler rule lookup service is supported. 
     *
     *  @return true if an recurring event enabler rule lookup service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerRuleLookup() {
        return (getAdapteeManager().supportsRecurringEventEnablerRuleLookup());
    }


    /**
     *  Tests if an recurring event enabler rule application service is 
     *  supported. 
     *
     *  @return true if recurring event enabler rule application service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerRuleApplication() {
        return (getAdapteeManager().supportsRecurringEventEnablerRuleApplication());
    }


    /**
     *  Tests if looking up offset event enablers is supported. 
     *
     *  @return <code> true </code> if offset event enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerLookup() {
        return (getAdapteeManager().supportsOffsetEventEnablerLookup());
    }


    /**
     *  Tests if querying offset event enablers is supported. 
     *
     *  @return <code> true </code> if offset event enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerQuery() {
        return (getAdapteeManager().supportsOffsetEventEnablerQuery());
    }


    /**
     *  Tests if searching offset event enablers is supported. 
     *
     *  @return <code> true </code> if offset event enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerSearch() {
        return (getAdapteeManager().supportsOffsetEventEnablerSearch());
    }


    /**
     *  Tests if an offset event enabler administrative service is supported. 
     *
     *  @return <code> true </code> if offset event enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerAdmin() {
        return (getAdapteeManager().supportsOffsetEventEnablerAdmin());
    }


    /**
     *  Tests if an offset event enabler notification service is supported. 
     *
     *  @return <code> true </code> if offset event enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerNotification() {
        return (getAdapteeManager().supportsOffsetEventEnablerNotification());
    }


    /**
     *  Tests if an offset event enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if an offset event enabler calendar lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerCalendar() {
        return (getAdapteeManager().supportsOffsetEventEnablerCalendar());
    }


    /**
     *  Tests if an offset event enabler calendar service is supported. 
     *
     *  @return <code> true </code> if offset event enabler calendar 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerCalendarAssignment() {
        return (getAdapteeManager().supportsOffsetEventEnablerCalendarAssignment());
    }


    /**
     *  Tests if an offset event enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if an offset event enabler calendar 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerSmartCalendar() {
        return (getAdapteeManager().supportsOffsetEventEnablerSmartCalendar());
    }


    /**
     *  Tests if an offset event enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an offset event enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerRuleLookup() {
        return (getAdapteeManager().supportsOffsetEventEnablerRuleLookup());
    }


    /**
     *  Tests if an offset event enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if offset event enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerRuleApplication() {
        return (getAdapteeManager().supportsOffsetEventEnablerRuleApplication());
    }


    /**
     *  Tests if looking up superseding event enablers is supported. 
     *
     *  @return <code> true </code> if superseding event enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerLookup() {
        return (getAdapteeManager().supportsSupersedingEventEnablerLookup());
    }


    /**
     *  Tests if querying superseding event enablers is supported. 
     *
     *  @return <code> true </code> if superseding event enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerQuery() {
        return (getAdapteeManager().supportsSupersedingEventEnablerQuery());
    }


    /**
     *  Tests if searching superseding event enablers is supported. 
     *
     *  @return <code> true </code> if superseding event enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerSearch() {
        return (getAdapteeManager().supportsSupersedingEventEnablerSearch());
    }


    /**
     *  Tests if an superseding event enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if superseding event enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerAdmin() {
        return (getAdapteeManager().supportsSupersedingEventEnablerAdmin());
    }


    /**
     *  Tests if an superseding event enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if superseding event enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerNotification() {
        return (getAdapteeManager().supportsSupersedingEventEnablerNotification());
    }


    /**
     *  Tests if an superseding event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an superseding event enabler calendar 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerCalendar() {
        return (getAdapteeManager().supportsSupersedingEventEnablerCalendar());
    }


    /**
     *  Tests if an superseding event enabler calendar service is supported. 
     *
     *  @return <code> true </code> if superseding event enabler calendar 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerCalendarAssignment() {
        return (getAdapteeManager().supportsSupersedingEventEnablerCalendarAssignment());
    }


    /**
     *  Tests if an superseding event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an superseding event enabler calendar 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerSmartCalendar() {
        return (getAdapteeManager().supportsSupersedingEventEnablerSmartCalendar());
    }


    /**
     *  Tests if an superseding event enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an superseding event enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerRuleLookup() {
        return (getAdapteeManager().supportsSupersedingEventEnablerRuleLookup());
    }


    /**
     *  Tests if an superseding event enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if superseding event enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerRuleApplication() {
        return (getAdapteeManager().supportsSupersedingEventEnablerRuleApplication());
    }


    /**
     *  Tests if looking up commitment enablers is supported. 
     *
     *  @return <code> true </code> if commitment enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerLookup() {
        return (getAdapteeManager().supportsCommitmentEnablerLookup());
    }


    /**
     *  Tests if querying commitment enablers is supported. 
     *
     *  @return <code> true </code> if commitment enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerQuery() {
        return (getAdapteeManager().supportsCommitmentEnablerQuery());
    }


    /**
     *  Tests if searching commitment enablers is supported. 
     *
     *  @return <code> true </code> if commitment enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerSearch() {
        return (getAdapteeManager().supportsCommitmentEnablerSearch());
    }


    /**
     *  Tests if a commitment enabler administrative service is supported. 
     *
     *  @return <code> true </code> if commitment enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerAdmin() {
        return (getAdapteeManager().supportsCommitmentEnablerAdmin());
    }


    /**
     *  Tests if a commitment enabler notification service is supported. 
     *
     *  @return <code> true </code> if commitment enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerNotification() {
        return (getAdapteeManager().supportsCommitmentEnablerNotification());
    }


    /**
     *  Tests if a commitment enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if a commitment enabler calendar lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerCalendar() {
        return (getAdapteeManager().supportsCommitmentEnablerCalendar());
    }


    /**
     *  Tests if a commitment enabler calendar service is supported. 
     *
     *  @return <code> true </code> if commitment enabler calendar assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerCalendarAssignment() {
        return (getAdapteeManager().supportsCommitmentEnablerCalendarAssignment());
    }


    /**
     *  Tests if a commitment enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if a commitment enabler calendar service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerSmartCalendar() {
        return (getAdapteeManager().supportsCommitmentEnablerSmartCalendar());
    }


    /**
     *  Tests if a commitment enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a commitment enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerRuleLookup() {
        return (getAdapteeManager().supportsCommitmentEnablerRuleLookup());
    }


    /**
     *  Tests if a commitment enabler rule application service is supported. 
     *
     *  @return <code> true </code> if commitment enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerRuleApplication() {
        return (getAdapteeManager().supportsCommitmentEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> RecurringEventEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> RecurringEventEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventEnablerRecordTypes() {
        return (getAdapteeManager().getRecurringEventEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> RecurringEventEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  recurringEventEnablerRecordType a <code> Type </code> 
     *          indicating a <code> RecurringEventEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerRecordType(org.osid.type.Type recurringEventEnablerRecordType) {
        return (getAdapteeManager().supportsRecurringEventEnablerRecordType(recurringEventEnablerRecordType));
    }


    /**
     *  Gets the supported <code> RecurringEventEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> RecurringEventEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventEnablerSearchRecordTypes() {
        return (getAdapteeManager().getRecurringEventEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> RecurringEventEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  recurringEventEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RecurringEventEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerSearchRecordType(org.osid.type.Type recurringEventEnablerSearchRecordType) {
        return (getAdapteeManager().supportsRecurringEventEnablerSearchRecordType(recurringEventEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> OffsetEventEnabler </code> record interface 
     *  types. 
     *
     *  @return a list containing the supported <code> OffsetEventEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventEnablerRecordTypes() {
        return (getAdapteeManager().getOffsetEventEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> OffsetEventEnabler </code> record interface 
     *  type is supported. 
     *
     *  @param  offsetEventEnablerRecordType a <code> Type </code> indicating 
     *          an <code> OffsetEventEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerRecordType(org.osid.type.Type offsetEventEnablerRecordType) {
        return (getAdapteeManager().supportsOffsetEventEnablerRecordType(offsetEventEnablerRecordType));
    }


    /**
     *  Gets the supported <code> OffsetEventEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> OffsetEventEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventEnablerSearchRecordTypes() {
        return (getAdapteeManager().getOffsetEventEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> OffsetEventEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  offsetEventEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> OffsetEventEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerSearchRecordType(org.osid.type.Type offsetEventEnablerSearchRecordType) {
        return (getAdapteeManager().supportsOffsetEventEnablerSearchRecordType(offsetEventEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> SupersedingEventEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> SupersedingEventEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventEnablerRecordTypes() {
        return (getAdapteeManager().getSupersedingEventEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> SupersedingEventEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  supersedingEventEnablerRecordType a <code> Type </code> 
     *          indicating an <code> SupersedingEventEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerRecordType(org.osid.type.Type supersedingEventEnablerRecordType) {
        return (getAdapteeManager().supportsSupersedingEventEnablerRecordType(supersedingEventEnablerRecordType));
    }


    /**
     *  Gets the supported <code> SupersedingEventEnabler </code> search 
     *  record interface types. 
     *
     *  @return a list containing the supported <code> SupersedingEventEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventEnablerSearchRecordTypes() {
        return (getAdapteeManager().getSupersedingEventEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SupersedingEventEnabler </code> search 
     *  record interface type is supported. 
     *
     *  @param  supersedingEventEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> SupersedingEventEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerSearchRecordType(org.osid.type.Type supersedingEventEnablerSearchRecordType) {
        return (getAdapteeManager().supportsSupersedingEventEnablerSearchRecordType(supersedingEventEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> CommitmentEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CommitmentEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentEnablerRecordTypes() {
        return (getAdapteeManager().getCommitmentEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> CommitmentEnabler </code> record type is 
     *  supported. 
     *
     *  @param  commitmentEnablerRecordType a <code> Type </code> indicating a 
     *          <code> CommitmentEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerRecordType(org.osid.type.Type commitmentEnablerRecordType) {
        return (getAdapteeManager().supportsCommitmentEnablerRecordType(commitmentEnablerRecordType));
    }


    /**
     *  Gets the supported <code> CommitmentEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CommitmentEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentEnablerSearchRecordTypes() {
        return (getAdapteeManager().getCommitmentEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CommitmentEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  commitmentEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CommitmentEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerSearchRecordType(org.osid.type.Type commitmentEnablerSearchRecordType) {
        return (getAdapteeManager().supportsCommitmentEnablerSearchRecordType(commitmentEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler lookup service. 
     *
     *  @return a <code> RecurringEventEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerLookupSession getRecurringEventEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerLookupSession getRecurringEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler query service. 
     *
     *  @return a <code> RecurringEventEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerQuerySession getRecurringEventEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerQuerySession getRecurringEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerQuerySessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler search service. 
     *
     *  @return a <code> RecurringEventEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSearchSession getRecurringEventEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSearchSession getRecurringEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerSearchSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler administration service. 
     *
     *  @return a <code> RecurringEventEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerAdminSession getRecurringEventEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerAdminSession getRecurringEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerAdminSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler notification service. 
     *
     *  @param  recurringEventEnablerReceiver the notification callback 
     *  @return a <code> RecurringEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerNotificationSession getRecurringEventEnablerNotificationSession(org.osid.calendaring.rules.RecurringEventEnablerReceiver recurringEventEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerNotificationSession(recurringEventEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler notification service for the given calendar. 
     *
     *  @param  recurringEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerReceiver </code> or <code> calendarId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerNotificationSession getRecurringEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.RecurringEventEnablerReceiver recurringEventEnablerReceiver, 
                                                                                                                                      org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerNotificationSessionForCalendar(recurringEventEnablerReceiver, calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup recurring event 
     *  enabler/calendar mappings for recurring event enablers. 
     *
     *  @return a <code> RecurringEventEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerCalendarSession getRecurringEventEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerCalendarSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  recurring event enablers to calendars. 
     *
     *  @return an <code> RecurringEventEnablerCalendarAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerCalendarAssignmentSession getRecurringEventEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerCalendarAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage recurring event enabler 
     *  smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSmartCalendarSession getRecurringEventEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerSmartCalendarSession(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler mapping lookup service for looking up the rules. 
     *
     *  @return a <code> RecurringEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleLookupSession getRecurringEventEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler mapping lookup service for the given calendar for 
     *  looking up rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleLookupSession getrecurringEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getrecurringEventEnablerRuleLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler assignment service to apply enablers. 
     *
     *  @return a <code> RecurringEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleApplicationSession getRecurringEventEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler assignment service for the given calendar to apply 
     *  enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleApplicationSession getRecurringEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventEnablerRuleApplicationSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler lookup service. 
     *
     *  @return an <code> OffsetEventEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerLookupSession getOffsetEventEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerLookupSession getOffsetEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler query service. 
     *
     *  @return an <code> OffsetEventEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerQuerySession getOffsetEventEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerQuerySession getOffsetEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerQuerySessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler search service. 
     *
     *  @return an <code> OffsetEventEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSearchSession getOffsetEventEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSearchSession getOffsetEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerSearchSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler administration service. 
     *
     *  @return an <code> OffsetEventEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerAdminSession getOffsetEventEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerAdminSession getOffsetEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerAdminSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler notification service. 
     *
     *  @param  offsetEventEnablerReceiver the notification callback 
     *  @return an <code> OffsetEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerNotificationSession getOffsetEventEnablerNotificationSession(org.osid.calendaring.rules.OffsetEventEnablerReceiver offsetEventEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerNotificationSession(offsetEventEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler notification service for the given calendar. 
     *
     *  @param  offsetEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerReceiver </code> or <code> calendarId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerNotificationSession getOffsetEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.OffsetEventEnablerReceiver offsetEventEnablerReceiver, 
                                                                                                                                org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerNotificationSessionForCalendar(offsetEventEnablerReceiver, calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offset event 
     *  enabler/calendar mappings for offset event enablers. 
     *
     *  @return an <code> OffsetEventEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerCalendarSession getOffsetEventEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerCalendarSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offset 
     *  event enablers to calendars. 
     *
     *  @return an <code> OffsetEventEnablerCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerCalendarAssignmentSession getOffsetEventEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerCalendarAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offset event enabler 
     *  smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSmartCalendarSession getOffsetEventEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerSmartCalendarSession(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler mapping lookup service for looking up the rules. 
     *
     *  @return an <code> OffsetEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleLookupSession getOffsetEventEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler mapping lookup service for the given calendar for looking up 
     *  rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleLookupSession getOffsetEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerRuleLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler assignment service to apply enablers. 
     *
     *  @return an <code> OffsetEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleApplicationSession getOffsetEventEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler assignment service for the given calendar to apply enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleApplicationSession getOffsetEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventEnablerRuleApplicationSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler lookup service. 
     *
     *  @return a <code> SupersedingEventEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerLookupSession getSupersedingEventEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerLookupSession getSupersedingEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler query service. 
     *
     *  @return a <code> SupersedingEventEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerQuerySession getSupersedingEventEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerQuerySession getSupersedingEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerQuerySessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler search service. 
     *
     *  @return a <code> SupersedingEventEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSearchSession getSupersedingEventEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSearchSession getSupersedingEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerSearchSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler administration service. 
     *
     *  @return a <code> SupersedingEventEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerAdminSession getSupersedingEventEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerAdminSession getSupersedingEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerAdminSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler notification service. 
     *
     *  @param  supersedingEventEnablerReceiver the notification callback 
     *  @return a <code> SupersedingEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerNotificationSession getSupersedingEventEnablerNotificationSession(org.osid.calendaring.rules.SupersedingEventEnablerReceiver supersedingEventEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerNotificationSession(supersedingEventEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler notification service for the given calendar. 
     *
     *  @param  supersedingEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerReceiver </code> or <code> calendarId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerNotificationSession getSupersedingEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.SupersedingEventEnablerReceiver supersedingEventEnablerReceiver, 
                                                                                                                                          org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerNotificationSessionForCalendar(supersedingEventEnablerReceiver, calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup superseding event 
     *  enabler/calendar mappings for superseding event enablers. 
     *
     *  @return a <code> SupersedingEventEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerCalendarSession getSupersedingEventEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerCalendarSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  superseding event enablers to calendars. 
     *
     *  @return a <code> SupersedingEventEnablerCalendarAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerCalendarAssignmentSession getSupersedingEventEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerCalendarAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage superseding event 
     *  enabler smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSmartCalendar() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSmartCalendarSession getSupersedingEventEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerSmartCalendarSession(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler mapping lookup service for looking up the rules. 
     *
     *  @return a <code> SupersedingEventEnablerRuleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleLookupSession getSupersedingEventEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler mapping lookup service for the given calendar for 
     *  looking up rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleLookupSession getSupersedingEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerRuleLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler assignment service to apply enablers. 
     *
     *  @return a <code> SupersedingEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleApplicationSession getSupersedingEventEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler assignment service for the given calendar to apply 
     *  enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleApplicationSession getSupersedingEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSupersedingEventEnablerRuleApplicationSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler lookup service. 
     *
     *  @return a <code> CommitmentEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerLookupSession getCommitmentEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerLookupSession getCommitmentEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler query service. 
     *
     *  @return a <code> CommitmentEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerQuerySession getCommitmentEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerQuerySession getCommitmentEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerQuerySessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler search service. 
     *
     *  @return a <code> CommitmentEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSearchSession getCommitmentEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSearchSession getCommitmentEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerSearchSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler administration service. 
     *
     *  @return a <code> CommitmentEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerAdminSession getCommitmentEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerAdminSession getCommitmentEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerAdminSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler notification service. 
     *
     *  @param  commitmentEnablerReceiver the notification callback 
     *  @return a <code> CommitmentEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerNotificationSession getCommitmentEnablerNotificationSession(org.osid.calendaring.rules.CommitmentEnablerReceiver commitmentEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerNotificationSession(commitmentEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler notification service for the given calendar. 
     *
     *  @param  commitmentEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerReceiver </code> or <code> calendarId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerNotificationSession getCommitmentEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.CommitmentEnablerReceiver commitmentEnablerReceiver, 
                                                                                                                              org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerNotificationSessionForCalendar(commitmentEnablerReceiver, calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commitment 
     *  enabler/calendar mappings for commitment enablers. 
     *
     *  @return a <code> CommitmentEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerCalendarSession getCommitmentEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerCalendarSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  commitment enablers to calendars for commitment. 
     *
     *  @return a <code> CommitmentEnablerCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerCalendarAssignmentSession getCommitmentEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerCalendarAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commitment enabler smart 
     *  calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSmartCalendarSession getCommitmentEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerSmartCalendarSession(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> CommitmentEnablertRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleLookupSession getCommitmentEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler mapping lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleLookupSession getCommitmentEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerRuleLookupSessionForCalendar(calendarId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler assignment service to apply enablers. 
     *
     *  @return a <code> CommitmentEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleApplicationSession getCommitmentEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler assignment service for the given calendar to apply enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleApplicationSession getCommitmentEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentEnablerRuleApplicationSessionForCalendar(calendarId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
