//
// InquiryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquiry;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class InquiryElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the InquiryElement Id.
     *
     *  @return the inquiry element Id
     */

    public static org.osid.id.Id getInquiryEntityId() {
        return (makeEntityId("osid.inquiry.Inquiry"));
    }


    /**
     *  Gets the AuditId element Id.
     *
     *  @return the AuditId element Id
     */

    public static org.osid.id.Id getAuditId() {
        return (makeElementId("osid.inquiry.inquiry.AuditId"));
    }


    /**
     *  Gets the Audit element Id.
     *
     *  @return the Audit element Id
     */

    public static org.osid.id.Id getAudit() {
        return (makeElementId("osid.inquiry.inquiry.Audit"));
    }


    /**
     *  Gets the Question element Id.
     *
     *  @return the Question element Id
     */

    public static org.osid.id.Id getQuestion() {
        return (makeElementId("osid.inquiry.inquiry.Question"));
    }


    /**
     *  Gets the Required element Id.
     *
     *  @return the Required element Id
     */

    public static org.osid.id.Id getRequired() {
        return (makeQueryElementId("osid.inquiry.inquiry.Required"));
    }


    /**
     *  Gets the AffirmationRequired element Id.
     *
     *  @return the AffirmationRequired element Id
     */

    public static org.osid.id.Id getAffirmationRequired() {
        return (makeQueryElementId("osid.inquiry.inquiry.AffirmationRequired"));
    }


    /**
     *  Gets the NeedsOneResponse element Id.
     *
     *  @return the NeedsOneResponse element Id
     */

    public static org.osid.id.Id getNeedsOneResponse() {
        return (makeQueryElementId("osid.inquiry.inquiry.NeedsOneResponse"));
    }


    /**
     *  Gets the InquestId element Id.
     *
     *  @return the InquestId element Id
     */

    public static org.osid.id.Id getInquestId() {
        return (makeQueryElementId("osid.inquiry.inquiry.InquestId"));
    }


    /**
     *  Gets the Inquest element Id.
     *
     *  @return the Inquest element Id
     */

    public static org.osid.id.Id getInquest() {
        return (makeQueryElementId("osid.inquiry.inquiry.Inquest"));
    }
}
