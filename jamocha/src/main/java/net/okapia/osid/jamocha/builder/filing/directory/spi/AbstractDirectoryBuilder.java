//
// AbstractDirectory.java
//
//     Defines a Directory builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.directory.spi;


/**
 *  Defines a <code>Directory</code> builder.
 */

public abstract class AbstractDirectoryBuilder<T extends AbstractDirectoryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.filing.directory.DirectoryMiter directory;


    /**
     *  Constructs a new <code>AbstractDirectoryBuilder</code>.
     *
     *  @param directory the directory to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDirectoryBuilder(net.okapia.osid.jamocha.builder.filing.directory.DirectoryMiter directory) {
        super(directory);
        this.directory = directory;
        return;
    }


    /**
     *  Builds the directory.
     *
     *  @return the new directory
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.filing.Directory build() {
        (new net.okapia.osid.jamocha.builder.validator.filing.directory.DirectoryValidator(getValidations())).validate(this.directory);
        return (new net.okapia.osid.jamocha.builder.filing.directory.ImmutableDirectory(this.directory));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the directory miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.filing.directory.DirectoryMiter getMiter() {
        return (this.directory);
    }


    /**
     *  Sets the name.
     *  
     *  @param name the file name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    public T name(String name) {
        getMiter().setName(name);
        return (self());
    }


    /**
     *  Sets the alias flag.
     */

    public T alias() {
        getMiter().setAlias(true);
        return (self());
    }


    /**
     *  Unsets the alias flag.
     */

    public T unalias() {
        getMiter().setAlias(false);
        return (self());
    }


    /**
     *  Sets the path.
     *  
     *  @param path the file path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T path(String path) {
        getMiter().setPath(path);
        return (self());
    }


    /**
     *  Sets the real path.
     *  
     *  @param path the file real path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T realPath(String path) {
        getMiter().setRealPath(path);
        return (self());
    }


    /**
     *  Sets the owner.
     *  
     *  @param agent the file owner
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T owner(org.osid.authentication.Agent agent) {
        getMiter().setOwner(agent);
        return (self());
    }


    /**
     *  Sets the created time.
     *  
     *  @param time the file created time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T createdTime(org.osid.calendaring.DateTime time) {
        getMiter().setCreatedTime(time);
        return (self());
    }

 
    /**
     *  Sets the modified time.
     *  
     *  @param time the file modified time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T modifiedTime(org.osid.calendaring.DateTime time) {
        getMiter().setLastModifiedTime(time);
        return (self());
    }


    /**
     *  Sets the accessed time.
     *  
     *  @param time the file accessed time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T accessTime(org.osid.calendaring.DateTime time) {
        getMiter().setLastAccessTime(time);
        return (self());
    }

    
    /**
     *  Adds a Directory record.
     *
     *  @param record a directory record
     *  @param recordType the type of directory record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.filing.records.DirectoryRecord record, org.osid.type.Type recordType) {
        getMiter().addDirectoryRecord(record, recordType);
        return (self());
    }
}       


