//
// AbstractAssemblyPostQuery.java
//
//     A PostQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.forum.post.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PostQuery that stores terms.
 */

public abstract class AbstractAssemblyPostQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.forum.PostQuery,
               org.osid.forum.PostQueryInspector,
               org.osid.forum.PostSearchOrder {

    private final java.util.Collection<org.osid.forum.records.PostQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.forum.records.PostQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.forum.records.PostSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPostQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPostQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches entries whose sent time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime startTime, 
                               org.osid.calendaring.DateTime endTime, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimestampColumn(), startTime, endTime, match);
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        getAssembler().clearTerms(getTimestampColumn());
        return;
    }


    /**
     *  Gets the timestamp terms. 
     *
     *  @return the timestamp terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getTimestampTerms() {
        return (getAssembler().getDateTimeTerms(getTimestampColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the timestamp. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimestamp(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimestampColumn(), style);
        return;
    }


    /**
     *  Gets the Timestamp column name.
     *
     * @return the column name
     */

    protected String getTimestampColumn() {
        return ("timestamp");
    }


    /**
     *  Matches the poster of the entry. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPosterId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getPosterIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the poster <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPosterIdTerms() {
        getAssembler().clearTerms(getPosterIdColumn());
        return;
    }


    /**
     *  Gets the poster <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPosterIdTerms() {
        return (getAssembler().getIdTerms(getPosterIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the poster. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPoster(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPosterColumn(), style);
        return;
    }


    /**
     *  Gets the PosterId column name.
     *
     * @return the column name
     */

    protected String getPosterIdColumn() {
        return ("poster_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  senders. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsPosterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getPosterQuery() {
        throw new org.osid.UnimplementedException("supportsPosterQuery() is false");
    }


    /**
     *  Clears the poster terms. 
     */

    @OSID @Override
    public void clearPosterTerms() {
        getAssembler().clearTerms(getPosterColumn());
        return;
    }


    /**
     *  Gets the poster terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getPosterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order interface is available. 
     *
     *  @return <code> true </code> if a poster order interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the poster order interface. 
     *
     *  @return the poster search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPosterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getPosterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPosterSearchOrder() is false");
    }


    /**
     *  Gets the Poster column name.
     *
     * @return the column name
     */

    protected String getPosterColumn() {
        return ("poster");
    }


    /**
     *  Matches the posting agent of the entry. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPostingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getPostingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the posting agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostingAgentIdTerms() {
        getAssembler().clearTerms(getPostingAgentIdColumn());
        return;
    }


    /**
     *  Gets the poster <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostingAgentIdTerms() {
        return (getAssembler().getIdTerms(getPostingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the posting 
     *  agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPostingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPostingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the PostingAgentId column name.
     *
     * @return the column name
     */

    protected String getPostingAgentIdColumn() {
        return ("posting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  posters. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getPostingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsPostingAgentQuery() is false");
    }


    /**
     *  Clears the posting agent terms. 
     */

    @OSID @Override
    public void clearPostingAgentTerms() {
        getAssembler().clearTerms(getPostingAgentColumn());
        return;
    }


    /**
     *  Gets the poster terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getPostingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if a posting agent order interface is available. 
     *
     *  @return <code> true </code> if a posting agent order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the posting agent search order interface. 
     *
     *  @return the posting agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getPostingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPostingAgentSearchOrder() is false");
    }


    /**
     *  Gets the PostingAgent column name.
     *
     * @return the column name
     */

    protected String getPostingAgentColumn() {
        return ("posting_agent");
    }


    /**
     *  Adds a subject line to match. Multiple subject line matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subject display name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> subject is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> subject </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSubjectLine(String subject, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getSubjectLineColumn(), subject, stringMatchType, match);
        return;
    }


    /**
     *  Matches entries with any subject line. 
     *
     *  @param  match <code> true </code> to match entries with any subject 
     *          line, <code> false </code> to match entries with no subject 
     *          line 
     */

    @OSID @Override
    public void matchAnySubjectLine(boolean match) {
        getAssembler().addStringWildcardTerm(getSubjectLineColumn(), match);
        return;
    }


    /**
     *  Clears the subject line terms. 
     */

    @OSID @Override
    public void clearSubjectLineTerms() {
        getAssembler().clearTerms(getSubjectLineColumn());
        return;
    }


    /**
     *  Gets the subject line terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSubjectLineTerms() {
        return (getAssembler().getStringTerms(getSubjectLineColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  subject.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubjectLine(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubjectLineColumn(), style);
        return;
    }


    /**
     *  Gets the SubjectLine column name.
     *
     * @return the column name
     */

    protected String getSubjectLineColumn() {
        return ("subject_line");
    }


    /**
     *  Adds text to match. Multiple text matches can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  text text to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getTextColumn(), text, stringMatchType, match);
        return;
    }


    /**
     *  Matches entries with any text. 
     *
     *  @param  match <code> true </code> to match entries with any text, 
     *          <code> false </code> to match entries with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        getAssembler().addStringWildcardTerm(getTextColumn(), match);
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        getAssembler().clearTerms(getTextColumn());
        return;
    }


    /**
     *  Gets the text terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (getAssembler().getStringTerms(getTextColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTextColumn(), style);
        return;
    }


    /**
     *  Gets the Text column name.
     *
     * @return the column name
     */

    protected String getTextColumn() {
        return ("text");
    }


    /**
     *  Sets the reply <code> Id </code> for this query to match replies 
     *  assigned to posts. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReplyId(org.osid.id.Id replyId, boolean match) {
        getAssembler().addIdTerm(getReplyIdColumn(), replyId, match);
        return;
    }


    /**
     *  Clears the reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReplyIdTerms() {
        getAssembler().clearTerms(getReplyIdColumn());
        return;
    }


    /**
     *  Gets the reply <code> Id </code> terms. 
     *
     *  @return the reply <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReplyIdTerms() {
        return (getAssembler().getIdTerms(getReplyIdColumn()));
    }


    /**
     *  Gets the ReplyId column name.
     *
     * @return the column name
     */

    protected String getReplyIdColumn() {
        return ("reply_id");
    }


    /**
     *  Tests if a reply query is available. 
     *
     *  @return <code> true </code> if a reply query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post. 
     *
     *  @return the reply query 
     *  @throws org.osid.UnimplementedException <code> supportsReplyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getReplyQuery() {
        throw new org.osid.UnimplementedException("supportsReplyQuery() is false");
    }


    /**
     *  Matches posts with any reply. 
     *
     *  @param  match <code> true </code> to match posts with any reply, 
     *          <code> false </code> to match posts with no replies 
     */

    @OSID @Override
    public void matchAnyReply(boolean match) {
        getAssembler().addIdWildcardTerm(getReplyColumn(), match);
        return;
    }


    /**
     *  Clears the reply terms. 
     */

    @OSID @Override
    public void clearReplyTerms() {
        getAssembler().clearTerms(getReplyColumn());
        return;
    }


    /**
     *  Gets the reply terms. 
     *
     *  @return the reply terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getReplyTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the Reply column name.
     *
     * @return the column name
     */

    protected String getReplyColumn() {
        return ("reply");
    }


    /**
     *  Sets the post <code> Id </code> for this query to match replies 
     *  assigned to forums. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchForumId(org.osid.id.Id forumId, boolean match) {
        getAssembler().addIdTerm(getForumIdColumn(), forumId, match);
        return;
    }


    /**
     *  Clears the forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearForumIdTerms() {
        getAssembler().clearTerms(getForumIdColumn());
        return;
    }


    /**
     *  Gets the forum <code> Id </code> terms. 
     *
     *  @return the forum <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getForumIdTerms() {
        return (getAssembler().getIdTerms(getForumIdColumn()));
    }


    /**
     *  Gets the ForumId column name.
     *
     * @return the column name
     */

    protected String getForumIdColumn() {
        return ("forum_id");
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> supportsForumQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getForumQuery() {
        throw new org.osid.UnimplementedException("supportsForumQuery() is false");
    }


    /**
     *  Clears the forum terms. 
     */

    @OSID @Override
    public void clearForumTerms() {
        getAssembler().clearTerms(getForumColumn());
        return;
    }


    /**
     *  Gets the forum terms. 
     *
     *  @return the forum terms 
     */

    @OSID @Override
    public org.osid.forum.ForumQueryInspector[] getForumTerms() {
        return (new org.osid.forum.ForumQueryInspector[0]);
    }


    /**
     *  Gets the Forum column name.
     *
     * @return the column name
     */

    protected String getForumColumn() {
        return ("forum");
    }


    /**
     *  Tests if this post supports the given record
     *  <code>Type</code>.
     *
     *  @param  postRecordType a post record type 
     *  @return <code>true</code> if the postRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type postRecordType) {
        for (org.osid.forum.records.PostQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  postRecordType the post record type 
     *  @return the post query record 
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.PostQueryRecord getPostQueryRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.PostQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  postRecordType the post record type 
     *  @return the post query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.PostQueryInspectorRecord getPostQueryInspectorRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.PostQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param postRecordType the post record type
     *  @return the post search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.PostSearchOrderRecord getPostSearchOrderRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.PostSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this post. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param postQueryRecord the post query record
     *  @param postQueryInspectorRecord the post query inspector
     *         record
     *  @param postSearchOrderRecord the post search order record
     *  @param postRecordType post record type
     *  @throws org.osid.NullArgumentException
     *          <code>postQueryRecord</code>,
     *          <code>postQueryInspectorRecord</code>,
     *          <code>postSearchOrderRecord</code> or
     *          <code>postRecordTypepost</code> is
     *          <code>null</code>
     */
            
    protected void addPostRecords(org.osid.forum.records.PostQueryRecord postQueryRecord, 
                                      org.osid.forum.records.PostQueryInspectorRecord postQueryInspectorRecord, 
                                      org.osid.forum.records.PostSearchOrderRecord postSearchOrderRecord, 
                                      org.osid.type.Type postRecordType) {

        addRecordType(postRecordType);

        nullarg(postQueryRecord, "post query record");
        nullarg(postQueryInspectorRecord, "post query inspector record");
        nullarg(postSearchOrderRecord, "post search order record");

        this.queryRecords.add(postQueryRecord);
        this.queryInspectorRecords.add(postQueryInspectorRecord);
        this.searchOrderRecords.add(postSearchOrderRecord);
        
        return;
    }
}
