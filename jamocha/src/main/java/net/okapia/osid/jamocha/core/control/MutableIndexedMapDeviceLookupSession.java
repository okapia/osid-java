//
// MutableIndexedMapDeviceLookupSession
//
//    Implements a Device lookup service backed by a collection of
//    devices indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Device lookup service backed by a collection of
 *  devices. The devices are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some devices may be compatible
 *  with more types than are indicated through these device
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of devices can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapDeviceLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractIndexedMapDeviceLookupSession
    implements org.osid.control.DeviceLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapDeviceLookupSession} with no devices.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system}
     *          is {@code null}
     */

      public MutableIndexedMapDeviceLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDeviceLookupSession} with a
     *  single device.
     *  
     *  @param system the system
     *  @param  device a single device
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code device} is {@code null}
     */

    public MutableIndexedMapDeviceLookupSession(org.osid.control.System system,
                                                  org.osid.control.Device device) {
        this(system);
        putDevice(device);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDeviceLookupSession} using an
     *  array of devices.
     *
     *  @param system the system
     *  @param  devices an array of devices
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code devices} is {@code null}
     */

    public MutableIndexedMapDeviceLookupSession(org.osid.control.System system,
                                                  org.osid.control.Device[] devices) {
        this(system);
        putDevices(devices);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDeviceLookupSession} using a
     *  collection of devices.
     *
     *  @param system the system
     *  @param  devices a collection of devices
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code devices} is {@code null}
     */

    public MutableIndexedMapDeviceLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.Device> devices) {

        this(system);
        putDevices(devices);
        return;
    }
    

    /**
     *  Makes a {@code Device} available in this session.
     *
     *  @param  device a device
     *  @throws org.osid.NullArgumentException {@code device{@code  is
     *          {@code null}
     */

    @Override
    public void putDevice(org.osid.control.Device device) {
        super.putDevice(device);
        return;
    }


    /**
     *  Makes an array of devices available in this session.
     *
     *  @param  devices an array of devices
     *  @throws org.osid.NullArgumentException {@code devices{@code 
     *          is {@code null}
     */

    @Override
    public void putDevices(org.osid.control.Device[] devices) {
        super.putDevices(devices);
        return;
    }


    /**
     *  Makes collection of devices available in this session.
     *
     *  @param  devices a collection of devices
     *  @throws org.osid.NullArgumentException {@code device{@code  is
     *          {@code null}
     */

    @Override
    public void putDevices(java.util.Collection<? extends org.osid.control.Device> devices) {
        super.putDevices(devices);
        return;
    }


    /**
     *  Removes a Device from this session.
     *
     *  @param deviceId the {@code Id} of the device
     *  @throws org.osid.NullArgumentException {@code deviceId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDevice(org.osid.id.Id deviceId) {
        super.removeDevice(deviceId);
        return;
    }    
}
