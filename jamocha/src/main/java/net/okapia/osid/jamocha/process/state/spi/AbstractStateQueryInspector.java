//
// AbstractStateQueryInspector.java
//
//     A template for making a StateQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.state.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for states.
 */

public abstract class AbstractStateQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.process.StateQueryInspector {

    private final java.util.Collection<org.osid.process.records.StateQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the process <code> Id </code> terms. 
     *
     *  @return the process <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the process terms. 
     *
     *  @return the process terms 
     */

    @OSID @Override
    public org.osid.process.ProcessQueryInspector[] getProcessTerms() {
        return (new org.osid.process.ProcessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given state query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a state implementing the requested record.
     *
     *  @param stateRecordType a state record type
     *  @return the state query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stateRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.StateQueryInspectorRecord getStateQueryInspectorRecord(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.StateQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(stateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stateRecordType + " is not supported");
    }


    /**
     *  Adds a record to this state query. 
     *
     *  @param stateQueryInspectorRecord state query inspector
     *         record
     *  @param stateRecordType state record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStateQueryInspectorRecord(org.osid.process.records.StateQueryInspectorRecord stateQueryInspectorRecord, 
                                                   org.osid.type.Type stateRecordType) {

        addRecordType(stateRecordType);
        nullarg(stateRecordType, "state record type");
        this.records.add(stateQueryInspectorRecord);        
        return;
    }
}
