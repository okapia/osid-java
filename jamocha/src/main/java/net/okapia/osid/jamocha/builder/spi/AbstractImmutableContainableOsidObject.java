//
// AbstractImmutableContainableOsidObject
//
//     Defines an immutable wrapper for a Containable OsidObject.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an immutable wrapper for a Containable OsidObject.
 */

public abstract class AbstractImmutableContainableOsidObject
    extends AbstractImmutableOsidObject
    implements org.osid.Containable,
               org.osid.OsidObject {

    private final org.osid.Containable containable;


    /**
     *  Constructs a new
     *  <code>AbstractImmutableContainableOsidObject</code>.
     *
     *  @param object
     *  @throws org.osid.NullArgumentException <code>object</code> 
     *          is <code>null</code>
     */

    protected AbstractImmutableContainableOsidObject(org.osid.OsidObject object) {
        super(object);

        if (!(object instanceof org.osid.Containable)) {
            throw new org.osid.UnsupportedException("object not a Containable");
        }

        this.containable = new ImmutableContainable((org.osid.Containable) object);
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    protected class ImmutableContainable
        extends AbstractImmutableContainable
        implements org.osid.Containable {


        /**
         *  Constructs a new <code>ImmutableContainable</code>.
         *
         *  @param object
         *  @throws org.osid.NullArgumentException <code>object</code>
         *          is <code>null</code>
         */
        
        protected ImmutableContainable(org.osid.Containable object) {
            super(object);
            return;
        }
    }
}
