//
// MutableObjectiveBankBatchFormList.java
//
//     Implements an ObjectiveBankBatchFormList. This list allows ObjectiveBankBatchForms to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.batch.objectivebankbatchform;


/**
 *  <p>Implements an ObjectiveBankBatchFormList. This list allows ObjectiveBankBatchForms to be
 *  added after this objectiveBankBatchForm has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this objectiveBankBatchForm must
 *  invoke <code>eol()</code> when there are no more objectiveBankBatchForms to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>ObjectiveBankBatchFormList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more objectiveBankBatchForms are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more objectiveBankBatchForms to be added.</p>
 */

public final class MutableObjectiveBankBatchFormList
    extends net.okapia.osid.jamocha.learning.batch.objectivebankbatchform.spi.AbstractMutableObjectiveBankBatchFormList
    implements org.osid.learning.batch.ObjectiveBankBatchFormList {


    /**
     *  Creates a new empty <code>MutableObjectiveBankBatchFormList</code>.
     */

    public MutableObjectiveBankBatchFormList() {
        super();
    }


    /**
     *  Creates a new <code>MutableObjectiveBankBatchFormList</code>.
     *
     *  @param objectiveBankBatchForm an <code>ObjectiveBankBatchForm</code>
     *  @throws org.osid.NullArgumentException <code>objectiveBankBatchForm</code>
     *          is <code>null</code>
     */

    public MutableObjectiveBankBatchFormList(org.osid.learning.batch.ObjectiveBankBatchForm objectiveBankBatchForm) {
        super(objectiveBankBatchForm);
        return;
    }


    /**
     *  Creates a new <code>MutableObjectiveBankBatchFormList</code>.
     *
     *  @param array an array of objectivebankbatchforms
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableObjectiveBankBatchFormList(org.osid.learning.batch.ObjectiveBankBatchForm[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableObjectiveBankBatchFormList</code>.
     *
     *  @param collection a java.util.Collection of objectivebankbatchforms
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableObjectiveBankBatchFormList(java.util.Collection<org.osid.learning.batch.ObjectiveBankBatchForm> collection) {
        super(collection);
        return;
    }
}
