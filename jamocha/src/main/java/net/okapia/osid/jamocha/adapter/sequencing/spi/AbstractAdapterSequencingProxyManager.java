//
// AbstractSequencingProxyManager.java
//
//     An adapter for a SequencingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a SequencingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterSequencingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.sequencing.SequencingProxyManager>
    implements org.osid.sequencing.SequencingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterSequencingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterSequencingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterSequencingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterSequencingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any action group federation is exposed. Federation is exposed 
     *  when a specific action group may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of action groups appears as a single action group. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up elements is supported. 
     *
     *  @return <code> true </code> if element lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsElementLookup() {
        return (getAdapteeManager().supportsElementLookup());
    }


    /**
     *  Tests if managing elements is supported. 
     *
     *  @return <code> true </code> if element management is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsElementAdmin() {
        return (getAdapteeManager().supportsElementAdmin());
    }


    /**
     *  Tests if sequencing elements is supported. 
     *
     *  @return <code> true </code> if element sequencing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequencing() {
        return (getAdapteeManager().supportsSequencing());
    }


    /**
     *  Tests if looking up chains is supported. 
     *
     *  @return <code> true </code> if chain lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainLookup() {
        return (getAdapteeManager().supportsChainLookup());
    }


    /**
     *  Tests if querying chains is supported. 
     *
     *  @return <code> true </code> if chain query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainQuery() {
        return (getAdapteeManager().supportsChainQuery());
    }


    /**
     *  Tests if searching chains is supported. 
     *
     *  @return <code> true </code> if chain search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainSearch() {
        return (getAdapteeManager().supportsChainSearch());
    }


    /**
     *  Tests if chain <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if chain administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainAdmin() {
        return (getAdapteeManager().supportsChainAdmin());
    }


    /**
     *  Tests if a chain <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if chain notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainNotification() {
        return (getAdapteeManager().supportsChainNotification());
    }


    /**
     *  Tests if a chain antimatroid lookup service is supported. 
     *
     *  @return <code> true </code> if a chain antimatroid lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainAntimatroid() {
        return (getAdapteeManager().supportsChainAntimatroid());
    }


    /**
     *  Tests if a chain antimatroid assignment service is supported. 
     *
     *  @return <code> true </code> if a chain to antimatroid assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainAntimatroidAssignment() {
        return (getAdapteeManager().supportsChainAntimatroidAssignment());
    }


    /**
     *  Tests if a chain smart antimatroid service is supported. 
     *
     *  @return <code> true </code> if a chain smart antimatroid service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainSmartAntimatroid() {
        return (getAdapteeManager().supportsChainSmartAntimatroid());
    }


    /**
     *  Tests if looking up antimatroids is supported. 
     *
     *  @return <code> true </code> if antimatroid lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidLookup() {
        return (getAdapteeManager().supportsAntimatroidLookup());
    }


    /**
     *  Tests if querying antimatroids is supported. 
     *
     *  @return <code> true </code> if an antimatroid query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidQuery() {
        return (getAdapteeManager().supportsAntimatroidQuery());
    }


    /**
     *  Tests if searching antimatroids is supported. 
     *
     *  @return <code> true </code> if antimatroid search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidSearch() {
        return (getAdapteeManager().supportsAntimatroidSearch());
    }


    /**
     *  Tests if antimatroid administrative service is supported. 
     *
     *  @return <code> true </code> if antimatroid administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidAdmin() {
        return (getAdapteeManager().supportsAntimatroidAdmin());
    }


    /**
     *  Tests if an antimatroid <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if antimatroid notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidNotification() {
        return (getAdapteeManager().supportsAntimatroidNotification());
    }


    /**
     *  Tests for the availability of an antimatroid hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if antimatroid hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidHierarchy() {
        return (getAdapteeManager().supportsAntimatroidHierarchy());
    }


    /**
     *  Tests for the availability of an antimatroid hierarchy design service. 
     *
     *  @return <code> true </code> if antimatroid hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidHierarchyDesign() {
        return (getAdapteeManager().supportsAntimatroidHierarchyDesign());
    }


    /**
     *  Gets the supported <code> Chain </code> record types. 
     *
     *  @return a list containing the supported <code> Chain </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChainRecordTypes() {
        return (getAdapteeManager().getChainRecordTypes());
    }


    /**
     *  Tests if the given <code> Chain </code> record type is supported. 
     *
     *  @param  chainRecordType a <code> Type </code> indicating a <code> 
     *          Chain </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> chainRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChainRecordType(org.osid.type.Type chainRecordType) {
        return (getAdapteeManager().supportsChainRecordType(chainRecordType));
    }


    /**
     *  Gets the supported <code> Chain </code> search types. 
     *
     *  @return a list containing the supported <code> Chain </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChainSearchRecordTypes() {
        return (getAdapteeManager().getChainSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Chain </code> search type is supported. 
     *
     *  @param  chainSearchRecordType a <code> Type </code> indicating a 
     *          <code> Chain </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> chainSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChainSearchRecordType(org.osid.type.Type chainSearchRecordType) {
        return (getAdapteeManager().supportsChainSearchRecordType(chainSearchRecordType));
    }


    /**
     *  Gets the supported <code> Antimatroid </code> record types. 
     *
     *  @return a list containing the supported <code> Antimatroid </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAntimatroidRecordTypes() {
        return (getAdapteeManager().getAntimatroidRecordTypes());
    }


    /**
     *  Tests if the given <code> Antimatroid </code> record type is 
     *  supported. 
     *
     *  @param  antimatroidRecordType a <code> Type </code> indicating an 
     *          <code> Antimatroid </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> antimatroidRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAntimatroidRecordType(org.osid.type.Type antimatroidRecordType) {
        return (getAdapteeManager().supportsAntimatroidRecordType(antimatroidRecordType));
    }


    /**
     *  Gets the supported <code> Antimatroid </code> search record types. 
     *
     *  @return a list containing the supported <code> Antimatroid </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAntimatroidSearchRecordTypes() {
        return (getAdapteeManager().getAntimatroidSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Antimatroid </code> search record type is 
     *  supported. 
     *
     *  @param  antimatroidSearchRecordType a <code> Type </code> indicating 
     *          an <code> Antimatroid </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          antimatroidSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAntimatroidSearchRecordType(org.osid.type.Type antimatroidSearchRecordType) {
        return (getAdapteeManager().supportsAntimatroidSearchRecordType(antimatroidSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ElementLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementLookupSession getElementLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getElementLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element lookup 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @param  proxy a proxy 
     *  @return an <code> ElementLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementLookupSession getElementLookupSessionForAntimatroid(org.osid.id.Id antimatroidId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getElementLookupSessionForAntimatroid(antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element admin 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ElementAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementAdminSession getElementAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getElementAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element admin 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @param  proxy a proxy 
     *  @return an <code> ElementAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementAdminSession getElementAdminSessionForAntimatroid(org.osid.id.Id antimatroidId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getElementAdminSessionForAntimatroid(antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element 
     *  sequencing service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequencingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSequencingn() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingSession getSequencingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequencingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element 
     *  sequencing service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @param  proxy a proxy 
     *  @return a <code> SequencingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSequencing() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingSession getSequencingSessionForAntimatroid(org.osid.id.Id antimatroidId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequencingSessionForAntimatroid(antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChainLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainLookupSession getChainLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain lookup 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @param  proxy a proxy 
     *  @return a <code> ChainLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainLookupSession getChainLookupSessionForAntimatroid(org.osid.id.Id antimatroidId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainLookupSessionForAntimatroid(antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChainQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQuerySession getChainQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain query 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ChainQuerySession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQuerySession getChainQuerySessionForAntimatroid(org.osid.id.Id antimatroidId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainQuerySessionForAntimatroid(antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChainSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainSearchSession getChainSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain search 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ChainSearchSession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainSearchSession getChainSearchSessionForAntimatroid(org.osid.id.Id antimatroidId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainSearchSessionForAntimatroid(antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChainAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAdminSession getChainAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  administration service for the given input. 
     *
     *  @param  inputId the <code> Id </code> of the <code> Input </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ChainAdminSession </code> 
     *  @throws org.osid.NotFoundException no input found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAdminSession getChainAdminSessionForInput(org.osid.id.Id inputId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAdminSessionForInput(inputId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  notification service. 
     *
     *  @param  chainReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ChainNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> chainReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainNotificationSession getChainNotificationSession(org.osid.sequencing.ChainReceiver chainReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainNotificationSession(chainReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  notification service for the given antimatroid. 
     *
     *  @param  chainReceiver the notification callback 
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ChainNotificationSession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> chainReceiver, </code> 
     *          <code> antimatroidId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainNotificationSession getChainNotificationSessionForAntimatroid(org.osid.sequencing.ChainReceiver chainReceiver, 
                                                                                                  org.osid.id.Id antimatroidId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainNotificationSessionForAntimatroid(chainReceiver, antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup chain/antimatroid 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChainAntimatroidSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainAntimatroid() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAntimatroidSession getChainAntimatroidSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAntimatroidSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning chains 
     *  to antimatroids. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChainAntimatroidAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainAntimatroidAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAntimatroidAssignmentSession getChainAntimatroidAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAntimatroidAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  antimatroids. 
     *
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ChainSmartAntimatroidSession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainSmartAntimatroid() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainSmartAntimatroidSession getChainSmartAntimatroidSession(org.osid.id.Id antimatroidId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainSmartAntimatroidSession(antimatroidId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AntimatroidLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidLookupSession getAntimatroidLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AntimatroidQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuerySession getAntimatroidQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AntimatroidSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidSearchSession getAntimatroidSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AntimatroidAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidAdminSession getAntimatroidAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  notification service. 
     *
     *  @param  antimatroidReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AntimatroidNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidNotificationSession getAntimatroidNotificationSession(org.osid.sequencing.AntimatroidReceiver antimatroidReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidNotificationSession(antimatroidReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AntimatroidHierarchySession </code> for antimatroids 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidHierarchySession getAntimatroidHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> HierarchyDesignSession </code> for antimatroids 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidHierarchyDesignSession getAntimatroidHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidHierarchyDesignSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
