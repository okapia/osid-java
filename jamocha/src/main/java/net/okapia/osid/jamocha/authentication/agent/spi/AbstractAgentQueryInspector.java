//
// AbstractAgentQueryInspector.java
//
//     A template for making an AgentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for agents.
 */

public abstract class AbstractAgentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.authentication.AgentQueryInspector {

    private final java.util.Collection<org.osid.authentication.records.AgentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agency <code> Id </code> terms. 
     *
     *  @return the agency <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agency terms. 
     *
     *  @return the agency terms 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQueryInspector[] getAgencyTerms() {
        return (new org.osid.authentication.AgencyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given agent query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an agent implementing the requested record.
     *
     *  @param agentRecordType an agent record type
     *  @return the agent query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgentQueryInspectorRecord getAgentQueryInspectorRecord(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(agentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agent query. 
     *
     *  @param agentQueryInspectorRecord agent query inspector
     *         record
     *  @param agentRecordType agent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgentQueryInspectorRecord(org.osid.authentication.records.AgentQueryInspectorRecord agentQueryInspectorRecord, 
                                                   org.osid.type.Type agentRecordType) {

        addRecordType(agentRecordType);
        nullarg(agentRecordType, "agent record type");
        this.records.add(agentQueryInspectorRecord);        
        return;
    }
}
