//
// AbstractFamilyQueryInspector.java
//
//     A template for making a FamilyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.family.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for families.
 */

public abstract class AbstractFamilyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.relationship.FamilyQueryInspector {

    private final java.util.Collection<org.osid.relationship.records.FamilyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the relationship <code> Id </code> terms. 
     *
     *  @return the relationship <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the relationship terms. 
     *
     *  @return the relationship terms 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQueryInspector[] getRelationshipTerms() {
        return (new org.osid.relationship.RelationshipQueryInspector[0]);
    }


    /**
     *  Gets the ancestor family <code> Id </code> terms. 
     *
     *  @return the ancestor family <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorFamilyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor family terms. 
     *
     *  @return the ancestor family terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getAncestorFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }


    /**
     *  Gets the descendant family <code> Id </code> terms. 
     *
     *  @return the descendant family <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantFamilyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant family terms. 
     *
     *  @return the descendant family terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getDescendantFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given family query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a family implementing the requested record.
     *
     *  @param familyRecordType a family record type
     *  @return the family query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(familyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.FamilyQueryInspectorRecord getFamilyQueryInspectorRecord(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.FamilyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(familyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(familyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this family query. 
     *
     *  @param familyQueryInspectorRecord family query inspector
     *         record
     *  @param familyRecordType family record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFamilyQueryInspectorRecord(org.osid.relationship.records.FamilyQueryInspectorRecord familyQueryInspectorRecord, 
                                                   org.osid.type.Type familyRecordType) {

        addRecordType(familyRecordType);
        nullarg(familyRecordType, "family record type");
        this.records.add(familyQueryInspectorRecord);        
        return;
    }
}
