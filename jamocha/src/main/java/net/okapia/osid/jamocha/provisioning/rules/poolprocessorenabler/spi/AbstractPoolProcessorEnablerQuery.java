//
// AbstractPoolProcessorEnablerQuery.java
//
//     A template for making a PoolProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for pool processor enablers.
 */

public abstract class AbstractPoolProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.provisioning.rules.PoolProcessorEnablerQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the pool processor. 
     *
     *  @param  poolProcessorId the pool processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPoolProcessorId(org.osid.id.Id poolProcessorId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the pool processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPoolProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPoolProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the pool processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPoolProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorQuery getRuledPoolProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPoolProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any pool processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any pool 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          pool processors 
     */

    @OSID @Override
    public void matchAnyRuledPoolProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the pool processor query terms. 
     */

    @OSID @Override
    public void clearRuledPoolProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given pool processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool processor enabler implementing the requested record.
     *
     *  @param poolProcessorEnablerRecordType a pool processor enabler record type
     *  @return the pool processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord getPoolProcessorEnablerQueryRecord(org.osid.type.Type poolProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool processor enabler query. 
     *
     *  @param poolProcessorEnablerQueryRecord pool processor enabler query record
     *  @param poolProcessorEnablerRecordType poolProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolProcessorEnablerQueryRecord(org.osid.provisioning.rules.records.PoolProcessorEnablerQueryRecord poolProcessorEnablerQueryRecord, 
                                          org.osid.type.Type poolProcessorEnablerRecordType) {

        addRecordType(poolProcessorEnablerRecordType);
        nullarg(poolProcessorEnablerQueryRecord, "pool processor enabler query record");
        this.records.add(poolProcessorEnablerQueryRecord);        
        return;
    }
}
