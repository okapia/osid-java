//
// AbstractAdapterBinLookupSession.java
//
//    A Bin lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Bin lookup session adapter.
 */

public abstract class AbstractAdapterBinLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.BinLookupSession {

    private final org.osid.resource.BinLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBinLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBinLookupSession(org.osid.resource.BinLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Bin} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBins() {
        return (this.session.canLookupBins());
    }


    /**
     *  A complete view of the {@code Bin} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBinView() {
        this.session.useComparativeBinView();
        return;
    }


    /**
     *  A complete view of the {@code Bin} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBinView() {
        this.session.usePlenaryBinView();
        return;
    }

     
    /**
     *  Gets the {@code Bin} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Bin} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Bin} and
     *  retained for compatibility.
     *
     *  @param binId {@code Id} of the {@code Bin}
     *  @return the bin
     *  @throws org.osid.NotFoundException {@code binId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code binId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBin(binId));
    }


    /**
     *  Gets a {@code BinList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  bins specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Bins} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  binIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Bin} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code binIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByIds(org.osid.id.IdList binIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBinsByIds(binIds));
    }


    /**
     *  Gets a {@code BinList} corresponding to the given
     *  bin genus {@code Type} which does not include
     *  bins of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  binGenusType a bin genus type 
     *  @return the returned {@code Bin} list
     *  @throws org.osid.NullArgumentException
     *          {@code binGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByGenusType(org.osid.type.Type binGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBinsByGenusType(binGenusType));
    }


    /**
     *  Gets a {@code BinList} corresponding to the given
     *  bin genus {@code Type} and include any additional
     *  bins with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  binGenusType a bin genus type 
     *  @return the returned {@code Bin} list
     *  @throws org.osid.NullArgumentException
     *          {@code binGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByParentGenusType(org.osid.type.Type binGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBinsByParentGenusType(binGenusType));
    }


    /**
     *  Gets a {@code BinList} containing the given
     *  bin record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  binRecordType a bin record type 
     *  @return the returned {@code Bin} list
     *  @throws org.osid.NullArgumentException
     *          {@code binRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByRecordType(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBinsByRecordType(binRecordType));
    }


    /**
     *  Gets a {@code BinList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Bin} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBinsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Bins}. 
     *
     *  In plenary mode, the returned list contains all known
     *  bins or an error results. Otherwise, the returned list
     *  may contain only those bins that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Bins} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBins()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBins());
    }
}
