//
// AbstractTopologyBatchProxyManager.java
//
//     An adapter for a TopologyBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TopologyBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTopologyBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.topology.batch.TopologyBatchProxyManager>
    implements org.osid.topology.batch.TopologyBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterTopologyBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTopologyBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTopologyBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTopologyBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of nodes is available. 
     *
     *  @return <code> true </code> if a node bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeBatchAdmin() {
        return (getAdapteeManager().supportsNodeBatchAdmin());
    }


    /**
     *  Tests if bulk administration of edges is available. 
     *
     *  @return <code> true </code> if an edge bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeBatchAdmin() {
        return (getAdapteeManager().supportsEdgeBatchAdmin());
    }


    /**
     *  Tests if bulk administration of graph is available. 
     *
     *  @return <code> true </code> if a graph bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphBatchAdmin() {
        return (getAdapteeManager().supportsGraphBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk node 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.NodeBatchAdminSession getNodeBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk node 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> NodeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.NodeBatchAdminSession getNodeBatchAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeBatchAdminSessionForGraph(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk edge 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.EdgeBatchAdminSession getEdgeBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk edge 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.EdgeBatchAdminSession getEdgeBatchAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeBatchAdminSessionForGraph(graphId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk graph 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GraphBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.GraphBatchAdminSession getGraphBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
