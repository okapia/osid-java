//
// AbstractFederatingAuthorizationEnablerLookupSession.java
//
//     An abstract federating adapter for an AuthorizationEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authorization.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AuthorizationEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.</p>
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAuthorizationEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authorization.rules.AuthorizationEnablerLookupSession>
    implements org.osid.authorization.rules.AuthorizationEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();


    /**
     *  Constructs a new <code>AbstractFederatingAuthorizationEnablerLookupSession</code>.
     */

    protected AbstractFederatingAuthorizationEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authorization.rules.AuthorizationEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Vault/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.vault.getId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.vault);
    }


    /**
     *  Sets the <code>Vault</code>.
     *
     *  @param  vault the vault for this session
     *  @throws org.osid.NullArgumentException <code>vault</code>
     *          is <code>null</code>
     */

    protected void setVault(org.osid.authorization.Vault vault) {
        nullarg(vault, "vault");
        this.vault = vault;
        return;
    }


    /**
     *  Tests if this user can perform <code>AuthorizationEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuthorizationEnablers() {
        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            if (session.canLookupAuthorizationEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AuthorizationEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuthorizationEnablerView() {
        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            session.useComparativeAuthorizationEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AuthorizationEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuthorizationEnablerView() {
        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            session.usePlenaryAuthorizationEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include authorization enablers in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            session.useFederatedVaultView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            session.useIsolatedVaultView();
        }

        return;
    }


    /**
     *  Only active authorization enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveAuthorizationEnablerView() {
        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            session.useActiveAuthorizationEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive authorization enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusAuthorizationEnablerView() {
        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            session.useAnyStatusAuthorizationEnablerView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AuthorizationEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuthorizationEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AuthorizationEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @param  authorizationEnablerId <code>Id</code> of the
     *          <code>AuthorizationEnabler</code>
     *  @return the authorization enabler
     *  @throws org.osid.NotFoundException <code>authorizationEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>authorizationEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnabler getAuthorizationEnabler(org.osid.id.Id authorizationEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            try {
                return (session.getAuthorizationEnabler(authorizationEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(authorizationEnablerId + " not found");
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  authorizationEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>AuthorizationEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @param  authorizationEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByIds(org.osid.id.IdList authorizationEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authorization.rules.authorizationenabler.MutableAuthorizationEnablerList ret = new net.okapia.osid.jamocha.authorization.rules.authorizationenabler.MutableAuthorizationEnablerList();

        try (org.osid.id.IdList ids = authorizationEnablerIds) {
            while (ids.hasNext()) {
                ret.addAuthorizationEnabler(getAuthorizationEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> corresponding to
     *  the given authorization enabler genus <code>Type</code> which
     *  does not include authorization enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the
     *  returned list may contain only those authorization enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @param  authorizationEnablerGenusType an authorizationEnabler genus type 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByGenusType(org.osid.type.Type authorizationEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.FederatingAuthorizationEnablerList ret = getAuthorizationEnablerList();

        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            ret.addAuthorizationEnablerList(session.getAuthorizationEnablersByGenusType(authorizationEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> corresponding to
     *  the given authorization enabler genus <code>Type</code> and
     *  include any additional authorization enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the
     *  returned list may contain only those authorization enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @param  authorizationEnablerGenusType an authorizationEnabler genus type 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByParentGenusType(org.osid.type.Type authorizationEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.FederatingAuthorizationEnablerList ret = getAuthorizationEnablerList();

        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            ret.addAuthorizationEnablerList(session.getAuthorizationEnablersByParentGenusType(authorizationEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> containing the
     *  given authorization enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the
     *  returned list may contain only those authorization enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @param  authorizationEnablerRecordType an authorizationEnabler record type 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByRecordType(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.FederatingAuthorizationEnablerList ret = getAuthorizationEnablerList();

        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            ret.addAuthorizationEnablerList(session.getAuthorizationEnablersByRecordType(authorizationEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the
     *  returned list may contain only those authorization enablers
     *  that are accessible through this session.
     *  
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AuthorizationEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.FederatingAuthorizationEnablerList ret = getAuthorizationEnablerList();

        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            ret.addAuthorizationEnablerList(session.getAuthorizationEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>AuthorizationEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the
     *  returned list may contain only those authorization enablers
     *  that are accessible through this session.
     *
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.FederatingAuthorizationEnablerList ret = getAuthorizationEnablerList();

        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            ret.addAuthorizationEnablerList(session.getAuthorizationEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AuthorizationEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  authorization enablers or an error results. Otherwise, the
     *  returned list may contain only those authorization enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, authorization enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  authorization enablers are returned.
     *
     *  @return a list of <code>AuthorizationEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.FederatingAuthorizationEnablerList ret = getAuthorizationEnablerList();

        for (org.osid.authorization.rules.AuthorizationEnablerLookupSession session : getSessions()) {
            ret.addAuthorizationEnablerList(session.getAuthorizationEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.FederatingAuthorizationEnablerList getAuthorizationEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.ParallelAuthorizationEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.rules.authorizationenabler.CompositeAuthorizationEnablerList());
        }
    }
}
