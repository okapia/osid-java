//
// AbstractDepotSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.depot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDepotSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.installation.DepotSearchResults {

    private org.osid.installation.DepotList depots;
    private final org.osid.installation.DepotQueryInspector inspector;
    private final java.util.Collection<org.osid.installation.records.DepotSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDepotSearchResults.
     *
     *  @param depots the result set
     *  @param depotQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>depots</code>
     *          or <code>depotQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDepotSearchResults(org.osid.installation.DepotList depots,
                                            org.osid.installation.DepotQueryInspector depotQueryInspector) {
        nullarg(depots, "depots");
        nullarg(depotQueryInspector, "depot query inspectpr");

        this.depots = depots;
        this.inspector = depotQueryInspector;

        return;
    }


    /**
     *  Gets the depot list resulting from a search.
     *
     *  @return a depot list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepots() {
        if (this.depots == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.installation.DepotList depots = this.depots;
        this.depots = null;
	return (depots);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.installation.DepotQueryInspector getDepotQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  depot search record <code> Type. </code> This method must
     *  be used to retrieve a depot implementing the requested
     *  record.
     *
     *  @param depotSearchRecordType a depot search 
     *         record type 
     *  @return the depot search
     *  @throws org.osid.NullArgumentException
     *          <code>depotSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(depotSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.DepotSearchResultsRecord getDepotSearchResultsRecord(org.osid.type.Type depotSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.installation.records.DepotSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(depotSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(depotSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record depot search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDepotRecord(org.osid.installation.records.DepotSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "depot record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
