//
// AbstractMapOublietteLookupSession
//
//    A simple framework for providing an Oubliette lookup service
//    backed by a fixed collection of oubliettes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Oubliette lookup service backed by a
 *  fixed collection of oubliettes. The oubliettes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Oubliettes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOublietteLookupSession
    extends net.okapia.osid.jamocha.hold.spi.AbstractOublietteLookupSession
    implements org.osid.hold.OublietteLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.hold.Oubliette> oubliettes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.hold.Oubliette>());


    /**
     *  Makes an <code>Oubliette</code> available in this session.
     *
     *  @param  oubliette an oubliette
     *  @throws org.osid.NullArgumentException <code>oubliette<code>
     *          is <code>null</code>
     */

    protected void putOubliette(org.osid.hold.Oubliette oubliette) {
        this.oubliettes.put(oubliette.getId(), oubliette);
        return;
    }


    /**
     *  Makes an array of oubliettes available in this session.
     *
     *  @param  oubliettes an array of oubliettes
     *  @throws org.osid.NullArgumentException <code>oubliettes<code>
     *          is <code>null</code>
     */

    protected void putOubliettes(org.osid.hold.Oubliette[] oubliettes) {
        putOubliettes(java.util.Arrays.asList(oubliettes));
        return;
    }


    /**
     *  Makes a collection of oubliettes available in this session.
     *
     *  @param  oubliettes a collection of oubliettes
     *  @throws org.osid.NullArgumentException <code>oubliettes<code>
     *          is <code>null</code>
     */

    protected void putOubliettes(java.util.Collection<? extends org.osid.hold.Oubliette> oubliettes) {
        for (org.osid.hold.Oubliette oubliette : oubliettes) {
            this.oubliettes.put(oubliette.getId(), oubliette);
        }

        return;
    }


    /**
     *  Removes an Oubliette from this session.
     *
     *  @param  oublietteId the <code>Id</code> of the oubliette
     *  @throws org.osid.NullArgumentException <code>oublietteId<code> is
     *          <code>null</code>
     */

    protected void removeOubliette(org.osid.id.Id oublietteId) {
        this.oubliettes.remove(oublietteId);
        return;
    }


    /**
     *  Gets the <code>Oubliette</code> specified by its <code>Id</code>.
     *
     *  @param  oublietteId <code>Id</code> of the <code>Oubliette</code>
     *  @return the oubliette
     *  @throws org.osid.NotFoundException <code>oublietteId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>oublietteId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.hold.Oubliette oubliette = this.oubliettes.get(oublietteId);
        if (oubliette == null) {
            throw new org.osid.NotFoundException("oubliette not found: " + oublietteId);
        }

        return (oubliette);
    }


    /**
     *  Gets all <code>Oubliettes</code>. In plenary mode, the returned
     *  list contains all known oubliettes or an error
     *  results. Otherwise, the returned list may contain only those
     *  oubliettes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Oubliettes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.oubliette.ArrayOublietteList(this.oubliettes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.oubliettes.clear();
        super.close();
        return;
    }
}
