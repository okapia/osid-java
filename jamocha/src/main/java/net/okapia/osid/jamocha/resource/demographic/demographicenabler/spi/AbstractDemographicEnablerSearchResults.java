//
// AbstractDemographicEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographicenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDemographicEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resource.demographic.DemographicEnablerSearchResults {

    private org.osid.resource.demographic.DemographicEnablerList demographicEnablers;
    private final org.osid.resource.demographic.DemographicEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.resource.demographic.records.DemographicEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDemographicEnablerSearchResults.
     *
     *  @param demographicEnablers the result set
     *  @param demographicEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>demographicEnablers</code>
     *          or <code>demographicEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDemographicEnablerSearchResults(org.osid.resource.demographic.DemographicEnablerList demographicEnablers,
                                            org.osid.resource.demographic.DemographicEnablerQueryInspector demographicEnablerQueryInspector) {
        nullarg(demographicEnablers, "demographic enablers");
        nullarg(demographicEnablerQueryInspector, "demographic enabler query inspectpr");

        this.demographicEnablers = demographicEnablers;
        this.inspector = demographicEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the demographic enabler list resulting from a search.
     *
     *  @return a demographic enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablers() {
        if (this.demographicEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resource.demographic.DemographicEnablerList demographicEnablers = this.demographicEnablers;
        this.demographicEnablers = null;
	return (demographicEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resource.demographic.DemographicEnablerQueryInspector getDemographicEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  demographic enabler search record <code> Type. </code> This method must
     *  be used to retrieve a demographicEnabler implementing the requested
     *  record.
     *
     *  @param demographicEnablerSearchRecordType a demographicEnabler search 
     *         record type 
     *  @return the demographic enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(demographicEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerSearchResultsRecord getDemographicEnablerSearchResultsRecord(org.osid.type.Type demographicEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resource.demographic.records.DemographicEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(demographicEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(demographicEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record demographic enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDemographicEnablerRecord(org.osid.resource.demographic.records.DemographicEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "demographic enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
