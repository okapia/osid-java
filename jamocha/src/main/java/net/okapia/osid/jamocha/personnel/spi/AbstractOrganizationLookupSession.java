//
// AbstractOrganizationLookupSession.java
//
//    A starter implementation framework for providing an Organization
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Organization
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getOrganizations(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractOrganizationLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.personnel.OrganizationLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.personnel.Realm realm = new net.okapia.osid.jamocha.nil.personnel.realm.UnknownRealm();
    

    /**
     *  Gets the <code>Realm/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Realm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.realm.getId());
    }


    /**
     *  Gets the <code>Realm</code> associated with this 
     *  session.
     *
     *  @return the <code>Realm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.realm);
    }


    /**
     *  Sets the <code>Realm</code>.
     *
     *  @param  realm the realm for this session
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>
     */

    protected void setRealm(org.osid.personnel.Realm realm) {
        nullarg(realm, "realm");
        this.realm = realm;
        return;
    }


    /**
     *  Tests if this user can perform <code>Organization</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOrganizations() {
        return (true);
    }


    /**
     *  A complete view of the <code>Organization</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOrganizationView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Organization</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOrganizationView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include organizations in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only organizations whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveOrganizationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All organizations of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveOrganizationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Organization</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Organization</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Organization</code> and
     *  retained for compatibility.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @param  organizationId <code>Id</code> of the
     *          <code>Organization</code>
     *  @return the organization
     *  @throws org.osid.NotFoundException <code>organizationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>organizationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Organization getOrganization(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.personnel.OrganizationList organizations = getOrganizations()) {
            while (organizations.hasNext()) {
                org.osid.personnel.Organization organization = organizations.getNextOrganization();
                if (organization.getId().equals(organizationId)) {
                    return (organization);
                }
            }
        } 

        throw new org.osid.NotFoundException(organizationId + " not found");
    }


    /**
     *  Gets an <code>OrganizationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  organizations specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Organizations</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, organizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  organizations and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getOrganizations()</code>.
     *
     *  @param  organizationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Organization</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>organizationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByIds(org.osid.id.IdList organizationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.personnel.Organization> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = organizationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getOrganization(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("organization " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.personnel.organization.LinkedOrganizationList(ret));
    }


    /**
     *  Gets an <code>OrganizationList</code> corresponding to the
     *  given organization genus <code>Type</code> which does not
     *  include organizations of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned
     *  list may contain only those organizations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, organizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  organizations and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getOrganizations()</code>.
     *
     *  @param  organizationGenusType an organization genus type 
     *  @return the returned <code>Organization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>organizationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByGenusType(org.osid.type.Type organizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.organization.OrganizationGenusFilterList(getOrganizations(), organizationGenusType));
    }


    /**
     *  Gets an <code>OrganizationList</code> corresponding to the given
     *  organization genus <code>Type</code> and include any additional
     *  organizations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOrganizations()</code>.
     *
     *  @param  organizationGenusType an organization genus type 
     *  @return the returned <code>Organization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>organizationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByParentGenusType(org.osid.type.Type organizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOrganizationsByGenusType(organizationGenusType));
    }


    /**
     *  Gets an <code>OrganizationList</code> containing the given
     *  organization record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOrganizations()</code>.
     *
     *  @param  organizationRecordType an organization record type 
     *  @return the returned <code>Organization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByRecordType(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.organization.OrganizationRecordFilterList(getOrganizations(), organizationRecordType));
    }


    /**
     *  Gets an <code>OrganizationList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned
     *  list may contain only those organizations that are accessible
     *  through this session.
     *  
     *  In effective mode, organizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  organizations and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Organization</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsOnDate(org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.organization.TemporalOrganizationFilterList(getOrganizations(), from, to));
    }
        

    /**
     *  Gets all <code>Organizations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Organizations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.personnel.OrganizationList getOrganizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the organization list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of organizations
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.personnel.OrganizationList filterOrganizationsOnViews(org.osid.personnel.OrganizationList list)
        throws org.osid.OperationFailedException {

        org.osid.personnel.OrganizationList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.personnel.organization.EffectiveOrganizationFilterList(ret);
        }

        return (ret);
    }
}
