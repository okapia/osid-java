//
// AbstractAssetQuery.java
//
//     A template for making an Asset Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for assets.
 */

public abstract class AbstractAssetQuery    
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQuery
    implements org.osid.repository.AssetQuery {

    private final java.util.Collection<org.osid.repository.records.AssetQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match assets with any title, 
     *          <code> false </code> to match assets with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Matches assets marked as public domain. 
     *
     *  @param  publicDomain public domain flag 
     */

    @OSID @Override
    public void matchPublicDomain(boolean publicDomain) {
        return;
    }


    /**
     *  Matches assets with any public domain value. 
     *
     *  @param  match <code> true </code> to match assets with any public 
     *          domain value, <code> false </code> to match assets with no 
     *          public domain value 
     */

    @OSID @Override
    public void matchAnyPublicDomain(boolean match) {
        return;
    }


    /**
     *  Clears the public domain terms. 
     */

    @OSID @Override
    public void clearPublicDomainTerms() {
        return;
    }


    /**
     *  Adds a copyright for this query. 
     *
     *  @param  copyright copyright string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> copyright </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> copyright </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCopyright(String copyright, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        return;
    }


    /**
     *  Matches assets with any copyright statement. 
     *
     *  @param  match <code> true </code> to match assets with any copyright 
     *          value, <code> false </code> to match assets with no copyright 
     *          value 
     */

    @OSID @Override
    public void matchAnyCopyright(boolean match) {
        return;
    }


    /**
     *  Clears the copyright terms. 
     */

    @OSID @Override
    public void clearCopyrightTerms() {
        return;
    }


    /**
     *  Adds a copyright registration for this query. 
     *
     *  @param  registration copyright registration string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> registration </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> registration </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCopyrightRegistration(String registration, 
                                           org.osid.type.Type stringMatchType, 
                                           boolean match) {
        return;
    }


    /**
     *  Matches assets with any copyright registration. 
     *
     *  @param  match <code> true </code> to match assets with any copyright 
     *          registration value, <code> false </code> to match assets with 
     *          no copyright registration value 
     */

    @OSID @Override
    public void matchAnyCopyrightRegistration(boolean match) {
        return;
    }


    /**
     *  Clears the copyright registration terms. 
     */

    @OSID @Override
    public void clearCopyrightRegistrationTerms() {
        return;
    }


    /**
     *  Matches assets marked as distributable. 
     *
     *  @param  distributable distribute verbatim rights flag 
     */

    @OSID @Override
    public void matchDistributeVerbatim(boolean distributable) {
        return;
    }


    /**
     *  Clears the distribute verbatim terms. 
     */

    @OSID @Override
    public void clearDistributeVerbatimTerms() {
        return;
    }


    /**
     *  Matches assets that whose alterations can be distributed. 
     *
     *  @param  alterable distribute alterations rights flag 
     */

    @OSID @Override
    public void matchDistributeAlterations(boolean alterable) {
        return;
    }


    /**
     *  Clears the distribute alterations terms. 
     */

    @OSID @Override
    public void clearDistributeAlterationsTerms() {
        return;
    }


    /**
     *  Matches assets that can be distributed as part of other compositions. 
     *
     *  @param  composable distribute compositions rights flag 
     */

    @OSID @Override
    public void matchDistributeCompositions(boolean composable) {
        return;
    }


    /**
     *  Clears the distribute compositions terms. 
     */

    @OSID @Override
    public void clearDistributeCompositionsTerms() {
        return;
    }


    /**
     *  Sets the source <code> Id </code> for this query. 
     *
     *  @param  sourceId the source <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id sourceId, boolean match) {
        return;
    }


    /**
     *  Clears the source <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for the source. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for the source. Multiple queries can be retrieved for a 
     *  nested <code> OR </code> term. 
     *
     *  @return the source query 
     *  @throws org.osid.UnimplementedException <code> supportsSourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSourceQuery() {
        throw new org.osid.UnimplementedException("supportsSourceQuery() is false");
    }


    /**
     *  Matches assets with any source. 
     *
     *  @param  match <code> true </code> to match assets with any source, 
     *          <code> false </code> to match assets with no sources 
     */

    @OSID @Override
    public void matchAnySource(boolean match) {
        return;
    }


    /**
     *  Clears the source terms. 
     */

    @OSID @Override
    public void clearSourceTerms() {
        return;
    }


    /**
     *  Match assets that are created between the specified time period. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedDate(org.osid.calendaring.DateTime start, 
                                 org.osid.calendaring.DateTime end, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches assets with any creation time. 
     *
     *  @param  match <code> true </code> to match assets with any created 
     *          time, <code> false </code> to match assets with no cerated 
     *          time 
     */

    @OSID @Override
    public void matchAnyCreatedDate(boolean match) {
        return;
    }


    /**
     *  Clears the created time terms. 
     */

    @OSID @Override
    public void clearCreatedDateTerms() {
        return;
    }


    /**
     *  Marks assets that are marked as published. 
     *
     *  @param  published published flag 
     */

    @OSID @Override
    public void matchPublished(boolean published) {
        return;
    }


    /**
     *  Clears the published terms. 
     */

    @OSID @Override
    public void clearPublishedTerms() {
        return;
    }


    /**
     *  Match assets that are published between the specified time period. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPublishedDate(org.osid.calendaring.DateTime start, 
                                   org.osid.calendaring.DateTime end, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches assets with any published time. 
     *
     *  @param  match <code> true </code> to match assets with any published 
     *          time, <code> false </code> to match assets with no published 
     *          time 
     */

    @OSID @Override
    public void matchAnyPublishedDate(boolean match) {
        return;
    }


    /**
     *  Clears the published time terms. 
     */

    @OSID @Override
    public void clearPublishedDateTerms() {
        return;
    }


    /**
     *  Adds a principal credit string for this query. 
     *
     *  @param  credit credit string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> credit </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> credit </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPrincipalCreditString(String credit, 
                                           org.osid.type.Type stringMatchType, 
                                           boolean match) {
        return;
    }


    /**
     *  Matches a principal credit string that has any value. 
     *
     *  @param  match <code> true </code> to match assets with any principal 
     *          credit string, <code> false </code> to match assets with no 
     *          principal credit string 
     */

    @OSID @Override
    public void matchAnyPrincipalCreditString(boolean match) {
        return;
    }


    /**
     *  Clears the principal credit string terms. 
     */

    @OSID @Override
    public void clearPrincipalCreditStringTerms() {
        return;
    }


    /**
     *  Match assets that whose coverage falls between the specified time 
     *  period inclusive. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTemporalCoverage(org.osid.calendaring.DateTime start, 
                                      org.osid.calendaring.DateTime end, 
                                      boolean match) {
        return;
    }


    /**
     *  Matches assets with any temporal coverage. 
     *
     *  @param  match <code> true </code> to match assets with any temporal 
     *          coverage, <code> false </code> to match assets with no 
     *          temporal coverage 
     */

    @OSID @Override
    public void matchAnyTemporalCoverage(boolean match) {
        return;
    }


    /**
     *  Clears the temporal coverage terms. 
     */

    @OSID @Override
    public void clearTemporalCoverageTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query of spatial 
     *  coverage. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for the provider. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple queries can be retrieved for a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches assets with any provider. 
     *
     *  @param  match <code> true </code> to match assets with any location, 
     *          <code> false </code> to match assets with no locations 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Matches assets that are contained within the given spatial unit. 
     *
     *  @param  spatialUnit the spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> spatialUnit </code> is 
     *          not suppoted 
     */

    @OSID @Override
    public void matchSpatialCoverage(org.osid.mapping.SpatialUnit spatialUnit, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the spatial coverage terms. 
     */

    @OSID @Override
    public void clearSpatialCoverageTerms() {
        return;
    }


    /**
     *  Matches assets that overlap or touch the given spatial unit. 
     *
     *  @param  spatialUnit the spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> spatialUnit </code> is 
     *          not suppoted 
     */

    @OSID @Override
    public void matchSpatialCoverageOverlap(org.osid.mapping.SpatialUnit spatialUnit, 
                                            boolean match) {
        return;
    }


    /**
     *  Matches assets with no spatial coverage. 
     *
     *  @param  match <code> true </code> to match assets with any spatial 
     *          coverage, <code> false </code> to match assets with no spatial 
     *          coverage 
     */

    @OSID @Override
    public void matchAnySpatialCoverage(boolean match) {
        return;
    }


    /**
     *  Clears the spatial coverage overlap terms. 
     */

    @OSID @Override
    public void clearSpatialCoverageOverlapTerms() {
        return;
    }


    /**
     *  Sets the asset content <code> Id </code> for this query. 
     *
     *  @param  assetContentId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetContentId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAssetContentId(org.osid.id.Id assetContentId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the asset content <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetContentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetContentQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset content query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetContentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the asset content. Multiple queries can be 
     *  retrieved for a nested <code> OR </code> term. 
     *
     *  @return the asset contents query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetContentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetContentQuery getAssetContentQuery() {
        throw new org.osid.UnimplementedException("supportsAssetContentQuery() is false");
    }


    /**
     *  Matches assets with any content. 
     *
     *  @param  match <code> true </code> to match assets with any content, 
     *          <code> false </code> to match assets with no content 
     */

    @OSID @Override
    public void matchAnyAssetContent(boolean match) {
        return;
    }


    /**
     *  Clears the asset content terms. 
     */

    @OSID @Override
    public void clearAssetContentTerms() {
        return;
    }


    /**
     *  Sets the composition <code> Id </code> for this query to match assets 
     *  that are a part of the composition. 
     *
     *  @param  compositionId the composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompositionId(org.osid.id.Id compositionId, boolean match) {
        return;
    }


    /**
     *  Clears the composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompositionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple queries can be retrieved 
     *  for a nested <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsCompositionQuery() is false");
    }


    /**
     *  Matches assets with any composition mappings. 
     *
     *  @param  match <code> true </code> to match assets with any 
     *          composition, <code> false </code> to match assets with no 
     *          composition mappings 
     */

    @OSID @Override
    public void matchAnyComposition(boolean match) {
        return;
    }


    /**
     *  Clears the composition terms. 
     */

    @OSID @Override
    public void clearCompositionTerms() {
        return;
    }


    /**
     *  Sets the repository <code> Id </code> for this query. 
     *
     *  @param  repositoryId the repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRepositoryId(org.osid.id.Id repositoryId, boolean match) {
        return;
    }


    /**
     *  Clears the repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRepositoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple queries can be retrieved for 
     *  a nested <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsRepositoryQuery() is false");
    }


    /**
     *  Clears the repository terms. 
     */

    @OSID @Override
    public void clearRepositoryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given asset query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an asset implementing the requested record.
     *
     *  @param assetRecordType an asset record type
     *  @return the asset query record
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetQueryRecord getAssetQueryRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetQueryRecord record : this.records) {
            if (record.implementsRecordType(assetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this asset query. 
     *
     *  @param assetQueryRecord asset query record
     *  @param assetRecordType asset record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssetQueryRecord(org.osid.repository.records.AssetQueryRecord assetQueryRecord, 
                                          org.osid.type.Type assetRecordType) {

        addRecordType(assetRecordType);
        nullarg(assetQueryRecord, "asset query record");
        this.records.add(assetQueryRecord);        
        return;
    }
}
