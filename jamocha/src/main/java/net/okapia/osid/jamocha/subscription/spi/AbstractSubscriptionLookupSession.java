//
// AbstractSubscriptionLookupSession.java
//
//    A starter implementation framework for providing a Subscription
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Subscription
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSubscriptions(), this other methods may need to be overridden
 *  for better performance.
 */

public abstract class AbstractSubscriptionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.subscription.SubscriptionLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.subscription.Publisher publisher = new net.okapia.osid.jamocha.nil.subscription.publisher.UnknownPublisher();
    

    /**
     *  Gets the <code>Publisher/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Publisher Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.publisher.getId());
    }


    /**
     *  Gets the <code>Publisher</code> associated with this session.
     *
     *  @return the <code>Publisher</code> associated with this
     *          session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.publisher);
    }


    /**
     *  Sets the <code>Publisher</code>.
     *
     *  @param  publisher the publisher for this session
     *  @throws org.osid.NullArgumentException <code>publisher</code>
     *          is <code>null</code>
     */

    protected void setPublisher(org.osid.subscription.Publisher publisher) {
        nullarg(publisher, "publisher");
        this.publisher = publisher;
        return;
    }


    /**
     *  Tests if this user can perform <code>Subscription</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSubscriptions() {
        return (true);
    }


    /**
     *  A complete view of the <code>Subscription</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSubscriptionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Subscription</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySubscriptionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscriptions in publishers which are
     *  children of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only subscriptions whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveSubscriptionView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All subscriptions of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveSubscriptionView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Subscription</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Subscription</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Subscription</code>
     *  and retained for compatibility.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @param  subscriptionId <code>Id</code> of the
     *          <code>Subscription</code>
     *  @return the subscription
     *  @throws org.osid.NotFoundException <code>subscriptionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>subscriptionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Subscription getSubscription(org.osid.id.Id subscriptionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.subscription.SubscriptionList subscriptions = getSubscriptions()) {
            while (subscriptions.hasNext()) {
                org.osid.subscription.Subscription subscription = subscriptions.getNextSubscription();
                if (subscription.getId().equals(subscriptionId)) {
                    return (subscription);
                }
            }
        } 

        throw new org.osid.NotFoundException(subscriptionId + " not found");
    }


    /**
     *  Gets a <code>SubscriptionList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  subscriptions specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Subscriptions</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSubscriptions()</code>.
     *
     *  @param  subscriptionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByIds(org.osid.id.IdList subscriptionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.subscription.Subscription> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = subscriptionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSubscription(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("subscription " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.subscription.subscription.LinkedSubscriptionList(ret));
    }


    /**
     *  Gets a <code>SubscriptionList</code> corresponding to the
     *  given subscription genus <code>Type</code> which does not
     *  include subscriptions of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSubscriptions()</code>.
     *
     *  @param subscriptionGenusType a subscription genus type
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptions(), subscriptionGenusType));
    }


    /**
     *  Gets a <code>SubscriptionList</code> corresponding to the
     *  given subscription genus <code>Type</code> and include any
     *  additional subscriptions with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSubscriptions()</code>.
     *
     *  @param subscriptionGenusType a subscription genus type
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByParentGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSubscriptionsByGenusType(subscriptionGenusType));
    }


    /**
     *  Gets a <code>SubscriptionList</code> containing the given
     *  subscription record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSubscriptions()</code>.
     *
     *  @param subscriptionRecordType a subscription record type
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByRecordType(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionRecordFilterList(getSubscriptions(), subscriptionRecordType));
    }


    /**
     *  Gets a <code>SubscriptionList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *  
     *  In active mode, subscriptions are returned that are currently
     *  active. In any status mode, active and inactive subscriptions
     *  are returned.
     *
     *  @param from start of date range
     *  @param  to end of date range 
     *  @return the returned <code>Subscription</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsOnDate(org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.TemporalSubscriptionFilterList(getSubscriptions(), from, to));
    }
        

    /**
     *  Gets a <code>SubscriptionList</code> by genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *  
     *  In active mode, subscriptions are returned that are currently
     *  active. In any status mode, active and inactive subscriptions
     *  are returned.
     *
     *  @param subscriptionGenusType a subscription genus type
     *  @param from start of date range
     *  @param  to end of date range 
     *  @return the returned <code>Subscription</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeOnDate(org.osid.type.Type subscriptionGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptionsOnDate(from, to), subscriptionGenusType));
    }
        
    
    /**
     *  Gets a list of subscriptions corresponding to a subscriber
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the subscriber
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriber(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionFilterList(new SubscriberFilter(resourceId), getSubscriptions()));
    }


    /**
     *  Gets a list of subscriptions of the given genus type
     *  corresponding to a subscriber <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code>SubscriptionList</code> 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>subscriptionGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriber(org.osid.id.Id resourceId, 
                                                                                           org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptionsForSubscriber(resourceId), subscriptionGenusType));
    }        


    /**
     *  Gets a list of subscriptions corresponding to a subscriber
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the subscriber
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.TemporalSubscriptionFilterList(getSubscriptionsForSubscriber(resourceId), from, to));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a subscriber <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are currently 
     *  effective. In any effective mode, effective subscriptions and those 
     *  currently expired are returned. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>SubscriptionList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>subscriptionGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberOnDate(org.osid.id.Id resourceId, 
                                                                                                 org.osid.type.Type subscriptionGenusType, 
                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptionsForSubscriberOnDate(resourceId, from, to), subscriptionGenusType));
    }


    /**
     *  Gets a list of subscriptions corresponding to a dispatch
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the <code>Id</code> of the dispatch
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>dispatchId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.subscription.SubscriptionList getSubscriptionsForDispatch(org.osid.id.Id dispatchId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionFilterList(new DispatchFilter(dispatchId), getSubscriptions()));
    }


    /**
     *  Gets a list of subscriptions corresponding to a dispatch
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param dispatchId the <code>Id</code> of the dispatch
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>dispatchId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForDispatchOnDate(org.osid.id.Id dispatchId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.TemporalSubscriptionFilterList(getSubscriptionsForDispatch(dispatchId), from, to));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a dispatch <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the <code>Id</code> of the dispatch 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code>SubscriptionList</code> 
     *  @throws org.osid.NullArgumentException <code>dispatchId</code>
     *          or <code>subscriptionGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForDispatch(org.osid.id.Id dispatchId, 
                                                                                         org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptionsForDispatch(dispatchId), subscriptionGenusType));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a dispatch <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId a dispatch <code>Id</code> 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>SubscriptionList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchId</code>,
     *          <code>subscriptionGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForDispatchOnDate(org.osid.id.Id dispatchId, 
                                                                                               org.osid.type.Type subscriptionGenusType, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptionsForDispatchOnDate(dispatchId, from, to), subscriptionGenusType));
    }


    /**
     *  Gets a list of subscriptions corresponding to subscriber and
     *  dispatch <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the subscriber
     *  @param  dispatchId the <code>Id</code> of the dispatch
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>dispatchId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberAndDispatch(org.osid.id.Id resourceId,
                                                                                           org.osid.id.Id dispatchId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionFilterList(new DispatchFilter(dispatchId), getSubscriptionsForSubscriber(resourceId)));
    }


    /**
     *  Gets a list of subscriptions corresponding to subscriber and
     *  dispatch <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param dispatchId the <code>Id</code> of the dispatch
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>SubscriptionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>dispatchId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberAndDispatchOnDate(org.osid.id.Id resourceId,
                                                                                                 org.osid.id.Id dispatchId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.TemporalSubscriptionFilterList(getSubscriptionsForSubscriberAndDispatch(resourceId, dispatchId), from, to));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a susbcriber and dispatch <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource 
     *  @param  dispatchId the <code>Id</code> of the dispatch 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code>SubscriptionList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>dispatchId</code> or
     *          <code>subscriptionGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberAndDispatch(org.osid.id.Id resourceId, 
                                                                                                      org.osid.id.Id dispatchId, 
                                                                                                      org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptionsForSubscriberAndDispatch(resourceId, dispatchId), subscriptionGenusType));
    }        


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a subscriber and dispatch <code>Id</code> and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource 
     *  @param  dispatchId a dispatch <code>Id</code> 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>SubscriptionList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>dispatch</code>,
     *          <code>subscriptionGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberAndDispatchOnDate(org.osid.id.Id resourceId, 
                                                                                                            org.osid.id.Id dispatchId, 
                                                                                                            org.osid.type.Type subscriptionGenusType, 
                                                                                                            org.osid.calendaring.DateTime from, 
                                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionGenusFilterList(getSubscriptionsForSubscriberAndDispatchOnDate(resourceId, dispatchId, from, to), subscriptionGenusType));
    }


    /**
     *  Gets all <code>Subscriptions</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @return a list of <code>Subscriptions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.subscription.SubscriptionList getSubscriptions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the subscription list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of subscriptions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.subscription.SubscriptionList filterSubscriptionsOnViews(org.osid.subscription.SubscriptionList list)
        throws org.osid.OperationFailedException {

        org.osid.subscription.SubscriptionList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.subscription.subscription.EffectiveSubscriptionFilterList(ret);
        }

        return (ret);
    }


    public static class SubscriberFilter
        implements net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>SubscriberFilter</code>.
         *
         *  @param resourceId the subscriber to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public SubscriberFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "subscriber Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the SubscriptionFilterList to filter the
         *  subscription list based on subscriber.
         *
         *  @param subscription the subscription
         *  @return <code>true</code> to pass the subscription,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.subscription.Subscription subscription) {
            return (subscription.getSubscriberId().equals(this.resourceId));
        }
    }


    public static class DispatchFilter
        implements net.okapia.osid.jamocha.inline.filter.subscription.subscription.SubscriptionFilter {
         
        private final org.osid.id.Id dispatchId;
         
         
        /**
         *  Constructs a new <code>DispatchFilter</code>.
         *
         *  @param dispatchId the dispatch to filter
         *  @throws org.osid.NullArgumentException
         *          <code>dispatchId</code> is <code>null</code>
         */
        
        public DispatchFilter(org.osid.id.Id dispatchId) {
            nullarg(dispatchId, "dispatch Id");
            this.dispatchId = dispatchId;
            return;
        }

         
        /**
         *  Used by the SubscriptionFilterList to filter the 
         *  subscription list based on dispatch.
         *
         *  @param subscription the subscription
         *  @return <code>true</code> to pass the subscription,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.subscription.Subscription subscription) {
            return (subscription.getDispatchId().equals(this.dispatchId));
        }
    }
}
