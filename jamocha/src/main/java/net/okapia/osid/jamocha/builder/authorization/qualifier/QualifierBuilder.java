//
// Qualifier.java
//
//     Defines a Qualifier builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.qualifier;


/**
 *  Defines a <code>Qualifier</code> builder.
 */

public final class QualifierBuilder
    extends net.okapia.osid.jamocha.builder.authorization.qualifier.spi.AbstractQualifierBuilder<QualifierBuilder> {
    

    /**
     *  Constructs a new <code>QualifierBuilder</code> using a
     *  <code>MutableQualifier</code>.
     */

    public QualifierBuilder() {
        super(new MutableQualifier());
        return;
    }


    /**
     *  Constructs a new <code>QualifierBuilder</code> using the given
     *  mutable qualifier.
     * 
     *  @param qualifier
     */

    public QualifierBuilder(QualifierMiter qualifier) {
        super(qualifier);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return QualifierBuilder
     */

    @Override
    protected QualifierBuilder self() {
        return (this);
    }
}       


