//
// AbstractTodoProducerSearchOdrer.java
//
//     Defines a TodoProducerSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code TodoProducerSearchOrder}.
 */

public abstract class AbstractTodoProducerSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.checklist.mason.TodoProducerSearchOrder {

    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the creation 
     *  rule flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreationRule(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the cyclic 
     *  event. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCyclicEvent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a cyclic event search order is available. 
     *
     *  @return <code> true </code> if a cyclic event search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the cyclic event search order. 
     *
     *  @return the cyclic event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchOrder getCyclicEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCyclicEventSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the stock. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStock(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a stock search order is available. 
     *
     *  @return <code> true </code> if a stock search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSearchOrder() {
        return (false);
    }


    /**
     *  Gets the stock search order. 
     *
     *  @return the stock search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchOrder getStockSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStockSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the stock level. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStockLevel(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  todoProducerRecordType a todo producer record type 
     *  @return {@code true} if the todoProducerRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code todoProducerRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type todoProducerRecordType) {
        for (org.osid.checklist.mason.records.TodoProducerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  todoProducerRecordType the todo producer record type 
     *  @return the todo producer search order record
     *  @throws org.osid.NullArgumentException
     *          {@code todoProducerRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(todoProducerRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerSearchOrderRecord getTodoProducerSearchOrderRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.mason.records.TodoProducerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this todo producer. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param todoProducerRecord the todo producer search odrer record
     *  @param todoProducerRecordType todo producer record type
     *  @throws org.osid.NullArgumentException
     *          {@code todoProducerRecord} or
     *          {@code todoProducerRecordTypetodoProducer} is
     *          {@code null}
     */
            
    protected void addTodoProducerRecord(org.osid.checklist.mason.records.TodoProducerSearchOrderRecord todoProducerSearchOrderRecord, 
                                     org.osid.type.Type todoProducerRecordType) {

        addRecordType(todoProducerRecordType);
        this.records.add(todoProducerSearchOrderRecord);
        
        return;
    }
}
