//
// AbstractQueryPersonLookupSession.java
//
//    A PersonQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PersonQuerySession adapter.
 */

public abstract class AbstractAdapterPersonQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.personnel.PersonQuerySession {

    private final org.osid.personnel.PersonQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterPersonQuerySession.
     *
     *  @param session the underlying person query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPersonQuerySession(org.osid.personnel.PersonQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeRealm</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeRealm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the {@codeRealm</code> associated with this 
     *  session.
     *
     *  @return the {@codeRealm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform {@codePerson</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchPersons() {
        return (this.session.canSearchPersons());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include persons in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this realm only.
     */
    
    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    
      
    /**
     *  Gets a person query. The returned query will not have an
     *  extension query.
     *
     *  @return the person query 
     */
      
    @OSID @Override
    public org.osid.personnel.PersonQuery getPersonQuery() {
        return (this.session.getPersonQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  personQuery the person query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code personQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code personQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByQuery(org.osid.personnel.PersonQuery personQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getPersonsByQuery(personQuery));
    }
}
