//
// AbstractControllerQueryInspector.java
//
//     A template for making a ControllerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.controller.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for controllers.
 */

public abstract class AbstractControllerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.control.ControllerQueryInspector {

    private final java.util.Collection<org.osid.control.records.ControllerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the address query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getAddressTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the model <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the model query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inventory.ModelQueryInspector[] getModelTerms() {
        return (new org.osid.inventory.ModelQueryInspector[0]);
    }


    /**
     *  Gets the version query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionTerms() {
        return (new org.osid.search.terms.VersionTerm[0]);
    }


    /**
     *  Gets the version since query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionSinceTerms() {
        return (new org.osid.search.terms.VersionTerm[0]);
    }


    /**
     *  Gets the toggle query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getToggleableTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the variable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getVariableTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the variable percentage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getVariableByPercentageTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the variable minimum query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariableMinimumTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the variable maximum query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariableMaximumTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the discreet query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDiscreetStatesTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDiscreetStateIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getDiscreetStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given controller query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a controller implementing the requested record.
     *
     *  @param controllerRecordType a controller record type
     *  @return the controller query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(controllerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerQueryInspectorRecord getControllerQueryInspectorRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ControllerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this controller query. 
     *
     *  @param controllerQueryInspectorRecord controller query inspector
     *         record
     *  @param controllerRecordType controller record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addControllerQueryInspectorRecord(org.osid.control.records.ControllerQueryInspectorRecord controllerQueryInspectorRecord, 
                                                   org.osid.type.Type controllerRecordType) {

        addRecordType(controllerRecordType);
        nullarg(controllerRecordType, "controller record type");
        this.records.add(controllerQueryInspectorRecord);        
        return;
    }
}
