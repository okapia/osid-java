//
// InvariantMapActivityUnitLookupSession
//
//    Implements an ActivityUnit lookup service backed by a fixed collection of
//    activityUnits.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements an ActivityUnit lookup service backed by a fixed
 *  collection of activity units. The activity units are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapActivityUnitLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractMapActivityUnitLookupSession
    implements org.osid.course.ActivityUnitLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityUnitLookupSession</code> with no
     *  activity units.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityUnitLookupSession</code> with a single
     *  activity unit.
     *  
     *  @param courseCatalog the course catalog
     *  @param activityUnit an single activity unit
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityUnit} is <code>null</code>
     */

      public InvariantMapActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.ActivityUnit activityUnit) {
        this(courseCatalog);
        putActivityUnit(activityUnit);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityUnitLookupSession</code> using an array
     *  of activity units.
     *  
     *  @param courseCatalog the course catalog
     *  @param activityUnits an array of activity units
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityUnits} is <code>null</code>
     */

      public InvariantMapActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.ActivityUnit[] activityUnits) {
        this(courseCatalog);
        putActivityUnits(activityUnits);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityUnitLookupSession</code> using a
     *  collection of activity units.
     *
     *  @param courseCatalog the course catalog
     *  @param activityUnits a collection of activity units
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityUnits} is <code>null</code>
     */

      public InvariantMapActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.ActivityUnit> activityUnits) {
        this(courseCatalog);
        putActivityUnits(activityUnits);
        return;
    }
}
