//
// InvariantMapBranchLookupSession
//
//    Implements a Branch lookup service backed by a fixed collection of
//    branches.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling;


/**
 *  Implements a Branch lookup service backed by a fixed
 *  collection of branches. The branches are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapBranchLookupSession
    extends net.okapia.osid.jamocha.core.journaling.spi.AbstractMapBranchLookupSession
    implements org.osid.journaling.BranchLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapBranchLookupSession</code> with no
     *  branches.
     *  
     *  @param journal the journal
     *  @throws org.osid.NullArgumnetException {@code journal} is
     *          {@code null}
     */

    public InvariantMapBranchLookupSession(org.osid.journaling.Journal journal) {
        setJournal(journal);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBranchLookupSession</code> with a single
     *  branch.
     *  
     *  @param journal the journal
     *  @param branch a single branch
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code branch} is <code>null</code>
     */

      public InvariantMapBranchLookupSession(org.osid.journaling.Journal journal,
                                               org.osid.journaling.Branch branch) {
        this(journal);
        putBranch(branch);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBranchLookupSession</code> using an array
     *  of branches.
     *  
     *  @param journal the journal
     *  @param branches an array of branches
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code branches} is <code>null</code>
     */

      public InvariantMapBranchLookupSession(org.osid.journaling.Journal journal,
                                               org.osid.journaling.Branch[] branches) {
        this(journal);
        putBranches(branches);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBranchLookupSession</code> using a
     *  collection of branches.
     *
     *  @param journal the journal
     *  @param branches a collection of branches
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code branches} is <code>null</code>
     */

      public InvariantMapBranchLookupSession(org.osid.journaling.Journal journal,
                                               java.util.Collection<? extends org.osid.journaling.Branch> branches) {
        this(journal);
        putBranches(branches);
        return;
    }
}
