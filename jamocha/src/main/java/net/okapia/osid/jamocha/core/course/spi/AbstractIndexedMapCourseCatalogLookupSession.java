//
// AbstractIndexedMapCourseCatalogLookupSession.java
//
//    A simple framework for providing a CourseCatalog lookup service
//    backed by a fixed collection of course catalogs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CourseCatalog lookup service backed by a
 *  fixed collection of course catalogs. The course catalogs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some course catalogs may be compatible
 *  with more types than are indicated through these course catalog
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CourseCatalogs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCourseCatalogLookupSession
    extends AbstractMapCourseCatalogLookupSession
    implements org.osid.course.CourseCatalogLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.CourseCatalog> courseCatalogsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.CourseCatalog>());
    private final MultiMap<org.osid.type.Type, org.osid.course.CourseCatalog> courseCatalogsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.CourseCatalog>());


    /**
     *  Makes a <code>CourseCatalog</code> available in this session.
     *
     *  @param  courseCatalog a course catalog
     *  @throws org.osid.NullArgumentException <code>courseCatalog<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        super.putCourseCatalog(courseCatalog);

        this.courseCatalogsByGenus.put(courseCatalog.getGenusType(), courseCatalog);
        
        try (org.osid.type.TypeList types = courseCatalog.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.courseCatalogsByRecord.put(types.getNextType(), courseCatalog);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a course catalog from this session.
     *
     *  @param courseCatalogId the <code>Id</code> of the course catalog
     *  @throws org.osid.NullArgumentException <code>courseCatalogId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCourseCatalog(org.osid.id.Id courseCatalogId) {
        org.osid.course.CourseCatalog courseCatalog;
        try {
            courseCatalog = getCourseCatalog(courseCatalogId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.courseCatalogsByGenus.remove(courseCatalog.getGenusType());

        try (org.osid.type.TypeList types = courseCatalog.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.courseCatalogsByRecord.remove(types.getNextType(), courseCatalog);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCourseCatalog(courseCatalogId);
        return;
    }


    /**
     *  Gets a <code>CourseCatalogList</code> corresponding to the given
     *  course catalog genus <code>Type</code> which does not include
     *  course catalogs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known course catalogs or an error results. Otherwise,
     *  the returned list may contain only those course catalogs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  courseCatalogGenusType a course catalog genus type 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByGenusType(org.osid.type.Type courseCatalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.coursecatalog.ArrayCourseCatalogList(this.courseCatalogsByGenus.get(courseCatalogGenusType)));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> containing the given
     *  course catalog record <code>Type</code>. In plenary mode, the
     *  returned list contains all known course catalogs or an error
     *  results. Otherwise, the returned list may contain only those
     *  course catalogs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  courseCatalogRecordType a course catalog record type 
     *  @return the returned <code>courseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByRecordType(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.coursecatalog.ArrayCourseCatalogList(this.courseCatalogsByRecord.get(courseCatalogRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.courseCatalogsByGenus.clear();
        this.courseCatalogsByRecord.clear();

        super.close();

        return;
    }
}
