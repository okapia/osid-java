//
// AbstractNodeJournalHierarchySession.java
//
//     Defines a Journal hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a journal hierarchy session for delivering a hierarchy
 *  of journals using the JournalNode interface.
 */

public abstract class AbstractNodeJournalHierarchySession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalHierarchySession
    implements org.osid.journaling.JournalHierarchySession {

    private java.util.Collection<org.osid.journaling.JournalNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root journal <code> Ids </code> in this hierarchy.
     *
     *  @return the root journal <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootJournalIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.journaling.journalnode.JournalNodeToIdList(this.roots));
    }


    /**
     *  Gets the root journals in the journal hierarchy. A node
     *  with no parents is an orphan. While all journal <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root journals 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getRootJournals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.journaling.journalnode.JournalNodeToJournalList(new net.okapia.osid.jamocha.journaling.journalnode.ArrayJournalNodeList(this.roots)));
    }


    /**
     *  Adds a root journal node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootJournal(org.osid.journaling.JournalNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root journal nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootJournals(java.util.Collection<org.osid.journaling.JournalNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root journal node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootJournal(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.journaling.JournalNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Journal </code> has any parents. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @return <code> true </code> if the journal has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentJournals(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getJournalNode(journalId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  journal.
     *
     *  @param  id an <code> Id </code> 
     *  @param  journalId the <code> Id </code> of a journal 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> journalId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> journalId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfJournal(org.osid.id.Id id, org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.journaling.JournalNodeList parents = getJournalNode(journalId).getParentJournalNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextJournalNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given journal. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @return the parent <code> Ids </code> of the journal 
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentJournalIds(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.journaling.journal.JournalToIdList(getParentJournals(journalId)));
    }


    /**
     *  Gets the parents of the given journal. 
     *
     *  @param  journalId the <code> Id </code> to query 
     *  @return the parents of the journal 
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getParentJournals(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.journaling.journalnode.JournalNodeToJournalList(getJournalNode(journalId).getParentJournalNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  journal.
     *
     *  @param  id an <code> Id </code> 
     *  @param  journalId the Id of a journal 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> journalId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfJournal(org.osid.id.Id id, org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfJournal(id, journalId)) {
            return (true);
        }

        try (org.osid.journaling.JournalList parents = getParentJournals(journalId)) {
            while (parents.hasNext()) {
                if (isAncestorOfJournal(id, parents.getNextJournal().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a journal has any children. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @return <code> true </code> if the <code> journalId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildJournals(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getJournalNode(journalId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  journal.
     *
     *  @param  id an <code> Id </code> 
     *  @param journalId the <code> Id </code> of a 
     *         journal
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> journalId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> journalId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfJournal(org.osid.id.Id id, org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfJournal(journalId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  journal.
     *
     *  @param  journalId the <code> Id </code> to query 
     *  @return the children of the journal 
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildJournalIds(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.journaling.journal.JournalToIdList(getChildJournals(journalId)));
    }


    /**
     *  Gets the children of the given journal. 
     *
     *  @param  journalId the <code> Id </code> to query 
     *  @return the children of the journal 
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getChildJournals(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.journaling.journalnode.JournalNodeToJournalList(getJournalNode(journalId).getChildJournalNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  journal.
     *
     *  @param  id an <code> Id </code> 
     *  @param journalId the <code> Id </code> of a 
     *         journal
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> journalId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfJournal(org.osid.id.Id id, org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfJournal(journalId, id)) {
            return (true);
        }

        try (org.osid.journaling.JournalList children = getChildJournals(journalId)) {
            while (children.hasNext()) {
                if (isDescendantOfJournal(id, children.getNextJournal().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  journal.
     *
     *  @param  journalId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified journal node 
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getJournalNodeIds(org.osid.id.Id journalId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.journaling.journalnode.JournalNodeToNode(getJournalNode(journalId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given journal.
     *
     *  @param  journalId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified journal node 
     *  @throws org.osid.NotFoundException <code> journalId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> journalId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalNode getJournalNodes(org.osid.id.Id journalId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getJournalNode(journalId));
    }


    /**
     *  Closes this <code>JournalHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a journal node.
     *
     *  @param journalId the id of the journal node
     *  @throws org.osid.NotFoundException <code>journalId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>journalId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.journaling.JournalNode getJournalNode(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(journalId, "journal Id");
        for (org.osid.journaling.JournalNode journal : this.roots) {
            if (journal.getId().equals(journalId)) {
                return (journal);
            }

            org.osid.journaling.JournalNode r = findJournal(journal, journalId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(journalId + " is not found");
    }


    protected org.osid.journaling.JournalNode findJournal(org.osid.journaling.JournalNode node, 
                                                          org.osid.id.Id journalId) 
	throws org.osid.OperationFailedException {

        try (org.osid.journaling.JournalNodeList children = node.getChildJournalNodes()) {
            while (children.hasNext()) {
                org.osid.journaling.JournalNode journal = children.getNextJournalNode();
                if (journal.getId().equals(journalId)) {
                    return (journal);
                }
                
                journal = findJournal(journal, journalId);
                if (journal != null) {
                    return (journal);
                }
            }
        }

        return (null);
    }
}
