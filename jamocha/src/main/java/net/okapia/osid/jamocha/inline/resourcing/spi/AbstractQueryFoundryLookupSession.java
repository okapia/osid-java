//
// AbstractQueryFoundryLookupSession.java
//
//    An inline adapter that maps a FoundryLookupSession to
//    a FoundryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a FoundryLookupSession to
 *  a FoundryQuerySession.
 */

public abstract class AbstractQueryFoundryLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractFoundryLookupSession
    implements org.osid.resourcing.FoundryLookupSession {

    private final org.osid.resourcing.FoundryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryFoundryLookupSession.
     *
     *  @param querySession the underlying foundry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryFoundryLookupSession(org.osid.resourcing.FoundryQuerySession querySession) {
        nullarg(querySession, "foundry query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Foundry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFoundries() {
        return (this.session.canSearchFoundries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Foundry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Foundry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Foundry</code> and
     *  retained for compatibility.
     *
     *  @param  foundryId <code>Id</code> of the
     *          <code>Foundry</code>
     *  @return the foundry
     *  @throws org.osid.NotFoundException <code>foundryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>foundryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.FoundryQuery query = getQuery();
        query.matchId(foundryId, true);
        org.osid.resourcing.FoundryList foundries = this.session.getFoundriesByQuery(query);
        if (foundries.hasNext()) {
            return (foundries.getNextFoundry());
        } 
        
        throw new org.osid.NotFoundException(foundryId + " not found");
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  foundries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Foundries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  foundryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>foundryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByIds(org.osid.id.IdList foundryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.FoundryQuery query = getQuery();

        try (org.osid.id.IdList ids = foundryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getFoundriesByQuery(query));
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  foundry genus <code>Type</code> which does not include
     *  foundries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.FoundryQuery query = getQuery();
        query.matchGenusType(foundryGenusType, true);
        return (this.session.getFoundriesByQuery(query));
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  foundry genus <code>Type</code> and include any additional
     *  foundries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByParentGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.FoundryQuery query = getQuery();
        query.matchParentGenusType(foundryGenusType, true);
        return (this.session.getFoundriesByQuery(query));
    }


    /**
     *  Gets a <code>FoundryList</code> containing the given
     *  foundry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryRecordType a foundry record type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByRecordType(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.FoundryQuery query = getQuery();
        query.matchRecordType(foundryRecordType, true);
        return (this.session.getFoundriesByQuery(query));
    }


    /**
     *  Gets a <code>FoundryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known foundries or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  foundries that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Foundry</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.FoundryQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getFoundriesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Foundries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Foundries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.FoundryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getFoundriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.FoundryQuery getQuery() {
        org.osid.resourcing.FoundryQuery query = this.session.getFoundryQuery();
        return (query);
    }
}
