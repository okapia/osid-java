//
// AbstractQueryInstallationLookupSession.java
//
//    An inline adapter that maps an InstallationLookupSession to
//    an InstallationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an InstallationLookupSession to
 *  an InstallationQuerySession.
 */

public abstract class AbstractQueryInstallationLookupSession
    extends net.okapia.osid.jamocha.installation.spi.AbstractInstallationLookupSession
    implements org.osid.installation.InstallationLookupSession {

    private final org.osid.installation.InstallationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryInstallationLookupSession.
     *
     *  @param querySession the underlying installation query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryInstallationLookupSession(org.osid.installation.InstallationQuerySession querySession) {
        nullarg(querySession, "installation query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Site</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Site Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSiteId() {
        return (this.session.getSiteId());
    }


    /**
     *  Gets the <code>Site</code> associated with this 
     *  session.
     *
     *  @return the <code>Site</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Site getSite()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getSite());
    }


    /**
     *  Tests if this user can perform <code>Installation</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInstallations() {
        return (this.session.canSearchInstallations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  The returns from the lookup methods may omit multiple versions of the 
     *  same installation. 
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.session.useNormalizedVersionView();
        return;
    }


    /**
     *  All versions of the same installation are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.session.useDenormalizedVersionView();
        return;
    }

    
    /**
     *  Gets the <code>Installation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Installation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Installation</code> and
     *  retained for compatibility.
     *
     *  @param  installationId <code>Id</code> of the
     *          <code>Installation</code>
     *  @return the installation
     *  @throws org.osid.NotFoundException <code>installationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>installationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Installation getInstallation(org.osid.id.Id installationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.InstallationQuery query = getQuery();
        query.matchId(installationId, true);
        org.osid.installation.InstallationList installations = this.session.getInstallationsByQuery(query);
        if (installations.hasNext()) {
            return (installations.getNextInstallation());
        } 
        
        throw new org.osid.NotFoundException(installationId + " not found");
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  installations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Installations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  installationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>installationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByIds(org.osid.id.IdList installationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.InstallationQuery query = getQuery();

        try (org.osid.id.IdList ids = installationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getInstallationsByQuery(query));
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  installation genus <code>Type</code> which does not include
     *  installations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.InstallationQuery query = getQuery();
        query.matchGenusType(installationGenusType, true);
        return (this.session.getInstallationsByQuery(query));
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  installation genus <code>Type</code> and include any additional
     *  installations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByParentGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.InstallationQuery query = getQuery();
        query.matchParentGenusType(installationGenusType, true);
        return (this.session.getInstallationsByQuery(query));
    }


    /**
     *  Gets an <code>InstallationList</code> containing the given
     *  installation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationRecordType an installation record type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByRecordType(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.InstallationQuery query = getQuery();
        query.matchRecordType(installationRecordType, true);
        return (this.session.getInstallationsByQuery(query));
    }

    
    /**
     *  Gets an <code> InstallationList </code> corresponding to the
     *  given <code> Package. </code> In plenary mode, the returned
     *  list contains all of the installations for the specified
     *  package, in the order of the list, including duplicates, or an
     *  error results if an <code> Id </code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible <code>
     *  Installations </code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  @param  packageId <code> Id </code> of a <code> Package </code> 
     *  @return the returned <code> Installation </code> list 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByPackage(org.osid.id.Id packageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.InstallationQuery query = getQuery();
        query.matchPackageId(packageId, true);
        return (this.session.getInstallationsByQuery(query));
    }


    /**
     *  Gets all <code>Installations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Installations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.InstallationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getInstallationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.installation.InstallationQuery getQuery() {
        org.osid.installation.InstallationQuery query = this.session.getInstallationQuery();
        return (query);
    }
}
