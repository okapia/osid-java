//
// AbstractAssemblyRecipeQuery.java
//
//     A RecipeQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recipe.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RecipeQuery that stores terms.
 */

public abstract class AbstractAssemblyRecipeQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblySourceableOsidObjectQuery
    implements org.osid.recipe.RecipeQuery,
               org.osid.recipe.RecipeQueryInspector,
               org.osid.recipe.RecipeSearchOrder {

    private final java.util.Collection<org.osid.recipe.records.RecipeQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.RecipeQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.RecipeSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRecipeQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRecipeQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches recipies with an estimated duration between the given range 
     *  inclusive. 
     *
     *  @param  start starting duration 
     *  @param  end ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalEstimatedDuration(org.osid.calendaring.Duration start, 
                                            org.osid.calendaring.Duration end, 
                                            boolean match) {
        getAssembler().addDurationRangeTerm(getTotalEstimatedDurationColumn(), start, end, match);
        return;
    }


    /**
     *  Matches recipes with any estimated duration. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          estimated duration, <code> false </code> to match directions 
     *          with no estimated duration 
     */

    @OSID @Override
    public void matchAnyTotalEstimatedDuration(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getTotalEstimatedDurationColumn(), match);
        return;
    }


    /**
     *  Clears the duration query terms. 
     */

    @OSID @Override
    public void clearTotalEstimatedDurationTerms() {
        getAssembler().clearTerms(getTotalEstimatedDurationColumn());
        return;
    }


    /**
     *  Gets the estimated duration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalEstimatedDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalEstimatedDurationColumn()));
    }


    /**
     *  Orders the results by total estimated duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalEstimatedDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalEstimatedDurationColumn(), style);
        return;
    }


    /**
     *  Gets the TotalEstimatedDuration column name.
     *
     * @return the column name
     */

    protected String getTotalEstimatedDurationColumn() {
        return ("total_estimated_duration");
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getAssetIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears the asset <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        getAssembler().clearTerms(getAssetIdColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (getAssembler().getIdTerms(getAssetIdColumn()));
    }


    /**
     *  Gets the AssetId column name.
     *
     * @return the column name
     */

    protected String getAssetIdColumn() {
        return ("asset_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches directions with any asset. 
     *
     *  @param  match <code> true </code> to match directions with any asset, 
     *          <code> false </code> to match directions with no assets 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        getAssembler().addIdWildcardTerm(getAssetColumn(), match);
        return;
    }


    /**
     *  Clears the asset query terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        getAssembler().clearTerms(getAssetColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the Asset column name.
     *
     * @return the column name
     */

    protected String getAssetColumn() {
        return ("asset");
    }


    /**
     *  Sets the direction <code> Id </code> for this query. 
     *
     *  @param  qualifierId the direction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> directionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDirectionId(org.osid.id.Id qualifierId, boolean match) {
        getAssembler().addIdTerm(getDirectionIdColumn(), qualifierId, match);
        return;
    }


    /**
     *  Clears the direction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDirectionIdTerms() {
        getAssembler().clearTerms(getDirectionIdColumn());
        return;
    }


    /**
     *  Gets the direction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDirectionIdTerms() {
        return (getAssembler().getIdTerms(getDirectionIdColumn()));
    }


    /**
     *  Gets the DirectionId column name.
     *
     * @return the column name
     */

    protected String getDirectionIdColumn() {
        return ("direction_id");
    }


    /**
     *  Tests if a <code> DirectionQuery </code> is available. 
     *
     *  @return <code> true </code> if a direction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a direction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the direction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuery getDirectionQuery() {
        throw new org.osid.UnimplementedException("supportsDirectionQuery() is false");
    }


    /**
     *  Matches recipes that have any direction. 
     *
     *  @param  match <code> true </code> to match recipes with any direction, 
     *          <code> false </code> to match recipes with no direction 
     */

    @OSID @Override
    public void matchAnyDirection(boolean match) {
        getAssembler().addIdWildcardTerm(getDirectionColumn(), match);
        return;
    }


    /**
     *  Clears the direction query terms. 
     */

    @OSID @Override
    public void clearDirectionTerms() {
        getAssembler().clearTerms(getDirectionColumn());
        return;
    }


    /**
     *  Gets the direction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQueryInspector[] getDirectionTerms() {
        return (new org.osid.recipe.DirectionQueryInspector[0]);
    }


    /**
     *  Gets the Direction column name.
     *
     * @return the column name
     */

    protected String getDirectionColumn() {
        return ("direction");
    }


    /**
     *  Matches recipies with an estimated duration between the given range 
     *  inclusive. 
     *
     *  @param  start starting duration 
     *  @param  end ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalDuration(org.osid.calendaring.Duration start, 
                                   org.osid.calendaring.Duration end, 
                                   boolean match) {
        getAssembler().addDurationRangeTerm(getTotalDurationColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the duration query terms. 
     */

    @OSID @Override
    public void clearTotalDurationTerms() {
        getAssembler().clearTerms(getTotalDurationColumn());
        return;
    }


    /**
     *  Gets the total duration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalDurationColumn()));
    }


    /**
     *  Gets the TotalDuration column name.
     *
     * @return the column name
     */

    protected String getTotalDurationColumn() {
        return ("total_duration");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match recipes 
     *  assigned to cookbooks. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCookbookId(org.osid.id.Id cookbookId, boolean match) {
        getAssembler().addIdTerm(getCookbookIdColumn(), cookbookId, match);
        return;
    }


    /**
     *  Clears the cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCookbookIdTerms() {
        getAssembler().clearTerms(getCookbookIdColumn());
        return;
    }


    /**
     *  Gets the cook book <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCookbookIdTerms() {
        return (getAssembler().getIdTerms(getCookbookIdColumn()));
    }


    /**
     *  Gets the CookbookId column name.
     *
     * @return the column name
     */

    protected String getCookbookIdColumn() {
        return ("cookbook_id");
    }


    /**
     *  Tests if a <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsCookbookQuery() is false");
    }


    /**
     *  Clears the cook book query terms. 
     */

    @OSID @Override
    public void clearCookbookTerms() {
        getAssembler().clearTerms(getCookbookColumn());
        return;
    }


    /**
     *  Gets the cook book query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }


    /**
     *  Gets the Cookbook column name.
     *
     * @return the column name
     */

    protected String getCookbookColumn() {
        return ("cookbook");
    }


    /**
     *  Tests if this recipe supports the given record
     *  <code>Type</code>.
     *
     *  @param  recipeRecordType a recipe record type 
     *  @return <code>true</code> if the recipeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recipeRecordType) {
        for (org.osid.recipe.records.RecipeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  recipeRecordType the recipe record type 
     *  @return the recipe query record 
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recipeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeQueryRecord getRecipeQueryRecord(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.RecipeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recipeRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  recipeRecordType the recipe record type 
     *  @return the recipe query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recipeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeQueryInspectorRecord getRecipeQueryInspectorRecord(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.RecipeQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recipeRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param recipeRecordType the recipe record type
     *  @return the recipe search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recipeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeSearchOrderRecord getRecipeSearchOrderRecord(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.RecipeSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recipeRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this recipe. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param recipeQueryRecord the recipe query record
     *  @param recipeQueryInspectorRecord the recipe query inspector
     *         record
     *  @param recipeSearchOrderRecord the recipe search order record
     *  @param recipeRecordType recipe record type
     *  @throws org.osid.NullArgumentException
     *          <code>recipeQueryRecord</code>,
     *          <code>recipeQueryInspectorRecord</code>,
     *          <code>recipeSearchOrderRecord</code> or
     *          <code>recipeRecordTyperecipe</code> is
     *          <code>null</code>
     */
            
    protected void addRecipeRecords(org.osid.recipe.records.RecipeQueryRecord recipeQueryRecord, 
                                      org.osid.recipe.records.RecipeQueryInspectorRecord recipeQueryInspectorRecord, 
                                      org.osid.recipe.records.RecipeSearchOrderRecord recipeSearchOrderRecord, 
                                      org.osid.type.Type recipeRecordType) {

        addRecordType(recipeRecordType);

        nullarg(recipeQueryRecord, "recipe query record");
        nullarg(recipeQueryInspectorRecord, "recipe query inspector record");
        nullarg(recipeSearchOrderRecord, "recipe search odrer record");

        this.queryRecords.add(recipeQueryRecord);
        this.queryInspectorRecords.add(recipeQueryInspectorRecord);
        this.searchOrderRecords.add(recipeSearchOrderRecord);
        
        return;
    }
}
