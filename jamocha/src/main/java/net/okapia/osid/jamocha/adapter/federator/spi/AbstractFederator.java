//
// AbstractFederator.java
//
//     This adapter federates one or more interfaces.
//
//
// Tom Coppeto
// Okapia
// 30 November 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This adapter federates one or more given interfaces of the same
 *  type. Method invocations are passed to the underlying interfaces.
 *
 *  This is a simple tool that is intended for use on methods that do
 *  not return information, such as conditions and proxies defining
 *  input-only methods.
 */

public abstract class AbstractFederator<T>
    implements java.lang.reflect.InvocationHandler {
    
    private final java.util.List<T> delegates = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractFederator</code>
     *
     *  @param delegate the underlying object
     *  @throws org.osid.NullArgumentException <code>delegate</code>
     *          is <code>null</code>
     */

    protected AbstractFederator(T delegate) {
        nullarg(delegate, "delegate");
        this.delegates.add(delegate);
        return;
    }


    /**
     *  Constructs a new <code>AbstractFederator</code>
     *
     *  @param delegates the underlying objects
     *  @throws org.osid.NullArgumentException <code>delegates</code>
     *          is <code>null</code>
     */

    protected AbstractFederator(java.util.Collection<T> delegates) {
        nullarg(delegates, "delegates");
        this.delegates.addAll(delegates);
        return;
    }


    /**
     *  Adds a delegate.
     *
     *  @param delegate the underlying object
     *  @throws org.osid.NullArgumentException <code>delegate</code>
     *          is <code>null</code>
     */

    protected void add(T delegate) {
        nullarg(delegate, "delegate");
        this.delegates.add(delegate);
        return;
    }


    /**
     *  Invokes a method. Part of the InvocationHandler interface.
     *
     *  @param proxy the instance of the object on which the method
     *         was invoked
     *  @param method the method invoked
     *  @param args the arguments to the method
     *  @return result of the method
     *  @throws Throwable
     */

    @Override
    public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args)
        throws Throwable {
        
        if (args == null) {
            args = new Object[0];
        }

        java.util.Collection<Object> ret = new java.util.ArrayList<>();
        for (Object delegate : this.delegates) {
            ret.add(method.invoke(delegate, args));
        }

        return (federateReturns(ret.toArray()));
    }


    /**
     *  Stub method for subclasses to handle federated returns. This
     *  method returns null.
     *
     *  @param objects array of method returns from the delegates
     *  @return a single object return
     */

    protected Object federateReturns(Object[] objects) {
        return (null);
    }


    /**
     *  Creates the adapter for an object. There must be one
     *  registered delegate.
     *
     *  @return the proxy adapter
     */

    @SuppressWarnings("unchecked")
    protected T create() {
        return ((T) java.lang.reflect.Proxy.newProxyInstance(getClass().getClassLoader(), 
                                                             this.delegates.get(0).getClass().getInterfaces(),
                                                             this));
    }
}
    
