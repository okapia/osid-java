//
// AbstractAdapterBidLookupSession.java
//
//    A Bid lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Bid lookup session adapter.
 */

public abstract class AbstractAdapterBidLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.BidLookupSession {

    private final org.osid.bidding.BidLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBidLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBidLookupSession(org.osid.bidding.BidLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AuctionHouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform {@code Bid} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBids() {
        return (this.session.canLookupBids());
    }


    /**
     *  A complete view of the {@code Bid} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBidView() {
        this.session.useComparativeBidView();
        return;
    }


    /**
     *  A complete view of the {@code Bid} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBidView() {
        this.session.usePlenaryBidView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include bids in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only bids whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveBidView() {
        this.session.useEffectiveBidView();
        return;
    }
    

    /**
     *  All bids of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveBidView() {
        this.session.useAnyEffectiveBidView();
        return;
    }

     
    /**
     *  Gets the {@code Bid} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Bid} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Bid} and
     *  retained for compatibility.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param bidId {@code Id} of the {@code Bid}
     *  @return the bid
     *  @throws org.osid.NotFoundException {@code bidId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code bidId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Bid getBid(org.osid.id.Id bidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBid(bidId));
    }


    /**
     *  Gets a {@code BidList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  bids specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Bids} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, bids are returned that are currently effective.
     *  In any effective mode, effective bids and those currently expired
     *  are returned.
     *
     *
     *  @param  bidIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Bid} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code bidIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByIds(org.osid.id.IdList bidIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsByIds(bidIds));
    }


    /**
     *  Gets a {@code BidList} corresponding to the given
     *  bid genus {@code Type} which does not include
     *  bids of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned {@code Bid} list
     *  @throws org.osid.NullArgumentException
     *          {@code bidGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsByGenusType(bidGenusType));
    }


    /**
     *  Gets a {@code BidList} corresponding to the given
     *  bid genus {@code Type} and include any additional
     *  bids with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned {@code Bid} list
     *  @throws org.osid.NullArgumentException
     *          {@code bidGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByParentGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsByParentGenusType(bidGenusType));
    }


    /**
     *  Gets a {@code BidList} containing the given
     *  bid record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidRecordType a bid record type 
     *  @return the returned {@code Bid} list
     *  @throws org.osid.NullArgumentException
     *          {@code bidRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByRecordType(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsByRecordType(bidRecordType));
    }


    /**
     *  Gets a {@code BidList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *  
     *  In active mode, bids are returned that are currently
     *  active. In any status mode, active and inactive bids
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Bid} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.BidList getBidsOnDate(org.osid.calendaring.DateTime from, 
                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsOnDate(from, to));
    }
        

    /**
     *  Gets a list of bids corresponding to an auction
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the {@code Id} of the auction
     *  @return the returned {@code BidList}
     *  @throws org.osid.NullArgumentException {@code auctionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsForAuction(auctionId));
    }


    /**
     *  Gets a list of bids corresponding to an auction
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the {@code Id} of the auction
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code BidList}
     *  @throws org.osid.NullArgumentException {@code auctionId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionOnDate(org.osid.id.Id auctionId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsForAuctionOnDate(auctionId, from, to));
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the bidder
     *  @return the returned {@code BidList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsForBidder(resourceId));
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the bidder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code BidList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForBidderOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsForBidderOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the {@code Id} of the auction
     *  @param  resourceId the {@code Id} of the bidder
     *  @return the returned {@code BidList}
     *  @throws org.osid.NullArgumentException {@code auctionId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidder(org.osid.id.Id auctionId,
                                                               org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsForAuctionAndBidder(auctionId, resourceId));
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the bidder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code BidList}
     *  @throws org.osid.NullArgumentException {@code auctionId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidderOnDate(org.osid.id.Id auctionId,
                                                                     org.osid.id.Id resourceId,
                                                                     org.osid.calendaring.DateTime from,
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBidsForAuctionAndBidderOnDate(auctionId, resourceId, from, to));
    }


    /**
     *  Gets all winning {@code Bids.} 
     *  
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session. In
     *  both cases, the order of the set is by start effective date.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @return a list of {@code Bids} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getWinningBids());
    }


    /**
     *  Gets a list of all winning bids effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session. In
     *  both cases, the order of the set is by the start of the
     *  effective date.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Bid} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsOnDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWinningBidsOnDate(from, to));
    }


    /**
     *  Gets a list of all winning bids for an auction. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  auctionId an auction {@code Id} 
     *  @return the returned {@code Bid} list 
     *  @throws org.osid.NullArgumentException {@code auctionId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWinningBidsForAuction(auctionId));
    }

    
    /**
     *  Gets a list of winning bids for an auction and effectiveduring
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  auctionId an auction {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Bid} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code auctionId, from,} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuctionOnDate(org.osid.id.Id auctionId, 
                                                                   org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWinningBidsForAuctionOnDate(auctionId, from, to));
    }


    /**
     *  Gets a list of all winning bids for a bidder. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resourceId {@code Id} 
     *  @return the returned {@code Bid} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWinningBidsForBidder(resourceId));
    }

    
    /**
     *  Gets a list of winning bids for a bidder and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned C {@code ommission} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidderOnDate(org.osid.id.Id resourceId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getWinningBidsForBidderOnDate(resourceId, from, to));
    }

    
    /**
     *  Gets all {@code Bids}. 
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Bids} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBids());
    }
}
