//
// CanonicalUnitProcessorEnablerValidator.java
//
//     Validates a CanonicalUnitProcessorEnabler.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.offering.rules.canonicalunitprocessorenabler;


/**
 *  Validates a CanonicalUnitProcessorEnabler.
 */

public final class CanonicalUnitProcessorEnablerValidator
    extends net.okapia.osid.jamocha.builder.validator.offering.rules.canonicalunitprocessorenabler.spi.AbstractCanonicalUnitProcessorEnablerValidator {


    /**
     *  Constructs a new
     *  <code>CanonicalUnitProcessorEnablerValidator</code>.
     */

    public CanonicalUnitProcessorEnablerValidator() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>CanonicalUnitProcessorEnablerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public CanonicalUnitProcessorEnablerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a CanonicalUnitProcessorEnabler with a default
     *  validation.
     *
     *  @param canonicalUnitProcessorEnabler a canonical unit
     *         processor enabler to validate
     *  @return the canonical unit processor enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnabler</code> is
     *          <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.offering.rules.CanonicalUnitProcessorEnabler validateCanonicalUnitProcessorEnabler(org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler) {
        CanonicalUnitProcessorEnablerValidator validator = new CanonicalUnitProcessorEnablerValidator();
        validator.validate(canonicalUnitProcessorEnabler);
        return (canonicalUnitProcessorEnabler);
    }


    /**
     *  Validates a CanonicalUnitProcessorEnabler for the given
     *  validations.
     *
     *  @param validation an EnumSet of validations
     *  @param canonicalUnitProcessorEnabler a canonical unit
     *         processor enabler to validate
     *  @return the canonical unit processor enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>canonicalUnitProcessorEnabler</code> is
     *          <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.offering.rules.CanonicalUnitProcessorEnabler validateCanonicalUnitProcessorEnabler(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                                                                              org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler) {

        CanonicalUnitProcessorEnablerValidator validator = new CanonicalUnitProcessorEnablerValidator(validation);
        validator.validate(canonicalUnitProcessorEnabler);
        return (canonicalUnitProcessorEnabler);
    }
}
