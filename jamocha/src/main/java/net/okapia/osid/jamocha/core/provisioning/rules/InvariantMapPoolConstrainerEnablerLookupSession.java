//
// InvariantMapPoolConstrainerEnablerLookupSession
//
//    Implements a PoolConstrainerEnabler lookup service backed by a fixed collection of
//    poolConstrainerEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolConstrainerEnabler lookup service backed by a fixed
 *  collection of pool constrainer enablers. The pool constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPoolConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerEnablerLookupSession</code> with no
     *  pool constrainer enablers.
     *  
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumnetException {@code distributor} is
     *          {@code null}
     */

    public InvariantMapPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerEnablerLookupSession</code> with a single
     *  pool constrainer enabler.
     *  
     *  @param distributor the distributor
     *  @param poolConstrainerEnabler a single pool constrainer enabler
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainerEnabler} is <code>null</code>
     */

      public InvariantMapPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolConstrainerEnabler poolConstrainerEnabler) {
        this(distributor);
        putPoolConstrainerEnabler(poolConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerEnablerLookupSession</code> using an array
     *  of pool constrainer enablers.
     *  
     *  @param distributor the distributor
     *  @param poolConstrainerEnablers an array of pool constrainer enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolConstrainerEnabler[] poolConstrainerEnablers) {
        this(distributor);
        putPoolConstrainerEnablers(poolConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerEnablerLookupSession</code> using a
     *  collection of pool constrainer enablers.
     *
     *  @param distributor the distributor
     *  @param poolConstrainerEnablers a collection of pool constrainer enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainerEnablers} is <code>null</code>
     */

      public InvariantMapPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                               java.util.Collection<? extends org.osid.provisioning.rules.PoolConstrainerEnabler> poolConstrainerEnablers) {
        this(distributor);
        putPoolConstrainerEnablers(poolConstrainerEnablers);
        return;
    }
}
