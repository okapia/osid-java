//
// AbstractAssemblyResourceQuery.java
//
//     A ResourceQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ResourceQuery that stores terms.
 */

public abstract class AbstractAssemblyResourceQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.resource.ResourceQuery,
               org.osid.resource.ResourceQueryInspector,
               org.osid.resource.ResourceSearchOrder {

    private final java.util.Collection<org.osid.resource.records.ResourceQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.records.ResourceQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.records.ResourceSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyResourceQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyResourceQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches resources that are also groups. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchGroup(boolean match) {
        getAssembler().addBooleanTerm(getGroupColumn(), match);
        return;
    }


    /**
     *  Clears the group terms. 
     */

    @OSID @Override
    public void clearGroupTerms() {
        getAssembler().clearTerms(getGroupColumn());
        return;
    }


    /**
     *  Gets the group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getGroupTerms() {
        return (getAssembler().getBooleanTerms(getGroupColumn()));
    }


    /**
     *  Groups the search results by resources that are groups. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGroup(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGroupColumn(), style);
        return;
    }


    /**
     *  Gets the Group column name.
     *
     * @return the column name
     */

    protected String getGroupColumn() {
        return ("group");
    }


    /**
     *  Matches resources that are also demographics. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchDemographic(boolean match) {
        getAssembler().addBooleanTerm(getDemographicColumn(), match);
        return;
    }


    /**
     *  Clears the demographic terms. 
     */

    @OSID @Override
    public void clearDemographicTerms() {
        getAssembler().clearTerms(getDemographicColumn());
        return;
    }


    /**
     *  Gets the demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDemographicTerms() {
        return (getAssembler().getBooleanTerms(getDemographicColumn()));
    }


    /**
     *  Groups the search results by resources that are demographics. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDemographic(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDemographicColumn(), style);
        return;
    }


    /**
     *  Gets the Demographic column name.
     *
     * @return the column name
     */

    protected String getDemographicColumn() {
        return ("demographic");
    }


    /**
     *  Sets the group <code> Id </code> for this query to match resources 
     *  within the given group. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingGroupId(org.osid.id.Id resourceId, 
                                       boolean match) {
        getAssembler().addIdTerm(getContainingGroupIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the group <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingGroupIdTerms() {
        getAssembler().clearTerms(getContainingGroupIdColumn());
        return;
    }


    /**
     *  Gets the containing group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingGroupIdTerms() {
        return (getAssembler().getIdTerms(getContainingGroupIdColumn()));
    }


    /**
     *  Gets the ContainingGroupId column name.
     *
     * @return the column name
     */

    protected String getContainingGroupIdColumn() {
        return ("containing_group_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  containing groups. 
     *
     *  @return <code> true </code> if a group resource query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for a a containing group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getContainingGroupQuery() {
        throw new org.osid.UnimplementedException("supportsContainingGroupQuery() is false");
    }


    /**
     *  Matches resources inside any group. 
     *
     *  @param  match <code> true </code> to match any containing group, 
     *          <code> false </code> to match resources part of no groups 
     */

    @OSID @Override
    public void matchAnyContainingGroup(boolean match) {
        getAssembler().addIdWildcardTerm(getContainingGroupColumn(), match);
        return;
    }


    /**
     *  Clears the containing group terms. 
     */

    @OSID @Override
    public void clearContainingGroupTerms() {
        getAssembler().clearTerms(getContainingGroupColumn());
        return;
    }


    /**
     *  Gets the containing group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getContainingGroupTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the ContainingGroup column name.
     *
     * @return the column name
     */

    protected String getContainingGroupColumn() {
        return ("containing_group");
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAvatarId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getAvatarIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAvatarIdTerms() {
        getAssembler().clearTerms(getAvatarIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAvatarIdTerms() {
        return (getAssembler().getIdTerms(getAvatarIdColumn()));
    }


    /**
     *  Orders the result set by avatar. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAvatar(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAvatarColumn(), style);
        return;
    }


    /**
     *  Gets the AvatarId column name.
     *
     * @return the column name
     */

    protected String getAvatarIdColumn() {
        return ("avatar_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvatarQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAvatarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAvatarQuery() {
        throw new org.osid.UnimplementedException("supportsAvatarQuery() is false");
    }


    /**
     *  Matches resources with any asset. 
     *
     *  @param  match <code> true </code> to match any asset, <code> false 
     *          </code> to match resources with no asset 
     */

    @OSID @Override
    public void matchAnyAvatar(boolean match) {
        getAssembler().addIdWildcardTerm(getAvatarColumn(), match);
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAvatarTerms() {
        getAssembler().clearTerms(getAvatarColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAvatarTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Tests if an <code> AssetSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an asset search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvatarSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an asset. 
     *
     *  @return the asset search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvatarSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchOrder getAvatarSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAvatarSearchOrder() is false");
    }


    /**
     *  Gets the Avatar column name.
     *
     * @return the column name
     */

    protected String getAvatarColumn() {
        return ("avatar");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches resources with any agent. 
     *
     *  @param  match <code> true </code> to match any agent, <code> false 
     *          </code> to match resources with no agent 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        getAssembler().addIdWildcardTerm(getAgentColumn(), match);
        return;
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Sets the resource relationship <code> Id </code> for this query. 
     *
     *  @param  resourceRelationshipId the resource relationship <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceRelationshipId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchResourceRelationshipId(org.osid.id.Id resourceRelationshipId, 
                                            boolean match) {
        getAssembler().addIdTerm(getResourceRelationshipIdColumn(), resourceRelationshipId, match);
        return;
    }


    /**
     *  Clears the resource relationship <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceRelationshipIdTerms() {
        getAssembler().clearTerms(getResourceRelationshipIdColumn());
        return;
    }


    /**
     *  Gets the resource relationship <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceRelationshipIdTerms() {
        return (getAssembler().getIdTerms(getResourceRelationshipIdColumn()));
    }


    /**
     *  Gets the ResourceRelationshipId column name.
     *
     * @return the column name
     */

    protected String getResourceRelationshipIdColumn() {
        return ("resource_relationship_id");
    }


    /**
     *  Tests if a <code> ResourceRelationshipQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource relationship query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for aa resource relationship. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource relationship query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuery getResourceRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsResourceRelationshipQuery() is false");
    }


    /**
     *  Matches resources with any resource relationship. 
     *
     *  @param  match <code> true </code> to match any resource relationship, 
     *          <code> false </code> to match resources with no relationship 
     */

    @OSID @Override
    public void matchAnyResourceRelationship(boolean match) {
        getAssembler().addIdWildcardTerm(getResourceRelationshipColumn(), match);
        return;
    }


    /**
     *  Clears the resource relationship terms. 
     */

    @OSID @Override
    public void clearResourceRelationshipTerms() {
        getAssembler().clearTerms(getResourceRelationshipColumn());
        return;
    }


    /**
     *  Gets the resource relationship query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQueryInspector[] getResourceRelationshipTerms() {
        return (new org.osid.resource.ResourceRelationshipQueryInspector[0]);
    }


    /**
     *  Gets the ResourceRelationship column name.
     *
     * @return the column name
     */

    protected String getResourceRelationshipColumn() {
        return ("resource_relationship");
    }


    /**
     *  Sets the bin <code> Id </code> for this query. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        getAssembler().addIdTerm(getBinIdColumn(), binId, match);
        return;
    }


    /**
     *  Clears the bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        getAssembler().clearTerms(getBinIdColumn());
        return;
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (getAssembler().getIdTerms(getBinIdColumn()));
    }


    /**
     *  Gets the BinId column name.
     *
     * @return the column name
     */

    protected String getBinIdColumn() {
        return ("bin_id");
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        getAssembler().clearTerms(getBinColumn());
        return;
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }


    /**
     *  Gets the Bin column name.
     *
     * @return the column name
     */

    protected String getBinColumn() {
        return ("bin");
    }


    /**
     *  Tests if this resource supports the given record
     *  <code>Type</code>.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return <code>true</code> if the resourceRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceRecordType) {
        for (org.osid.resource.records.ResourceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  resourceRecordType the resource record type 
     *  @return the resource query record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceQueryRecord getResourceQueryRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  resourceRecordType the resource record type 
     *  @return the resource query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceQueryInspectorRecord getResourceQueryInspectorRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param resourceRecordType the resource record type
     *  @return the resource search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceSearchOrderRecord getResourceSearchOrderRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this resource. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceQueryRecord the resource query record
     *  @param resourceQueryInspectorRecord the resource query inspector
     *         record
     *  @param resourceSearchOrderRecord the resource search order record
     *  @param resourceRecordType resource record type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceQueryRecord</code>,
     *          <code>resourceQueryInspectorRecord</code>,
     *          <code>resourceSearchOrderRecord</code> or
     *          <code>resourceRecordTyperesource</code> is
     *          <code>null</code>
     */
            
    protected void addResourceRecords(org.osid.resource.records.ResourceQueryRecord resourceQueryRecord, 
                                      org.osid.resource.records.ResourceQueryInspectorRecord resourceQueryInspectorRecord, 
                                      org.osid.resource.records.ResourceSearchOrderRecord resourceSearchOrderRecord, 
                                      org.osid.type.Type resourceRecordType) {

        addRecordType(resourceRecordType);

        nullarg(resourceQueryRecord, "resource query record");
        nullarg(resourceQueryInspectorRecord, "resource query inspector record");
        nullarg(resourceSearchOrderRecord, "resource search odrer record");

        this.queryRecords.add(resourceQueryRecord);
        this.queryInspectorRecords.add(resourceQueryInspectorRecord);
        this.searchOrderRecords.add(resourceSearchOrderRecord);
        
        return;
    }
}
