//
// AbstractQueryRaceLookupSession.java
//
//    An inline adapter that maps a RaceLookupSession to
//    a RaceQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RaceLookupSession to
 *  a RaceQuerySession.
 */

public abstract class AbstractQueryRaceLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractRaceLookupSession
    implements org.osid.voting.RaceLookupSession {

    private boolean activeonly    = false;
    private final org.osid.voting.RaceQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRaceLookupSession.
     *
     *  @param querySession the underlying race query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRaceLookupSession(org.osid.voting.RaceQuerySession querySession) {
        nullarg(querySession, "race query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform <code>Race</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaces() {
        return (this.session.canSearchRaces());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include races in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active races are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive races are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Race</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Race</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Race</code> and
     *  retained for compatibility.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceId <code>Id</code> of the
     *          <code>Race</code>
     *  @return the race
     *  @throws org.osid.NotFoundException <code>raceId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Race getRace(org.osid.id.Id raceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();
        query.matchId(raceId, true);
        org.osid.voting.RaceList races = this.session.getRacesByQuery(query);
        if (races.hasNext()) {
            return (races.getNextRace());
        } 
        
        throw new org.osid.NotFoundException(raceId + " not found");
    }


    /**
     *  Gets a <code>RaceList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  races specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Races</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByIds(org.osid.id.IdList raceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();

        try (org.osid.id.IdList ids = raceIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRacesByQuery(query));
    }


    /**
     *  Gets a <code>RaceList</code> corresponding to the given
     *  race genus <code>Type</code> which does not include
     *  races of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceGenusType a race genus type 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByGenusType(org.osid.type.Type raceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();
        query.matchGenusType(raceGenusType, true);
        return (this.session.getRacesByQuery(query));
    }


    /**
     *  Gets a <code>RaceList</code> corresponding to the given
     *  race genus <code>Type</code> and include any additional
     *  races with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceGenusType a race genus type 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByParentGenusType(org.osid.type.Type raceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();
        query.matchParentGenusType(raceGenusType, true);
        return (this.session.getRacesByQuery(query));
    }


    /**
     *  Gets a <code>RaceList</code> containing the given
     *  race record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceRecordType a race record type 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByRecordType(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();
        query.matchRecordType(raceRecordType, true);
        return (this.session.getRacesByQuery(query));
    }


    /**
     *  Gets a <code>RaceList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known races or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  races that are accessible through this session. 
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Race</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getRacesByQuery(query));        
    }


    /**
     *  Gets a <code> RaceList </code> for the given <code>
     *  Ballot. </code>
     *  
     *  In plenary mode, the returned list contains all known races or
     *  an error results. Otherwise, the returned list may contain
     *  only those races that are accessible through this session.
     *  
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races are
     *  returned.
     *
     *  @param  ballotId a ballot <code> Id </code> 
     *  @return the returned <code> Race </code> list 
     *  @throws org.osid.NullArgumentException <code> ballotId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesForBallot(org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();
        query.matchBallotId(ballotId, true);
        return (this.session.getRacesByQuery(query));
    }


    /**
     *  Gets all <code>Races</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @return a list of <code>Races</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRaces()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.RaceQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRacesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.RaceQuery getQuery() {
        org.osid.voting.RaceQuery query = this.session.getRaceQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
