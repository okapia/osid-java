//
// AbstractActivityLookupSession.java
//
//    A starter implementation framework for providing an Activity
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Activity
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getActivities(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractActivityLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.ActivityLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Activity</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (true);
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only activities whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveActivityView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All activities of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveActivityView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Activity</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Activity</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Activity</code> and
     *  retained for compatibility.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityId <code>Id</code> of the
     *          <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.ActivityList activities = getActivities()) {
            while (activities.hasNext()) {
                org.osid.financials.Activity activity = activities.getNextActivity();
                if (activity.getId().equals(activityId)) {
                    return (activity);
                }
            }
        } 

        throw new org.osid.NotFoundException(activityId + " not found");
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, activities are returned that are currently effective.
     *  In any effective mode, effective activities and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActivities()</code>.
     *
     *  @param  activityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.Activity> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = activityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getActivity(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("activity " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.activity.LinkedActivityList(ret));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently effective.
     *  In any effective mode, effective activities and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.activity.ActivityGenusFilterList(getActivities(), activityGenusType));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> and include any additional
     *  activities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivitiesByGenusType(activityGenusType));
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.activity.ActivityRecordFilterList(getActivities(), activityRecordType));
    }


    /**
     *  Gets an <code>ActivityList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *  
     *  In active mode, activities are returned that are currently
     *  active. In any status mode, active and inactive activities
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesOnDate(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.activity.TemporalActivityFilterList(getActivities(), from, to));
    }
        

    /**
     *  Gets an <code> ActivityList </code> associated wuth the given
     *  organization. In plenary mode, the returned list contains all
     *  known activities or an error results. Otherwise, the returned
     *  list may contain only those activities that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByOrganization(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.activity.ActivityFilterList(new OrganizationFilter(resourceId), getActivities()));
    }



    /**
     *  Gets an <code>ActivityList</code> for an organization
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *  
     *  In active mode, activities are returned that are currently
     *  active. In any status mode, active and inactive activities
     *  are returned.
     *
     *  @param resourceId a resource Id
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByOrganizationOnDate(org.osid.id.Id resourceId, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.activity.TemporalActivityFilterList(getActivitiesByOrganization(resourceId), from, to));
    }
        

    /**
     *  Gets an <code> ActivityList </code> associated with the given
     *  code. In plenary mode, the returned list contains all
     *  known activities or an error results. Otherwise, the returned
     *  list may contain only those activities that are accessible
     *  through this session.
     *
     *  @param  code an activity code
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.activity.ActivityFilterList(new CodeFilter(code), getActivities()));
    }


    /**
     *  Gets all <code>Activities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.financials.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the activity list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of activities
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.financials.ActivityList filterActivitiesOnViews(org.osid.financials.ActivityList list)
        throws org.osid.OperationFailedException {

        org.osid.financials.ActivityList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.financials.activity.EffectiveActivityFilterList(ret);
        }

        return (ret);
    }


    public static class OrganizationFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.activity.ActivityFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>OrganizationFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public OrganizationFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the ActivityFilterList to filter the activity list
         *  based on organization.
         *
         *  @param activity the activity
         *  @return <code>true</code> to pass the activity,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.Activity activity) {
            return (activity.getOrganizationId().equals(this.resourceId));
        }
    }


    public static class CodeFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.activity.ActivityFilter {

        private final String code;

        
        /**
         *  Constructs a new <code>CodeFilter</code>.
         *
         *  @param code the code to filter
         *  @throws org.osid.NullArgumentException <code>code</code>
         *          is <code>null</code>
         */

        public CodeFilter(String code) {
            nullarg(code, "code");
            this.code = code;
            return;
        }


        /**
         *  Used by the ActivityFilterList to filter the activity list
         *  based on code.
         *
         *  @param activity the activity
         *  @return <code>true</code> to pass the activity,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.Activity activity) {
            return (activity.getCode().equals(this.code));
        }
    }
}
