//
// AbstractCompetency.java
//
//     Defines a Competency builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.competency.spi;


/**
 *  Defines a <code>Competency</code> builder.
 */

public abstract class AbstractCompetencyBuilder<T extends AbstractCompetencyBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resourcing.competency.CompetencyMiter competency;


    /**
     *  Constructs a new <code>AbstractCompetencyBuilder</code>.
     *
     *  @param competency the competency to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCompetencyBuilder(net.okapia.osid.jamocha.builder.resourcing.competency.CompetencyMiter competency) {
        super(competency);
        this.competency = competency;
        return;
    }


    /**
     *  Builds the competency.
     *
     *  @return the new competency
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resourcing.Competency build() {
        (new net.okapia.osid.jamocha.builder.validator.resourcing.competency.CompetencyValidator(getValidations())).validate(this.competency);
        return (new net.okapia.osid.jamocha.builder.resourcing.competency.ImmutableCompetency(this.competency));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the competency miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resourcing.competency.CompetencyMiter getMiter() {
        return (this.competency);
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public T learningObjective(org.osid.learning.Objective objective) {
        getMiter().addLearningObjective(objective);
        return (self());
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    public T learningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        getMiter().setLearningObjectives(objectives);
        return (self());
    }


    /**
     *  Adds a Competency record.
     *
     *  @param record a competency record
     *  @param recordType the type of competency record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resourcing.records.CompetencyRecord record, org.osid.type.Type recordType) {
        getMiter().addCompetencyRecord(record, recordType);
        return (self());
    }
}       


