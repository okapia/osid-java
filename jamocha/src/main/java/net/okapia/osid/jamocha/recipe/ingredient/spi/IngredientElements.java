//
// IngredientElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.ingredient.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class IngredientElements
    extends net.okapia.osid.jamocha.spi.IdentifiableElements {


    /**
     *  Gets the IngredientElement Id.
     *
     *  @return the ingredient element Id
     */

    public static org.osid.id.Id getIngredientEntityId() {
        return (makeEntityId("osid.recipe.Ingredient"));
    }


    /**
     *  Gets the Quantity element Id.
     *
     *  @return the Quantity element Id
     */

    public static org.osid.id.Id getQuantity() {
        return (makeElementId("osid.recipe.ingredient.Quantity"));
    }


    /**
     *  Gets the UnitType element Id.
     *
     *  @return the UnitType element Id
     */

    public static org.osid.id.Id getUnitType() {
        return (makeElementId("osid.recipe.ingredient.UnitType"));
    }


    /**
     *  Gets the StockId element Id.
     *
     *  @return the StockId element Id
     */

    public static org.osid.id.Id getStockId() {
        return (makeElementId("osid.recipe.ingredient.StockId"));
    }


    /**
     *  Gets the Stock element Id.
     *
     *  @return the Stock element Id
     */

    public static org.osid.id.Id getStock() {
        return (makeElementId("osid.recipe.ingredient.Stock"));
    }
}
