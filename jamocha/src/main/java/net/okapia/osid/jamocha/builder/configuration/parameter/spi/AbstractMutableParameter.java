//
// AbstractMutableParameter.java
//
//     Defines a mutable Parameter.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Parameter</code>.
 */

public abstract class AbstractMutableParameter
    extends net.okapia.osid.jamocha.configuration.parameter.spi.AbstractParameter
    implements org.osid.configuration.Parameter,
               net.okapia.osid.jamocha.builder.configuration.parameter.ParameterMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this parameter. 
     *
     *  @param record parameter record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addParameterRecord(org.osid.configuration.records.ParameterRecord record, org.osid.type.Type recordType) {
        super.addParameterRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this parameter. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this parameter. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *          disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this parameter.
     *
     *  @param displayName the name for this parameter
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this parameter.
     *
     *  @param description the description of this parameter
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException
     *          <code>rule</code> is <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the value syntax.
     *
     *  @param syntax a value syntax
     *  @throws org.osid.NullArgumentException <code>syntax</code> is
     *          <code>null</code>
     */

    @Override
    public void setValueSyntax(org.osid.Syntax syntax) {
        super.setValueSyntax(syntax);
        return;
    }


    /**
     *  Sets the value coordinate type.
     *
     *  @param coordinateType a value coordinate type
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code> is <code>null</code>
     */

    @Override
    public void setValueCoordinateType(org.osid.type.Type coordinateType) {
        super.setValueCoordinateType(coordinateType);
        return;
    }


    /**
     *  Sets the value heading type.
     *
     *  @param headingType a value heading type
     *  @throws org.osid.NullArgumentException
     *          <code>headingType</code> is <code>null</code>
     */

    @Override
    public void setValueHeadingType(org.osid.type.Type headingType) {
        super.setValueHeadingType(headingType);
        return;
    }


    /**
     *  Sets the value spatial unit record type.
     *
     *  @param spatialUnitRecordType a value spatial unit record type
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnitRecordType</code> is
     *          <code>null</code>
     */

    @Override
    public void setValueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        super.setValueSpatialUnitRecordType(spatialUnitRecordType);
        return;
    }


    /**
     *  Sets the value object type.
     *
     *  @param objectType a value object type
     *  @throws org.osid.NullArgumentException <code>objectType</code>
     *          is <code>null</code>
     */

    @Override
    public void setValueObjectType(org.osid.type.Type objectType) {
        super.setValueObjectType(objectType);
        return;
    }


    /**
     *  Sets the version scheme.
     *
     *  @param versionType an version scheme
     *  @throws org.osid.NullArgumentException
     *          <code>versionType</code> is <code>null</code>
     */

    @Override
    public void setValueVersionScheme(org.osid.type.Type versionType) {
        super.setValueVersionScheme(versionType);
        return;
    }

    
    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code> true </code> if the values are
     *          shuffled, <code> false </code> otherwise
     */

    @Override
    public void setShuffled(boolean shuffled) {
        super.setShuffled(shuffled);
        return;
    }
}

