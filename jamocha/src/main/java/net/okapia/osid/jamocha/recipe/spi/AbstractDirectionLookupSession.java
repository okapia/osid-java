//
// AbstractDirectionLookupSession.java
//
//    A starter implementation framework for providing a Direction
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Direction
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getDirections(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractDirectionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.recipe.DirectionLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.recipe.Cookbook cookbook = new net.okapia.osid.jamocha.nil.recipe.cookbook.UnknownCookbook();
    

    /**
     *  Gets the <code>Cookbook/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Cookbook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.cookbook.getId());
    }


    /**
     *  Gets the <code>Cookbook</code> associated with this 
     *  session.
     *
     *  @return the <code>Cookbook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.cookbook);
    }


    /**
     *  Sets the <code>Cookbook</code>.
     *
     *  @param  cookbook the cookbook for this session
     *  @throws org.osid.NullArgumentException <code>cookbook</code>
     *          is <code>null</code>
     */

    protected void setCookbook(org.osid.recipe.Cookbook cookbook) {
        nullarg(cookbook, "cookbook");
        this.cookbook = cookbook;
        return;
    }


    /**
     *  Tests if this user can perform <code>Direction</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDirections() {
        return (true);
    }


    /**
     *  A complete view of the <code>Direction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDirectionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Direction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDirectionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include directions in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Direction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Direction</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Direction</code> and
     *  retained for compatibility.
     *
     *  @param  directionId <code>Id</code> of the
     *          <code>Direction</code>
     *  @return the direction
     *  @throws org.osid.NotFoundException <code>directionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>directionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Direction getDirection(org.osid.id.Id directionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.recipe.DirectionList directions = getDirections()) {
            while (directions.hasNext()) {
                org.osid.recipe.Direction direction = directions.getNextDirection();
                if (direction.getId().equals(directionId)) {
                    return (direction);
                }
            }
        } 

        throw new org.osid.NotFoundException(directionId + " not found");
    }


    /**
     *  Gets a <code>DirectionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  directions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Directions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getDirections()</code>.
     *
     *  @param  directionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>directionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByIds(org.osid.id.IdList directionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.recipe.Direction> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = directionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getDirection(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("direction " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.recipe.direction.LinkedDirectionList(ret));
    }


    /**
     *  Gets a <code>DirectionList</code> corresponding to the given
     *  direction genus <code>Type</code> which does not include
     *  directions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getDirections()</code>.
     *
     *  @param  directionGenusType a direction genus type 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByGenusType(org.osid.type.Type directionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recipe.direction.DirectionGenusFilterList(getDirections(), directionGenusType));
    }


    /**
     *  Gets a <code>DirectionList</code> corresponding to the given
     *  direction genus <code>Type</code> and include any additional
     *  directions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDirections()</code>.
     *
     *  @param  directionGenusType a direction genus type 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByParentGenusType(org.osid.type.Type directionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDirectionsByGenusType(directionGenusType));
    }


    /**
     *  Gets a <code>DirectionList</code> containing the given
     *  direction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDirections()</code>.
     *
     *  @param  directionRecordType a direction record type 
     *  @return the returned <code>Direction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByRecordType(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recipe.direction.DirectionRecordFilterList(getDirections(), directionRecordType));
    }


    /**
     *  Gets a <code>DirectionList</code> for the given recipe
     *  <code>Id</code.> In plenary mode, the returned list contains
     *  all known directions or an error results. Otherwise, the
     *  returned list may contain only those directions that are
     *  accessible through this session.
     *
     *  @param  recipeId a recipe <code>Id</code> 
     *  @return the returned <code>Direction</code> list 
     *  @throws org.osid.NullArgumentException <code>recipeId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsForRecipe(org.osid.id.Id recipeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recipe.direction.DirectionFilterList(new RecipeFilter(recipeId), getDirections()));
    }


    /**
     *  Gets all <code>Directions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Directions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.recipe.DirectionList getDirections()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the direction list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of directions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.recipe.DirectionList filterDirectionsOnViews(org.osid.recipe.DirectionList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class RecipeFilter
        implements net.okapia.osid.jamocha.inline.filter.recipe.direction.DirectionFilter {
         
        private final org.osid.id.Id recipeId;
         
         
        /**
         *  Constructs a new <code>RecipeFilter</code>.
         *
         *  @param recipeId the recipe to filter
         *  @throws org.osid.NullArgumentException
         *          <code>recipeId</code> is <code>null</code>
         */
        
        public RecipeFilter(org.osid.id.Id recipeId) {
            nullarg(recipeId, "recipe Id");
            this.recipeId = recipeId;
            return;
        }

         
        /**
         *  Used by the DirectionFilterList to filter the 
         *  direction list based on recipe.
         *
         *  @param direction the direction
         *  @return <code>true</code> to pass the direction,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.recipe.Direction direction) {
            return (direction.getRecipeId().equals(this.recipeId));
        }
    }    
}
