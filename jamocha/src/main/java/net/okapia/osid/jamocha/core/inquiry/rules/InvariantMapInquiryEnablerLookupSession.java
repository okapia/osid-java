//
// InvariantMapInquiryEnablerLookupSession
//
//    Implements an InquiryEnabler lookup service backed by a fixed collection of
//    inquiryEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules;


/**
 *  Implements an InquiryEnabler lookup service backed by a fixed
 *  collection of inquiry enablers. The inquiry enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapInquiryEnablerLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.rules.spi.AbstractMapInquiryEnablerLookupSession
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapInquiryEnablerLookupSession</code> with no
     *  inquiry enablers.
     *  
     *  @param inquest the inquest
     *  @throws org.osid.NullArgumnetException {@code inquest} is
     *          {@code null}
     */

    public InvariantMapInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest) {
        setInquest(inquest);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInquiryEnablerLookupSession</code> with a single
     *  inquiry enabler.
     *  
     *  @param inquest the inquest
     *  @param inquiryEnabler an single inquiry enabler
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code inquiryEnabler} is <code>null</code>
     */

      public InvariantMapInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.rules.InquiryEnabler inquiryEnabler) {
        this(inquest);
        putInquiryEnabler(inquiryEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInquiryEnablerLookupSession</code> using an array
     *  of inquiry enablers.
     *  
     *  @param inquest the inquest
     *  @param inquiryEnablers an array of inquiry enablers
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code inquiryEnablers} is <code>null</code>
     */

      public InvariantMapInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.rules.InquiryEnabler[] inquiryEnablers) {
        this(inquest);
        putInquiryEnablers(inquiryEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInquiryEnablerLookupSession</code> using a
     *  collection of inquiry enablers.
     *
     *  @param inquest the inquest
     *  @param inquiryEnablers a collection of inquiry enablers
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code inquiryEnablers} is <code>null</code>
     */

      public InvariantMapInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                               java.util.Collection<? extends org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers) {
        this(inquest);
        putInquiryEnablers(inquiryEnablers);
        return;
    }
}
