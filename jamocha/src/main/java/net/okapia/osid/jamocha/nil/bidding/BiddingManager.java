//
// BiddingManager
//
//     Defines a null BiddingManager.
//
//
// Tom Coppeto
// Okapia
// 8 October 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.bidding;


/**
 *  Defines a do-nothing null <code>BiddingManager</code>.
 */

public final class BiddingManager 
    extends net.okapia.osid.jamocha.bidding.spi.AbstractBiddingManager
    implements org.osid.bidding.BiddingManager,
               org.osid.bidding.BiddingProxyManager {

    private static final String ID           = "urn:osid:okapia.net:identifiers:providers:bidding:nil";
    private static final String DISPLAY_NAME = "Null Bidding Service";
    private static final String DESCRIPTION  = "This Bidding service does absolutely nothing.";
    private static final String VERSION      = "1.0.0";
    private static final String RELEASE_DATE = "1919-1-15 12:40Z";


    /**
     *  Constructs a new <code>BiddingManager</code>.
     */

    public BiddingManager() {
        super(new BiddingProvider());
        return;
    }

    
    static class BiddingProvider     
        extends net.okapia.osid.provider.spi.AbstractServiceProvider
        implements net.okapia.osid.provider.ServiceProvider {

        BiddingProvider() {
            setServiceId(ID);
            setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
            setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
            setImplementationVersion(VERSION);
            setReleaseDate(RELEASE_DATE);
            setProvider(net.okapia.osid.provider.Providers.OKAPIA.getProvider());

            return;
        }
    }        
}

