//
// BrokerProcessorElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BrokerProcessorElements
    extends net.okapia.osid.jamocha.spi.OsidProcessorElements {


    /**
     *  Gets the BrokerProcessorElement Id.
     *
     *  @return the broker processor element Id
     */

    public static org.osid.id.Id getBrokerProcessorEntityId() {
        return (makeEntityId("osid.provisioning.rules.BrokerProcessor"));
    }


    /**
     *  Gets the FixedLeaseDuration element Id.
     *
     *  @return the FixedLeaseDuration element Id
     */

    public static org.osid.id.Id getFixedLeaseDuration() {
        return (makeElementId("osid.provisioning.rules.brokerprocessor.FixedLeaseDuration"));
    }


    /**
     *  Gets the Leasing element Id.
     *
     *  @return the Leasing element Id
     */

    public static org.osid.id.Id getLeasing() {
        return (makeElementId("osid.provisioning.rules.brokerprocessor.Leasing"));
    }


    /**
     *  Gets the MustReturnProvisions element Id.
     *
     *  @return the MustReturnProvisions element Id
     */

    public static org.osid.id.Id getMustReturnProvisions() {
        return (makeElementId("osid.provisioning.rules.brokerprocessor.MustReturnProvisions"));
    }


    /**
     *  Gets the AllowsProvisionExchange element Id.
     *
     *  @return the AllowsProvisionExchange element Id
     */

    public static org.osid.id.Id getAllowsProvisionExchange() {
        return (makeElementId("osid.provisioning.rules.brokerprocessor.AllowsProvisionExchange"));
    }


    /**
     *  Gets the AllowsCompoundRequests element Id.
     *
     *  @return the AllowsCompoundRequests element Id
     */

    public static org.osid.id.Id getAllowsCompoundRequests() {
        return (makeElementId("osid.provisioning.rules.brokerprocessor.AllowsCompoundRequests"));
    }


    /**
     *  Gets the RuledBrokerId element Id.
     *
     *  @return the RuledBrokerId element Id
     */

    public static org.osid.id.Id getRuledBrokerId() {
        return (makeQueryElementId("osid.provisioning.rules.brokerprocessor.RuledBrokerId"));
    }


    /**
     *  Gets the RuledBroker element Id.
     *
     *  @return the RuledBroker element Id
     */

    public static org.osid.id.Id getRuledBroker() {
        return (makeQueryElementId("osid.provisioning.rules.brokerprocessor.RuledBroker"));
    }


    /**
     *  Gets the DistributorId element Id.
     *
     *  @return the DistributorId element Id
     */

    public static org.osid.id.Id getDistributorId() {
        return (makeQueryElementId("osid.provisioning.rules.brokerprocessor.DistributorId"));
    }


    /**
     *  Gets the Distributor element Id.
     *
     *  @return the Distributor element Id
     */

    public static org.osid.id.Id getDistributor() {
        return (makeQueryElementId("osid.provisioning.rules.brokerprocessor.Distributor"));
    }
}
