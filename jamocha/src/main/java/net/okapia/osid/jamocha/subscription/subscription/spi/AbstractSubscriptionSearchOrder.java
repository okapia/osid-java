//
// AbstractSubscriptionSearchOdrer.java
//
//     Defines a SubscriptionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code SubscriptionSearchOrder}.
 */

public abstract class AbstractSubscriptionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.subscription.SubscriptionSearchOrder {

    private final java.util.Collection<org.osid.subscription.records.SubscriptionSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the dispatch. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDispatch(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a dispatch order is available. 
     *
     *  @return <code> true </code> if a dispatch order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchSearchOrder() {
        return (false);
    }


    /**
     *  Gets the dispatch order. 
     *
     *  @return the dispatch search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchOrder getDispatchSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDispatchSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the subscriber. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubscriber(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a subscriber order is available. 
     *
     *  @return <code> true </code> if a subscriber order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriberSearchOrder() {
        return (false);
    }


    /**
     *  Gets the subscriber order. 
     *
     *  @return the subscriber search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriberSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSubscriberSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubscriberSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the address. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddress(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a address order is available. 
     *
     *  @return <code> true </code> if an address order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressSearchOrder() {
        return (false);
    }


    /**
     *  Gets the address order. 
     *
     *  @return the address search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSearchOrder getAddressSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAddressSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  subscriptionRecordType a subscription record type 
     *  @return {@code true} if the subscriptionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type subscriptionRecordType) {
        for (org.osid.subscription.records.SubscriptionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  subscriptionRecordType the subscription record type 
     *  @return the subscription search order record
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(subscriptionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionSearchOrderRecord getSubscriptionSearchOrderRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.SubscriptionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this subscription. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param subscriptionRecord the subscription search odrer record
     *  @param subscriptionRecordType subscription record type
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionRecord} or
     *          {@code subscriptionRecordTypesubscription} is
     *          {@code null}
     */
            
    protected void addSubscriptionRecord(org.osid.subscription.records.SubscriptionSearchOrderRecord subscriptionSearchOrderRecord, 
                                     org.osid.type.Type subscriptionRecordType) {

        addRecordType(subscriptionRecordType);
        this.records.add(subscriptionSearchOrderRecord);
        
        return;
    }
}
