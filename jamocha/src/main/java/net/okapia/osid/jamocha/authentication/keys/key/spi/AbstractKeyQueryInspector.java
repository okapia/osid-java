//
// AbstractKeyQueryInspector.java
//
//     A template for making a KeyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for keys.
 */

public abstract class AbstractKeyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.authentication.keys.KeyQueryInspector {

    private final java.util.Collection<org.osid.authentication.keys.records.KeyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given key query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a key implementing the requested record.
     *
     *  @param keyRecordType a key record type
     *  @return the key query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(keyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeyQueryInspectorRecord getKeyQueryInspectorRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.keys.records.KeyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(keyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this key query. 
     *
     *  @param keyQueryInspectorRecord key query inspector
     *         record
     *  @param keyRecordType key record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addKeyQueryInspectorRecord(org.osid.authentication.keys.records.KeyQueryInspectorRecord keyQueryInspectorRecord, 
                                                   org.osid.type.Type keyRecordType) {

        addRecordType(keyRecordType);
        nullarg(keyRecordType, "key record type");
        this.records.add(keyQueryInspectorRecord);        
        return;
    }
}
