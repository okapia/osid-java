//
// AbstractImmutablePool.java
//
//     Wraps a mutable Pool to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.pool.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Pool</code> to hide modifiers. This
 *  wrapper provides an immutized Pool from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying pool whose state changes are visible.
 */

public abstract class AbstractImmutablePool
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.provisioning.Pool {

    private final org.osid.provisioning.Pool pool;


    /**
     *  Constructs a new <code>AbstractImmutablePool</code>.
     *
     *  @param pool the pool to immutablize
     *  @throws org.osid.NullArgumentException <code>pool</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePool(org.osid.provisioning.Pool pool) {
        super(pool);
        this.pool = pool;
        return;
    }


    /**
     *  Gets the broker <code> Id. </code> 
     *
     *  @return the broker <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBrokerId() {
        return (this.pool.getBrokerId());
    }


    /**
     *  Gets the broker. 
     *
     *  @return the broker 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker()
        throws org.osid.OperationFailedException {

        return (this.pool.getBroker());
    }


    /**
     *  Tests if a pool size is available. 
     *
     *  @return <code> true </code> if a queue size is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSize() {
        return (this.pool.hasSize());
    }


    /**
     *  Gets the size of the pool. 
     *
     *  @return the size 
     *  @throws org.osid.IllegalStateException <code> hasSize() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getSize() {
        return (this.pool.getSize());
    }


    /**
     *  Gets the pool record corresponding to the given <code> Pool </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> poolRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(poolRecordType) </code> is <code> true </code> . 
     *
     *  @param  poolRecordType the type of pool record to retrieve 
     *  @return the pool record 
     *  @throws org.osid.NullArgumentException <code> poolRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(poolRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolRecord getPoolRecord(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException {

        return (this.pool.getPoolRecord(poolRecordType));
    }
}

