//
// AbstractFederatingInquiryLookupSession.java
//
//     An abstract federating adapter for an InquiryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  InquiryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingInquiryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inquiry.InquiryLookupSession>
    implements org.osid.inquiry.InquiryLookupSession {

    private boolean parallel = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();


    /**
     *  Constructs a new <code>AbstractFederatingInquiryLookupSession</code>.
     */

    protected AbstractFederatingInquiryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inquiry.InquiryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Inquest/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Inquest Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.inquest.getId());
    }


    /**
     *  Gets the <code>Inquest</code> associated with this 
     *  session.
     *
     *  @return the <code>Inquest</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.inquest);
    }


    /**
     *  Sets the <code>Inquest</code>.
     *
     *  @param  inquest the inquest for this session
     *  @throws org.osid.NullArgumentException <code>inquest</code>
     *          is <code>null</code>
     */

    protected void setInquest(org.osid.inquiry.Inquest inquest) {
        nullarg(inquest, "inquest");
        this.inquest = inquest;
        return;
    }


    /**
     *  Tests if this user can perform <code>Inquiry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInquiries() {
        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            if (session.canLookupInquiries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Inquiry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInquiryView() {
        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            session.useComparativeInquiryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Inquiry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInquiryView() {
        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            session.usePlenaryInquiryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inquiries in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            session.useFederatedInquestView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            session.useIsolatedInquestView();
        }

        return;
    }


    /**
     *  Only active inquiries are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInquiryView() {
        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            session.useActiveInquiryView();
        }

        return;
    }


    /**
     *  Active and inactive inquiries are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInquiryView() {
        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            session.useAnyStatusInquiryView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Inquiry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Inquiry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Inquiry</code> and
     *  retained for compatibility.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @param  inquiryId <code>Id</code> of the
     *          <code>Inquiry</code>
     *  @return the inquiry
     *  @throws org.osid.NotFoundException <code>inquiryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquiry getInquiry(org.osid.id.Id inquiryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            try {
                return (session.getInquiry(inquiryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(inquiryId + " not found");
    }


    /**
     *  Gets an <code>InquiryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inquiries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Inquiries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInquiries()</code>.
     *
     *  @param  inquiryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Inquiry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByIds(org.osid.id.IdList inquiryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inquiry.inquiry.MutableInquiryList ret = new net.okapia.osid.jamocha.inquiry.inquiry.MutableInquiryList();

        try (org.osid.id.IdList ids = inquiryIds) {
            while (ids.hasNext()) {
                ret.addInquiry(getInquiry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>InquiryList</code> corresponding to the given
     *  inquiry genus <code>Type</code> which does not include
     *  inquiries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInquiries()</code>.
     *
     *  @param  inquiryGenusType an inquiry genus type 
     *  @return the returned <code>Inquiry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByGenusType(org.osid.type.Type inquiryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.FederatingInquiryList ret = getInquiryList();

        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            ret.addInquiryList(session.getInquiriesByGenusType(inquiryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquiryList</code> corresponding to the given
     *  inquiry genus <code>Type</code> and include any additional
     *  inquiries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquiries()</code>.
     *
     *  @param  inquiryGenusType an inquiry genus type 
     *  @return the returned <code>Inquiry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByParentGenusType(org.osid.type.Type inquiryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.FederatingInquiryList ret = getInquiryList();

        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            ret.addInquiryList(session.getInquiriesByParentGenusType(inquiryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquiryList</code> containing the given
     *  inquiry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquiries()</code>.
     *
     *  @param  inquiryRecordType an inquiry record type 
     *  @return the returned <code>Inquiry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByRecordType(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.FederatingInquiryList ret = getInquiryList();

        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            ret.addInquiryList(session.getInquiriesByRecordType(inquiryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of inquiries for a supplied audit. <code> </code>
     *
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @param  auditId an audit <code> Id </code>
     *  @return the returned <code> Inquiry </code> list
     *  @throws org.osid.NullArgumentException <code> auditId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesForAudit(org.osid.id.Id auditId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.FederatingInquiryList ret = getInquiryList();

        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            ret.addInquiryList(session.getInquiriesForAudit(auditId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Inquiries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inquiries or an error results. Otherwise, the returned list
     *  may contain only those inquiries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiries are returned that are currently
     *  active. In any status mode, active and inactive inquiries
     *  are returned.
     *
     *  @return a list of <code>Inquiries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.FederatingInquiryList ret = getInquiryList();

        for (org.osid.inquiry.InquiryLookupSession session : getSessions()) {
            ret.addInquiryList(session.getInquiries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.FederatingInquiryList getInquiryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.ParallelInquiryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.inquiry.CompositeInquiryList());
        }
    }
}
