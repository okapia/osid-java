//
// AbstractAssemblyProgramOfferingQuery.java
//
//     A ProgramOfferingQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.program.programoffering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProgramOfferingQuery that stores terms.
 */

public abstract class AbstractAssemblyProgramOfferingQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.program.ProgramOfferingQuery,
               org.osid.course.program.ProgramOfferingQueryInspector,
               org.osid.course.program.ProgramOfferingSearchOrder {

    private final java.util.Collection<org.osid.course.program.records.ProgramOfferingQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.ProgramOfferingQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.ProgramOfferingSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProgramOfferingQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProgramOfferingQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the program <code> Id </code> for this query to match program 
     *  offerings that have a related program. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        getAssembler().addIdTerm(getProgramIdColumn(), programId, match);
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        getAssembler().clearTerms(getProgramIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (getAssembler().getIdTerms(getProgramIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProgramColumn(), style);
        return;
    }


    /**
     *  Gets the ProgramId column name.
     *
     * @return the column name
     */

    protected String getProgramIdColumn() {
        return ("program_id");
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        getAssembler().clearTerms(getProgramColumn());
        return;
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program query terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }


    /**
     *  Gets the Program column name.
     *
     * @return the column name
     */

    protected String getProgramColumn() {
        return ("program");
    }


    /**
     *  Sets the term <code> Id </code> for this query to match program 
     *  offerings that have a related term. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        getAssembler().clearTerms(getTermIdColumn());
        return;
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (getAssembler().getIdTerms(getTermIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by term. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerm(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTermColumn(), style);
        return;
    }


    /**
     *  Gets the TermId column name.
     *
     * @return the column name
     */

    protected String getTermIdColumn() {
        return ("term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a reporting term. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the reporting term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        getAssembler().clearTerms(getTermColumn());
        return;
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term query terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Tests if a term order is available. 
     *
     *  @return <code> true </code> if a term order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearchOrder() {
        return (false);
    }


    /**
     *  Gets the term order. 
     *
     *  @return the term search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchOrder getTermSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTermSearchOrder() is false");
    }


    /**
     *  Gets the Term column name.
     *
     * @return the column name
     */

    protected String getTermColumn() {
        return ("term");
    }


    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          title, <code> false </code> to match program offerings with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program offering 
     *  title. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Adds a program number for this query. 
     *
     *  @param  number program number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        getAssembler().addStringTerm(getNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches a program number that has any value. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          number, <code> false </code> to match program offerings with 
     *          no number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getNumberColumn(), match);
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        getAssembler().clearTerms(getNumberColumn());
        return;
    }


    /**
     *  Gets the bumber query terms. 
     *
     *  @return the number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (getAssembler().getStringTerms(getNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program offering 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumberColumn(), style);
        return;
    }


    /**
     *  Gets the Number column name.
     *
     * @return the column name
     */

    protected String getNumberColumn() {
        return ("number");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match program 
     *  offerings that have an instructor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getInstructorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the instructor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearInstructorIdTerms() {
        getAssembler().clearTerms(getInstructorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstructorIdTerms() {
        return (getAssembler().getIdTerms(getInstructorIdColumn()));
    }


    /**
     *  Gets the InstructorId column name.
     *
     * @return the column name
     */

    protected String getInstructorIdColumn() {
        return ("instructor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instructor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getInstructorQuery() {
        throw new org.osid.UnimplementedException("supportsInstructorQuery() is false");
    }


    /**
     *  Matches program offerings that have any instructor. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          instructor, <code> false </code> to match program offerings 
     *          with no instructors 
     */

    @OSID @Override
    public void matchAnyInstructor(boolean match) {
        getAssembler().addIdWildcardTerm(getInstructorColumn(), match);
        return;
    }


    /**
     *  Clears the instructor terms. 
     */

    @OSID @Override
    public void clearInstructorTerms() {
        getAssembler().clearTerms(getInstructorColumn());
        return;
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getInstructorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Instructor column name.
     *
     * @return the column name
     */

    protected String getInstructorColumn() {
        return ("instructor");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match programs 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches programs that have any sponsor. 
     *
     *  @param  match <code> true </code> to match programs with any sponsor, 
     *          <code> false </code> to match programs with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        getAssembler().addIdWildcardTerm(getSponsorColumn(), match);
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Matches programs with the prerequisites informational string. 
     *
     *  @param  requirementsInfo completion requirements string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> requirementsInfo 
     *          </code> not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> requirementsInfo </code> 
     *          or <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsInfo(String requirementsInfo, 
                                                org.osid.type.Type stringMatchType, 
                                                boolean match) {
        getAssembler().addStringTerm(getCompletionRequirementsInfoColumn(), requirementsInfo, stringMatchType, match);
        return;
    }


    /**
     *  Matches a program that has any completion requirements information 
     *  assigned. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          completion requirements information, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public void matchAnyCompletionRequirementsInfo(boolean match) {
        getAssembler().addStringWildcardTerm(getCompletionRequirementsInfoColumn(), match);
        return;
    }


    /**
     *  Clears the completion requirements info terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsInfoTerms() {
        getAssembler().clearTerms(getCompletionRequirementsInfoColumn());
        return;
    }


    /**
     *  Gets the completion requirements query terms. 
     *
     *  @return the prereq query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCompletionRequirementsInfoTerms() {
        return (getAssembler().getStringTerms(getCompletionRequirementsInfoColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program 
     *  completion requirements. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompletionRequirementsInfo(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompletionRequirementsInfoColumn(), style);
        return;
    }


    /**
     *  Gets the CompletionRequirementsInfo column name.
     *
     * @return the column name
     */

    protected String getCompletionRequirementsInfoColumn() {
        return ("completion_requirements_info");
    }


    /**
     *  Sets the requisite <code> Id </code> for this query. 
     *
     *  @param  ruleId a rule <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsId(org.osid.id.Id ruleId, 
                                              boolean match) {
        getAssembler().addIdTerm(getCompletionRequirementsIdColumn(), ruleId, match);
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsIdTerms() {
        getAssembler().clearTerms(getCompletionRequirementsIdColumn());
        return;
    }


    /**
     *  Gets the completion requirements requisite <code> Id </code> query 
     *  terms. 
     *
     *  @return the requisite <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompletionRequirementsIdTerms() {
        return (getAssembler().getIdTerms(getCompletionRequirementsIdColumn()));
    }


    /**
     *  Gets the CompletionRequirementsId column name.
     *
     * @return the column name
     */

    protected String getCompletionRequirementsIdColumn() {
        return ("completion_requirements_id");
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompletionRequirementsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompletionRequirementsQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getCompletionRequirementsQuery() {
        throw new org.osid.UnimplementedException("supportsCompletionRequirementsQuery() is false");
    }


    /**
     *  Matches programs that have any completion requirement requisite. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          requisite, <code> false </code> to match programs with no 
     *          requisites 
     */

    @OSID @Override
    public void matchAnyCompletionRequirements(boolean match) {
        getAssembler().addIdWildcardTerm(getCompletionRequirementsColumn(), match);
        return;
    }


    /**
     *  Clears the requisite terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsTerms() {
        getAssembler().clearTerms(getCompletionRequirementsColumn());
        return;
    }


    /**
     *  Gets the completion requirements requisite query terms. 
     *
     *  @return the requisite query terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getCompletionRequirementsTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the CompletionRequirements column name.
     *
     * @return the column name
     */

    protected String getCompletionRequirementsColumn() {
        return ("completion_requirements");
    }


    /**
     *  Sets the credential <code> Id </code> for this query. 
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> credentialId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCredentialId(org.osid.id.Id credentialId, boolean match) {
        getAssembler().addIdTerm(getCredentialIdColumn(), credentialId, match);
        return;
    }


    /**
     *  Clears the credential <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCredentialIdTerms() {
        getAssembler().clearTerms(getCredentialIdColumn());
        return;
    }


    /**
     *  Gets the credential <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCredentialIdTerms() {
        return (getAssembler().getIdTerms(getCredentialIdColumn()));
    }


    /**
     *  Gets the CredentialId column name.
     *
     * @return the column name
     */

    protected String getCredentialIdColumn() {
        return ("credential_id");
    }


    /**
     *  Tests if a <code> CredentialQuery </code> is available. 
     *
     *  @return <code> true </code> if a credential query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credential. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a credential query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuery getCredentialQuery() {
        throw new org.osid.UnimplementedException("supportsCredentialQuery() is false");
    }


    /**
     *  Matches programs that have any credentials. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          credentials, <code> false </code> to match programs with no 
     *          credentials 
     */

    @OSID @Override
    public void matchAnyCredential(boolean match) {
        getAssembler().addIdWildcardTerm(getCredentialColumn(), match);
        return;
    }


    /**
     *  Clears the credential terms. 
     */

    @OSID @Override
    public void clearCredentialTerms() {
        getAssembler().clearTerms(getCredentialColumn());
        return;
    }


    /**
     *  Gets the credential query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQueryInspector[] getCredentialTerms() {
        return (new org.osid.course.program.CredentialQueryInspector[0]);
    }


    /**
     *  Gets the Credential column name.
     *
     * @return the column name
     */

    protected String getCredentialColumn() {
        return ("credential");
    }


    /**
     *  Matches program offerings that require registration. 
     *
     *  @param  match <code> true </code> to match program offerings requiring 
     *          registration,, <code> false </code> to match program offerings 
     *          not requiring registration 
     */

    @OSID @Override
    public void matchRequiresRegistration(boolean match) {
        getAssembler().addBooleanTerm(getRequiresRegistrationColumn(), match);
        return;
    }


    /**
     *  Clears the requires registration terms. 
     */

    @OSID @Override
    public void clearRequiresRegistrationTerms() {
        getAssembler().clearTerms(getRequiresRegistrationColumn());
        return;
    }


    /**
     *  Gets the requires registration query terms. 
     *
     *  @return the requires registration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRequiresRegistrationTerms() {
        return (getAssembler().getBooleanTerms(getRequiresRegistrationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program 
     *  offerings requiring registration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiresRegistration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequiresRegistrationColumn(), style);
        return;
    }


    /**
     *  Gets the RequiresRegistration column name.
     *
     * @return the column name
     */

    protected String getRequiresRegistrationColumn() {
        return ("requires_registration");
    }


    /**
     *  Matches program offerings with minimum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMinimumSeats(long min, long max, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumSeatsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches a program offering that has any minimum seating assigned. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          minimum seats, <code> false </code> to match program offerings 
     *          with no minimum seats 
     */

    @OSID @Override
    public void matchAnyMinimumSeats(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMinimumSeatsColumn(), match);
        return;
    }


    /**
     *  Clears the minimum seats terms. 
     */

    @OSID @Override
    public void clearMinimumSeatsTerms() {
        getAssembler().clearTerms(getMinimumSeatsColumn());
        return;
    }


    /**
     *  Gets the minimum seats query terms. 
     *
     *  @return the minimum seats query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumSeatsTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumSeatsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumSeats(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumSeatsColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumSeats column name.
     *
     * @return the column name
     */

    protected String getMinimumSeatsColumn() {
        return ("minimum_seats");
    }


    /**
     *  Matches program offerings with maximum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMaximumSeats(long min, long max, boolean match) {
        getAssembler().addCardinalRangeTerm(getMaximumSeatsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches a program offering that has any maximum seating assigned. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          maximum seats, <code> false </code> to match program offerings 
     *          with no maximum seats 
     */

    @OSID @Override
    public void matchAnyMaximumSeats(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMaximumSeatsColumn(), match);
        return;
    }


    /**
     *  Clears the maximum seats terms. 
     */

    @OSID @Override
    public void clearMaximumSeatsTerms() {
        getAssembler().clearTerms(getMaximumSeatsColumn());
        return;
    }


    /**
     *  Gets the maximum seats query terms. 
     *
     *  @return the maximum seats query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumSeatsTerms() {
        return (getAssembler().getCardinalRangeTerms(getMaximumSeatsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumSeats(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMaximumSeatsColumn(), style);
        return;
    }


    /**
     *  Gets the MaximumSeats column name.
     *
     * @return the column name
     */

    protected String getMaximumSeatsColumn() {
        return ("maximum_seats");
    }


    /**
     *  Adds a class url for this query. 
     *
     *  @param  url url string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        getAssembler().addStringTerm(getURLColumn(), url, stringMatchType, match);
        return;
    }


    /**
     *  Matches a url that has any value. 
     *
     *  @param  match <code> true </code> to match program offerings with any 
     *          url, <code> false </code> to match program offerings with no 
     *          url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        getAssembler().addStringWildcardTerm(getURLColumn(), match);
        return;
    }


    /**
     *  Clears the url terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        getAssembler().clearTerms(getURLColumn());
        return;
    }


    /**
     *  Gets the url query terms. 
     *
     *  @return the url query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getURLTerms() {
        return (getAssembler().getStringTerms(getURLColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by url. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByURL(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getURLColumn(), style);
        return;
    }


    /**
     *  Gets the URL column name.
     *
     * @return the column name
     */

    protected String getURLColumn() {
        return ("u_rl");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  program offerings assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this programOffering supports the given record
     *  <code>Type</code>.
     *
     *  @param  programOfferingRecordType a program offering record type 
     *  @return <code>true</code> if the programOfferingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programOfferingRecordType) {
        for (org.osid.course.program.records.ProgramOfferingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(programOfferingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  programOfferingRecordType the program offering record type 
     *  @return the program offering query record 
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingQueryRecord getProgramOfferingQueryRecord(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramOfferingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(programOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programOfferingRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  programOfferingRecordType the program offering record type 
     *  @return the program offering query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingQueryInspectorRecord getProgramOfferingQueryInspectorRecord(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramOfferingQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(programOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programOfferingRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param programOfferingRecordType the program offering record type
     *  @return the program offering search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingSearchOrderRecord getProgramOfferingSearchOrderRecord(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramOfferingSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(programOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programOfferingRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this program offering. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programOfferingQueryRecord the program offering query record
     *  @param programOfferingQueryInspectorRecord the program offering query inspector
     *         record
     *  @param programOfferingSearchOrderRecord the program offering search order record
     *  @param programOfferingRecordType program offering record type
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingQueryRecord</code>,
     *          <code>programOfferingQueryInspectorRecord</code>,
     *          <code>programOfferingSearchOrderRecord</code> or
     *          <code>programOfferingRecordTypeprogramOffering</code> is
     *          <code>null</code>
     */
            
    protected void addProgramOfferingRecords(org.osid.course.program.records.ProgramOfferingQueryRecord programOfferingQueryRecord, 
                                      org.osid.course.program.records.ProgramOfferingQueryInspectorRecord programOfferingQueryInspectorRecord, 
                                      org.osid.course.program.records.ProgramOfferingSearchOrderRecord programOfferingSearchOrderRecord, 
                                      org.osid.type.Type programOfferingRecordType) {

        addRecordType(programOfferingRecordType);

        nullarg(programOfferingQueryRecord, "program offering query record");
        nullarg(programOfferingQueryInspectorRecord, "program offering query inspector record");
        nullarg(programOfferingSearchOrderRecord, "program offering search odrer record");

        this.queryRecords.add(programOfferingQueryRecord);
        this.queryInspectorRecords.add(programOfferingQueryInspectorRecord);
        this.searchOrderRecords.add(programOfferingSearchOrderRecord);
        
        return;
    }
}
