//
// AbstractMapEdgeLookupSession
//
//    A simple framework for providing an Edge lookup service
//    backed by a fixed collection of edges.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Edge lookup service backed by a
 *  fixed collection of edges. The edges are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Edges</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEdgeLookupSession
    extends net.okapia.osid.jamocha.topology.spi.AbstractEdgeLookupSession
    implements org.osid.topology.EdgeLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.topology.Edge> edges = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.topology.Edge>());


    /**
     *  Makes an <code>Edge</code> available in this session.
     *
     *  @param  edge an edge
     *  @throws org.osid.NullArgumentException <code>edge<code>
     *          is <code>null</code>
     */

    protected void putEdge(org.osid.topology.Edge edge) {
        this.edges.put(edge.getId(), edge);
        return;
    }


    /**
     *  Makes an array of edges available in this session.
     *
     *  @param  edges an array of edges
     *  @throws org.osid.NullArgumentException <code>edges<code>
     *          is <code>null</code>
     */

    protected void putEdges(org.osid.topology.Edge[] edges) {
        putEdges(java.util.Arrays.asList(edges));
        return;
    }


    /**
     *  Makes a collection of edges available in this session.
     *
     *  @param  edges a collection of edges
     *  @throws org.osid.NullArgumentException <code>edges<code>
     *          is <code>null</code>
     */

    protected void putEdges(java.util.Collection<? extends org.osid.topology.Edge> edges) {
        for (org.osid.topology.Edge edge : edges) {
            this.edges.put(edge.getId(), edge);
        }

        return;
    }


    /**
     *  Removes an Edge from this session.
     *
     *  @param  edgeId the <code>Id</code> of the edge
     *  @throws org.osid.NullArgumentException <code>edgeId<code> is
     *          <code>null</code>
     */

    protected void removeEdge(org.osid.id.Id edgeId) {
        this.edges.remove(edgeId);
        return;
    }


    /**
     *  Gets the <code>Edge</code> specified by its <code>Id</code>.
     *
     *  @param  edgeId <code>Id</code> of the <code>Edge</code>
     *  @return the edge
     *  @throws org.osid.NotFoundException <code>edgeId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>edgeId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Edge getEdge(org.osid.id.Id edgeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.topology.Edge edge = this.edges.get(edgeId);
        if (edge == null) {
            throw new org.osid.NotFoundException("edge not found: " + edgeId);
        }

        return (edge);
    }


    /**
     *  Gets all <code>Edges</code>. In plenary mode, the returned
     *  list contains all known edges or an error
     *  results. Otherwise, the returned list may contain only those
     *  edges that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Edges</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.edge.ArrayEdgeList(this.edges.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.edges.clear();
        super.close();
        return;
    }
}
