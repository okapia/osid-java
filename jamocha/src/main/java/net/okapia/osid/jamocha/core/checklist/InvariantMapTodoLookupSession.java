//
// InvariantMapTodoLookupSession
//
//    Implements a Todo lookup service backed by a fixed collection of
//    todos.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist;


/**
 *  Implements a Todo lookup service backed by a fixed
 *  collection of todos. The todos are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapTodoLookupSession
    extends net.okapia.osid.jamocha.core.checklist.spi.AbstractMapTodoLookupSession
    implements org.osid.checklist.TodoLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoLookupSession</code> with no
     *  todos.
     *  
     *  @param checklist the checklist
     *  @throws org.osid.NullArgumnetException {@code checklist} is
     *          {@code null}
     */

    public InvariantMapTodoLookupSession(org.osid.checklist.Checklist checklist) {
        setChecklist(checklist);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoLookupSession</code> with a single
     *  todo.
     *  
     *  @param checklist the checklist
     *  @param todo a single todo
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todo} is <code>null</code>
     */

      public InvariantMapTodoLookupSession(org.osid.checklist.Checklist checklist,
                                               org.osid.checklist.Todo todo) {
        this(checklist);
        putTodo(todo);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoLookupSession</code> using an array
     *  of todos.
     *  
     *  @param checklist the checklist
     *  @param todos an array of todos
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todos} is <code>null</code>
     */

      public InvariantMapTodoLookupSession(org.osid.checklist.Checklist checklist,
                                               org.osid.checklist.Todo[] todos) {
        this(checklist);
        putTodos(todos);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoLookupSession</code> using a
     *  collection of todos.
     *
     *  @param checklist the checklist
     *  @param todos a collection of todos
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todos} is <code>null</code>
     */

      public InvariantMapTodoLookupSession(org.osid.checklist.Checklist checklist,
                                               java.util.Collection<? extends org.osid.checklist.Todo> todos) {
        this(checklist);
        putTodos(todos);
        return;
    }
}
