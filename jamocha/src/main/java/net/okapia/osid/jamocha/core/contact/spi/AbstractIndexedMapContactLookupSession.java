//
// AbstractIndexedMapContactLookupSession.java
//
//    A simple framework for providing a Contact lookup service
//    backed by a fixed collection of contacts with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Contact lookup service backed by a
 *  fixed collection of contacts. The contacts are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some contacts may be compatible
 *  with more types than are indicated through these contact
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Contacts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapContactLookupSession
    extends AbstractMapContactLookupSession
    implements org.osid.contact.ContactLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.contact.Contact> contactsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.Contact>());
    private final MultiMap<org.osid.type.Type, org.osid.contact.Contact> contactsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.Contact>());


    /**
     *  Makes a <code>Contact</code> available in this session.
     *
     *  @param  contact a contact
     *  @throws org.osid.NullArgumentException <code>contact<code> is
     *          <code>null</code>
     */

    @Override
    protected void putContact(org.osid.contact.Contact contact) {
        super.putContact(contact);

        this.contactsByGenus.put(contact.getGenusType(), contact);
        
        try (org.osid.type.TypeList types = contact.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.contactsByRecord.put(types.getNextType(), contact);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a contact from this session.
     *
     *  @param contactId the <code>Id</code> of the contact
     *  @throws org.osid.NullArgumentException <code>contactId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeContact(org.osid.id.Id contactId) {
        org.osid.contact.Contact contact;
        try {
            contact = getContact(contactId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.contactsByGenus.remove(contact.getGenusType());

        try (org.osid.type.TypeList types = contact.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.contactsByRecord.remove(types.getNextType(), contact);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeContact(contactId);
        return;
    }


    /**
     *  Gets a <code>ContactList</code> corresponding to the given
     *  contact genus <code>Type</code> which does not include
     *  contacts of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known contacts or an error results. Otherwise,
     *  the returned list may contain only those contacts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  contactGenusType a contact genus type 
     *  @return the returned <code>Contact</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.contact.ArrayContactList(this.contactsByGenus.get(contactGenusType)));
    }


    /**
     *  Gets a <code>ContactList</code> containing the given
     *  contact record <code>Type</code>. In plenary mode, the
     *  returned list contains all known contacts or an error
     *  results. Otherwise, the returned list may contain only those
     *  contacts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  contactRecordType a contact record type 
     *  @return the returned <code>contact</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByRecordType(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.contact.ArrayContactList(this.contactsByRecord.get(contactRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.contactsByGenus.clear();
        this.contactsByRecord.clear();

        super.close();

        return;
    }
}
