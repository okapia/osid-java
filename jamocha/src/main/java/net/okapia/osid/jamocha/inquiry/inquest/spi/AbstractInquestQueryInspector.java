//
// AbstractInquestQueryInspector.java
//
//     A template for making an InquestQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquest.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for inquests.
 */

public abstract class AbstractInquestQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.inquiry.InquestQueryInspector {

    private final java.util.Collection<org.osid.inquiry.records.InquestQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the inquiry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inquiry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQueryInspector[] getInquiryTerms() {
        return (new org.osid.inquiry.InquiryQueryInspector[0]);
    }


    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuditIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Gets the response <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResponseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the response query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQueryInspector[] getResponseTerms() {
        return (new org.osid.inquiry.ResponseQueryInspector[0]);
    }


    /**
     *  Gets the ancestor inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorInquestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getAncestorInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }


    /**
     *  Gets the descendant inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantInquestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getDescendantInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given inquest query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an inquest implementing the requested record.
     *
     *  @param inquestRecordType an inquest record type
     *  @return the inquest query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquestRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquestQueryInspectorRecord getInquestQueryInspectorRecord(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquestQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(inquestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquestRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inquest query. 
     *
     *  @param inquestQueryInspectorRecord inquest query inspector
     *         record
     *  @param inquestRecordType inquest record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInquestQueryInspectorRecord(org.osid.inquiry.records.InquestQueryInspectorRecord inquestQueryInspectorRecord, 
                                                   org.osid.type.Type inquestRecordType) {

        addRecordType(inquestRecordType);
        nullarg(inquestRecordType, "inquest record type");
        this.records.add(inquestQueryInspectorRecord);        
        return;
    }
}
