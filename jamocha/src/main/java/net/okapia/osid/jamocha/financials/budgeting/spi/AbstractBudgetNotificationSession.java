//
// AbstractBudgetNotificationSession.java
//
//     A template for making BudgetNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Budget} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Budget} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for budget entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractBudgetNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.budgeting.BudgetNotificationSession {

    private boolean federated = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();


    /**
     *  Gets the {@code Business/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }

    
    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the {@code Business}.
     *
     *  @param business the business for this session
     *  @throws org.osid.NullArgumentException {@code business}
     *          is {@code null}
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can register for {@code Budget}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForBudgetNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeBudgetNotification() </code>.
     */

    @OSID @Override
    public void reliableBudgetNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableBudgetNotifications() {
        return;
    }


    /**
     *  Acknowledge a budget notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeBudgetNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for budgets in businesses
     *  which are children of this business in the business
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new budgets. {@code
     *  BudgetReceiver.newBudget()} is invoked when a new {@code
     *  Budget} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewBudgets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new budgets for the given
     *  activity.  {@code BudgetReceiver.newBudget()} is invoked when
     *  a new {@code Budget} appears in this business.
     *
     *  @param activityId the {@code Id} of the {@code Activity} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code activityId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewBudgetsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new budgets for the given fiscal
     *  period.  {@code BudgetReceiver.newBudget()} is invoked when a
     *  new {@code Budget} appears in this business.
     *
     *  @param fiscalPeriodId the {@code Id} of the {@code
     *          FiscalPeriod} to monitor
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewBudgetsForFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }

    
    /**
     *  Registers for notification of updated budgets. {@code
     *  BudgetReceiver.changedBudget()} is invoked when a budget is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedBudgets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated budgets for the given
     *  activity.  {@code BudgetReceiver.changedBudget()} is invoked
     *  when a budget in this business is changed.
     *
     *  @param activityId the {@code Id} of the {@code Activity} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code activityId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerFoChangedBudgetsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated budgets for the given
     *  fiscal period. {@code BudgetReceiver.changedBudget()} is
     *  invoked when a budget in this business is changed.
     *
     *  @param  fiscalPeriodId the {@code Id} of the {@code 
     *          FiscalPeriod} to monitor 
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerFoChangedBudgetsForFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated budget. {@code
     *  BudgetReceiver.changedBudget()} is invoked when the specified
     *  budget is changed.
     *
     *  @param budgetId the {@code Id} of the {@code Budget} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code budgetId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedBudget(org.osid.id.Id budgetId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted budgets. {@code
     *  BudgetReceiver.deletedBudget()} is invoked when a budget is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedBudgets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted budgets for the given
     *  activity.  {@code BudgetReceiver.deletedBudget()} is invoked
     *  when a budget is deleted or removed from this business.
     *
     *  @param activityId the {@code Id} of the {@code Activity} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code activityId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedBudgetsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted budgets for the given
     *  fiscal period. {@code BudgetReceiver.deletedBudget()} is
     *  invoked when a budget is deleted or removed from this
     *  business.
     *
     *  @param fiscalPeriodId the {@code Id} of the {@code
     *          FiscalPeriod} to monitor
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedBudgetsForFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted budget. {@code
     *  BudgetReceiver.deletedBudget()} is invoked when the specified
     *  budget is deleted.
     *
     *  @param budgetId the {@code Id} of the
     *          {@code Budget} to monitor
     *  @throws org.osid.NullArgumentException {@code budgetId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedBudget(org.osid.id.Id budgetId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
