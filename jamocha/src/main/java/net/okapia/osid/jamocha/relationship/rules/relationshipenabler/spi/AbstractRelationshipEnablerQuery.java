//
// AbstractRelationshipEnablerQuery.java
//
//     A template for making a RelationshipEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.relationshipenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for relationship enablers.
 */

public abstract class AbstractRelationshipEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.relationship.rules.RelationshipEnablerQuery {

    private final java.util.Collection<org.osid.relationship.rules.records.RelationshipEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the relationship. 
     *
     *  @param  relationshipId the relationship <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relationshipId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRelationshipId(org.osid.id.Id relationshipId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the relationship <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRelationshipIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RelationshipQuery </code> is available. 
     *
     *  @return <code> true </code> if a relationship query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relationship. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the relationship query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRuledRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRelationshipQuery() is false");
    }


    /**
     *  Matches enablers mapped to any relationship. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          relationship, <code> false </code> to match enablers mapped to 
     *          no relationship 
     */

    @OSID @Override
    public void matchAnyRuledRelationship(boolean match) {
        return;
    }


    /**
     *  Clears the relationship query terms. 
     */

    @OSID @Override
    public void clearRuledRelationshipTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to a family. 
     *
     *  @param  familyId the family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFamilyId(org.osid.id.Id familyId, boolean match) {
        return;
    }


    /**
     *  Clears the family <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFamilyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a family. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsFamilyQuery() is false");
    }


    /**
     *  Clears the family query terms. 
     */

    @OSID @Override
    public void clearFamilyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given relationship enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a relationship enabler implementing the requested record.
     *
     *  @param relationshipEnablerRecordType a relationship enabler record type
     *  @return the relationship enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.rules.records.RelationshipEnablerQueryRecord getRelationshipEnablerQueryRecord(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.rules.records.RelationshipEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(relationshipEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relationship enabler query. 
     *
     *  @param relationshipEnablerQueryRecord relationship enabler query record
     *  @param relationshipEnablerRecordType relationshipEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelationshipEnablerQueryRecord(org.osid.relationship.rules.records.RelationshipEnablerQueryRecord relationshipEnablerQueryRecord, 
                                          org.osid.type.Type relationshipEnablerRecordType) {

        addRecordType(relationshipEnablerRecordType);
        nullarg(relationshipEnablerQueryRecord, "relationship enabler query record");
        this.records.add(relationshipEnablerQueryRecord);        
        return;
    }
}
