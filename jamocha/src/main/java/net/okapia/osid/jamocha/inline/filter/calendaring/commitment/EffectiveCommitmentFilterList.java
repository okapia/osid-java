//
// EffectiveCommitmentFilterList.java
//
//     Implements a filter for effectiveness.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.commitment;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for effective temporals.
 */

public final class EffectiveCommitmentFilterList
    extends net.okapia.osid.jamocha.inline.filter.calendaring.commitment.spi.AbstractCommitmentFilterList
    implements org.osid.calendaring.CommitmentList,
               CommitmentFilter {


    /**
     *  Creates a new <code>EffectiveCommitmentFilterList</code>.
     *
     *  @param list a <code>CommitmentList</code>
     *  @throws org.osid.NullArgumentException <code>list</code> is
     *          <code>null</code>
     */

    public EffectiveCommitmentFilterList(org.osid.calendaring.CommitmentList list) {
        super(list);
        return;
    }    

    
    /**
     *  Filters Commitments.
     *
     *  @param commitment the commitment to filter
     *  @return <code>true</code> if the commitment passes the filter,
     *          <code>false</code> if the commitment should be filtered
     */

    @Override
    public boolean pass(org.osid.calendaring.Commitment commitment) {
        return (commitment.isEffective());
    }
}
