//
// AbstractPosition.java
//
//     Defines a Position.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.percentarg;


/**
 *  Defines a <code>Position</code>.
 */

public abstract class AbstractPosition
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.personnel.Position {

    private org.osid.personnel.Organization organization;
    private org.osid.locale.DisplayText title;
    private org.osid.grading.Grade level;

    private boolean hasQualifications = false;
    private final java.util.Collection<org.osid.learning.Objective> qualifications = new java.util.LinkedHashSet<>();

    private long targetAppointments;
    private long requiredCommitment;
    private org.osid.financials.Currency lowSalaryRange;
    private org.osid.financials.Currency midpointSalaryRange;
    private org.osid.financials.Currency highSalaryRange;
    private org.osid.calendaring.Duration compensationFrequency;

    private boolean exempt;
    private org.osid.type.Type benefitsType;

    private final java.util.Collection<org.osid.personnel.records.PositionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the organization to which this position 
     *  is assigned. 
     *
     *  @return the <code> Organization </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrganizationId() {
        return (this.organization.getId());
    }


    /**
     *  Gets the organization for this position. 
     *
     *  @return the <code> Organization </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.personnel.Organization getOrganization()
        throws org.osid.OperationFailedException {

        return (this.organization);
    }


    /**
     *  Sets the organization.
     *
     *  @param organization an organization
     *  @throws org.osid.NullArgumentException
     *          <code>organization</code> is <code>null</code>
     */

    protected void setOrganization(org.osid.personnel.Organization organization) {
        nullarg(organization, "organization");
        this.organization = organization;
        return;
    }


    /**
     *  Gets the title for this position. 
     *
     *  @return the position title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the job level. 
     *
     *  @return the <code> Grade </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        return (this.level.getId());
    }


    /**
     *  Gets the grade level for this position. 
     *
     *  @return the <code> Grade </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        return (this.level);
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    protected void setLevel(org.osid.grading.Grade level) {
        nullarg(level, "level");
        this.level = level;
        return;
    }


    /**
     *  Tests if qualifications are avilable for this position. 
     *
     *  @return <code> true </code> if qualifications are available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasQualifications() {
        return (this.hasQualifications);
    }


    /**
     *  Gets the qualifcation Ids. 
     *
     *  @return a list of objective <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasQualifications() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getQualificationIds() {
        if (!hasQualifications()) {
            throw new org.osid.IllegalStateException("hasQualifications() is false");
        }

        try {
            org.osid.learning.ObjectiveList qualifications = getQualifications();
            return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(qualifications));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the list of qualifications required for this position. 
     *
     *  @return the <code> Objectives </code> 
     *  @throws org.osid.IllegalStateException <code> hasQualifications() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getQualifications()
        throws org.osid.OperationFailedException {

        if (!hasQualifications()) {
            throw new org.osid.IllegalStateException("hasQualifications() is false");
        }

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.qualifications));
    }


    /**
     *  Adds a qualification.
     *
     *  @param qualification a qualification
     *  @throws org.osid.NullArgumentException
     *          <code>qualification</code> is <code>null</code>
     */

    protected void addQualification(org.osid.learning.Objective qualification) {
        nullarg(qualification, "qualification");

        this.qualifications.add(qualification);
        this.hasQualifications = true;

        return;
    }


    /**
     *  Sets all the qualifications.
     *
     *  @param qualifications a collection of qualifications
     *  @throws org.osid.NullArgumentException
     *          <code>qualifications</code> is <code>null</code>
     */

    protected void setQualifications(java.util.Collection<org.osid.learning.Objective> qualifications) {
        nullarg(qualifications, "qualifications");

        this.qualifications.clear();
        this.qualifications.addAll(qualifications);
        this.hasQualifications = true;

        return;
    }


    /**
     *  Gets the target number of <code> Appointments </code> desired for this 
     *  <code> Position. </code> 
     *
     *  @return the number of appointments 
     */

    @OSID @Override
    public long getTargetAppointments() {
        return (this.targetAppointments);
    }


    /**
     *  Sets the target appointments.
     *
     *  @param appointments a target number of appointments
     *  @throws org.osid.InvalidArgumentException
     *          <code>appointments</code> is negative
     */

    protected void setTargetAppointments(long appointments) {
        cardinalarg(appointments, "target appointments");
        this.targetAppointments = appointments;
        return;
    }


    /**
     *  Gets the required percentage commitment (0-100). 
     *
     *  @return the required percentage commitment 
     */

    @OSID @Override
    public long getRequiredCommitment() {
        return (this.requiredCommitment);
    }


    /**
     *  Sets the required commitment.
     *
     *  @param percentage a required commitment (0-100)
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    protected void setRequiredCommitment(long percentage) {
        percentarg(percentage, "percentage commitment");
        this.requiredCommitment = requiredCommitment;
        return;
    }


    /**
     *  Tests if a salary range is available. 
     *
     *  @return <code> true </code> if a salary range is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSalaryRange() {
        return ((this.lowSalaryRange != null) && (this.midpointSalaryRange != null) &&
                (this.highSalaryRange != null) && (this.compensationFrequency != null));
    }


    /**
     *  Gets the low end of the salary range. 
     *
     *  @return the low slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getLowSalaryRange() {
        if (!hasSalaryRange()) {
            throw new org.osid.IllegalStateException("hasSalaryRange() is false");
        }

        return (this.lowSalaryRange);
    }


    /**
     *  Sets the low salary range.
     *
     *  @param salary a low salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    protected void setLowSalaryRange(org.osid.financials.Currency salary) {
        nullarg(salary, "low salary range");
        this.lowSalaryRange = salary;
        return;
    }


    /**
     *  Gets the midpoint of the salary range. 
     *
     *  @return the midpoint slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getMidpointSalaryRange() {
        if (!hasSalaryRange()) {
            throw new org.osid.IllegalStateException("hasSalaryRange() is false");
        }

        return (this.midpointSalaryRange);
    }


    /**
     *  Sets the midpoint salary range.
     *
     *  @param salary a midpoint salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    protected void setMidpointSalaryRange(org.osid.financials.Currency salary) {
        nullarg(salary, "mid salary range");
        this.midpointSalaryRange = salary;
        return;
    }


    /**
     *  Gets the high end of the salary range. 
     *
     *  @return the high slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getHighSalaryRange() {
        if (!hasSalaryRange()) {
            throw new org.osid.IllegalStateException("hasSalaryRange() is false");
        }

        return (this.highSalaryRange);
    }


    /**
     *  Sets the high salary range.
     *
     *  @param salary a high salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    protected void setHighSalaryRange(org.osid.financials.Currency salary) {
        nullarg(salary, "high salary range");
        this.highSalaryRange = salary;
        return;
    }


    /**
     *  Gets the frequency of compensation. 
     *
     *  @return the frequency 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getCompensationFrequency() {
        if (!hasSalaryRange()) {
            throw new org.osid.IllegalStateException("hasSalaryRange() is false");
        }

        return (this.compensationFrequency);
    }


    /**
     *  Sets the compensation frequency.
     *
     *  @param frequency a compensation frequency
     *  @throws org.osid.NullArgumentException <code>frequency</code>
     *          is <code>null</code>
     */

    protected void setCompensationFrequency(org.osid.calendaring.Duration frequency) {
        nullarg(frequency, "compensation frequency");
        this.compensationFrequency = frequency;
        return;
    }


    /**
     *  Tests if this position is FLSA exempt. 
     *
     *  @return <code> true </code> if this position is exempt, <code> false 
     *          </code> is non-exempt 
     */

    @OSID @Override
    public boolean isExempt() {
        return (this.exempt);
    }


    /**
     *  Sets the exempt flag.
     *
     *  @param exempt <code> true </code> if this position is exempt,
     *         <code> false </code> is non-exempt
     */

    protected void setExempt(boolean exempt) {
        this.exempt = exempt;
        return;
    }


    /**
     *  Tests if this position is eligible for benefits. 
     *
     *  @return <code> true </code> if this position has benefits, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasBenefits() {
        return (this.benefitsType != null);
    }


    /**
     *  Gets the benefits Type assoicated with this position. 
     *
     *  @return the benefits type 
     *  @throws org.osid.IllegalStateException <code> hasBenefits() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.Type getBenefitsType() {
        return (this.benefitsType);
    }


    /**
     *  Sets the benefits type.
     *
     *  @param benefitsType a benefits type
     *  @throws org.osid.NullArgumentException
     *          <code>benefitsType</code> is <code>null</code>
     */

    protected void setBenefitsType(org.osid.type.Type benefitsType) {
        this.benefitsType = benefitsType;
        return;
    }


    /**
     *  Tests if this position supports the given record
     *  <code>Type</code>.
     *
     *  @param  positionRecordType a position record type 
     *  @return <code>true</code> if the positionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type positionRecordType) {
        for (org.osid.personnel.records.PositionRecord record : this.records) {
            if (record.implementsRecordType(positionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Position</code> record <code>Type</code>.
     *
     *  @param  positionRecordType the position record type 
     *  @return the position record 
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(positionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PositionRecord getPositionRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PositionRecord record : this.records) {
            if (record.implementsRecordType(positionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this position. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param positionRecord the position record
     *  @param positionRecordType position record type
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecord</code> or
     *          <code>positionRecordTypeposition</code> is
     *          <code>null</code>
     */
            
    protected void addPositionRecord(org.osid.personnel.records.PositionRecord positionRecord, 
                                     org.osid.type.Type positionRecordType) {

        nullarg(positionRecord, "position record");
        addRecordType(positionRecordType);
        this.records.add(positionRecord);
        
        return;
    }
}
