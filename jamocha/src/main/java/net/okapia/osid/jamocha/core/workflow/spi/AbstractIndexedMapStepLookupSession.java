//
// AbstractIndexedMapStepLookupSession.java
//
//    A simple framework for providing a Step lookup service
//    backed by a fixed collection of steps with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Step lookup service backed by a
 *  fixed collection of steps. The steps are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some steps may be compatible
 *  with more types than are indicated through these step
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Steps</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStepLookupSession
    extends AbstractMapStepLookupSession
    implements org.osid.workflow.StepLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.Step> stepsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.Step>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.Step> stepsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.Step>());


    /**
     *  Makes a <code>Step</code> available in this session.
     *
     *  @param  step a step
     *  @throws org.osid.NullArgumentException <code>step<code> is
     *          <code>null</code>
     */

    @Override
    protected void putStep(org.osid.workflow.Step step) {
        super.putStep(step);

        this.stepsByGenus.put(step.getGenusType(), step);
        
        try (org.osid.type.TypeList types = step.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepsByRecord.put(types.getNextType(), step);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a step from this session.
     *
     *  @param stepId the <code>Id</code> of the step
     *  @throws org.osid.NullArgumentException <code>stepId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeStep(org.osid.id.Id stepId) {
        org.osid.workflow.Step step;
        try {
            step = getStep(stepId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.stepsByGenus.remove(step.getGenusType());

        try (org.osid.type.TypeList types = step.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepsByRecord.remove(types.getNextType(), step);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeStep(stepId);
        return;
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  step genus <code>Type</code> which does not include
     *  steps of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known steps or an error results. Otherwise,
     *  the returned list may contain only those steps that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  stepGenusType a step genus type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.step.ArrayStepList(this.stepsByGenus.get(stepGenusType)));
    }


    /**
     *  Gets a <code>StepList</code> containing the given
     *  step record <code>Type</code>. In plenary mode, the
     *  returned list contains all known steps or an error
     *  results. Otherwise, the returned list may contain only those
     *  steps that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  stepRecordType a step record type 
     *  @return the returned <code>step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByRecordType(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.step.ArrayStepList(this.stepsByRecord.get(stepRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepsByGenus.clear();
        this.stepsByRecord.clear();

        super.close();

        return;
    }
}
