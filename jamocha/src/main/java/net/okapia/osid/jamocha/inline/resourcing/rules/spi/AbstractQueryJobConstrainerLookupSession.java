//
// AbstractQueryJobConstrainerLookupSession.java
//
//    An inline adapter that maps a JobConstrainerLookupSession to
//    a JobConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a JobConstrainerLookupSession to
 *  a JobConstrainerQuerySession.
 */

public abstract class AbstractQueryJobConstrainerLookupSession
    extends net.okapia.osid.jamocha.resourcing.rules.spi.AbstractJobConstrainerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.resourcing.rules.JobConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryJobConstrainerLookupSession.
     *
     *  @param querySession the underlying job constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryJobConstrainerLookupSession(org.osid.resourcing.rules.JobConstrainerQuerySession querySession) {
        nullarg(querySession, "job constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>JobConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJobConstrainers() {
        return (this.session.canSearchJobConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job constrainers in foundries which are
     *  children of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active job constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveJobConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive job constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusJobConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>JobConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JobConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>JobConstrainer</code> and retained for compatibility.
     *
     *  In active mode, job constrainers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainers are returned.
     *
     *  @param  jobConstrainerId <code>Id</code> of the
     *          <code>JobConstrainer</code>
     *  @return the job constrainer
     *  @throws org.osid.NotFoundException <code>jobConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>jobConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainer getJobConstrainer(org.osid.id.Id jobConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerQuery query = getQuery();
        query.matchId(jobConstrainerId, true);
        org.osid.resourcing.rules.JobConstrainerList jobConstrainers = this.session.getJobConstrainersByQuery(query);
        if (jobConstrainers.hasNext()) {
            return (jobConstrainers.getNextJobConstrainer());
        } 
        
        throw new org.osid.NotFoundException(jobConstrainerId + " not found");
    }


    /**
     *  Gets a <code>JobConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  jobConstrainers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>JobConstrainers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, job constrainers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainers are returned.
     *
     *  @param  jobConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JobConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByIds(org.osid.id.IdList jobConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = jobConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getJobConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>JobConstrainerList</code> corresponding to the
     *  given job constrainer genus <code>Type</code> which does not
     *  include job constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known job
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, job constrainers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainers are returned.
     *
     *  @param  jobConstrainerGenusType a jobConstrainer genus type 
     *  @return the returned <code>JobConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByGenusType(org.osid.type.Type jobConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerQuery query = getQuery();
        query.matchGenusType(jobConstrainerGenusType, true);
        return (this.session.getJobConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>JobConstrainerList</code> corresponding to the
     *  given job constrainer genus <code>Type</code> and include any
     *  additional job constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known job
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, job constrainers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainers are returned.
     *
     *  @param  jobConstrainerGenusType a jobConstrainer genus type 
     *  @return the returned <code>JobConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByParentGenusType(org.osid.type.Type jobConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerQuery query = getQuery();
        query.matchParentGenusType(jobConstrainerGenusType, true);
        return (this.session.getJobConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>JobConstrainerList</code> containing the given
     *  job constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known job
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, job constrainers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainers are returned.
     *
     *  @param  jobConstrainerRecordType a jobConstrainer record type 
     *  @return the returned <code>JobConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByRecordType(org.osid.type.Type jobConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerQuery query = getQuery();
        query.matchRecordType(jobConstrainerRecordType, true);
        return (this.session.getJobConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>JobConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known job
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those job constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, job constrainers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainers are returned.
     *
     *  @return a list of <code>JobConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getJobConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.rules.JobConstrainerQuery getQuery() {
        org.osid.resourcing.rules.JobConstrainerQuery query = this.session.getJobConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
