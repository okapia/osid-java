//
// AbstractCourseCatalogQueryInspector.java
//
//     A template for making a CourseCatalogQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.coursecatalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for course catalogs.
 */

public abstract class AbstractCourseCatalogQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.course.CourseCatalogQueryInspector {

    private final java.util.Collection<org.osid.course.records.CourseCatalogQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course query terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the activity unit <code> Id </code> query terms. 
     *
     *  @return the activity unit <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityUnitIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity unit query terms. 
     *
     *  @return the activity unit query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQueryInspector[] getActivityUnitTerms() {
        return (new org.osid.course.ActivityUnitQueryInspector[0]);
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term query terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Gets the ancestor course catalog <code> Id </code> query terms. 
     *
     *  @return the ancestor course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor course catalog query terms. 
     *
     *  @return the ancestor course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getAncestorCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the descendant course catalog <code> Id </code> query terms. 
     *
     *  @return the descendant course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant course catalog query terms. 
     *
     *  @return the descendant course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getDescendantCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given course catalog query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a course catalog implementing the requested record.
     *
     *  @param courseCatalogRecordType a course catalog record type
     *  @return the course catalog query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseCatalogRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseCatalogQueryInspectorRecord getCourseCatalogQueryInspectorRecord(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseCatalogQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(courseCatalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseCatalogRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course catalog query. 
     *
     *  @param courseCatalogQueryInspectorRecord course catalog query inspector
     *         record
     *  @param courseCatalogRecordType courseCatalog record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseCatalogQueryInspectorRecord(org.osid.course.records.CourseCatalogQueryInspectorRecord courseCatalogQueryInspectorRecord, 
                                                   org.osid.type.Type courseCatalogRecordType) {

        addRecordType(courseCatalogRecordType);
        nullarg(courseCatalogRecordType, "course catalog record type");
        this.records.add(courseCatalogQueryInspectorRecord);        
        return;
    }
}
