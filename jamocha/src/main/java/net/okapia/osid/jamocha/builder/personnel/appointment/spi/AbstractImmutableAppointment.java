//
// AbstractImmutableAppointment.java
//
//     Wraps a mutable Appointment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.appointment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Appointment</code> to hide modifiers. This
 *  wrapper provides an immutized Appointment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying appointment whose state changes are visible.
 */

public abstract class AbstractImmutableAppointment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.personnel.Appointment {

    private final org.osid.personnel.Appointment appointment;


    /**
     *  Constructs a new <code>AbstractImmutableAppointment</code>.
     *
     *  @param appointment the appointment to immutablize
     *  @throws org.osid.NullArgumentException <code>appointment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAppointment(org.osid.personnel.Appointment appointment) {
        super(appointment);
        this.appointment = appointment;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the person. 
     *
     *  @return the person <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPersonId() {
        return (this.appointment.getPersonId());
    }


    /**
     *  Gets the person in this appointment. 
     *
     *  @return the <code> Person </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.personnel.Person getPerson()
        throws org.osid.OperationFailedException {

        return (this.appointment.getPerson());
    }


    /**
     *  Gets the <code> Id </code> of the position in this appointment. 
     *
     *  @return the <code> Position </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPositionId() {
        return (this.appointment.getPositionId());
    }


    /**
     *  Gets the position in this appointment. 
     *
     *  @return the <code> Position </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.personnel.Position getPosition()
        throws org.osid.OperationFailedException {

        return (this.appointment.getPosition());
    }


    /**
     *  Gets the percentage commitment (0-100). 
     *
     *  @return the percentage commitment 
     */

    @OSID @Override
    public long getCommitment() {
        return (this.appointment.getCommitment());
    }


    /**
     *  Gets the title for this appointment. 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.appointment.getTitle());
    }


    /**
     *  Tests if a salary is available. 
     *
     *  @return <code> true </code> if a salary is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSalary() {
        return (this.appointment.hasSalary());
    }


    /**
     *  Gets the salary. 
     *
     *  @return the slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalary() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getSalary() {
        return (this.appointment.getSalary());
    }


    /**
     *  Gets the basis in number of months per year. 
     *
     *  @return the number of months 
     *  @throws org.osid.IllegalStateException <code> hasSalary() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getSalaryBasis() {
        return (this.appointment.getSalaryBasis());
    }


    /**
     *  Gets the record corresponding to the given <code> Appointment </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> appointmentRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(appointmentRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  appointmentRecordType the type of appointment record to 
     *          retrieve 
     *  @return the appointment record 
     *  @throws org.osid.NullArgumentException <code> appointmentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(appointmentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentRecord getAppointmentRecord(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException {

        return (this.appointment.getAppointmentRecord(appointmentRecordType));
    }
}

