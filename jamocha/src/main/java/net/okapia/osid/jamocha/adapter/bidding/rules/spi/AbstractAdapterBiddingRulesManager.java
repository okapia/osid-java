//
// AbstractBiddingRulesManager.java
//
//     An adapter for a BiddingRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a BiddingRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterBiddingRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.bidding.rules.BiddingRulesManager>
    implements org.osid.bidding.rules.BiddingRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterBiddingRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterBiddingRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterBiddingRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterBiddingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up auction constrainer is supported. 
     *
     *  @return <code> true </code> if auction constrainer lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerLookup() {
        return (getAdapteeManager().supportsAuctionConstrainerLookup());
    }


    /**
     *  Tests if querying auction constrainer is supported. 
     *
     *  @return <code> true </code> if auction constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerQuery() {
        return (getAdapteeManager().supportsAuctionConstrainerQuery());
    }


    /**
     *  Tests if searching auction constrainer is supported. 
     *
     *  @return <code> true </code> if auction constrainer search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerSearch() {
        return (getAdapteeManager().supportsAuctionConstrainerSearch());
    }


    /**
     *  Tests if an auction constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if auction constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerAdmin() {
        return (getAdapteeManager().supportsAuctionConstrainerAdmin());
    }


    /**
     *  Tests if an auction constrainer notification service is supported. 
     *
     *  @return <code> true </code> if auction constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerNotification() {
        return (getAdapteeManager().supportsAuctionConstrainerNotification());
    }


    /**
     *  Tests if an auction constrainer auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction constrainer auction house 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerAuctionHouse() {
        return (getAdapteeManager().supportsAuctionConstrainerAuctionHouse());
    }


    /**
     *  Tests if an auction constrainer auction house service is supported. 
     *
     *  @return <code> true </code> if auction constrainer auction house 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerAuctionHouseAssignment() {
        return (getAdapteeManager().supportsAuctionConstrainerAuctionHouseAssignment());
    }


    /**
     *  Tests if an auction constrainer auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction constrainer auction house 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerSmartAuctionHouse() {
        return (getAdapteeManager().supportsAuctionConstrainerSmartAuctionHouse());
    }


    /**
     *  Tests if an auction constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if an auction constrainer rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerRuleLookup() {
        return (getAdapteeManager().supportsAuctionConstrainerRuleLookup());
    }


    /**
     *  Tests if an auction constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if an auction constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerRuleApplication() {
        return (getAdapteeManager().supportsAuctionConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up auction constrainer enablers is supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying auction constrainer enablers is supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching auction constrainer enablers is supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerSearch());
    }


    /**
     *  Tests if an auction constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerAdmin());
    }


    /**
     *  Tests if an auction constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerNotification());
    }


    /**
     *  Tests if an auction constrainer enabler auction house lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if an auction constrainer enabler auction 
     *          house lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerAuctionHouse() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerAuctionHouse());
    }


    /**
     *  Tests if an auction constrainer enabler auction house service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler auction 
     *          house assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerAuctionHouseAssignment() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerAuctionHouseAssignment());
    }


    /**
     *  Tests if an auction constrainer enabler auction house lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if an auction constrainer enabler auction 
     *          house service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerSmartAuctionHouse() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerSmartAuctionHouse());
    }


    /**
     *  Tests if an auction constrainer enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction constrainer enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if an auction constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up auction processor is supported. 
     *
     *  @return <code> true </code> if auction processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorLookup() {
        return (getAdapteeManager().supportsAuctionProcessorLookup());
    }


    /**
     *  Tests if querying auction processor is supported. 
     *
     *  @return <code> true </code> if auction processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorQuery() {
        return (getAdapteeManager().supportsAuctionProcessorQuery());
    }


    /**
     *  Tests if searching auction processor is supported. 
     *
     *  @return <code> true </code> if auction processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorSearch() {
        return (getAdapteeManager().supportsAuctionProcessorSearch());
    }


    /**
     *  Tests if an auction processor administrative service is supported. 
     *
     *  @return <code> true </code> if auction processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorAdmin() {
        return (getAdapteeManager().supportsAuctionProcessorAdmin());
    }


    /**
     *  Tests if an auction processor notification service is supported. 
     *
     *  @return <code> true </code> if auction processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorNotification() {
        return (getAdapteeManager().supportsAuctionProcessorNotification());
    }


    /**
     *  Tests if an auction processor auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor auction house 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorAuctionHouse() {
        return (getAdapteeManager().supportsAuctionProcessorAuctionHouse());
    }


    /**
     *  Tests if an auction processor auction house service is supported. 
     *
     *  @return <code> true </code> if auction processor auction house 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorAuctionHouseAssignment() {
        return (getAdapteeManager().supportsAuctionProcessorAuctionHouseAssignment());
    }


    /**
     *  Tests if an auction processor auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor auction house 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorSmartAuctionHouse() {
        return (getAdapteeManager().supportsAuctionProcessorSmartAuctionHouse());
    }


    /**
     *  Tests if an auction processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if an auction processor rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorRuleLookup() {
        return (getAdapteeManager().supportsAuctionProcessorRuleLookup());
    }


    /**
     *  Tests if an auction processor rule application service is supported. 
     *
     *  @return <code> true </code> if auction processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorRuleApplication() {
        return (getAdapteeManager().supportsAuctionProcessorRuleApplication());
    }


    /**
     *  Tests if looking up auction processor enablers is supported. 
     *
     *  @return <code> true </code> if auction processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerLookup() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerLookup());
    }


    /**
     *  Tests if querying auction processor enablers is supported. 
     *
     *  @return <code> true </code> if auction processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerQuery() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerQuery());
    }


    /**
     *  Tests if searching auction processor enablers is supported. 
     *
     *  @return <code> true </code> if auction processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerSearch() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerSearch());
    }


    /**
     *  Tests if an auction processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerAdmin());
    }


    /**
     *  Tests if an auction processor enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerNotification() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerNotification());
    }


    /**
     *  Tests if an auction processor enabler auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor enabler auction 
     *          house lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerAuctionHouse() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerAuctionHouse());
    }


    /**
     *  Tests if an auction processor enabler auction house service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler auction house 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerAuctionHouseAssignment() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerAuctionHouseAssignment());
    }


    /**
     *  Tests if an auction processor enabler auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor enabler auction 
     *          house service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerSmartAuctionHouse() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerSmartAuctionHouse());
    }


    /**
     *  Tests if an auction processor enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if an auction processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsAuctionProcessorEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> AuctionConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> AuctionConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerRecordTypes() {
        return (getAdapteeManager().getAuctionConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  auctionConstrainerRecordType a <code> Type </code> indicating 
     *          an <code> AuctionConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerRecordType(org.osid.type.Type auctionConstrainerRecordType) {
        return (getAdapteeManager().supportsAuctionConstrainerRecordType(auctionConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> AuctionConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AuctionConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getAuctionConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionConstrainer </code> search record 
     *  type is supported. 
     *
     *  @param  auctionConstrainerSearchRecordType a <code> Type </code> 
     *          indicating an <code> AuctionConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerSearchRecordType(org.osid.type.Type auctionConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsAuctionConstrainerSearchRecordType(auctionConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> AuctionConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          AuctionConstrainerEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getAuctionConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionConstrainerEnabler </code> record 
     *  type is supported. 
     *
     *  @param  auctionConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating an <code> AuctionConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerRecordType(auctionConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> AuctionConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          AuctionConstrainerEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getAuctionConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  auctionConstrainerEnablerSearchRecordType a <code> Type 
     *          </code> indicating an <code> AuctionConstrainerEnabler </code> 
     *          search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerSearchRecordType(org.osid.type.Type auctionConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsAuctionConstrainerEnablerSearchRecordType(auctionConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> AuctionProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> AuctionProcessor 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorRecordTypes() {
        return (getAdapteeManager().getAuctionProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionProcessor </code> record type is 
     *  supported. 
     *
     *  @param  auctionProcessorRecordType a <code> Type </code> indicating an 
     *          <code> AuctionProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorRecordType(org.osid.type.Type auctionProcessorRecordType) {
        return (getAdapteeManager().supportsAuctionProcessorRecordType(auctionProcessorRecordType));
    }


    /**
     *  Gets the supported <code> AuctionProcessor </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AuctionProcessor 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorSearchRecordTypes() {
        return (getAdapteeManager().getAuctionProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionProcessor </code> search record type 
     *  is supported. 
     *
     *  @param  auctionProcessorSearchRecordType a <code> Type </code> 
     *          indicating an <code> AuctionProcessor </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorSearchRecordType(org.osid.type.Type auctionProcessorSearchRecordType) {
        return (getAdapteeManager().supportsAuctionProcessorSearchRecordType(auctionProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> AuctionProcessorEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> AuctionProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getAuctionProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionProcessorEnabler </code> record type 
     *  is supported. 
     *
     *  @param  auctionProcessorEnablerRecordType a <code> Type </code> 
     *          indicating an <code> AuctionProcessorEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerRecordType(org.osid.type.Type auctionProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsAuctionProcessorEnablerRecordType(auctionProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> AuctionProcessorEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> AuctionProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getAuctionProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionProcessorEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  auctionProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> AuctionProcessorEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerSearchRecordType(org.osid.type.Type auctionProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsAuctionProcessorEnablerSearchRecordType(auctionProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer lookup service. 
     *
     *  @return an <code> AuctionConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerLookupSession getAuctionConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerLookupSession getAuctionConstrainerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer query service. 
     *
     *  @return an <code> AuctionConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuerySession getAuctionConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuerySession getAuctionConstrainerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerQuerySessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer search service. 
     *
     *  @return an <code> AuctionConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSearchSession getAuctionConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSearchSession getAuctionConstrainerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerSearchSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer administration service. 
     *
     *  @return an <code> AuctionConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAdminSession getAuctionConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAdminSession getAuctionConstrainerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerAdminSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer notification service. 
     *
     *  @param  auctionConstrainerReceiver the notification callback 
     *  @return an <code> AuctionConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerNotificationSession getAuctionConstrainerNotificationSession(org.osid.bidding.rules.AuctionConstrainerReceiver auctionConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerNotificationSession(auctionConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer notification service for the given auction house. 
     *
     *  @param  auctionConstrainerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerReceiver </code> or <code> auctionHouseId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerNotificationSession getAuctionConstrainerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionConstrainerReceiver auctionConstrainerReceiver, 
                                                                                                                                org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerNotificationSessionForAuctionHouse(auctionConstrainerReceiver, auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction 
     *  constrainer/auction house mappings for auction constrainers. 
     *
     *  @return an <code> AuctionConstrainerAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAuctionHouse() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAuctionHouseSession getAuctionConstrainerAuctionHouseSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerAuctionHouseSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  constrainer to auction houses. 
     *
     *  @return an <code> AuctionConstrainerAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAuctionHouseAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAuctionHouseAssignmentSession getAuctionConstrainerAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerAuctionHouseAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction constrainer 
     *  smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSmartAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSmartAuctionHouseSession getAuctionConstrainerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerSmartAuctionHouseSession(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer auction mapping lookup service for looking up the rules 
     *  applied to the auction house. 
     *
     *  @return an <code> AuctionConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleLookupSession getAuctionConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer mapping lookup service for the given auction house for 
     *  looking up rules applied to an auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleLookupSession getAuctionConstrainerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerRuleLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer assignment service to apply to auction houses. 
     *
     *  @return an <code> AuctionConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleApplicationSession getAuctionConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer assignment service for the given auction house to apply to 
     *  auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleApplicationSession getAuctionConstrainerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerRuleApplicationSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler lookup service. 
     *
     *  @return an <code> AuctionConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession getAuctionConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession getAuctionConstrainerEnablerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler query service. 
     *
     *  @return an <code> AuctionConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession getAuctionConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession getAuctionConstrainerEnablerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerQuerySessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler search service. 
     *
     *  @return an <code> AuctionConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSearchSession getAuctionConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enablers earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSearchSession getAuctionConstrainerEnablerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerSearchSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler administration service. 
     *
     *  @return an <code> AuctionConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAdminSession getAuctionConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler administration service for the given auction 
     *  house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAdminSession getAuctionConstrainerEnablerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerAdminSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler notification service. 
     *
     *  @param  auctionConstrainerEnablerReceiver the notification callback 
     *  @return an <code> AuctionConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerReceiver </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerNotificationSession getAuctionConstrainerEnablerNotificationSession(org.osid.bidding.rules.AuctionConstrainerEnablerReceiver auctionConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerNotificationSession(auctionConstrainerEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler notification service for the given auction house. 
     *
     *  @param  auctionConstrainerEnablerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerReceiver </code> or <code> 
     *          auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerNotificationSession getAuctionConstrainerEnablerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionConstrainerEnablerReceiver auctionConstrainerEnablerReceiver, 
                                                                                                                                              org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerNotificationSessionForAuctionHouse(auctionConstrainerEnablerReceiver, auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction constrainer 
     *  enabler/auction house mappings for auction constrainer enablers. 
     *
     *  @return an <code> AuctionConstrainerEnablerAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAuctionHouseSession getAuctionConstrainerEnablerAuctionHouseSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerAuctionHouseSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  constrainer enablers to auction houses. 
     *
     *  @return an <code> 
     *          AuctionConstrainerEnablerAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAuctionHouseAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAuctionHouseAssignmentSession getAuctionConstrainerEnablerAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerAuctionHouseAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction constrainer 
     *  enabler smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerSmartAuctionHouseSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSmartAuctionHouse() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSmartAuctionHouseSession getAuctionConstrainerEnablerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerSmartAuctionHouseSession(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return an <code> AuctionConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleLookupSession getAuctionConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler mapping lookup service for the given auction 
     *  house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleLookupSession getAuctionConstrainerEnablerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerRuleLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler assignment service. 
     *
     *  @return an <code> AuctionConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleApplicationSession getAuctionConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleApplicationSession getAuctionConstrainerEnablerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionConstrainerEnablerRuleApplicationSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor lookup service. 
     *
     *  @return an <code> AuctionProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorLookupSession getAuctionProcessorLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorLookupSession getAuctionProcessorLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor query service. 
     *
     *  @return an <code> AuctionProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuerySession getAuctionProcessorQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuerySession getAuctionProcessorQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorQuerySessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor search service. 
     *
     *  @return an <code> AuctionProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSearchSession getAuctionProcessorSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSearchSession getAuctionProcessorSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorSearchSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor administration service. 
     *
     *  @return an <code> AuctionProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAdminSession getAuctionProcessorAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAdminSession getAuctionProcessorAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorAdminSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor notification service. 
     *
     *  @param  auctionProcessorReceiver the notification callback 
     *  @return an <code> AuctionProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorNotificationSession getAuctionProcessorNotificationSession(org.osid.bidding.rules.AuctionProcessorReceiver auctionProcessorReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorNotificationSession(auctionProcessorReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor notification service for the given auction house. 
     *
     *  @param  auctionProcessorReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionProcessorReceiver 
     *          </code> or <code> auctionHouseId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorNotificationSession getAuctionProcessorNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionProcessorReceiver auctionProcessorReceiver, 
                                                                                                                            org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorNotificationSessionForAuctionHouse(auctionProcessorReceiver, auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction 
     *  processor/auction house mappings for auction processors. 
     *
     *  @return an <code> AuctionProcessorAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAuctionHouse() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAuctionHouseSession getAuctionProcessorAuctionHouseSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorAuctionHouseSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  processor to auction houses. 
     *
     *  @return an <code> AuctionProcessorAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAuctionHouseAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAuctionHouseAssignmentSession getAuctionProcessorAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorAuctionHouseAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction processor smart 
     *  auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSmartAuctionHouse() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSmartAuctionHouseSession getAuctionProcessorSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorSmartAuctionHouseSession(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor auction mapping lookup service for looking up the rules 
     *  applied to the auction house. 
     *
     *  @return an <code> AuctionProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleLookupSession getAuctionProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor mapping lookup service for the given auction house for 
     *  looking up rules applied to an auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleLookupSession getAuctionProcessorRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorRuleLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor assignment service. 
     *
     *  @return an <code> AuctionProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleApplicationSession getAuctionProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleApplicationSession getAuctionProcessorRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorRuleApplicationSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler lookup service. 
     *
     *  @return an <code> AuctionProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerLookupSession getAuctionProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerLookupSession getAuctionProcessorEnablerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler query service. 
     *
     *  @return an <code> AuctionProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerQuerySession getAuctionProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerQuerySession getAuctionProcessorEnablerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerQuerySessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler search service. 
     *
     *  @return an <code> AuctionProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSearchSession getAuctionProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enablers earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSearchSession getAuctionProcessorEnablerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerSearchSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler administration service. 
     *
     *  @return an <code> AuctionProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAdminSession getAuctionProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAdminSession getAuctionProcessorEnablerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerAdminSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler notification service. 
     *
     *  @param  auctionProcessorEnablerReceiver the notification callback 
     *  @return an <code> AuctionProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerNotificationSession getAuctionProcessorEnablerNotificationSession(org.osid.bidding.rules.AuctionProcessorEnablerReceiver auctionProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerNotificationSession(auctionProcessorEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler notification service for the given auction house. 
     *
     *  @param  auctionProcessorEnablerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerReceiver </code> or <code> 
     *          auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerNotificationSession getAuctionProcessorEnablerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionProcessorEnablerReceiver auctionProcessorEnablerReceiver, 
                                                                                                                                          org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerNotificationSessionForAuctionHouse(auctionProcessorEnablerReceiver, auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction processor 
     *  enabler/auction house mappings for auction processor enablers. 
     *
     *  @return an <code> AuctionProcessorEnablerAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAuctionHouseSession getAuctionProcessorEnablerAuctionHouseSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerAuctionHouseSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  processor enablers to auction houses. 
     *
     *  @return an <code> AuctionProcessorEnablerAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAuctionHouseAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAuctionHouseAssignmentSession getAuctionProcessorEnablerAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerAuctionHouseAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction processor 
     *  enabler smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerSmartAuctionHouseSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSmartAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSmartAuctionHouseSession getAuctionProcessorEnablerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerSmartAuctionHouseSession(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler mapping lookup service. 
     *
     *  @return an <code> AuctionProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleLookupSession getAuctionProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler mapping lookup service. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleLookupSession getAuctionProcessorEnablerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerRuleLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler assignment service. 
     *
     *  @return an <code> AuctionProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleApplicationSession getAuctionProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleApplicationSession getAuctionProcessorEnablerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionProcessorEnablerRuleApplicationSessionForAuctionHouse(auctionHouseId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
