//
// AbstractIndexedMapBankLookupSession.java
//
//    A simple framework for providing a Bank lookup service
//    backed by a fixed collection of banks with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Bank lookup service backed by a
 *  fixed collection of banks. The banks are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some banks may be compatible
 *  with more types than are indicated through these bank
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Banks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBankLookupSession
    extends AbstractMapBankLookupSession
    implements org.osid.assessment.BankLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.Bank> banksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.Bank>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.Bank> banksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.Bank>());


    /**
     *  Makes a <code>Bank</code> available in this session.
     *
     *  @param  bank a bank
     *  @throws org.osid.NullArgumentException <code>bank<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBank(org.osid.assessment.Bank bank) {
        super.putBank(bank);

        this.banksByGenus.put(bank.getGenusType(), bank);
        
        try (org.osid.type.TypeList types = bank.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.banksByRecord.put(types.getNextType(), bank);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a bank from this session.
     *
     *  @param bankId the <code>Id</code> of the bank
     *  @throws org.osid.NullArgumentException <code>bankId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBank(org.osid.id.Id bankId) {
        org.osid.assessment.Bank bank;
        try {
            bank = getBank(bankId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.banksByGenus.remove(bank.getGenusType());

        try (org.osid.type.TypeList types = bank.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.banksByRecord.remove(types.getNextType(), bank);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBank(bankId);
        return;
    }


    /**
     *  Gets a <code>BankList</code> corresponding to the given
     *  bank genus <code>Type</code> which does not include
     *  banks of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known banks or an error results. Otherwise,
     *  the returned list may contain only those banks that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  bankGenusType a bank genus type 
     *  @return the returned <code>Bank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bankGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanksByGenusType(org.osid.type.Type bankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.bank.ArrayBankList(this.banksByGenus.get(bankGenusType)));
    }


    /**
     *  Gets a <code>BankList</code> containing the given
     *  bank record <code>Type</code>. In plenary mode, the
     *  returned list contains all known banks or an error
     *  results. Otherwise, the returned list may contain only those
     *  banks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  bankRecordType a bank record type 
     *  @return the returned <code>bank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanksByRecordType(org.osid.type.Type bankRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.bank.ArrayBankList(this.banksByRecord.get(bankRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.banksByGenus.clear();
        this.banksByRecord.clear();

        super.close();

        return;
    }
}
