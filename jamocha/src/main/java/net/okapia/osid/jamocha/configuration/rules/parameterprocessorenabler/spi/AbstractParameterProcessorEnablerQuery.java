//
// AbstractParameterProcessorEnablerQuery.java
//
//     A template for making a ParameterProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for parameter processor enablers.
 */

public abstract class AbstractParameterProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.configuration.rules.ParameterProcessorEnablerQuery {

    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the parameter processor. 
     *
     *  @param  parameterProcessorId the parameter processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledParameterProcessorId(org.osid.id.Id parameterProcessorId, 
                                               boolean match) {
        return;
    }


    /**
     *  Clears the parameter processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledParameterProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ParameterProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter processor query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledParameterProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parameter processor. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the parameter processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledParameterProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuery getRuledParameterProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledParameterProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any parameter processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any parameter 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          parameter processors 
     */

    @OSID @Override
    public void matchAnyRuledParameterProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the parameter processor query terms. 
     */

    @OSID @Override
    public void clearRuledParameterProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the configuration. 
     *
     *  @param  configurationId the configuration <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration query terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given parameter processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a parameter processor enabler implementing the requested record.
     *
     *  @param parameterProcessorEnablerRecordType a parameter processor enabler record type
     *  @return the parameter processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord getParameterProcessorEnablerQueryRecord(org.osid.type.Type parameterProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter processor enabler query. 
     *
     *  @param parameterProcessorEnablerQueryRecord parameter processor enabler query record
     *  @param parameterProcessorEnablerRecordType parameterProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterProcessorEnablerQueryRecord(org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord parameterProcessorEnablerQueryRecord, 
                                          org.osid.type.Type parameterProcessorEnablerRecordType) {

        addRecordType(parameterProcessorEnablerRecordType);
        nullarg(parameterProcessorEnablerQueryRecord, "parameter processor enabler query record");
        this.records.add(parameterProcessorEnablerQueryRecord);        
        return;
    }
}
