//
// AbstractSettingSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.setting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSettingSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.SettingSearchResults {

    private org.osid.control.SettingList settings;
    private final org.osid.control.SettingQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.SettingSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSettingSearchResults.
     *
     *  @param settings the result set
     *  @param settingQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>settings</code>
     *          or <code>settingQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSettingSearchResults(org.osid.control.SettingList settings,
                                            org.osid.control.SettingQueryInspector settingQueryInspector) {
        nullarg(settings, "settings");
        nullarg(settingQueryInspector, "setting query inspectpr");

        this.settings = settings;
        this.inspector = settingQueryInspector;

        return;
    }


    /**
     *  Gets the setting list resulting from a search.
     *
     *  @return a setting list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings() {
        if (this.settings == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.SettingList settings = this.settings;
        this.settings = null;
	return (settings);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.SettingQueryInspector getSettingQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  setting search record <code> Type. </code> This method must
     *  be used to retrieve a setting implementing the requested
     *  record.
     *
     *  @param settingSearchRecordType a setting search 
     *         record type 
     *  @return the setting search
     *  @throws org.osid.NullArgumentException
     *          <code>settingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(settingSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingSearchResultsRecord getSettingSearchResultsRecord(org.osid.type.Type settingSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.SettingSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(settingSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(settingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record setting search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSettingRecord(org.osid.control.records.SettingSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "setting record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
