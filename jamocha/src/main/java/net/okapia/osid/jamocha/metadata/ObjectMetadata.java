//
// ObjectMetadata.java
//
//     Defines an object Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 january 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines an object Metadata.
 */

public final class ObjectMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractObjectMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code ObjectMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public ObjectMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code ObjectMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public ObjectMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code ObjectMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public ObjectMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code false} if
     *         can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }

    
    /**
     *  Add support for an object type.
     *
     *  @param objectType the type of object
     *  @throws org.osid.NullArgumentException {@code
     *          objectType} is {@code null}
     */

    public void addObjectType(org.osid.type.Type objectType) {
        super.addObjectType(objectType);
        return;
    }

    
    /**
     *  Sets the object set.
     *
     *  @param values a collection of accepted object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setObjectSet(java.util.Collection<java.lang.Object> values) {
        super.setObjectSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the object set.
     *
     *  @param values a collection of accepted object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToObjectSet(java.util.Collection<java.lang.Object> values) {
        super.addToObjectSet(values);
        return;
    }


    /**
     *  Adds a value to the object set.
     *
     *  @param value an object value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void addToObjectSet(java.lang.Object value) {
        super.addToObjectSet(value);
        return;
    }


    /**
     *  Removes a value from the object set.
     *
     *  @param value an object value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void removeFromObjectSet(java.lang.Object value) {
        super.removeFromObjectSet(value);
        return;
    }


    /**
     *  Clears the object set.
     */

    public void clearObjectSet() {
        super.clearObjectSet();
        return;
    }


    /**
     *  Sets the default object set.
     *
     *  @param values a collection of default object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultObjectValues(java.util.Collection<java.lang.Object> values) {
        super.setDefaultObjectValues(values);
        return;
    }


    /**
     *  Adds a collection of default object values.
     *
     *  @param values a collection of default object values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultObjectValues(java.util.Collection<java.lang.Object> values) {
        super.addDefaultObjectValues(values);
        return;
    }


    /**
     *  Adds a default object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultObjectValue(java.lang.Object value) {
        super.addDefaultObjectValue(value);
        return;
    }


    /**
     *  Removes a default object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultObjectValue(java.lang.Object value) {
        super.removeDefaultObjectValue(value);
        return;
    }


    /**
     *  Clears the default object values.
     */

    public void clearDefaultObjectValues() {
        super.clearDefaultObjectValues();
        return;
    }


    /**
     *  Sets the existing object set.
     *
     *  @param values a collection of existing object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingObjectValues(java.util.Collection<java.lang.Object> values) {
        super.setExistingObjectValues(values);
        return;
    }


    /**
     *  Adds a collection of existing object values.
     *
     *  @param values a collection of existing object values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingObjectValues(java.util.Collection<java.lang.Object> values) {
        super.addExistingObjectValues(values);
        return;
    }


    /**
     *  Adds a existing object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingObjectValue(java.lang.Object value) {
        super.addExistingObjectValue(value);
        return;
    }


    /**
     *  Removes a existing object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingObjectValue(java.lang.Object value) {
        super.removeExistingObjectValue(value);
        return;
    }


    /**
     *  Clears the existing object values.
     */

    public void clearExistingObjectValues() {
        super.clearExistingObjectValues();
        return;
    }            
}
