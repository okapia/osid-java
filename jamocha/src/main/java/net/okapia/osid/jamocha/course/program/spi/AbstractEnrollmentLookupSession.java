//
// AbstractEnrollmentLookupSession.java
//
//    A starter implementation framework for providing an Enrollment
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Enrollment
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEnrollments(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEnrollmentLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.program.EnrollmentLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Enrollment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEnrollments() {
        return (true);
    }


    /**
     *  A complete view of the <code>Enrollment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEnrollmentView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Enrollment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEnrollmentView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include enrollments in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only enrollments whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveEnrollmentView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All enrollments of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveEnrollmentView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Enrollment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Enrollment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Enrollment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentId <code>Id</code> of the
     *          <code>Enrollment</code>
     *  @return the enrollment
     *  @throws org.osid.NotFoundException <code>enrollmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>enrollmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Enrollment getEnrollment(org.osid.id.Id enrollmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.program.EnrollmentList enrollments = getEnrollments()) {
            while (enrollments.hasNext()) {
                org.osid.course.program.Enrollment enrollment = enrollments.getNextEnrollment();
                if (enrollment.getId().equals(enrollmentId)) {
                    return (enrollment);
                }
            }
        } 

        throw new org.osid.NotFoundException(enrollmentId + " not found");
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  enrollments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Enrollments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEnrollments()</code>.
     *
     *  @param  enrollmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByIds(org.osid.id.IdList enrollmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.program.Enrollment> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = enrollmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getEnrollment(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("enrollment " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.program.enrollment.LinkedEnrollmentList(ret));
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  enrollment genus <code>Type</code> which does not include
     *  enrollments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEnrollments()</code>.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentGenusFilterList(getEnrollments(), enrollmentGenusType));
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  enrollment genus <code>Type</code> and include any additional
     *  enrollments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEnrollments()</code>.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByParentGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEnrollmentsByGenusType(enrollmentGenusType));
    }


    /**
     *  Gets an <code>EnrollmentList</code> containing the given
     *  enrollment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEnrollments()</code>.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByRecordType(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentRecordFilterList(getEnrollments(), enrollmentRecordType));
    }


    /**
     *  Gets an <code>EnrollmentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *  
     *  In active mode, enrollments are returned that are currently
     *  active. In any status mode, active and inactive enrollments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Enrollment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.TemporalEnrollmentFilterList(getEnrollments(), from, to));
    }
        

    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOffering(org.osid.id.Id programOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentFilterList(new ProgramOfferingFilter(programOfferingId), getEnrollments()));
    }


    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param programOfferingId the <code>Id</code> of the program
     *         offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingOnDate(org.osid.id.Id programOfferingId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.TemporalEnrollmentFilterList(getEnrollmentsForProgramOffering(programOfferingId), from, to));
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.program.EnrollmentList getEnrollmentsForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentFilterList(new StudentFilter(resourceId), getEnrollments()));
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForStudentOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.TemporalEnrollmentFilterList(getEnrollmentsForStudent(resourceId), from, to));
    }


    /**
     *  Gets a list of enrollments corresponding to program offering
     *  and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param programOfferingId the <code>Id</code> of the program
     *         offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudent(org.osid.id.Id programOfferingId,
                                                                                           org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentFilterList(new StudentFilter(resourceId), getEnrollmentsForProgramOffering(programOfferingId)));
    }


    /**
     *  Gets a list of enrollments corresponding to program offering
     *  and student <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudentOnDate(org.osid.id.Id programOfferingId,
                                                                                                 org.osid.id.Id resourceId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.TemporalEnrollmentFilterList(getEnrollmentsForProgramOfferingAndStudent(programOfferingId, resourceId), from, to));
    }


    /**
     *  Gets all <code> Enrollments </code> related to a program. 
     *  
     *  In plenary mode, the returned list contains all known enrollments or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  enrollments that are accessible through this session. 
     *  
     *  In effective mode, enrollments are returned that are currently 
     *  effective. In any effective mode, effective enrollments and those 
     *  currently expired are returned. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @return a list of <code> Enrollments </code> 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        java.util.Collection<org.osid.course.program.Enrollment> ret = new java.util.ArrayList<>();

        try (org.osid.course.program.EnrollmentList enrollments = getEnrollments()) {
            while (enrollments.hasNext()) {
                org.osid.course.program.Enrollment enrollment = enrollments.getNextEnrollment();
                org.osid.course.program.ProgramOffering offering = enrollment.getProgramOffering();
                if (offering.getProgramId().equals(programId)) {
                    ret.add(enrollment);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.program.enrollment.LinkedEnrollmentList(ret));
    }


    /**
     *  Gets all <code> Enrollments </code> for a program effective during the 
     *  entire given date range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known enrollments or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  enrollments that are accessible through this session. 
     *  
     *  In effective mode, enrollments are returned that are currently 
     *  effective. In any effective mode, effective enrollments and those 
     *  currently expired are returned. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of <code> Enrollments </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> programId, from </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOnDate(org.osid.id.Id programId, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.TemporalEnrollmentFilterList(getEnrollmentsForProgram(programId), from, to));
    }


    /**
     *  Gets all <code> Enrollments </code> for a given program and student. 
     *  
     *  In plenary mode, the returned list contains all known enrollments or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  enrollments that are accessible through this session. 
     *  
     *  In effective mode, enrollments are returned that are currently 
     *  effective. In any effective mode, effective enrollments and those 
     *  currently expired are returned. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @return a list of <code> Enrollments </code> 
     *  @throws org.osid.NullArgumentException <code> programId </code> or 
     *          <code> resourceId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramAndStudent(org.osid.id.Id programId, 
                                                                                     org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentFilterList(new StudentFilter(resourceId), getEnrollmentsForProgram(programId)));
    }


    /**
     *  Gets all <code> Enrollments </code> for a program and student 
     *  effective during the entire given date range inclusive but not 
     *  confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known enrollments or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  enrollments that are accessible through this session. 
     *  
     *  In effective mode, enrollments are returned that are currently 
     *  effective. In any effective mode, effective enrollments and those 
     *  currently expired are returned. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of <code> Enrollments </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> programId, resourceId, 
     *          from, </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramAndStudentOnDate(org.osid.id.Id programId, 
                                                                                           org.osid.id.Id resourceId, 
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.TemporalEnrollmentFilterList(getEnrollmentsForProgramAndStudent(programId, resourceId), from, to));
    }


    /**
     *  Gets all <code>Enrollments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Enrollments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.program.EnrollmentList getEnrollments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the enrollment list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of enrollments
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.program.EnrollmentList filterEnrollmentsOnViews(org.osid.course.program.EnrollmentList list)
        throws org.osid.OperationFailedException {

        org.osid.course.program.EnrollmentList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EffectiveEnrollmentFilterList(ret);
        }

        return (ret);
    }


    public static class ProgramOfferingFilter
        implements net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentFilter {
         
        private final org.osid.id.Id programOfferingId;
         
         
        /**
         *  Constructs a new <code>ProgramOfferingFilter</code>.
         *
         *  @param programOfferingId the program offering to filter
         *  @throws org.osid.NullArgumentException
         *          <code>programOfferingId</code> is <code>null</code>
         */
        
        public ProgramOfferingFilter(org.osid.id.Id programOfferingId) {
            nullarg(programOfferingId, "program offering Id");
            this.programOfferingId = programOfferingId;
            return;
        }

         
        /**
         *  Used by the EnrollmentFilterList to filter the enrollment
         *  list based on program offering.
         *
         *  @param enrollment the enrollment
         *  @return <code>true</code> to pass the enrollment,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.program.Enrollment enrollment) {
            return (enrollment.getProgramOfferingId().equals(this.programOfferingId));
        }
    }


    public static class StudentFilter
        implements net.okapia.osid.jamocha.inline.filter.course.program.enrollment.EnrollmentFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>StudentFilter</code>.
         *
         *  @param resourceId the student to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public StudentFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "student Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the EnrollmentFilterList to filter the 
         *  enrollment list based on student.
         *
         *  @param enrollment the enrollment
         *  @return <code>true</code> to pass the enrollment,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.program.Enrollment enrollment) {
            return (enrollment.getStudentId().equals(this.resourceId));
        }
    }
}
