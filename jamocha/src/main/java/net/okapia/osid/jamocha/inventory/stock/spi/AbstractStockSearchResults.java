//
// AbstractStockSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.stock.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractStockSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inventory.StockSearchResults {

    private org.osid.inventory.StockList stocks;
    private final org.osid.inventory.StockQueryInspector inspector;
    private final java.util.Collection<org.osid.inventory.records.StockSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractStockSearchResults.
     *
     *  @param stocks the result set
     *  @param stockQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>stocks</code>
     *          or <code>stockQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractStockSearchResults(org.osid.inventory.StockList stocks,
                                            org.osid.inventory.StockQueryInspector stockQueryInspector) {
        nullarg(stocks, "stocks");
        nullarg(stockQueryInspector, "stock query inspectpr");

        this.stocks = stocks;
        this.inspector = stockQueryInspector;

        return;
    }


    /**
     *  Gets the stock list resulting from a search.
     *
     *  @return a stock list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocks() {
        if (this.stocks == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inventory.StockList stocks = this.stocks;
        this.stocks = null;
	return (stocks);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inventory.StockQueryInspector getStockQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  stock search record <code> Type. </code> This method must
     *  be used to retrieve a stock implementing the requested
     *  record.
     *
     *  @param stockSearchRecordType a stock search 
     *         record type 
     *  @return the stock search
     *  @throws org.osid.NullArgumentException
     *          <code>stockSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(stockSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.StockSearchResultsRecord getStockSearchResultsRecord(org.osid.type.Type stockSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inventory.records.StockSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(stockSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(stockSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record stock search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addStockRecord(org.osid.inventory.records.StockSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "stock record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
