//
// AbstractRouteProgress.java
//
//     Defines a RouteProgress builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.routeprogress.spi;


/**
 *  Defines a <code>RouteProgress</code> builder.
 */

public abstract class AbstractRouteProgressBuilder<T extends AbstractRouteProgressBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCompendiumBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.route.routeprogress.RouteProgressMiter routeProgress;


    /**
     *  Constructs a new <code>AbstractRouteProgressBuilder</code>.
     *
     *  @param routeProgress the route progress to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRouteProgressBuilder(net.okapia.osid.jamocha.builder.mapping.route.routeprogress.RouteProgressMiter routeProgress) {
        super(routeProgress);
        this.routeProgress = routeProgress;
        return;
    }


    /**
     *  Builds the route progress.
     *
     *  @return the new route progress
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.route.RouteProgress build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.route.routeprogress.RouteProgressValidator(getValidations())).validate(this.routeProgress);
        return (new net.okapia.osid.jamocha.builder.mapping.route.routeprogress.ImmutableRouteProgress(this.routeProgress));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the route progress miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.route.routeprogress.RouteProgressMiter getMiter() {
        return (this.routeProgress);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the route.
     *
     *  @param route a route
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>route</code> is <code>null</code>
     */

    public T route(org.osid.mapping.route.Route route) {
        getMiter().setRoute(route);
        return (self());
    }


    /**
     *  Sets the time started.
     *
     *  @param time a time started
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T timeStarted(org.osid.calendaring.DateTime time) {
        getMiter().setTimeStarted(time);
        return (self());
    }


    /**
     *  Sets the total distance traveled.
     *
     *  @param distance a total distance traveled
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public T totalDistanceTraveled(org.osid.mapping.Distance distance) {
        getMiter().setTotalDistanceTraveled(distance);
        return (self());
    }


    /**
     *  Sets the total travel time.
     *
     *  @param duration a total travel time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public T totalTravelTime(org.osid.calendaring.Duration duration) {
        getMiter().setTotalTravelTime(duration);
        return (self());
    }


    /**
     *  Sets the total idle time.
     *
     *  @param duration a total idle time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    public T totalIdleTime(org.osid.calendaring.Duration duration) {
        getMiter().setTotalIdleTime(duration);
        return (self());
    }


    /**
     *  Sets the time last moved.
     *
     *  @param time a time last moved
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    public T timeLastMoved(org.osid.calendaring.DateTime time) {
        getMiter().setTimeLastMoved(time);
        return (self());
    }


    /**
     *  Sets the route segment.
     *
     *  @param routeSegment a route segment
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegment</code> is <code>null</code>
     */

    public T routeSegment(org.osid.mapping.route.RouteSegment routeSegment) {
        getMiter().setRouteSegment(routeSegment);
        return (self());
    }


    /**
     *  Sets the estimated travel time to next segment.
     *
     *  @param duration the estimated travel time to next segment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public T etaToNextSegment(org.osid.calendaring.Duration duration) {
        getMiter().setETAToNextSegment(duration);
        return (self());
    }


    /**
     *  Sets the route segment traveled.
     *
     *  @param routeSegmentTraveled a route segment traveled
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentTraveled</code> is <code>null</code>
     */

    public T routeSegmentTraveled(org.osid.mapping.Distance routeSegmentTraveled) {
        getMiter().setRouteSegmentTraveled(routeSegmentTraveled);
        return (self());
    }


    /**
     *  Sets the time completed.
     *
     *  @param time a time completed
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    public T timeCompleted(org.osid.calendaring.DateTime time) {
        getMiter().setTimeCompleted(time);
        return (self());
    }


    /**
     *  Adds a RouteProgress record.
     *
     *  @param record a route progress record
     *  @param recordType the type of route progress record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.route.records.RouteProgressRecord record, org.osid.type.Type recordType) {
        getMiter().addRouteProgressRecord(record, recordType);
        return (self());
    }
}       


