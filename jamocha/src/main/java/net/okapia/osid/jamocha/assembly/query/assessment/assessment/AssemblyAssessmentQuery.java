//
// AssemblyAssessmentQuery.java
//
//     An AssessmentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AssessmentQuery that stores terms.
 */

public final class AssemblyAssessmentQuery
    extends net.okapia.osid.jamocha.assembly.query.assessment.assessment.spi.AbstractAssemblyAssessmentQuery
    implements org.osid.assessment.AssessmentQuery,
               org.osid.assessment.AssessmentQueryInspector,
               org.osid.assessment.AssessmentSearchOrder {


    /** 
     *  Constructs a new <code>AssemblyAssessmentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    public AssemblyAssessmentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }


    /**
     *  Gets the query assembler.
     *
     *  @return the query assembler
     */

    public net.okapia.osid.jamocha.assembly.query.QueryAssembler getAssembler() {
        return (super.getAssembler());
    }
}
