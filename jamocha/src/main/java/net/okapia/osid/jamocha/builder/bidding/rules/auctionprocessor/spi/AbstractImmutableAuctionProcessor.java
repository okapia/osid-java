//
// AbstractImmutableAuctionProcessor.java
//
//     Wraps a mutable AuctionProcessor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.rules.auctionprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AuctionProcessor</code> to hide modifiers. This
 *  wrapper provides an immutized AuctionProcessor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying auctionProcessor whose state changes are visible.
 */

public abstract class AbstractImmutableAuctionProcessor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidProcessor
    implements org.osid.bidding.rules.AuctionProcessor {

    private final org.osid.bidding.rules.AuctionProcessor auctionProcessor;


    /**
     *  Constructs a new <code>AbstractImmutableAuctionProcessor</code>.
     *
     *  @param auctionProcessor the auction processor to immutablize
     *  @throws org.osid.NullArgumentException <code>auctionProcessor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAuctionProcessor(org.osid.bidding.rules.AuctionProcessor auctionProcessor) {
        super(auctionProcessor);
        this.auctionProcessor = auctionProcessor;
        return;
    }


    /**
     *  Gets the auction processor record corresponding to the given <code> 
     *  AuctionProcessor </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> auctionRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(auctionRecordType) </code> is <code> true </code> . 
     *
     *  @param  auctionProcessorRecordType the type of auction processor 
     *          record to retrieve 
     *  @return the auction processor record 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(auctionProcessorRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorRecord getAuctionProcessorRecord(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException {

        return (this.auctionProcessor.getAuctionProcessorRecord(auctionProcessorRecordType));
    }
}

