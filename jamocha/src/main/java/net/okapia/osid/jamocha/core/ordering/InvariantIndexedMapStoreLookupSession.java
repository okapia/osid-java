//
// InvariantIndexedMapStoreLookupSession
//
//    Implements a Store lookup service backed by a fixed
//    collection of stores indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements a Store lookup service backed by a fixed
 *  collection of stores. The stores are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some stores may be compatible
 *  with more types than are indicated through these store
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapStoreLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractIndexedMapStoreLookupSession
    implements org.osid.ordering.StoreLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapStoreLookupSession} using an
     *  array of stores.
     *
     *  @param stores an array of stores
     *  @throws org.osid.NullArgumentException {@code stores} is
     *          {@code null}
     */

    public InvariantIndexedMapStoreLookupSession(org.osid.ordering.Store[] stores) {
        putStores(stores);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapStoreLookupSession} using a
     *  collection of stores.
     *
     *  @param stores a collection of stores
     *  @throws org.osid.NullArgumentException {@code stores} is
     *          {@code null}
     */

    public InvariantIndexedMapStoreLookupSession(java.util.Collection<? extends org.osid.ordering.Store> stores) {
        putStores(stores);
        return;
    }
}
