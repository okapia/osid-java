//
// AbstractPostEntrySearch.java
//
//     A template for making a PostEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing post entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPostEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.financials.posting.PostEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.financials.posting.records.PostEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.financials.posting.PostEntrySearchOrder postEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of post entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  postEntryIds list of post entries
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPostEntries(org.osid.id.IdList postEntryIds) {
        while (postEntryIds.hasNext()) {
            try {
                this.ids.add(postEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPostEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of post entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPostEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  postEntrySearchOrder post entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>postEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>postEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPostEntryResults(org.osid.financials.posting.PostEntrySearchOrder postEntrySearchOrder) {
	this.postEntrySearchOrder = postEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.financials.posting.PostEntrySearchOrder getPostEntrySearchOrder() {
	return (this.postEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given post entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a post entry implementing the requested record.
     *
     *  @param postEntrySearchRecordType a post entry search record
     *         type
     *  @return the post entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>postEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntrySearchRecord getPostEntrySearchRecord(org.osid.type.Type postEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.financials.posting.records.PostEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(postEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this post entry search. 
     *
     *  @param postEntrySearchRecord post entry search record
     *  @param postEntrySearchRecordType postEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPostEntrySearchRecord(org.osid.financials.posting.records.PostEntrySearchRecord postEntrySearchRecord, 
                                           org.osid.type.Type postEntrySearchRecordType) {

        addRecordType(postEntrySearchRecordType);
        this.records.add(postEntrySearchRecord);        
        return;
    }
}
