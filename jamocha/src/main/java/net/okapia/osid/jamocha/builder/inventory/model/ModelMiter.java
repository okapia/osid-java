//
// ModelMiter.java
//
//     Defines a Model miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.model;


/**
 *  Defines a <code>Model</code> miter for use with the builders.
 */

public interface ModelMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.inventory.Model {


    /**
     *  Sets the manufacturer.
     *
     *  @param manufacturer a manufacturer
     *  @throws org.osid.NullArgumentException
     *          <code>manufacturer</code> is <code>null</code>
     */

    public void setManufacturer(org.osid.resource.Resource manufacturer);


    /**
     *  Sets the archetype.
     *
     *  @param archetype an archetype
     *  @throws org.osid.NullArgumentException <code>archetype</code>
     *          is <code>null</code>
     */

    public void setArchetype(String archetype);


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setNumber(String number);


    /**
     *  Adds a Model record.
     *
     *  @param record a model record
     *  @param recordType the type of model record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addModelRecord(org.osid.inventory.records.ModelRecord record, org.osid.type.Type recordType);
}       


