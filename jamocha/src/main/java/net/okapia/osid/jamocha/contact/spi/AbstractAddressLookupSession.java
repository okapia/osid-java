//
// AbstractAddressLookupSession.java
//
//    A starter implementation framework for providing an Address
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Address
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAddresses(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAddressLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.contact.AddressLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.contact.AddressBook addressBook = new net.okapia.osid.jamocha.nil.contact.addressbook.UnknownAddressBook();
    

    /**
     *  Gets the <code>AddressBook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AddressBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.addressBook.getId());
    }


    /**
     *  Gets the <code>AddressBook</code> associated with this 
     *  session.
     *
     *  @return the <code>AddressBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.addressBook);
    }


    /**
     *  Sets the <code>AddressBook</code>.
     *
     *  @param  addressBook the address book for this session
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>
     */

    protected void setAddressBook(org.osid.contact.AddressBook addressBook) {
        nullarg(addressBook, "address book");
        this.addressBook = addressBook;
        return;
    }


    /**
     *  Tests if this user can perform <code>Address</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAddresses() {
        return (true);
    }


    /**
     *  A complete view of the <code>Address</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAddressView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Address</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAddressView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include address book entries in address books which are
     *  children of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Address</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Address</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Address</code> and
     *  retained for compatibility.
     *
     *  @param  addressId <code>Id</code> of the
     *          <code>Address</code>
     *  @return the address
     *  @throws org.osid.NotFoundException <code>addressId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>addressId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress(org.osid.id.Id addressId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.contact.AddressList addresses = getAddresses()) {
            while (addresses.hasNext()) {
                org.osid.contact.Address address = addresses.getNextAddress();
                if (address.getId().equals(addressId)) {
                    return (address);
                }
            }
        } 

        throw new org.osid.NotFoundException(addressId + " not found");
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  addresses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Addresses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAddresses()</code>.
     *
     *  @param  addressIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>addressIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByIds(org.osid.id.IdList addressIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.contact.Address> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = addressIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAddress(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("address " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.contact.address.LinkedAddressList(ret));
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  address genus <code>Type</code> which does not include
     *  addresses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAddresses()</code>.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.contact.address.AddressGenusFilterList(getAddresses(), addressGenusType));
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  address genus <code>Type</code> and include any additional
     *  addresses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAddresses()</code>.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByParentGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAddressesByGenusType(addressGenusType));
    }


    /**
     *  Gets an <code>AddressList</code> containing the given
     *  address record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAddresses()</code>.
     *
     *  @param  addressRecordType an address record type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByRecordType(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.contact.address.AddressRecordFilterList(getAddresses(), addressRecordType));
    }


    /**
     *  Gets an <code>AddressList</code> for the given resource. In
     *  plenary mode, the returned list contains all known addresses
     *  or an error results. Otherwise, the returned list may contain
     *  only those addresses that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Address</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.contact.address.AddressFilterList(new ResourceFilter(resourceId), getAddresses()));
    }


    /**
     *  Gets all <code>Addresses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Addresses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.contact.AddressList getAddresses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the address list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of addresses
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.contact.AddressList filterAddressesOnViews(org.osid.contact.AddressList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.contact.address.AddressFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the AddressFilterList to filter the 
         *  address list based on resource.
         *
         *  @param address the address
         *  @return <code>true</code> to pass the address,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.contact.Address address) {
            return (address.getResourceId().equals(this.resourceId));
        }
    }    
}
