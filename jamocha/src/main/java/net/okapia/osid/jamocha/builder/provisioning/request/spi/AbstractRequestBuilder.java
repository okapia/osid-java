//
// AbstractRequest.java
//
//     Defines a Request builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.request.spi;


/**
 *  Defines a <code>Request</code> builder.
 */

public abstract class AbstractRequestBuilder<T extends AbstractRequestBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.request.RequestMiter request;


    /**
     *  Constructs a new <code>AbstractRequestBuilder</code>.
     *
     *  @param request the request to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRequestBuilder(net.okapia.osid.jamocha.builder.provisioning.request.RequestMiter request) {
        super(request);
        this.request = request;
        return;
    }


    /**
     *  Builds the request.
     *
     *  @return the new request
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.Request build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.request.RequestValidator(getValidations())).validate(this.request);
        return (new net.okapia.osid.jamocha.builder.provisioning.request.ImmutableRequest(this.request));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the request miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.request.RequestMiter getMiter() {
        return (this.request);
    }


    /**
     *  Sets the request transaction.
     *
     *  @param requestTransaction a request transaction
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransaction</code> is <code>null</code>
     */

    public T requestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        getMiter().setRequestTransaction(requestTransaction);
        return (self());
    }


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>queue</code> is
     *          <code>null</code>
     */

    public T queue(org.osid.provisioning.Queue queue) {
        getMiter().setQueue(queue);
        return (self());
    }


    /**
     *  Sets the request date.
     *
     *  @param date a request date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T requestDate(org.osid.calendaring.DateTime date) {
        getMiter().setRequestDate(date);
        return (self());
    }


    /**
     *  Sets the requester.
     *
     *  @param requester a requester
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>requester</code>
     *          is <code>null</code>
     */

    public T requester(org.osid.resource.Resource requester) {
        getMiter().setRequester(requester);
        return (self());
    }


    /**
     *  Sets the requesting agent.
     *
     *  @param agent a requesting agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T requestingAgent(org.osid.authentication.Agent agent) {
        getMiter().setRequestingAgent(agent);
        return (self());
    }


    /**
     *  Sets the pool.
     *
     *  @param pool a pool
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>pool</code> is
     *          <code>null</code>
     */

    public T pool(org.osid.provisioning.Pool pool) {
        getMiter().setPool(pool);
        return (self());
    }


    /**
     *  Adds a requested provisionable.
     *
     *  @param provisionable a requested provisionable
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    public T requestedProvisionable(org.osid.provisioning.Provisionable provisionable) {
        getMiter().addRequestedProvisionable(provisionable);
        return (self());
    }


    /**
     *  Sets all the requested provisionables.
     *
     *  @param provisionables a collection of requested provisionables
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>provisionables</code> is <code>null</code>
     */

    public T requestedProvisionables(java.util.Collection<org.osid.provisioning.Provisionable> provisionables) {
        getMiter().setRequestedProvisionables(provisionables);
        return (self());
    }


    /**
     *  Sets the exchange provision.
     *
     *  @param provision an exchange provision
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>provision</code>
     *          is <code>null</code>
     */

    public T exchangeProvision(org.osid.provisioning.Provision provision) {
        getMiter().setExchangeProvision(provision);
        return (self());
    }


    /**
     *  Sets the origin provision.
     *
     *  @param provision an origin provision
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>provision</code>
     *          is <code>null</code>
     */

    public T originProvision(org.osid.provisioning.Provision provision) {
        getMiter().setOriginProvision(provision);
        return (self());
    }


    /**
     *  Sets the position.
     *
     *  @param position a position
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>position</code>
     *          is negative
     */

    public T position(long position) {
        getMiter().setPosition(position);
        return (self());
    }


    /**
     *  Sets the ewa.
     *
     *  @param ewa the estimatde waiting time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>ewa</code> is
     *          <code>null</code>
     */

    public T ewa(org.osid.calendaring.Duration ewa) {
        getMiter().setEWA(ewa);
        return (self());
    }


    /**
     *  Adds a Request record.
     *
     *  @param record a request record
     *  @param recordType the type of request record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.records.RequestRecord record, org.osid.type.Type recordType) {
        getMiter().addRequestRecord(record, recordType);
        return (self());
    }
}       


