//
// MutableIndexedMapDispatchLookupSession
//
//    Implements a Dispatch lookup service backed by a collection of
//    dispatches indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription;


/**
 *  Implements a Dispatch lookup service backed by a collection of
 *  dispatches. The dispatches are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some dispatches may be compatible
 *  with more types than are indicated through these dispatch
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of dispatches can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapDispatchLookupSession
    extends net.okapia.osid.jamocha.core.subscription.spi.AbstractIndexedMapDispatchLookupSession
    implements org.osid.subscription.DispatchLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapDispatchLookupSession} with no dispatches.
     *
     *  @param publisher the publisher
     *  @throws org.osid.NullArgumentException {@code publisher}
     *          is {@code null}
     */

      public MutableIndexedMapDispatchLookupSession(org.osid.subscription.Publisher publisher) {
        setPublisher(publisher);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDispatchLookupSession} with a
     *  single dispatch.
     *  
     *  @param publisher the publisher
     *  @param  dispatch a single dispatch
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code dispatch} is {@code null}
     */

    public MutableIndexedMapDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                                  org.osid.subscription.Dispatch dispatch) {
        this(publisher);
        putDispatch(dispatch);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDispatchLookupSession} using an
     *  array of dispatches.
     *
     *  @param publisher the publisher
     *  @param  dispatches an array of dispatches
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code dispatches} is {@code null}
     */

    public MutableIndexedMapDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                                  org.osid.subscription.Dispatch[] dispatches) {
        this(publisher);
        putDispatches(dispatches);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDispatchLookupSession} using a
     *  collection of dispatches.
     *
     *  @param publisher the publisher
     *  @param  dispatches a collection of dispatches
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code dispatches} is {@code null}
     */

    public MutableIndexedMapDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                                  java.util.Collection<? extends org.osid.subscription.Dispatch> dispatches) {

        this(publisher);
        putDispatches(dispatches);
        return;
    }
    

    /**
     *  Makes a {@code Dispatch} available in this session.
     *
     *  @param  dispatch a dispatch
     *  @throws org.osid.NullArgumentException {@code dispatch{@code  is
     *          {@code null}
     */

    @Override
    public void putDispatch(org.osid.subscription.Dispatch dispatch) {
        super.putDispatch(dispatch);
        return;
    }


    /**
     *  Makes an array of dispatches available in this session.
     *
     *  @param  dispatches an array of dispatches
     *  @throws org.osid.NullArgumentException {@code dispatches{@code 
     *          is {@code null}
     */

    @Override
    public void putDispatches(org.osid.subscription.Dispatch[] dispatches) {
        super.putDispatches(dispatches);
        return;
    }


    /**
     *  Makes collection of dispatches available in this session.
     *
     *  @param  dispatches a collection of dispatches
     *  @throws org.osid.NullArgumentException {@code dispatch{@code  is
     *          {@code null}
     */

    @Override
    public void putDispatches(java.util.Collection<? extends org.osid.subscription.Dispatch> dispatches) {
        super.putDispatches(dispatches);
        return;
    }


    /**
     *  Removes a Dispatch from this session.
     *
     *  @param dispatchId the {@code Id} of the dispatch
     *  @throws org.osid.NullArgumentException {@code dispatchId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDispatch(org.osid.id.Id dispatchId) {
        super.removeDispatch(dispatchId);
        return;
    }    
}
