//
// AbstractParticipantSearch.java
//
//     A template for making a Participant Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing participant searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractParticipantSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.ParticipantSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.records.ParticipantSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.ParticipantSearchOrder participantSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of participants. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  participantIds list of participants
     *  @throws org.osid.NullArgumentException
     *          <code>participantIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongParticipants(org.osid.id.IdList participantIds) {
        while (participantIds.hasNext()) {
            try {
                this.ids.add(participantIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongParticipants</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of participant Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getParticipantIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  participantSearchOrder participant search order 
     *  @throws org.osid.NullArgumentException
     *          <code>participantSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>participantSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderParticipantResults(org.osid.offering.ParticipantSearchOrder participantSearchOrder) {
	this.participantSearchOrder = participantSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.ParticipantSearchOrder getParticipantSearchOrder() {
	return (this.participantSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given participant search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a participant implementing the requested record.
     *
     *  @param participantSearchRecordType a participant search record
     *         type
     *  @return the participant search record
     *  @throws org.osid.NullArgumentException
     *          <code>participantSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(participantSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantSearchRecord getParticipantSearchRecord(org.osid.type.Type participantSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.records.ParticipantSearchRecord record : this.records) {
            if (record.implementsRecordType(participantSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this participant search. 
     *
     *  @param participantSearchRecord participant search record
     *  @param participantSearchRecordType participant search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParticipantSearchRecord(org.osid.offering.records.ParticipantSearchRecord participantSearchRecord, 
                                           org.osid.type.Type participantSearchRecordType) {

        addRecordType(participantSearchRecordType);
        this.records.add(participantSearchRecord);        
        return;
    }
}
