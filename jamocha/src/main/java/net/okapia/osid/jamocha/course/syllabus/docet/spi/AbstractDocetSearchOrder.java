//
// AbstractDocetSearchOdrer.java
//
//     Defines a DocetSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code DocetSearchOrder}.
 */

public abstract class AbstractDocetSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.syllabus.DocetSearchOrder {

    private final java.util.Collection<org.osid.course.syllabus.records.DocetSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the module. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByModule(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ModuleSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a module search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a module. 
     *
     *  @return the module search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchOrder getModuleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsModuleSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the activity unit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivityUnit(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> ActivityUnitSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an activity unit search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an actvity unit. 
     *
     *  @return the activity unit search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchOrder getActivityUnitSearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivityUnitSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the in class flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInClass(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  docetRecordType a docet record type 
     *  @return {@code true} if the docetRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code docetRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type docetRecordType) {
        for (org.osid.course.syllabus.records.DocetSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(docetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  docetRecordType the docet record type 
     *  @return the docet search order record
     *  @throws org.osid.NullArgumentException
     *          {@code docetRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(docetRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetSearchOrderRecord getDocetSearchOrderRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.DocetSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(docetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this docet. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param docetRecord the docet search odrer record
     *  @param docetRecordType docet record type
     *  @throws org.osid.NullArgumentException
     *          {@code docetRecord} or
     *          {@code docetRecordTypedocet} is
     *          {@code null}
     */
            
    protected void addDocetRecord(org.osid.course.syllabus.records.DocetSearchOrderRecord docetSearchOrderRecord, 
                                     org.osid.type.Type docetRecordType) {

        addRecordType(docetRecordType);
        this.records.add(docetSearchOrderRecord);
        
        return;
    }
}
