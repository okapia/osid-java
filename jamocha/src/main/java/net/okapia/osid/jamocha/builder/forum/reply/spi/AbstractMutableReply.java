//
// AbstractMutableReply.java
//
//     Defines a mutable Reply.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Reply</code>.
 */

public abstract class AbstractMutableReply
    extends net.okapia.osid.jamocha.forum.reply.spi.AbstractReply
    implements org.osid.forum.Reply,
               net.okapia.osid.jamocha.builder.forum.reply.ReplyMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    @Override
    public void setSequestered(boolean sequestered) {
        super.setSequestered(sequestered);
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this reply. 
     *
     *  @param record reply record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addReplyRecord(org.osid.forum.records.ReplyRecord record, org.osid.type.Type recordType) {
        super.addReplyRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this reply.
     *
     *  @param displayName the name for this reply
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this reply.
     *
     *  @param description the description of this reply
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @throws org.osid.NullArgumentException
     *          <code>post</code> is <code>null</code>
     */

    @Override
    public void setPost(org.osid.forum.Post post) {
        super.setPost(post);
        return;
    }


    /**
     *  Adds a reply.
     *
     *  @param reply a reply
     *  @throws org.osid.NullArgumentException
     *          <code>reply</code> is <code>null</code>
     */

    @Override
    public void addReply(org.osid.forum.Reply reply) {
        super.addReply(reply);
        return;
    }


    /**
     *  Sets all the replies.
     *
     *  @param replies a collection of replies
     *  @throws org.osid.NullArgumentException <code>replies</code> is
     *          <code>null</code>
     */

    @Override
    public void setReplies(java.util.Collection<org.osid.forum.Reply> replies) {
        super.setReplies(replies);
        return;
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    @Override
    public void setTimestamp(org.osid.calendaring.DateTime timestamp) {
        super.setTimestamp(timestamp);
        return;
    }


    /**
     *  Sets the poster.
     *
     *  @param poster a poster
     *  @throws org.osid.NullArgumentException <code>poster</code> is
     *          <code>null</code>
     */

    @Override
    public void setPoster(org.osid.resource.Resource poster) {
        super.setPoster(poster);
        return;
    }


    /**
     *  Sets the posting agent.
     *
     *  @param agent a posting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setPostingAgent(org.osid.authentication.Agent agent) {
        super.setPostingAgent(agent);
        return;
    }


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    @Override
    public void setSubjectLine(org.osid.locale.DisplayText subjectLine) {
        super.setSubjectLine(subjectLine);
        return;
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    @Override
    public void setText(org.osid.locale.DisplayText text) {
        super.setText(text);
        return;
    }
}

