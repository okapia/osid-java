//
// AbstractAdapterStepConstrainerLookupSession.java
//
//    A StepConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A StepConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterStepConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.rules.StepConstrainerLookupSession {

    private final org.osid.workflow.rules.StepConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStepConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStepConstrainerLookupSession(org.osid.workflow.rules.StepConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Office/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Office Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@code Office} associated with this session.
     *
     *  @return the {@code Office} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@code StepConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupStepConstrainers() {
        return (this.session.canLookupStepConstrainers());
    }


    /**
     *  A complete view of the {@code StepConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepConstrainerView() {
        this.session.useComparativeStepConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code StepConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepConstrainerView() {
        this.session.usePlenaryStepConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active step constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepConstrainerView() {
        this.session.useActiveStepConstrainerView();
        return;
    }


    /**
     *  Active and inactive step constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepConstrainerView() {
        this.session.useAnyStatusStepConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code StepConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code StepConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code StepConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @param stepConstrainerId {@code Id} of the {@code StepConstrainer}
     *  @return the step constrainer
     *  @throws org.osid.NotFoundException {@code stepConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code stepConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainer getStepConstrainer(org.osid.id.Id stepConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainer(stepConstrainerId));
    }


    /**
     *  Gets a {@code StepConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stepConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code StepConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @param  stepConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code StepConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByIds(org.osid.id.IdList stepConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainersByIds(stepConstrainerIds));
    }


    /**
     *  Gets a {@code StepConstrainerList} corresponding to the given
     *  step constrainer genus {@code Type} which does not include
     *  step constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned {@code StepConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainersByGenusType(stepConstrainerGenusType));
    }


    /**
     *  Gets a {@code StepConstrainerList} corresponding to the given
     *  step constrainer genus {@code Type} and include any additional
     *  step constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned {@code StepConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByParentGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainersByParentGenusType(stepConstrainerGenusType));
    }


    /**
     *  Gets a {@code StepConstrainerList} containing the given
     *  step constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @param  stepConstrainerRecordType a stepConstrainer record type 
     *  @return the returned {@code StepConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByRecordType(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainersByRecordType(stepConstrainerRecordType));
    }


    /**
     *  Gets all {@code StepConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @return a list of {@code StepConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainers());
    }
}
