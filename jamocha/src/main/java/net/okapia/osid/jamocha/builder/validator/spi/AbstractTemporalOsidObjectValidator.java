//
// AbstractTemporalOsidObjectValidator.java
//
//     Validates Temporal OsidObjects.
//
//
// Tom Coppeto
// Okapia
// 20 September 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;


/**
 *  Validates a Temporal OsidObject.
 */

public abstract class AbstractTemporalOsidObjectValidator
    extends AbstractOsidObjectValidator {

    private final TemporalValidator validator;


    /**
     *  Constructs a new <code>AbstractTemporalOsidObjectValidator</code>.
     */

    protected AbstractTemporalOsidObjectValidator() {
        this.validator = new TemporalValidator();
        return;
    }


    /**
     *  Constructs a new <code>AbstractTemporalOsidObjectValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractTemporalOsidObjectValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        this.validator = new TemporalValidator(validation);
        return;
    }

    
    /**
     *  Validates a TemporalOsidObject.
     *
     *  @param object the object to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     *  @throws org.osid.UnsupportedException <code>object</code> is
     *          not a <code>Temporal</code>
     */

    public void validate(org.osid.OsidObject object) {
        super.validate(object);

        if (!(object instanceof org.osid.Temporal)) {
            throw new org.osid.UnsupportedException("object not a Temporal");
        }

        this.validator.validate((org.osid.Temporal) object);
        return;
    }


    protected class TemporalValidator
        extends AbstractTemporalValidator {

        protected TemporalValidator() {
            return;
        }


        protected TemporalValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
            super(validation);
            return;
        }

        
        public void validate(org.osid.Temporal temporal) {
            super.validate(temporal);
            return;
        }
    }
}
