//
// AbstractParameterProcessorEnablerQueryInspector.java
//
//     A template for making a ParameterProcessorEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for parameter processor enablers.
 */

public abstract class AbstractParameterProcessorEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.configuration.rules.ParameterProcessorEnablerQueryInspector {

    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the parameter processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledParameterProcessorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the parameter processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQueryInspector[] getRuledParameterProcessorTerms() {
        return (new org.osid.configuration.rules.ParameterProcessorQueryInspector[0]);
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given parameter processor enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a parameter processor enabler implementing the requested record.
     *
     *  @param parameterProcessorEnablerRecordType a parameter processor enabler record type
     *  @return the parameter processor enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord getParameterProcessorEnablerQueryInspectorRecord(org.osid.type.Type parameterProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter processor enabler query. 
     *
     *  @param parameterProcessorEnablerQueryInspectorRecord parameter processor enabler query inspector
     *         record
     *  @param parameterProcessorEnablerRecordType parameterProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterProcessorEnablerQueryInspectorRecord(org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord parameterProcessorEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type parameterProcessorEnablerRecordType) {

        addRecordType(parameterProcessorEnablerRecordType);
        nullarg(parameterProcessorEnablerRecordType, "parameter processor enabler record type");
        this.records.add(parameterProcessorEnablerQueryInspectorRecord);        
        return;
    }
}
