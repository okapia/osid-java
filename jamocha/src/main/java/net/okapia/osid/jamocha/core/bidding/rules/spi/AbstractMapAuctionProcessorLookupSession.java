//
// AbstractMapAuctionProcessorLookupSession
//
//    A simple framework for providing an AuctionProcessor lookup service
//    backed by a fixed collection of auction processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AuctionProcessor lookup service backed by a
 *  fixed collection of auction processors. The auction processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuctionProcessorLookupSession
    extends net.okapia.osid.jamocha.bidding.rules.spi.AbstractAuctionProcessorLookupSession
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.bidding.rules.AuctionProcessor> auctionProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.bidding.rules.AuctionProcessor>());


    /**
     *  Makes an <code>AuctionProcessor</code> available in this session.
     *
     *  @param  auctionProcessor an auction processor
     *  @throws org.osid.NullArgumentException <code>auctionProcessor<code>
     *          is <code>null</code>
     */

    protected void putAuctionProcessor(org.osid.bidding.rules.AuctionProcessor auctionProcessor) {
        this.auctionProcessors.put(auctionProcessor.getId(), auctionProcessor);
        return;
    }


    /**
     *  Makes an array of auction processors available in this session.
     *
     *  @param  auctionProcessors an array of auction processors
     *  @throws org.osid.NullArgumentException <code>auctionProcessors<code>
     *          is <code>null</code>
     */

    protected void putAuctionProcessors(org.osid.bidding.rules.AuctionProcessor[] auctionProcessors) {
        putAuctionProcessors(java.util.Arrays.asList(auctionProcessors));
        return;
    }


    /**
     *  Makes a collection of auction processors available in this session.
     *
     *  @param  auctionProcessors a collection of auction processors
     *  @throws org.osid.NullArgumentException <code>auctionProcessors<code>
     *          is <code>null</code>
     */

    protected void putAuctionProcessors(java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessor> auctionProcessors) {
        for (org.osid.bidding.rules.AuctionProcessor auctionProcessor : auctionProcessors) {
            this.auctionProcessors.put(auctionProcessor.getId(), auctionProcessor);
        }

        return;
    }


    /**
     *  Removes an AuctionProcessor from this session.
     *
     *  @param  auctionProcessorId the <code>Id</code> of the auction processor
     *  @throws org.osid.NullArgumentException <code>auctionProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeAuctionProcessor(org.osid.id.Id auctionProcessorId) {
        this.auctionProcessors.remove(auctionProcessorId);
        return;
    }


    /**
     *  Gets the <code>AuctionProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  auctionProcessorId <code>Id</code> of the <code>AuctionProcessor</code>
     *  @return the auctionProcessor
     *  @throws org.osid.NotFoundException <code>auctionProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auctionProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessor getAuctionProcessor(org.osid.id.Id auctionProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.rules.AuctionProcessor auctionProcessor = this.auctionProcessors.get(auctionProcessorId);
        if (auctionProcessor == null) {
            throw new org.osid.NotFoundException("auctionProcessor not found: " + auctionProcessorId);
        }

        return (auctionProcessor);
    }


    /**
     *  Gets all <code>AuctionProcessors</code>. In plenary mode, the returned
     *  list contains all known auctionProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  auctionProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AuctionProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessor.ArrayAuctionProcessorList(this.auctionProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionProcessors.clear();
        super.close();
        return;
    }
}
