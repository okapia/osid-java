//
// AbstractCourseCatalogSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.coursecatalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCourseCatalogSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.CourseCatalogSearchResults {

    private org.osid.course.CourseCatalogList courseCatalogs;
    private final org.osid.course.CourseCatalogQueryInspector inspector;
    private final java.util.Collection<org.osid.course.records.CourseCatalogSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCourseCatalogSearchResults.
     *
     *  @param courseCatalogs the result set
     *  @param courseCatalogQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>courseCatalogs</code>
     *          or <code>courseCatalogQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCourseCatalogSearchResults(org.osid.course.CourseCatalogList courseCatalogs,
                                            org.osid.course.CourseCatalogQueryInspector courseCatalogQueryInspector) {
        nullarg(courseCatalogs, "course catalogs");
        nullarg(courseCatalogQueryInspector, "course catalog query inspectpr");

        this.courseCatalogs = courseCatalogs;
        this.inspector = courseCatalogQueryInspector;

        return;
    }


    /**
     *  Gets the course catalog list resulting from a search.
     *
     *  @return a course catalog list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogs() {
        if (this.courseCatalogs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.CourseCatalogList courseCatalogs = this.courseCatalogs;
        this.courseCatalogs = null;
	return (courseCatalogs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.CourseCatalogQueryInspector getCourseCatalogQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  course catalog search record <code> Type. </code> This method must
     *  be used to retrieve a courseCatalog implementing the requested
     *  record.
     *
     *  @param courseCatalogSearchRecordType a courseCatalog search 
     *         record type 
     *  @return the course catalog search
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(courseCatalogSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseCatalogSearchResultsRecord getCourseCatalogSearchResultsRecord(org.osid.type.Type courseCatalogSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.records.CourseCatalogSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(courseCatalogSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(courseCatalogSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record course catalog search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCourseCatalogRecord(org.osid.course.records.CourseCatalogSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "course catalog record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
