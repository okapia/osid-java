//
// AbstractQueryCategoryLookupSession.java
//
//    An inline adapter that maps a CategoryLookupSession to
//    a CategoryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CategoryLookupSession to
 *  a CategoryQuerySession.
 */

public abstract class AbstractQueryCategoryLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractCategoryLookupSession
    implements org.osid.billing.CategoryLookupSession {

    private final org.osid.billing.CategoryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCategoryLookupSession.
     *
     *  @param querySession the underlying category query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCategoryLookupSession(org.osid.billing.CategoryQuerySession querySession) {
        nullarg(querySession, "category query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Category</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCategories() {
        return (this.session.canSearchCategories());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include categories in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the <code>Category</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Category</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Category</code> and
     *  retained for compatibility.
     *
     *  @param  categoryId <code>Id</code> of the
     *          <code>Category</code>
     *  @return the category
     *  @throws org.osid.NotFoundException <code>categoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>categoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Category getCategory(org.osid.id.Id categoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CategoryQuery query = getQuery();
        query.matchId(categoryId, true);
        org.osid.billing.CategoryList categories = this.session.getCategoriesByQuery(query);
        if (categories.hasNext()) {
            return (categories.getNextCategory());
        } 
        
        throw new org.osid.NotFoundException(categoryId + " not found");
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  categories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Categories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  categoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>categoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByIds(org.osid.id.IdList categoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CategoryQuery query = getQuery();

        try (org.osid.id.IdList ids = categoryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCategoriesByQuery(query));
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  category genus <code>Type</code> which does not include
     *  categories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  categoryGenusType a category genus type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByGenusType(org.osid.type.Type categoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CategoryQuery query = getQuery();
        query.matchGenusType(categoryGenusType, true);
        return (this.session.getCategoriesByQuery(query));
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  category genus <code>Type</code> and include any additional
     *  categories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  categoryGenusType a category genus type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByParentGenusType(org.osid.type.Type categoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CategoryQuery query = getQuery();
        query.matchParentGenusType(categoryGenusType, true);
        return (this.session.getCategoriesByQuery(query));
    }


    /**
     *  Gets a <code>CategoryList</code> containing the given
     *  category record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  categoryRecordType a category record type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByRecordType(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CategoryQuery query = getQuery();
        query.matchRecordType(categoryRecordType, true);
        return (this.session.getCategoriesByQuery(query));
    }

    
    /**
     *  Gets all <code>Categories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Categories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.CategoryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCategoriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.billing.CategoryQuery getQuery() {
        org.osid.billing.CategoryQuery query = this.session.getCategoryQuery();        
        return (query);
    }
}
