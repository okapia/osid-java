//
// AbstractSequenceRuleQuery.java
//
//     A template for making a SequenceRule Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for sequence rules.
 */

public abstract class AbstractSequenceRuleQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.assessment.authoring.SequenceRuleQuery {

    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentPartIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentPartQuery() is false");
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearAssessmentPartTerms() {
        return;
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchNextAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearNextAssessmentPartIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getNextAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsNextAssessmentPartQuery() is false");
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearNextAssessmentPartTerms() {
        return;
    }


    /**
     *  Matches minimum scores that fall in between the given scores 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchMinimumScore(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches assessment parts with any minimum score assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          minimum score, <code> false </code> to match assessment parts 
     *          with no minimum score 
     */

    @OSID @Override
    public void matchAnyMinimumScore(boolean match) {
        return;
    }


    /**
     *  Clears all minimum score terms. 
     */

    @OSID @Override
    public void clearMinimumScoreTerms() {
        return;
    }


    /**
     *  Matches maximum scores that fall in between the given scores 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchMaximumScore(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches assessment parts with any maximum score assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          maximum score, <code> false </code> to match assessment parts 
     *          with no maximum score 
     */

    @OSID @Override
    public void matchAnyMaximumScore(boolean match) {
        return;
    }


    /**
     *  Clears all maximum score terms. 
     */

    @OSID @Override
    public void clearMaximumScoreTerms() {
        return;
    }


    /**
     *  Matches cumulative rules. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchCumulative(boolean match) {
        return;
    }


    /**
     *  Clears all cumulative terms. 
     */

    @OSID @Override
    public void clearCumulativeTerms() {
        return;
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAppliedAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAppliedAssessmentPartIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppliedAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppliedAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getAppliedAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsAppliedAssessmentPartQuery() is false");
    }


    /**
     *  Matches assessment parts with any applied assessment part. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          applied assessment part, <code> false </code> to match 
     *          assessment parts with no applied assessment parts 
     */

    @OSID @Override
    public void matchAnyAppliedAssessmentPart(boolean match) {
        return;
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearAppliedAssessmentPartTerms() {
        return;
    }


    /**
     *  Matches constrainers mapped to the bank. 
     *
     *  @param  bankId the bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears the bank <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears the bank query terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given sequence rule query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a sequence rule implementing the requested record.
     *
     *  @param sequenceRuleRecordType a sequence rule record type
     *  @return the sequence rule query record
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleQueryRecord getSequenceRuleQueryRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleQueryRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this sequence rule query. 
     *
     *  @param sequenceRuleQueryRecord sequence rule query record
     *  @param sequenceRuleRecordType sequenceRule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSequenceRuleQueryRecord(org.osid.assessment.authoring.records.SequenceRuleQueryRecord sequenceRuleQueryRecord, 
                                          org.osid.type.Type sequenceRuleRecordType) {

        addRecordType(sequenceRuleRecordType);
        nullarg(sequenceRuleQueryRecord, "sequence rule query record");
        this.records.add(sequenceRuleQueryRecord);        
        return;
    }
}
