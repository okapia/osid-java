//
// AbstractNodeOrganizationHierarchySession.java
//
//     Defines an Organization hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an organization hierarchy session for delivering a hierarchy
 *  of organizations using the OrganizationNode interface.
 */

public abstract class AbstractNodeOrganizationHierarchySession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractOrganizationHierarchySession
    implements org.osid.personnel.OrganizationHierarchySession {

    private java.util.Collection<org.osid.personnel.OrganizationNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root organization <code> Ids </code> in this hierarchy.
     *
     *  @return the root organization <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootOrganizationIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.personnel.organizationnode.OrganizationNodeToIdList(this.roots));
    }


    /**
     *  Gets the root organizations in the organization hierarchy. A node
     *  with no parents is an orphan. While all organization <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root organizations 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getRootOrganizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.organizationnode.OrganizationNodeToOrganizationList(new net.okapia.osid.jamocha.personnel.organizationnode.ArrayOrganizationNodeList(this.roots)));
    }


    /**
     *  Adds a root organization node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootOrganization(org.osid.personnel.OrganizationNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root organization nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootOrganizations(java.util.Collection<org.osid.personnel.OrganizationNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root organization node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootOrganization(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.personnel.OrganizationNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Organization </code> has any parents. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @return <code> true </code> if the organization has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentOrganizations(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getOrganizationNode(organizationId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  organization.
     *
     *  @param  id an <code> Id </code> 
     *  @param  organizationId the <code> Id </code> of an organization 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> organizationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> organizationId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfOrganization(org.osid.id.Id id, org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.personnel.OrganizationNodeList parents = getOrganizationNode(organizationId).getParentOrganizationNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextOrganizationNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given organization. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @return the parent <code> Ids </code> of the organization 
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentOrganizationIds(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.organization.OrganizationToIdList(getParentOrganizations(organizationId)));
    }


    /**
     *  Gets the parents of the given organization. 
     *
     *  @param  organizationId the <code> Id </code> to query 
     *  @return the parents of the organization 
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getParentOrganizations(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.organizationnode.OrganizationNodeToOrganizationList(getOrganizationNode(organizationId).getParentOrganizationNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  organization.
     *
     *  @param  id an <code> Id </code> 
     *  @param  organizationId the Id of an organization 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> organizationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfOrganization(org.osid.id.Id id, org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOrganization(id, organizationId)) {
            return (true);
        }

        try (org.osid.personnel.OrganizationList parents = getParentOrganizations(organizationId)) {
            while (parents.hasNext()) {
                if (isAncestorOfOrganization(id, parents.getNextOrganization().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an organization has any children. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @return <code> true </code> if the <code> organizationId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildOrganizations(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOrganizationNode(organizationId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  organization.
     *
     *  @param  id an <code> Id </code> 
     *  @param organizationId the <code> Id </code> of an 
     *         organization
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> organizationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> organizationId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfOrganization(org.osid.id.Id id, org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfOrganization(organizationId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  organization.
     *
     *  @param  organizationId the <code> Id </code> to query 
     *  @return the children of the organization 
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildOrganizationIds(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.organization.OrganizationToIdList(getChildOrganizations(organizationId)));
    }


    /**
     *  Gets the children of the given organization. 
     *
     *  @param  organizationId the <code> Id </code> to query 
     *  @return the children of the organization 
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getChildOrganizations(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.organizationnode.OrganizationNodeToOrganizationList(getOrganizationNode(organizationId).getChildOrganizationNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  organization.
     *
     *  @param  id an <code> Id </code> 
     *  @param organizationId the <code> Id </code> of an 
     *         organization
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> organizationId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfOrganization(org.osid.id.Id id, org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOrganization(organizationId, id)) {
            return (true);
        }

        try (org.osid.personnel.OrganizationList children = getChildOrganizations(organizationId)) {
            while (children.hasNext()) {
                if (isDescendantOfOrganization(id, children.getNextOrganization().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  organization.
     *
     *  @param  organizationId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified organization node 
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getOrganizationNodeIds(org.osid.id.Id organizationId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.personnel.organizationnode.OrganizationNodeToNode(getOrganizationNode(organizationId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given organization.
     *
     *  @param  organizationId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified organization node 
     *  @throws org.osid.NotFoundException <code> organizationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> organizationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationNode getOrganizationNodes(org.osid.id.Id organizationId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOrganizationNode(organizationId));
    }


    /**
     *  Closes this <code>OrganizationHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an organization node.
     *
     *  @param organizationId the id of the organization node
     *  @throws org.osid.NotFoundException <code>organizationId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>organizationId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.personnel.OrganizationNode getOrganizationNode(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(organizationId, "organization Id");
        for (org.osid.personnel.OrganizationNode organization : this.roots) {
            if (organization.getId().equals(organizationId)) {
                return (organization);
            }

            org.osid.personnel.OrganizationNode r = findOrganization(organization, organizationId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(organizationId + " is not found");
    }


    protected org.osid.personnel.OrganizationNode findOrganization(org.osid.personnel.OrganizationNode node, 
                                                                   org.osid.id.Id organizationId) 
	throws org.osid.OperationFailedException {

        try (org.osid.personnel.OrganizationNodeList children = node.getChildOrganizationNodes()) {
            while (children.hasNext()) {
                org.osid.personnel.OrganizationNode organization = children.getNextOrganizationNode();
                if (organization.getId().equals(organizationId)) {
                    return (organization);
                }
                
                organization = findOrganization(organization, organizationId);
                if (organization != null) {
                    return (organization);
                }
            }
        }

        return (null);
    }
}
