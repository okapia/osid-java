//
// AbstractSpeedZoneSearch.java
//
//     A template for making a SpeedZone Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing speed zone searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSpeedZoneSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.mapping.path.SpeedZoneSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.mapping.path.SpeedZoneSearchOrder speedZoneSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of speed zones. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  speedZoneIds list of speed zones
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSpeedZones(org.osid.id.IdList speedZoneIds) {
        while (speedZoneIds.hasNext()) {
            try {
                this.ids.add(speedZoneIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSpeedZones</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of speed zone Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSpeedZoneIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  speedZoneSearchOrder speed zone search order 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>speedZoneSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSpeedZoneResults(org.osid.mapping.path.SpeedZoneSearchOrder speedZoneSearchOrder) {
	this.speedZoneSearchOrder = speedZoneSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.mapping.path.SpeedZoneSearchOrder getSpeedZoneSearchOrder() {
	return (this.speedZoneSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given speed zone search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a speed zone implementing the requested record.
     *
     *  @param speedZoneSearchRecordType a speed zone search record
     *         type
     *  @return the speed zone search record
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneSearchRecord getSpeedZoneSearchRecord(org.osid.type.Type speedZoneSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.mapping.path.records.SpeedZoneSearchRecord record : this.records) {
            if (record.implementsRecordType(speedZoneSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this speed zone search. 
     *
     *  @param speedZoneSearchRecord speed zone search record
     *  @param speedZoneSearchRecordType speedZone search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSpeedZoneSearchRecord(org.osid.mapping.path.records.SpeedZoneSearchRecord speedZoneSearchRecord, 
                                           org.osid.type.Type speedZoneSearchRecordType) {

        addRecordType(speedZoneSearchRecordType);
        this.records.add(speedZoneSearchRecord);        
        return;
    }
}
