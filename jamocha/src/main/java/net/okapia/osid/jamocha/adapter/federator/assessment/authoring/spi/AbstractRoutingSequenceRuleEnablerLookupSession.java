//
// AbstractRoutingSequenceRuleEnablerLookupSession
//
//     A routing federating adapter for a SequenceRuleEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for a SequenceRuleEnablerLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of a sequence rule enabler Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the sequence rule enabler is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingSequenceRuleEnablerLookupSession
    extends AbstractFederatingSequenceRuleEnablerLookupSession
    implements org.osid.assessment.authoring.SequenceRuleEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.authoring.SequenceRuleEnablerLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.authoring.SequenceRuleEnablerLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.authoring.SequenceRuleEnablerLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.authoring.SequenceRuleEnablerLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingSequenceRuleEnablerLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingSequenceRuleEnablerLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingSequenceRuleEnablerLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingSequenceRuleEnablerLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>SequenceRuleEnabler</code> specified by its <code>Id</code>. 
     *
     *  @param  sequenceRuleEnablerId <code>Id</code> of the
     *          <code>SequenceRuleEnabler</code>
     *  @return the sequence rule enabler
     *  @throws org.osid.NotFoundException <code>sequenceRuleEnablerId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnabler getSequenceRuleEnabler(org.osid.id.Id sequenceRuleEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session = this.sessionsById.get(sequenceRuleEnablerId.getAuthority());
        if (session != null) {
            return (session.getSequenceRuleEnabler(sequenceRuleEnablerId));
        }
        
        if (this.idFallback) {
            return (super.getSequenceRuleEnabler(sequenceRuleEnablerId));
        }

        throw new org.osid.NotFoundException(sequenceRuleEnablerId + " not found");
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> corresponding to the given
     *  sequenceRuleEnabler genus <code>Type</code> which does not include
     *  sequence rule enablers of types derived from the specified
     *  <code>Type</code>.
     *  
     *
     *  @param  sequenceRuleEnablerGenusType a sequence rule enabler genus type 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByGenusType(org.osid.type.Type sequenceRuleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();
        java.util.Collection<org.osid.assessment.authoring.SequenceRuleEnablerLookupSession> sessions = this.sessionsByType.get(sequenceRuleEnablerGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getSequenceRuleEnablersByGenusType(sequenceRuleEnablerGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.assessment.authoring.sequenceruleenabler.EmptySequenceRuleEnablerList());
            }
        }


        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : sessions) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersByGenusType(sequenceRuleEnablerGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> corresponding to the given
     *  sequence rule enabler genus <code>Type</code> and include any additional
     *  sequence rule enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  sequenceRuleEnablerGenusType a sequence rule enabler genus type 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByParentGenusType(org.osid.type.Type sequenceRuleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();
        java.util.Collection<org.osid.assessment.authoring.SequenceRuleEnablerLookupSession> sessions = this.sessionsByType.get(sequenceRuleEnablerGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getSequenceRuleEnablersByParentGenusType(sequenceRuleEnablerGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.assessment.authoring.sequenceruleenabler.EmptySequenceRuleEnablerList());
            }
        }


        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : sessions) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersByParentGenusType(sequenceRuleEnablerGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> containing the given
     *  sequence rule enabler record <code>Type</code>.
     *
     *  @param  sequenceRuleEnablerRecordType a sequence rule enabler record type 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByRecordType(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();
        java.util.Collection<org.osid.assessment.authoring.SequenceRuleEnablerLookupSession> sessions = this.sessionsByType.get(sequenceRuleEnablerRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getSequenceRuleEnablersByRecordType(sequenceRuleEnablerRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.assessment.authoring.sequenceruleenabler.EmptySequenceRuleEnablerList());
            }
        }

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : sessions) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersByRecordType(sequenceRuleEnablerRecordType));
        }
        
        ret.noMore();
        return (ret);
    }
}
