//
// AbstractFederatingSignalEnablerLookupSession.java
//
//     An abstract federating adapter for a SignalEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SignalEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSignalEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.path.rules.SignalEnablerLookupSession>
    implements org.osid.mapping.path.rules.SignalEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Constructs a new <code>AbstractFederatingSignalEnablerLookupSession</code>.
     */

    protected AbstractFederatingSignalEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.path.rules.SignalEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>SignalEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSignalEnablers() {
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            if (session.canLookupSignalEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>SignalEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSignalEnablerView() {
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            session.useComparativeSignalEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>SignalEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySignalEnablerView() {
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            session.usePlenarySignalEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include signal enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            session.useFederatedMapView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            session.useIsolatedMapView();
        }

        return;
    }


    /**
     *  Only active signal enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSignalEnablerView() {
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            session.useActiveSignalEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive signal enablers are returned by methods in
     *  this session.
     */
    
    @OSID @Override
    public void useAnyStatusSignalEnablerView() {
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            session.useAnyStatusSignalEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>SignalEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SignalEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SignalEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, signal enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  signal enablers are returned.
     *
     *  @param  signalEnablerId <code>Id</code> of the
     *          <code>SignalEnabler</code>
     *  @return the signal enabler
     *  @throws org.osid.NotFoundException <code>signalEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>signalEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnabler getSignalEnabler(org.osid.id.Id signalEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            try {
                return (session.getSignalEnabler(signalEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(signalEnablerId + " not found");
    }


    /**
     *  Gets an <code>SignalEnablerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  signalEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>SignalEnablers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, signal enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  signal enablers are returned.
     *
     *  @param  signalEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByIds(org.osid.id.IdList signalEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.path.rules.signalenabler.MutableSignalEnablerList ret = new net.okapia.osid.jamocha.mapping.path.rules.signalenabler.MutableSignalEnablerList();

        try (org.osid.id.IdList ids = signalEnablerIds) {
            while (ids.hasNext()) {
                ret.addSignalEnabler(getSignalEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SignalEnablerList</code> corresponding to the
     *  given signal enabler genus <code>Type</code> which does not
     *  include signal enablers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known signal
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  signalEnablerGenusType a signal enabler genus type
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByGenusType(org.osid.type.Type signalEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.FederatingSignalEnablerList ret = getSignalEnablerList();

        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            ret.addSignalEnablerList(session.getSignalEnablersByGenusType(signalEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SignalEnablerList</code> corresponding to the
     *  given signal enabler genus <code>Type</code> and include any
     *  additional signal enablers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known signal
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In plenary mode, the returned list contains all known signal
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  signalEnablerGenusType a signal enabler record type
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByParentGenusType(org.osid.type.Type signalEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.FederatingSignalEnablerList ret = getSignalEnablerList();
        
        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            ret.addSignalEnablerList(session.getSignalEnablersByParentGenusType(signalEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {
                break;
            }
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SignalEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  signalEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>SignalEnablers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, signal enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  signal enablers are returned.
     *
     *  @param  signalEnablerRecordType a signalEnabler record type 
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByRecordType(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.FederatingSignalEnablerList ret = getSignalEnablerList();

        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            ret.addSignalEnablerList(session.getSignalEnablersByRecordType(signalEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SignalEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible
     *  through this session.
     *  
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SignalEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.FederatingSignalEnablerList ret = getSignalEnablerList();

        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            ret.addSignalEnablerList(session.getSignalEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>SignalEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible
     *  through this session.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.FederatingSignalEnablerList ret = getSignalEnablerList();

        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            ret.addSignalEnablerList(session.getSignalEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>SignalEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known signal
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  signal enablers are returned.
     *
     *  @return a list of <code>SignalEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.FederatingSignalEnablerList ret = getSignalEnablerList();

        for (org.osid.mapping.path.rules.SignalEnablerLookupSession session : getSessions()) {
            ret.addSignalEnablerList(session.getSignalEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.FederatingSignalEnablerList getSignalEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.ParallelSignalEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.signalenabler.CompositeSignalEnablerList());
        }
    }
}
