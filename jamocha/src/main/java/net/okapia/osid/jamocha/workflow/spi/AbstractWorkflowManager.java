//
// AbstractWorkflowManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractWorkflowManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.workflow.WorkflowManager,
               org.osid.workflow.WorkflowProxyManager {

    private final Types processRecordTypes                 = new TypeRefSet();
    private final Types processSearchRecordTypes           = new TypeRefSet();

    private final Types stepRecordTypes                    = new TypeRefSet();
    private final Types stepSearchRecordTypes              = new TypeRefSet();

    private final Types workRecordTypes                    = new TypeRefSet();
    private final Types workSearchRecordTypes              = new TypeRefSet();

    private final Types workflowEventRecordTypes           = new TypeRefSet();
    private final Types officeRecordTypes                  = new TypeRefSet();
    private final Types officeSearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractWorkflowManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractWorkflowManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any office federation is exposed. Federation is exposed when 
     *  a specific office may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  offices appears as a single office. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up process is supported. 
     *
     *  @return <code> true </code> if process lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessLookup() {
        return (false);
    }


    /**
     *  Tests if querying process is supported. 
     *
     *  @return <code> true </code> if process query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Tests if searching process is supported. 
     *
     *  @return <code> true </code> if process search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSearch() {
        return (false);
    }


    /**
     *  Tests if process administrative service is supported. 
     *
     *  @return <code> true </code> if process administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessAdmin() {
        return (false);
    }


    /**
     *  Tests if a process notification service is supported. 
     *
     *  @return <code> true </code> if process notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessNotification() {
        return (false);
    }


    /**
     *  Tests if a process office lookup service is supported. 
     *
     *  @return <code> true </code> if a process office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessOffice() {
        return (false);
    }


    /**
     *  Tests if a process office service is supported. 
     *
     *  @return <code> true </code> if process to office assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a process smart office lookup service is supported. 
     *
     *  @return <code> true </code> if a process smart office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSmartOffice() {
        return (false);
    }


    /**
     *  Tests if looking up steps is supported. 
     *
     *  @return <code> true </code> if step lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepLookup() {
        return (false);
    }


    /**
     *  Tests if querying steps is supported. 
     *
     *  @return <code> true </code> if step query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (false);
    }


    /**
     *  Tests if searching steps is supported. 
     *
     *  @return <code> true </code> if step search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepSearch() {
        return (false);
    }


    /**
     *  Tests if a step administrative service is supported. 
     *
     *  @return <code> true </code> if step administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepAdmin() {
        return (false);
    }


    /**
     *  Tests if a step <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if step notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepNotification() {
        return (false);
    }


    /**
     *  Tests if a step office lookup service is supported. 
     *
     *  @return <code> true </code> if a step office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepOffice() {
        return (false);
    }


    /**
     *  Tests if a step office assignment service is supported. 
     *
     *  @return <code> true </code> if a step to office assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a step smart office service is supported. 
     *
     *  @return <code> true </code> if a step smart office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepSmartOffice() {
        return (false);
    }


    /**
     *  Tests if looking up work is supported. 
     *
     *  @return <code> true </code> if work lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkLookup() {
        return (false);
    }


    /**
     *  Tests if querying work is supported. 
     *
     *  @return <code> true </code> if work query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Tests if searching work is supported. 
     *
     *  @return <code> true </code> if work search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSearch() {
        return (false);
    }


    /**
     *  Tests if work administrative service is supported. 
     *
     *  @return <code> true </code> if work administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkAdmin() {
        return (false);
    }


    /**
     *  Tests if a work notification service is supported. 
     *
     *  @return <code> true </code> if work notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkNotification() {
        return (false);
    }


    /**
     *  Tests if a work office lookup service is supported. 
     *
     *  @return <code> true </code> if a work office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkOffice() {
        return (false);
    }


    /**
     *  Tests if a work office service is supported. 
     *
     *  @return <code> true </code> if work to office assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a work smart office lookup service is supported. 
     *
     *  @return <code> true </code> if a work smart office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSmartOffice() {
        return (false);
    }


    /**
     *  Tests if a workflow service is supported. 
     *
     *  @return <code> true </code> if workflow is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflow() {
        return (false);
    }


    /**
     *  Tests if a workflow initiation service is supported. 
     *
     *  @return <code> true </code> if workflow initiation is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowInitiation() {
        return (false);
    }


    /**
     *  Tests if a workflow management service is supported. 
     *
     *  @return <code> true </code> if workflow management is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowManagement() {
        return (false);
    }


    /**
     *  Tests if a manual workflow service is supported. 
     *
     *  @return <code> true </code> if manual workflow is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsManualWorkflow() {
        return (false);
    }


    /**
     *  Tests if a workflow <code> </code> event lookup service is supported. 
     *
     *  @return <code> true </code> if workflow event lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowEventLookup() {
        return (false);
    }


    /**
     *  Tests if a workflow event notification service is supported. 
     *
     *  @return <code> true </code> if a workflow event notification service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowEventNotification() {
        return (false);
    }


    /**
     *  Tests if looking up offices is supported. 
     *
     *  @return <code> true </code> if office lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeLookup() {
        return (false);
    }


    /**
     *  Tests if querying offices is supported. 
     *
     *  @return <code> true </code> if a office query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Tests if searching offices is supported. 
     *
     *  @return <code> true </code> if office search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeSearch() {
        return (false);
    }


    /**
     *  Tests if office administrative service is supported. 
     *
     *  @return <code> true </code> if office administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeAdmin() {
        return (false);
    }


    /**
     *  Tests if a office <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if office notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a office hierarchy traversal service. 
     *
     *  @return <code> true </code> if office hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a office hierarchy design service. 
     *
     *  @return <code> true </code> if office hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a workflow batch service. 
     *
     *  @return <code> true </code> if a workflow batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a workflow rules service. 
     *
     *  @return <code> true </code> if a workflow rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Process </code> record types. 
     *
     *  @return a list containing the supported <code> Process </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.processRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Process </code> record type is supported. 
     *
     *  @param  processRecordType a <code> Type </code> indicating a <code> 
     *          Process </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessRecordType(org.osid.type.Type processRecordType) {
        return (this.processRecordTypes.contains(processRecordType));
    }


    /**
     *  Adds support for a process record type.
     *
     *  @param processRecordType a process record type
     *  @throws org.osid.NullArgumentException
     *  <code>processRecordType</code> is <code>null</code>
     */

    protected void addProcessRecordType(org.osid.type.Type processRecordType) {
        this.processRecordTypes.add(processRecordType);
        return;
    }


    /**
     *  Removes support for a process record type.
     *
     *  @param processRecordType a process record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>processRecordType</code> is <code>null</code>
     */

    protected void removeProcessRecordType(org.osid.type.Type processRecordType) {
        this.processRecordTypes.remove(processRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Process </code> search record types. 
     *
     *  @return a list containing the supported <code> Process </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.processSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Process </code> search record type is 
     *  supported. 
     *
     *  @param  processSearchRecordType a <code> Type </code> indicating a 
     *          <code> Process </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        return (this.processSearchRecordTypes.contains(processSearchRecordType));
    }


    /**
     *  Adds support for a process search record type.
     *
     *  @param processSearchRecordType a process search record type
     *  @throws org.osid.NullArgumentException
     *  <code>processSearchRecordType</code> is <code>null</code>
     */

    protected void addProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        this.processSearchRecordTypes.add(processSearchRecordType);
        return;
    }


    /**
     *  Removes support for a process search record type.
     *
     *  @param processSearchRecordType a process search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>processSearchRecordType</code> is <code>null</code>
     */

    protected void removeProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        this.processSearchRecordTypes.remove(processSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Step </code> record types. 
     *
     *  @return a list containing the supported <code> Step </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Step </code> record type is supported. 
     *
     *  @param  stepRecordType a <code> Type </code> indicating a <code> Step 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stepRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepRecordType(org.osid.type.Type stepRecordType) {
        return (this.stepRecordTypes.contains(stepRecordType));
    }


    /**
     *  Adds support for a step record type.
     *
     *  @param stepRecordType a step record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepRecordType</code> is <code>null</code>
     */

    protected void addStepRecordType(org.osid.type.Type stepRecordType) {
        this.stepRecordTypes.add(stepRecordType);
        return;
    }


    /**
     *  Removes support for a step record type.
     *
     *  @param stepRecordType a step record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepRecordType</code> is <code>null</code>
     */

    protected void removeStepRecordType(org.osid.type.Type stepRecordType) {
        this.stepRecordTypes.remove(stepRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Step </code> search types. 
     *
     *  @return a list containing the supported <code> Step </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Step </code> search type is supported. 
     *
     *  @param  stepSearchRecordType a <code> Type </code> indicating a <code> 
     *          Step </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effiortSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepSearchRecordType(org.osid.type.Type stepSearchRecordType) {
        return (this.stepSearchRecordTypes.contains(stepSearchRecordType));
    }


    /**
     *  Adds support for a step search record type.
     *
     *  @param stepSearchRecordType a step search record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepSearchRecordType</code> is <code>null</code>
     */

    protected void addStepSearchRecordType(org.osid.type.Type stepSearchRecordType) {
        this.stepSearchRecordTypes.add(stepSearchRecordType);
        return;
    }


    /**
     *  Removes support for a step search record type.
     *
     *  @param stepSearchRecordType a step search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepSearchRecordType</code> is <code>null</code>
     */

    protected void removeStepSearchRecordType(org.osid.type.Type stepSearchRecordType) {
        this.stepSearchRecordTypes.remove(stepSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Work </code> record types. 
     *
     *  @return a list containing the supported <code> Work </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.workRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Work </code> record type is supported. 
     *
     *  @param  workRecordType a <code> Type </code> indicating a <code> Work 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkRecordType(org.osid.type.Type workRecordType) {
        return (this.workRecordTypes.contains(workRecordType));
    }


    /**
     *  Adds support for a work record type.
     *
     *  @param workRecordType a work record type
     *  @throws org.osid.NullArgumentException
     *  <code>workRecordType</code> is <code>null</code>
     */

    protected void addWorkRecordType(org.osid.type.Type workRecordType) {
        this.workRecordTypes.add(workRecordType);
        return;
    }


    /**
     *  Removes support for a work record type.
     *
     *  @param workRecordType a work record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>workRecordType</code> is <code>null</code>
     */

    protected void removeWorkRecordType(org.osid.type.Type workRecordType) {
        this.workRecordTypes.remove(workRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Work </code> search record types. 
     *
     *  @return a list containing the supported <code> Work </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.workSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Work </code> search record type is 
     *  supported. 
     *
     *  @param  workSearchRecordType a <code> Type </code> indicating a <code> 
     *          Work </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        return (this.workSearchRecordTypes.contains(workSearchRecordType));
    }


    /**
     *  Adds support for a work search record type.
     *
     *  @param workSearchRecordType a work search record type
     *  @throws org.osid.NullArgumentException
     *  <code>workSearchRecordType</code> is <code>null</code>
     */

    protected void addWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        this.workSearchRecordTypes.add(workSearchRecordType);
        return;
    }


    /**
     *  Removes support for a work search record type.
     *
     *  @param workSearchRecordType a work search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>workSearchRecordType</code> is <code>null</code>
     */

    protected void removeWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        this.workSearchRecordTypes.remove(workSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> WorkflowEvent </code> record types. 
     *
     *  @return a list containing the supported <code> WorkflowEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkflowEventRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.workflowEventRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> WorkflowEvent </code> record type is 
     *  supported. 
     *
     *  @param  workflowEventRecordType a <code> Type </code> indicating a 
     *          <code> WorkflowEvent </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workflowEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkflowEventRecordType(org.osid.type.Type workflowEventRecordType) {
        return (this.workflowEventRecordTypes.contains(workflowEventRecordType));
    }


    /**
     *  Adds support for a workflow event record type.
     *
     *  @param workflowEventRecordType a workflow event record type
     *  @throws org.osid.NullArgumentException
     *  <code>workflowEventRecordType</code> is <code>null</code>
     */

    protected void addWorkflowEventRecordType(org.osid.type.Type workflowEventRecordType) {
        this.workflowEventRecordTypes.add(workflowEventRecordType);
        return;
    }


    /**
     *  Removes support for a workflow event record type.
     *
     *  @param workflowEventRecordType a workflow event record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>workflowEventRecordType</code> is <code>null</code>
     */

    protected void removeWorkflowEventRecordType(org.osid.type.Type workflowEventRecordType) {
        this.workflowEventRecordTypes.remove(workflowEventRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Office </code> record types. 
     *
     *  @return a list containing the supported <code> Office </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfficeRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.officeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Office </code> record type is supported. 
     *
     *  @param  officeRecordType a <code> Type </code> indicating a <code> 
     *          Office </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> officeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfficeRecordType(org.osid.type.Type officeRecordType) {
        return (this.officeRecordTypes.contains(officeRecordType));
    }


    /**
     *  Adds support for an office record type.
     *
     *  @param officeRecordType an office record type
     *  @throws org.osid.NullArgumentException
     *  <code>officeRecordType</code> is <code>null</code>
     */

    protected void addOfficeRecordType(org.osid.type.Type officeRecordType) {
        this.officeRecordTypes.add(officeRecordType);
        return;
    }


    /**
     *  Removes support for an office record type.
     *
     *  @param officeRecordType an office record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>officeRecordType</code> is <code>null</code>
     */

    protected void removeOfficeRecordType(org.osid.type.Type officeRecordType) {
        this.officeRecordTypes.remove(officeRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Office </code> search record types. 
     *
     *  @return a list containing the supported <code> Office </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfficeSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.officeSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Office </code> search record type is 
     *  supported. 
     *
     *  @param  officeSearchRecordType a <code> Type </code> indicating a 
     *          <code> Office </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> officeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfficeSearchRecordType(org.osid.type.Type officeSearchRecordType) {
        return (this.officeSearchRecordTypes.contains(officeSearchRecordType));
    }


    /**
     *  Adds support for an office search record type.
     *
     *  @param officeSearchRecordType an office search record type
     *  @throws org.osid.NullArgumentException
     *  <code>officeSearchRecordType</code> is <code>null</code>
     */

    protected void addOfficeSearchRecordType(org.osid.type.Type officeSearchRecordType) {
        this.officeSearchRecordTypes.add(officeSearchRecordType);
        return;
    }


    /**
     *  Removes support for an office search record type.
     *
     *  @param officeSearchRecordType an office search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>officeSearchRecordType</code> is <code>null</code>
     */

    protected void removeOfficeSearchRecordType(org.osid.type.Type officeSearchRecordType) {
        this.officeSearchRecordTypes.remove(officeSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process lookup 
     *  service. 
     *
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessLookupSession getProcessLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessLookupSession getProcessLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessLookupSession getProcessLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessLookupSession getProcessLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process query 
     *  service. 
     *
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuerySession getProcessQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuerySession getProcessQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuerySession getProcessQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuerySession getProcessQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process search 
     *  service. 
     *
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchSession getProcessSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchSession getProcessSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchSession getProcessSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchSession getProcessSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  administration service. 
     *
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessAdminSession getProcessAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessAdminSession getProcessAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessAdminSession getProcessAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessAdminSession getProcessAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  notification service. 
     *
     *  @param  processReceiver the notification callback 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessNotificationSession getProcessNotificationSession(org.osid.workflow.ProcessReceiver processReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  notification service. 
     *
     *  @param  processReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessNotificationSession getProcessNotificationSession(org.osid.workflow.ProcessReceiver processReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  notification service for the given office. 
     *
     *  @param  processReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver </code> 
     *          or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessNotificationSession getProcessNotificationSessionForOffice(org.osid.workflow.ProcessReceiver processReceiver, 
                                                                                               org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  notification service for the given office. 
     *
     *  @param  processReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver, 
     *          officeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessNotificationSession getProcessNotificationSessionForOffice(org.osid.workflow.ProcessReceiver processReceiver, 
                                                                                               org.osid.id.Id officeId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup process/office mappings. 
     *
     *  @return a <code> ProcessOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessOfficeSession getProcessOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup process/office mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessOfficeSession getProcessOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning process 
     *  to offices. 
     *
     *  @return a <code> ProcessOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessOfficeAssignmentSession getProcessOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning process 
     *  to offices. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessOfficeAssignmentSession getProcessOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage process smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSmartOfficeSession getProcessSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getProcessSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage process smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSmartOfficeSession getProcessSmartOfficeSession(org.osid.id.Id officeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getProcessSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step lookup 
     *  service. 
     *
     *  @return a <code> StepLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepLookupSession getStepLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepLookupSession getStepLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepLookupSession getStepLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepLookupSession getStepLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step query 
     *  service. 
     *
     *  @return a <code> StepQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuerySession getStepQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuerySession getStepQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuerySession getStepQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuerySession getStepQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step search 
     *  service. 
     *
     *  @return a <code> StepSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchSession getStepSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchSession getStepSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchSession getStepSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchSession getStepSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  administration service. 
     *
     *  @return a <code> StepAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepAdminSession getStepAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepAdminSession getStepAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepAdminSession getStepAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepAdminSession getStepAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  notification service. 
     *
     *  @param  stepReceiver the notification callback 
     *  @return a <code> StepNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepNotificationSession getStepNotificationSession(org.osid.workflow.StepReceiver stepReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  notification service. 
     *
     *  @param  stepReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> StepNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepNotificationSession getStepNotificationSession(org.osid.workflow.StepReceiver stepReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  notification service for the given office. 
     *
     *  @param  stepReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepReceiver </code> or 
     *          <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepNotificationSession getStepNotificationSessionForOffice(org.osid.workflow.StepReceiver stepReceiver, 
                                                                                         org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  notification service for the given office. 
     *
     *  @param  stepReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepReceiver, officeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepNotificationSession getStepNotificationSessionForOffice(org.osid.workflow.StepReceiver stepReceiver, 
                                                                                         org.osid.id.Id officeId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step/office mappings. 
     *
     *  @return a <code> StepOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeSession getStepOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step/office mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeSession getStepOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning steps to 
     *  offices. 
     *
     *  @return a <code> StepOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepOfficeAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeAssignmentSession getStepOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning steps to 
     *  offices. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepOfficeAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeAssignmentSession getStepOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeSession getStepSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getStepSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeSession getStepSmartOfficeSession(org.osid.id.Id officeId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getStepSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service. 
     *
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkLookupSession getWorkLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkLookupSession getWorkLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkLookupSession getWorkLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkLookupSession getWorkLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service. 
     *
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuerySession getWorkQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuerySession getWorkQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuerySession getWorkQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuerySession getWorkQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service. 
     *
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSearchSession getWorkSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSearchSession getWorkSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSearchSession getWorkSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSearchSession getWorkSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service. 
     *
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkAdminSession getWorkAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkAdminSession getWorkAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkAdminSession getWorkAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkAdminSession getWorkAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service. 
     *
     *  @param  workReceiver the notification callback 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkNotificationSession getWorkNotificationSession(org.osid.workflow.WorkReceiver workReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkNotificationSession getWorkNotificationSession(org.osid.workflow.WorkReceiver workReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service for the given office. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> or 
     *          <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkNotificationSession getWorkNotificationSessionForOffice(org.osid.workflow.WorkReceiver workReceiver, 
                                                                                         org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service for the given office. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver, officeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkNotificationSession getWorkNotificationSessionForOffice(org.osid.workflow.WorkReceiver workReceiver, 
                                                                                         org.osid.id.Id officeId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup work/office mappings. 
     *
     *  @return a <code> WorkOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkOfficeSession getWorkOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup work/office mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkOfficeSession getWorkOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning work to 
     *  offices. 
     *
     *  @return a <code> WorkOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkbOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkOfficeAssignmentSession getWorkOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning work to 
     *  offices. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkbOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkOfficeAssignmentSession getWorkOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage work smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSmartOfficeSession getWorkSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage work smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException no <code> Office </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSmartOfficeSession getWorkSmartOfficeSession(org.osid.id.Id officeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow service. 
     *
     *  @return a <code> WorkflowSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflow() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowSession getWorkflowSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflow() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowSession getWorkflowSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow service for the 
     *  given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkflowSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflow() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowSession getWorkflowSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow service for the 
     *  given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflow() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowSession getWorkflowSessionForOffice(org.osid.id.Id officeId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow initiation service. 
     *
     *  @return a <code> WorkflowInitiationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowInitiation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowInitiationSession getWorkflowInitiationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowInitiationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow initiation service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowInitiationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowInitiation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowInitiationSession getWorkflowInitiationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowInitiationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow initiation service 
     *  for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkflowInitiationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> offiecId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowInitiation() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowInitiationSession getWorkflowInitiationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowInitiationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow initiation service 
     *  for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowInitiationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowInitiation() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowInitiationSession getWorkflowInitiationSessionForOffice(org.osid.id.Id officeId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowInitiationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow management service. 
     *
     *  @return a <code> WorkflowManagementSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManagementSession getWorkflowManagementSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowManagementSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow management service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowManagementSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManagementSession getWorkflowManagementSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowManagementSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow management service 
     *  for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkflowManagementSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowManagement() or supportsVisibleFederation() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManagementSession getWorkflowManagementSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowManagementSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow management service 
     *  for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowManagementSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowManagement() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManagementSession getWorkflowManagementSessionForOffice(org.osid.id.Id officeId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowManagementSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a manual workflow service. 
     *
     *  @return a <code> ManualWorkflowSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManualWorkflow() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ManualWorkflowSession getManualWorkflowSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getManualWorkflowSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a manual workflow service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ManualWorkflowSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManualWorkflow() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ManualWorkflowSession getManualWorkflowSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getManualWorkflowSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a manual workflow service for 
     *  the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> office </code> 
     *  @return a <code> ManualWorkflowSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManualWorkflow() or supportsVisibleFederation() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ManualWorkflowSession getManualWorkflowSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getManualWorkflowSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a manual workflow service for 
     *  the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ManualWorkflowSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManualWorkflow() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ManualWorkflowSession getManualWorkflowSessionForOffice(org.osid.id.Id officeId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getManualWorkflowSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow event lookup 
     *  service. 
     *
     *  @return a <code> WorkflowLogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventLookupSession getWorkflowEventLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow event lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventLookupSession getWorkflowEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow event lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> office </code> 
     *  @return a <code> WorkflowEventLookupSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventLookup() or supportsVisibleFederation() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventLookupSession getWorkflowEventLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowEventLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow event lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventLookupSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventLookupSession getWorkflowEventLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowEventLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the workflow event 
     *  notification service. 
     *
     *  @param  workflowEventReceiver the notification callback 
     *  @return a <code> WorkflowEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workflowEventReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventNotificationSession getWorkflowEventNotificationSession(org.osid.workflow.WorkflowEventReceiver workflowEventReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowEventNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the workflow event 
     *  notification service. 
     *
     *  @param  workflowEventReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workflowEventReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventNotificationSession getWorkflowEventNotificationSession(org.osid.workflow.WorkflowEventReceiver workflowEventReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowEventNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the workflow event 
     *  notification service for the given office. 
     *
     *  @param  workflowEventReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkflowEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> workflowEventReceiver 
     *          </code> or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventNotificationSession getWorkflowEventNotificationSessionForOffice(org.osid.workflow.WorkflowEventReceiver workflowEventReceiver, 
                                                                                                           org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowEventNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the workflow event 
     *  notification service for the given office. 
     *
     *  @param  workflowEventReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> workflowEventReceiver, 
     *          officeId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventNotificationSession getWorkflowEventNotificationSessionForOffice(org.osid.workflow.WorkflowEventReceiver workflowEventReceiver, 
                                                                                                           org.osid.id.Id officeId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowEventNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office lookup 
     *  service. 
     *
     *  @return a <code> OfficeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeLookupSession getOfficeLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getOfficeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeLookupSession getOfficeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getOfficeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office query 
     *  service. 
     *
     *  @return a <code> OfficeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuerySession getOfficeQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getOfficeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuerySession getOfficeQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getOfficeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office search 
     *  service. 
     *
     *  @return a <code> OfficeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeSearchSession getOfficeSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getOfficeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeSearchSession getOfficeSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getOfficeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  administrative service. 
     *
     *  @return a <code> OfficeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeAdminSession getOfficeAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getOfficeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeAdminSession getOfficeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getOfficeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  notification service. 
     *
     *  @param  officeReceiver the notification callback 
     *  @return a <code> OfficeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> officeReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeNotificationSession getOfficeNotificationSession(org.osid.workflow.OfficeReceiver officeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getOfficeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  notification service. 
     *
     *  @param  officeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> OfficeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> officeReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeNotificationSession getOfficeNotificationSession(org.osid.workflow.OfficeReceiver officeReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getOfficeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  hierarchy service. 
     *
     *  @return a <code> OfficeHierarchySession </code> for offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeHierarchySession getOfficeHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getOfficeHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeHierarchySession </code> for offices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeHierarchySession getOfficeHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getOfficeHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeHierarchyDesignSession getOfficeHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getOfficeHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for offices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeHierarchyDesignSession getOfficeHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getOfficeHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> WorkflowBatchManager. </code> 
     *
     *  @return a <code> WorkflowbatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflowBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.WorkflowBatchManager getWorkflowBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowBatchManager not implemented");
    }


    /**
     *  Gets a <code> WorkflowBatchProxyManager. </code> 
     *
     *  @return a <code> WorkflowbatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflowBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.WorkflowBatchProxyManager getWorkflowBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> WorkflowRulesManager. </code> 
     *
     *  @return a <code> WorkflowRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflowRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.WorkflowRulesManager getWorkflowRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowManager.getWorkflowRulesManager not implemented");
    }


    /**
     *  Gets a <code> WorkflowRulesProxyManager </code> . 
     *
     *  @return a <code> WorkflowRulesProxyManager </code> for offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflowRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.WorkflowRulesProxyManager getWorkflowRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.WorkflowProxyManager.getWorkflowRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.processRecordTypes.clear();
        this.processRecordTypes.clear();

        this.processSearchRecordTypes.clear();
        this.processSearchRecordTypes.clear();

        this.stepRecordTypes.clear();
        this.stepRecordTypes.clear();

        this.stepSearchRecordTypes.clear();
        this.stepSearchRecordTypes.clear();

        this.workRecordTypes.clear();
        this.workRecordTypes.clear();

        this.workSearchRecordTypes.clear();
        this.workSearchRecordTypes.clear();

        this.workflowEventRecordTypes.clear();
        this.workflowEventRecordTypes.clear();

        this.officeRecordTypes.clear();
        this.officeRecordTypes.clear();

        this.officeSearchRecordTypes.clear();
        this.officeSearchRecordTypes.clear();

        return;
    }
}
