//
// AbstractAdapterPoolConstrainerEnablerLookupSession.java
//
//    A PoolConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A PoolConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterPoolConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession {

    private final org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPoolConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPoolConstrainerEnablerLookupSession(org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code PoolConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPoolConstrainerEnablers() {
        return (this.session.canLookupPoolConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code PoolConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolConstrainerEnablerView() {
        this.session.useComparativePoolConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code PoolConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolConstrainerEnablerView() {
        this.session.usePlenaryPoolConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool constrainer enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pool constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolConstrainerEnablerView() {
        this.session.useActivePoolConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive pool constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolConstrainerEnablerView() {
        this.session.useAnyStatusPoolConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code PoolConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code PoolConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code PoolConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param poolConstrainerEnablerId {@code Id} of the {@code PoolConstrainerEnabler}
     *  @return the pool constrainer enabler
     *  @throws org.osid.NotFoundException {@code poolConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code poolConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnabler getPoolConstrainerEnabler(org.osid.id.Id poolConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainerEnabler(poolConstrainerEnablerId));
    }


    /**
     *  Gets a {@code PoolConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  poolConstrainerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code PoolConstrainerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  poolConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code PoolConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByIds(org.osid.id.IdList poolConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainerEnablersByIds(poolConstrainerEnablerIds));
    }


    /**
     *  Gets a {@code PoolConstrainerEnablerList} corresponding to the given
     *  pool constrainer enabler genus {@code Type} which does not include
     *  pool constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  poolConstrainerEnablerGenusType a poolConstrainerEnabler genus type 
     *  @return the returned {@code PoolConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByGenusType(org.osid.type.Type poolConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainerEnablersByGenusType(poolConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code PoolConstrainerEnablerList} corresponding to the given
     *  pool constrainer enabler genus {@code Type} and include any additional
     *  pool constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  poolConstrainerEnablerGenusType a poolConstrainerEnabler genus type 
     *  @return the returned {@code PoolConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByParentGenusType(org.osid.type.Type poolConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainerEnablersByParentGenusType(poolConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code PoolConstrainerEnablerList} containing the given
     *  pool constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  poolConstrainerEnablerRecordType a poolConstrainerEnabler record type 
     *  @return the returned {@code PoolConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByRecordType(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainerEnablersByRecordType(poolConstrainerEnablerRecordType));
    }


    /**
     *  Gets a {@code PoolConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code PoolConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code PoolConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code PoolConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getPoolConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code PoolConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainer enablers
     *  are returned.
     *
     *  @return a list of {@code PoolConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainerEnablers());
    }
}
