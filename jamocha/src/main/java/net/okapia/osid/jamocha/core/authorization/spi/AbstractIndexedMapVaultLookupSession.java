//
// AbstractIndexedMapVaultLookupSession.java
//
//    A simple framework for providing a Vault lookup service
//    backed by a fixed collection of vaults with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Vault lookup service backed by a
 *  fixed collection of vaults. The vaults are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some vaults may be compatible
 *  with more types than are indicated through these vault
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Vaults</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapVaultLookupSession
    extends AbstractMapVaultLookupSession
    implements org.osid.authorization.VaultLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authorization.Vault> vaultsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Vault>());
    private final MultiMap<org.osid.type.Type, org.osid.authorization.Vault> vaultsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Vault>());


    /**
     *  Makes a <code>Vault</code> available in this session.
     *
     *  @param  vault a vault
     *  @throws org.osid.NullArgumentException <code>vault<code> is
     *          <code>null</code>
     */

    @Override
    protected void putVault(org.osid.authorization.Vault vault) {
        super.putVault(vault);

        this.vaultsByGenus.put(vault.getGenusType(), vault);
        
        try (org.osid.type.TypeList types = vault.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.vaultsByRecord.put(types.getNextType(), vault);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a vault from this session.
     *
     *  @param vaultId the <code>Id</code> of the vault
     *  @throws org.osid.NullArgumentException <code>vaultId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeVault(org.osid.id.Id vaultId) {
        org.osid.authorization.Vault vault;
        try {
            vault = getVault(vaultId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.vaultsByGenus.remove(vault.getGenusType());

        try (org.osid.type.TypeList types = vault.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.vaultsByRecord.remove(types.getNextType(), vault);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeVault(vaultId);
        return;
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  vault genus <code>Type</code> which does not include
     *  vaults of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known vaults or an error results. Otherwise,
     *  the returned list may contain only those vaults that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.vault.ArrayVaultList(this.vaultsByGenus.get(vaultGenusType)));
    }


    /**
     *  Gets a <code>VaultList</code> containing the given
     *  vault record <code>Type</code>. In plenary mode, the
     *  returned list contains all known vaults or an error
     *  results. Otherwise, the returned list may contain only those
     *  vaults that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  vaultRecordType a vault record type 
     *  @return the returned <code>vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByRecordType(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.vault.ArrayVaultList(this.vaultsByRecord.get(vaultRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.vaultsByGenus.clear();
        this.vaultsByRecord.clear();

        super.close();

        return;
    }
}
