//
// TimeRangeQueryTerm.java
//
//     A time range query term.
//
//
// Tom Coppeto
// Okapia
// 20 April 2013
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A time query term.
 */

public class TimeRangeQueryTerm 
    extends net.okapia.osid.primordium.terms.spi.AbstractTimeRangeTerm
    implements org.osid.search.terms.TimeRangeTerm,
               net.okapia.osid.jamocha.assembly.query.QueryTerm {

    private final String column;


    /**
     *  Constructs a new <code>TimeQueryTerm</code>.
     *
     *  @param column name of query colum
     *  @param start the start of the time range
     *  @param end the end of the time range
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.InvalidArgumentException<code>start</code> is
     *          greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */

    public TimeRangeQueryTerm(String column, org.osid.calendaring.Time start, 
                                  org.osid.calendaring.Time end, boolean match) {
        super(start, end, match);
        nullarg(column, "column");
        this.column = column;

        return;
    }

    
    /**
     *  Gets the query string for this term.
     *
     *  @return the query string
     */

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append('(');
        sb.append(getColumn());
        sb.append(getStartOperator());
        sb.append(getStartValue());
        sb.append(')');
        sb.append(getJoinOperator());
        sb.append('(');
        sb.append(getColumn());
        sb.append(getEndOperator());
        sb.append(getStartValue());
        sb.append(')');

        return (sb.toString());
    }


    /**
     *  Gets the query column.
     *
     *  @return the column
     */

    @Override
    public String getColumn() {
        return (this.column);
    }


    /**
     *  Gets the join operator.     
     *
     *  @return the operator
     */

    protected String getJoinOperator() {
        if (isPositive()) {
            return (" AND ");
        } else {
            return (" OR ");
        }
    }


    /**
     *  Gets the query operator for the first expression.
     *
     *  @return the operator
     */

    protected String getStartOperator() {
        if (isPositive()) {
            return (">=");
        } else {
            return ("<");
        }
    }


    /**
     *  Gets the query operator for the second expression.
     *
     *  @return the operator
     */

    protected String getEndOperator() {
        if (isPositive()) {
            return ("<=");
        } else {
            return (">");
        }
    }


    /**
     *  Gets the starting query value.
     *
     *  @return the value
     */

    protected String getStartValue() {
        return (getTimeRangeStart().toString());
    }


    /**
     *  Gets the ending query value.
     *
     *  @return the value
     */

    protected String getEndValue() {
        return (getTimeRangeEnd().toString());
    }
}
