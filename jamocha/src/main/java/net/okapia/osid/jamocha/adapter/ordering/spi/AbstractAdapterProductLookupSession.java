//
// AbstractAdapterProductLookupSession.java
//
//    A Product lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Product lookup session adapter.
 */

public abstract class AbstractAdapterProductLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ordering.ProductLookupSession {

    private final org.osid.ordering.ProductLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProductLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProductLookupSession(org.osid.ordering.ProductLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Store/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Store Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the {@code Store} associated with this session.
     *
     *  @return the {@code Store} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform {@code Product} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProducts() {
        return (this.session.canLookupProducts());
    }


    /**
     *  A complete view of the {@code Product} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProductView() {
        this.session.useComparativeProductView();
        return;
    }


    /**
     *  A complete view of the {@code Product} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProductView() {
        this.session.usePlenaryProductView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include products in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    
     
    /**
     *  Gets the {@code Product} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Product} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Product} and
     *  retained for compatibility.
     *
     *  @param productId {@code Id} of the {@code Product}
     *  @return the product
     *  @throws org.osid.NotFoundException {@code productId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code productId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct(org.osid.id.Id productId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProduct(productId));
    }


    /**
     *  Gets a {@code ProductList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  products specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Products} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  productIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Product} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code productIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByIds(org.osid.id.IdList productIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProductsByIds(productIds));
    }


    /**
     *  Gets a {@code ProductList} corresponding to the given
     *  product genus {@code Type} which does not include
     *  products of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  productGenusType a product genus type 
     *  @return the returned {@code Product} list
     *  @throws org.osid.NullArgumentException
     *          {@code productGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByGenusType(org.osid.type.Type productGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProductsByGenusType(productGenusType));
    }


    /**
     *  Gets a {@code ProductList} corresponding to the given
     *  product genus {@code Type} and include any additional
     *  products with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  productGenusType a product genus type 
     *  @return the returned {@code Product} list
     *  @throws org.osid.NullArgumentException
     *          {@code productGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByParentGenusType(org.osid.type.Type productGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProductsByParentGenusType(productGenusType));
    }


    /**
     *  Gets a {@code ProductList} containing the given
     *  product record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  productRecordType a product record type 
     *  @return the returned {@code Product} list
     *  @throws org.osid.NullArgumentException
     *          {@code productRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByRecordType(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProductsByRecordType(productRecordType));
    }

    
    /**
     *  Gets the {@code Product} specified by its product code.
     *
     *  @param  code a product code 
     *  @return the product 
     *  @throws org.osid.NotFoundException {@code code} not found 
     *  @throws org.osid.NullArgumentException {@code code} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByCode(String code)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProductsByCode(code));
    }


    /**
     *  Gets a {@code ProductList} containing the given price
     *  schedule.  In plenary mode, the returned list contains all
     *  known products or an error results. Otherwise, the returned
     *  list may contain only those products that are accessible
     *  through this session.
     *
     *  @param  priceScheduleId a price schedule {@code Id} 
     *  @return the product list 
     *  @throws org.osid.NullArgumentException {@code priceScheduleId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProductsByPriceSchedule(priceScheduleId));
    }


    /**
     *  Gets all {@code Products}. 
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Products} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProducts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProducts());
    }
}
