//
// AbstractAssemblyLogEntryQuery.java
//
//     A LogEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.logging.logentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A LogEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyLogEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.logging.LogEntryQuery,
               org.osid.logging.LogEntryQueryInspector,
               org.osid.logging.LogEntrySearchOrder {

    private final java.util.Collection<org.osid.logging.records.LogEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.logging.records.LogEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.logging.records.LogEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyLogEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyLogEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a priority <code> Type </code> for the log entry. 
     *
     *  @param  priorityType <code> Type </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriority(org.osid.type.Type priorityType, boolean match) {
        getAssembler().addTypeTerm(getPriorityColumn(), priorityType, match);
        return;
    }


    /**
     *  Matches log entries with any priority. 
     *
     *  @param  match <code> true </code> to match log entries with any 
     *          priority, <code> false </code> to match log entries with no 
     *          priority 
     */

    @OSID @Override
    public void matchAnyPriority(boolean match) {
        getAssembler().addTypeWildcardTerm(getPriorityColumn(), match);
        return;
    }


    /**
     *  Clears the priority terms. 
     */

    @OSID @Override
    public void clearPriorityTerms() {
        getAssembler().clearTerms(getPriorityColumn());
        return;
    }


    /**
     *  Gets the prirority query terms. 
     *
     *  @return the priority terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getPriorityTerms() {
        return (getAssembler().getTypeTerms(getPriorityColumn()));
    }


    /**
     *  Specifies a preference for ordering log entris by priority type. 
     *
     *  @param  style search otrder style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriority(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPriorityColumn(), style);
        return;
    }


    /**
     *  Gets the Priority column name.
     *
     * @return the column name
     */

    protected String getPriorityColumn() {
        return ("priority");
    }


    /**
     *  Matches a log entries including and above the given priority type. 
     *
     *  @param  priorityType <code> Type </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMinimumPriority(org.osid.type.Type priorityType, 
                                     boolean match) {
        getAssembler().addTypeTerm(getMinimumPriorityColumn(), priorityType, match);
        return;
    }


    /**
     *  Clears the minimum priority terms. 
     */

    @OSID @Override
    public void clearMinimumPriorityTerms() {
        getAssembler().clearTerms(getMinimumPriorityColumn());
        return;
    }


    /**
     *  Gets the minimum prirority query terms. 
     *
     *  @return the minimum priority terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getMinimumPriorityTerms() {
        return (getAssembler().getTypeTerms(getMinimumPriorityColumn()));
    }


    /**
     *  Gets the MinimumPriority column name.
     *
     * @return the column name
     */

    protected String getMinimumPriorityColumn() {
        return ("minimum_priority");
    }


    /**
     *  Matches the time of this log entry. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime startTime, 
                               org.osid.calendaring.DateTime endTime, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimestampColumn(), startTime, endTime, match);
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        getAssembler().clearTerms(getTimestampColumn());
        return;
    }


    /**
     *  Gets the timestamp query terms. 
     *
     *  @return the timestamp terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimestampTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimestampColumn()));
    }


    /**
     *  Specifies a preference for ordering log entries by time. 
     *
     *  @param  style search otrder style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimestamp(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimestampColumn(), style);
        return;
    }


    /**
     *  Gets the Timestamp column name.
     *
     * @return the column name
     */

    protected String getTimestampColumn() {
        return ("timestamp");
    }


    /**
     *  Matches a resource in this log entry. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering log entries by resource. 
     *
     *  @param  style search otrder style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Matches an agent in this log entry. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering log entries by agent. 
     *
     *  @param  style search otrder style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgentColumn(), style);
        return;
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Matches a log. 
     *
     *  @param  logId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchLogId(org.osid.id.Id logId, boolean match) {
        getAssembler().addIdTerm(getLogIdColumn(), logId, match);
        return;
    }


    /**
     *  Clears the log <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLogIdTerms() {
        getAssembler().clearTerms(getLogIdColumn());
        return;
    }


    /**
     *  Gets the log <code> Id </code> query terms. 
     *
     *  @return the log <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLogIdTerms() {
        return (getAssembler().getIdTerms(getLogIdColumn()));
    }


    /**
     *  Gets the LogId column name.
     *
     * @return the column name
     */

    protected String getLogIdColumn() {
        return ("log_id");
    }


    /**
     *  Tests if a <code> LogQuery </code> is available for querying logs. 
     *
     *  @return <code> true </code> if a log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log. 
     *
     *  @return the log query 
     *  @throws org.osid.UnimplementedException <code> supportsLogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuery getLogQuery() {
        throw new org.osid.UnimplementedException("supportsLogQuery() is false");
    }


    /**
     *  Clears the log terms. 
     */

    @OSID @Override
    public void clearLogTerms() {
        getAssembler().clearTerms(getLogColumn());
        return;
    }


    /**
     *  Gets the log query terms. 
     *
     *  @return the log terms 
     */

    @OSID @Override
    public org.osid.logging.LogQueryInspector[] getLogTerms() {
        return (new org.osid.logging.LogQueryInspector[0]);
    }


    /**
     *  Gets the Log column name.
     *
     * @return the column name
     */

    protected String getLogColumn() {
        return ("log");
    }


    /**
     *  Tests if this logEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  logEntryRecordType a log entry record type 
     *  @return <code>true</code> if the logEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type logEntryRecordType) {
        for (org.osid.logging.records.LogEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  logEntryRecordType the log entry record type 
     *  @return the log entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogEntryQueryRecord getLogEntryQueryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  logEntryRecordType the log entry record type 
     *  @return the log entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogEntryQueryInspectorRecord getLogEntryQueryInspectorRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param logEntryRecordType the log entry record type
     *  @return the log entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogEntrySearchOrderRecord getLogEntrySearchOrderRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this log entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param logEntryQueryRecord the log entry query record
     *  @param logEntryQueryInspectorRecord the log entry query inspector
     *         record
     *  @param logEntrySearchOrderRecord the log entry search order record
     *  @param logEntryRecordType log entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryQueryRecord</code>,
     *          <code>logEntryQueryInspectorRecord</code>,
     *          <code>logEntrySearchOrderRecord</code> or
     *          <code>logEntryRecordTypelogEntry</code> is
     *          <code>null</code>
     */
            
    protected void addLogEntryRecords(org.osid.logging.records.LogEntryQueryRecord logEntryQueryRecord, 
                                      org.osid.logging.records.LogEntryQueryInspectorRecord logEntryQueryInspectorRecord, 
                                      org.osid.logging.records.LogEntrySearchOrderRecord logEntrySearchOrderRecord, 
                                      org.osid.type.Type logEntryRecordType) {

        addRecordType(logEntryRecordType);

        nullarg(logEntryQueryRecord, "log entry query record");
        nullarg(logEntryQueryInspectorRecord, "log entry query inspector record");
        nullarg(logEntrySearchOrderRecord, "log entry search odrer record");

        this.queryRecords.add(logEntryQueryRecord);
        this.queryInspectorRecords.add(logEntryQueryInspectorRecord);
        this.searchOrderRecords.add(logEntrySearchOrderRecord);
        
        return;
    }
}
