//
// AbstractJournalQueryInspector.java
//
//     A template for making a JournalQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journal.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for journals.
 */

public abstract class AbstractJournalQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.journaling.JournalQueryInspector {

    private final java.util.Collection<org.osid.journaling.records.JournalQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the journal entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJournalEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the journal entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }


    /**
     *  Gets the branch <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBranchIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the branch query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.BranchQueryInspector[] getBranchTerms() {
        return (new org.osid.journaling.BranchQueryInspector[0]);
    }


    /**
     *  Gets the ancestor journal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorJournalIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor journal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalQueryInspector[] getAncestorJournalTerms() {
        return (new org.osid.journaling.JournalQueryInspector[0]);
    }


    /**
     *  Gets the descendant journal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantJournalIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant journal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalQueryInspector[] getDescendantJournalTerms() {
        return (new org.osid.journaling.JournalQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given journal query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a journal implementing the requested record.
     *
     *  @param journalRecordType a journal record type
     *  @return the journal query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalQueryInspectorRecord getJournalQueryInspectorRecord(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(journalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalRecordType + " is not supported");
    }


    /**
     *  Adds a record to this journal query. 
     *
     *  @param journalQueryInspectorRecord journal query inspector
     *         record
     *  @param journalRecordType journal record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJournalQueryInspectorRecord(org.osid.journaling.records.JournalQueryInspectorRecord journalQueryInspectorRecord, 
                                                   org.osid.type.Type journalRecordType) {

        addRecordType(journalRecordType);
        nullarg(journalRecordType, "journal record type");
        this.records.add(journalQueryInspectorRecord);        
        return;
    }
}
