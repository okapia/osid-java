//
// AbstractQueryConvocationLookupSession.java
//
//    An inline adapter that maps a ConvocationLookupSession to
//    a ConvocationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ConvocationLookupSession to
 *  a ConvocationQuerySession.
 */

public abstract class AbstractQueryConvocationLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractConvocationLookupSession
    implements org.osid.recognition.ConvocationLookupSession {

    private boolean activeonly    = false;
    private final org.osid.recognition.ConvocationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryConvocationLookupSession.
     *
     *  @param querySession the underlying convocation query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryConvocationLookupSession(org.osid.recognition.ConvocationQuerySession querySession) {
        nullarg(querySession, "convocation query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Academy</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.session.getAcademyId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this 
     *  session.
     *
     *  @return the <code>Academy</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAcademy());
    }


    /**
     *  Tests if this user can perform <code>Convocation</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupConvocations() {
        return (this.session.canSearchConvocations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include convocations in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.session.useFederatedAcademyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.session.useIsolatedAcademyView();
        return;
    }
    

    /**
     *  Only active convocations are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveConvocationView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive convocations are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusConvocationView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Convocation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Convocation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Convocation</code> and
     *  retained for compatibility.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationId <code>Id</code> of the
     *          <code>Convocation</code>
     *  @return the convocation
     *  @throws org.osid.NotFoundException <code>convocationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>convocationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Convocation getConvocation(org.osid.id.Id convocationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchId(convocationId, true);
        org.osid.recognition.ConvocationList convocations = this.session.getConvocationsByQuery(query);
        if (convocations.hasNext()) {
            return (convocations.getNextConvocation());
        } 
        
        throw new org.osid.NotFoundException(convocationId + " not found");
    }


    /**
     *  Gets a <code>ConvocationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  convocations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Convocations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>convocationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByIds(org.osid.id.IdList convocationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();

        try (org.osid.id.IdList ids = convocationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getConvocationsByQuery(query));
    }


    /**
     *  Gets a <code>ConvocationList</code> corresponding to the given
     *  convocation genus <code>Type</code> which does not include
     *  convocations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationGenusType a convocation genus type 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByGenusType(org.osid.type.Type convocationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchGenusType(convocationGenusType, true);
        return (this.session.getConvocationsByQuery(query));
    }


    /**
     *  Gets a <code>ConvocationList</code> corresponding to the given
     *  convocation genus <code>Type</code> and include any additional
     *  convocations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationGenusType a convocation genus type 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByParentGenusType(org.osid.type.Type convocationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchParentGenusType(convocationGenusType, true);
        return (this.session.getConvocationsByQuery(query));
    }


    /**
     *  Gets a <code>ConvocationList</code> containing the given
     *  convocation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationRecordType a convocation record type 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByRecordType(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchRecordType(convocationRecordType, true);
        return (this.session.getConvocationsByQuery(query));
    }


    /**
     *  Gets a list of convocations with a date within the given date
     *  range inclusive. In plenary mode, the returned list contains
     *  all known convocations or an error results. Otherwise, the
     *  returned list may contain only those convocations that are
     *  accessible through this session.
     *
     *  @param  from the starting date 
     *  @param  to the ending date 
     *  @return the returned <code> ConvocationList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByDate(org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getConvocationsByQuery(query));
    }


    /**
     *  Gets a list of all convocations corresponding to an award
     *  <code> Id.  </code> In plenary mode, the returned list
     *  contains all known convocations or an error
     *  results. Otherwise, the returned list may contain only those
     *  convocations that are accessible through this session.
     *
     *  @param  awardId the <code> Id </code> of the award 
     *  @return the returned <code> ConvocationList </code> 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByAward(org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchAwardId(awardId, true);
        return (this.session.getConvocationsByQuery(query));
    }

    
    /**
     *  Gets a list of all convocations corresponding to a time period
     *  <code> Id. </code> In plenary mode, the returned list contains
     *  all known convocations or an error results. Otherwise, the
     *  returned list may contain only those convocations that are
     *  accessible through this session.
     *
     *  @param  timePeriodId the <code> Id </code> of the time period 
     *  @return the returned <code> ConvocationList </code> 
     *  @throws org.osid.NullArgumentException <code> timePeriod </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchTimePeriodId(timePeriodId, true);
        return (this.session.getConvocationsByQuery(query));
    }


    /**
     *  Gets a <code>ConvocationList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known convocations or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  convocations that are accessible through this session. 
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Convocation</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getConvocationsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Convocations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @return a list of <code>Convocations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConvocationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getConvocationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.recognition.ConvocationQuery getQuery() {
        org.osid.recognition.ConvocationQuery query = this.session.getConvocationQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
