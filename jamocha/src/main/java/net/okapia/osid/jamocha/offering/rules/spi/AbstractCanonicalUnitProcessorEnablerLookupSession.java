//
// AbstractCanonicalUnitProcessorEnablerLookupSession.java
//
//    A starter implementation framework for providing a CanonicalUnitProcessorEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CanonicalUnitProcessorEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCanonicalUnitProcessorEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCanonicalUnitProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();
    

    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnitProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitProcessorEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>CanonicalUnitProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitProcessorEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CanonicalUnitProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitProcessorEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit processor enablers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active canonical unit processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitProcessorEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive canonical unit processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitProcessorEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }    

     
    /**
     *  Gets the <code>CanonicalUnitProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnitProcessorEnabler</code> may have a
     *  different <code>Id</code> than requested, such as the case
     *  where a duplicate <code>Id</code> was assigned to a
     *  <code>CanonicalUnitProcessorEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  @param  canonicalUnitProcessorEnablerId <code>Id</code> of the
     *          <code>CanonicalUnitProcessorEnabler</code>
     *  @return the canonical unit processor enabler
     *  @throws org.osid.NotFoundException <code>canonicalUnitProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnabler getCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.offering.rules.CanonicalUnitProcessorEnablerList canonicalUnitProcessorEnablers = getCanonicalUnitProcessorEnablers()) {
            while (canonicalUnitProcessorEnablers.hasNext()) {
                org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler = canonicalUnitProcessorEnablers.getNextCanonicalUnitProcessorEnabler();
                if (canonicalUnitProcessorEnabler.getId().equals(canonicalUnitProcessorEnablerId)) {
                    return (canonicalUnitProcessorEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(canonicalUnitProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code>
     *  corresponding to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitProcessorEnablers specified in the
     *  <code>Id</code> list, in the order of the list, including
     *  duplicates, or an error results if an <code>Id</code> in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible <code>CanonicalUnitProcessorEnablers</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from
     *  <code>getCanonicalUnitProcessorEnablers()</code>.
     *
     *  @param  canonicalUnitProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByIds(org.osid.id.IdList canonicalUnitProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.offering.rules.CanonicalUnitProcessorEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = canonicalUnitProcessorEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCanonicalUnitProcessorEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("canonical unit processor enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessorenabler.LinkedCanonicalUnitProcessorEnablerList(ret));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code>
     *  corresponding to the given canonical unit processor enabler
     *  genus <code>Type</code> which does not include canonical unit
     *  processor enablers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from
     *  <code>getCanonicalUnitProcessorEnablers()</code>.
     *
     *  @param canonicalUnitProcessorEnablerGenusType a
     *         canonicalUnitProcessorEnabler genus type
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByGenusType(org.osid.type.Type canonicalUnitProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.rules.canonicalunitprocessorenabler.CanonicalUnitProcessorEnablerGenusFilterList(getCanonicalUnitProcessorEnablers(), canonicalUnitProcessorEnablerGenusType));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code>
     *  corresponding to the given canonical unit processor enabler
     *  genus <code>Type</code> and include any additional canonical
     *  unit processor enablers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCanonicalUnitProcessorEnablers()</code>.
     *
     *  @param canonicalUnitProcessorEnablerGenusType a
     *  canonicalUnitProcessorEnabler genus type
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByParentGenusType(org.osid.type.Type canonicalUnitProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCanonicalUnitProcessorEnablersByGenusType(canonicalUnitProcessorEnablerGenusType));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code>
     *  containing the given canonical unit processor enabler record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from
     *  <code>getCanonicalUnitProcessorEnablers()</code>.
     *
     *  @param canonicalUnitProcessorEnablerRecordType a
     *         canonicalUnitProcessorEnabler record type
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.rules.canonicalunitprocessorenabler.CanonicalUnitProcessorEnablerRecordFilterList(getCanonicalUnitProcessorEnablers(), canonicalUnitProcessorEnablerRecordType));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code>
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processor enablers that are accessible through
     *  this session.
     *  
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.rules.canonicalunitprocessorenabler.TemporalCanonicalUnitProcessorEnablerFilterList(getCanonicalUnitProcessorEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList </code> which
     *  are effective for the entire given date range inclusive but
     *  not confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processor enablers that are accessible through
     *  this session.
     *
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                                                      org.osid.calendaring.DateTime from,
                                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getCanonicalUnitProcessorEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>CanonicalUnitProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processor enablers are returned
     *  that are currently active. In any status mode, active and
     *  inactive canonical unit processor enablers are returned.
     *
     *  @return a list of <code>CanonicalUnitProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the canonical unit processor enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of canonical unit processor enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.offering.rules.CanonicalUnitProcessorEnablerList filterCanonicalUnitProcessorEnablersOnViews(org.osid.offering.rules.CanonicalUnitProcessorEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.offering.rules.CanonicalUnitProcessorEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.offering.rules.canonicalunitprocessorenabler.ActiveCanonicalUnitProcessorEnablerFilterList(ret);
        }

        return (ret);
    }
}
