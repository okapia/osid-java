//
// MutableMapTriggerLookupSession
//
//    Implements a Trigger lookup service backed by a collection of
//    triggers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Trigger lookup service backed by a collection of
 *  triggers. The triggers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of triggers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapTriggerLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapTriggerLookupSession
    implements org.osid.control.TriggerLookupSession {


    /**
     *  Constructs a new {@code MutableMapTriggerLookupSession}
     *  with no triggers.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system} is
     *          {@code null}
     */

      public MutableMapTriggerLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapTriggerLookupSession} with a
     *  single trigger.
     *
     *  @param system the system  
     *  @param trigger a trigger
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code trigger} is {@code null}
     */

    public MutableMapTriggerLookupSession(org.osid.control.System system,
                                           org.osid.control.Trigger trigger) {
        this(system);
        putTrigger(trigger);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapTriggerLookupSession}
     *  using an array of triggers.
     *
     *  @param system the system
     *  @param triggers an array of triggers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggers} is {@code null}
     */

    public MutableMapTriggerLookupSession(org.osid.control.System system,
                                           org.osid.control.Trigger[] triggers) {
        this(system);
        putTriggers(triggers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapTriggerLookupSession}
     *  using a collection of triggers.
     *
     *  @param system the system
     *  @param triggers a collection of triggers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggers} is {@code null}
     */

    public MutableMapTriggerLookupSession(org.osid.control.System system,
                                           java.util.Collection<? extends org.osid.control.Trigger> triggers) {

        this(system);
        putTriggers(triggers);
        return;
    }

    
    /**
     *  Makes a {@code Trigger} available in this session.
     *
     *  @param trigger a trigger
     *  @throws org.osid.NullArgumentException {@code trigger{@code  is
     *          {@code null}
     */

    @Override
    public void putTrigger(org.osid.control.Trigger trigger) {
        super.putTrigger(trigger);
        return;
    }


    /**
     *  Makes an array of triggers available in this session.
     *
     *  @param triggers an array of triggers
     *  @throws org.osid.NullArgumentException {@code triggers{@code 
     *          is {@code null}
     */

    @Override
    public void putTriggers(org.osid.control.Trigger[] triggers) {
        super.putTriggers(triggers);
        return;
    }


    /**
     *  Makes collection of triggers available in this session.
     *
     *  @param triggers a collection of triggers
     *  @throws org.osid.NullArgumentException {@code triggers{@code  is
     *          {@code null}
     */

    @Override
    public void putTriggers(java.util.Collection<? extends org.osid.control.Trigger> triggers) {
        super.putTriggers(triggers);
        return;
    }


    /**
     *  Removes a Trigger from this session.
     *
     *  @param triggerId the {@code Id} of the trigger
     *  @throws org.osid.NullArgumentException {@code triggerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeTrigger(org.osid.id.Id triggerId) {
        super.removeTrigger(triggerId);
        return;
    }    
}
