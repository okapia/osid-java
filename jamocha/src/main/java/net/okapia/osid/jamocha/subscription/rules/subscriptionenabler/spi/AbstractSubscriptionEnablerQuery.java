//
// AbstractSubscriptionEnablerQuery.java
//
//     A template for making a SubscriptionEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for subscription enablers.
 */

public abstract class AbstractSubscriptionEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.subscription.rules.SubscriptionEnablerQuery {

    private final java.util.Collection<org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the subscription. 
     *
     *  @param  subscriptionId the subscription <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subscriptionId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSubscriptionId(org.osid.id.Id subscriptionId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the subscription <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSubscriptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SubscriptionQuery </code> is available. 
     *
     *  @return <code> true </code> if a subscription query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSubscriptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subscription. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the subscription query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSubscriptionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuery getRuledSubscriptionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSubscriptionQuery() is false");
    }


    /**
     *  Matches enablers mapped to any subscription. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          subscription, <code> false </code> to match enablers mapped to 
     *          no subscription 
     */

    @OSID @Override
    public void matchAnyRuledSubscription(boolean match) {
        return;
    }


    /**
     *  Clears the subscription query terms. 
     */

    @OSID @Override
    public void clearRuledSubscriptionTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the publisher. 
     *
     *  @param  publisherId the publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPublisherId(org.osid.id.Id publisherId, boolean match) {
        return;
    }


    /**
     *  Clears the publisher <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPublisherIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsPublisherQuery() is false");
    }


    /**
     *  Clears the publisher query terms. 
     */

    @OSID @Override
    public void clearPublisherTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given subscription enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a subscription enabler implementing the requested record.
     *
     *  @param subscriptionEnablerRecordType a subscription enabler record type
     *  @return the subscription enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord getSubscriptionEnablerQueryRecord(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(subscriptionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subscription enabler query. 
     *
     *  @param subscriptionEnablerQueryRecord subscription enabler query record
     *  @param subscriptionEnablerRecordType subscriptionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubscriptionEnablerQueryRecord(org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord subscriptionEnablerQueryRecord, 
                                          org.osid.type.Type subscriptionEnablerRecordType) {

        addRecordType(subscriptionEnablerRecordType);
        nullarg(subscriptionEnablerQueryRecord, "subscription enabler query record");
        this.records.add(subscriptionEnablerQueryRecord);        
        return;
    }
}
