//
// AbstractNodeMapHierarchySession.java
//
//     Defines a Map hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a map hierarchy session for delivering a hierarchy
 *  of maps using the MapNode interface.
 */

public abstract class AbstractNodeMapHierarchySession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractMapHierarchySession
    implements org.osid.mapping.MapHierarchySession {

    private java.util.Collection<org.osid.mapping.MapNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root map <code> Ids </code> in this hierarchy.
     *
     *  @return the root map <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootMapIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.mapping.mapnode.MapNodeToIdList(this.roots));
    }


    /**
     *  Gets the root maps in the map hierarchy. A node
     *  with no parents is an orphan. While all map <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root maps 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getRootMaps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.mapnode.MapNodeToMapList(new net.okapia.osid.jamocha.mapping.mapnode.ArrayMapNodeList(this.roots)));
    }


    /**
     *  Adds a root map node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootMap(org.osid.mapping.MapNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root map nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootMaps(java.util.Collection<org.osid.mapping.MapNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root map node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootMap(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.mapping.MapNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Map </code> has any parents. 
     *
     *  @param  mapId a map <code> Id </code> 
     *  @return <code> true </code> if the map has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentMaps(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getMapNode(mapId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  map.
     *
     *  @param  id an <code> Id </code> 
     *  @param  mapId the <code> Id </code> of a map 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> mapId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> mapId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfMap(org.osid.id.Id id, org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.MapNodeList parents = getMapNode(mapId).getParentMapNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextMapNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given map. 
     *
     *  @param  mapId a map <code> Id </code> 
     *  @return the parent <code> Ids </code> of the map 
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentMapIds(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.map.MapToIdList(getParentMaps(mapId)));
    }


    /**
     *  Gets the parents of the given map. 
     *
     *  @param  mapId the <code> Id </code> to query 
     *  @return the parents of the map 
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getParentMaps(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.mapnode.MapNodeToMapList(getMapNode(mapId).getParentMapNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  map.
     *
     *  @param  id an <code> Id </code> 
     *  @param  mapId the Id of a map 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> mapId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfMap(org.osid.id.Id id, org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfMap(id, mapId)) {
            return (true);
        }

        try (org.osid.mapping.MapList parents = getParentMaps(mapId)) {
            while (parents.hasNext()) {
                if (isAncestorOfMap(id, parents.getNextMap().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a map has any children. 
     *
     *  @param  mapId a map <code> Id </code> 
     *  @return <code> true </code> if the <code> mapId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildMaps(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getMapNode(mapId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  map.
     *
     *  @param  id an <code> Id </code> 
     *  @param mapId the <code> Id </code> of a 
     *         map
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> mapId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> mapId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfMap(org.osid.id.Id id, org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfMap(mapId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  map.
     *
     *  @param  mapId the <code> Id </code> to query 
     *  @return the children of the map 
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildMapIds(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.map.MapToIdList(getChildMaps(mapId)));
    }


    /**
     *  Gets the children of the given map. 
     *
     *  @param  mapId the <code> Id </code> to query 
     *  @return the children of the map 
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getChildMaps(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.mapping.mapnode.MapNodeToMapList(getMapNode(mapId).getChildMapNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  map.
     *
     *  @param  id an <code> Id </code> 
     *  @param mapId the <code> Id </code> of a 
     *         map
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> mapId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfMap(org.osid.id.Id id, org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfMap(mapId, id)) {
            return (true);
        }

        try (org.osid.mapping.MapList children = getChildMaps(mapId)) {
            while (children.hasNext()) {
                if (isDescendantOfMap(id, children.getNextMap().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  map.
     *
     *  @param  mapId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified map node 
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getMapNodeIds(org.osid.id.Id mapId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.mapping.mapnode.MapNodeToNode(getMapNode(mapId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given map.
     *
     *  @param  mapId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified map node 
     *  @throws org.osid.NotFoundException <code> mapId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mapId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapNode getMapNodes(org.osid.id.Id mapId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getMapNode(mapId));
    }


    /**
     *  Closes this <code>MapHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a map node.
     *
     *  @param mapId the id of the map node
     *  @throws org.osid.NotFoundException <code>mapId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>mapId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.mapping.MapNode getMapNode(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(mapId, "map Id");
        for (org.osid.mapping.MapNode map : this.roots) {
            if (map.getId().equals(mapId)) {
                return (map);
            }

            org.osid.mapping.MapNode r = findMap(map, mapId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(mapId + " is not found");
    }


    protected org.osid.mapping.MapNode findMap(org.osid.mapping.MapNode node, 
                                               org.osid.id.Id mapId) 
	throws org.osid.OperationFailedException {

        try (org.osid.mapping.MapNodeList children = node.getChildMapNodes()) {
            while (children.hasNext()) {
                org.osid.mapping.MapNode map = children.getNextMapNode();
                if (map.getId().equals(mapId)) {
                    return (map);
                }
                
                map = findMap(map, mapId);
                if (map != null) {
                    return (map);
                }
            }
        }

        return (null);
    }
}
