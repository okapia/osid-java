//
// AbstractFederatingHoldLookupSession.java
//
//     An abstract federating adapter for a HoldLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  HoldLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingHoldLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.hold.HoldLookupSession>
    implements org.osid.hold.HoldLookupSession {

    private boolean parallel = false;
    private org.osid.hold.Oubliette oubliette = new net.okapia.osid.jamocha.nil.hold.oubliette.UnknownOubliette();


    /**
     *  Constructs a new <code>AbstractFederatingHoldLookupSession</code>.
     */

    protected AbstractFederatingHoldLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.hold.HoldLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Oubliette/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Oubliette Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.oubliette.getId());
    }


    /**
     *  Gets the <code>Oubliette</code> associated with this 
     *  session.
     *
     *  @return the <code>Oubliette</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.oubliette);
    }


    /**
     *  Sets the <code>Oubliette</code>.
     *
     *  @param  oubliette the oubliette for this session
     *  @throws org.osid.NullArgumentException <code>oubliette</code>
     *          is <code>null</code>
     */

    protected void setOubliette(org.osid.hold.Oubliette oubliette) {
        nullarg(oubliette, "oubliette");
        this.oubliette = oubliette;
        return;
    }


    /**
     *  Tests if this user can perform <code>Hold</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupHolds() {
        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            if (session.canLookupHolds()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Hold</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeHoldView() {
        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            session.useComparativeHoldView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Hold</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryHoldView() {
        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            session.usePlenaryHoldView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include holds in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            session.useFederatedOublietteView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            session.useIsolatedOublietteView();
        }

        return;
    }


    /**
     *  Only holds whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveHoldView() {
        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            session.useEffectiveHoldView();
        }

        return;
    }


    /**
     *  All holds of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveHoldView() {
        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            session.useAnyEffectiveHoldView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Hold</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Hold</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Hold</code> and
     *  retained for compatibility.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdId <code>Id</code> of the
     *          <code>Hold</code>
     *  @return the hold
     *  @throws org.osid.NotFoundException <code>holdId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>holdId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Hold getHold(org.osid.id.Id holdId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            try {
                return (session.getHold(holdId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(holdId + " not found");
    }


    /**
     *  Gets a <code>HoldList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  holds specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Holds</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  holdIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>holdIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByIds(org.osid.id.IdList holdIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.hold.hold.MutableHoldList ret = new net.okapia.osid.jamocha.hold.hold.MutableHoldList();

        try (org.osid.id.IdList ids = holdIds) {
            while (ids.hasNext()) {
                ret.addHold(getHold(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>HoldList</code> corresponding to the given
     *  hold genus <code>Type</code> which does not include
     *  holds of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  holdGenusType a hold genus type 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByGenusType(org.osid.type.Type holdGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsByGenusType(holdGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>HoldList</code> corresponding to the given
     *  hold genus <code>Type</code> and include any additional
     *  holds with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdGenusType a hold genus type 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByParentGenusType(org.osid.type.Type holdGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsByParentGenusType(holdGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>HoldList</code> containing the given
     *  hold record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and
     *  those currently expired are returned.
     *
     *  @param  holdRecordType a hold record type 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByRecordType(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsByRecordType(holdRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>HoldList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *  
     *  In active mode, holds are returned that are currently
     *  active. In any status mode, active and inactive holds
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Hold</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.hold.HoldList getHoldsOnDate(org.osid.calendaring.DateTime from, 
                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of holds corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.hold.HoldList getHoldsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceOnDate(org.osid.id.Id resourceId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to an issue
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  issueId the <code>Id</code> of the issue
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>issueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.hold.HoldList getHoldsForIssue(org.osid.id.Id issueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForIssue(issueId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to an issue
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  issueId the <code>Id</code> of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>issueId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForIssueOnDate(org.osid.id.Id issueId,
                                                         org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForIssueOnDate(issueId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to resource and issue
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  issueId the <code>Id</code> of the issue
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>issueId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceAndIssue(org.osid.id.Id resourceId,
                                                              org.osid.id.Id issueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForResourceAndIssue(resourceId, issueId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to resource and issue
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the resource
     *  @param  issueId the <code>Id</code> of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>issueId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForResourceAndIssueOnDate(org.osid.id.Id resourceId,
                                                                    org.osid.id.Id issueId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForResourceAndIssueOnDate(resourceId, issueId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to an agent
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.hold.HoldList getHoldsForAgent(org.osid.id.Id agentId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForAgent(agentId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to an agent
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are
     *  currently effective.  In any effective mode, effective
     *  holds and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentOnDate(org.osid.id.Id agentId,
                                                         org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForAgentOnDate(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to agent and issue
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  issueId the <code>Id</code> of the issue
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>issueId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentAndIssue(org.osid.id.Id agentId,
                                                           org.osid.id.Id issueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForAgentAndIssue(agentId, issueId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of holds corresponding to agent and issue
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible
     *  through this session.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @param agentId the <code>Id</code> of the agent
     *  @param  issueId the <code>Id</code> of the issue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>HoldList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>issueId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsForAgentAndIssueOnDate(org.osid.id.Id agentId,
                                                                 org.osid.id.Id issueId,
                                                                 org.osid.calendaring.DateTime from,
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHoldsForAgentAndIssueOnDate(agentId, issueId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Holds</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  holds or an error results. Otherwise, the returned list
     *  may contain only those holds that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, holds are returned that are currently
     *  effective.  In any effective mode, effective holds and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Holds</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHolds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList ret = getHoldList();

        for (org.osid.hold.HoldLookupSession session : getSessions()) {
            ret.addHoldList(session.getHolds());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.hold.hold.FederatingHoldList getHoldList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.hold.hold.ParallelHoldList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.hold.hold.CompositeHoldList());
        }
    }
}
