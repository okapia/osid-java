//
// AbstractMapObjectiveLookupSession
//
//    A simple framework for providing an Objective lookup service
//    backed by a fixed collection of objectives.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Objective lookup service backed by a
 *  fixed collection of objectives. The objectives are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Objectives</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapObjectiveLookupSession
    extends net.okapia.osid.jamocha.learning.spi.AbstractObjectiveLookupSession
    implements org.osid.learning.ObjectiveLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.learning.Objective> objectives = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.learning.Objective>());


    /**
     *  Makes an <code>Objective</code> available in this session.
     *
     *  @param  objective an objective
     *  @throws org.osid.NullArgumentException <code>objective<code>
     *          is <code>null</code>
     */

    protected void putObjective(org.osid.learning.Objective objective) {
        this.objectives.put(objective.getId(), objective);
        return;
    }


    /**
     *  Makes an array of objectives available in this session.
     *
     *  @param  objectives an array of objectives
     *  @throws org.osid.NullArgumentException <code>objectives<code>
     *          is <code>null</code>
     */

    protected void putObjectives(org.osid.learning.Objective[] objectives) {
        putObjectives(java.util.Arrays.asList(objectives));
        return;
    }


    /**
     *  Makes a collection of objectives available in this session.
     *
     *  @param  objectives a collection of objectives
     *  @throws org.osid.NullArgumentException <code>objectives<code>
     *          is <code>null</code>
     */

    protected void putObjectives(java.util.Collection<? extends org.osid.learning.Objective> objectives) {
        for (org.osid.learning.Objective objective : objectives) {
            this.objectives.put(objective.getId(), objective);
        }

        return;
    }


    /**
     *  Removes an Objective from this session.
     *
     *  @param  objectiveId the <code>Id</code> of the objective
     *  @throws org.osid.NullArgumentException <code>objectiveId<code> is
     *          <code>null</code>
     */

    protected void removeObjective(org.osid.id.Id objectiveId) {
        this.objectives.remove(objectiveId);
        return;
    }


    /**
     *  Gets the <code>Objective</code> specified by its <code>Id</code>.
     *
     *  @param  objectiveId <code>Id</code> of the <code>Objective</code>
     *  @return the objective
     *  @throws org.osid.NotFoundException <code>objectiveId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>objectiveId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.learning.Objective objective = this.objectives.get(objectiveId);
        if (objective == null) {
            throw new org.osid.NotFoundException("objective not found: " + objectiveId);
        }

        return (objective);
    }


    /**
     *  Gets all <code>Objectives</code>. In plenary mode, the returned
     *  list contains all known objectives or an error
     *  results. Otherwise, the returned list may contain only those
     *  objectives that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Objectives</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.objectives.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.objectives.clear();
        super.close();
        return;
    }
}
