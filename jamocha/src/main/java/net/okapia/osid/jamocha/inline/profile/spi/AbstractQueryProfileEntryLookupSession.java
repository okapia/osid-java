//
// AbstractQueryProfileEntryLookupSession.java
//
//    An inline adapter that maps a ProfileEntryLookupSession to
//    a ProfileEntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProfileEntryLookupSession to
 *  a ProfileEntryQuerySession.
 */

public abstract class AbstractQueryProfileEntryLookupSession
    extends net.okapia.osid.jamocha.profile.spi.AbstractProfileEntryLookupSession
    implements org.osid.profile.ProfileEntryLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.profile.ProfileEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProfileEntryLookupSession.
     *
     *  @param querySession the underlying profile entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProfileEntryLookupSession(org.osid.profile.ProfileEntryQuerySession querySession) {
        nullarg(querySession, "profile entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Profile</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Profile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.session.getProfileId());
    }


    /**
     *  Gets the <code>Profile</code> associated with this 
     *  session.
     *
     *  @return the <code>Profile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getProfile());
    }


    /**
     *  Tests if this user can perform <code>ProfileEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProfileEntries() {
        return (this.session.canSearchProfileEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile entries in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.session.useFederatedProfileView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        this.session.useIsolatedProfileView();
        return;
    }
    

    /**
     *  Only profile entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProfileEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All profile entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProfileEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }


    /**
     *  Sets the view for methods in this session to implicit profile
     *  entries.  An implicit view will include profile entries
     *  derived from other entries as a result of the <code>
     *  ProfileItem </code> or <code> Resource </code>
     *  hierarchies. This method is the opposite of <code>
     *  explicitProfileEntryView(). </code>
     */

    @OSID @Override
    public void useImplicitProfileEntryView() {
        this.session.useImplicitProfileEntryView();
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit profile
     *  entries.  An explicit view includes only those profile entries
     *  that were explicitly defined and not implied. This method is
     *  the opposite of <code> implicitProfileEntryView(). </code>
     */

    @OSID @Override
    public void useExplicitProfileEntryView() {
        this.session.useExplicitProfileEntryView();
        return;
    }

     
    /**
     *  Gets the <code>ProfileEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProfileEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProfileEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryId <code>Id</code> of the
     *          <code>ProfileEntry</code>
     *  @return the profile entry
     *  @throws org.osid.NotFoundException <code>profileEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>profileEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchId(profileEntryId, true);
        org.osid.profile.ProfileEntryList profileEntries = this.session.getProfileEntriesByQuery(query);
        if (profileEntries.hasNext()) {
            return (profileEntries.getNextProfileEntry());
        } 
        
        throw new org.osid.NotFoundException(profileEntryId + " not found");
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profileEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProfileEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, profile entries are returned that are currently effective.
     *  In any effective mode, effective profile entries and those currently expired
     *  are returned.
     *
     *  @param  profileEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByIds(org.osid.id.IdList profileEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();

        try (org.osid.id.IdList ids = profileEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the given
     *  profile entry genus <code>Type</code> which does not include
     *  profile entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently effective.
     *  In any effective mode, effective profile entries and those currently expired
     *  are returned.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchGenusType(profileEntryGenusType, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the given
     *  profile entry genus <code>Type</code> and include any additional
     *  profile entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByParentGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchParentGenusType(profileEntryGenusType, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> containing the given
     *  profile entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryRecordType a profileEntry record type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByRecordType(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchRecordType(profileEntryRecordType, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *  
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProfileEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getProfileEntriesByQuery(query));
    }
        

    /**
     *  Gets a list of profile entries corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a list of profile entries corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a list of <code> ProfileEntries </code> associated with a
     *  given agent.
     *  
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile ebtries that are accessible through
     *  this session.
     *  
     *  In effective mode, profile entries are returned that are
     *  currently effective. In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @return the returned <code> ProfileEntry list </code> 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgent(org.osid.id.Id agentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchAgentId(agentId, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a <code> ProfileEntryList </code> for the given agent and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session.
     *  
     *  In effective mode, profile entries are returned that are
     *  currently effective in addition to being effective during the
     *  date range. In any effective mode, effective profile entries
     *  and those currently expired are returned.
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> ProfileEntry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> agentId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentOnDate(org.osid.id.Id agentId, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchAgentId(agentId, true);
        query.matchDate(from, to, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>profileItemId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItem(org.osid.id.Id profileItemId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchProfileItemId(profileItemId, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>profileItemId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItemOnDate(org.osid.id.Id profileItemId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchProfileItemId(profileItemId, true);
        query.matchDate(from, to, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a list of profile entries corresponding to resource and
     *  profile item <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>profileItemId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItem(org.osid.id.Id resourceId,
                                                                                        org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchProfileItemId(profileItemId, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a list of profile entries corresponding to resource and
     *  profile item <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  profileItemId the <code>Id</code> of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProfileEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>profileItemId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItemOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.id.Id profileItemId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchProfileItemId(profileItemId, true);
        query.matchDate(from, to, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a list of <code> ProfileEntries </code> associated for
     *  entries explicitly mapped to a given agent and profile item.
     *  
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile ebtries that are accessible through
     *  this session.
     *  
     *  In effective mode, profile entries are returned that are
     *  currently effective. In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  profileItemId a profile item <code> Id </code> 
     *  @return the returned <code> ProfileEntry list </code> 
     *  @throws org.osid.NullArgumentException <code> agentId </code> or 
     *          <code> profileItemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItem(org.osid.id.Id agentId, 
                                                                                     org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchAgentId(agentId, true);
        query.matchProfileItemId(profileItemId, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets a <code> ProfileEntryList </code> for entries explicitly
     *  mapped to a given agent and profile item and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session.
     *  
     *  In effective mode, profile entries are returned that are
     *  currently effective. In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param agentId an agent <code> Id </code>
     *  @param  profileItemId a profile item <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> ProfileEntry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> agentId,
     *          profileItemId, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItemOnDate(org.osid.id.Id agentId, 
                                                                                           org.osid.id.Id profileItemId, 
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchAgentId(agentId, true);
        query.matchProfileItemId(profileItemId, true);
        query.matchDate(from, to, true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets the explicit <code> ProfileEntry </code> that generated
     *  the given implicit profile entry. If the given <code>
     *  ProfileEntry </code> is explicit, then the same <code>
     *  ProfileEntry </code> is returned.
     *
     *  @param  profileEntryId a profile entry 
     *  @return the explicit <code> ProfileEntry </code> 
     *  @throws org.osid.NotFoundException <code> profileEntryId
     *          </code> is not found
     *  @throws org.osid.NullArgumentException <code> profileEntryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getExplicitProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchRelatedProfileEntryId(profileEntryId, true);
        query.matchImplicit(false);

        try (org.osid.profile.ProfileEntryList entries = this.session.getProfileEntriesByQuery(query)) {
            if (entries.hasNext()) {
                return (entries.getNextProfileEntry());
            }
        }

        throw new org.osid.NotFoundException("no explicit entry found for " + profileEntryId);
    }


    /**
     *  Gets all <code>ProfileEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>ProfileEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.profile.ProfileEntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProfileEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.profile.ProfileEntryQuery getQuery() {
        org.osid.profile.ProfileEntryQuery query = this.session.getProfileEntryQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
