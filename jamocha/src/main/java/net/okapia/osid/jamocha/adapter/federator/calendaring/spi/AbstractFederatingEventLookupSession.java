//
// AbstractFederatingEventLookupSession.java
//
//     An abstract federating adapter for an EventLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EventLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEventLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.EventLookupSession>
    implements org.osid.calendaring.EventLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingEventLookupSession</code>.
     */

    protected AbstractFederatingEventLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.EventLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>Event</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEvents() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            if (session.canLookupEvents()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Event</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useComparativeEventView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Event</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.usePlenaryEventView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }


    /**
     *  Only events whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useEffectiveEventView();
        }

        return;
    }


    /**
     *  All events of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useAnyEffectiveEventView();
        }

        return;
    }


    /**
     *  A normalized view uses a single <code> Event </code> to represent a
     *  set of recurring events.
     */

    @OSID @Override
    public void useNormalizedEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useNormalizedEventView();
        }

        return;
    }


    /**
     *  A denormalized view expands recurring events into a series of <code>
     *  Events. </code>
     */

    @OSID @Override
    public void useDenormalizedEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useDenormalizedEventView();
        }

        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  events.
     */

    @OSID @Override
    public void useSequesteredEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useSequesteredEventView();
        }

        return;
    }


    /**
     *  All events are returned including sequestered events.
     */

    @OSID @Override
    public void useUnsequesteredEventView() {
        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            session.useUnsequesteredEventView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Event</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Event</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Event</code> and
     *  retained for compatibility.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventId <code>Id</code> of the
     *          <code>Event</code>
     *  @return the event
     *  @throws org.osid.NotFoundException <code>eventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>eventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent(org.osid.id.Id eventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            try {
                return (session.getEvent(eventId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(eventId + " not found");
    }


    /**
     *  Gets an <code>EventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  events specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Events</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, events are returned that are currently effective.
     *  In any effective mode, effective events and those currently expired
     *  are returned.
     *
     *  @param  eventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>eventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByIds(org.osid.id.IdList eventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.event.MutableEventList ret = new net.okapia.osid.jamocha.calendaring.event.MutableEventList();

        try (org.osid.id.IdList ids = eventIds) {
            while (ids.hasNext()) {
                ret.addEvent(getEvent(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EventList</code> corresponding to the given
     *  event genus <code>Type</code> which does not include
     *  events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently effective.
     *  In any effective mode, effective events and those currently expired
     *  are returned.
     *
     *  @param  eventGenusType an event genus type 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByGenusType(org.osid.type.Type eventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsByGenusType(eventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EventList</code> corresponding to the given
     *  event genus <code>Type</code> and include any additional
     *  events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventGenusType an event genus type 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByParentGenusType(org.osid.type.Type eventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsByParentGenusType(eventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EventList</code> containing the given
     *  event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @param  eventRecordType an event record type 
     *  @return the returned <code>Event</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByRecordType(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsByRecordType(eventRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EventList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible
     *  through this session.
     *  
     *  In active mode, events are returned that are currently
     *  active. In any status mode, active and inactive events
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Event</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.EventList getEventsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code> EventList </code> that fall within the given
     *  range inclusive.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsInDateRange(org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsInDateRange(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the next upcoming events on this calendar.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  number the number of events
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getUpcomingEvents(long number)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getUpcomingEvents(number));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of events with the given location.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.NullArgumentException <code> locationId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
 
        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsByLocation(locationId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> EventList </code> at the given location where
     *  the given date range falls within the event dates
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> locationId,
     *          from</code>, or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocationOnDate(org.osid.id.Id locationId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsByLocationOnDate(locationId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> EventList </code> that fall within the given
     *  range inclusive at the given location.
     *
     *  <code> </code> In plenary mode, the returned list contains all
     *  known events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through this
     *  session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  locationId a location
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> locationId, from </code>
     *          , or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsByLocationInDateRange(org.osid.id.Id locationId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsByLocationInDateRange(locationId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of events with the given sponsor.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsor(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsBySponsor(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> EventList </code> with the given sponsor where
     *  the given date range falls within the event dates
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsorOnDate(org.osid.id.Id resourceId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsBySponsorOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> EventList </code> that fall within the given range
     *  inclusive at the given location.
     *
     *  In plenary mode, the returned list contains all known events
     *  or an error results. Otherwise, the returned list may contain
     *  only those events that are accessible through this session.
     *
     *  In normalized mode, recurring events appear as a single
     *  event. In denormalized mode, each element in the recurring
     *  series appears as a separate event.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered events are returned. In
     *  unsequestered mode, all events are returned.
     *
     *  @param  resourceId a sponsor
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> Event </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEventsBySponsorInDateRange(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEventsBySponsorInDateRange(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Events</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  events or an error results. Otherwise, the returned list
     *  may contain only those events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, events are returned that are currently
     *  effective.  In any effective mode, effective events and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Events</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList ret = getEventList();

        for (org.osid.calendaring.EventLookupSession session : getSessions()) {
            ret.addEventList(session.getEvents());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.event.FederatingEventList getEventList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.event.ParallelEventList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.event.CompositeEventList());
        }
    }
}
