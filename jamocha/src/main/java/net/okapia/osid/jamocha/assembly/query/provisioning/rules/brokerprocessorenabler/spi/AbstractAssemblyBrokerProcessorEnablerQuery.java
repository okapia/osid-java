//
// AbstractAssemblyBrokerProcessorEnablerQuery.java
//
//     A BrokerProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.brokerprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BrokerProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyBrokerProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.provisioning.rules.BrokerProcessorEnablerQuery,
               org.osid.provisioning.rules.BrokerProcessorEnablerQueryInspector,
               org.osid.provisioning.rules.BrokerProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBrokerProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBrokerProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the broker processor. 
     *
     *  @param  brokerProcessorId the broker processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerProcessorId(org.osid.id.Id brokerProcessorId, 
                                            boolean match) {
        getAssembler().addIdTerm(getRuledBrokerProcessorIdColumn(), brokerProcessorId, match);
        return;
    }


    /**
     *  Clears the broker processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerProcessorIdTerms() {
        getAssembler().clearTerms(getRuledBrokerProcessorIdColumn());
        return;
    }


    /**
     *  Gets the broker processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledBrokerProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledBrokerProcessorIdColumn()));
    }


    /**
     *  Gets the RuledBrokerProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerProcessorIdColumn() {
        return ("ruled_broker_processor_id");
    }


    /**
     *  Tests if a <code> BrokerProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the broker processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorQuery getRuledBrokerProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any broker processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any broker 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          broker processors 
     */

    @OSID @Override
    public void matchAnyRuledBrokerProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledBrokerProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the broker processor query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerProcessorTerms() {
        getAssembler().clearTerms(getRuledBrokerProcessorColumn());
        return;
    }


    /**
     *  Gets the broker processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorQueryInspector[] getRuledBrokerProcessorTerms() {
        return (new org.osid.provisioning.rules.BrokerProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledBrokerProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledBrokerProcessorColumn() {
        return ("ruled_broker_processor");
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this brokerProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  brokerProcessorEnablerRecordType a broker processor enabler record type 
     *  @return <code>true</code> if the brokerProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type brokerProcessorEnablerRecordType) {
        for (org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  brokerProcessorEnablerRecordType the broker processor enabler record type 
     *  @return the broker processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord getBrokerProcessorEnablerQueryRecord(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  brokerProcessorEnablerRecordType the broker processor enabler record type 
     *  @return the broker processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryInspectorRecord getBrokerProcessorEnablerQueryInspectorRecord(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(brokerProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param brokerProcessorEnablerRecordType the broker processor enabler record type
     *  @return the broker processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchOrderRecord getBrokerProcessorEnablerSearchOrderRecord(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(brokerProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this broker processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param brokerProcessorEnablerQueryRecord the broker processor enabler query record
     *  @param brokerProcessorEnablerQueryInspectorRecord the broker processor enabler query inspector
     *         record
     *  @param brokerProcessorEnablerSearchOrderRecord the broker processor enabler search order record
     *  @param brokerProcessorEnablerRecordType broker processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerQueryRecord</code>,
     *          <code>brokerProcessorEnablerQueryInspectorRecord</code>,
     *          <code>brokerProcessorEnablerSearchOrderRecord</code> or
     *          <code>brokerProcessorEnablerRecordTypebrokerProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addBrokerProcessorEnablerRecords(org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord brokerProcessorEnablerQueryRecord, 
                                      org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryInspectorRecord brokerProcessorEnablerQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchOrderRecord brokerProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type brokerProcessorEnablerRecordType) {

        addRecordType(brokerProcessorEnablerRecordType);

        nullarg(brokerProcessorEnablerQueryRecord, "broker processor enabler query record");
        nullarg(brokerProcessorEnablerQueryInspectorRecord, "broker processor enabler query inspector record");
        nullarg(brokerProcessorEnablerSearchOrderRecord, "broker processor enabler search odrer record");

        this.queryRecords.add(brokerProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(brokerProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(brokerProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
