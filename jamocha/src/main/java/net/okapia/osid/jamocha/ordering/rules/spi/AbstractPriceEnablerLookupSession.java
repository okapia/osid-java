//
// AbstractPriceEnablerLookupSession.java
//
//    A starter implementation framework for providing a PriceEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a PriceEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPriceEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.ordering.Store store = new net.okapia.osid.jamocha.nil.ordering.store.UnknownStore();
    

    /**
     *  Gets the <code>Store/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.store.getId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.store);
    }


    /**
     *  Sets the <code>Store</code>.
     *
     *  @param  store the store for this session
     *  @throws org.osid.NullArgumentException <code>store</code>
     *          is <code>null</code>
     */

    protected void setStore(org.osid.ordering.Store store) {
        nullarg(store, "store");
        this.store = store;
        return;
    }


    /**
     *  Tests if this user can perform <code>PriceEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPriceEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>PriceEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePriceEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>PriceEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPriceEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price enablers in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active price enablers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActivePriceEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive price enablers are returned by methods in
     *  this session.
     */
    
    @OSID @Override
    public void useAnyStatusPriceEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>PriceEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PriceEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PriceEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerId <code>Id</code> of the
     *          <code>PriceEnabler</code>
     *  @return the price enabler
     *  @throws org.osid.NotFoundException <code>priceEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>priceEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnabler getPriceEnabler(org.osid.id.Id priceEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ordering.rules.PriceEnablerList priceEnablers = getPriceEnablers()) {
            while (priceEnablers.hasNext()) {
                org.osid.ordering.rules.PriceEnabler priceEnabler = priceEnablers.getNextPriceEnabler();
                if (priceEnabler.getId().equals(priceEnablerId)) {
                    return (priceEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(priceEnablerId + " not found");
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  priceEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>PriceEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPriceEnablers()</code>.
     *
     *  @param  priceEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByIds(org.osid.id.IdList priceEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ordering.rules.PriceEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = priceEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPriceEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("price enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ordering.rules.priceenabler.LinkedPriceEnablerList(ret));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  price enabler genus <code>Type</code> which does not include
     *  price enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPriceEnablers()</code>.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.rules.priceenabler.PriceEnablerGenusFilterList(getPriceEnablers(), priceEnablerGenusType));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  price enabler genus <code>Type</code> and include any additional
     *  price enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPriceEnablers()</code>.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByParentGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPriceEnablersByGenusType(priceEnablerGenusType));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> containing the given
     *  price enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPriceEnablers()</code>.
     *
     *  @param  priceEnablerRecordType a priceEnabler record type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByRecordType(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.rules.priceenabler.PriceEnablerRecordFilterList(getPriceEnablers(), priceEnablerRecordType));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible
     *  through this session.
     *  
     *  In active mode, price enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive price enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PriceEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.rules.priceenabler.TemporalPriceEnablerFilterList(getPriceEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>PriceEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible
     *  through this session.
     *
     *  In active mode, price enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive price enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getPriceEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>PriceEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @return a list of <code>PriceEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.ordering.rules.PriceEnablerList getPriceEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the price enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of price enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.ordering.rules.PriceEnablerList filterPriceEnablersOnViews(org.osid.ordering.rules.PriceEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.ordering.rules.PriceEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.ordering.rules.priceenabler.ActivePriceEnablerFilterList(ret);
        }

        return (ret);
    }
}
