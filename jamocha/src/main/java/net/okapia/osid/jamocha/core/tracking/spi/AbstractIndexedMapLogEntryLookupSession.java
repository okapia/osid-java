//
// AbstractIndexedMapLogEntryLookupSession.java
//
//    A simple framework for providing a LogEntry lookup service
//    backed by a fixed collection of log entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a LogEntry lookup service backed by a
 *  fixed collection of log entries. The log entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some log entries may be compatible
 *  with more types than are indicated through these log entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>LogEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapLogEntryLookupSession
    extends AbstractMapLogEntryLookupSession
    implements org.osid.tracking.LogEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.tracking.LogEntry> logEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.tracking.LogEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.tracking.LogEntry> logEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.tracking.LogEntry>());


    /**
     *  Makes a <code>LogEntry</code> available in this session.
     *
     *  @param  logEntry a log entry
     *  @throws org.osid.NullArgumentException <code>logEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putLogEntry(org.osid.tracking.LogEntry logEntry) {
        super.putLogEntry(logEntry);

        this.logEntriesByGenus.put(logEntry.getGenusType(), logEntry);
        
        try (org.osid.type.TypeList types = logEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.logEntriesByRecord.put(types.getNextType(), logEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a log entry from this session.
     *
     *  @param logEntryId the <code>Id</code> of the log entry
     *  @throws org.osid.NullArgumentException <code>logEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeLogEntry(org.osid.id.Id logEntryId) {
        org.osid.tracking.LogEntry logEntry;
        try {
            logEntry = getLogEntry(logEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.logEntriesByGenus.remove(logEntry.getGenusType());

        try (org.osid.type.TypeList types = logEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.logEntriesByRecord.remove(types.getNextType(), logEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeLogEntry(logEntryId);
        return;
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  log entry genus <code>Type</code> which does not include
     *  log entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known log entries or an error results. Otherwise,
     *  the returned list may contain only those log entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  logEntryGenusType a log entry genus type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.logentry.ArrayLogEntryList(this.logEntriesByGenus.get(logEntryGenusType)));
    }


    /**
     *  Gets a <code>LogEntryList</code> containing the given
     *  log entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known log entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  log entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  logEntryRecordType a log entry record type 
     *  @return the returned <code>logEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByRecordType(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.logentry.ArrayLogEntryList(this.logEntriesByRecord.get(logEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.logEntriesByGenus.clear();
        this.logEntriesByRecord.clear();

        super.close();

        return;
    }
}
