//
// AbstractMapActivityRegistrationLookupSession
//
//    A simple framework for providing an ActivityRegistration lookup service
//    backed by a fixed collection of activity registrations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an ActivityRegistration lookup service backed by a
 *  fixed collection of activity registrations. The activity registrations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActivityRegistrations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapActivityRegistrationLookupSession
    extends net.okapia.osid.jamocha.course.registration.spi.AbstractActivityRegistrationLookupSession
    implements org.osid.course.registration.ActivityRegistrationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.registration.ActivityRegistration> activityRegistrations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.registration.ActivityRegistration>());


    /**
     *  Makes an <code>ActivityRegistration</code> available in this session.
     *
     *  @param  activityRegistration an activity registration
     *  @throws org.osid.NullArgumentException <code>activityRegistration<code>
     *          is <code>null</code>
     */

    protected void putActivityRegistration(org.osid.course.registration.ActivityRegistration activityRegistration) {
        this.activityRegistrations.put(activityRegistration.getId(), activityRegistration);
        return;
    }


    /**
     *  Makes an array of activity registrations available in this session.
     *
     *  @param  activityRegistrations an array of activity registrations
     *  @throws org.osid.NullArgumentException <code>activityRegistrations<code>
     *          is <code>null</code>
     */

    protected void putActivityRegistrations(org.osid.course.registration.ActivityRegistration[] activityRegistrations) {
        putActivityRegistrations(java.util.Arrays.asList(activityRegistrations));
        return;
    }


    /**
     *  Makes a collection of activity registrations available in this session.
     *
     *  @param  activityRegistrations a collection of activity registrations
     *  @throws org.osid.NullArgumentException <code>activityRegistrations<code>
     *          is <code>null</code>
     */

    protected void putActivityRegistrations(java.util.Collection<? extends org.osid.course.registration.ActivityRegistration> activityRegistrations) {
        for (org.osid.course.registration.ActivityRegistration activityRegistration : activityRegistrations) {
            this.activityRegistrations.put(activityRegistration.getId(), activityRegistration);
        }

        return;
    }


    /**
     *  Removes an ActivityRegistration from this session.
     *
     *  @param  activityRegistrationId the <code>Id</code> of the activity registration
     *  @throws org.osid.NullArgumentException <code>activityRegistrationId<code> is
     *          <code>null</code>
     */

    protected void removeActivityRegistration(org.osid.id.Id activityRegistrationId) {
        this.activityRegistrations.remove(activityRegistrationId);
        return;
    }


    /**
     *  Gets the <code>ActivityRegistration</code> specified by its <code>Id</code>.
     *
     *  @param  activityRegistrationId <code>Id</code> of the <code>ActivityRegistration</code>
     *  @return the activityRegistration
     *  @throws org.osid.NotFoundException <code>activityRegistrationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>activityRegistrationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistration getActivityRegistration(org.osid.id.Id activityRegistrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.ActivityRegistration activityRegistration = this.activityRegistrations.get(activityRegistrationId);
        if (activityRegistration == null) {
            throw new org.osid.NotFoundException("activityRegistration not found: " + activityRegistrationId);
        }

        return (activityRegistration);
    }


    /**
     *  Gets all <code>ActivityRegistrations</code>. In plenary mode, the returned
     *  list contains all known activityRegistrations or an error
     *  results. Otherwise, the returned list may contain only those
     *  activityRegistrations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ActivityRegistrations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.activityregistration.ArrayActivityRegistrationList(this.activityRegistrations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activityRegistrations.clear();
        super.close();
        return;
    }
}
