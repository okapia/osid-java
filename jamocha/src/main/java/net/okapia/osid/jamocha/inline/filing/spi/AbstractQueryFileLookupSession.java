//
// AbstractQueryFileLookupSession.java
//
//    An inline adapter that maps a FileLookupSession to
//    a FileQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a FileLookupSession to
 *  a FileQuerySession.
 */

public abstract class AbstractQueryFileLookupSession
    extends net.okapia.osid.jamocha.filing.spi.AbstractFileLookupSession
    implements org.osid.filing.FileLookupSession {

    private final org.osid.filing.FileQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryFileLookupSession.
     *
     *  @param querySession the underlying file query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryFileLookupSession(org.osid.filing.FileQuerySession querySession) {
        nullarg(querySession, "file query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Directory</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Directory Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDirectoryId() {
        return (this.session.getDirectoryId());
    }


    /**
     *  Gets the <code>Directory</code> associated with this 
     *  session.
     *
     *  @return the <code>Directory</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDirectory());
    }


    /**
     *  Tests if this user can perform <code>File</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFiles() {
        return (this.session.canSearchFiles());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include files in directories which are children
     *  of this directory in the directory hierarchy.
     */

    @OSID @Override
    public void useFederatedDirectoryView() {
        this.session.useFederatedDirectoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this directory only.
     */

    @OSID @Override
    public void useIsolatedDirectoryView() {
        this.session.useIsolatedDirectoryView();
        return;
    }
    
     
    /**
     *  Gets the <code>File</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>File</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>File</code> and
     *  retained for compatibility.
     *
     *  @param  fileId <code>Id</code> of the
     *          <code>File</code>
     *  @return the file
     *  @throws org.osid.NotFoundException <code>fileId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>fileId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.File getFile(org.osid.id.Id fileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.FileQuery query = getQuery();
        query.matchId(fileId, true);
        org.osid.filing.FileList files = this.session.getFilesByQuery(query);
        if (files.hasNext()) {
            return (files.getNextFile());
        } 
        
        throw new org.osid.NotFoundException(fileId + " not found");
    }


    /**
     *  Gets a <code>FileList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  files specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Files</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  fileIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>File</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>fileIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByIds(org.osid.id.IdList fileIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.FileQuery query = getQuery();

        try (org.osid.id.IdList ids = fileIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getFilesByQuery(query));
    }


    /**
     *  Gets a <code>FileList</code> corresponding to the given
     *  file genus <code>Type</code> which does not include
     *  files of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fileGenusType a file genus type 
     *  @return the returned <code>File</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fileGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByGenusType(org.osid.type.Type fileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.FileQuery query = getQuery();
        query.matchGenusType(fileGenusType, true);
        return (this.session.getFilesByQuery(query));
    }


    /**
     *  Gets a <code>FileList</code> corresponding to the given
     *  file genus <code>Type</code> and include any additional
     *  files with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fileGenusType a file genus type 
     *  @return the returned <code>File</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fileGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByParentGenusType(org.osid.type.Type fileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.FileQuery query = getQuery();
        query.matchParentGenusType(fileGenusType, true);
        return (this.session.getFilesByQuery(query));
    }


    /**
     *  Gets a <code>FileList</code> containing the given
     *  file record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fileRecordType a file record type 
     *  @return the returned <code>File</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fileRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByRecordType(org.osid.type.Type fileRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.FileQuery query = getQuery();
        query.matchRecordType(fileRecordType, true);
        return (this.session.getFilesByQuery(query));
    }

    
    /**
     *  Gets all <code>Files</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Files</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFiles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.filing.FileQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getFilesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.filing.FileQuery getQuery() {
        org.osid.filing.FileQuery query = this.session.getFileQuery();
        
        return (query);
    }
}
