//
// AbstractCalendarLookupSession.java
//
//    A starter implementation framework for providing a Calendar
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Calendar
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCalendars(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCalendarLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.CalendarLookupSession {

    private boolean pedantic = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    


    /**
     *  Tests if this user can perform <code>Calendar</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCalendars() {
        return (true);
    }


    /**
     *  A complete view of the <code>Calendar</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCalendarView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Calendar</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCalendarView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Calendar</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Calendar</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Calendar</code> and
     *  retained for compatibility.
     *
     *  @param  calendarId <code>Id</code> of the
     *          <code>Calendar</code>
     *  @return the calendar
     *  @throws org.osid.NotFoundException <code>calendarId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>calendarId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.CalendarList calendars = getCalendars()) {
            while (calendars.hasNext()) {
                org.osid.calendaring.Calendar calendar = calendars.getNextCalendar();
                if (calendar.getId().equals(calendarId)) {
                    return (calendar);
                }
            }
        } 

        throw new org.osid.NotFoundException(calendarId + " not found");
    }


    /**
     *  Gets a <code>CalendarList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  calendars specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Calendars</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCalendars()</code>.
     *
     *  @param  calendarIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Calendar</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>calendarIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByIds(org.osid.id.IdList calendarIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.Calendar> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = calendarIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCalendar(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("calendar " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.calendar.LinkedCalendarList(ret));
    }


    /**
     *  Gets a <code>CalendarList</code> corresponding to the given
     *  calendar genus <code>Type</code> which does not include
     *  calendars of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCalendars()</code>.
     *
     *  @param  calendarGenusType a calendar genus type 
     *  @return the returned <code>Calendar</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>calendarGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByGenusType(org.osid.type.Type calendarGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.calendar.CalendarGenusFilterList(getCalendars(), calendarGenusType));
    }


    /**
     *  Gets a <code>CalendarList</code> corresponding to the given
     *  calendar genus <code>Type</code> and include any additional
     *  calendars with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCalendars()</code>.
     *
     *  @param  calendarGenusType a calendar genus type 
     *  @return the returned <code>Calendar</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>calendarGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByParentGenusType(org.osid.type.Type calendarGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCalendarsByGenusType(calendarGenusType));
    }


    /**
     *  Gets a <code>CalendarList</code> containing the given
     *  calendar record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCalendars()</code>.
     *
     *  @param  calendarRecordType a calendar record type 
     *  @return the returned <code>Calendar</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByRecordType(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.calendar.CalendarRecordFilterList(getCalendars(), calendarRecordType));
    }


    /**
     *  Gets a <code>CalendarList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known calendars or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  calendars that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Calendar</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.calendaring.calendar.CalendarProviderFilterList(getCalendars(), resourceId));
    }


    /**
     *  Gets all <code>Calendars</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Calendars</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.CalendarList getCalendars()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the calendar list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of calendars
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.CalendarList filterCalendarsOnViews(org.osid.calendaring.CalendarList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
