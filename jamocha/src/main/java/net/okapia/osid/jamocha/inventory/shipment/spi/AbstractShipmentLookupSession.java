//
// AbstractShipmentLookupSession.java
//
//    A starter implementation framework for providing a Shipment
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Shipment
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getShipments(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractShipmentLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();
    

    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Shipment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupShipments() {
        return (true);
    }


    /**
     *  A complete view of the <code>Shipment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeShipmentView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Shipment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryShipmentView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include shipments in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Shipment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Shipment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Shipment</code> and
     *  retained for compatibility.
     *
     *  @param  shipmentId <code>Id</code> of the
     *          <code>Shipment</code>
     *  @return the shipment
     *  @throws org.osid.NotFoundException <code>shipmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>shipmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.Shipment getShipment(org.osid.id.Id shipmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.shipment.ShipmentList shipments = getShipments()) {
            while (shipments.hasNext()) {
                org.osid.inventory.shipment.Shipment shipment = shipments.getNextShipment();
                if (shipment.getId().equals(shipmentId)) {
                    return (shipment);
                }
            }
        } 

        throw new org.osid.NotFoundException(shipmentId + " not found");
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  shipments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Shipments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getShipments()</code>.
     *
     *  @param  shipmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByIds(org.osid.id.IdList shipmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.shipment.Shipment> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = shipmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getShipment(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("shipment " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.shipment.shipment.LinkedShipmentList(ret));
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  shipment genus <code>Type</code> which does not include
     *  shipments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getShipments()</code>.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentGenusFilterList(getShipments(), shipmentGenusType));
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  shipment genus <code>Type</code> and include any additional
     *  shipments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getShipments()</code>.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByParentGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getShipmentsByGenusType(shipmentGenusType));
    }


    /**
     *  Gets a <code>ShipmentList</code> containing the given
     *  shipment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getShipments()</code>.
     *
     *  @param  shipmentRecordType a shipment record type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByRecordType(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentRecordFilterList(getShipments(), shipmentRecordType));
    }


    /**
     *  Gets a <code>ShipmentList</code> received between the given
     *  date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>ShipmentList</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentFilterList(new DateFilter(from, to), getShipments()));
    }


    /**
     *  Gets a <code>ShipmentList</code> for to the given stock.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  stockId a stock <code>Id</code> 
     *  @return the returned <code>ShipmentList</code> list 
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.shipment.Shipment> ret = new java.util.ArrayList<>();

        try (org.osid.inventory.shipment.ShipmentList shipments = getShipments()) {
            while (shipments.hasNext()) {
                org.osid.inventory.shipment.Shipment shipment = shipments.getNextShipment();
                try (org.osid.inventory.shipment.EntryList entries = shipment.getEntries()) {
                    while (entries.hasNext()) {
                        org.osid.inventory.shipment.Entry entry = entries.getNextEntry();
                        if (entry.getStockId().equals(stockId)) {
                            ret.add(shipment);
                        }
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.shipment.shipment.LinkedShipmentList(ret));
    }


    /**
     *  Gets a <code>ShipmentList</code> for the given stock and received 
     *  between the given date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  stockId a stock <code>Id</code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>ShipmentList</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>stockId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStockOnDate(org.osid.id.Id stockId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentFilterList(new DateFilter(from, to), getShipmentsForStock(stockId)));
    }        


    /**
     *  Gets a <code>ShipmentList</code> from to the given source.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>ShipmentList</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentFilterList(new SourceFilter(resourceId), getShipments()));
    }        


    /**
     *  Gets a <code>ShipmentList</code> from the given source and
     *  received between the given date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>ShipmentList</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code>, </code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySourceOnDate(org.osid.id.Id resourceId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentFilterList(new DateFilter(from, to), getShipmentsBySource(resourceId)));
    }
        
    
    /**
     *  Gets all <code>Shipments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Shipments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inventory.shipment.ShipmentList getShipments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the shipment list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of shipments
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inventory.shipment.ShipmentList filterShipmentsOnViews(org.osid.inventory.shipment.ShipmentList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;

        
        /**
         *  Constructs a new <date>DateFilter</date>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <date>from</date>
         *          or <code>to</code> is <date>null</date>
         */

        public DateFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to   = to;
;
            return;
        }


        /**
         *  Used by the ShipmentFilterList to filter the shipment list based
         *  on date.
         *
         *  @param shipment the shipment
         *  @return <date>true</date> to pass the shipment,
         *          <date>false</date> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.shipment.Shipment shipment) {
            if (shipment.getDate().isLess(this.from)) {
                return (false);
            }

            if (shipment.getDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }    


    public static class SourceFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.shipment.shipment.ShipmentFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>SourceFilter</code>.
         *
         *  @param resourceId the source to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public SourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ShipmentFilterList to filter the 
         *  shipment list based on source.
         *
         *  @param shipment the shipment
         *  @return <code>true</code> to pass the shipment,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.shipment.Shipment shipment) {
            return (shipment.getSourceId().equals(this.resourceId));
        }
    }
}
