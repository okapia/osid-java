//
// AbstractUnknownEntry.java
//
//     Defines an unknown Entry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.billing.entry.spi;


/**
 *  Defines an unknown <code>Entry</code>.
 */

public abstract class AbstractUnknownEntry
    extends net.okapia.osid.jamocha.billing.entry.spi.AbstractEntry
    implements org.osid.billing.Entry {

    protected static final String OBJECT = "osid.billing.Entry";


    /**
     *  Constructs a new AbstractUnknown<code>Entry</code>.
     */

    public AbstractUnknownEntry() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setCustomer(new net.okapia.osid.jamocha.nil.billing.customer.UnknownCustomer());
        setItem(new net.okapia.osid.jamocha.nil.billing.item.UnknownItem());
        setPeriod(new net.okapia.osid.jamocha.nil.billing.period.UnknownPeriod());
        setAmount(net.okapia.osid.primordium.financials.USDCurrency.valueOf(0));

        return;
    }


    /**
     *  Constructs a new AbstractUnknown<code>Entry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownEntry(boolean optional) {
        this();
        return;
    }
}
