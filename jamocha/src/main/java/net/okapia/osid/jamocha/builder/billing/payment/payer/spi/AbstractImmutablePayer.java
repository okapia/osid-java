//
// AbstractImmutablePayer.java
//
//     Wraps a mutable Payer to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.payer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Payer</code> to hide modifiers. This
 *  wrapper provides an immutized Payer from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying payer whose state changes are visible.
 */

public abstract class AbstractImmutablePayer
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.billing.payment.Payer {

    private final org.osid.billing.payment.Payer payer;


    /**
     *  Constructs a new <code>AbstractImmutablePayer</code>.
     *
     *  @param payer the payer to immutablize
     *  @throws org.osid.NullArgumentException <code>payer</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePayer(org.osid.billing.payment.Payer payer) {
        super(payer);
        this.payer = payer;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> representing the billing contact. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.payer.getResourceId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.payer.getResource());
    }


    /**
     *  Tests if this payer is linked directly to a customer account. 
     *
     *  @return <code> true </code> if this payer is linked to a customer, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCustomer() {
        return (this.payer.hasCustomer());
    }


    /**
     *  Gets the customer <code> Id. </code> 
     *
     *  @return the customer <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCustomer() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.payer.getCustomerId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.IllegalStateException <code> hasCustomer() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        return (this.payer.getCustomer());
    }


    /**
     *  Tests if this payer uses the customer activity account for payments. 
     *  If <code> usesActivity() </code> is true, then <code> usesCash(), 
     *  </code> <code> hasCreditCard(), </code> and <code> hasBankAccount() 
     *  </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if this payer uses the customer activity, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasCustomer() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean usesActivity() {
        return (this.payer.usesActivity());
    }


    /**
     *  Tests if this payer uses cash for payments. 
     *
     *  @return <code> true </code> if this payer uses the cash, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean usesCash() {
        return (this.payer.usesCash());
    }


    /**
     *  Tests if this payer pays by credit card. If <code> hasCreditCard() 
     *  </code> is true, then <code> usesActivity(), </code> <code> 
     *  usesCash(), </code> <code> hasCreditCard(), </code> and <code> 
     *  hasBankAccount() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if this payer pays by credit card, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCreditCard() {
        return (this.payer.hasCreditCard());
    }


    /**
     *  Gets the credit card number. 
     *
     *  @return the credit card number 
     *  @throws org.osid.IllegalStateException <code> hasCreditCard() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public String getCreditCardNumber() {
        return (this.payer.getCreditCardNumber());
    }


    /**
     *  Gets the credit card expiration date. 
     *
     *  @return the expiration date 
     *  @throws org.osid.IllegalStateException <code> hasCreditCard() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreditCardExpiration() {
        return (this.payer.getCreditCardExpiration());
    }


    /**
     *  Gets the credit card security code. 
     *
     *  @return the credit card security code 
     *  @throws org.osid.IllegalStateException <code> hasCreditCard() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public String getCreditCardCode() {
        return (this.payer.getCreditCardCode());
    }


    /**
     *  Tests if this payer pays by bank account If <code> hasBankAccount() 
     *  </code> is true, then <code> usesActivity(), </code> <code> 
     *  usesCash(), </code> <code> hasCreditCard(), </code> and <code> 
     *  hasCreditCard() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if this payer pays by bank account, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasBankAccount() {
        return (this.payer.hasBankAccount());
    }


    /**
     *  Gets the bank routing number. 
     *
     *  @return the bank routing number number 
     *  @throws org.osid.IllegalStateException <code> hasBankAccount() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public String getBankRoutingNumber() {
        return (this.payer.getBankRoutingNumber());
    }


    /**
     *  Gets the bank account number. 
     *
     *  @return the bank account number 
     *  @throws org.osid.IllegalStateException <code> hasBankAccount() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public String getBankAccountNumber() {
        return (this.payer.getBankAccountNumber());
    }


    /**
     *  Gets the payer record corresponding to the given <code> Payer </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> payerRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(payerRecordType) </code> is <code> true </code> . 
     *
     *  @param  payerRecordType the type of payer record to retrieve 
     *  @return the payer record 
     *  @throws org.osid.NullArgumentException <code> payerRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(payerRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerRecord getPayerRecord(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException {

        return (this.payer.getPayerRecord(payerRecordType));
    }
}

