//
// SearchOrderTerm.java
//
//     Provides a utility to hold search orders.
//
//
// Tom Coppeto
// Okapia
// 20 April 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Provides a holder for search query terms.
 */

public final class SearchOrderTerm {

    private final String column;
    private final org.osid.SearchOrderStyle style;
    private final org.osid.id.Id qualifierId;


    /**
     *  Constructs a new <code>SearchOrderTerm</code>.
     *
     *  @param column the column
     *  @param style search order style
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>style</code> is <code>null</code>
     */

    public SearchOrderTerm(String column, org.osid.SearchOrderStyle style) {
        nullarg(column, "column");
        nullarg(style, "search order style");

        this.column = column;
        this.style = style;
        this.qualifierId = null;

        return;
    }


    /**
     *  Constructs a new <code>SearchOrderTerm</code>.
     *
     *  @param column the column
     *  @param qualifierId a qualifiying Id     
     *  @param style search order style
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>qualifierId</code> or <code>style</code> is
     *          <code>null</code>
     */

    public SearchOrderTerm(String column, org.osid.id.Id qualifierId, 
                           org.osid.SearchOrderStyle style) {

        nullarg(column, "column");
        nullarg(qualifierId, "qualifier Id");
        nullarg(style, "search order style");

        this.column = column;
        this.style = style;
        this.qualifierId = qualifierId;

        return;
    }


    /**
     *  Gets the query column.
     *
     *  @return the column
     */

    public String getColumn() {
        return (this.column);
    }


    /**
     *  Gets the qualifier.
     *
     *  @return the column
     */

    public org.osid.id.Id getQualifier() {
        return (this.qualifierId);
    }


    /**
     *  Gets the search order style.
     *
     *  @return the search order style
     */

    public org.osid.SearchOrderStyle getStyle() {
        return (this.style);
    }
}
