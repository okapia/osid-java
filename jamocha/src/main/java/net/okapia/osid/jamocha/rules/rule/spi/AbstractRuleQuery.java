//
// AbstractRuleQuery.java
//
//     A template for making a Rule Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.rule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for rules.
 */

public abstract class AbstractRuleQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.rules.RuleQuery {

    private final java.util.Collection<org.osid.rules.records.RuleQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the engine <code> Id </code> for this query to match rules 
     *  assigned to engines. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given rule query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a rule implementing the requested record.
     *
     *  @param ruleRecordType a rule record type
     *  @return the rule query record
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ruleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.RuleQueryRecord getRuleQueryRecord(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.records.RuleQueryRecord record : this.records) {
            if (record.implementsRecordType(ruleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ruleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this rule query. 
     *
     *  @param ruleQueryRecord rule query record
     *  @param ruleRecordType rule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRuleQueryRecord(org.osid.rules.records.RuleQueryRecord ruleQueryRecord, 
                                          org.osid.type.Type ruleRecordType) {

        addRecordType(ruleRecordType);
        nullarg(ruleQueryRecord, "rule query record");
        this.records.add(ruleQueryRecord);        
        return;
    }
}
