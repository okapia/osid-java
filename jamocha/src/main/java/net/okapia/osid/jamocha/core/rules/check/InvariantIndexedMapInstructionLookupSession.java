//
// InvariantIndexedMapInstructionLookupSession
//
//    Implements an Instruction lookup service backed by a fixed
//    collection of instructions indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check;


/**
 *  Implements an Instruction lookup service backed by a fixed
 *  collection of instructions. The instructions are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some instructions may be compatible
 *  with more types than are indicated through these instruction
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapInstructionLookupSession
    extends net.okapia.osid.jamocha.core.rules.check.spi.AbstractIndexedMapInstructionLookupSession
    implements org.osid.rules.check.InstructionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapInstructionLookupSession} using an
     *  array of instructions.
     *
     *  @param engine the engine
     *  @param instructions an array of instructions
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code instructions} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapInstructionLookupSession(org.osid.rules.Engine engine,
                                                    org.osid.rules.check.Instruction[] instructions) {

        setEngine(engine);
        putInstructions(instructions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapInstructionLookupSession} using a
     *  collection of instructions.
     *
     *  @param engine the engine
     *  @param instructions a collection of instructions
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code instructions} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapInstructionLookupSession(org.osid.rules.Engine engine,
                                                    java.util.Collection<? extends org.osid.rules.check.Instruction> instructions) {

        setEngine(engine);
        putInstructions(instructions);
        return;
    }
}
