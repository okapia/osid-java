//
// InvariantMapValueLookupSession
//
//    Implements a Value lookup service backed by a fixed collection of
//    values.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration;


/**
 *  Implements a Value lookup service backed by a fixed
 *  collection of values. The values are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapValueLookupSession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractMapValueLookupSession
    implements org.osid.configuration.ValueLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapValueLookupSession</code> with no
     *  values.
     *  
     *  @param configuration the configuration
     *  @throws org.osid.NullArgumnetException {@code configuration} is
     *          {@code null}
     */

    public InvariantMapValueLookupSession(org.osid.configuration.Configuration configuration) {
        setConfiguration(configuration);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapValueLookupSession</code> with a single
     *  value.
     *  
     *  @param configuration the configuration
     *  @param value a single value
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code value} is <code>null</code>
     */

      public InvariantMapValueLookupSession(org.osid.configuration.Configuration configuration,
                                               org.osid.configuration.Value value) {
        this(configuration);
        putValue(value);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapValueLookupSession</code> using an array
     *  of values.
     *  
     *  @param configuration the configuration
     *  @param values an array of values
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code values} is <code>null</code>
     */

      public InvariantMapValueLookupSession(org.osid.configuration.Configuration configuration,
                                               org.osid.configuration.Value[] values) {
        this(configuration);
        putValues(values);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapValueLookupSession</code> using a
     *  collection of values.
     *
     *  @param configuration the configuration
     *  @param values a collection of values
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code values} is <code>null</code>
     */

      public InvariantMapValueLookupSession(org.osid.configuration.Configuration configuration,
                                               java.util.Collection<? extends org.osid.configuration.Value> values) {
        this(configuration);
        putValues(values);
        return;
    }
}
