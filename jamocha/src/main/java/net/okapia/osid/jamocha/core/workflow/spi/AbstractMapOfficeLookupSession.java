//
// AbstractMapOfficeLookupSession
//
//    A simple framework for providing an Office lookup service
//    backed by a fixed collection of offices.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Office lookup service backed by a
 *  fixed collection of offices. The offices are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Offices</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOfficeLookupSession
    extends net.okapia.osid.jamocha.workflow.spi.AbstractOfficeLookupSession
    implements org.osid.workflow.OfficeLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.workflow.Office> offices = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.workflow.Office>());


    /**
     *  Makes an <code>Office</code> available in this session.
     *
     *  @param  office an office
     *  @throws org.osid.NullArgumentException <code>office<code>
     *          is <code>null</code>
     */

    protected void putOffice(org.osid.workflow.Office office) {
        this.offices.put(office.getId(), office);
        return;
    }


    /**
     *  Makes an array of offices available in this session.
     *
     *  @param  offices an array of offices
     *  @throws org.osid.NullArgumentException <code>offices<code>
     *          is <code>null</code>
     */

    protected void putOffices(org.osid.workflow.Office[] offices) {
        putOffices(java.util.Arrays.asList(offices));
        return;
    }


    /**
     *  Makes a collection of offices available in this session.
     *
     *  @param  offices a collection of offices
     *  @throws org.osid.NullArgumentException <code>offices<code>
     *          is <code>null</code>
     */

    protected void putOffices(java.util.Collection<? extends org.osid.workflow.Office> offices) {
        for (org.osid.workflow.Office office : offices) {
            this.offices.put(office.getId(), office);
        }

        return;
    }


    /**
     *  Removes an Office from this session.
     *
     *  @param  officeId the <code>Id</code> of the office
     *  @throws org.osid.NullArgumentException <code>officeId<code> is
     *          <code>null</code>
     */

    protected void removeOffice(org.osid.id.Id officeId) {
        this.offices.remove(officeId);
        return;
    }


    /**
     *  Gets the <code>Office</code> specified by its <code>Id</code>.
     *
     *  @param  officeId <code>Id</code> of the <code>Office</code>
     *  @return the office
     *  @throws org.osid.NotFoundException <code>officeId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>officeId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.workflow.Office office = this.offices.get(officeId);
        if (office == null) {
            throw new org.osid.NotFoundException("office not found: " + officeId);
        }

        return (office);
    }


    /**
     *  Gets all <code>Offices</code>. In plenary mode, the returned
     *  list contains all known offices or an error
     *  results. Otherwise, the returned list may contain only those
     *  offices that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Offices</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.office.ArrayOfficeList(this.offices.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offices.clear();
        super.close();
        return;
    }
}
