//
// AbstractFederatingValueEnablerLookupSession.java
//
//     An abstract federating adapter for a ValueEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ValueEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingValueEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.configuration.rules.ValueEnablerLookupSession>
    implements org.osid.configuration.rules.ValueEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();


    /**
     *  Constructs a new <code>AbstractFederatingValueEnablerLookupSession</code>.
     */

    protected AbstractFederatingValueEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.configuration.rules.ValueEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>ValueEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupValueEnablers() {
        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            if (session.canLookupValueEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ValueEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeValueEnablerView() {
        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            session.useComparativeValueEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ValueEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryValueEnablerView() {
        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            session.usePlenaryValueEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include value enablers in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            session.useFederatedConfigurationView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            session.useIsolatedConfigurationView();
        }

        return;
    }


    /**
     *  Only active value enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveValueEnablerView() {
        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            session.useActiveValueEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive value enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusValueEnablerView() {
        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            session.useAnyStatusValueEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>ValueEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ValueEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ValueEnabler</code>
     *  and retained for compatibility.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerId <code>Id</code> of the
     *          <code>ValueEnabler</code>
     *  @return the value enabler
     *  @throws org.osid.NotFoundException <code>valueEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>valueEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnabler getValueEnabler(org.osid.id.Id valueEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            try {
                return (session.getValueEnabler(valueEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(valueEnablerId + " not found");
    }


    /**
     *  Gets a <code>ValueEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  valueEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>ValueEnablers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByIds(org.osid.id.IdList valueEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.configuration.rules.valueenabler.MutableValueEnablerList ret = new net.okapia.osid.jamocha.configuration.rules.valueenabler.MutableValueEnablerList();

        try (org.osid.id.IdList ids = valueEnablerIds) {
            while (ids.hasNext()) {
                ret.addValueEnabler(getValueEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ValueEnablerList</code> corresponding to the given
     *  value enabler genus <code>Type</code> which does not include
     *  value enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known value
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerGenusType a valueEnabler genus type 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByGenusType(org.osid.type.Type valueEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.FederatingValueEnablerList ret = getValueEnablerList();

        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            ret.addValueEnablerList(session.getValueEnablersByGenusType(valueEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ValueEnablerList</code> corresponding to the given
     *  value enabler genus <code>Type</code> and include any additional
     *  value enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerGenusType a valueEnabler genus type 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByParentGenusType(org.osid.type.Type valueEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.FederatingValueEnablerList ret = getValueEnablerList();

        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            ret.addValueEnablerList(session.getValueEnablersByParentGenusType(valueEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ValueEnablerList</code> containing the given
     *  value enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known value
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  valueEnablerRecordType a valueEnabler record type 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByRecordType(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.FederatingValueEnablerList ret = getValueEnablerList();

        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            ret.addValueEnablerList(session.getValueEnablersByRecordType(valueEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ValueEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible
     *  through this session.
     *  
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ValueEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.FederatingValueEnablerList ret = getValueEnablerList();

        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            ret.addValueEnablerList(session.getValueEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>ValueEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  value enablers or an error results. Otherwise, the returned list
     *  may contain only those value enablers that are accessible
     *  through this session.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.FederatingValueEnablerList ret = getValueEnablerList();

        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            ret.addValueEnablerList(session.getValueEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ValueEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known value
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those value enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, value enablers are returned that are currently
     *  active. In any status mode, active and inactive value enablers
     *  are returned.
     *
     *  @return a list of <code>ValueEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.FederatingValueEnablerList ret = getValueEnablerList();

        for (org.osid.configuration.rules.ValueEnablerLookupSession session : getSessions()) {
            ret.addValueEnablerList(session.getValueEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.FederatingValueEnablerList getValueEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.ParallelValueEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.rules.valueenabler.CompositeValueEnablerList());
        }
    }
}
