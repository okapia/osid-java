//
// InvariantMapProxyProgramLookupSession
//
//    Implements a Program lookup service backed by a fixed
//    collection of programs. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a Program lookup service backed by a fixed
 *  collection of programs. The programs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyProgramLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractMapProgramLookupSession
    implements org.osid.course.program.ProgramLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProgramLookupSession} with no
     *  programs.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyProgramLookupSession} with a single
     *  program.
     *
     *  @param courseCatalog the course catalog
     *  @param program a single program
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code program} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Program program, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putProgram(program);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyProgramLookupSession} using
     *  an array of programs.
     *
     *  @param courseCatalog the course catalog
     *  @param programs an array of programs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programs} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Program[] programs, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putPrograms(programs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProgramLookupSession} using a
     *  collection of programs.
     *
     *  @param courseCatalog the course catalog
     *  @param programs a collection of programs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programs} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.program.Program> programs,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putPrograms(programs);
        return;
    }
}
