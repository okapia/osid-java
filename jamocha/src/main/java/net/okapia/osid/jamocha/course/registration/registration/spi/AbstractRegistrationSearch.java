//
// AbstractRegistrationSearch.java
//
//     A template for making a Registration Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing registration searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRegistrationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.registration.RegistrationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.RegistrationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.registration.RegistrationSearchOrder registrationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of registrations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  registrationIds list of registrations
     *  @throws org.osid.NullArgumentException
     *          <code>registrationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRegistrations(org.osid.id.IdList registrationIds) {
        while (registrationIds.hasNext()) {
            try {
                this.ids.add(registrationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRegistrations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of registration Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRegistrationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  registrationSearchOrder registration search order 
     *  @throws org.osid.NullArgumentException
     *          <code>registrationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>registrationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRegistrationResults(org.osid.course.registration.RegistrationSearchOrder registrationSearchOrder) {
	this.registrationSearchOrder = registrationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.registration.RegistrationSearchOrder getRegistrationSearchOrder() {
	return (this.registrationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given registration search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a registration implementing the requested record.
     *
     *  @param registrationSearchRecordType a registration search record
     *         type
     *  @return the registration search record
     *  @throws org.osid.NullArgumentException
     *          <code>registrationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(registrationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationSearchRecord getRegistrationSearchRecord(org.osid.type.Type registrationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.registration.records.RegistrationSearchRecord record : this.records) {
            if (record.implementsRecordType(registrationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(registrationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this registration search. 
     *
     *  @param registrationSearchRecord registration search record
     *  @param registrationSearchRecordType registration search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRegistrationSearchRecord(org.osid.course.registration.records.RegistrationSearchRecord registrationSearchRecord, 
                                           org.osid.type.Type registrationSearchRecordType) {

        addRecordType(registrationSearchRecordType);
        this.records.add(registrationSearchRecord);        
        return;
    }
}
