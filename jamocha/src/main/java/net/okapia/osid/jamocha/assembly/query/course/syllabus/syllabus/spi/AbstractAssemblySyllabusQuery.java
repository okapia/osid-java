//
// AbstractAssemblySyllabusQuery.java
//
//     A SyllabusQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.syllabus.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SyllabusQuery that stores terms.
 */

public abstract class AbstractAssemblySyllabusQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.course.syllabus.SyllabusQuery,
               org.osid.course.syllabus.SyllabusQueryInspector,
               org.osid.course.syllabus.SyllabusSearchOrder {

    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySyllabusQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySyllabusQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a course <code> Id. </code> 
     *
     *  @param  courseId a curse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> terms. 
     *
     *  @return the course <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the course. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseColumn(), style);
        return;
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Sets a module <code> Id. </code> 
     *
     *  @param  moduleId a module <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> moduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModuleId(org.osid.id.Id moduleId, boolean match) {
        getAssembler().addIdTerm(getModuleIdColumn(), moduleId, match);
        return;
    }


    /**
     *  Clears the module <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModuleIdTerms() {
        getAssembler().clearTerms(getModuleIdColumn());
        return;
    }


    /**
     *  Gets the module <code> Id </code> terms. 
     *
     *  @return the module <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModuleIdTerms() {
        return (getAssembler().getIdTerms(getModuleIdColumn()));
    }


    /**
     *  Gets the ModuleId column name.
     *
     * @return the column name
     */

    protected String getModuleIdColumn() {
        return ("module_id");
    }


    /**
     *  Tests if a <code> ModuleQuery </code> is available. 
     *
     *  @return <code> true </code> if a module query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a module query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the module query 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuery getModuleQuery() {
        throw new org.osid.UnimplementedException("supportsModuleQuery() is false");
    }


    /**
     *  Matches any module. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyModule(boolean match) {
        getAssembler().addIdWildcardTerm(getModuleColumn(), match);
        return;
    }


    /**
     *  Clears the module terms. 
     */

    @OSID @Override
    public void clearModuleTerms() {
        getAssembler().clearTerms(getModuleColumn());
        return;
    }


    /**
     *  Gets the module terms. 
     *
     *  @return the module terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQueryInspector[] getModuleTerms() {
        return (new org.osid.course.syllabus.ModuleQueryInspector[0]);
    }


    /**
     *  Gets the Module column name.
     *
     * @return the column name
     */

    protected String getModuleColumn() {
        return ("module");
    }


    /**
     *  Sets the docet <code> Id </code> for this query to match syllabi 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this syllabus supports the given record
     *  <code>Type</code>.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return <code>true</code> if the syllabusRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type syllabusRecordType) {
        for (org.osid.course.syllabus.records.SyllabusQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  syllabusRecordType the syllabus record type 
     *  @return the syllabus query record 
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(syllabusRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusQueryRecord getSyllabusQueryRecord(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.SyllabusQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(syllabusRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  syllabusRecordType the syllabus record type 
     *  @return the syllabus query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(syllabusRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusQueryInspectorRecord getSyllabusQueryInspectorRecord(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.SyllabusQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(syllabusRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param syllabusRecordType the syllabus record type
     *  @return the syllabus search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(syllabusRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusSearchOrderRecord getSyllabusSearchOrderRecord(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.SyllabusSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(syllabusRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this syllabus. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param syllabusQueryRecord the syllabus query record
     *  @param syllabusQueryInspectorRecord the syllabus query inspector
     *         record
     *  @param syllabusSearchOrderRecord the syllabus search order record
     *  @param syllabusRecordType syllabus record type
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusQueryRecord</code>,
     *          <code>syllabusQueryInspectorRecord</code>,
     *          <code>syllabusSearchOrderRecord</code> or
     *          <code>syllabusRecordTypesyllabus</code> is
     *          <code>null</code>
     */
            
    protected void addSyllabusRecords(org.osid.course.syllabus.records.SyllabusQueryRecord syllabusQueryRecord, 
                                      org.osid.course.syllabus.records.SyllabusQueryInspectorRecord syllabusQueryInspectorRecord, 
                                      org.osid.course.syllabus.records.SyllabusSearchOrderRecord syllabusSearchOrderRecord, 
                                      org.osid.type.Type syllabusRecordType) {

        addRecordType(syllabusRecordType);

        nullarg(syllabusQueryRecord, "syllabus query record");
        nullarg(syllabusQueryInspectorRecord, "syllabus query inspector record");
        nullarg(syllabusSearchOrderRecord, "syllabus search odrer record");

        this.queryRecords.add(syllabusQueryRecord);
        this.queryInspectorRecords.add(syllabusQueryInspectorRecord);
        this.searchOrderRecords.add(syllabusSearchOrderRecord);
        
        return;
    }
}
