//
// InvariantIndexedMapProxyAcademyLookupSession
//
//    Implements an Academy lookup service backed by a fixed
//    collection of academies indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements a Academy lookup service backed by a fixed
 *  collection of academies. The academies are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some academies may be compatible
 *  with more types than are indicated through these academy
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyAcademyLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractIndexedMapAcademyLookupSession
    implements org.osid.recognition.AcademyLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAcademyLookupSession}
     *  using an array of academies.
     *
     *  @param academies an array of academies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academies} or
     *          {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAcademyLookupSession(org.osid.recognition.Academy[] academies, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAcademies(academies);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAcademyLookupSession}
     *  using a collection of academies.
     *
     *  @param academies a collection of academies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academies} or
     *          {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAcademyLookupSession(java.util.Collection<? extends org.osid.recognition.Academy> academies,
                                                         org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAcademies(academies);
        return;
    }
}
