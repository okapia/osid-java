//
// AbstractAdapterActivityRegistrationLookupSession.java
//
//    An ActivityRegistration lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.registration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An ActivityRegistration lookup session adapter.
 */

public abstract class AbstractAdapterActivityRegistrationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.registration.ActivityRegistrationLookupSession {

    private final org.osid.course.registration.ActivityRegistrationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterActivityRegistrationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActivityRegistrationLookupSession(org.osid.course.registration.ActivityRegistrationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code ActivityRegistration} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupActivityRegistrations() {
        return (this.session.canLookupActivityRegistrations());
    }


    /**
     *  A complete view of the {@code ActivityRegistration} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityRegistrationView() {
        this.session.useComparativeActivityRegistrationView();
        return;
    }


    /**
     *  A complete view of the {@code ActivityRegistration} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityRegistrationView() {
        this.session.usePlenaryActivityRegistrationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity registrations in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only activity registrations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveActivityRegistrationView() {
        this.session.useEffectiveActivityRegistrationView();
        return;
    }
    

    /**
     *  All activity registrations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveActivityRegistrationView() {
        this.session.useAnyEffectiveActivityRegistrationView();
        return;
    }

     
    /**
     *  Gets the {@code ActivityRegistration} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ActivityRegistration} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ActivityRegistration} and
     *  retained for compatibility.
     *
     *  In effective mode, activity registrations are returned that are currently
     *  effective.  In any effective mode, effective activity registrations and
     *  those currently expired are returned.
     *
     *  @param activityRegistrationId {@code Id} of the {@code ActivityRegistration}
     *  @return the activity registration
     *  @throws org.osid.NotFoundException {@code activityRegistrationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code activityRegistrationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistration getActivityRegistration(org.osid.id.Id activityRegistrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistration(activityRegistrationId));
    }


    /**
     *  Gets an {@code ActivityRegistrationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityRegistrations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ActivityRegistrations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, activity registrations are returned that are currently
     *  effective.  In any effective mode, effective activity registrations and
     *  those currently expired are returned.
     *
     *  @param  activityRegistrationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ActivityRegistration} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code activityRegistrationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByIds(org.osid.id.IdList activityRegistrationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsByIds(activityRegistrationIds));
    }


    /**
     *  Gets an {@code ActivityRegistrationList} corresponding to the given
     *  activity registration genus {@code Type} which does not include
     *  activity registrations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activity registrations are returned that are currently
     *  effective.  In any effective mode, effective activity registrations and
     *  those currently expired are returned.
     *
     *  @param  activityRegistrationGenusType an activityRegistration genus type 
     *  @return the returned {@code ActivityRegistration} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityRegistrationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByGenusType(org.osid.type.Type activityRegistrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsByGenusType(activityRegistrationGenusType));
    }


    /**
     *  Gets an {@code ActivityRegistrationList} corresponding to the given
     *  activity registration genus {@code Type} and include any additional
     *  activity registrations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activity registrations are returned that are currently
     *  effective.  In any effective mode, effective activity registrations and
     *  those currently expired are returned.
     *
     *  @param  activityRegistrationGenusType an activityRegistration genus type 
     *  @return the returned {@code ActivityRegistration} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityRegistrationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByParentGenusType(org.osid.type.Type activityRegistrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsByParentGenusType(activityRegistrationGenusType));
    }


    /**
     *  Gets an {@code ActivityRegistrationList} containing the given
     *  activity registration record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityRegistrationRecordType an activityRegistration record type 
     *  @return the returned {@code ActivityRegistration} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityRegistrationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByRecordType(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsByRecordType(activityRegistrationRecordType));
    }


    /**
     *  Gets an {@code ActivityRegistrationList} effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *  
     *  In active mode, activity registrations are returned that are
     *  currently active. In any status mode, active and inactive
     *  activity registrations are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ActivityRegistration} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsOnDate(org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsOnDate(from, to));
    }
        

    /**
     *  Gets all {@code ActivityRegistrations} for a registration. 
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In effective mode, activity registrations are returned that are 
     *  currently effective. In any effective mode, effective activity 
     *  registrations and those currently expired are returned. 
     *
     *  @param  registrationId a registration {@code Id} 
     *  @return a list of {@code ActivityRegistrations} 
     *  @throws org.osid.NullArgumentException {@code registrationId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForRegistration(org.osid.id.Id registrationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForRegistration(registrationId));
    }


    /**
     *  Gets all {@code ActivityRegistrations} for a registration and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In effective mode, activity registrations are returned that
     *  are currently effective. In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  registrationId a registration {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of {@code ActivityRegistrations} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code registrationId,
     *         from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForRegistrationOnDate(org.osid.id.Id registrationId, 
                                                                                                               org.osid.calendaring.DateTime from, 
                                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getActivityRegistrationsForRegistrationOnDate(registrationId, from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to an
     *  activity {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible through
     *  this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  activityId the {@code Id} of the activity
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForActivity(activityId));
    }


    /**
     *  Gets a list of activity registrations corresponding to an
     *  activity {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  activityId the {@code Id} of the activity
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityOnDate(org.osid.id.Id activityId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForActivityOnDate(activityId, from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to a student
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible
     *  through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForStudent(resourceId));
    }


    /**
     *  Gets a list of activity registrations corresponding to a student
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible
     *  through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForStudentOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForStudentOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to activity and student
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible
     *  through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  activityId the {@code Id} of the activity
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityAndStudent(org.osid.id.Id activityId,
                                                                                                               org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForActivityAndStudent(activityId, resourceId));
    }


    /**
     *  Gets a list of activity registrations corresponding to activity and student
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible
     *  through this session.
     *
     *  In effective mode, activity registrations are returned that are currently
     *  effective. In any effective mode, effective activity registrations and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code activityId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityAndStudentOnDate(org.osid.id.Id activityId,
                                                                                                                     org.osid.id.Id resourceId,
                                                                                                                     org.osid.calendaring.DateTime from,
                                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForActivityAndStudentOnDate(activityId, resourceId, from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to an
     *  course offering {@code Id}.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForCourseOffering(courseOfferingId));
    }


    /**
     *  Gets a list of activity registrations corresponding to an
     *  course offering {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForCourseOfferingOnDate(courseOfferingId, from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to course
     *  offering and student {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingAndStudent(org.osid.id.Id courseOfferingId,
                                                                                                               org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForCourseOfferingAndStudent(courseOfferingId, resourceId));
    }


    /**
     *  Gets a list of activity registrations corresponding to course
     *  offering and student {@code Ids} and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective. In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param courseOfferingId the {@code Id} of the course offering
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ActivityRegistrationList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingAndStudentOnDate(org.osid.id.Id courseOfferingId,
                                                                                                                     org.osid.id.Id resourceId,
                                                                                                                     org.osid.calendaring.DateTime from,
                                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrationsForCourseOfferingAndStudentOnDate(courseOfferingId, resourceId, from, to));
    }


    /**
     *  Gets all {@code ActivityRegistrations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activity registrations are returned that are currently
     *  effective.  In any effective mode, effective activity registrations and
     *  those currently expired are returned.
     *
     *  @return a list of {@code ActivityRegistrations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivityRegistrations());
    }
}
