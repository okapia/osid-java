//
// MutableIndexedMapProxyMeterLookupSession
//
//    Implements a Meter lookup service backed by a collection of
//    meters indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering;


/**
 *  Implements a Meter lookup service backed by a collection of
 *  meters. The meters are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some meters may be compatible
 *  with more types than are indicated through these meter
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of meters can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyMeterLookupSession
    extends net.okapia.osid.jamocha.core.metering.spi.AbstractIndexedMapMeterLookupSession
    implements org.osid.metering.MeterLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMeterLookupSession} with
     *  no meter.
     *
     *  @param utility the utility
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code utility} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMeterLookupSession(org.osid.metering.Utility utility,
                                                       org.osid.proxy.Proxy proxy) {
        setUtility(utility);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMeterLookupSession} with
     *  a single meter.
     *
     *  @param utility the utility
     *  @param  meter an meter
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code utility},
     *          {@code meter}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMeterLookupSession(org.osid.metering.Utility utility,
                                                       org.osid.metering.Meter meter, org.osid.proxy.Proxy proxy) {

        this(utility, proxy);
        putMeter(meter);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMeterLookupSession} using
     *  an array of meters.
     *
     *  @param utility the utility
     *  @param  meters an array of meters
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code utility},
     *          {@code meters}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMeterLookupSession(org.osid.metering.Utility utility,
                                                       org.osid.metering.Meter[] meters, org.osid.proxy.Proxy proxy) {

        this(utility, proxy);
        putMeters(meters);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMeterLookupSession} using
     *  a collection of meters.
     *
     *  @param utility the utility
     *  @param  meters a collection of meters
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code utility},
     *          {@code meters}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMeterLookupSession(org.osid.metering.Utility utility,
                                                       java.util.Collection<? extends org.osid.metering.Meter> meters,
                                                       org.osid.proxy.Proxy proxy) {
        this(utility, proxy);
        putMeters(meters);
        return;
    }

    
    /**
     *  Makes a {@code Meter} available in this session.
     *
     *  @param  meter a meter
     *  @throws org.osid.NullArgumentException {@code meter{@code 
     *          is {@code null}
     */

    @Override
    public void putMeter(org.osid.metering.Meter meter) {
        super.putMeter(meter);
        return;
    }


    /**
     *  Makes an array of meters available in this session.
     *
     *  @param  meters an array of meters
     *  @throws org.osid.NullArgumentException {@code meters{@code 
     *          is {@code null}
     */

    @Override
    public void putMeters(org.osid.metering.Meter[] meters) {
        super.putMeters(meters);
        return;
    }


    /**
     *  Makes collection of meters available in this session.
     *
     *  @param  meters a collection of meters
     *  @throws org.osid.NullArgumentException {@code meter{@code 
     *          is {@code null}
     */

    @Override
    public void putMeters(java.util.Collection<? extends org.osid.metering.Meter> meters) {
        super.putMeters(meters);
        return;
    }


    /**
     *  Removes a Meter from this session.
     *
     *  @param meterId the {@code Id} of the meter
     *  @throws org.osid.NullArgumentException {@code meterId{@code  is
     *          {@code null}
     */

    @Override
    public void removeMeter(org.osid.id.Id meterId) {
        super.removeMeter(meterId);
        return;
    }    
}
