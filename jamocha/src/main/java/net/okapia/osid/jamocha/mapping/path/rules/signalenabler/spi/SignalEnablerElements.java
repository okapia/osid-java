//
// SignalEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.signalenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SignalEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the SignalEnablerElement Id.
     *
     *  @return the signal enabler element Id
     */

    public static org.osid.id.Id getSignalEnablerEntityId() {
        return (makeEntityId("osid.mapping.path.rules.SignalEnabler"));
    }


    /**
     *  Gets the RuledSignalId element Id.
     *
     *  @return the RuledSignalId element Id
     */

    public static org.osid.id.Id getRuledSignalId() {
        return (makeQueryElementId("osid.mapping.path.rules.signalenabler.RuledSignalId"));
    }


    /**
     *  Gets the RuledSignal element Id.
     *
     *  @return the RuledSignal element Id
     */

    public static org.osid.id.Id getRuledSignal() {
        return (makeQueryElementId("osid.mapping.path.rules.signalenabler.RuledSignal"));
    }


    /**
     *  Gets the MapId element Id.
     *
     *  @return the MapId element Id
     */

    public static org.osid.id.Id getMapId() {
        return (makeQueryElementId("osid.mapping.path.rules.signalenabler.MapId"));
    }


    /**
     *  Gets the Map element Id.
     *
     *  @return the Map element Id
     */

    public static org.osid.id.Id getMap() {
        return (makeQueryElementId("osid.mapping.path.rules.signalenabler.Map"));
    }
}
