//
// AbstractBranchQueryInspector.java
//
//     A template for making a BranchQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.branch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for branches.
 */

public abstract class AbstractBranchQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQueryInspector
    implements org.osid.journaling.BranchQueryInspector {

    private final java.util.Collection<org.osid.journaling.records.BranchQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the origin journal entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOriginJournalEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the origin journal entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getOriginJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }


    /**
     *  Gets the latest journal entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLatestJournalEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the latest journal entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getLatestJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given branch query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a branch implementing the requested record.
     *
     *  @param branchRecordType a branch record type
     *  @return the branch query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(branchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.BranchQueryInspectorRecord getBranchQueryInspectorRecord(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.BranchQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(branchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(branchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this branch query. 
     *
     *  @param branchQueryInspectorRecord branch query inspector
     *         record
     *  @param branchRecordType branch record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBranchQueryInspectorRecord(org.osid.journaling.records.BranchQueryInspectorRecord branchQueryInspectorRecord, 
                                                   org.osid.type.Type branchRecordType) {

        addRecordType(branchRecordType);
        nullarg(branchRecordType, "branch record type");
        this.records.add(branchQueryInspectorRecord);        
        return;
    }
}
