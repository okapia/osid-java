//
// AbstractImmutableBrokerProcessor.java
//
//     Wraps a mutable BrokerProcessor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>BrokerProcessor</code> to hide modifiers. This
 *  wrapper provides an immutized BrokerProcessor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying brokerProcessor whose state changes are visible.
 */

public abstract class AbstractImmutableBrokerProcessor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidProcessor
    implements org.osid.provisioning.rules.BrokerProcessor {

    private final org.osid.provisioning.rules.BrokerProcessor brokerProcessor;


    /**
     *  Constructs a new <code>AbstractImmutableBrokerProcessor</code>.
     *
     *  @param brokerProcessor the broker processor to immutablize
     *  @throws org.osid.NullArgumentException <code>brokerProcessor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBrokerProcessor(org.osid.provisioning.rules.BrokerProcessor brokerProcessor) {
        super(brokerProcessor);
        this.brokerProcessor = brokerProcessor;
        return;
    }


    /**
     *  Tests if this broker provides leases. A lease is a temporary provision 
     *  that expires or must be returned. 
     *
     *  @return <code> true </code> if this is a leasing broker, <code> false 
     *          </code> if the brokered provisions are permanent 
     */

    @OSID @Override
    public boolean isLeasing() {
        return (this.brokerProcessor.isLeasing());
    }


    /**
     *  Tests if leases from this broker have fixed durations. 
     *
     *  @return <code> true </code> if leases have fixed durations, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> isLeasing() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean hasFixedLeaseDuration() {
        return (this.brokerProcessor.hasFixedLeaseDuration());
    }


    /**
     *  Gets the fixed lease duration. 
     *
     *  @return the fixed lease duration 
     *  @throws org.osid.IllegalStateException <code> hasFixedLeaseDuration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedLeaseDuration() {
        return (this.brokerProcessor.getFixedLeaseDuration());
    }


    /**
     *  Tests if provisions from this broker must be returned. 
     *
     *  @return <code> true </code> if this broker requires provision return, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> isLeasing() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean mustReturnProvisions() {
        return (this.brokerProcessor.mustReturnProvisions());
    }


    /**
     *  Tests if this broker allows exchanging of provisions. 
     *
     *  @return <code> true </code> if provision exchange is permitted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean allowsProvisionExchange() {
        return (this.brokerProcessor.allowsProvisionExchange());
    }


    /**
     *  Tests if this broker allows compound requests using <code> 
     *  RequestTransactions. </code> 
     *
     *  @return <code> true </code> if compound requests are permitted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean allowsCompoundRequests() {
        return (this.brokerProcessor.allowsCompoundRequests());
    }


    /**
     *  Gets the broker processor record corresponding to the given <code> 
     *  BrokerProcessor </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> brokerProcessorRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(brokerProcessorRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  brokerProcessorRecordType the type of broker processor record 
     *          to retrieve 
     *  @return the broker processor record 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(brokerProcessorRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorRecord getBrokerProcessorRecord(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException {

        return (this.brokerProcessor.getBrokerProcessorRecord(brokerProcessorRecordType));
    }
}

