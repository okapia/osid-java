//
// AbstractAdapterForumLookupSession.java
//
//    A Forum lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.forum.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Forum lookup session adapter.
 */

public abstract class AbstractAdapterForumLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.forum.ForumLookupSession {

    private final org.osid.forum.ForumLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterForumLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterForumLookupSession(org.osid.forum.ForumLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Forum} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupForums() {
        return (this.session.canLookupForums());
    }


    /**
     *  A complete view of the {@code Forum} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeForumView() {
        this.session.useComparativeForumView();
        return;
    }


    /**
     *  A complete view of the {@code Forum} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryForumView() {
        this.session.usePlenaryForumView();
        return;
    }

     
    /**
     *  Gets the {@code Forum} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Forum} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Forum} and
     *  retained for compatibility.
     *
     *  @param forumId {@code Id} of the {@code Forum}
     *  @return the forum
     *  @throws org.osid.NotFoundException {@code forumId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code forumId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getForum(forumId));
    }


    /**
     *  Gets a {@code ForumList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  forums specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Forums} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  forumIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Forum} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code forumIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByIds(org.osid.id.IdList forumIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getForumsByIds(forumIds));
    }


    /**
     *  Gets a {@code ForumList} corresponding to the given
     *  forum genus {@code Type} which does not include
     *  forums of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned {@code Forum} list
     *  @throws org.osid.NullArgumentException
     *          {@code forumGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getForumsByGenusType(forumGenusType));
    }


    /**
     *  Gets a {@code ForumList} corresponding to the given
     *  forum genus {@code Type} and include any additional
     *  forums with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned {@code Forum} list
     *  @throws org.osid.NullArgumentException
     *          {@code forumGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByParentGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getForumsByParentGenusType(forumGenusType));
    }


    /**
     *  Gets a {@code ForumList} containing the given
     *  forum record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumRecordType a forum record type 
     *  @return the returned {@code Forum} list
     *  @throws org.osid.NullArgumentException
     *          {@code forumRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByRecordType(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getForumsByRecordType(forumRecordType));
    }


    /**
     *  Gets a {@code ForumList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Forum} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getForumsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Forums}. 
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Forums} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForums()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getForums());
    }
}
