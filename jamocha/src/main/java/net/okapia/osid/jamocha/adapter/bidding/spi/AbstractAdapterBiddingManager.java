//
// AbstractBiddingManager.java
//
//     An adapter for a BiddingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a BiddingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterBiddingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.bidding.BiddingManager>
    implements org.osid.bidding.BiddingManager {


    /**
     *  Constructs a new {@code AbstractAdapterBiddingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterBiddingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterBiddingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterBiddingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any auction house federation is exposed. Federation is 
     *  exposed when a specific auction house may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of auction houses appears as a single auction house. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a my auction service is supported for the current agent. 
     *
     *  @return <code> true </code> if my auction is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyAuction() {
        return (getAdapteeManager().supportsMyAuction());
    }


    /**
     *  Tests if a my auction notification service is supported for the 
     *  current agent. 
     *
     *  @return <code> true </code> if my auction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyAuctionNotification() {
        return (getAdapteeManager().supportsMyAuctionNotification());
    }


    /**
     *  Tests if looking up auctions is supported. 
     *
     *  @return <code> true </code> if auction lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionLookup() {
        return (getAdapteeManager().supportsAuctionLookup());
    }


    /**
     *  Tests if querying auctions is supported. 
     *
     *  @return <code> true </code> if auction query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionQuery() {
        return (getAdapteeManager().supportsAuctionQuery());
    }


    /**
     *  Tests if searching auctions is supported. 
     *
     *  @return <code> true </code> if auction search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionSearch() {
        return (getAdapteeManager().supportsAuctionSearch());
    }


    /**
     *  Tests if an auction administrative service is supported. 
     *
     *  @return <code> true </code> if auction administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionAdmin() {
        return (getAdapteeManager().supportsAuctionAdmin());
    }


    /**
     *  Tests if an auction <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if auction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionNotification() {
        return (getAdapteeManager().supportsAuctionNotification());
    }


    /**
     *  Tests if an auction auction house lookup service is supported. 
     *
     *  @return <code> true </code> if an auction auction house lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionAuctionHouse() {
        return (getAdapteeManager().supportsAuctionAuctionHouse());
    }


    /**
     *  Tests if an auction auction house assignment service is supported. 
     *
     *  @return <code> true </code> if an auction to auction house assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionAuctionHouseAssignment() {
        return (getAdapteeManager().supportsAuctionAuctionHouseAssignment());
    }


    /**
     *  Tests if an auction smart auction house service is supported. 
     *
     *  @return <code> true </code> if an v smart auction house service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionSmartAuctionHouse() {
        return (getAdapteeManager().supportsAuctionSmartAuctionHouse());
    }


    /**
     *  Tests if looking up bids is supported. 
     *
     *  @return <code> true </code> if bid lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidLookup() {
        return (getAdapteeManager().supportsBidLookup());
    }


    /**
     *  Tests if querying bids is supported. 
     *
     *  @return <code> true </code> if bid query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidQuery() {
        return (getAdapteeManager().supportsBidQuery());
    }


    /**
     *  Tests if searching bids is supported. 
     *
     *  @return <code> true </code> if bid search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidSearch() {
        return (getAdapteeManager().supportsBidSearch());
    }


    /**
     *  Tests if bid administrative service is supported. 
     *
     *  @return <code> true </code> if bid administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidAdmin() {
        return (getAdapteeManager().supportsBidAdmin());
    }


    /**
     *  Tests if a bid notification service is supported. 
     *
     *  @return <code> true </code> if bid notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidNotification() {
        return (getAdapteeManager().supportsBidNotification());
    }


    /**
     *  Tests if a bid auction house lookup service is supported. 
     *
     *  @return <code> true </code> if a bid auction house lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidAuctionHouse() {
        return (getAdapteeManager().supportsBidAuctionHouse());
    }


    /**
     *  Tests if a bid auction house service is supported. 
     *
     *  @return <code> true </code> if bid to auction house assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidAuctionHouseAssignment() {
        return (getAdapteeManager().supportsBidAuctionHouseAssignment());
    }


    /**
     *  Tests if a bid smart auction house lookup service is supported. 
     *
     *  @return <code> true </code> if a bid smart auction house service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidSmartAuctionHouse() {
        return (getAdapteeManager().supportsBidSmartAuctionHouse());
    }


    /**
     *  Tests if looking up auction houses is supported. 
     *
     *  @return <code> true </code> if auction house lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseLookup() {
        return (getAdapteeManager().supportsAuctionHouseLookup());
    }


    /**
     *  Tests if querying auction houses is supported. 
     *
     *  @return <code> true </code> if a auction house query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (getAdapteeManager().supportsAuctionHouseQuery());
    }


    /**
     *  Tests if searching auction houses is supported. 
     *
     *  @return <code> true </code> if auction house search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseSearch() {
        return (getAdapteeManager().supportsAuctionHouseSearch());
    }


    /**
     *  Tests if auction house administrative service is supported. 
     *
     *  @return <code> true </code> if auction house administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseAdmin() {
        return (getAdapteeManager().supportsAuctionHouseAdmin());
    }


    /**
     *  Tests if a auction house <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction house notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseNotification() {
        return (getAdapteeManager().supportsAuctionHouseNotification());
    }


    /**
     *  Tests for the availability of a auction house hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if auction house hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseHierarchy() {
        return (getAdapteeManager().supportsAuctionHouseHierarchy());
    }


    /**
     *  Tests for the availability of a auction house hierarchy design 
     *  service. 
     *
     *  @return <code> true </code> if auction house hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseHierarchyDesign() {
        return (getAdapteeManager().supportsAuctionHouseHierarchyDesign());
    }


    /**
     *  Tests for the availability of a bidding batch service. 
     *
     *  @return <code> true </code> ifa bidding batch servicen is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingBatch() {
        return (getAdapteeManager().supportsBiddingBatch());
    }


    /**
     *  Tests for the availability of a bidding rules service. 
     *
     *  @return <code> true </code> ifa bidding rules servicen is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingRules() {
        return (getAdapteeManager().supportsBiddingRules());
    }


    /**
     *  Gets the supported <code> Auction </code> record types. 
     *
     *  @return a list containing the supported <code> Auction </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionRecordTypes() {
        return (getAdapteeManager().getAuctionRecordTypes());
    }


    /**
     *  Tests if the given <code> Auction </code> record type is supported. 
     *
     *  @param  auctionRecordType a <code> Type </code> indicating an <code> 
     *          Auction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auctionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionRecordType(org.osid.type.Type auctionRecordType) {
        return (getAdapteeManager().supportsAuctionRecordType(auctionRecordType));
    }


    /**
     *  Gets the supported <code> Auction </code> search types. 
     *
     *  @return a list containing the supported <code> Auction </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionSearchRecordTypes() {
        return (getAdapteeManager().getAuctionSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Auction </code> search type is supported. 
     *
     *  @param  auctionSearchRecordType a <code> Type </code> indicating an 
     *          <code> Auction </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auctionSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionSearchRecordType(org.osid.type.Type auctionSearchRecordType) {
        return (getAdapteeManager().supportsAuctionSearchRecordType(auctionSearchRecordType));
    }


    /**
     *  Gets the supported <code> Bid </code> record types. 
     *
     *  @return a list containing the supported <code> Bid </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBidRecordTypes() {
        return (getAdapteeManager().getBidRecordTypes());
    }


    /**
     *  Tests if the given <code> Bid </code> record type is supported. 
     *
     *  @param  bidRecordType a <code> Type </code> indicating a <code> Bid 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bidRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBidRecordType(org.osid.type.Type bidRecordType) {
        return (getAdapteeManager().supportsBidRecordType(bidRecordType));
    }


    /**
     *  Gets the supported <code> Bid </code> search record types. 
     *
     *  @return a list containing the supported <code> Bid </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBidSearchRecordTypes() {
        return (getAdapteeManager().getBidSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Bid </code> search record type is supported. 
     *
     *  @param  bidSearchRecordType a <code> Type </code> indicating a <code> 
     *          Bid </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bidSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBidSearchRecordType(org.osid.type.Type bidSearchRecordType) {
        return (getAdapteeManager().supportsBidSearchRecordType(bidSearchRecordType));
    }


    /**
     *  Gets the supported <code> AuctionHouse </code> record types. 
     *
     *  @return a list containing the supported <code> AuctionHouse </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionHouseRecordTypes() {
        return (getAdapteeManager().getAuctionHouseRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionHouse </code> record type is 
     *  supported. 
     *
     *  @param  auctionHouseRecordType a <code> Type </code> indicating a 
     *          <code> AuctionHouse </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auctionHouseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionHouseRecordType(org.osid.type.Type auctionHouseRecordType) {
        return (getAdapteeManager().supportsAuctionHouseRecordType(auctionHouseRecordType));
    }


    /**
     *  Gets the supported <code> AuctionHouse </code> search record types. 
     *
     *  @return a list containing the supported <code> AuctionHouse </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionHouseSearchRecordTypes() {
        return (getAdapteeManager().getAuctionHouseSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AuctionHouse </code> search record type is 
     *  supported. 
     *
     *  @param  auctionHouseSearchRecordType a <code> Type </code> indicating 
     *          a <code> AuctionHouse </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionHouseSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionHouseSearchRecordType(org.osid.type.Type auctionHouseSearchRecordType) {
        return (getAdapteeManager().supportsAuctionHouseSearchRecordType(auctionHouseSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  service. 
     *
     *  @return a <code> MyAuctionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyAuction() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionSession getMyAuctionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyAuctionSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the auction house 
     *  @return a <code> MyAuctionSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyAuction() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionSession getMyAuctionSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyAuctionSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  notification service. 
     *
     *  @param  myAuctionReceiver the notification callback 
     *  @return a <code> MyAuctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> myAuctionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAuctionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionNotificationSession getMyAuctionNotificationSession(org.osid.bidding.MyAuctionReceiver myAuctionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyAuctionNotificationSession(myAuctionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  notification service for the given auction house. 
     *
     *  @param  myAuctionReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the auction house 
     *  @return a <code> MyAuctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> myAuctionReceiver 
     *          </code> or <code> auctionHouseId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAuctionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionNotificationSession getMyAuctionNotificationSessionForAuctionHouse(org.osid.bidding.MyAuctionReceiver myAuctionReceiver, 
                                                                                                        org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyAuctionNotificationSessionForAuctionHouse(myAuctionReceiver, auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction lookup 
     *  service. 
     *
     *  @return an <code> AuctionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionLookupSession getAuctionLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction lookup 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionLookupSession getAuctionLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction query 
     *  service. 
     *
     *  @return an <code> AuctionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuerySession getAuctionQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction query 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionQuerySession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuerySession getAuctionQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionQuerySessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction search 
     *  service. 
     *
     *  @return an <code> AuctionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchSession getAuctionSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction search 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionSearchSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchSession getAuctionSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionSearchSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  administration service. 
     *
     *  @return an <code> AuctionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAdminSession getAuctionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionAdminSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAdminSession getAuctionAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionAdminSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  notification service. 
     *
     *  @param  auctionReceiver the notification callback 
     *  @return an <code> AuctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionNotificationSession getAuctionNotificationSession(org.osid.bidding.AuctionReceiver auctionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionNotificationSession(auctionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  notification service for the given auction house. 
     *
     *  @param  auctionReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionReceiver </code> 
     *          or <code> auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionNotificationSession getAuctionNotificationSessionForAuctionHouse(org.osid.bidding.AuctionReceiver auctionReceiver, 
                                                                                                    org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionNotificationSessionForAuctionHouse(auctionReceiver, auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction/auctionHouse 
     *  mappings. 
     *
     *  @return an <code> AuctionAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseSession getAuctionAuctionHouseSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionAuctionHouseSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auctions 
     *  to auction houses. 
     *
     *  @return an <code> AuctionAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionAuctionHouseAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseAssignmentSession getAuctionAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionAuctionHouseAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction smart auction 
     *  houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionSmartAuctionHouse() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseSession getAuctionSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionSmartAuctionHouseSession(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid lookup 
     *  service. 
     *
     *  @return a <code> BidLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidLookupSession getBidLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBidLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid lookup 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidLookupSession getBidLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBidLookupSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid query 
     *  service. 
     *
     *  @return a <code> BidQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuerySession getBidQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBidQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid query 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> CBidQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuerySession getBidQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBidQuerySessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid search 
     *  service. 
     *
     *  @return a <code> BidSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSearchSession getBidSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBidSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid search 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSearchSession getBidSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBidSearchSessionForAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  administrative service. 
     *
     *  @return a <code> BidAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAdminSession getBidAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBidAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  administrative service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAdminSession getBidAdminSessionForAuction(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBidAdminSessionForAuction(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  notification service. 
     *
     *  @param  bidReceiver the notification callback 
     *  @return a <code> BidNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> bidReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidNotificationSession getBidNotificationSession(org.osid.bidding.BidReceiver bidReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBidNotificationSession(bidReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  notification service for the given auction house. 
     *
     *  @param  bidReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bidReceiver </code> or 
     *          <code> auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidNotificationSession getBidNotificationSessionForAuctionHouse(org.osid.bidding.BidReceiver bidReceiver, 
                                                                                            org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBidNotificationSessionForAuctionHouse(bidReceiver, auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup bid/auctionHouse 
     *  mappings. 
     *
     *  @return a <code> BidAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAuctionHouseSession getBidAuctionHouseSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBidAuctionHouseSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to auction houses. 
     *
     *  @return a <code> BidyAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidAuctionHouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAuctionHouseAssignmentSession getBidAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBidAuctionHouseAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage bid smart auction 
     *  houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidSmartAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSmartAuctionHouseSession getBidSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBidSmartAuctionHouseSession(auctionHouseId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  lookup service. 
     *
     *  @return a <code> AuctionHouseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseLookupSession getAuctionHouseLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionHouseLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  query service. 
     *
     *  @return a <code> AuctionHouseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuerySession getAuctionHouseQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionHouseQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  search service. 
     *
     *  @return a <code> AuctionHouseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseSearchSession getAuctionHouseSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionHouseSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  administrative service. 
     *
     *  @return a <code> AuctionHouseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseAdminSession getAuctionHouseAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionHouseAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  notification service. 
     *
     *  @param  auctionHouseReceiver the notification callback 
     *  @return a <code> AuctionHouseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseNotificationSession getAuctionHouseNotificationSession(org.osid.bidding.AuctionHouseReceiver auctionHouseReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionHouseNotificationSession(auctionHouseReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  hierarchy service. 
     *
     *  @return a <code> AuctionHouseHierarchySession </code> for auction 
     *          houses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseHierarchySession getAuctionHouseHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionHouseHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for auction houses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseHierarchyDesignSession getAuctionHouseHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuctionHouseHierarchyDesignSession());
    }


    /**
     *  Gets a <code> BiddingBatchManager. </code> 
     *
     *  @return a <code> BiddingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBiddingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.BiddingBatchManager getBiddingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBiddingBatchManager());
    }


    /**
     *  Gets a <code> BiddingRulesManager. </code> 
     *
     *  @return a <code> BiddingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBiddingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.BiddingRulesManager getBiddingRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBiddingRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
