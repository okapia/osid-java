//
// AbstractAvailabilitySearchOdrer.java
//
//     Defines an AvailabilitySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.availability.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AvailabilitySearchOrder}.
 */

public abstract class AbstractAvailabilitySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.resourcing.AvailabilitySearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.AvailabilitySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Orders the results by job. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByJob(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a job search order is available. 
     *
     *  @return <code> true </code> if a job search order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobSearchOrder() {
        return (false);
    }


    /**
     *  Gets the job search order. 
     *
     *  @return the job search order 
     *  @throws org.osid.IllegalStateException <code> supportsJobSearchOrder() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchOrder getJobSearchOrder() {
        throw new org.osid.UnimplementedException("supportsJobSearchOrder() is false");
    }


    /**
     *  Orders the results by competency. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompetency(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a competency search order is available. 
     *
     *  @return <code> true </code> if a competency search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencySearchOrder() {
        return (false);
    }


    /**
     *  Gets the competency search order. 
     *
     *  @return the competency search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCompetencySearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchOrder getCompetencySearchOrder() {
        throw new org.osid.UnimplementedException("supportsCompetencySearchOrder() is false");
    }


    /**
     *  Orders the results by percentage availability. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPercentage(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  availabilityRecordType an availability record type 
     *  @return {@code true} if the availabilityRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type availabilityRecordType) {
        for (org.osid.resourcing.records.AvailabilitySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  availabilityRecordType the availability record type 
     *  @return the availability search order record
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(availabilityRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilitySearchOrderRecord getAvailabilitySearchOrderRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.AvailabilitySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this availability. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param availabilityRecord the availability search odrer record
     *  @param availabilityRecordType availability record type
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityRecord} or
     *          {@code availabilityRecordTypeavailability} is
     *          {@code null}
     */
            
    protected void addAvailabilityRecord(org.osid.resourcing.records.AvailabilitySearchOrderRecord availabilitySearchOrderRecord, 
                                     org.osid.type.Type availabilityRecordType) {

        addRecordType(availabilityRecordType);
        this.records.add(availabilitySearchOrderRecord);
        
        return;
    }
}
