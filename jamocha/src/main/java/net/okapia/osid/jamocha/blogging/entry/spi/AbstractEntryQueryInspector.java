//
// AbstractEntryQueryInspector.java
//
//     A template for making an EntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for entries.
 */

public abstract class AbstractEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQueryInspector
    implements org.osid.blogging.EntryQueryInspector {

    private final java.util.Collection<org.osid.blogging.records.EntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the timestamp terms. 
     *
     *  @return the timestamp terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getTimestampTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the poster <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPosterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the poster terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getPosterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the posting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the posting agent terms. 
     *
     *  @return the agent query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getPostingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the subject line terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSubjectLineTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the summary terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSummaryTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the text terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the blog <code> Id </code> terms. 
     *
     *  @return the blog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBlogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the blog terms. 
     *
     *  @return the blog terms 
     */

    @OSID @Override
    public org.osid.blogging.BlogQueryInspector[] getBlogTerms() {
        return (new org.osid.blogging.BlogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an entry implementing the requested record.
     *
     *  @param entryRecordType an entry record type
     *  @return the entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.EntryQueryInspectorRecord getEntryQueryInspectorRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.EntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry query. 
     *
     *  @param entryQueryInspectorRecord entry query inspector
     *         record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEntryQueryInspectorRecord(org.osid.blogging.records.EntryQueryInspectorRecord entryQueryInspectorRecord, 
                                                   org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);
        nullarg(entryRecordType, "entry record type");
        this.records.add(entryQueryInspectorRecord);        
        return;
    }
}
