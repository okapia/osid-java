//
// AbstractCalendarSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.calendar.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCalendarSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.CalendarSearchResults {

    private org.osid.calendaring.CalendarList calendars;
    private final org.osid.calendaring.CalendarQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.records.CalendarSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCalendarSearchResults.
     *
     *  @param calendars the result set
     *  @param calendarQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>calendars</code>
     *          or <code>calendarQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCalendarSearchResults(org.osid.calendaring.CalendarList calendars,
                                            org.osid.calendaring.CalendarQueryInspector calendarQueryInspector) {
        nullarg(calendars, "calendars");
        nullarg(calendarQueryInspector, "calendar query inspectpr");

        this.calendars = calendars;
        this.inspector = calendarQueryInspector;

        return;
    }


    /**
     *  Gets the calendar list resulting from a search.
     *
     *  @return a calendar list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendars() {
        if (this.calendars == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.CalendarList calendars = this.calendars;
        this.calendars = null;
	return (calendars);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.CalendarQueryInspector getCalendarQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  calendar search record <code> Type. </code> This method must
     *  be used to retrieve a calendar implementing the requested
     *  record.
     *
     *  @param calendarSearchRecordType a calendar search 
     *         record type 
     *  @return the calendar search
     *  @throws org.osid.NullArgumentException
     *          <code>calendarSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(calendarSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CalendarSearchResultsRecord getCalendarSearchResultsRecord(org.osid.type.Type calendarSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.records.CalendarSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(calendarSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(calendarSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record calendar search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCalendarRecord(org.osid.calendaring.records.CalendarSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "calendar record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
