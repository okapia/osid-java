//
// AbstractDocet.java
//
//     Defines a Docet.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Docet</code>.
 */

public abstract class AbstractDocet
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.syllabus.Docet {

    private org.osid.course.syllabus.Module module;
    private org.osid.course.ActivityUnit activityUnit;
    private boolean inClass = true;

    private final java.util.Collection<org.osid.learning.Objective> learningObjectives = new java.util.LinkedHashSet<>();
    private org.osid.calendaring.Duration duration;
    private final java.util.Collection<org.osid.repository.Asset> assets = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.Assessment> assessments = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.DocetRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Ids </code> of the learning module. 
     *
     *  @return the learning module <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getModuleId() {
        return (this.module.getId());
    }


    /**
     *  Gets the learning module 
     *
     *  @return the learning module 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.Module getModule()
        throws org.osid.OperationFailedException {

        return (this.module);
    }


    /**
     *  Sets the module.
     *
     *  @param module a module
     *  @throws org.osid.NullArgumentException
     *          <code>module</code> is <code>null</code>
     */

    protected void setModule(org.osid.course.syllabus.Module module) {
        nullarg(module, "module");
        this.module = module;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the activity unit. 
     *
     *  @return the activity unit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityUnitId() {
        return (this.activityUnit.getId());
    }


    /**
     *  Gets the activity unit. 
     *
     *  @return the activity unit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit()
        throws org.osid.OperationFailedException {

        return (this.activityUnit);
    }


    /**
     *  Sets the activity unit.
     *
     *  @param activityUnit an activity unit
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnit</code> is <code>null</code>
     */

    protected void setActivityUnit(org.osid.course.ActivityUnit activityUnit) {
        nullarg(activityUnit, "activity unit");
        this.activityUnit = activityUnit;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the learning objectives. 
     *
     *  @return the learning objective <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        try {
            org.osid.learning.ObjectiveList learningObjectives = getLearningObjectives();
            return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(learningObjectives));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the learning objectives. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.learningObjectives));
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException
     *          <code>objective</code> is <code>null</code>
     */

    protected void addLearningObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "learning objective");
        this.learningObjectives.add(objective);
        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException
     *          <code>objectives</code> is <code>null</code>
     */

    protected void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        nullarg(objectives, "learning objectives");
        this.learningObjectives.clear();
        this.learningObjectives.addAll(objectives);
        return;
    }


    /**
     *  Tests if this <code> Docet </code> occurs within an <code> Activity 
     *  </code> or it outlines efforts spent outside a convened activity. In 
     *  class docets can be used to align the estimated duration with the 
     *  schedule of activities. 
     *
     *  @return <code>true</code> if this occurs within an activity,
     *          <code>false</code> if occurs outside a convened
     *          activity
     */

    @OSID @Override
    public boolean isInClass() {
        return (this.inClass);
    }


    /**
     *  Sets the in class flag.
     *
     *  @param inClass <code>true</code> if this occurs within an
     *         activity, <code>false</code> if occurs outside a
     *         convened activity
     */

    protected void setInClass(boolean inClass) {
        this.inClass = inClass;
        return;
    }


    /**
     *  Gets the estimated duration. 
     *
     *  @return the duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration() {
        return (this.duration);
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.duration = duration;
        return;
    }


    /**
     *  Tests if this <code> Docet </code> has a reading materials expressed 
     *  as <code> Assets. </code> 
     *
     *  @return <code> true </code> if assets are avilable, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean hasAssets() {
        return ((this.assets != null) && (this.assets.size() > 0));
    }


    /**
     *  Gets a list of asset materials distributed to the students. 
     *
     *  @return the asset <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssets() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        if (!hasAssets()) {
            throw new org.osid.IllegalStateException("hasAssets() is false");
        }

        try {
            org.osid.repository.AssetList assets = getAssets();
            return (new net.okapia.osid.jamocha.adapter.converter.repository.asset.AssetToIdList(assets));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the assets distributed to the students. 
     *
     *  @return a list of assets 
     *  @throws org.osid.IllegalStateException <code> hasAssets() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        if (!hasAssets()) {
            throw new org.osid.IllegalStateException("hasAssets() is false");
        }

        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.assets));
    }


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    protected void addAsset(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");
        this.assets.add(asset);
        return;
    }


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    protected void setAssets(java.util.Collection<org.osid.repository.Asset> assets) {
        nullarg(assets, "assets");

        this.assets.clear();
        this.assets.addAll(assets);

        return;
    }


    /**
     *  Tests if this <code> Docet </code> has a quiz or assignment
     *  expressed as an <code> Assessment </code>.
     *
     *  @return <code> true </code> if assessments are avilable,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasAssessments() {
        return ((this.assessments != null) && (this.assessments.size() > 0));
    }


    /**
     *  Gets a list of assessments. 
     *
     *  @return the assessment <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssessments() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssessmentIds() {
        if (!hasAssessments()) {
            throw new org.osid.IllegalStateException("hasAssessments() is false");
        }

        try {
            org.osid.assessment.AssessmentList assessments = getAssessments();
            return (new net.okapia.osid.jamocha.adapter.converter.assessment.assessment.AssessmentToIdList(assessments));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the assessments. 
     *
     *  @return a list of assessments 
     *  @throws org.osid.IllegalStateException <code> hasAssessments() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException {

        if (!hasAssessments()) {
            throw new org.osid.IllegalStateException("hasAssessments() is false");
        }

        return (new net.okapia.osid.jamocha.assessment.assessment.ArrayAssessmentList(this.assessments));
    }


    /**
     *  Adds an assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    protected void addAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");
        this.assessments.add(assessment);
        return;
    }


    /**
     *  Sets all the assessments.
     *
     *  @param assessments a collection of assessments
     *  @throws org.osid.NullArgumentException
     *          <code>assessments</code> is <code>null</code>
     */

    protected void setAssessments(java.util.Collection<org.osid.assessment.Assessment> assessments) {
        nullarg(assessments, "assessments");

        this.assessments.clear();
        this.assessments.addAll(assessments);

        return;
    }


    /**
     *  Tests if this docet supports the given record
     *  <code>Type</code>.
     *
     *  @param  docetRecordType a docet record type 
     *  @return <code>true</code> if the docetRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type docetRecordType) {
        for (org.osid.course.syllabus.records.DocetRecord record : this.records) {
            if (record.implementsRecordType(docetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Docet</code>
     *  record <code>Type</code>.
     *
     *  @param  docetRecordType the docet record type 
     *  @return the docet record 
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(docetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetRecord getDocetRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.DocetRecord record : this.records) {
            if (record.implementsRecordType(docetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this docet. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param docetRecord the docet record
     *  @param docetRecordType docet record type
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecord</code> or
     *          <code>docetRecordTypedocet</code> is <code>null</code>
     */
            
    protected void addDocetRecord(org.osid.course.syllabus.records.DocetRecord docetRecord, 
                                  org.osid.type.Type docetRecordType) {

        nullarg(docetRecord, "docet record");
        addRecordType(docetRecordType);
        this.records.add(docetRecord);
        
        return;
    }
}
