//
// AbstractInstructionLookupSession.java
//
//    A starter implementation framework for providing an Instruction
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Instruction
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getInstructions(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractInstructionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.rules.check.InstructionLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();
    

    /**
     *  Gets the <code>Engine/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this 
     *  session.
     *
     *  @return the <code>Engine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the <code>Engine</code>.
     *
     *  @param  engine the engine for this session
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can perform <code>Instruction</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInstructions() {
        return (true);
    }


    /**
     *  A complete view of the <code>Instruction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInstructionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Instruction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInstructionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include instructions in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active instructions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInstructionView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive instructions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInstructionView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Instruction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Instruction</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Instruction</code> and
     *  retained for compatibility.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionId <code>Id</code> of the
     *          <code>Instruction</code>
     *  @return the instruction
     *  @throws org.osid.NotFoundException <code>instructionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>instructionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Instruction getInstruction(org.osid.id.Id instructionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.rules.check.InstructionList instructions = getInstructions()) {
            while (instructions.hasNext()) {
                org.osid.rules.check.Instruction instruction = instructions.getNextInstruction();
                if (instruction.getId().equals(instructionId)) {
                    return (instruction);
                }
            }
        } 

        throw new org.osid.NotFoundException(instructionId + " not found");
    }


    /**
     *  Gets an <code>InstructionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  instructions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Instructions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInstructions()</code>.
     *
     *  @param  instructionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>instructionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByIds(org.osid.id.IdList instructionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.rules.check.Instruction> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = instructionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getInstruction(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("instruction " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.rules.check.instruction.LinkedInstructionList(ret));
    }


    /**
     *  Gets an <code>InstructionList</code> corresponding to the given
     *  instruction genus <code>Type</code> which does not include
     *  instructions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInstructions()</code>.
     *
     *  @param  instructionGenusType an instruction genus type 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByGenusType(org.osid.type.Type instructionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.InstructionGenusFilterList(getInstructions(), instructionGenusType));
    }


    /**
     *  Gets an <code>InstructionList</code> corresponding to the given
     *  instruction genus <code>Type</code> and include any additional
     *  instructions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInstructions()</code>.
     *
     *  @param  instructionGenusType an instruction genus type 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByParentGenusType(org.osid.type.Type instructionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getInstructionsByGenusType(instructionGenusType));
    }


    /**
     *  Gets an <code>InstructionList</code> containing the given
     *  instruction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInstructions()</code>.
     *
     *  @param  instructionRecordType an instruction record type 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByRecordType(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.InstructionRecordFilterList(getInstructions(), instructionRecordType));
    }


    /**
     *  Gets an <code>InstructionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive instructions are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Instruction</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsOnDate(org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.TemporalInstructionFilterList(getInstructions(), from, to));
    }
        

    /**
     *  Gets a list of instructions corresponding to an agenda
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  agendaId the <code>Id</code> of the agenda
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.rules.check.InstructionList getInstructionsForAgenda(org.osid.id.Id agendaId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.InstructionFilterList(new AgendaFilter(agendaId), getInstructions()));
    }


    /**
     *  Gets a list of instructions corresponding to an agenda
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  agendaId the <code>Id</code> of the agenda
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaOnDate(org.osid.id.Id agendaId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.TemporalInstructionFilterList(getInstructionsForAgenda(agendaId), from, to));
    }


    /**
     *  Gets a list of instructions corresponding to a check
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  checkId the <code>Id</code> of the check
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>checkId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.rules.check.InstructionList getInstructionsForCheck(org.osid.id.Id checkId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.InstructionFilterList(new CheckFilter(checkId), getInstructions()));
    }


    /**
     *  Gets a list of instructions corresponding to a check
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  checkId the <code>Id</code> of the check
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>checkId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForCheckOnDate(org.osid.id.Id checkId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.TemporalInstructionFilterList(getInstructionsForCheck(checkId), from, to));
    }


    /**
     *  Gets a list of instructions corresponding to agenda and check
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  agendaId the <code>Id</code> of the agenda
     *  @param  checkId the <code>Id</code> of the check
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code>,
     *          <code>checkId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaAndCheck(org.osid.id.Id agendaId,
                                                                        org.osid.id.Id checkId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.InstructionFilterList(new CheckFilter(checkId), getInstructionsForAgenda(agendaId)));
    }


    /**
     *  Gets a list of instructions corresponding to agenda and check
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  checkId the <code>Id</code> of the check
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>InstructionList</code>
     *  @throws org.osid.NullArgumentException <code>agendaId</code>,
     *          <code>checkId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaAndCheckOnDate(org.osid.id.Id agendaId,
                                                                                       org.osid.id.Id checkId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.TemporalInstructionFilterList(getInstructionsForAgendaAndCheck(agendaId, checkId), from, to));
    }


    /**
     *  Gets all <code>Instructions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @return a list of <code>Instructions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.rules.check.InstructionList getInstructions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the instruction list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of instructions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.rules.check.InstructionList filterInstructionsOnViews(org.osid.rules.check.InstructionList list)
        throws org.osid.OperationFailedException {

        org.osid.rules.check.InstructionList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.rules.check.instruction.ActiveInstructionFilterList(ret);
        }

        return (ret);
    }

    public static class AgendaFilter
        implements net.okapia.osid.jamocha.inline.filter.rules.check.instruction.InstructionFilter {
         
        private final org.osid.id.Id agendaId;
         
         
        /**
         *  Constructs a new <code>AgendaFilter</code>.
         *
         *  @param agendaId the agenda to filter
         *  @throws org.osid.NullArgumentException
         *          <code>agendaId</code> is <code>null</code>
         */
        
        public AgendaFilter(org.osid.id.Id agendaId) {
            nullarg(agendaId, "agenda Id");
            this.agendaId = agendaId;
            return;
        }

         
        /**
         *  Used by the InstructionFilterList to filter the 
         *  instruction list based on agenda.
         *
         *  @param instruction the instruction
         *  @return <code>true</code> to pass the instruction,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.rules.check.Instruction instruction) {
            return (instruction.getAgendaId().equals(this.agendaId));
        }
    }


    public static class CheckFilter
        implements net.okapia.osid.jamocha.inline.filter.rules.check.instruction.InstructionFilter {
         
        private final org.osid.id.Id checkId;
         
         
        /**
         *  Constructs a new <code>CheckFilter</code>.
         *
         *  @param checkId the check to filter
         *  @throws org.osid.NullArgumentException
         *          <code>checkId</code> is <code>null</code>
         */
        
        public CheckFilter(org.osid.id.Id checkId) {
            nullarg(checkId, "check Id");
            this.checkId = checkId;
            return;
        }

         
        /**
         *  Used by the InstructionFilterList to filter the 
         *  instruction list based on check.
         *
         *  @param instruction the instruction
         *  @return <code>true</code> to pass the instruction,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.rules.check.Instruction instruction) {
            return (instruction.getCheckId().equals(this.checkId));
        }
    }
}
