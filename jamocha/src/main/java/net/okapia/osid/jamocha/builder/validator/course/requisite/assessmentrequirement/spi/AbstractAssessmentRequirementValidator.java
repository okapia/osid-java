//
// AbstractAssessmentRequirementValidator.java
//
//     Validates an AssessmentRequirement.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.requisite.assessmentrequirement.spi;


/**
 *  Validates an AssessmentRequirement.
 */

public abstract class AbstractAssessmentRequirementValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractAssessmentRequirementValidator</code>.
     */

    protected AbstractAssessmentRequirementValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractAssessmentRequirementValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractAssessmentRequirementValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an AssessmentRequirement.
     *
     *  @param assessmentRequirement an assessment requirement to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>assessmentRequirement</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {
        super.validate(assessmentRequirement);

        test(assessmentRequirement.getAltRequisites(), "getAltRequisites()");
        testNestedObject(assessmentRequirement, "getAssessment");

        testConditionalMethod(assessmentRequirement, "getTimeframe", assessmentRequirement.hasTimeframe(), "hasTimeframe()");

        testConditionalMethod(assessmentRequirement, "getMinimumGradeId", assessmentRequirement.hasMinimumGrade(), "hasMinimumGrade()");
        testConditionalMethod(assessmentRequirement, "getMinimumGrade", assessmentRequirement.hasMinimumGrade(), "hasMinimumGrade()");
        if (assessmentRequirement.hasMinimumGrade()) {
            testNestedObject(assessmentRequirement, "getMinimumGrade");
        }

        testConditionalMethod(assessmentRequirement, "getMinimumScoreSystemId", assessmentRequirement.hasMinimumScore(), "hasMinimumScore()");
        testConditionalMethod(assessmentRequirement, "getMinimumScoreSystem", assessmentRequirement.hasMinimumScore(), "hasMinimumScore()");
        testConditionalMethod(assessmentRequirement, "getMinimumScore", assessmentRequirement.hasMinimumScore(), "hasMinimumScore()");
        if (assessmentRequirement.hasMinimumScore()) {
            testNestedObject(assessmentRequirement, "getMinimumScoreSystem");
        }

        return;
    }
}
