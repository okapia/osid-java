//
// AbstractMapFrontOfficeLookupSession
//
//    A simple framework for providing a FrontOffice lookup service
//    backed by a fixed collection of front offices.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a FrontOffice lookup service backed by a
 *  fixed collection of front offices. The front offices are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>FrontOffices</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapFrontOfficeLookupSession
    extends net.okapia.osid.jamocha.tracking.spi.AbstractFrontOfficeLookupSession
    implements org.osid.tracking.FrontOfficeLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.tracking.FrontOffice> frontOffices = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.tracking.FrontOffice>());


    /**
     *  Makes a <code>FrontOffice</code> available in this session.
     *
     *  @param  frontOffice a front office
     *  @throws org.osid.NullArgumentException <code>frontOffice<code>
     *          is <code>null</code>
     */

    protected void putFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        this.frontOffices.put(frontOffice.getId(), frontOffice);
        return;
    }


    /**
     *  Makes an array of front offices available in this session.
     *
     *  @param  frontOffices an array of front offices
     *  @throws org.osid.NullArgumentException <code>frontOffices<code>
     *          is <code>null</code>
     */

    protected void putFrontOffices(org.osid.tracking.FrontOffice[] frontOffices) {
        putFrontOffices(java.util.Arrays.asList(frontOffices));
        return;
    }


    /**
     *  Makes a collection of front offices available in this session.
     *
     *  @param  frontOffices a collection of front offices
     *  @throws org.osid.NullArgumentException <code>frontOffices<code>
     *          is <code>null</code>
     */

    protected void putFrontOffices(java.util.Collection<? extends org.osid.tracking.FrontOffice> frontOffices) {
        for (org.osid.tracking.FrontOffice frontOffice : frontOffices) {
            this.frontOffices.put(frontOffice.getId(), frontOffice);
        }

        return;
    }


    /**
     *  Removes a FrontOffice from this session.
     *
     *  @param  frontOfficeId the <code>Id</code> of the front office
     *  @throws org.osid.NullArgumentException <code>frontOfficeId<code> is
     *          <code>null</code>
     */

    protected void removeFrontOffice(org.osid.id.Id frontOfficeId) {
        this.frontOffices.remove(frontOfficeId);
        return;
    }


    /**
     *  Gets the <code>FrontOffice</code> specified by its <code>Id</code>.
     *
     *  @param  frontOfficeId <code>Id</code> of the <code>FrontOffice</code>
     *  @return the frontOffice
     *  @throws org.osid.NotFoundException <code>frontOfficeId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>frontOfficeId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.tracking.FrontOffice frontOffice = this.frontOffices.get(frontOfficeId);
        if (frontOffice == null) {
            throw new org.osid.NotFoundException("frontOffice not found: " + frontOfficeId);
        }

        return (frontOffice);
    }


    /**
     *  Gets all <code>FrontOffices</code>. In plenary mode, the returned
     *  list contains all known frontOffices or an error
     *  results. Otherwise, the returned list may contain only those
     *  frontOffices that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>FrontOffices</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.frontoffice.ArrayFrontOfficeList(this.frontOffices.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.frontOffices.clear();
        super.close();
        return;
    }
}
