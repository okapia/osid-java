//
// MutableMapProxySequenceRuleLookupSession
//
//    Implements a SequenceRule lookup service backed by a collection of
//    sequenceRules that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements a SequenceRule lookup service backed by a collection of
 *  sequenceRules. The sequenceRules are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of sequence rules can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxySequenceRuleLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractMapSequenceRuleLookupSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxySequenceRuleLookupSession}
     *  with no sequence rules.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxySequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.proxy.Proxy proxy) {
        setBank(bank);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxySequenceRuleLookupSession} with a
     *  single sequence rule.
     *
     *  @param bank the bank
     *  @param sequenceRule a sequence rule
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRule}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.authoring.SequenceRule sequenceRule, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putSequenceRule(sequenceRule);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySequenceRuleLookupSession} using an
     *  array of sequence rules.
     *
     *  @param bank the bank
     *  @param sequenceRules an array of sequence rules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRules}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.authoring.SequenceRule[] sequenceRules, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putSequenceRules(sequenceRules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySequenceRuleLookupSession} using a
     *  collection of sequence rules.
     *
     *  @param bank the bank
     *  @param sequenceRules a collection of sequence rules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRules}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                java.util.Collection<? extends org.osid.assessment.authoring.SequenceRule> sequenceRules,
                                                org.osid.proxy.Proxy proxy) {
   
        this(bank, proxy);
        setSessionProxy(proxy);
        putSequenceRules(sequenceRules);
        return;
    }

    
    /**
     *  Makes a {@code SequenceRule} available in this session.
     *
     *  @param sequenceRule an sequence rule
     *  @throws org.osid.NullArgumentException {@code sequenceRule{@code 
     *          is {@code null}
     */

    @Override
    public void putSequenceRule(org.osid.assessment.authoring.SequenceRule sequenceRule) {
        super.putSequenceRule(sequenceRule);
        return;
    }


    /**
     *  Makes an array of sequenceRules available in this session.
     *
     *  @param sequenceRules an array of sequence rules
     *  @throws org.osid.NullArgumentException {@code sequenceRules{@code 
     *          is {@code null}
     */

    @Override
    public void putSequenceRules(org.osid.assessment.authoring.SequenceRule[] sequenceRules) {
        super.putSequenceRules(sequenceRules);
        return;
    }


    /**
     *  Makes collection of sequence rules available in this session.
     *
     *  @param sequenceRules
     *  @throws org.osid.NullArgumentException {@code sequenceRule{@code 
     *          is {@code null}
     */

    @Override
    public void putSequenceRules(java.util.Collection<? extends org.osid.assessment.authoring.SequenceRule> sequenceRules) {
        super.putSequenceRules(sequenceRules);
        return;
    }


    /**
     *  Removes a SequenceRule from this session.
     *
     *  @param sequenceRuleId the {@code Id} of the sequence rule
     *  @throws org.osid.NullArgumentException {@code sequenceRuleId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSequenceRule(org.osid.id.Id sequenceRuleId) {
        super.removeSequenceRule(sequenceRuleId);
        return;
    }    
}
