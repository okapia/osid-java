//
// AbstractAction.java
//
//     Defines an Action.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.action.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Action</code>.
 */

public abstract class AbstractAction
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.control.Action {

    private org.osid.control.ActionGroup actionGroup;
    private org.osid.calendaring.Duration delay;
    private boolean isBlocking = true;
    private org.osid.control.ActionGroup nextActionGroup;
    private org.osid.control.Scene scene;
    private org.osid.control.Setting setting;
    private org.osid.control.Controller matchingController;
    private java.math.BigDecimal matchingAmountFactor;
    private java.math.BigDecimal matchingRateFactor;

    private final java.util.Collection<org.osid.control.records.ActionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the action group <code> Id </code> for this action. 
     *
     *  @return the action group <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActionGroupId() {
        return (this.actionGroup.getId());
    }


    /**
     *  Gets the action group for this action. 
     *
     *  @return the action group 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getActionGroup()
        throws org.osid.OperationFailedException {

        return (this.actionGroup);
    }


    /**
     *  Sets the action group.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    protected void setActionGroup(org.osid.control.ActionGroup actionGroup) {
        nullarg(actionGroup, "action group");
        this.actionGroup = actionGroup;
        return;
    }


    /**
     *  Gets the delay before proceeding with execution. 
     *
     *  @return the delay 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDelay() {
        return (this.delay);
    }


    /**
     *  Sets the delay.
     *
     *  @param delay a delay
     *  @throws org.osid.NullArgumentException
     *          <code>delay</code> is <code>null</code>
     */

    protected void setDelay(org.osid.calendaring.Duration delay) {
        nullarg(delay, "delay");
        this.delay = delay;
        return;
    }


    /**
     *  Tests if this action blocks further actions until complete. 
     *
     *  @return <code> true </code> if blocking, <code> false </code> if 
     *          subsequent actions can begin before this action completes 
     */

    @OSID @Override
    public boolean isBlocking() {
        return (this.isBlocking);
    }


    /**
     *  Sets the blocking.
     *
     *  @param blocking a blocking
     */

    protected void setBlocking(boolean blocking) {
        this.isBlocking = blocking;
        return;
    }


    /**
     *  Tests if this rule executes a scene. If <code>
     *  executesActionGroup() </code> is <code> true, </code> <code>
     *  hasRule(), executesScene(), executesSetting(), </code> and
     *  <code> executesMatchingSetting() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this is a scene execution,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean executesActionGroup() {
        return (this.nextActionGroup != null);
    }


    /**
     *  Gets the action group <code> Id </code> to execute. 
     *
     *  @return the action group <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> executesActionGroup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getNextActionGroupId() {
        return (this.nextActionGroup.getId());
    }


    /**
     *  Gets the action group to execute, 
     *
     *  @return the action group 
     *  @throws org.osid.IllegalStateException <code> executesActionGroup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getNextActionGroup()
        throws org.osid.OperationFailedException {

        if (!executesActionGroup()) {
            throw new org.osid.IllegalStateException("executesActionGroup() is false");
        }

        return (this.nextActionGroup);
    }


    /**
     *  Sets the next action group.
     *
     *  @param actionGroup a next action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    protected void setNextActionGroup(org.osid.control.ActionGroup actionGroup) {
        nullarg(actionGroup, "action group");
        clearStuff();
        this.nextActionGroup = actionGroup;
        return;
    }


    /**
     *  Tests if this rule executes a scene. If <code> executesScene()
     *  </code> is <code> true, </code> <code> hasRule(),
     *  executesActionGroup(), executesSetting(), </code> and <code>
     *  executesMatchingSetting() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this is a scene execution,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean executesScene() {
        return (this.scene != null);
    }


    /**
     *  Gets the scene <code> Id. </code> 
     *
     *  @return the scene <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> executesScene() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSceneId() {
        return (this.scene.getId());
    }


    /**
     *  Gets the scene. 
     *
     *  @return the scene 
     *  @throws org.osid.IllegalStateException <code> executesScene() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Scene getScene()
        throws org.osid.OperationFailedException {

        if (!executesScene()) {
            throw new org.osid.IllegalStateException("executesScene() is false");
        }

        return (this.scene);
    }


    /**
     *  Sets the scene.
     *
     *  @param scene a scene
     *  @throws org.osid.NullArgumentException <code>scene</code> is
     *          <code>null</code>
     */

    protected void setScene(org.osid.control.Scene scene) {
        nullarg(scene, "scene");
        clearStuff();
        this.scene = scene;
        return;
    }


    /**
     *  Tests if this rule executes a setting. If <code>
     *  executesSetting() </code> is <code> true, </code> <code>
     *  hasRule(), executesActionGroup(), executesScene(), </code> and
     *  <code> executesMatchingSetting() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this is a setting execution,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean executesSetting() {
        return (this.setting != null);
    }


    /**
     *  Gets the setting <code> Id. </code> 
     *
     *  @return the setting <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code>
     *          executesSetting() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getSettingId() {
        return (this.setting.getId());
    }


    /**
     *  Gets the setting. 
     *
     *  @return the setting 
     *  @throws org.osid.IllegalStateException <code> executesSetting() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Setting getSetting()
        throws org.osid.OperationFailedException {

        if (!executesSetting()) {
            throw new org.osid.IllegalStateException("executesSetting() is false");
        }

        return (this.setting);
    }


    /**
     *  Sets the setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException <code>setting</code> is
     *          <code>null</code>
     */

    protected void setSetting(org.osid.control.Setting setting) {
        nullarg(setting, "setting");
        clearStuff();
        this.setting = setting;
        return;
    }


    /**
     *  Tests if this rule executes a matching setting. If <code>
     *  executesMatchingSetting() </code> is <code> true, </code>
     *  <code> hasRule(), </code> <code> executesScene(),
     *  executesActionGroup(), </code> and <code> executesSetting()
     *  </code> must be <code> false.  </code>
     *
     *  @return <code> true </code> if this is a setting execution, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean executesMatchingSetting() {
        return ((this.matchingController != null) && 
                (this.matchingAmountFactor != null) &&
                (this.matchingRateFactor != null));
    }


    /**
     *  Gets the matching controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMatchingControllerId() {
        return (this.matchingController.getId());
    }


    /**
     *  Gets the matching controller. 
     *
     *  @return the controller 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getMatchingController()
        throws org.osid.OperationFailedException {

        if (!executesMatchingSetting()) {
            throw new org.osid.IllegalStateException("executesMatchingSetting() is false");
        }

        return (this.matchingController);
    }


    /**
     *  Sets the matching controller.
     *
     *  @param controller a matching controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    protected void setMatchingController(org.osid.control.Controller controller) {
        nullarg(controller, "matching controller");
        clearStuff();
        this.matchingController = matchingController;
        return;
    }


    /**
     *  Gets the factor by which the matching amount differs. A factor of 1 
     *  matches the amount of the matching controller. 
     *
     *  @return the factor 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMatchingAmountFactor() {
        if (!executesMatchingSetting()) {
            throw new org.osid.IllegalStateException("executesMatchingSetting() is false");
        }

        return (this.matchingAmountFactor);
    }


    /**
     *  Sets the matching amount factor.
     *
     *  @param factor a matching amount factor
     *  @throws org.osid.NullArgumentException <code>factor</code> is
     *          <code>null</code>
     */

    protected void setMatchingAmountFactor(java.math.BigDecimal factor) {
        nullarg(factor, "matching amount factor");
        this.matchingAmountFactor = factor;
        return;
    }


    /**
     *  Gets the factor by which the matching transition rate differs. A 
     *  factor of 1 matches the ramp rate of the matching controller. 
     *
     *  @return the factor 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMatchingRateFactor() {
        return (this.matchingRateFactor);
    }


    /**
     *  Sets the matching rate factor.
     *
     *  @param factor a matching rate factor
     *  @throws org.osid.NullArgumentException <code>factor</code> is
     *          <code>null</code>
     */

    protected void setMatchingRateFactor(java.math.BigDecimal factor) {
        nullarg(factor, "matching rate factor");
        this.matchingRateFactor = factor;
        return;
    }


    protected void clearStuff() {
        this.nextActionGroup    = null;
        this.scene              = null;
        this.setting            = null;
        this.matchingController = null;

        return;
    }


    /**
     *  Tests if this action supports the given record
     *  <code>Type</code>.
     *
     *  @param  actionRecordType an action record type 
     *  @return <code>true</code> if the actionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type actionRecordType) {
        for (org.osid.control.records.ActionRecord record : this.records) {
            if (record.implementsRecordType(actionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  actionRecordType the action record type 
     *  @return the action record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionRecord getActionRecord(org.osid.type.Type actionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionRecord record : this.records) {
            if (record.implementsRecordType(actionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param actionRecord the action record
     *  @param actionRecordType action record type
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecord</code> or
     *          <code>actionRecordTypeaction</code> is
     *          <code>null</code>
     */
            
    protected void addActionRecord(org.osid.control.records.ActionRecord actionRecord, 
                                     org.osid.type.Type actionRecordType) {

        addRecordType(actionRecordType);
        this.records.add(actionRecord);
        
        return;
    }
}
