//
// AbstractRequestSearch.java
//
//     A template for making a Request Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing request searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRequestSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.RequestSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.RequestSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.RequestSearchOrder requestSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of requests. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  requestIds list of requests
     *  @throws org.osid.NullArgumentException
     *          <code>requestIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRequests(org.osid.id.IdList requestIds) {
        while (requestIds.hasNext()) {
            try {
                this.ids.add(requestIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRequests</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of request Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRequestIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  requestSearchOrder request search order 
     *  @throws org.osid.NullArgumentException
     *          <code>requestSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>requestSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRequestResults(org.osid.provisioning.RequestSearchOrder requestSearchOrder) {
	this.requestSearchOrder = requestSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.RequestSearchOrder getRequestSearchOrder() {
	return (this.requestSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given request search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a request implementing the requested record.
     *
     *  @param requestSearchRecordType a request search record
     *         type
     *  @return the request search record
     *  @throws org.osid.NullArgumentException
     *          <code>requestSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestSearchRecord getRequestSearchRecord(org.osid.type.Type requestSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.records.RequestSearchRecord record : this.records) {
            if (record.implementsRecordType(requestSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this request search. 
     *
     *  @param requestSearchRecord request search record
     *  @param requestSearchRecordType request search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRequestSearchRecord(org.osid.provisioning.records.RequestSearchRecord requestSearchRecord, 
                                           org.osid.type.Type requestSearchRecordType) {

        addRecordType(requestSearchRecordType);
        this.records.add(requestSearchRecord);        
        return;
    }
}
