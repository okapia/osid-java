//
// AbstractSequenceRuleLookupSession.java
//
//    A starter implementation framework for providing a SequenceRule
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a SequenceRule
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSequenceRules(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSequenceRuleLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();
    

    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }

    /**
     *  Tests if this user can perform <code>SequenceRule</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSequenceRules() {
        return (true);
    }


    /**
     *  A complete view of the <code>SequenceRule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSequenceRuleView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>SequenceRule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySequenceRuleView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rules in banks which are
     *  children of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active sequence rules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSequenceRuleView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive sequence rules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSequenceRuleView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SequenceRule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code>SequenceRule</code> may have a different <code> Id
     *  </code> than requested, such as the case where a duplicate
     *  <code> Id </code> was assigned to a <code>SequenceRule</code>
     *  and retained for compatibility.  In active mode, sequence
     *  rules are returned that are currently active. In any status
     *  mode, active and inactive sequence rules are returned.
     *
     *  @param  sequenceRuleId <code>Id</code> of the
     *          <code>SequenceRule</code>
     *  @return the sequence rule
     *  @throws org.osid.NotFoundException <code>sequenceRuleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>sequenceRuleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRule getSequenceRule(org.osid.id.Id sequenceRuleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.assessment.authoring.SequenceRuleList sequenceRules = getSequenceRules()) {
            while (sequenceRules.hasNext()) {
                org.osid.assessment.authoring.SequenceRule sequenceRule = sequenceRules.getNextSequenceRule();
                if (sequenceRule.getId().equals(sequenceRuleId)) {
                    return (sequenceRule);
                }
            }
        } 

        throw new org.osid.NotFoundException(sequenceRuleId + " not found");
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  sequenceRules specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>SequenceRules</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSequenceRules()</code>.
     *
     *  @param  sequenceRuleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByIds(org.osid.id.IdList sequenceRuleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.authoring.SequenceRule> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = sequenceRuleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSequenceRule(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("sequence rule " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.assessment.authoring.sequencerule.LinkedSequenceRuleList(ret));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  sequence rule genus <code>Type</code> which does not include
     *  sequence rules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rules or an error results. Otherwise, the returned list may
     *  contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSequenceRules()</code>.
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleGenusFilterList(getSequenceRules(), sequenceRuleGenusType)); 
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  sequence rule genus <code>Type</code> and include any additional
     *  sequence rules with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSequenceRules()</code>.
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByParentGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSequenceRulesByGenusType(sequenceRuleGenusType));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> containing the given
     *  sequence rule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSequenceRules()</code>.
     *
     *  @param  sequenceRuleRecordType a sequenceRule record type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByRecordType(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleRecordFilterList(getSequenceRules(), sequenceRuleRecordType)); 
    }


    /**
     *  Gets a <code>SequenceRuleList</code> for the given source
     *  assessment part.
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentPartId an assessment part <code> Id </code>
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessmentPart(org.osid.id.Id assessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleFilterList(new AssessmentPartFilter(assessmentPartId), getSequenceRules()));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> for the given target
     *  assessment part.
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  nextAssessmentPartId an assessment part <code> Id </code> 
     *  @return the returned <code>SequenceRule</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>nextAssessmentPartId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForNextAssessmentPart(org.osid.id.Id nextAssessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleFilterList(new NextAssessmentPartFilter(nextAssessmentPartId), getSequenceRules()));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> for the given source and
     *  target assessment parts.
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentPartId source assessment part <code>Id</code> 
     *  @param  nextAssessmentPartId target assessment part <code>Id</code> 
     *  @return the returned <code>SequenceRule</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartId</code> or
     *          <code>nextAssessmentPartId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessmentParts(org.osid.id.Id assessmentPartId, 
                                                                                             org.osid.id.Id nextAssessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleFilterList(new AssessmentPartsFilter(assessmentPartId, nextAssessmentPartId), getSequenceRules()));        
    }


    /**
     *  Gets a <code>SequenceRuleList</code> for an entire assessment. 
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentId an assessment <code>Id</code> 
     *  @return the returned <code>SequenceRule</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.authoring.SequenceRule> ret = new java.util.ArrayList<>();

        try (org.osid.assessment.authoring.SequenceRuleList rules = getSequenceRules()) {
            while (rules.hasNext()) {
                org.osid.assessment.authoring.SequenceRule rule = rules.getNextSequenceRule();
                if (rule.getAssessmentPart().getAssessmentId().equals(assessmentId)) {
                    ret.add(rule);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.assessment.authoring.sequencerule.LinkedSequenceRuleList(ret));
    }


    /**
     *  Gets all <code>SequenceRules</code>. 
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rules or an error results. Otherwise, the returned list may
     *  contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @return a list of <code>SequenceRules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.assessment.authoring.SequenceRuleList getSequenceRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the sequence rule list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of sequence rules
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.assessment.authoring.SequenceRuleList filterSequenceRulesOnViews(org.osid.assessment.authoring.SequenceRuleList list)
        throws org.osid.OperationFailedException {
            
        org.osid.assessment.authoring.SequenceRuleList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.ActiveSequenceRuleFilterList(ret);
        }

        return (ret);
    }


    public static class AssessmentPartFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleFilter {

        private final org.osid.id.Id assessmentPartId;

        
        /**
         *  Constructs a new <code>AssessmentPartFilter</code>.
         *
         *  @param assessmentPartId the assessment part to filter
         *  @throws org.osid.NullArgumentException
         *          <code>assessmentPartId</code> is <code>null</code>
         */

        public AssessmentPartFilter(org.osid.id.Id assessmentPartId) {
            nullarg(assessmentPartId, "assessment part Id");
            this.assessmentPartId = assessmentPartId;
            return;
        }


        /**
         *  Used by the SequenceRuleFilterList to filter the list
         *  based on assessment part.
         *
         *  @param sequenceRule the sequence rule
         *  @return <code>true</code> to pass the sequence rule,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.authoring.SequenceRule sequenceRule) {
            if (sequenceRule.getAssessmentPartId().equals(this.assessmentPartId)) {
                return (true);
            } else {
                return (false);
            }
        }
    }


    public static class NextAssessmentPartFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleFilter {

        private final org.osid.id.Id nextAssessmentPartId;

        
        /**
         *  Constructs a new <code>NextAssessmentPartFilter</code>.
         *
         *  @param nextAssessmentPartId the next assessment part to filter
         *  @throws org.osid.NullArgumentException
         *          <code>assessmentPartId</code> is <code>null</code>
         */

        public NextAssessmentPartFilter(org.osid.id.Id nextAssessmentPartId) {
            nullarg(nextAssessmentPartId, "next assessment part Id");
            this.nextAssessmentPartId = nextAssessmentPartId;
            return;
        }


        /**
         *  Used by the SequenceRuleFilterList to filter the list
         *  based on next assessment part.
         *
         *  @param sequenceRule the sequence rule
         *  @return <code>true</code> to pass the sequence rule,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.authoring.SequenceRule sequenceRule) {
            if (sequenceRule.getNextAssessmentPartId().equals(this.nextAssessmentPartId)) {
                return (true);
            } else {
                return (false);
            }
        }
    }


    public static class AssessmentPartsFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.authoring.sequencerule.SequenceRuleFilter {

        private final org.osid.id.Id assessmentPartId;
        private final org.osid.id.Id nextAssessmentPartId;

        
        /**
         *  Constructs a new <code>AssessmentPartsFilter</code>.
         *
         *  @param assessmentPartId the assessment part to filter
         *  @param nextAssessmentPartId the next assessment part to filter
         *  @throws org.osid.NullArgumentException
         *          <code>assessmentPartId</code> or
         *          <code>nextAssessmentPartId</code> is
         *          <code>null</code>
         */

        public AssessmentPartsFilter(org.osid.id.Id assessmentPartId,
                                     org.osid.id.Id nextAssessmentPartId) {

            nullarg(assessmentPartId, "assessment part Id");
            nullarg(nextAssessmentPartId, "next assessment part Id");

            this.assessmentPartId = assessmentPartId;
            this.nextAssessmentPartId = nextAssessmentPartId;

            return;
        }


        /**
         *  Used by the SequenceRuleFilterList to filter the list
         *  based on next assessment part.
         *
         *  @param sequenceRule the sequence rule
         *  @return <code>true</code> to pass the sequence rule,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.authoring.SequenceRule sequenceRule) {
            if (sequenceRule.getAssessmentPartId().equals(this.assessmentPartId) &&
                sequenceRule.getNextAssessmentPartId().equals(this.nextAssessmentPartId)) {
                return (true);
            } else {
                return (false);
            }
        }
    }
}
