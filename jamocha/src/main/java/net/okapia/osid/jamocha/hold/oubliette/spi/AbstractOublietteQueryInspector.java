//
// AbstractOublietteQueryInspector.java
//
//     A template for making an OublietteQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.oubliette.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for oubliettes.
 */

public abstract class AbstractOublietteQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.hold.OublietteQueryInspector {

    private final java.util.Collection<org.osid.hold.records.OublietteQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the block <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBlockIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the block query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.BlockQueryInspector[] getBlockTerms() {
        return (new org.osid.hold.BlockQueryInspector[0]);
    }


    /**
     *  Gets the issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.hold.IssueQueryInspector[0]);
    }


    /**
     *  Gets the hold <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getHoldIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the hold query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.HoldQueryInspector[] getHoldTerms() {
        return (new org.osid.hold.HoldQueryInspector[0]);
    }


    /**
     *  Gets the ancestor oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOublietteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getAncestorOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }


    /**
     *  Gets the descendant oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOublietteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getDescendantOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given oubliette query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an oubliette implementing the requested record.
     *
     *  @param oublietteRecordType an oubliette record type
     *  @return the oubliette query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(oublietteRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.OublietteQueryInspectorRecord getOublietteQueryInspectorRecord(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.OublietteQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(oublietteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(oublietteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this oubliette query. 
     *
     *  @param oublietteQueryInspectorRecord oubliette query inspector
     *         record
     *  @param oublietteRecordType oubliette record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOublietteQueryInspectorRecord(org.osid.hold.records.OublietteQueryInspectorRecord oublietteQueryInspectorRecord, 
                                                   org.osid.type.Type oublietteRecordType) {

        addRecordType(oublietteRecordType);
        nullarg(oublietteRecordType, "oubliette record type");
        this.records.add(oublietteQueryInspectorRecord);        
        return;
    }
}
