//
// AbstractOfficeLookupSession.java
//
//    A starter implementation framework for providing an Office
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an Office
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getOffices(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractOfficeLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.OfficeLookupSession {

    private boolean pedantic = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    


    /**
     *  Tests if this user can perform <code>Office</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOffices() {
        return (true);
    }


    /**
     *  A complete view of the <code>Office</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOfficeView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Office</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOfficeView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Office</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Office</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Office</code> and retained for
     *  compatibility.
     *
     *  @param  officeId <code>Id</code> of the
     *          <code>Office</code>
     *  @return the office
     *  @throws org.osid.NotFoundException <code>officeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>officeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.OfficeList offices = getOffices()) {
            while (offices.hasNext()) {
                org.osid.workflow.Office office = offices.getNextOffice();
                if (office.getId().equals(officeId)) {
                    return (office);
                }
            }
        } 

        throw new org.osid.NotFoundException(officeId + " not found");
    }


    /**
     *  Gets an <code>OfficeList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the offices
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Offices</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getOffices()</code>.
     *
     *  @param  officeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>officeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByIds(org.osid.id.IdList officeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.Office> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = officeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getOffice(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("office " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.office.LinkedOfficeList(ret));
    }


    /**
     *  Gets an <code>OfficeList</code> corresponding to the given
     *  office genus <code>Type</code> which does not include offices
     *  of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getOffices()</code>.
     *
     *  @param  officeGenusType an office genus type 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByGenusType(org.osid.type.Type officeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.office.OfficeGenusFilterList(getOffices(), officeGenusType));
    }


    /**
     *  Gets an <code>OfficeList</code> corresponding to the given
     *  office genus <code>Type</code> and include any additional
     *  offices with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOffices()</code>.
     *
     *  @param  officeGenusType an office genus type 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByParentGenusType(org.osid.type.Type officeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOfficesByGenusType(officeGenusType));
    }


    /**
     *  Gets an <code>OfficeList</code> containing the given office
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOffices()</code>.
     *
     *  @param  officeRecordType an office record type 
     *  @return the returned <code>Office</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByRecordType(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.office.OfficeRecordFilterList(getOffices(), officeRecordType));
    }


    /**
     *  Gets an <code>OfficeList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Office</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.workflow.office.OfficeProviderFilterList(getOffices(), resourceId));
    }


    /**
     *  Gets all <code>Offices</code>.
     *
     *  In plenary mode, the returned list contains all known offices
     *  or an error results. Otherwise, the returned list may contain
     *  only those offices that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Offices</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.OfficeList getOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the office list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of offices
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.OfficeList filterOfficesOnViews(org.osid.workflow.OfficeList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
