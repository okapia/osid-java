//
// MutableDeviceBatchFormList.java
//
//     Implements a DeviceBatchFormList. This list allows DeviceBatchForms to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.batch.devicebatchform;


/**
 *  <p>Implements a DeviceBatchFormList. This list allows DeviceBatchForms to be
 *  added after this deviceBatchForm has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this deviceBatchForm must
 *  invoke <code>eol()</code> when there are no more deviceBatchForms to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>DeviceBatchFormList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more deviceBatchForms are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more deviceBatchForms to be added.</p>
 */

public final class MutableDeviceBatchFormList
    extends net.okapia.osid.jamocha.control.batch.devicebatchform.spi.AbstractMutableDeviceBatchFormList
    implements org.osid.control.batch.DeviceBatchFormList {


    /**
     *  Creates a new empty <code>MutableDeviceBatchFormList</code>.
     */

    public MutableDeviceBatchFormList() {
        super();
    }


    /**
     *  Creates a new <code>MutableDeviceBatchFormList</code>.
     *
     *  @param deviceBatchForm a <code>DeviceBatchForm</code>
     *  @throws org.osid.NullArgumentException <code>deviceBatchForm</code>
     *          is <code>null</code>
     */

    public MutableDeviceBatchFormList(org.osid.control.batch.DeviceBatchForm deviceBatchForm) {
        super(deviceBatchForm);
        return;
    }


    /**
     *  Creates a new <code>MutableDeviceBatchFormList</code>.
     *
     *  @param array an array of devicebatchforms
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableDeviceBatchFormList(org.osid.control.batch.DeviceBatchForm[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableDeviceBatchFormList</code>.
     *
     *  @param collection a java.util.Collection of devicebatchforms
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableDeviceBatchFormList(java.util.Collection<org.osid.control.batch.DeviceBatchForm> collection) {
        super(collection);
        return;
    }
}
