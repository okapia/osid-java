//
// AbstractAdapterDirectionLookupSession.java
//
//    A Direction lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Direction lookup session adapter.
 */

public abstract class AbstractAdapterDirectionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recipe.DirectionLookupSession {

    private final org.osid.recipe.DirectionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDirectionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDirectionLookupSession(org.osid.recipe.DirectionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Cookbook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Cookbook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.session.getCookbookId());
    }


    /**
     *  Gets the {@code Cookbook} associated with this session.
     *
     *  @return the {@code Cookbook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCookbook());
    }


    /**
     *  Tests if this user can perform {@code Direction} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDirections() {
        return (this.session.canLookupDirections());
    }


    /**
     *  A complete view of the {@code Direction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDirectionView() {
        this.session.useComparativeDirectionView();
        return;
    }


    /**
     *  A complete view of the {@code Direction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDirectionView() {
        this.session.usePlenaryDirectionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include directions in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.session.useFederatedCookbookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        this.session.useIsolatedCookbookView();
        return;
    }
    
     
    /**
     *  Gets the {@code Direction} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Direction} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Direction} and
     *  retained for compatibility.
     *
     *  @param directionId {@code Id} of the {@code Direction}
     *  @return the direction
     *  @throws org.osid.NotFoundException {@code directionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code directionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Direction getDirection(org.osid.id.Id directionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirection(directionId));
    }


    /**
     *  Gets a {@code DirectionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  directions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Directions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  directionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Direction} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code directionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByIds(org.osid.id.IdList directionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectionsByIds(directionIds));
    }


    /**
     *  Gets a {@code DirectionList} corresponding to the given
     *  direction genus {@code Type} which does not include
     *  directions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directionGenusType a direction genus type 
     *  @return the returned {@code Direction} list
     *  @throws org.osid.NullArgumentException
     *          {@code directionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByGenusType(org.osid.type.Type directionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectionsByGenusType(directionGenusType));
    }


    /**
     *  Gets a {@code DirectionList} corresponding to the given
     *  direction genus {@code Type} and include any additional
     *  directions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directionGenusType a direction genus type 
     *  @return the returned {@code Direction} list
     *  @throws org.osid.NullArgumentException
     *          {@code directionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByParentGenusType(org.osid.type.Type directionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectionsByParentGenusType(directionGenusType));
    }


    /**
     *  Gets a {@code DirectionList} containing the given
     *  direction record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directionRecordType a direction record type 
     *  @return the returned {@code Direction} list
     *  @throws org.osid.NullArgumentException
     *          {@code directionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsByRecordType(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirectionsByRecordType(directionRecordType));
    }


    /**
     *  Gets a {@code DirectionList} for the given recipe {@code Id}.
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session.
     *
     *  @param  recipeId a recipe {@code Id} 
     *  @return the returned {@code Direction} list 
     *  @throws org.osid.NullArgumentException {@code recipeId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirectionsForRecipe(org.osid.id.Id recipeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDirectionsForRecipe(recipeId));
    }


    /**
     *  Gets all {@code Directions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  directions or an error results. Otherwise, the returned list
     *  may contain only those directions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Directions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirections()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDirections());
    }
}
