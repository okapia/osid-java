//
// AbstractStatisticQueryInspector.java
//
//     A template for making a StatisticQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.statistic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for statistics.
 */

public abstract class AbstractStatisticQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendiumQueryInspector
    implements org.osid.metering.StatisticQueryInspector {

    private final java.util.Collection<org.osid.metering.records.StatisticQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the meter <code> Id </code> terms. 
     *
     *  @return the meter <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMeterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the meter terms. 
     *
     *  @return the meter terms 
     */

    @OSID @Override
    public org.osid.metering.MeterQueryInspector[] getMeterTerms() {
        return (new org.osid.metering.MeterQueryInspector[0]);
    }


    /**
     *  Gets the object <code> Id </code> terms. 
     *
     *  @return the object <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMeteredObjectIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the time interval term. 
     *
     *  @return the time interval 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeRangeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the sum terms. 
     *
     *  @return the sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getSumTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minumim sum terms. 
     *
     *  @return the minumim sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumSumTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the mean terms. 
     *
     *  @return the mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMeanTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minumim mean terms. 
     *
     *  @return the minumim mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMeanTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the median terms. 
     *
     *  @return the median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMedianTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minumim median terms. 
     *
     *  @return the minumim median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMedianTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the mode terms. 
     *
     *  @return the mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getModeTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minumim mode terms. 
     *
     *  @return the minumim mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumModeTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the standard deviation terms. 
     *
     *  @return the standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getStandardDeviationTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minumim standard deviation terms. 
     *
     *  @return the minumim standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumStandardDeviationTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the root mean square terms. 
     *
     *  @return the root mean square terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getRMSTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minumim root mean square terms. 
     *
     *  @return the minumim root mean square terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumRMSTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the delta terms. 
     *
     *  @return the delta terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDeltaTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minumim delta terms. 
     *
     *  @return the minumim delta terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumDeltaTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the percentage change terms. 
     *
     *  @return the percentage change terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getPercentChangeTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum delta terms. 
     *
     *  @return the minumim percentage change terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumPercentChangeTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the average rate terms. 
     *
     *  @return the average rate terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getAverageRateTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }



    /**
     *  Gets the record corresponding to the given statistic query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a statistic implementing the requested record.
     *
     *  @param statisticRecordType a statistic record type
     *  @return the statistic query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(statisticRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.StatisticQueryInspectorRecord getStatisticQueryInspectorRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.StatisticQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(statisticRecordType + " is not supported");
    }


    /**
     *  Adds a record to this statistic query. 
     *
     *  @param statisticQueryInspectorRecord statistic query inspector
     *         record
     *  @param statisticRecordType statistic record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStatisticQueryInspectorRecord(org.osid.metering.records.StatisticQueryInspectorRecord statisticQueryInspectorRecord, 
                                                   org.osid.type.Type statisticRecordType) {

        addRecordType(statisticRecordType);
        nullarg(statisticRecordType, "statistic record type");
        this.records.add(statisticQueryInspectorRecord);        
        return;
    }
}
