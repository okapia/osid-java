//
// AbstractSyllabus.java
//
//     Defines a Syllabus builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.syllabus.syllabus.spi;


/**
 *  Defines a <code>Syllabus</code> builder.
 */

public abstract class AbstractSyllabusBuilder<T extends AbstractSyllabusBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.syllabus.syllabus.SyllabusMiter syllabus;


    /**
     *  Constructs a new <code>AbstractSyllabusBuilder</code>.
     *
     *  @param syllabus the syllabus to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSyllabusBuilder(net.okapia.osid.jamocha.builder.course.syllabus.syllabus.SyllabusMiter syllabus) {
        super(syllabus);
        this.syllabus = syllabus;
        return;
    }


    /**
     *  Builds the syllabus.
     *
     *  @return the new syllabus
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.syllabus.Syllabus build() {
        (new net.okapia.osid.jamocha.builder.validator.course.syllabus.syllabus.SyllabusValidator(getValidations())).validate(this.syllabus);
        return (new net.okapia.osid.jamocha.builder.course.syllabus.syllabus.ImmutableSyllabus(this.syllabus));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the syllabus miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.syllabus.syllabus.SyllabusMiter getMiter() {
        return (this.syllabus);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public T course(org.osid.course.Course course) {
        getMiter().setCourse(course);
        return (self());
    }


    /**
     *  Adds a Syllabus record.
     *
     *  @param record a syllabus record
     *  @param recordType the type of syllabus record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.syllabus.records.SyllabusRecord record, org.osid.type.Type recordType) {
        getMiter().addSyllabusRecord(record, recordType);
        return (self());
    }
}       


