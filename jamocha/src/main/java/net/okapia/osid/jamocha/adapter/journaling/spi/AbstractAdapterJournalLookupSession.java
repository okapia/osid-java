//
// AbstractAdapterJournalLookupSession.java
//
//    A Journal lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.journaling.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Journal lookup session adapter.
 */

public abstract class AbstractAdapterJournalLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.journaling.JournalLookupSession {

    private final org.osid.journaling.JournalLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterJournalLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJournalLookupSession(org.osid.journaling.JournalLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Journal} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupJournals() {
        return (this.session.canLookupJournals());
    }


    /**
     *  A complete view of the {@code Journal} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJournalView() {
        this.session.useComparativeJournalView();
        return;
    }


    /**
     *  A complete view of the {@code Journal} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJournalView() {
        this.session.usePlenaryJournalView();
        return;
    }

     
    /**
     *  Gets the {@code Journal} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Journal} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Journal} and
     *  retained for compatibility.
     *
     *  @param journalId {@code Id} of the {@code Journal}
     *  @return the journal
     *  @throws org.osid.NotFoundException {@code journalId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code journalId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournal(journalId));
    }


    /**
     *  Gets a {@code JournalList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journals specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Journals} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  journalIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Journal} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code journalIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByIds(org.osid.id.IdList journalIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalsByIds(journalIds));
    }


    /**
     *  Gets a {@code JournalList} corresponding to the given
     *  journal genus {@code Type} which does not include
     *  journals of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned {@code Journal} list
     *  @throws org.osid.NullArgumentException
     *          {@code journalGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalsByGenusType(journalGenusType));
    }


    /**
     *  Gets a {@code JournalList} corresponding to the given
     *  journal genus {@code Type} and include any additional
     *  journals with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned {@code Journal} list
     *  @throws org.osid.NullArgumentException
     *          {@code journalGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByParentGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalsByParentGenusType(journalGenusType));
    }


    /**
     *  Gets a {@code JournalList} containing the given
     *  journal record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalRecordType a journal record type 
     *  @return the returned {@code Journal} list
     *  @throws org.osid.NullArgumentException
     *          {@code journalRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByRecordType(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalsByRecordType(journalRecordType));
    }


    /**
     *  Gets a {@code JournalList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Journal} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournalsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Journals}. 
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Journals} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJournals());
    }
}
