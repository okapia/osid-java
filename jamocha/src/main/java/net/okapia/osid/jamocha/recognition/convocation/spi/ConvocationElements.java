//
// ConvocationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.convocation.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ConvocationElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the ConvocationElement Id.
     *
     *  @return the convocation element Id
     */

    public static org.osid.id.Id getConvocationEntityId() {
        return (makeEntityId("osid.recognition.Convocation"));
    }


    /**
     *  Gets the AwardIds element Id.
     *
     *  @return the AwardIds element Id
     */

    public static org.osid.id.Id getAwardIds() {
        return (makeElementId("osid.recognition.convocation.AwardIds"));
    }


    /**
     *  Gets the Awards element Id.
     *
     *  @return the Awards element Id
     */

    public static org.osid.id.Id getAwards() {
        return (makeElementId("osid.recognition.convocation.Awards"));
    }


    /**
     *  Gets the TimePeriodId element Id.
     *
     *  @return the TimePeriodId element Id
     */

    public static org.osid.id.Id getTimePeriodId() {
        return (makeElementId("osid.recognition.convocation.TimePeriodId"));
    }


    /**
     *  Gets the TimePeriod element Id.
     *
     *  @return the TimePeriod element Id
     */

    public static org.osid.id.Id getTimePeriod() {
        return (makeElementId("osid.recognition.convocation.TimePeriod"));
    }


    /**
     *  Gets the Date element Id.
     *
     *  @return the Date element Id
     */

    public static org.osid.id.Id getDate() {
        return (makeElementId("osid.recognition.convocation.Date"));
    }


    /**
     *  Gets the ConferralId element Id.
     *
     *  @return the ConferralId element Id
     */

    public static org.osid.id.Id getConferralId() {
        return (makeQueryElementId("osid.recognition.convocation.ConferralId"));
    }


    /**
     *  Gets the Conferral element Id.
     *
     *  @return the Conferral element Id
     */

    public static org.osid.id.Id getConferral() {
        return (makeQueryElementId("osid.recognition.convocation.Conferral"));
    }


    /**
     *  Gets the AcademyId element Id.
     *
     *  @return the AcademyId element Id
     */

    public static org.osid.id.Id getAcademyId() {
        return (makeQueryElementId("osid.recognition.convocation.AcademyId"));
    }


    /**
     *  Gets the Academy element Id.
     *
     *  @return the Academy element Id
     */

    public static org.osid.id.Id getAcademy() {
        return (makeQueryElementId("osid.recognition.convocation.Academy"));
    }
}
