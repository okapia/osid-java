//
// AbstractQueryCourseOfferingLookupSession.java
//
//    An inline adapter that maps a CourseOfferingLookupSession to
//    a CourseOfferingQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CourseOfferingLookupSession to
 *  a CourseOfferingQuerySession.
 */

public abstract class AbstractQueryCourseOfferingLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractCourseOfferingLookupSession
    implements org.osid.course.CourseOfferingLookupSession {

    private boolean effectiveonly = false;    
    private final org.osid.course.CourseOfferingQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCourseOfferingLookupSession.
     *
     *  @param querySession the underlying course offering query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCourseOfferingLookupSession(org.osid.course.CourseOfferingQuerySession querySession) {
        nullarg(querySession, "course offering query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>CourseOffering</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourseOfferings() {
        return (this.session.canSearchCourseOfferings());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include course offerings in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only course offerings whose effective dates are current are
     *  returned by methods in this session.
     */

    public void useEffectiveCourseOfferingView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All course offerings of any effective dates are returned by
     *  all methods in this session.
     */

    public void useAnyEffectiveCourseOfferingView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>CourseOffering</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseOffering</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CourseOffering</code> and retained for compatibility.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingId <code>Id</code> of the
     *          <code>CourseOffering</code>
     *  @return the course offering
     *  @throws org.osid.NotFoundException <code>courseOfferingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchId(courseOfferingId, true);
        org.osid.course.CourseOfferingList courseOfferings = this.session.getCourseOfferingsByQuery(query);
        if (courseOfferings.hasNext()) {
            return (courseOfferings.getNextCourseOffering());
        } 
        
        throw new org.osid.NotFoundException(courseOfferingId + " not found");
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courseOfferings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CourseOfferings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByIds(org.osid.id.IdList courseOfferingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();

        try (org.osid.id.IdList ids = courseOfferingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the given
     *  course offering genus <code>Type</code> which does not include
     *  course offerings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  course offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingGenusType a courseOffering genus type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByGenusType(org.osid.type.Type courseOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchGenusType(courseOfferingGenusType, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the
     *  given course offering genus <code>Type</code> and include any
     *  additional course offerings with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingGenusType a courseOffering genus type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByParentGenusType(org.osid.type.Type courseOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchParentGenusType(courseOfferingGenusType, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> containing the given
     *  course offering record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingRecordType a courseOffering record type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByRecordType(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchRecordType(courseOfferingRecordType, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code> Course. </code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> courseId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchCourseId(courseId, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> for a course and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseOnDate(org.osid.id.Id courseId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchCourseId(courseId, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchTermId(termId, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> for a term and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForTermOnDate(org.osid.id.Id termId, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchTermId(termId, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code>Course</code> and <code>Term</code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results.  Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  termId a term <code> Id </code> 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> courseId </code>
     *          or <code> termId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseAndTerm(org.osid.id.Id courseId, 
                                                                                 org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchCourseId(courseId, true);
        query.matchTermId(termId, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> for a course, term, and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  termId a term <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>,
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseAndTermOnDate(org.osid.id.Id courseId, 
                                                                                       org.osid.id.Id termId,
                                                                                       org.osid.calendaring.DateTime from, 
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchCourseId(courseId, true);
        query.matchTermId(termId, true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets all <code>CourseOfferings</code>. 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @return a list of <code>CourseOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseOfferingQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCourseOfferingsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.CourseOfferingQuery getQuery() {
        org.osid.course.CourseOfferingQuery query = this.session.getCourseOfferingQuery();

        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
