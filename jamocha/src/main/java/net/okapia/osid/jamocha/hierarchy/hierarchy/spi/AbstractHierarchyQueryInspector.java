//
// AbstractHierarchyQueryInspector.java
//
//     A template for making a HierarchyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hierarchy.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for hierarchies.
 */

public abstract class AbstractHierarchyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.hierarchy.HierarchyQueryInspector {

    private final java.util.Collection<org.osid.hierarchy.records.HierarchyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the node <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNodeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }



    /**
     *  Gets the record corresponding to the given hierarchy query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a hierarchy implementing the requested record.
     *
     *  @param hierarchyRecordType a hierarchy record type
     *  @return the hierarchy query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(hierarchyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchyQueryInspectorRecord getHierarchyQueryInspectorRecord(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hierarchy.records.HierarchyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(hierarchyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(hierarchyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this hierarchy query. 
     *
     *  @param hierarchyQueryInspectorRecord hierarchy query inspector
     *         record
     *  @param hierarchyRecordType hierarchy record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addHierarchyQueryInspectorRecord(org.osid.hierarchy.records.HierarchyQueryInspectorRecord hierarchyQueryInspectorRecord, 
                                                   org.osid.type.Type hierarchyRecordType) {

        addRecordType(hierarchyRecordType);
        nullarg(hierarchyRecordType, "hierarchy record type");
        this.records.add(hierarchyQueryInspectorRecord);        
        return;
    }
}
