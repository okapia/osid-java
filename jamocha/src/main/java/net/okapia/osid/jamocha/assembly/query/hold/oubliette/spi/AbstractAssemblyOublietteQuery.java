//
// AbstractAssemblyOublietteQuery.java
//
//     An OublietteQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.hold.oubliette.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OublietteQuery that stores terms.
 */

public abstract class AbstractAssemblyOublietteQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.hold.OublietteQuery,
               org.osid.hold.OublietteQueryInspector,
               org.osid.hold.OublietteSearchOrder {

    private final java.util.Collection<org.osid.hold.records.OublietteQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.OublietteQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.hold.records.OublietteSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOublietteQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOublietteQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the effort <code> Id </code> for this query to match oubliettes 
     *  containing blocks. 
     *
     *  @param  blockId the block <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockId(org.osid.id.Id blockId, boolean match) {
        getAssembler().addIdTerm(getBlockIdColumn(), blockId, match);
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearBlockIdTerms() {
        getAssembler().clearTerms(getBlockIdColumn());
        return;
    }


    /**
     *  Gets the block <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBlockIdTerms() {
        return (getAssembler().getIdTerms(getBlockIdColumn()));
    }


    /**
     *  Gets the BlockId column name.
     *
     * @return the column name
     */

    protected String getBlockIdColumn() {
        return ("block_id");
    }


    /**
     *  Tests if a <code> BlockQuery </code> is available. 
     *
     *  @return <code> true </code> if a block query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a block. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the block query 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuery getBlockQuery() {
        throw new org.osid.UnimplementedException("supportsBlockQuery() is false");
    }


    /**
     *  Matches oubliettes that have any block. 
     *
     *  @param  match <code> true </code> to match oubliettes with any block, 
     *          <code> false </code> to match oubliettes with no block 
     */

    @OSID @Override
    public void matchAnyBlock(boolean match) {
        getAssembler().addIdWildcardTerm(getBlockColumn(), match);
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearBlockTerms() {
        getAssembler().clearTerms(getBlockColumn());
        return;
    }


    /**
     *  Gets the block query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.BlockQueryInspector[] getBlockTerms() {
        return (new org.osid.hold.BlockQueryInspector[0]);
    }


    /**
     *  Gets the Block column name.
     *
     * @return the column name
     */

    protected String getBlockColumn() {
        return ("block");
    }


    /**
     *  Sets the issue <code> Id </code> for this query to match oubliettes 
     *  that have a related issue. 
     *
     *  @param  issueId a issue <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        getAssembler().clearTerms(getIssueIdColumn());
        return;
    }


    /**
     *  Gets the issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (getAssembler().getIdTerms(getIssueIdColumn()));
    }


    /**
     *  Gets the IssueId column name.
     *
     * @return the column name
     */

    protected String getIssueIdColumn() {
        return ("issue_id");
    }


    /**
     *  Tests if a <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Matches oubliettes that have any issue. 
     *
     *  @param  match <code> true </code> to match oubliettes with any issue, 
     *          <code> false </code> to match oubliettes with no issue 
     */

    @OSID @Override
    public void matchAnyIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getIssueColumn(), match);
        return;
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        getAssembler().clearTerms(getIssueColumn());
        return;
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.hold.IssueQueryInspector[0]);
    }


    /**
     *  Gets the Issue column name.
     *
     * @return the column name
     */

    protected String getIssueColumn() {
        return ("issue");
    }


    /**
     *  Sets the hold <code> Id </code> for this query. 
     *
     *  @param  holdId the hold <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> holdId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchHoldId(org.osid.id.Id holdId, boolean match) {
        getAssembler().addIdTerm(getHoldIdColumn(), holdId, match);
        return;
    }


    /**
     *  Clears the hold <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearHoldIdTerms() {
        getAssembler().clearTerms(getHoldIdColumn());
        return;
    }


    /**
     *  Gets the hold <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getHoldIdTerms() {
        return (getAssembler().getIdTerms(getHoldIdColumn()));
    }


    /**
     *  Gets the HoldId column name.
     *
     * @return the column name
     */

    protected String getHoldIdColumn() {
        return ("hold_id");
    }


    /**
     *  Tests if a <code> HoldQuery </code> is available. 
     *
     *  @return <code> true </code> if a hold query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldQuery() {
        return (false);
    }


    /**
     *  Gets the query for a hold. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the hold query 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuery getHoldQuery() {
        throw new org.osid.UnimplementedException("supportsHoldQuery() is false");
    }


    /**
     *  Matches oubliettes referenced by any hold. 
     *
     *  @param  match <code> true </code> to match oubliettes with any holds, 
     *          <code> false </code> to match oubliettes with no holds 
     */

    @OSID @Override
    public void matchAnyHold(boolean match) {
        getAssembler().addIdWildcardTerm(getHoldColumn(), match);
        return;
    }


    /**
     *  Clears the hold query terms. 
     */

    @OSID @Override
    public void clearHoldTerms() {
        getAssembler().clearTerms(getHoldColumn());
        return;
    }


    /**
     *  Gets the hold query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.HoldQueryInspector[] getHoldTerms() {
        return (new org.osid.hold.HoldQueryInspector[0]);
    }


    /**
     *  Gets the Hold column name.
     *
     * @return the column name
     */

    protected String getHoldColumn() {
        return ("hold");
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match 
     *  oubliettes that have the specified oubliette as an ancestor. 
     *
     *  @param  oublietteId an oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOublietteId(org.osid.id.Id oublietteId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorOublietteIdColumn(), oublietteId, match);
        return;
    }


    /**
     *  Clears the ancestor oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorOublietteIdTerms() {
        getAssembler().clearTerms(getAncestorOublietteIdColumn());
        return;
    }


    /**
     *  Gets the ancestor oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOublietteIdTerms() {
        return (getAssembler().getIdTerms(getAncestorOublietteIdColumn()));
    }


    /**
     *  Gets the AncestorOublietteId column name.
     *
     * @return the column name
     */

    protected String getAncestorOublietteIdColumn() {
        return ("ancestor_oubliette_id");
    }


    /**
     *  Tests if an <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOublietteQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getAncestorOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOublietteQuery() is false");
    }


    /**
     *  Matches oubliettes with any ancestor. 
     *
     *  @param  match <code> true </code> to match oubliettes with any 
     *          ancestor, <code> false </code> to match root oubliettes 
     */

    @OSID @Override
    public void matchAnyAncestorOubliette(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorOublietteColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor oubliette query terms. 
     */

    @OSID @Override
    public void clearAncestorOublietteTerms() {
        getAssembler().clearTerms(getAncestorOublietteColumn());
        return;
    }


    /**
     *  Gets the ancestor oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getAncestorOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }


    /**
     *  Gets the AncestorOubliette column name.
     *
     * @return the column name
     */

    protected String getAncestorOublietteColumn() {
        return ("ancestor_oubliette");
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match 
     *  oubliettes that have the specified oubliette as a descendant. 
     *
     *  @param  oublietteId an oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOublietteId(org.osid.id.Id oublietteId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantOublietteIdColumn(), oublietteId, match);
        return;
    }


    /**
     *  Clears the descendant oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantOublietteIdTerms() {
        getAssembler().clearTerms(getDescendantOublietteIdColumn());
        return;
    }


    /**
     *  Gets the descendant oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOublietteIdTerms() {
        return (getAssembler().getIdTerms(getDescendantOublietteIdColumn()));
    }


    /**
     *  Gets the DescendantOublietteId column name.
     *
     * @return the column name
     */

    protected String getDescendantOublietteIdColumn() {
        return ("descendant_oubliette_id");
    }


    /**
     *  Tests if an <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOublietteQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getDescendantOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOublietteQuery() is false");
    }


    /**
     *  Matches oubliettes with any descendant. 
     *
     *  @param  match <code> true </code> to match oubliettes with any 
     *          descendant, <code> false </code> to match leaf oubliettes 
     */

    @OSID @Override
    public void matchAnyDescendantOubliette(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantOublietteColumn(), match);
        return;
    }


    /**
     *  Clears the descendant oubliette query terms. 
     */

    @OSID @Override
    public void clearDescendantOublietteTerms() {
        getAssembler().clearTerms(getDescendantOublietteColumn());
        return;
    }


    /**
     *  Gets the descendant oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getDescendantOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }


    /**
     *  Gets the DescendantOubliette column name.
     *
     * @return the column name
     */

    protected String getDescendantOublietteColumn() {
        return ("descendant_oubliette");
    }


    /**
     *  Tests if this oubliette supports the given record
     *  <code>Type</code>.
     *
     *  @param  oublietteRecordType an oubliette record type 
     *  @return <code>true</code> if the oublietteRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type oublietteRecordType) {
        for (org.osid.hold.records.OublietteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(oublietteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  oublietteRecordType the oubliette record type 
     *  @return the oubliette query record 
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(oublietteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.OublietteQueryRecord getOublietteQueryRecord(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.OublietteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(oublietteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(oublietteRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  oublietteRecordType the oubliette record type 
     *  @return the oubliette query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(oublietteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.OublietteQueryInspectorRecord getOublietteQueryInspectorRecord(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.OublietteQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(oublietteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(oublietteRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param oublietteRecordType the oubliette record type
     *  @return the oubliette search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(oublietteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.OublietteSearchOrderRecord getOublietteSearchOrderRecord(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.OublietteSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(oublietteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(oublietteRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this oubliette. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param oublietteQueryRecord the oubliette query record
     *  @param oublietteQueryInspectorRecord the oubliette query inspector
     *         record
     *  @param oublietteSearchOrderRecord the oubliette search order record
     *  @param oublietteRecordType oubliette record type
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteQueryRecord</code>,
     *          <code>oublietteQueryInspectorRecord</code>,
     *          <code>oublietteSearchOrderRecord</code> or
     *          <code>oublietteRecordTypeoubliette</code> is
     *          <code>null</code>
     */
            
    protected void addOublietteRecords(org.osid.hold.records.OublietteQueryRecord oublietteQueryRecord, 
                                      org.osid.hold.records.OublietteQueryInspectorRecord oublietteQueryInspectorRecord, 
                                      org.osid.hold.records.OublietteSearchOrderRecord oublietteSearchOrderRecord, 
                                      org.osid.type.Type oublietteRecordType) {

        addRecordType(oublietteRecordType);

        nullarg(oublietteQueryRecord, "oubliette query record");
        nullarg(oublietteQueryInspectorRecord, "oubliette query inspector record");
        nullarg(oublietteSearchOrderRecord, "oubliette search odrer record");

        this.queryRecords.add(oublietteQueryRecord);
        this.queryInspectorRecords.add(oublietteQueryInspectorRecord);
        this.searchOrderRecords.add(oublietteSearchOrderRecord);
        
        return;
    }
}
