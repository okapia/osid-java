//
// AbstractFederatingAuctionLookupSession.java
//
//     An abstract federating adapter for an AuctionLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AuctionLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAuctionLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.bidding.AuctionLookupSession>
    implements org.osid.bidding.AuctionLookupSession {

    private boolean parallel = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();


    /**
     *  Constructs a new <code>AbstractFederatingAuctionLookupSession</code>.
     */

    protected AbstractFederatingAuctionLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.bidding.AuctionLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Auction</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctions() {
        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            if (session.canLookupAuctions()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Auction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionView() {
        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            session.useComparativeAuctionView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Auction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionView() {
        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            session.usePlenaryAuctionView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auctions in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            session.useFederatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            session.useIsolatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Only active auctions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionView() {
        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            session.useActiveAuctionView();
        }

        return;
    }


    /**
     *  Active and inactive auctions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionView() {
        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            session.useAnyStatusAuctionView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Auction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Auction</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Auction</code> and retained for
     *  compatibility.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  auctionId <code>Id</code> of the
     *          <code>Auction</code>
     *  @return the auction
     *  @throws org.osid.NotFoundException <code>auctionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Auction getAuction(org.osid.id.Id auctionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            try {
                return (session.getAuction(auctionId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(auctionId + " not found");
    }


    /**
     *  Gets an <code>AuctionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Auctions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  auctionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByIds(org.osid.id.IdList auctionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.bidding.auction.MutableAuctionList ret = new net.okapia.osid.jamocha.bidding.auction.MutableAuctionList();

        try (org.osid.id.IdList ids = auctionIds) {
            while (ids.hasNext()) {
                ret.addAuction(getAuction(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionList</code> corresponding to the given
     *  auction genus <code>Type</code> which does not include
     *  auctions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsByGenusType(auctionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionList</code> corresponding to the given
     *  auction genus <code>Type</code> and include any additional
     *  auctions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByParentGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsByParentGenusType(auctionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionList</code> containing the given
     *  auction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByRecordType(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsByRecordType(auctionRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known auctions or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  auctions that are accessible through this session. 
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Auction</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible
     *  through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Auction</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of auctions for an item.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemId a resource <code> Id </code>
     *  @return the returned <code> Auction </code> list
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItem(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsByItem(itemId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of auctions for an item genus type.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemGenusType an item genus type
     *  @return the returned <code> Auction </code> list
     *  @throws org.osid.NullArgumentException <code> itemGenusType </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsByItemGenusType(itemGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> AuctionList </code> for an item genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known auctions or an
     *  error results. Otherwise, the returned list may contain only those
     *  auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently active. In
     *  any status mode, active and inactive auctions are returned.
     *
     *  @param  itemGenusType an item genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Auction </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> itemGenusType, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusTypeOnDate(org.osid.type.Type itemGenusType,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctionsByItemGenusTypeOnDate(itemGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Auctions</code>. 
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @return a list of <code>Auctions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList ret = getAuctionList();

        for (org.osid.bidding.AuctionLookupSession session : getSessions()) {
            ret.addAuctionList(session.getAuctions());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.bidding.auction.FederatingAuctionList getAuctionList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.auction.ParallelAuctionList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.auction.CompositeAuctionList());
        }
    }
}
