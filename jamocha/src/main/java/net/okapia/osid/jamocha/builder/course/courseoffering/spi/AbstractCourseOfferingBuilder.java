//
// AbstractCourseOffering.java
//
//     Defines a CourseOffering builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.courseoffering.spi;


/**
 *  Defines a <code>CourseOffering</code> builder.
 */

public abstract class AbstractCourseOfferingBuilder<T extends AbstractCourseOfferingBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.courseoffering.CourseOfferingMiter courseOffering;


    /**
     *  Constructs a new <code>AbstractCourseOfferingBuilder</code>.
     *
     *  @param courseOffering the course offering to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCourseOfferingBuilder(net.okapia.osid.jamocha.builder.course.courseoffering.CourseOfferingMiter courseOffering) {
        super(courseOffering);
        this.courseOffering = courseOffering;
        return;
    }


    /**
     *  Builds the course offering.
     *
     *  @return the new course offering
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.CourseOffering build() {
        (new net.okapia.osid.jamocha.builder.validator.course.courseoffering.CourseOfferingValidator(getValidations())).validate(this.courseOffering);
        return (new net.okapia.osid.jamocha.builder.course.courseoffering.ImmutableCourseOffering(this.courseOffering));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the course offering miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.courseoffering.CourseOfferingMiter getMiter() {
        return (this.courseOffering);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public T course(org.osid.course.Course course) {
        getMiter().setCourse(course);
        return (self());
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public T term(org.osid.course.Term term) {
        getMiter().setTerm(term);
        return (self());
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T number(String number) {
        getMiter().setNumber(number);
        return (self());
    }


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>instructor</code>
     *          is <code>null</code>
     */

    public T instructor(org.osid.resource.Resource instructor) {
        getMiter().addInstructor(instructor);
        return (self());
    }


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    public T instructors(java.util.Collection<org.osid.resource.Resource> instructors) {
        getMiter().setInstructors(instructors);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T creditAmount(org.osid.grading.Grade amount) {
        getMiter().addCreditAmount(amount);
        return (self());
    }


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    public T creditAmounts(java.util.Collection<org.osid.grading.Grade> amounts) {
        getMiter().setCreditAmounts(amounts);
        return (self());
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public T gradingOption(org.osid.grading.GradeSystem option) {
        getMiter().addGradingOption(option);
        return (self());
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public T gradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        getMiter().setGradingOptions(options);
        return (self());
    }


    /**
     *  Marks this offering to require registration.
     *
     *  @return the builder
     */

    public T requireRegistration() {
        getMiter().setRequiresRegistration(true);
        return (self());
    }


    /**
     *  Marks this offering to not require registration.
     *
     *  @return the builder
     */

    public T notRequireRegistration() {
        getMiter().setRequiresRegistration(false);
        return (self());
    }

    
    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public T minimumSeats(long seats) {
        getMiter().setMinimumSeats(seats);
        return (self());
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public T maximumSeats(long seats) {
        getMiter().setMaximumSeats(seats);
        return (self());
    }


    /**
     *  Sets the URL.
     *
     *  @param url a URL
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public T url(String url) {
        getMiter().setURL(url);
        return (self());
    }


    /**
     *  Sets the schedule info.
     *
     *  @param info a schedule info
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public T scheduleInfo(org.osid.locale.DisplayText info) {
        getMiter().setScheduleInfo(info);
        return (self());
    }


    /**
     *  Sets the event.
     *
     *  @param event an event
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public T event(org.osid.calendaring.Event event) {
        getMiter().setEvent(event);
        return (self());
    }


    /**
     *  Adds a CourseOffering record.
     *
     *  @param record a course offering record
     *  @param recordType the type of course offering record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.records.CourseOfferingRecord record, org.osid.type.Type recordType) {
        getMiter().addCourseOfferingRecord(record, recordType);
        return (self());
    }
}       


