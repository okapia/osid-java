//
// MutableIndexedMapDictionaryLookupSession
//
//    Implements a Dictionary lookup service backed by a collection of
//    dictionaries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  Implements a Dictionary lookup service backed by a collection of
 *  dictionaries. The dictionaries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some dictionaries may be compatible
 *  with more types than are indicated through these dictionary
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of dictionaries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapDictionaryLookupSession
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractIndexedMapDictionaryLookupSession
    implements org.osid.dictionary.DictionaryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDictionaryLookupSession} with no
     *  dictionaries.
     */

    public MutableIndexedMapDictionaryLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDictionaryLookupSession} with a
     *  single dictionary.
     *  
     *  @param  dictionary a single dictionary
     *  @throws org.osid.NullArgumentException {@code dictionary}
     *          is {@code null}
     */

    public MutableIndexedMapDictionaryLookupSession(org.osid.dictionary.Dictionary dictionary) {
        putDictionary(dictionary);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDictionaryLookupSession} using an
     *  array of dictionaries.
     *
     *  @param  dictionaries an array of dictionaries
     *  @throws org.osid.NullArgumentException {@code dictionaries}
     *          is {@code null}
     */

    public MutableIndexedMapDictionaryLookupSession(org.osid.dictionary.Dictionary[] dictionaries) {
        putDictionaries(dictionaries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDictionaryLookupSession} using a
     *  collection of dictionaries.
     *
     *  @param  dictionaries a collection of dictionaries
     *  @throws org.osid.NullArgumentException {@code dictionaries} is
     *          {@code null}
     */

    public MutableIndexedMapDictionaryLookupSession(java.util.Collection<? extends org.osid.dictionary.Dictionary> dictionaries) {
        putDictionaries(dictionaries);
        return;
    }
    

    /**
     *  Makes a {@code Dictionary} available in this session.
     *
     *  @param  dictionary a dictionary
     *  @throws org.osid.NullArgumentException {@code dictionary{@code  is
     *          {@code null}
     */

    @Override
    public void putDictionary(org.osid.dictionary.Dictionary dictionary) {
        super.putDictionary(dictionary);
        return;
    }


    /**
     *  Makes an array of dictionaries available in this session.
     *
     *  @param  dictionaries an array of dictionaries
     *  @throws org.osid.NullArgumentException {@code dictionaries{@code 
     *          is {@code null}
     */

    @Override
    public void putDictionaries(org.osid.dictionary.Dictionary[] dictionaries) {
        super.putDictionaries(dictionaries);
        return;
    }


    /**
     *  Makes collection of dictionaries available in this session.
     *
     *  @param  dictionaries a collection of dictionaries
     *  @throws org.osid.NullArgumentException {@code dictionary{@code  is
     *          {@code null}
     */

    @Override
    public void putDictionaries(java.util.Collection<? extends org.osid.dictionary.Dictionary> dictionaries) {
        super.putDictionaries(dictionaries);
        return;
    }


    /**
     *  Removes a Dictionary from this session.
     *
     *  @param dictionaryId the {@code Id} of the dictionary
     *  @throws org.osid.NullArgumentException {@code dictionaryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDictionary(org.osid.id.Id dictionaryId) {
        super.removeDictionary(dictionaryId);
        return;
    }    
}
