//
// AbstractAssemblyGradeEntryQuery.java
//
//     A GradeEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradeEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyGradeEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.grading.GradeEntryQuery,
               org.osid.grading.GradeEntryQueryInspector,
               org.osid.grading.GradeEntrySearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradeEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyGradeEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyGradeEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradebook column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        getAssembler().addIdTerm(getGradebookColumnIdColumn(), gradebookColumnId, match);
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        getAssembler().clearTerms(getGradebookColumnIdColumn());
        return;
    }


    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (getAssembler().getIdTerms(getGradebookColumnIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the gradebook column. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradebookColumn(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradebookColumnColumn(), style);
        return;
    }


    /**
     *  Gets the GradebookColumnId column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnIdColumn() {
        return ("gradebook_column_id");
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available for 
     *  querying creators. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Clears the gradebook column terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        getAssembler().clearTerms(getGradebookColumnColumn());
        return;
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebook column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Tests if a <code> GradebookColumnSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a gradebook column search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a gradebook column. 
     *
     *  @return the gradebook column search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchOrder getGradebookColumnSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnSearchOrder() is false");
    }


    /**
     *  Gets the GradebookColumn column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnColumn() {
        return ("gradebook_column");
    }


    /**
     *  Sets the key resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchKeyResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getKeyResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the key resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearKeyResourceIdTerms() {
        getAssembler().clearTerms(getKeyResourceIdColumn());
        return;
    }


    /**
     *  Gets the key resource <code> Id </code> terms. 
     *
     *  @return the key resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getKeyResourceIdTerms() {
        return (getAssembler().getIdTerms(getKeyResourceIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the key resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByKeyResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getKeyResourceColumn(), style);
        return;
    }


    /**
     *  Gets the KeyResourceId column name.
     *
     * @return the column name
     */

    protected String getKeyResourceIdColumn() {
        return ("key_resource_id");
    }


    /**
     *  Tests if a <code> ResourceQUery </code> is available for querying key 
     *  resources. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a key resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyResourceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getKeyResourceQuery() {
        throw new org.osid.UnimplementedException("supportsKeyResourceQuery() is false");
    }


    /**
     *  Matches grade entries with any key resource. 
     *
     *  @param  match <code> true </code> to match grade entries with any key 
     *          resource, <code> false </code> to match entries with no key 
     *          resource 
     */

    @OSID @Override
    public void matchAnyKeyResource(boolean match) {
        getAssembler().addIdWildcardTerm(getKeyResourceColumn(), match);
        return;
    }


    /**
     *  Clears the key resource terms. 
     */

    @OSID @Override
    public void clearKeyResourceTerms() {
        getAssembler().clearTerms(getKeyResourceColumn());
        return;
    }


    /**
     *  Gets the key resource terms. 
     *
     *  @return the key resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getKeyResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a key resource search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a resource. 
     *
     *  @return the key resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyResourceSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getKeyResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsKeyResourceSearchOrder() is false");
    }


    /**
     *  Gets the KeyResource column name.
     *
     * @return the column name
     */

    protected String getKeyResourceColumn() {
        return ("key_resource");
    }


    /**
     *  Matches derived grade entries. 
     *
     *  @param  match <code> true </code> to match derived grade entries , 
     *          <code> false </code> to match manual entries 
     */

    @OSID @Override
    public void matchDerived(boolean match) {
        getAssembler().addBooleanTerm(getDerivedColumn(), match);
        return;
    }


    /**
     *  Clears the derived terms. 
     */

    @OSID @Override
    public void clearDerivedTerms() {
        getAssembler().clearTerms(getDerivedColumn());
        return;
    }


    /**
     *  Gets the derived terms. 
     *
     *  @return the derived terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDerivedTerms() {
        return (getAssembler().getBooleanTerms(getDerivedColumn()));
    }


    /**
     *  Specified a preference for ordering results by the derived entries. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDerived(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDerivedColumn(), style);
        return;
    }


    /**
     *  Gets the Derived column name.
     *
     * @return the column name
     */

    protected String getDerivedColumn() {
        return ("derived");
    }


    /**
     *  Sets the grade entry <code> Id </code> for an overridden calculated 
     *  grade entry. 
     *
     *  @param  gradeEntryId a grade entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOverriddenGradeEntryId(org.osid.id.Id gradeEntryId, 
                                            boolean match) {
        getAssembler().addIdTerm(getOverriddenGradeEntryIdColumn(), gradeEntryId, match);
        return;
    }


    /**
     *  Clears the overridden grade entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOverriddenGradeEntryIdTerms() {
        getAssembler().clearTerms(getOverriddenGradeEntryIdColumn());
        return;
    }


    /**
     *  Gets the overridden calculated grade entry <code> Id </code> terms. 
     *
     *  @return the overridden grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOverriddenGradeEntryIdTerms() {
        return (getAssembler().getIdTerms(getOverriddenGradeEntryIdColumn()));
    }


    /**
     *  Gets the OverriddenGradeEntryId column name.
     *
     * @return the column name
     */

    protected String getOverriddenGradeEntryIdColumn() {
        return ("overridden_grade_entry_id");
    }


    /**
     *  Tests if a <code> GradeEntry </code> is available for querying 
     *  overridden calculated grade entries. 
     *
     *  @return <code> true </code> if a grade entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOverriddenGradeEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an overridden derived grade entry. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the grade entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOverriddenGradeEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuery getOverriddenGradeEntryQuery() {
        throw new org.osid.UnimplementedException("supportsOverriddenGradeEntryQuery() is false");
    }


    /**
     *  Matches grade entries overriding any calculated grade entry. 
     *
     *  @param  match <code> true </code> to match grade entries overriding 
     *          any grade entry, <code> false </code> to match entries not 
     *          overriding any entry 
     */

    @OSID @Override
    public void matchAnyOverriddenGradeEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getOverriddenGradeEntryColumn(), match);
        return;
    }


    /**
     *  Clears the overridden grade entry terms. 
     */

    @OSID @Override
    public void clearOverriddenGradeEntryTerms() {
        getAssembler().clearTerms(getOverriddenGradeEntryColumn());
        return;
    }


    /**
     *  Gets the overriden derived grade terms. 
     *
     *  @return the overridden grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getOverriddenGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the OverriddenGradeEntry column name.
     *
     * @return the column name
     */

    protected String getOverriddenGradeEntryColumn() {
        return ("overridden_grade_entry");
    }


    /**
     *  Matches grade entries ignored for calculations. 
     *
     *  @param  match <code> true </code> to match grade entries ignored for 
     *          calculations, <code> false </code> to match entries used in 
     *          calculations 
     */

    @OSID @Override
    public void matchIgnoredForCalculations(boolean match) {
        getAssembler().addBooleanTerm(getIgnoredForCalculationsColumn(), match);
        return;
    }


    /**
     *  Clears the ignored for calculation entries terms. 
     */

    @OSID @Override
    public void clearIgnoredForCalculationsTerms() {
        getAssembler().clearTerms(getIgnoredForCalculationsColumn());
        return;
    }


    /**
     *  Gets the ignored for caluclation entries terms. 
     *
     *  @return the ignored for calculation terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getIgnoredForCalculationsTerms() {
        return (getAssembler().getBooleanTerms(getIgnoredForCalculationsColumn()));
    }


    /**
     *  Specified a preference for ordering results by the ignore for 
     *  calculations flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByIgnoredForCalculations(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getIgnoredForCalculationsColumn(), style);
        return;
    }


    /**
     *  Gets the IgnoredForCalculations column name.
     *
     * @return the column name
     */

    protected String getIgnoredForCalculationsColumn() {
        return ("ignored_for_calculations");
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getGradeIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        getAssembler().clearTerms(getGradeIdColumn());
        return;
    }


    /**
     *  Gets the grade <code> Id </code> terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (getAssembler().getIdTerms(getGradeIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the grade or score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeColumn(), style);
        return;
    }


    /**
     *  Gets the GradeId column name.
     *
     * @return the column name
     */

    protected String getGradeIdColumn() {
        return ("grade_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying grades. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches grade entries with any grade. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          grade, <code> false </code> to match entries with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeColumn(), match);
        return;
    }


    /**
     *  Clears the grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        getAssembler().clearTerms(getGradeColumn());
        return;
    }


    /**
     *  Gets the grade terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a <code> GradeSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grade. 
     *
     *  @return the grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Gets the Grade column name.
     *
     * @return the column name
     */

    protected String getGradeColumn() {
        return ("grade");
    }


    /**
     *  Matches grade entries which score is between the specified score 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchScore(java.math.BigDecimal start, 
                           java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getScoreColumn(), start, end, match);
        return;
    }


    /**
     *  Matches grade entries with any score. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          score, <code> false </code> to match entries with no score 
     */

    @OSID @Override
    public void matchAnyScore(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getScoreColumn(), match);
        return;
    }


    /**
     *  Clears the score terms. 
     */

    @OSID @Override
    public void clearScoreTerms() {
        getAssembler().clearTerms(getScoreColumn());
        return;
    }


    /**
     *  Gets the score terms. 
     *
     *  @return the score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getScoreColumn()));
    }


    /**
     *  Gets the Score column name.
     *
     * @return the column name
     */

    protected String getScoreColumn() {
        return ("score");
    }


    /**
     *  Matches grade entries which graded time is between the specified times 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchTimeGraded(org.osid.calendaring.DateTime start, 
                                org.osid.calendaring.DateTime end, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimeGradedColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the time graded terms. 
     */

    @OSID @Override
    public void clearTimeGradedTerms() {
        getAssembler().clearTerms(getTimeGradedColumn());
        return;
    }


    /**
     *  Gets the time graded terms. 
     *
     *  @return the time graded terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeGradedTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimeGradedColumn()));
    }


    /**
     *  Specified a preference for ordering results by the time graded. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeGraded(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimeGradedColumn(), style);
        return;
    }


    /**
     *  Gets the TimeGraded column name.
     *
     * @return the column name
     */

    protected String getTimeGradedColumn() {
        return ("time_graded");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraderId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getGraderIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the grader <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGraderIdTerms() {
        getAssembler().clearTerms(getGraderIdColumn());
        return;
    }


    /**
     *  Gets the grader <code> Id </code> terms. 
     *
     *  @return the grader <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraderIdTerms() {
        return (getAssembler().getIdTerms(getGraderIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the grader. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrader(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGraderColumn(), style);
        return;
    }


    /**
     *  Gets the GraderId column name.
     *
     * @return the column name
     */

    protected String getGraderIdColumn() {
        return ("grader_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  graders. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getGraderQuery() {
        throw new org.osid.UnimplementedException("supportsGraderQuery() is false");
    }


    /**
     *  Matches grade entries with any grader. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          grader, <code> false </code> to match entries with no grader 
     */

    @OSID @Override
    public void matchAnyGrader(boolean match) {
        getAssembler().addIdWildcardTerm(getGraderColumn(), match);
        return;
    }


    /**
     *  Clears the grader terms. 
     */

    @OSID @Override
    public void clearGraderTerms() {
        getAssembler().clearTerms(getGraderColumn());
        return;
    }


    /**
     *  Gets the grader terms. 
     *
     *  @return the grader terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getGraderTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available for grader 
     *  resources. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grader. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getGraderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGraderSearchOrder() is false");
    }


    /**
     *  Gets the Grader column name.
     *
     * @return the column name
     */

    protected String getGraderColumn() {
        return ("grader");
    }


    /**
     *  Sets the grading agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getGradingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the grader <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingAgentIdTerms() {
        getAssembler().clearTerms(getGradingAgentIdColumn());
        return;
    }


    /**
     *  Gets the grading agent <code> Id </code> terms. 
     *
     *  @return the grading agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingAgentIdTerms() {
        return (getAssembler().getIdTerms(getGradingAgentIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the grading agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the GradingAgentId column name.
     *
     * @return the column name
     */

    protected String getGradingAgentIdColumn() {
        return ("grading_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  grading agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getGradingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsGradingAgentQuery() is false");
    }


    /**
     *  Matches grade entries with any grading agent. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          grading agent, <code> false </code> to match entries with no 
     *          grading agent 
     */

    @OSID @Override
    public void matchAnyGradingAgent(boolean match) {
        getAssembler().addIdWildcardTerm(getGradingAgentColumn(), match);
        return;
    }


    /**
     *  Clears the grading agent terms. 
     */

    @OSID @Override
    public void clearGradingAgentTerms() {
        getAssembler().clearTerms(getGradingAgentColumn());
        return;
    }


    /**
     *  Gets the grading agent terms. 
     *
     *  @return the grading agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getGradingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an <code> AgentSearchOrder </code> is available fo grading 
     *  agents. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grading agent. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getGradingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradingAgentSearchOrder() is false");
    }


    /**
     *  Gets the GradingAgent column name.
     *
     * @return the column name
     */

    protected String getGradingAgentColumn() {
        return ("grading_agent");
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        getAssembler().addIdTerm(getGradebookIdColumn(), gradebookId, match);
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        getAssembler().clearTerms(getGradebookIdColumn());
        return;
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (getAssembler().getIdTerms(getGradebookIdColumn()));
    }


    /**
     *  Gets the GradebookId column name.
     *
     * @return the column name
     */

    protected String getGradebookIdColumn() {
        return ("gradebook_id");
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        getAssembler().clearTerms(getGradebookColumn());
        return;
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the Gradebook column name.
     *
     * @return the column name
     */

    protected String getGradebookColumn() {
        return ("gradebook");
    }


    /**
     *  Tests if this gradeEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradeEntryRecordType a grade entry record type 
     *  @return <code>true</code> if the gradeEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeEntryRecordType) {
        for (org.osid.grading.records.GradeEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  gradeEntryRecordType the grade entry record type 
     *  @return the grade entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntryQueryRecord getGradeEntryQueryRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  gradeEntryRecordType the grade entry record type 
     *  @return the grade entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntryQueryInspectorRecord getGradeEntryQueryInspectorRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param gradeEntryRecordType the grade entry record type
     *  @return the grade entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntrySearchOrderRecord getGradeEntrySearchOrderRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this grade entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeEntryQueryRecord the grade entry query record
     *  @param gradeEntryQueryInspectorRecord the grade entry query inspector
     *         record
     *  @param gradeEntrySearchOrderRecord the grade entry search order record
     *  @param gradeEntryRecordType grade entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryQueryRecord</code>,
     *          <code>gradeEntryQueryInspectorRecord</code>,
     *          <code>gradeEntrySearchOrderRecord</code> or
     *          <code>gradeEntryRecordTypegradeEntry</code> is
     *          <code>null</code>
     */
            
    protected void addGradeEntryRecords(org.osid.grading.records.GradeEntryQueryRecord gradeEntryQueryRecord, 
                                      org.osid.grading.records.GradeEntryQueryInspectorRecord gradeEntryQueryInspectorRecord, 
                                      org.osid.grading.records.GradeEntrySearchOrderRecord gradeEntrySearchOrderRecord, 
                                      org.osid.type.Type gradeEntryRecordType) {

        addRecordType(gradeEntryRecordType);

        nullarg(gradeEntryQueryRecord, "grade entry query record");
        nullarg(gradeEntryQueryInspectorRecord, "grade entry query inspector record");
        nullarg(gradeEntrySearchOrderRecord, "grade entry search odrer record");

        this.queryRecords.add(gradeEntryQueryRecord);
        this.queryInspectorRecords.add(gradeEntryQueryInspectorRecord);
        this.searchOrderRecords.add(gradeEntrySearchOrderRecord);
        
        return;
    }
}
