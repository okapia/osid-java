//
// AbstractMapRealmLookupSession
//
//    A simple framework for providing a Realm lookup service
//    backed by a fixed collection of realms.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Realm lookup service backed by a
 *  fixed collection of realms. The realms are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Realms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRealmLookupSession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractRealmLookupSession
    implements org.osid.personnel.RealmLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.personnel.Realm> realms = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.personnel.Realm>());


    /**
     *  Makes a <code>Realm</code> available in this session.
     *
     *  @param  realm a realm
     *  @throws org.osid.NullArgumentException <code>realm<code>
     *          is <code>null</code>
     */

    protected void putRealm(org.osid.personnel.Realm realm) {
        this.realms.put(realm.getId(), realm);
        return;
    }


    /**
     *  Makes an array of realms available in this session.
     *
     *  @param  realms an array of realms
     *  @throws org.osid.NullArgumentException <code>realms<code>
     *          is <code>null</code>
     */

    protected void putRealms(org.osid.personnel.Realm[] realms) {
        putRealms(java.util.Arrays.asList(realms));
        return;
    }


    /**
     *  Makes a collection of realms available in this session.
     *
     *  @param  realms a collection of realms
     *  @throws org.osid.NullArgumentException <code>realms<code>
     *          is <code>null</code>
     */

    protected void putRealms(java.util.Collection<? extends org.osid.personnel.Realm> realms) {
        for (org.osid.personnel.Realm realm : realms) {
            this.realms.put(realm.getId(), realm);
        }

        return;
    }


    /**
     *  Removes a Realm from this session.
     *
     *  @param  realmId the <code>Id</code> of the realm
     *  @throws org.osid.NullArgumentException <code>realmId<code> is
     *          <code>null</code>
     */

    protected void removeRealm(org.osid.id.Id realmId) {
        this.realms.remove(realmId);
        return;
    }


    /**
     *  Gets the <code>Realm</code> specified by its <code>Id</code>.
     *
     *  @param  realmId <code>Id</code> of the <code>Realm</code>
     *  @return the realm
     *  @throws org.osid.NotFoundException <code>realmId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>realmId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.personnel.Realm realm = this.realms.get(realmId);
        if (realm == null) {
            throw new org.osid.NotFoundException("realm not found: " + realmId);
        }

        return (realm);
    }


    /**
     *  Gets all <code>Realms</code>. In plenary mode, the returned
     *  list contains all known realms or an error
     *  results. Otherwise, the returned list may contain only those
     *  realms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Realms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.realm.ArrayRealmList(this.realms.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.realms.clear();
        super.close();
        return;
    }
}
