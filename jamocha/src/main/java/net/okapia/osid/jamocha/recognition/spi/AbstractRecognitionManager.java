//
// AbstractRecognitionManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRecognitionManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.recognition.RecognitionManager,
               org.osid.recognition.RecognitionProxyManager {

    private final Types conferralRecordTypes               = new TypeRefSet();
    private final Types conferralSearchRecordTypes         = new TypeRefSet();

    private final Types awardRecordTypes                   = new TypeRefSet();
    private final Types awardSearchRecordTypes             = new TypeRefSet();

    private final Types convocationRecordTypes             = new TypeRefSet();
    private final Types convocationSearchRecordTypes       = new TypeRefSet();

    private final Types academyRecordTypes                 = new TypeRefSet();
    private final Types academySearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRecognitionManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRecognitionManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any award federation is exposed. Federation is exposed when a 
     *  specific award may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of awards 
     *  appears as a single award. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a conferral lookup service. 
     *
     *  @return <code> true </code> if conferral lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralLookup() {
        return (false);
    }


    /**
     *  Tests if querying conferrals is available. 
     *
     *  @return <code> true </code> if conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (false);
    }


    /**
     *  Tests if searching for conferrals is available. 
     *
     *  @return <code> true </code> if conferral search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralSearch() {
        return (false);
    }


    /**
     *  Tests if searching for conferrals is available. 
     *
     *  @return <code> true </code> if conferral search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralAdmin() {
        return (false);
    }


    /**
     *  Tests if conferral notification is available. 
     *
     *  @return <code> true </code> if conferral notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralNotification() {
        return (false);
    }


    /**
     *  Tests if a conferral to academy lookup session is available. 
     *
     *  @return <code> true </code> if conferral academy lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralAcademy() {
        return (false);
    }


    /**
     *  Tests if a conferral to academy assignment session is available. 
     *
     *  @return <code> true </code> if conferral academy assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralAcademyAssignment() {
        return (false);
    }


    /**
     *  Tests if a conferral smart academy session is available. 
     *
     *  @return <code> true </code> if conferral smart academy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralSmartAcademy() {
        return (false);
    }


    /**
     *  Tests for the availability of an award lookup service. 
     *
     *  @return <code> true </code> if award lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardLookup() {
        return (false);
    }


    /**
     *  Tests if querying awards is available. 
     *
     *  @return <code> true </code> if award query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Tests if searching for awards is available. 
     *
     *  @return <code> true </code> if award search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of an award administrative service for 
     *  creating and deleting awards. 
     *
     *  @return <code> true </code> if award administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of an award notification service. 
     *
     *  @return <code> true </code> if award notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardNotification() {
        return (false);
    }


    /**
     *  Tests if an award to academy lookup session is available. 
     *
     *  @return <code> true </code> if award academy lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardAcademy() {
        return (false);
    }


    /**
     *  Tests if an award to academy assignment session is available. 
     *
     *  @return <code> true </code> if award academy assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardAcademyAssignment() {
        return (false);
    }


    /**
     *  Tests if an award smart academy session is available. 
     *
     *  @return <code> true </code> if award smart academy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardSmartAcademy() {
        return (false);
    }


    /**
     *  Tests for the availability of a convocation service for getting 
     *  available convocations for a resource. 
     *
     *  @return <code> true </code> if convocation is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocation() {
        return (false);
    }


    /**
     *  Tests for the availability of a convocation lookup service. 
     *
     *  @return <code> true </code> if convocation lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationLookup() {
        return (false);
    }


    /**
     *  Tests if querying convocations is available. 
     *
     *  @return <code> true </code> if convocation query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationQuery() {
        return (false);
    }


    /**
     *  Tests if searching for convocations is available. 
     *
     *  @return <code> true </code> if convocation search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationSearch() {
        return (false);
    }


    /**
     *  Tests if searching for convocations is available. 
     *
     *  @return <code> true </code> if convocation search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationAdmin() {
        return (false);
    }


    /**
     *  Tests if convocation notification is available. 
     *
     *  @return <code> true </code> if convocation notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationNotification() {
        return (false);
    }


    /**
     *  Tests if a convocation to academy lookup session is available. 
     *
     *  @return <code> true </code> if convocation academy lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationAcademy() {
        return (false);
    }


    /**
     *  Tests if a convocation to academy assignment session is available. 
     *
     *  @return <code> true </code> if convocation academy assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationAcademyAssignment() {
        return (false);
    }


    /**
     *  Tests if a convocation smart academy session is available. 
     *
     *  @return <code> true </code> if convocation smart academy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationSmartAcademy() {
        return (false);
    }


    /**
     *  Tests for the availability of an academy lookup service. 
     *
     *  @return <code> true </code> if academy lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyLookup() {
        return (false);
    }


    /**
     *  Tests if querying academies is available. 
     *
     *  @return <code> true </code> if academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (false);
    }


    /**
     *  Tests if searching for academies is available. 
     *
     *  @return <code> true </code> if academy search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademySearch() {
        return (false);
    }


    /**
     *  Tests for the availability of an academy administrative service for 
     *  creating and deleting academies. 
     *
     *  @return <code> true </code> if academy administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of an academy notification service. 
     *
     *  @return <code> true </code> if academy notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of an academy hierarchy traversal service. 
     *
     *  @return <code> true </code> if academy hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of an academy hierarchy design service. 
     *
     *  @return <code> true </code> if academy hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a recognition batch service. 
     *
     *  @return <code> true </code> if a recognition batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecognitionBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Conferral </code> record types. 
     *
     *  @return a list containing the supported conferral record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConferralRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.conferralRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Conferral </code> record type is supported. 
     *
     *  @param  conferralRecordType a <code> Type </code> indicating a <code> 
     *          Conferral </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> conferralRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConferralRecordType(org.osid.type.Type conferralRecordType) {
        return (this.conferralRecordTypes.contains(conferralRecordType));
    }


    /**
     *  Adds support for a conferral record type.
     *
     *  @param conferralRecordType a conferral record type
     *  @throws org.osid.NullArgumentException
     *  <code>conferralRecordType</code> is <code>null</code>
     */

    protected void addConferralRecordType(org.osid.type.Type conferralRecordType) {
        this.conferralRecordTypes.add(conferralRecordType);
        return;
    }


    /**
     *  Removes support for a conferral record type.
     *
     *  @param conferralRecordType a conferral record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>conferralRecordType</code> is <code>null</code>
     */

    protected void removeConferralRecordType(org.osid.type.Type conferralRecordType) {
        this.conferralRecordTypes.remove(conferralRecordType);
        return;
    }


    /**
     *  Gets the supported conferral search record types. 
     *
     *  @return a list containing the supported conferral search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConferralSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.conferralSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given conferral search record type is supported. 
     *
     *  @param  conferralSearchRecordType a <code> Type </code> indicating a 
     *          conferral record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          conferralSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConferralSearchRecordType(org.osid.type.Type conferralSearchRecordType) {
        return (this.conferralSearchRecordTypes.contains(conferralSearchRecordType));
    }


    /**
     *  Adds support for a conferral search record type.
     *
     *  @param conferralSearchRecordType a conferral search record type
     *  @throws org.osid.NullArgumentException
     *  <code>conferralSearchRecordType</code> is <code>null</code>
     */

    protected void addConferralSearchRecordType(org.osid.type.Type conferralSearchRecordType) {
        this.conferralSearchRecordTypes.add(conferralSearchRecordType);
        return;
    }


    /**
     *  Removes support for a conferral search record type.
     *
     *  @param conferralSearchRecordType a conferral search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>conferralSearchRecordType</code> is <code>null</code>
     */

    protected void removeConferralSearchRecordType(org.osid.type.Type conferralSearchRecordType) {
        this.conferralSearchRecordTypes.remove(conferralSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Award </code> record types. 
     *
     *  @return a list containing the supported award record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.awardRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Award </code> record type is supported. 
     *
     *  @param  awardRecordType a <code> Type </code> indicating a <code> 
     *          Award </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> awardRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardRecordType(org.osid.type.Type awardRecordType) {
        return (this.awardRecordTypes.contains(awardRecordType));
    }


    /**
     *  Adds support for an award record type.
     *
     *  @param awardRecordType an award record type
     *  @throws org.osid.NullArgumentException
     *  <code>awardRecordType</code> is <code>null</code>
     */

    protected void addAwardRecordType(org.osid.type.Type awardRecordType) {
        this.awardRecordTypes.add(awardRecordType);
        return;
    }


    /**
     *  Removes support for an award record type.
     *
     *  @param awardRecordType an award record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>awardRecordType</code> is <code>null</code>
     */

    protected void removeAwardRecordType(org.osid.type.Type awardRecordType) {
        this.awardRecordTypes.remove(awardRecordType);
        return;
    }


    /**
     *  Gets the supported award search record types. 
     *
     *  @return a list containing the supported award search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.awardSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given award search record type is supported. 
     *
     *  @param  awardSearchRecordType a <code> Type </code> indicating an 
     *          award record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> awardSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardSearchRecordType(org.osid.type.Type awardSearchRecordType) {
        return (this.awardSearchRecordTypes.contains(awardSearchRecordType));
    }


    /**
     *  Adds support for an award search record type.
     *
     *  @param awardSearchRecordType an award search record type
     *  @throws org.osid.NullArgumentException
     *  <code>awardSearchRecordType</code> is <code>null</code>
     */

    protected void addAwardSearchRecordType(org.osid.type.Type awardSearchRecordType) {
        this.awardSearchRecordTypes.add(awardSearchRecordType);
        return;
    }


    /**
     *  Removes support for an award search record type.
     *
     *  @param awardSearchRecordType an award search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>awardSearchRecordType</code> is <code>null</code>
     */

    protected void removeAwardSearchRecordType(org.osid.type.Type awardSearchRecordType) {
        this.awardSearchRecordTypes.remove(awardSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Convocation </code> record types. 
     *
     *  @return a list containing the supported convocation record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConvocationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.convocationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Convocation </code> record type is 
     *  supported. 
     *
     *  @param  convocationRecordType a <code> Type </code> indicating a 
     *          <code> Convocation </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> convocationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConvocationRecordType(org.osid.type.Type convocationRecordType) {
        return (this.convocationRecordTypes.contains(convocationRecordType));
    }


    /**
     *  Adds support for a convocation record type.
     *
     *  @param convocationRecordType a convocation record type
     *  @throws org.osid.NullArgumentException
     *  <code>convocationRecordType</code> is <code>null</code>
     */

    protected void addConvocationRecordType(org.osid.type.Type convocationRecordType) {
        this.convocationRecordTypes.add(convocationRecordType);
        return;
    }


    /**
     *  Removes support for a convocation record type.
     *
     *  @param convocationRecordType a convocation record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>convocationRecordType</code> is <code>null</code>
     */

    protected void removeConvocationRecordType(org.osid.type.Type convocationRecordType) {
        this.convocationRecordTypes.remove(convocationRecordType);
        return;
    }


    /**
     *  Gets the supported convocation search record types. 
     *
     *  @return a list containing the supported convocation search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConvocationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.convocationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given convocation search record type is supported. 
     *
     *  @param  convocationSearchRecordType a <code> Type </code> indicating a 
     *          convocation record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          convocationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConvocationSearchRecordType(org.osid.type.Type convocationSearchRecordType) {
        return (this.convocationSearchRecordTypes.contains(convocationSearchRecordType));
    }


    /**
     *  Adds support for a convocation search record type.
     *
     *  @param convocationSearchRecordType a convocation search record type
     *  @throws org.osid.NullArgumentException
     *  <code>convocationSearchRecordType</code> is <code>null</code>
     */

    protected void addConvocationSearchRecordType(org.osid.type.Type convocationSearchRecordType) {
        this.convocationSearchRecordTypes.add(convocationSearchRecordType);
        return;
    }


    /**
     *  Removes support for a convocation search record type.
     *
     *  @param convocationSearchRecordType a convocation search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>convocationSearchRecordType</code> is <code>null</code>
     */

    protected void removeConvocationSearchRecordType(org.osid.type.Type convocationSearchRecordType) {
        this.convocationSearchRecordTypes.remove(convocationSearchRecordType);
        return;
    }


    /**
     *  Gets the supported academy record types. 
     *
     *  @return a list containing the supported academy record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAcademyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.academyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given academy record type is supported. 
     *
     *  @param  academyRecordType a <code> Type </code> indicating an academy 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> academyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAcademyRecordType(org.osid.type.Type academyRecordType) {
        return (this.academyRecordTypes.contains(academyRecordType));
    }


    /**
     *  Adds support for an academy record type.
     *
     *  @param academyRecordType an academy record type
     *  @throws org.osid.NullArgumentException
     *  <code>academyRecordType</code> is <code>null</code>
     */

    protected void addAcademyRecordType(org.osid.type.Type academyRecordType) {
        this.academyRecordTypes.add(academyRecordType);
        return;
    }


    /**
     *  Removes support for an academy record type.
     *
     *  @param academyRecordType an academy record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>academyRecordType</code> is <code>null</code>
     */

    protected void removeAcademyRecordType(org.osid.type.Type academyRecordType) {
        this.academyRecordTypes.remove(academyRecordType);
        return;
    }


    /**
     *  Gets the supported academy search record types. 
     *
     *  @return a list containing the supported academy search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAcademySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.academySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given academy search record type is supported. 
     *
     *  @param  academySearchRecordType a <code> Type </code> indicating an 
     *          academy record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> academySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAcademySearchRecordType(org.osid.type.Type academySearchRecordType) {
        return (this.academySearchRecordTypes.contains(academySearchRecordType));
    }


    /**
     *  Adds support for an academy search record type.
     *
     *  @param academySearchRecordType an academy search record type
     *  @throws org.osid.NullArgumentException
     *  <code>academySearchRecordType</code> is <code>null</code>
     */

    protected void addAcademySearchRecordType(org.osid.type.Type academySearchRecordType) {
        this.academySearchRecordTypes.add(academySearchRecordType);
        return;
    }


    /**
     *  Removes support for an academy search record type.
     *
     *  @param academySearchRecordType an academy search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>academySearchRecordType</code> is <code>null</code>
     */

    protected void removeAcademySearchRecordType(org.osid.type.Type academySearchRecordType) {
        this.academySearchRecordTypes.remove(academySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  lookup service. 
     *
     *  @return a <code> ConferralLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralLookupSession getConferralLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConferralLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralLookupSession getConferralLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  lookup service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralLookupSession getConferralLookupSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralLookupSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  lookup service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralLookupSession getConferralLookupSessionForAcademy(org.osid.id.Id academyId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralLookupSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  query service. 
     *
     *  @return a <code> ConferralQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuerySession getConferralQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConferralQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuerySession getConferralQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  query service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuerySession getConferralQuerySessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralQuerySessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  query service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuerySession getConferralQuerySessionForAcademy(org.osid.id.Id academyId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralQuerySessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  search service. 
     *
     *  @return a <code> ConferralSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSearchSession getConferralSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConferralSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSearchSession getConferralSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  search service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSearchSession getConferralSearchSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralSearchSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  search service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSearchSession getConferralSearchSessionForAcademy(org.osid.id.Id academyId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralSearchSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  administration service. 
     *
     *  @return a <code> ConferralAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAdminSession getConferralAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConferralAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAdminSession getConferralAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAdminSession getConferralAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAdminSession getConferralAdminSessionForAcademy(org.osid.id.Id academyId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  notification service. 
     *
     *  @param  conferralReceiver the receiver 
     *  @return a <code> ConferralNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> conferralReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralNotificationSession getConferralNotificationSession(org.osid.recognition.ConferralReceiver conferralReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  notification service. 
     *
     *  @param  conferralReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> conferralReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralNotificationSession getConferralNotificationSession(org.osid.recognition.ConferralReceiver conferralReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  notification service for the given academy. 
     *
     *  @param  conferralReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> conferralReceiver 
     *          </code> or <code> academyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralNotificationSession getConferralNotificationSessionForAcademy(org.osid.recognition.ConferralReceiver conferralReceiver, 
                                                                                                       org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralNotificationSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  notification service for the given academy. 
     *
     *  @param  conferralReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> conferralReceiver, 
     *          academyId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralNotificationSession getConferralNotificationSessionForAcademy(org.osid.recognition.ConferralReceiver conferralReceiver, 
                                                                                                       org.osid.id.Id academyId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralNotificationSessionForAcademy not implemented");
    }


    /**
     *  Gets the session for retrieving conferral to academy mappings. 
     *
     *  @return a <code> ConferralAcademySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAcademySession getConferralAcademySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralAcademySession not implemented");
    }


    /**
     *  Gets the session for retrieving conferral to academy mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConferralAcademySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAcademySession getConferralAcademySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralAcademySession not implemented");
    }


    /**
     *  Gets the session for assigning conferral to academy mappings. 
     *
     *  @return a <code> ConferralAcademyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAcademyAssignmentSession getConferralAcademyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralAcademyAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning conferral to academy mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConferralAcademyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAcademyAssignmentSession getConferralAcademyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralAcademyAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the conferral smart academy for the 
     *  given academy. 
     *
     *  @param  academyId the <code> Id </code> of the academy 
     *  @return a <code> ConferralSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> conferralBookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> conferralBookId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSmartAcademy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSmartAcademySession getConferralSmartAcademySession(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConferralSmartAcademySession not implemented");
    }


    /**
     *  Gets the session associated with the conferral smart academy for the 
     *  given academy. 
     *
     *  @param  academyId the <code> Id </code> of the conferral book 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> conferralBookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> conferralBookId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSmartAcademy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSmartAcademySession getConferralSmartAcademySession(org.osid.id.Id academyId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConferralSmartAcademySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award lookup 
     *  service. 
     *
     *  @return an <code> AwardLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardLookupSession getAwardLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardLookupSession getAwardLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award lookup 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardLookupSession getAwardLookupSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardLookupSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award lookup 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AwardLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardLookupSession getAwardLookupSessionForAcademy(org.osid.id.Id academyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardLookupSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award query 
     *  service. 
     *
     *  @return an <code> AwardQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuerySession getAwardQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuerySession getAwardQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award query 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuerySession getAwardQuerySessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardQuerySessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award query 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AwardQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Award </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuerySession getAwardQuerySessionForAcademy(org.osid.id.Id academyId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardQuerySessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award search 
     *  service. 
     *
     *  @return an <code> AwardSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchSession getAwardSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchSession getAwardSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award search 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchSession getAwardSearchSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardSearchSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award search 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AwardSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Award </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchSession getAwardSearchSessionForAcademy(org.osid.id.Id academyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardSearchSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  administrative service. 
     *
     *  @return an <code> AwardAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAdminSession getAwardAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAdminSession getAwardAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  administrative service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAdminSession getAwardAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AwardAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Award </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAdminSession getAwardAdminSessionForAcademy(org.osid.id.Id academyId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  notification service. 
     *
     *  @param  awardReceiver the receiver 
     *  @return an <code> AwardNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> awardReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardNotificationSession getAwardNotificationSession(org.osid.recognition.AwardReceiver awardReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  notification service. 
     *
     *  @param  awardReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AwardNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> awardReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardNotificationSession getAwardNotificationSession(org.osid.recognition.AwardReceiver awardReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  notification service for the given academy. 
     *
     *  @param  awardReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> awardReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardNotificationSession getAwardNotificationSessionForAcademy(org.osid.recognition.AwardReceiver awardReceiver, 
                                                                                               org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardNotificationSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  notification service for the given academy. 
     *
     *  @param  awardReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AwardNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Award </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> awardReceiver, academyId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardNotificationSession getAwardNotificationSessionForAcademy(org.osid.recognition.AwardReceiver awardReceiver, 
                                                                                               org.osid.id.Id academyId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardNotificationSessionForAcademy not implemented");
    }


    /**
     *  Gets the session for retrieving award to academy mappings. 
     *
     *  @return an <code> AwardAcademySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAcademy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAcademySession getAwardAcademySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardAcademySession not implemented");
    }


    /**
     *  Gets the session for retrieving award to academy mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardAcademySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAcademy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAcademySession getAwardAcademySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardAcademySession not implemented");
    }


    /**
     *  Gets the session for assigning award to academy mappings. 
     *
     *  @return an <code> AwardAcademyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAcademyAssignmentSession getAwardAcademyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardAcademyAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning award to academy mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardAcademyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAcademyAssignmentSession getAwardAcademyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardAcademyAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the award smart academy for the given 
     *  academy. 
     *
     *  @param  academyId the <code> Id </code> of the academy 
     *  @return an <code> AwardSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> academyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardSmartAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSmartAcademySession getAwardSmartAcademySession(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAwardSmartAcademySession not implemented");
    }


    /**
     *  Gets the session for managing dynamic award academies for the given 
     *  academy. 
     *
     *  @param  academyId the <code> Id </code> of an academy 
     *  @param  proxy a proxy 
     *  @return an <code> AwardSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> academyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardSmartAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSmartAcademySession getAwardSmartAcademySession(org.osid.id.Id academyId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAwardSmartAcademySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  lookup service. 
     *
     *  @return a <code> ConvocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationLookupSession getConvocationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationLookupSession getConvocationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  lookup service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationLookupSession getConvocationLookupSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationLookupSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  lookup service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationLookupSession getConvocationLookupSessionForAcademy(org.osid.id.Id academyId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationLookupSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  query service. 
     *
     *  @return a <code> ConvocationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuerySession getConvocationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuerySession getConvocationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  query service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuerySession getConvocationQuerySessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationQuerySessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  query service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuerySession getConvocationQuerySessionForAcademy(org.osid.id.Id academyId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationQuerySessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  search service. 
     *
     *  @return a <code> ConvocationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchSession getConvocationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchSession getConvocationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  search service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchSession getConvocationSearchSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationSearchSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  search service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchSession getConvocationSearchSessionForAcademy(org.osid.id.Id academyId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationSearchSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  administration service. 
     *
     *  @return a <code> ConvocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAdminSession getConvocationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAdminSession getConvocationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAdminSession getConvocationAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAdminSession getConvocationAdminSessionForAcademy(org.osid.id.Id academyId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  notification service. 
     *
     *  @param  convocationReceiver the receiver 
     *  @return a <code> ConvocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> convocationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationNotificationSession getConvocationNotificationSession(org.osid.recognition.ConvocationReceiver convocationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  notification service. 
     *
     *  @param  convocationReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> convocationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationNotificationSession getConvocationNotificationSession(org.osid.recognition.ConvocationReceiver convocationReceiver, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  notification service for the given academy. 
     *
     *  @param  convocationReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> convocationReceiver 
     *          </code> or <code> academyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationNotificationSession getConvocationNotificationSessionForAcademy(org.osid.recognition.ConvocationReceiver convocationReceiver, 
                                                                                                           org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationNotificationSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  notification service for the given academy. 
     *
     *  @param  convocationReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> convocationReceiver, 
     *          academyId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationNotificationSession getConvocationNotificationSessionForAcademy(org.osid.recognition.ConvocationReceiver convocationReceiver, 
                                                                                                           org.osid.id.Id academyId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationNotificationSessionForAcademy not implemented");
    }


    /**
     *  Gets the session for retrieving convocation to academy mappings. 
     *
     *  @return a <code> ConvocationAcademySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAcademySession getConvocationAcademySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationAcademySession not implemented");
    }


    /**
     *  Gets the session for retrieving convocation to academy mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationAcademySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAcademySession getConvocationAcademySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationAcademySession not implemented");
    }


    /**
     *  Gets the session for assigning convocation to academy mappings. 
     *
     *  @return a <code> ConvocationAcademyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAcademyAssignmentSession getConvocationAcademyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationAcademyAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning convocation to academy mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationAcademyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAcademyAssignmentSession getConvocationAcademyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationAcademyAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the convocation smart academy for the 
     *  given academy. 
     *
     *  @param  academyId the <code> Id </code> of the convocation book 
     *  @return a <code> ConvocationSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> convocationBookId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> convocationBookId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSmartAcademy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSmartAcademySession getConvocationSmartAcademySession(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getConvocationSmartAcademySession not implemented");
    }


    /**
     *  Gets the session associated with the convocation smart academy for the 
     *  given academy. 
     *
     *  @param  academyId the <code> Id </code> of the convocation book 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> convocationBookId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> convocationBookId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSmartAcademy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSmartAcademySession getConvocationSmartAcademySession(org.osid.id.Id academyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getConvocationSmartAcademySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy lookup 
     *  service. 
     *
     *  @return an <code> AcademyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyLookupSession getAcademyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAcademyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AcademyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyLookupSession getAcademyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAcademyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy query 
     *  service. 
     *
     *  @return an <code> AcademyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuerySession getAcademyQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAcademyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AcademyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuerySession getAcademyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAcademyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy search 
     *  service. 
     *
     *  @return an <code> AcademySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademySearchSession getAcademySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAcademySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AcademySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademySearchSession getAcademySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAcademySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  administrative service. 
     *
     *  @return an <code> AcademyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyAdminSession getAcademyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAcademyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AcademyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyAdminSession getAcademyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAcademyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  notification service. 
     *
     *  @param  academyReceiver the receiver 
     *  @return an <code> AcademyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> academyReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyNotificationSession getAcademyNotificationSession(org.osid.recognition.AcademyReceiver academyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAcademyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  notification service. 
     *
     *  @param  academyReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AcademyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> academyReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyNotificationSession getAcademyNotificationSession(org.osid.recognition.AcademyReceiver academyReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAcademyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  hierarchy service. 
     *
     *  @return an <code> AcademyHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyHierarchySession getAcademyHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAcademyHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AcademyHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyHierarchySession getAcademyHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAcademyHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  hierarchy design service. 
     *
     *  @return an <code> AcademyHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyHierarchyDesignSession getAcademyHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getAcademyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AcademyHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyHierarchyDesignSession getAcademyHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getAcademyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> RecognitionBatchManager. </code> 
     *
     *  @return a <code> RecognitionBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecognitionBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.RecognitionBatchManager getRecognitionBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionManager.getRecognitionBatchManager not implemented");
    }


    /**
     *  Gets a <code> RecognitionBatchProxyManager. </code> 
     *
     *  @return a <code> RecognitionBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecognitionBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.RecognitionBatchProxyManager getRecognitionBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.RecognitionProxyManager.getRecognitionBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.conferralRecordTypes.clear();
        this.conferralRecordTypes.clear();

        this.conferralSearchRecordTypes.clear();
        this.conferralSearchRecordTypes.clear();

        this.awardRecordTypes.clear();
        this.awardRecordTypes.clear();

        this.awardSearchRecordTypes.clear();
        this.awardSearchRecordTypes.clear();

        this.convocationRecordTypes.clear();
        this.convocationRecordTypes.clear();

        this.convocationSearchRecordTypes.clear();
        this.convocationSearchRecordTypes.clear();

        this.academyRecordTypes.clear();
        this.academyRecordTypes.clear();

        this.academySearchRecordTypes.clear();
        this.academySearchRecordTypes.clear();

        return;
    }
}
