//
// InvariantMapProxyAuctionLookupSession
//
//    Implements an Auction lookup service backed by a fixed
//    collection of auctions. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding;


/**
 *  Implements an Auction lookup service backed by a fixed
 *  collection of auctions. The auctions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAuctionLookupSession
    extends net.okapia.osid.jamocha.core.bidding.spi.AbstractMapAuctionLookupSession
    implements org.osid.bidding.AuctionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuctionLookupSession} with no
     *  auctions.
     *
     *  @param auctionHouse the auction house
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.proxy.Proxy proxy) {
        setAuctionHouse(auctionHouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAuctionLookupSession} with a single
     *  auction.
     *
     *  @param auctionHouse the auction house
     *  @param auction an single auction
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auction} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.Auction auction, org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putAuction(auction);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAuctionLookupSession} using
     *  an array of auctions.
     *
     *  @param auctionHouse the auction house
     *  @param auctions an array of auctions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auctions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.Auction[] auctions, org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putAuctions(auctions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuctionLookupSession} using a
     *  collection of auctions.
     *
     *  @param auctionHouse the auction house
     *  @param auctions a collection of auctions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auctions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  java.util.Collection<? extends org.osid.bidding.Auction> auctions,
                                                  org.osid.proxy.Proxy proxy) {

        this(auctionHouse, proxy);
        putAuctions(auctions);
        return;
    }
}
