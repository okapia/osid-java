//
// MutableIndexedMapRealmLookupSession
//
//    Implements a Realm lookup service backed by a collection of
//    realms indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements a Realm lookup service backed by a collection of
 *  realms. The realms are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some realms may be compatible
 *  with more types than are indicated through these realm
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of realms can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRealmLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractIndexedMapRealmLookupSession
    implements org.osid.personnel.RealmLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRealmLookupSession} with no
     *  realms.
     */

    public MutableIndexedMapRealmLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRealmLookupSession} with a
     *  single realm.
     *  
     *  @param  realm a single realm
     *  @throws org.osid.NullArgumentException {@code realm}
     *          is {@code null}
     */

    public MutableIndexedMapRealmLookupSession(org.osid.personnel.Realm realm) {
        putRealm(realm);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRealmLookupSession} using an
     *  array of realms.
     *
     *  @param  realms an array of realms
     *  @throws org.osid.NullArgumentException {@code realms}
     *          is {@code null}
     */

    public MutableIndexedMapRealmLookupSession(org.osid.personnel.Realm[] realms) {
        putRealms(realms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRealmLookupSession} using a
     *  collection of realms.
     *
     *  @param  realms a collection of realms
     *  @throws org.osid.NullArgumentException {@code realms} is
     *          {@code null}
     */

    public MutableIndexedMapRealmLookupSession(java.util.Collection<? extends org.osid.personnel.Realm> realms) {
        putRealms(realms);
        return;
    }
    

    /**
     *  Makes a {@code Realm} available in this session.
     *
     *  @param  realm a realm
     *  @throws org.osid.NullArgumentException {@code realm{@code  is
     *          {@code null}
     */

    @Override
    public void putRealm(org.osid.personnel.Realm realm) {
        super.putRealm(realm);
        return;
    }


    /**
     *  Makes an array of realms available in this session.
     *
     *  @param  realms an array of realms
     *  @throws org.osid.NullArgumentException {@code realms{@code 
     *          is {@code null}
     */

    @Override
    public void putRealms(org.osid.personnel.Realm[] realms) {
        super.putRealms(realms);
        return;
    }


    /**
     *  Makes collection of realms available in this session.
     *
     *  @param  realms a collection of realms
     *  @throws org.osid.NullArgumentException {@code realm{@code  is
     *          {@code null}
     */

    @Override
    public void putRealms(java.util.Collection<? extends org.osid.personnel.Realm> realms) {
        super.putRealms(realms);
        return;
    }


    /**
     *  Removes a Realm from this session.
     *
     *  @param realmId the {@code Id} of the realm
     *  @throws org.osid.NullArgumentException {@code realmId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRealm(org.osid.id.Id realmId) {
        super.removeRealm(realmId);
        return;
    }    
}
