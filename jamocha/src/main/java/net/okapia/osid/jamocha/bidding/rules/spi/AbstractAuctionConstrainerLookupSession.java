//
// AbstractAuctionConstrainerLookupSession.java
//
//    A starter implementation framework for providing an AuctionConstrainer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an AuctionConstrainer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAuctionConstrainers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAuctionConstrainerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.rules.AuctionConstrainerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();
    

    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }

    /**
     *  Tests if this user can perform <code>AuctionConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionConstrainers() {
        return (true);
    }


    /**
     *  A complete view of the <code>AuctionConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionConstrainerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>AuctionConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionConstrainerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction constrainers in auction houses which
     *  are children of this auction house in the auction house
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active auction constrainers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveAuctionConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auction constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>AuctionConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AuctionConstrainer</code> and retained for
     *  compatibility.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  @param  auctionConstrainerId <code>Id</code> of the
     *          <code>AuctionConstrainer</code>
     *  @return the auction constrainer
     *  @throws org.osid.NotFoundException <code>auctionConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainer getAuctionConstrainer(org.osid.id.Id auctionConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.bidding.rules.AuctionConstrainerList auctionConstrainers = getAuctionConstrainers()) {
            while (auctionConstrainers.hasNext()) {
                org.osid.bidding.rules.AuctionConstrainer auctionConstrainer = auctionConstrainers.getNextAuctionConstrainer();
                if (auctionConstrainer.getId().equals(auctionConstrainerId)) {
                    return (auctionConstrainer);
                }
            }
        } 

        throw new org.osid.NotFoundException(auctionConstrainerId + " not found");
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionConstrainers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>AuctionConstrainers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAuctionConstrainers()</code>.
     *
     *  @param  auctionConstrainerIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByIds(org.osid.id.IdList auctionConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.bidding.rules.AuctionConstrainer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = auctionConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAuctionConstrainer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("auction constrainer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.bidding.rules.auctionconstrainer.LinkedAuctionConstrainerList(ret));
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> corresponding to
     *  the given auction constrainer genus <code>Type</code> which
     *  does not include auction constrainers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAuctionConstrainers()</code>.
     *
     *  @param  auctionConstrainerGenusType an auctionConstrainer genus type 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByGenusType(org.osid.type.Type auctionConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionconstrainer.AuctionConstrainerGenusFilterList(getAuctionConstrainers(), auctionConstrainerGenusType));
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> corresponding to
     *  the given auction constrainer genus <code>Type</code> and
     *  include any additional auction constrainers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionConstrainers()</code>.
     *
     *  @param  auctionConstrainerGenusType an auctionConstrainer genus type 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByParentGenusType(org.osid.type.Type auctionConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuctionConstrainersByGenusType(auctionConstrainerGenusType));
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> containing the
     *  given auction constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known auction
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, auction constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction constrainers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionConstrainers()</code>.
     *
     *  @param  auctionConstrainerRecordType an auctionConstrainer record type 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByRecordType(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionconstrainer.AuctionConstrainerRecordFilterList(getAuctionConstrainers(), auctionConstrainerRecordType));
    }


    /**
     *  Gets all <code>AuctionConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @return a list of <code>AuctionConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the auction constrainer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of auction constrainers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.bidding.rules.AuctionConstrainerList filterAuctionConstrainersOnViews(org.osid.bidding.rules.AuctionConstrainerList list)
        throws org.osid.OperationFailedException {

        org.osid.bidding.rules.AuctionConstrainerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionconstrainer.ActiveAuctionConstrainerFilterList(ret);
        }

        return (ret);
    }
}
