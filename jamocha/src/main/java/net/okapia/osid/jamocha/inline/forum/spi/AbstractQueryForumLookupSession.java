//
// AbstractQueryForumLookupSession.java
//
//    An inline adapter that maps a ForumLookupSession to
//    a ForumQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ForumLookupSession to
 *  a ForumQuerySession.
 */

public abstract class AbstractQueryForumLookupSession
    extends net.okapia.osid.jamocha.forum.spi.AbstractForumLookupSession
    implements org.osid.forum.ForumLookupSession {

    private final org.osid.forum.ForumQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryForumLookupSession.
     *
     *  @param querySession the underlying forum query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryForumLookupSession(org.osid.forum.ForumQuerySession querySession) {
        nullarg(querySession, "forum query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Forum</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupForums() {
        return (this.session.canSearchForums());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Forum</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Forum</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Forum</code> and
     *  retained for compatibility.
     *
     *  @param  forumId <code>Id</code> of the
     *          <code>Forum</code>
     *  @return the forum
     *  @throws org.osid.NotFoundException <code>forumId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>forumId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.ForumQuery query = getQuery();
        query.matchId(forumId, true);
        org.osid.forum.ForumList forums = this.session.getForumsByQuery(query);
        if (forums.hasNext()) {
            return (forums.getNextForum());
        } 
        
        throw new org.osid.NotFoundException(forumId + " not found");
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  forums specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Forums</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  forumIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>forumIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByIds(org.osid.id.IdList forumIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.ForumQuery query = getQuery();

        try (org.osid.id.IdList ids = forumIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getForumsByQuery(query));
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  forum genus <code>Type</code> which does not include
     *  forums of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.ForumQuery query = getQuery();
        query.matchGenusType(forumGenusType, true);
        return (this.session.getForumsByQuery(query));
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  forum genus <code>Type</code> and include any additional
     *  forums with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByParentGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.ForumQuery query = getQuery();
        query.matchParentGenusType(forumGenusType, true);
        return (this.session.getForumsByQuery(query));
    }


    /**
     *  Gets a <code>ForumList</code> containing the given
     *  forum record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumRecordType a forum record type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByRecordType(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.ForumQuery query = getQuery();
        query.matchRecordType(forumRecordType, true);
        return (this.session.getForumsByQuery(query));
    }


    /**
     *  Gets a <code>ForumList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known forums or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  forums that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Forum</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.ForumQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getForumsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Forums</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Forums</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForums()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.ForumQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getForumsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.forum.ForumQuery getQuery() {
        org.osid.forum.ForumQuery query = this.session.getForumQuery();
        return (query);
    }
}
