//
// AbstractTimePeriod.java
//
//     Defines a TimePeriod.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>TimePeriod</code>.
 */

public abstract class AbstractTimePeriod
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.calendaring.TimePeriod {

    private org.osid.calendaring.DateTime start;
    private org.osid.calendaring.DateTime end;

    private final java.util.Collection<org.osid.calendaring.Event> exceptions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.TimePeriodRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the start time of the time period. 
     *
     *  @return the start time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStart() {
        return (this.start);
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setStart(org.osid.calendaring.DateTime date) {
        nullarg(date, "start date");
        this.start = date;
        return;
    }


    /**
     *  Gets the end time of the time period. 
     *
     *  @return the end time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEnd() {
        return (this.end);
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setEnd(org.osid.calendaring.DateTime date) {
        nullarg(date, "end date");
        this.end = date;
        return;
    }


    /**
     *  Gets the exception <code> Ids </code> to this time
     *  period. Recurring events overlapping with these events do not
     *  appear in any recurring event for this time period.
     *
     *  @return list of exception event <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getExceptionIds() {
        try {
            org.osid.calendaring.EventList exceptions = getExceptions();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.event.EventToIdList(exceptions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the exceptions to this time period. Recurring events
     *  overlapping with these events do not appear in any recurring
     *  event for this time period.
     *
     *  @return event exceptions 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getExceptions()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.calendaring.event.ArrayEventList(this.exceptions));
    }


    /**
     *  Adds an exception.
     *
     *  @param exception an exception
     *  @throws org.osid.NullArgumentException <code>exception</code>
     *          is <code>null</code>
     */

    protected void addException(org.osid.calendaring.Event exception) {
        nullarg(exception, "exception event");
        this.exceptions.add(exception);
        return;
    }


    /**
     *  Sets all the exceptions.
     *
     *  @param exceptions a collection of exceptions
     *  @throws org.osid.NullArgumentException <code>exceptions</code>
     *          is <code>null</code>
     */

    protected void setExceptions(java.util.Collection<org.osid.calendaring.Event> exceptions) {
        nullarg(exceptions, "exception events");

        this.exceptions.clear();
        this.exceptions.addAll(exceptions);

        return;
    }


    /**
     *  Tests if this time period supports the given record
     *  <code>Type</code>.
     *
     *  @param  timePeriodRecordType a time period record type 
     *  @return <code>true</code> if the timePeriodRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type timePeriodRecordType) {
        for (org.osid.calendaring.records.TimePeriodRecord record : this.records) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>TimePeriod</code> record <code>Type</code>.
     *
     *  @param  timePeriodRecordType the time period record type 
     *  @return the time period record 
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(timePeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodRecord getTimePeriodRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.TimePeriodRecord record : this.records) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this time period. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param timePeriodRecord the time period record
     *  @param timePeriodRecordType time period record type
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecord</code> or
     *          <code>timePeriodRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addTimePeriodRecord(org.osid.calendaring.records.TimePeriodRecord timePeriodRecord, 
                                       org.osid.type.Type timePeriodRecordType) {

        nullarg(timePeriodRecord, "time period record");
        addRecordType(timePeriodRecordType);
        this.records.add(timePeriodRecord);
        
        return;
    }
}
