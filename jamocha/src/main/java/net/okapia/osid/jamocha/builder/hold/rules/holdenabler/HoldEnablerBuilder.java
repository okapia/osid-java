//
// HoldEnabler.java
//
//     Defines a HoldEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.rules.holdenabler;


/**
 *  Defines a <code>HoldEnabler</code> builder.
 */

public final class HoldEnablerBuilder
    extends net.okapia.osid.jamocha.builder.hold.rules.holdenabler.spi.AbstractHoldEnablerBuilder<HoldEnablerBuilder> {
    

    /**
     *  Constructs a new <code>HoldEnablerBuilder</code> using a
     *  <code>MutableHoldEnabler</code>.
     */

    public HoldEnablerBuilder() {
        super(new MutableHoldEnabler());
        return;
    }


    /**
     *  Constructs a new <code>HoldEnablerBuilder</code> using the given
     *  mutable holdEnabler.
     * 
     *  @param holdEnabler
     */

    public HoldEnablerBuilder(HoldEnablerMiter holdEnabler) {
        super(holdEnabler);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return HoldEnablerBuilder
     */

    @Override
    protected HoldEnablerBuilder self() {
        return (this);
    }
}       


