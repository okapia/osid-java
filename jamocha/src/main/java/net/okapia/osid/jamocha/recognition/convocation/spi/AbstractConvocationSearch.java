//
// AbstractConvocationSearch.java
//
//     A template for making a Convocation Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing convocation searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractConvocationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.recognition.ConvocationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.recognition.records.ConvocationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.recognition.ConvocationSearchOrder convocationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of convocations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  convocationIds list of convocations
     *  @throws org.osid.NullArgumentException
     *          <code>convocationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongConvocations(org.osid.id.IdList convocationIds) {
        while (convocationIds.hasNext()) {
            try {
                this.ids.add(convocationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongConvocations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of convocation Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getConvocationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  convocationSearchOrder convocation search order 
     *  @throws org.osid.NullArgumentException
     *          <code>convocationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>convocationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderConvocationResults(org.osid.recognition.ConvocationSearchOrder convocationSearchOrder) {
	this.convocationSearchOrder = convocationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.recognition.ConvocationSearchOrder getConvocationSearchOrder() {
	return (this.convocationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given convocation search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a convocation implementing the requested record.
     *
     *  @param convocationSearchRecordType a convocation search record
     *         type
     *  @return the convocation search record
     *  @throws org.osid.NullArgumentException
     *          <code>convocationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(convocationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationSearchRecord getConvocationSearchRecord(org.osid.type.Type convocationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.recognition.records.ConvocationSearchRecord record : this.records) {
            if (record.implementsRecordType(convocationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this convocation search. 
     *
     *  @param convocationSearchRecord convocation search record
     *  @param convocationSearchRecordType convocation search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConvocationSearchRecord(org.osid.recognition.records.ConvocationSearchRecord convocationSearchRecord, 
                                           org.osid.type.Type convocationSearchRecordType) {

        addRecordType(convocationSearchRecordType);
        this.records.add(convocationSearchRecord);        
        return;
    }
}
