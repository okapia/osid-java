//
// MutableIndexedMapProxyCommentLookupSession
//
//    Implements a Comment lookup service backed by a collection of
//    comments indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.commenting;


/**
 *  Implements a Comment lookup service backed by a collection of
 *  comments. The comments are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some comments may be compatible
 *  with more types than are indicated through these comment
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of comments can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyCommentLookupSession
    extends net.okapia.osid.jamocha.core.commenting.spi.AbstractIndexedMapCommentLookupSession
    implements org.osid.commenting.CommentLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCommentLookupSession} with
     *  no comment.
     *
     *  @param book the book
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code book} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCommentLookupSession(org.osid.commenting.Book book,
                                                       org.osid.proxy.Proxy proxy) {
        setBook(book);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCommentLookupSession} with
     *  a single comment.
     *
     *  @param book the book
     *  @param  comment an comment
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code book},
     *          {@code comment}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCommentLookupSession(org.osid.commenting.Book book,
                                                       org.osid.commenting.Comment comment, org.osid.proxy.Proxy proxy) {

        this(book, proxy);
        putComment(comment);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCommentLookupSession} using
     *  an array of comments.
     *
     *  @param book the book
     *  @param  comments an array of comments
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code book},
     *          {@code comments}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCommentLookupSession(org.osid.commenting.Book book,
                                                       org.osid.commenting.Comment[] comments, org.osid.proxy.Proxy proxy) {

        this(book, proxy);
        putComments(comments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCommentLookupSession} using
     *  a collection of comments.
     *
     *  @param book the book
     *  @param  comments a collection of comments
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code book},
     *          {@code comments}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCommentLookupSession(org.osid.commenting.Book book,
                                                       java.util.Collection<? extends org.osid.commenting.Comment> comments,
                                                       org.osid.proxy.Proxy proxy) {
        this(book, proxy);
        putComments(comments);
        return;
    }

    
    /**
     *  Makes a {@code Comment} available in this session.
     *
     *  @param  comment a comment
     *  @throws org.osid.NullArgumentException {@code comment{@code 
     *          is {@code null}
     */

    @Override
    public void putComment(org.osid.commenting.Comment comment) {
        super.putComment(comment);
        return;
    }


    /**
     *  Makes an array of comments available in this session.
     *
     *  @param  comments an array of comments
     *  @throws org.osid.NullArgumentException {@code comments{@code 
     *          is {@code null}
     */

    @Override
    public void putComments(org.osid.commenting.Comment[] comments) {
        super.putComments(comments);
        return;
    }


    /**
     *  Makes collection of comments available in this session.
     *
     *  @param  comments a collection of comments
     *  @throws org.osid.NullArgumentException {@code comment{@code 
     *          is {@code null}
     */

    @Override
    public void putComments(java.util.Collection<? extends org.osid.commenting.Comment> comments) {
        super.putComments(comments);
        return;
    }


    /**
     *  Removes a Comment from this session.
     *
     *  @param commentId the {@code Id} of the comment
     *  @throws org.osid.NullArgumentException {@code commentId{@code  is
     *          {@code null}
     */

    @Override
    public void removeComment(org.osid.id.Id commentId) {
        super.removeComment(commentId);
        return;
    }    
}
