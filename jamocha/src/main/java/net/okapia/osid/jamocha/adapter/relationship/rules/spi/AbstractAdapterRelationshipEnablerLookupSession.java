//
// AbstractAdapterRelationshipEnablerLookupSession.java
//
//    A RelationshipEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.relationship.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RelationshipEnabler lookup session adapter.
 */

public abstract class AbstractAdapterRelationshipEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {

    private final org.osid.relationship.rules.RelationshipEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRelationshipEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRelationshipEnablerLookupSession(org.osid.relationship.rules.RelationshipEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Family/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Family Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.session.getFamilyId());
    }


    /**
     *  Gets the {@code Family} associated with this session.
     *
     *  @return the {@code Family} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFamily());
    }


    /**
     *  Tests if this user can perform {@code RelationshipEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRelationshipEnablers() {
        return (this.session.canLookupRelationshipEnablers());
    }


    /**
     *  A complete view of the {@code RelationshipEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelationshipEnablerView() {
        this.session.useComparativeRelationshipEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code RelationshipEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelationshipEnablerView() {
        this.session.usePlenaryRelationshipEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationship enablers in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.session.useFederatedFamilyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        this.session.useIsolatedFamilyView();
        return;
    }
    

    /**
     *  Only active relationship enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRelationshipEnablerView() {
        this.session.useActiveRelationshipEnablerView();
        return;
    }


    /**
     *  Active and inactive relationship enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRelationshipEnablerView() {
        this.session.useAnyStatusRelationshipEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code RelationshipEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code RelationshipEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code RelationshipEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @param relationshipEnablerId {@code Id} of the {@code RelationshipEnabler}
     *  @return the relationship enabler
     *  @throws org.osid.NotFoundException {@code relationshipEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code relationshipEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnabler getRelationshipEnabler(org.osid.id.Id relationshipEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipEnabler(relationshipEnablerId));
    }


    /**
     *  Gets a {@code RelationshipEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relationshipEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code RelationshipEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @param  relationshipEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RelationshipEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByIds(org.osid.id.IdList relationshipEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipEnablersByIds(relationshipEnablerIds));
    }


    /**
     *  Gets a {@code RelationshipEnablerList} corresponding to the given
     *  relationship enabler genus {@code Type} which does not include
     *  relationship enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the returned list
     *  may contain only those relationship enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @param  relationshipEnablerGenusType a relationshipEnabler genus type 
     *  @return the returned {@code RelationshipEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByGenusType(org.osid.type.Type relationshipEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipEnablersByGenusType(relationshipEnablerGenusType));
    }


    /**
     *  Gets a {@code RelationshipEnablerList} corresponding to the given
     *  relationship enabler genus {@code Type} and include any additional
     *  relationship enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the returned list
     *  may contain only those relationship enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @param  relationshipEnablerGenusType a relationshipEnabler genus type 
     *  @return the returned {@code RelationshipEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByParentGenusType(org.osid.type.Type relationshipEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipEnablersByParentGenusType(relationshipEnablerGenusType));
    }


    /**
     *  Gets a {@code RelationshipEnablerList} containing the given
     *  relationship enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the returned list
     *  may contain only those relationship enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @param  relationshipEnablerRecordType a relationshipEnabler record type 
     *  @return the returned {@code RelationshipEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByRecordType(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipEnablersByRecordType(relationshipEnablerRecordType));
    }


    /**
     *  Gets a {@code RelationshipEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the returned list
     *  may contain only those relationship enablers that are accessible
     *  through this session.
     *  
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RelationshipEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code RelationshipEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the returned list
     *  may contain only those relationship enablers that are accessible
     *  through this session.
     *
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code RelationshipEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getRelationshipEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code RelationshipEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the returned list
     *  may contain only those relationship enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relationship enablers are returned that are currently
     *  active. In any status mode, active and inactive relationship enablers
     *  are returned.
     *
     *  @return a list of {@code RelationshipEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipEnablers());
    }
}
