//
// AbstractProductSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProductSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ordering.ProductSearchResults {

    private org.osid.ordering.ProductList products;
    private final org.osid.ordering.ProductQueryInspector inspector;
    private final java.util.Collection<org.osid.ordering.records.ProductSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProductSearchResults.
     *
     *  @param products the result set
     *  @param productQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>products</code>
     *          or <code>productQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProductSearchResults(org.osid.ordering.ProductList products,
                                            org.osid.ordering.ProductQueryInspector productQueryInspector) {
        nullarg(products, "products");
        nullarg(productQueryInspector, "product query inspectpr");

        this.products = products;
        this.inspector = productQueryInspector;

        return;
    }


    /**
     *  Gets the product list resulting from a search.
     *
     *  @return a product list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProducts() {
        if (this.products == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ordering.ProductList products = this.products;
        this.products = null;
	return (products);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ordering.ProductQueryInspector getProductQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  product search record <code> Type. </code> This method must
     *  be used to retrieve a product implementing the requested
     *  record.
     *
     *  @param productSearchRecordType a product search 
     *         record type 
     *  @return the product search
     *  @throws org.osid.NullArgumentException
     *          <code>productSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(productSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductSearchResultsRecord getProductSearchResultsRecord(org.osid.type.Type productSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ordering.records.ProductSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(productSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(productSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record product search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProductRecord(org.osid.ordering.records.ProductSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "product record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
