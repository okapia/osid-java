//
// MutableDeviceEnablerList.java
//
//     Implements a DeviceEnablerList. This list allows DeviceEnablers to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.deviceenabler;


/**
 *  <p>Implements a DeviceEnablerList. This list allows DeviceEnablers to be
 *  added after this deviceEnabler has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this deviceEnabler must
 *  invoke <code>eol()</code> when there are no more deviceEnablers to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>DeviceEnablerList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more deviceEnablers are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more deviceEnablers to be added.</p>
 */

public final class MutableDeviceEnablerList
    extends net.okapia.osid.jamocha.control.rules.deviceenabler.spi.AbstractMutableDeviceEnablerList
    implements org.osid.control.rules.DeviceEnablerList {


    /**
     *  Creates a new empty <code>MutableDeviceEnablerList</code>.
     */

    public MutableDeviceEnablerList() {
        super();
    }


    /**
     *  Creates a new <code>MutableDeviceEnablerList</code>.
     *
     *  @param deviceEnabler a <code>DeviceEnabler</code>
     *  @throws org.osid.NullArgumentException <code>deviceEnabler</code>
     *          is <code>null</code>
     */

    public MutableDeviceEnablerList(org.osid.control.rules.DeviceEnabler deviceEnabler) {
        super(deviceEnabler);
        return;
    }


    /**
     *  Creates a new <code>MutableDeviceEnablerList</code>.
     *
     *  @param array an array of deviceenablers
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableDeviceEnablerList(org.osid.control.rules.DeviceEnabler[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableDeviceEnablerList</code>.
     *
     *  @param collection a java.util.Collection of deviceenablers
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableDeviceEnablerList(java.util.Collection<org.osid.control.rules.DeviceEnabler> collection) {
        super(collection);
        return;
    }
}
