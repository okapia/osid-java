//
// AbstractAdapterTermLookupSession.java
//
//    A Term lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Term lookup session adapter.
 */

public abstract class AbstractAdapterTermLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.TermLookupSession {

    private final org.osid.course.TermLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTermLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTermLookupSession(org.osid.course.TermLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Term} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupTerms() {
        return (this.session.canLookupTerms());
    }


    /**
     *  A complete view of the {@code Term} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTermView() {
        this.session.useComparativeTermView();
        return;
    }


    /**
     *  A complete view of the {@code Term} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTermView() {
        this.session.usePlenaryTermView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include terms in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the {@code Term} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Term} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Term} and
     *  retained for compatibility.
     *
     *  @param termId {@code Id} of the {@code Term}
     *  @return the term
     *  @throws org.osid.NotFoundException {@code termId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code termId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Term getTerm(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTerm(termId));
    }


    /**
     *  Gets a {@code TermList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  terms specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Terms} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  termIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Term} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code termIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByIds(org.osid.id.IdList termIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTermsByIds(termIds));
    }


    /**
     *  Gets a {@code TermList} corresponding to the given
     *  term genus {@code Type} which does not include
     *  terms of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  termGenusType a term genus type 
     *  @return the returned {@code Term} list
     *  @throws org.osid.NullArgumentException
     *          {@code termGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByGenusType(org.osid.type.Type termGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTermsByGenusType(termGenusType));
    }


    /**
     *  Gets a {@code TermList} corresponding to the given
     *  term genus {@code Type} and include any additional
     *  terms with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  termGenusType a term genus type 
     *  @return the returned {@code Term} list
     *  @throws org.osid.NullArgumentException
     *          {@code termGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByParentGenusType(org.osid.type.Type termGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTermsByParentGenusType(termGenusType));
    }


    /**
     *  Gets a {@code TermList} containing the given
     *  term record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  termRecordType a term record type 
     *  @return the returned {@code Term} list
     *  @throws org.osid.NullArgumentException
     *          {@code termRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByRecordType(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTermsByRecordType(termRecordType));
    }


    /**
     *  Gets a {@code TermList} where to the given {@code DateTime}
     *  falls within the classes date range inclusive. Terms
     *  containing the given date are matched. In plenary mode, the
     *  returned list contains all of the terms specified in the
     *  {@code Id} list, in the order of the list, including
     *  duplicates, or an error results if an {@code Id} in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Terms} may be omitted from the list
     *  including returning a unique set.
     *
     *  @param  datetime a date 
     *  @return the returned {@code Term} list 
     *  @throws org.osid.NullArgumentException {@code datetime} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByClassesDate(org.osid.calendaring.DateTime datetime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTermsByClassesDate(datetime));
    }


    /**
     *  Gets all {@code Terms}. 
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Terms} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTerms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTerms());
    }
}
