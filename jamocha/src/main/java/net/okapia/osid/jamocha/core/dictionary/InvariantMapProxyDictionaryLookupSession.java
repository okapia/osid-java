//
// InvariantMapProxyDictionaryLookupSession
//
//    Implements a Dictionary lookup service backed by a fixed
//    collection of dictionaries. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  Implements a Dictionary lookup service backed by a fixed
 *  collection of dictionaries. The dictionaries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyDictionaryLookupSession
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractMapDictionaryLookupSession
    implements org.osid.dictionary.DictionaryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDictionaryLookupSession} with no
     *  dictionaries.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyDictionaryLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDictionaryLookupSession} with a
     *  single dictionary.
     *
     *  @param dictionary a single dictionary
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionary} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDictionaryLookupSession(org.osid.dictionary.Dictionary dictionary, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDictionary(dictionary);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDictionaryLookupSession} using
     *  an array of dictionaries.
     *
     *  @param dictionaries an array of dictionaries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionaries} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDictionaryLookupSession(org.osid.dictionary.Dictionary[] dictionaries, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDictionaries(dictionaries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDictionaryLookupSession} using a
     *  collection of dictionaries.
     *
     *  @param dictionaries a collection of dictionaries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionaries} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDictionaryLookupSession(java.util.Collection<? extends org.osid.dictionary.Dictionary> dictionaries,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDictionaries(dictionaries);
        return;
    }
}
