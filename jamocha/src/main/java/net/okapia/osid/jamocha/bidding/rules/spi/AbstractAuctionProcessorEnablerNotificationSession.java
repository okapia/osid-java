//
// AbstractAuctionProcessorEnablerNotificationSession.java
//
//     A template for making AuctionProcessorEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code AuctionProcessorEnabler} objects. This
 *  session is intended for consumers needing to synchronize their
 *  state with this service without the use of polling. Notifications
 *  are cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code AuctionProcessorEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for auction processor enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractAuctionProcessorEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.rules.AuctionProcessorEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();


    /**
     *  Gets the {@code AuctionHouse} {@code Id} associated with
     *  this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }

    
    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the {@code AuctionHouse}.
     *
     *  @param auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException {@code auctionHouse}
     *          is {@code null}
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  AuctionProcessorEnabler} notifications.  A return of true does
     *  not guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForAuctionProcessorEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeAuctionProcessorEnablerNotification() </code>.
     */

    @OSID @Override
    public void reliableAuctionProcessorEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableAuctionProcessorEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge an auction processor enebaler notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeAuctionProcessorEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for auction processor enablers
     *  in auction houses which are children of this auction house in
     *  the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new auction processor
     *  enablers. {@code
     *  AuctionProcessorEnablerReceiver.newAuctionProcessorEnabler()}
     *  is invoked when an new {@code AuctionProcessorEnabler} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewAuctionProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated auction processor
     *  enablers. {@code
     *  AuctionProcessorEnablerReceiver.changedAuctionProcessorEnabler()}
     *  is invoked when an auction processor enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAuctionProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated auction processor
     *  enabler. {@code
     *  AuctionProcessorEnablerReceiver.changedAuctionProcessorEnabler()}
     *  is invoked when the specified auction processor enabler is
     *  changed.
     *
     *  @param auctionProcessorEnablerId the {@code Id} of the {@code AuctionProcessorEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code auctionProcessorEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted auction processor
     *  enablers. {@code
     *  AuctionProcessorEnablerReceiver.deletedAuctionProcessorEnabler()}
     *  is invoked when an auction processor enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAuctionProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted auction processor
     *  enabler. {@code
     *  AuctionProcessorEnablerReceiver.deletedAuctionProcessorEnabler()}
     *  is invoked when the specified auction processor enabler is
     *  deleted.
     *
     *  @param auctionProcessorEnablerId the {@code Id} of the
     *          {@code AuctionProcessorEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code auctionProcessorEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
