//
// MutableIndexedMapProxyPublisherLookupSession
//
//    Implements a Publisher lookup service backed by a collection of
//    publishers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription;


/**
 *  Implements a Publisher lookup service backed by a collection of
 *  publishers. The publishers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some publishers may be compatible
 *  with more types than are indicated through these publisher
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of publishers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyPublisherLookupSession
    extends net.okapia.osid.jamocha.core.subscription.spi.AbstractIndexedMapPublisherLookupSession
    implements org.osid.subscription.PublisherLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPublisherLookupSession} with
     *  no publisher.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyPublisherLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPublisherLookupSession} with
     *  a single publisher.
     *
     *  @param  publisher an publisher
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyPublisherLookupSession(org.osid.subscription.Publisher publisher, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPublisher(publisher);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPublisherLookupSession} using
     *  an array of publishers.
     *
     *  @param  publishers an array of publishers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publishers} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyPublisherLookupSession(org.osid.subscription.Publisher[] publishers, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPublishers(publishers);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyPublisherLookupSession} using
     *  a collection of publishers.
     *
     *  @param  publishers a collection of publishers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publishers} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyPublisherLookupSession(java.util.Collection<? extends org.osid.subscription.Publisher> publishers,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPublishers(publishers);
        return;
    }

    
    /**
     *  Makes a {@code Publisher} available in this session.
     *
     *  @param  publisher a publisher
     *  @throws org.osid.NullArgumentException {@code publisher{@code 
     *          is {@code null}
     */

    @Override
    public void putPublisher(org.osid.subscription.Publisher publisher) {
        super.putPublisher(publisher);
        return;
    }


    /**
     *  Makes an array of publishers available in this session.
     *
     *  @param  publishers an array of publishers
     *  @throws org.osid.NullArgumentException {@code publishers{@code 
     *          is {@code null}
     */

    @Override
    public void putPublishers(org.osid.subscription.Publisher[] publishers) {
        super.putPublishers(publishers);
        return;
    }


    /**
     *  Makes collection of publishers available in this session.
     *
     *  @param  publishers a collection of publishers
     *  @throws org.osid.NullArgumentException {@code publisher{@code 
     *          is {@code null}
     */

    @Override
    public void putPublishers(java.util.Collection<? extends org.osid.subscription.Publisher> publishers) {
        super.putPublishers(publishers);
        return;
    }


    /**
     *  Removes a Publisher from this session.
     *
     *  @param publisherId the {@code Id} of the publisher
     *  @throws org.osid.NullArgumentException {@code publisherId{@code  is
     *          {@code null}
     */

    @Override
    public void removePublisher(org.osid.id.Id publisherId) {
        super.removePublisher(publisherId);
        return;
    }    
}
