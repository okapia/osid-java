//
// ProgramOfferingElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.programoffering.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProgramOfferingElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ProgramOfferingElement Id.
     *
     *  @return the program offering element Id
     */

    public static org.osid.id.Id getProgramOfferingEntityId() {
        return (makeEntityId("osid.course.program.ProgramOffering"));
    }


    /**
     *  Gets the ProgramId element Id.
     *
     *  @return the ProgramId element Id
     */

    public static org.osid.id.Id getProgramId() {
        return (makeElementId("osid.course.program.programoffering.ProgramId"));
    }


    /**
     *  Gets the Program element Id.
     *
     *  @return the Program element Id
     */

    public static org.osid.id.Id getProgram() {
        return (makeElementId("osid.course.program.programoffering.Program"));
    }


    /**
     *  Gets the TermId element Id.
     *
     *  @return the TermId element Id
     */

    public static org.osid.id.Id getTermId() {
        return (makeElementId("osid.course.program.programoffering.TermId"));
    }


    /**
     *  Gets the Term element Id.
     *
     *  @return the Term element Id
     */

    public static org.osid.id.Id getTerm() {
        return (makeElementId("osid.course.program.programoffering.Term"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.course.program.programoffering.Title"));
    }


    /**
     *  Gets the Number element Id.
     *
     *  @return the Number element Id
     */

    public static org.osid.id.Id getNumber() {
        return (makeElementId("osid.course.program.programoffering.Number"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.course.program.programoffering.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.course.program.programoffering.Sponsors"));
    }


    /**
     *  Gets the CompletionRequirementsInfo element Id.
     *
     *  @return the CompletionRequirementsInfo element Id
     */

    public static org.osid.id.Id getCompletionRequirementsInfo() {
        return (makeElementId("osid.course.program.programoffering.CompletionRequirementsInfo"));
    }


    /**
     *  Gets the CompletionRequirementIds element Id.
     *
     *  @return the CompletionRequirementIds element Id
     */

    public static org.osid.id.Id getCompletionRequirementIds() {
        return (makeElementId("osid.course.program.programoffering.CompletionRequirementIds"));
    }


    /**
     *  Gets the CompletionRequirements element Id.
     *
     *  @return the CompletionRequirements element Id
     */

    public static org.osid.id.Id getCompletionRequirements() {
        return (makeElementId("osid.course.program.programoffering.CompletionRequirements"));
    }


    /**
     *  Gets the CredentialIds element Id.
     *
     *  @return the CredentialIds element Id
     */

    public static org.osid.id.Id getCredentialIds() {
        return (makeElementId("osid.course.program.programoffering.CredentialIds"));
    }


    /**
     *  Gets the Credentials element Id.
     *
     *  @return the Credentials element Id
     */

    public static org.osid.id.Id getCredentials() {
        return (makeElementId("osid.course.program.programoffering.Credentials"));
    }


    /**
     *  Gets the MinimumSeats element Id.
     *
     *  @return the MinimumSeats element Id
     */

    public static org.osid.id.Id getMinimumSeats() {
        return (makeElementId("osid.course.program.programoffering.MinimumSeats"));
    }


    /**
     *  Gets the MaximumSeats element Id.
     *
     *  @return the MaximumSeats element Id
     */

    public static org.osid.id.Id getMaximumSeats() {
        return (makeElementId("osid.course.program.programoffering.MaximumSeats"));
    }


    /**
     *  Gets the URL element Id.
     *
     *  @return the URL element Id
     */

    public static org.osid.id.Id getURL() {
        return (makeElementId("osid.course.program.programoffering.URL"));
    }


    /**
     *  Gets the InstructorId element Id.
     *
     *  @return the InstructorId element Id
     */

    public static org.osid.id.Id getInstructorId() {
        return (makeQueryElementId("osid.course.program.programoffering.InstructorId"));
    }


    /**
     *  Gets the Instructor element Id.
     *
     *  @return the Instructor element Id
     */

    public static org.osid.id.Id getInstructor() {
        return (makeQueryElementId("osid.course.program.programoffering.Instructor"));
    }


    /**
     *  Gets the RequiresRegistration element Id.
     *
     *  @return the RequiresRegistration element Id
     */

    public static org.osid.id.Id getRequiresRegistration() {
        return (makeElementId("osid.course.program.programoffering.RequiresRegistration"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.program.programoffering.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.program.programoffering.CourseCatalog"));
    }
}
