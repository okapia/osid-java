//
// AbstractFiscalPeriod.java
//
//     Defines a FiscalPeriod builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.fiscalperiod.spi;


/**
 *  Defines a <code>FiscalPeriod</code> builder.
 */

public abstract class AbstractFiscalPeriodBuilder<T extends AbstractFiscalPeriodBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.financials.fiscalperiod.FiscalPeriodMiter fiscalPeriod;


    /**
     *  Constructs a new <code>AbstractFiscalPeriodBuilder</code>.
     *
     *  @param fiscalPeriod the fiscal period to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractFiscalPeriodBuilder(net.okapia.osid.jamocha.builder.financials.fiscalperiod.FiscalPeriodMiter fiscalPeriod) {
        super(fiscalPeriod);
        this.fiscalPeriod = fiscalPeriod;
        return;
    }


    /**
     *  Builds the fiscal period.
     *
     *  @return the new fiscal period
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.financials.FiscalPeriod build() {
        (new net.okapia.osid.jamocha.builder.validator.financials.fiscalperiod.FiscalPeriodValidator(getValidations())).validate(this.fiscalPeriod);
        return (new net.okapia.osid.jamocha.builder.financials.fiscalperiod.ImmutableFiscalPeriod(this.fiscalPeriod));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the fiscal period miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.financials.fiscalperiod.FiscalPeriodMiter getMiter() {
        return (this.fiscalPeriod);
    }


    /**
     *  Sets the display label.
     *
     *  @param displayLabel a display label
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>displayLabel</code> is <code>null</code>
     */

    public T displayLabel(org.osid.locale.DisplayText displayLabel) {
        getMiter().setDisplayLabel(displayLabel);
        return (self());
    }


    /**
     *  Sets the fiscal year.
     *
     *  @param year a fiscal year
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>year</code> is
     *          <code>null</code>
     */

    public T fiscalYear(long year) {
        getMiter().setFiscalYear(year);
        return (self());
    }


    /**
     *  Sets the start date.
     *
     *  @param date a start date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T startDate(org.osid.calendaring.DateTime date) {
        getMiter().setStartDate(date);
        return (self());
    }


    /**
     *  Sets the end date.
     *
     *  @param date an end date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T endDate(org.osid.calendaring.DateTime date) {
        getMiter().setEndDate(date);
        return (self());
    }


    /**
     *  Sets the budget deadline.
     *
     *  @param date a budget deadline
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T budgetDeadline(org.osid.calendaring.DateTime date) {
        getMiter().setBudgetDeadline(date);
        return (self());
    }


    /**
     *  Sets the posting deadline.
     *
     *  @param date a posting deadline
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T postingDeadline(org.osid.calendaring.DateTime date) {
        getMiter().setPostingDeadline(date);
        return (self());
    }


    /**
     *  Sets the closing.
     *
     *  @param date a closing
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T closing(org.osid.calendaring.DateTime date) {
        getMiter().setClosing(date);
        return (self());
    }


    /**
     *  Adds a FiscalPeriod record.
     *
     *  @param record a fiscal period record
     *  @param recordType the type of fiscal period record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.financials.records.FiscalPeriodRecord record, org.osid.type.Type recordType) {
        getMiter().addFiscalPeriodRecord(record, recordType);
        return (self());
    }
}       


