//
// AbstractAdapterAuctionLookupSession.java
//
//    An Auction lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Auction lookup session adapter.
 */

public abstract class AbstractAdapterAuctionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.AuctionLookupSession {

    private final org.osid.bidding.AuctionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuctionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionLookupSession(org.osid.bidding.AuctionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AuctionHouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform {@code Auction} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAuctions() {
        return (this.session.canLookupAuctions());
    }


    /**
     *  A complete view of the {@code Auction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionView() {
        this.session.useComparativeAuctionView();
        return;
    }


    /**
     *  A complete view of the {@code Auction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionView() {
        this.session.usePlenaryAuctionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auctions in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auctions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionView() {
        this.session.useActiveAuctionView();
        return;
    }


    /**
     *  Active and inactive auctions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionView() {
        this.session.useAnyStatusAuctionView();
        return;
    }
    
     
    /**
     *  Gets the {@code Auction} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Auction} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Auction} and
     *  retained for compatibility.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param auctionId {@code Id} of the {@code Auction}
     *  @return the auction
     *  @throws org.osid.NotFoundException {@code auctionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code auctionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Auction getAuction(org.osid.id.Id auctionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuction(auctionId));
    }


    /**
     *  Gets an {@code AuctionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Auctions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Auction} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code auctionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByIds(org.osid.id.IdList auctionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsByIds(auctionIds));
    }


    /**
     *  Gets an {@code AuctionList} corresponding to the given
     *  auction genus {@code Type} which does not include
     *  auctions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned {@code Auction} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsByGenusType(auctionGenusType));
    }


    /**
     *  Gets an {@code AuctionList} corresponding to the given
     *  auction genus {@code Type} and include any additional
     *  auctions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned {@code Auction} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByParentGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsByParentGenusType(auctionGenusType));
    }


    /**
     *  Gets an {@code AuctionList} containing the given
     *  auction record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return the returned {@code Auction} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByRecordType(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsByRecordType(auctionRecordType));
    }


    /**
     *  Gets an {@code AuctionList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Auction} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsByProvider(resourceId));
    }


    /**
     *  Gets an {@code AuctionList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible
     *  through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Auction} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsOnDate(from, to));
    }
        

    /**
     *  Gets a list of auctions for an item. {@code} 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemId a resource {@code Id} 
     *  @return the returned {@code Auction} list 
     *  @throws org.osid.NullArgumentException {@code itemId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItem(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsByItem(itemId));
    }


    /**
     *  Gets a list of auctions for an item genus type. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned {@code Auction} list 
     *  @throws org.osid.NullArgumentException {@code itemGenusType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionsByItemGenusType(itemGenusType));
    }


    /**
     *  Gets an {@code AuctionList} for an item genus type and 
     *  effective during the entire given date range inclusive but not 
     *  confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemGenusType an item genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Auction} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code itemGenusType,
     *         from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusTypeOnDate(org.osid.type.Type itemGenusType, 
                                                                         org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionsByItemGenusTypeOnDate(itemGenusType, from, to));
    }

               
    /**
     *  Gets all {@code Auctions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @return a list of {@code Auctions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctions());
    }
}
