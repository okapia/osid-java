//
// AbstractImmutableSupersedingEventEnabler.java
//
//     Wraps a mutable SupersedingEventEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.rules.supersedingeventenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>SupersedingEventEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized SupersedingEventEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying supersedingEventEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableSupersedingEventEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.calendaring.rules.SupersedingEventEnabler {

    private final org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableSupersedingEventEnabler</code>.
     *
     *  @param supersedingEventEnabler the superseding event enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSupersedingEventEnabler(org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler) {
        super(supersedingEventEnabler);
        this.supersedingEventEnabler = supersedingEventEnabler;
        return;
    }


    /**
     *  Gets the superseding event enabler record corresponding to the given 
     *  <code> SupersedingEventEnabler </code> record <code> Type. </code> 
     *  This method is used to retrieve an object implementing the requested 
     *  record. The <code> supersedingEventEnablerRecordType </code> may be 
     *  the <code> Type </code> returned in <code> getRecordTypes() </code> or 
     *  any of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(supersedingEventEnablerRecordType) </code> is <code> 
     *  true </code> . 
     *
     *  @param  supersedingEventEnablerRecordType the type of superseding 
     *          event enabler record to retrieve 
     *  @return the superseding event enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerRecordType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(supersedingEventEnablerRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerRecord getSupersedingEventEnablerRecord(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.supersedingEventEnabler.getSupersedingEventEnablerRecord(supersedingEventEnablerRecordType));
    }
}

