//
// MutableIndexedMapProxyBuildingLookupSession
//
//    Implements a Building lookup service backed by a collection of
//    buildings indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room;


/**
 *  Implements a Building lookup service backed by a collection of
 *  buildings. The buildings are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some buildings may be compatible
 *  with more types than are indicated through these building
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of buildings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyBuildingLookupSession
    extends net.okapia.osid.jamocha.core.room.spi.AbstractIndexedMapBuildingLookupSession
    implements org.osid.room.BuildingLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBuildingLookupSession} with
     *  no building.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBuildingLookupSession(org.osid.room.Campus campus,
                                                       org.osid.proxy.Proxy proxy) {
        setCampus(campus);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBuildingLookupSession} with
     *  a single building.
     *
     *  @param campus the campus
     *  @param  building an building
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code building}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBuildingLookupSession(org.osid.room.Campus campus,
                                                       org.osid.room.Building building, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putBuilding(building);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBuildingLookupSession} using
     *  an array of buildings.
     *
     *  @param campus the campus
     *  @param  buildings an array of buildings
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code buildings}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBuildingLookupSession(org.osid.room.Campus campus,
                                                       org.osid.room.Building[] buildings, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putBuildings(buildings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBuildingLookupSession} using
     *  a collection of buildings.
     *
     *  @param campus the campus
     *  @param  buildings a collection of buildings
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code buildings}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBuildingLookupSession(org.osid.room.Campus campus,
                                                       java.util.Collection<? extends org.osid.room.Building> buildings,
                                                       org.osid.proxy.Proxy proxy) {
        this(campus, proxy);
        putBuildings(buildings);
        return;
    }

    
    /**
     *  Makes a {@code Building} available in this session.
     *
     *  @param  building a building
     *  @throws org.osid.NullArgumentException {@code building{@code 
     *          is {@code null}
     */

    @Override
    public void putBuilding(org.osid.room.Building building) {
        super.putBuilding(building);
        return;
    }


    /**
     *  Makes an array of buildings available in this session.
     *
     *  @param  buildings an array of buildings
     *  @throws org.osid.NullArgumentException {@code buildings{@code 
     *          is {@code null}
     */

    @Override
    public void putBuildings(org.osid.room.Building[] buildings) {
        super.putBuildings(buildings);
        return;
    }


    /**
     *  Makes collection of buildings available in this session.
     *
     *  @param  buildings a collection of buildings
     *  @throws org.osid.NullArgumentException {@code building{@code 
     *          is {@code null}
     */

    @Override
    public void putBuildings(java.util.Collection<? extends org.osid.room.Building> buildings) {
        super.putBuildings(buildings);
        return;
    }


    /**
     *  Removes a Building from this session.
     *
     *  @param buildingId the {@code Id} of the building
     *  @throws org.osid.NullArgumentException {@code buildingId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBuilding(org.osid.id.Id buildingId) {
        super.removeBuilding(buildingId);
        return;
    }    
}
