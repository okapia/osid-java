//
// MutableIndexedMapProxyInputLookupSession
//
//    Implements an Input lookup service backed by a collection of
//    inputs indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements an Input lookup service backed by a collection of
 *  inputs. The inputs are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some inputs may be compatible
 *  with more types than are indicated through these input
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of inputs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyInputLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractIndexedMapInputLookupSession
    implements org.osid.control.InputLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputLookupSession} with
     *  no input.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputLookupSession(org.osid.control.System system,
                                                       org.osid.proxy.Proxy proxy) {
        setSystem(system);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputLookupSession} with
     *  a single input.
     *
     *  @param system the system
     *  @param  input an input
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code input}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputLookupSession(org.osid.control.System system,
                                                       org.osid.control.Input input, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putInput(input);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputLookupSession} using
     *  an array of inputs.
     *
     *  @param system the system
     *  @param  inputs an array of inputs
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code inputs}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputLookupSession(org.osid.control.System system,
                                                       org.osid.control.Input[] inputs, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putInputs(inputs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputLookupSession} using
     *  a collection of inputs.
     *
     *  @param system the system
     *  @param  inputs a collection of inputs
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code inputs}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputLookupSession(org.osid.control.System system,
                                                       java.util.Collection<? extends org.osid.control.Input> inputs,
                                                       org.osid.proxy.Proxy proxy) {
        this(system, proxy);
        putInputs(inputs);
        return;
    }

    
    /**
     *  Makes an {@code Input} available in this session.
     *
     *  @param  input an input
     *  @throws org.osid.NullArgumentException {@code input{@code 
     *          is {@code null}
     */

    @Override
    public void putInput(org.osid.control.Input input) {
        super.putInput(input);
        return;
    }


    /**
     *  Makes an array of inputs available in this session.
     *
     *  @param  inputs an array of inputs
     *  @throws org.osid.NullArgumentException {@code inputs{@code 
     *          is {@code null}
     */

    @Override
    public void putInputs(org.osid.control.Input[] inputs) {
        super.putInputs(inputs);
        return;
    }


    /**
     *  Makes collection of inputs available in this session.
     *
     *  @param  inputs a collection of inputs
     *  @throws org.osid.NullArgumentException {@code input{@code 
     *          is {@code null}
     */

    @Override
    public void putInputs(java.util.Collection<? extends org.osid.control.Input> inputs) {
        super.putInputs(inputs);
        return;
    }


    /**
     *  Removes an Input from this session.
     *
     *  @param inputId the {@code Id} of the input
     *  @throws org.osid.NullArgumentException {@code inputId{@code  is
     *          {@code null}
     */

    @Override
    public void removeInput(org.osid.id.Id inputId) {
        super.removeInput(inputId);
        return;
    }    
}
