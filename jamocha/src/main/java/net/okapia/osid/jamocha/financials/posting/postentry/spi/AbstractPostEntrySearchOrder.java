//
// AbstractPostEntrySearchOdrer.java
//
//     Defines a PostEntrySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PostEntrySearchOrder}.
 */

public abstract class AbstractPostEntrySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.financials.posting.PostEntrySearchOrder {

    private final java.util.Collection<org.osid.financials.posting.records.PostEntrySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by post. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPost(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a post search order is available. 
     *
     *  @return <code> true </code> if a post search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSearchOrder() {
        return (false);
    }


    /**
     *  Gets the post order. 
     *
     *  @return the post search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchOrder getPostSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPostSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by account. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAccount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an account search order is available. 
     *
     *  @return <code> true </code> if an search account order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSearchOrder() {
        return (false);
    }


    /**
     *  Gets the account order. 
     *
     *  @return the account search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchOrder getAccountSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAccountSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by activity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an activity search order is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity search order. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchOrder getActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivitySearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the debit flag. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDebit(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  postEntryRecordType a post entry record type 
     *  @return {@code true} if the postEntryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code postEntryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type postEntryRecordType) {
        for (org.osid.financials.posting.records.PostEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  postEntryRecordType the post entry record type 
     *  @return the post entry search order record
     *  @throws org.osid.NullArgumentException
     *          {@code postEntryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(postEntryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntrySearchOrderRecord getPostEntrySearchOrderRecord(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postEntryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this post entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param postEntryRecord the post entry search odrer record
     *  @param postEntryRecordType post entry record type
     *  @throws org.osid.NullArgumentException
     *          {@code postEntryRecord} or
     *          {@code postEntryRecordTypepostEntry} is
     *          {@code null}
     */
            
    protected void addPostEntryRecord(org.osid.financials.posting.records.PostEntrySearchOrderRecord postEntrySearchOrderRecord, 
                                     org.osid.type.Type postEntryRecordType) {

        addRecordType(postEntryRecordType);
        this.records.add(postEntrySearchOrderRecord);
        
        return;
    }
}
