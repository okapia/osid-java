//
// AbstractCanonicalUnitEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCanonicalUnitEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.rules.CanonicalUnitEnablerSearchResults {

    private org.osid.offering.rules.CanonicalUnitEnablerList canonicalUnitEnablers;
    private final org.osid.offering.rules.CanonicalUnitEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCanonicalUnitEnablerSearchResults.
     *
     *  @param canonicalUnitEnablers the result set
     *  @param canonicalUnitEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablers</code>
     *          or <code>canonicalUnitEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCanonicalUnitEnablerSearchResults(org.osid.offering.rules.CanonicalUnitEnablerList canonicalUnitEnablers,
                                            org.osid.offering.rules.CanonicalUnitEnablerQueryInspector canonicalUnitEnablerQueryInspector) {
        nullarg(canonicalUnitEnablers, "canonical unit enablers");
        nullarg(canonicalUnitEnablerQueryInspector, "canonical unit enabler query inspectpr");

        this.canonicalUnitEnablers = canonicalUnitEnablers;
        this.inspector = canonicalUnitEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the canonical unit enabler list resulting from a search.
     *
     *  @return a canonical unit enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablers() {
        if (this.canonicalUnitEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.rules.CanonicalUnitEnablerList canonicalUnitEnablers = this.canonicalUnitEnablers;
        this.canonicalUnitEnablers = null;
	return (canonicalUnitEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.rules.CanonicalUnitEnablerQueryInspector getCanonicalUnitEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  canonical unit enabler search record <code> Type. </code> This method must
     *  be used to retrieve a canonicalUnitEnabler implementing the requested
     *  record.
     *
     *  @param canonicalUnitEnablerSearchRecordType a canonicalUnitEnabler search 
     *         record type 
     *  @return the canonical unit enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(canonicalUnitEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitEnablerSearchResultsRecord getCanonicalUnitEnablerSearchResultsRecord(org.osid.type.Type canonicalUnitEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.rules.records.CanonicalUnitEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(canonicalUnitEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record canonical unit enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCanonicalUnitEnablerRecord(org.osid.offering.rules.records.CanonicalUnitEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "canonical unit enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
