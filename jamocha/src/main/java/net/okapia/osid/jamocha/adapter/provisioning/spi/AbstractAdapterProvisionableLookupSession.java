//
// AbstractAdapterProvisionableLookupSession.java
//
//    A Provisionable lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Provisionable lookup session adapter.
 */

public abstract class AbstractAdapterProvisionableLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.ProvisionableLookupSession {

    private final org.osid.provisioning.ProvisionableLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProvisionableLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProvisionableLookupSession(org.osid.provisioning.ProvisionableLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code Provisionable} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProvisionables() {
        return (this.session.canLookupProvisionables());
    }


    /**
     *  A complete view of the {@code Provisionable} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProvisionableView() {
        this.session.useComparativeProvisionableView();
        return;
    }


    /**
     *  A complete view of the {@code Provisionable} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProvisionableView() {
        this.session.usePlenaryProvisionableView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisionables in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only provisionables whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProvisionableView() {
        this.session.useEffectiveProvisionableView();
        return;
    }
    

    /**
     *  All provisionables of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProvisionableView() {
        this.session.useAnyEffectiveProvisionableView();
        return;
    }

     
    /**
     *  Gets the {@code Provisionable} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Provisionable} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Provisionable} and
     *  retained for compatibility.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @param provisionableId {@code Id} of the {@code Provisionable}
     *  @return the provisionable
     *  @throws org.osid.NotFoundException {@code provisionableId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code provisionableId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provisionable getProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionable(provisionableId));
    }


    /**
     *  Gets a {@code ProvisionableList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  provisionables specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Provisionables} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @param  provisionableIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Provisionable} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code provisionableIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByIds(org.osid.id.IdList provisionableIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesByIds(provisionableIds));
    }


    /**
     *  Gets a {@code ProvisionableList} corresponding to the given
     *  provisionable genus {@code Type} which does not include
     *  provisionables of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @param  provisionableGenusType a provisionable genus type 
     *  @return the returned {@code Provisionable} list
     *  @throws org.osid.NullArgumentException
     *          {@code provisionableGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByGenusType(org.osid.type.Type provisionableGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesByGenusType(provisionableGenusType));
    }


    /**
     *  Gets a {@code ProvisionableList} corresponding to the given
     *  provisionable genus {@code Type} and include any additional
     *  provisionables with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @param  provisionableGenusType a provisionable genus type 
     *  @return the returned {@code Provisionable} list
     *  @throws org.osid.NullArgumentException
     *          {@code provisionableGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByParentGenusType(org.osid.type.Type provisionableGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesByParentGenusType(provisionableGenusType));
    }


    /**
     *  Gets a {@code ProvisionableList} containing the given
     *  provisionable record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @param  provisionableRecordType a provisionable record type 
     *  @return the returned {@code Provisionable} list
     *  @throws org.osid.NullArgumentException
     *          {@code provisionableRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByRecordType(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesByRecordType(provisionableRecordType));
    }


    /**
     *  Gets a {@code ProvisionableList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible
     *  through this session.
     *  
     *  In active mode, provisionables are returned that are currently
     *  active. In any status mode, active and inactive provisionables
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Provisionable} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesOnDate(org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesOnDate(from, to));
    }
        

    /**
     *  Gets a list of provisionables corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code ProvisionableList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesForResource(resourceId));
    }


    /**
     *  Gets a list of provisionables corresponding to a resource
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProvisionableList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of provisionables corresponding to a pool
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the {@code Id} of the pool
     *  @return the returned {@code ProvisionableList}
     *  @throws org.osid.NullArgumentException {@code poolId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForPool(org.osid.id.Id poolId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesForPool(poolId));
    }


    /**
     *  Gets a list of provisionables corresponding to a pool {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the {@code Id} of the pool
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProvisionableList}
     *  @throws org.osid.NullArgumentException {@code poolId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForPoolOnDate(org.osid.id.Id poolId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesForPoolOnDate(poolId, from, to));
    }


    /**
     *  Gets a list of provisionables corresponding to resource and
     *  pool {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  poolId the {@code Id} of the pool
     *  @return the returned {@code ProvisionableList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code poolId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceAndPool(org.osid.id.Id resourceId,
                                                                                       org.osid.id.Id poolId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesForResourceAndPool(resourceId, poolId));
    }


    /**
     *  Gets a list of provisionables corresponding to resource and
     *  pool {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective. In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the {@code Id} of the pool
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProvisionableList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code poolId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceAndPoolOnDate(org.osid.id.Id resourceId,
                                                                                             org.osid.id.Id poolId,
                                                                                             org.osid.calendaring.DateTime from,
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionablesForResourceAndPoolOnDate(resourceId, poolId, from, to));
    }


    /**
     *  Gets all {@code Provisionables}. 
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Provisionables} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionables()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionables());
    }
}
