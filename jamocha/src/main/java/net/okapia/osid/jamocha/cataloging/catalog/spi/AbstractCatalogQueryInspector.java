//
// AbstractCatalogQueryInspector.java
//
//     A template for making a CatalogQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.catalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for catalogs.
 */

public abstract class AbstractCatalogQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.cataloging.CatalogQueryInspector {

    private final java.util.Collection<org.osid.cataloging.records.CatalogQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getAncestorCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }


    /**
     *  Gets the descendant catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getDescendantCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given catalog query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a catalog implementing the requested record.
     *
     *  @param catalogRecordType a catalog record type
     *  @return the catalog query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.records.CatalogQueryInspectorRecord getCatalogQueryInspectorRecord(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.records.CatalogQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(catalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalog query. 
     *
     *  @param catalogQueryInspectorRecord catalog query inspector
     *         record
     *  @param catalogRecordType catalog record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogQueryInspectorRecord(org.osid.cataloging.records.CatalogQueryInspectorRecord catalogQueryInspectorRecord, 
                                                   org.osid.type.Type catalogRecordType) {

        addRecordType(catalogRecordType);
        nullarg(catalogRecordType, "catalog record type");
        this.records.add(catalogQueryInspectorRecord);        
        return;
    }
}
