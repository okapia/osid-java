//
// AbstractUnknownRouteProgress.java
//
//     Defines an unknown RouteProgress.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.mapping.route.routeprogress.spi;


/**
 *  Defines an unknown <code>RouteProgress</code>.
 */

public abstract class AbstractUnknownRouteProgress
    extends net.okapia.osid.jamocha.mapping.route.routeprogress.spi.AbstractRouteProgress
    implements org.osid.mapping.route.RouteProgress {

    protected static final String OBJECT = "osid.mapping.route.RouteProgress";


    /**
     *  Constructs a new <code>AbstractUnknownRouteProgress</code>.
     */

    public AbstractUnknownRouteProgress() {
        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setResource(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setRoute(new net.okapia.osid.jamocha.nil.mapping.route.route.UnknownRoute());
        setTimeStarted(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        
        setTotalDistanceTraveled(new net.okapia.osid.primordium.mapping.Distance(0));
        setTotalTravelTime(net.okapia.osid.primordium.calendaring.GregorianUTCDuration.unknown());
        setTotalIdleTime(net.okapia.osid.primordium.calendaring.GregorianUTCDuration.unknown());
        setTimeLastMoved(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setRouteSegment(new net.okapia.osid.jamocha.nil.mapping.route.routesegment.UnknownRouteSegment());
        setETAToNextSegment(net.okapia.osid.primordium.calendaring.GregorianUTCDuration.unknown());
        setRouteSegmentTraveled(new net.okapia.osid.primordium.mapping.Distance(0));

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownRouteProgress</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownRouteProgress(boolean optional) {
        this();
        return;
    }
}
