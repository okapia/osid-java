//
// AbstractQueryShipmentLookupSession.java
//
//    An inline adapter that maps a ShipmentLookupSession to
//    a ShipmentQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ShipmentLookupSession to
 *  a ShipmentQuerySession.
 */

public abstract class AbstractQueryShipmentLookupSession
    extends net.okapia.osid.jamocha.inventory.shipment.spi.AbstractShipmentLookupSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {

    private final org.osid.inventory.shipment.ShipmentQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryShipmentLookupSession.
     *
     *  @param querySession the underlying shipment query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryShipmentLookupSession(org.osid.inventory.shipment.ShipmentQuerySession querySession) {
        nullarg(querySession, "shipment query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Warehouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform <code>Shipment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupShipments() {
        return (this.session.canSearchShipments());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include shipments in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the <code>Shipment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Shipment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Shipment</code> and
     *  retained for compatibility.
     *
     *  @param  shipmentId <code>Id</code> of the
     *          <code>Shipment</code>
     *  @return the shipment
     *  @throws org.osid.NotFoundException <code>shipmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>shipmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.Shipment getShipment(org.osid.id.Id shipmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchId(shipmentId, true);
        org.osid.inventory.shipment.ShipmentList shipments = this.session.getShipmentsByQuery(query);
        if (shipments.hasNext()) {
            return (shipments.getNextShipment());
        } 
        
        throw new org.osid.NotFoundException(shipmentId + " not found");
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  shipments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Shipments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  shipmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByIds(org.osid.id.IdList shipmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();

        try (org.osid.id.IdList ids = shipmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  shipment genus <code>Type</code> which does not include
     *  shipments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchGenusType(shipmentGenusType, true);
        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  shipment genus <code>Type</code> and include any additional
     *  shipments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByParentGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchParentGenusType(shipmentGenusType, true);
        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets a <code>ShipmentList</code> containing the given
     *  shipment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentRecordType a shipment record type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByRecordType(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchRecordType(shipmentRecordType, true);
        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets a <code> ShipmentList </code> received between the given
     *  date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> ShipmentList </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets a <code> ShipmentList </code> for to the given stock.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param stockId a stock <code> Id </code>
     *  @return the returned <code> ShipmentList </code> list 
     *  @throws org.osid.NullArgumentException <code> stockId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        if (query.supportsEntryQuery()) {
            query.getEntryQuery().matchStockId(stockId, true);
            return (this.session.getShipmentsByQuery(query));
        }

        return (super.getShipmentsForStock(stockId));
    }


    /**
     *  Gets a <code> ShipmentList </code> for the given stock and
     *  received between the given date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> ShipmentList </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> stockId, from, </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStockOnDate(org.osid.id.Id stockId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        if (query.supportsEntryQuery()) {
            query.getEntryQuery().matchStockId(stockId, true);
            query.matchDate(from, to, true);
            return (this.session.getShipmentsByQuery(query));
        }

        return (super.getShipmentsForStockOnDate(stockId, from, to));
    }


    /**
     *  Gets a <code> ShipmentList </code> from to the given source.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> ShipmentList </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchSourceId(resourceId, true);
        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets a <code> ShipmentList </code> from the given source and
     *  received between the given date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> ShipmentList </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          from, </code> or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySourceOnDate(org.osid.id.Id resourceId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchSourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets all <code>Shipments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Shipments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.shipment.ShipmentQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getShipmentsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.inventory.shipment.ShipmentQuery getQuery() {
        org.osid.inventory.shipment.ShipmentQuery query = this.session.getShipmentQuery();
        return (query);
    }
}
