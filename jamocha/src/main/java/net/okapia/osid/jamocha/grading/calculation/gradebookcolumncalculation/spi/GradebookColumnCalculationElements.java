//
// GradebookColumnCalculationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradebookColumnCalculationElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the GradebookColumnCalculationElement Id.
     *
     *  @return the gradebook column calculation element Id
     */

    public static org.osid.id.Id getGradebookColumnCalculationEntityId() {
        return (makeEntityId("osid.grading.calculation.GradebookColumnCalculation"));
    }


    /**
     *  Gets the GradebookColumnId element Id.
     *
     *  @return the GradebookColumnId element Id
     */

    public static org.osid.id.Id getGradebookColumnId() {
        return (makeElementId("osid.grading.calculation.gradebookcolumncalculation.GradebookColumnId"));
    }


    /**
     *  Gets the GradebookColumn element Id.
     *
     *  @return the GradebookColumn element Id
     */

    public static org.osid.id.Id getGradebookColumn() {
        return (makeElementId("osid.grading.calculation.gradebookcolumncalculation.GradebookColumn"));
    }


    /**
     *  Gets the InputGradebookColumnIds element Id.
     *
     *  @return the InputGradebookColumnIds element Id
     */

    public static org.osid.id.Id getInputGradebookColumnIds() {
        return (makeElementId("osid.grading.calculation.gradebookcolumncalculation.InputGradebookColumnIds"));
    }


    /**
     *  Gets the InputGradebookColumns element Id.
     *
     *  @return the InputGradebookColumns element Id
     */

    public static org.osid.id.Id getInputGradebookColumns() {
        return (makeElementId("osid.grading.calculation.gradebookcolumncalculation.InputGradebookColumns"));
    }


    /**
     *  Gets the Operation element Id.
     *
     *  @return the Operation element Id
     */

    public static org.osid.id.Id getOperation() {
        return (makeElementId("osid.grading.calculation.gradebookcolumncalculation.Operation"));
    }


    /**
     *  Gets the TweakedCenter element Id.
     *
     *  @return the TweakedCenter element Id
     */

    public static org.osid.id.Id getTweakedCenter() {
        return (makeElementId("osid.grading.calculation.gradebookcolumncalculation.TweakedCenter"));
    }


    /**
     *  Gets the TweakedStandardDeviation element Id.
     *
     *  @return the TweakedStandardDeviation element Id
     */

    public static org.osid.id.Id getTweakedStandardDeviation() {
        return (makeElementId("osid.grading.calculation.gradebookcolumncalculation.TweakedStandardDeviation"));
    }
}
