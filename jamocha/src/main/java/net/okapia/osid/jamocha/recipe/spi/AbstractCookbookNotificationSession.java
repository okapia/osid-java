//
// AbstractCookbookNotificationSession.java
//
//     A template for making CookbookNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Cookbook} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Cookbook} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for cookbook entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractCookbookNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.recipe.CookbookNotificationSession {


    /**
     *  Tests if this user can register for {@code Cookbook}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForCookbookNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new cookbooks. {@code
     *  CookbookReceiver.newCookbook()} is invoked when a new {@code
     *  Cookbook} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCookbooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  cookbook. {@code CookbookReceiver.newAncestorCookbook()} is
     *  invoked when the specified cookbook node gets a new ancestor.
     *
     *  @param cookbookId the {@code Id} of the
     *         {@code Cookbook} node to monitor
     *  @throws org.osid.NullArgumentException {@code cookbookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCookbookAncestors(org.osid.id.Id cookbookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  cookbook. {@code CookbookReceiver.newDescendantCookbook()} is
     *  invoked when the specified cookbook node gets a new
     *  descendant.
     *
     *  @param cookbookId the {@code Id} of the
     *         {@code Cookbook} node to monitor
     *  @throws org.osid.NullArgumentException {@code cookbookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCookbookDescendants(org.osid.id.Id cookbookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated cookbooks. {@code
     *  CookbookReceiver.changedCookbook()} is invoked when a cookbook
     *  is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCookbooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated cookbook. {@code
     *  CookbookReceiver.changedCookbook()} is invoked when the
     *  specified cookbook is changed.
     *
     *  @param cookbookId the {@code Id} of the {@code Cookbook} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code cookbookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCookbook(org.osid.id.Id cookbookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted cookbooks. {@code
     *  CookbookReceiver.deletedCookbook()} is invoked when a cookbook
     *  is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCookbooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted cookbook. {@code
     *  CookbookReceiver.deletedCookbook()} is invoked when the
     *  specified cookbook is deleted.
     *
     *  @param cookbookId the {@code Id} of the
     *          {@code Cookbook} to monitor
     *  @throws org.osid.NullArgumentException {@code cookbookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCookbook(org.osid.id.Id cookbookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified cookbook. {@code
     *  CookbookReceiver.deletedAncestor()} is invoked when the
     *  specified cookbook node loses an ancestor.
     *
     *  @param cookbookId the {@code Id} of the
     *         {@code Cookbook} node to monitor
     *  @throws org.osid.NullArgumentException {@code cookbookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCookbookAncestors(org.osid.id.Id cookbookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified cookbook. {@code
     *  CookbookReceiver.deletedDescendant()} is invoked when the
     *  specified cookbook node loses a descendant.
     *
     *  @param cookbookId the {@code Id} of the
     *          {@code Cookbook} node to monitor
     *  @throws org.osid.NullArgumentException {@code cookbookId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCookbookDescendants(org.osid.id.Id cookbookId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
