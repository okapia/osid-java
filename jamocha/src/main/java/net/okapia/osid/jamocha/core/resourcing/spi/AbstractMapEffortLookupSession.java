//
// AbstractMapEffortLookupSession
//
//    A simple framework for providing an Effort lookup service
//    backed by a fixed collection of efforts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Effort lookup service backed by a
 *  fixed collection of efforts. The efforts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Efforts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEffortLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractEffortLookupSession
    implements org.osid.resourcing.EffortLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.Effort> efforts = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.Effort>());


    /**
     *  Makes an <code>Effort</code> available in this session.
     *
     *  @param  effort an effort
     *  @throws org.osid.NullArgumentException <code>effort<code>
     *          is <code>null</code>
     */

    protected void putEffort(org.osid.resourcing.Effort effort) {
        this.efforts.put(effort.getId(), effort);
        return;
    }


    /**
     *  Makes an array of efforts available in this session.
     *
     *  @param  efforts an array of efforts
     *  @throws org.osid.NullArgumentException <code>efforts<code>
     *          is <code>null</code>
     */

    protected void putEfforts(org.osid.resourcing.Effort[] efforts) {
        putEfforts(java.util.Arrays.asList(efforts));
        return;
    }


    /**
     *  Makes a collection of efforts available in this session.
     *
     *  @param  efforts a collection of efforts
     *  @throws org.osid.NullArgumentException <code>efforts<code>
     *          is <code>null</code>
     */

    protected void putEfforts(java.util.Collection<? extends org.osid.resourcing.Effort> efforts) {
        for (org.osid.resourcing.Effort effort : efforts) {
            this.efforts.put(effort.getId(), effort);
        }

        return;
    }


    /**
     *  Removes an Effort from this session.
     *
     *  @param  effortId the <code>Id</code> of the effort
     *  @throws org.osid.NullArgumentException <code>effortId<code> is
     *          <code>null</code>
     */

    protected void removeEffort(org.osid.id.Id effortId) {
        this.efforts.remove(effortId);
        return;
    }


    /**
     *  Gets the <code>Effort</code> specified by its <code>Id</code>.
     *
     *  @param  effortId <code>Id</code> of the <code>Effort</code>
     *  @return the effort
     *  @throws org.osid.NotFoundException <code>effortId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>effortId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Effort getEffort(org.osid.id.Id effortId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.Effort effort = this.efforts.get(effortId);
        if (effort == null) {
            throw new org.osid.NotFoundException("effort not found: " + effortId);
        }

        return (effort);
    }


    /**
     *  Gets all <code>Efforts</code>. In plenary mode, the returned
     *  list contains all known efforts or an error
     *  results. Otherwise, the returned list may contain only those
     *  efforts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Efforts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEfforts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.effort.ArrayEffortList(this.efforts.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.efforts.clear();
        super.close();
        return;
    }
}
