//
// AbstractSequenceRule.java
//
//     Defines a SequenceRule.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>SequenceRule</code>.
 */

public abstract class AbstractSequenceRule
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.assessment.authoring.SequenceRule {

    private org.osid.assessment.authoring.AssessmentPart assessmentPart;
    private org.osid.assessment.authoring.AssessmentPart nextAssessmentPart;
    private long minimumScore;
    private long maximumScore;
    private boolean cumulative = false;
    private final java.util.Collection<org.osid.assessment.authoring.AssessmentPart> appliedAssessmentParts = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the assessment part <code> Id </code> to which this rule belongs. 
     *
     *  @return <code> Id </code> of an assessment part 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentPartId() {
        return (this.assessmentPart.getId());
    }


    /**
     *  Gets the assessment part to which this rule belongs. 
     *
     *  @return an assessment part 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart()
        throws org.osid.OperationFailedException {

        return (this.assessmentPart);
    }


    /**
     *  Sets the assessment part.
     *
     *  @param assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPart</code> is <code>null</code>
     */

    protected void setAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        nullarg(assessmentPart, "assessment part");
        this.assessmentPart = assessmentPart;
        return;
    }


    /**
     *  Gets the next assessment part <code> Id </code> for success of this 
     *  rule. 
     *
     *  @return <code> Id </code> of an assessment part 
     */

    @OSID @Override
    public org.osid.id.Id getNextAssessmentPartId() {
        return (this.nextAssessmentPart.getId());
    }


    /**
     *  Gets the next assessment part for success of this rule. 
     *
     *  @return an assessment part 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getNextAssessmentPart()
        throws org.osid.OperationFailedException {

        return (this.nextAssessmentPart);
    }


    /**
     *  Sets the next assessment part.
     *
     *  @param assessmentPart a next assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPart</code> is <code>null</code>
     */

    protected void setNextAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        nullarg(assessmentPart, "assessment part");
        this.nextAssessmentPart = assessmentPart;
        return;
    }


    /**
     *  Gets the minimum score expressed as an integer (0-100) for this rule. 
     *
     *  @return minimum score 
     */

    @OSID @Override
    public long getMinimumScore() {
        return (this.minimumScore);
    }


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     *  @throws org.osid.InvalidArgumentException <code>score</code>
     *          is out of range
     */

    protected void setMinimumScore(long score) {
        cardinalarg(score, "score");
        if (score > 100) {
            throw new org.osid.InvalidArgumentException("score > 100");
        }

        this.minimumScore = score;
        return;
    }


    /**
     *  Gets the maximum score expressed as an integer (0-100) for
     *  this rule.
     *
     *  @return maximum score 
     */

    @OSID @Override
    public long getMaximumScore() {
        return (this.maximumScore);
    }


    /**
     *  Sets the maximum score.
     *
     *  @param score a maximum score
     *  @throws org.osid.NullArgumentException
     *          <code>score</code> is <code>null</code>
     */

    protected void setMaximumScore(long score) {
        cardinalarg(score, "score");
        if (score > 100) {
            throw new org.osid.InvalidArgumentException("score > 100");
        }
        
        this.maximumScore = score;
        return;
    }


    /**
     *  Tests if the score is applied to all previous assessment parts. 
     *
     *  @return <code> true </code> if the score is applied to all previous 
     *          assessment parts, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isCumulative() {
        return (this.cumulative);
    }


    /**
     *  Sets the cumulative flag.
     *
     *  @param cumulative <code> true </code> if the score is applied
     *         to all previous assessment parts, <code> false </code>
     *         otherwise
     */

    protected void setCumulative(boolean cumulative) {
        this.cumulative = cumulative;
        return;
    }


    /**
     *  Gets the list of assessment part <code> Ids </code> the minimum and 
     *  maximum score is applied to. If <code> isCumulative() </code> is 
     *  <code> true, </code> this method may return an empty list. 
     *
     *  @return list of assessment parts 
     */

    @OSID @Override
    public org.osid.id.IdList getAppliedAssessmentPartIds() {
        try {
            org.osid.assessment.authoring.AssessmentPartList appliedAssessmentParts = getAppliedAssessmentParts();
            return (new net.okapia.osid.jamocha.adapter.converter.assessment.authoring.assessmentpart.AssessmentPartToIdList(appliedAssessmentParts));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the list of assessment parts the minimum and maximum score is 
     *  applied to. If <code> isCumulative() </code> is <code> true, </code> 
     *  this method may return an empty list. 
     *
     *  @return list of assessment parts 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAppliedAssessmentParts()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.assessmentpart.ArrayAssessmentPartList(this.appliedAssessmentParts));
    }


    /**
     *  Adds an applied assessment part.
     *
     *  @param assessmentPart an applied assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPart</code> is <code>null</code>
     */

    protected void addAppliedAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        nullarg(assessmentPart, "applied assessment part");
        this.appliedAssessmentParts.add(assessmentPart);
        return;
    }


    /**
     *  Sets all the applied assessment parts.
     *
     *  @param assessmentParts a collection of applied assessment parts
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentParts</code> is <code>null</code>
     */

    protected void setAppliedAssessmentParts(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> assessmentParts) {
        nullarg(assessmentParts, "applied assessment parts");

        this.appliedAssessmentParts.clear();
        this.appliedAssessmentParts.addAll(assessmentParts);

        return;
    }


    /**
     *  Tests if this sequence rule supports the given record
     *  <code>Type</code>.
     *
     *  @param  sequenceRuleRecordType a sequence rule record type 
     *  @return <code>true</code> if the sequenceRuleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type sequenceRuleRecordType) {
        for (org.osid.assessment.authoring.records.SequenceRuleRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssessmentSequenceRule</code> record <code>Type</code>.
     *
     *  @param  sequenceRuleRecordType the sequence rule record type 
     *  @return the sequence rule record 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleRecord getSequenceRuleRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this sequence rule. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param sequenceRuleRecord the sequence rule record
     *  @param sequenceRuleRecordType sequence rule record type
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecord</code> or
     *          <code>sequenceRuleRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addSequenceRuleRecord(org.osid.assessment.authoring.records.SequenceRuleRecord sequenceRuleRecord, 
                                         org.osid.type.Type sequenceRuleRecordType) {

        nullarg(sequenceRuleRecord, "sequence rule record");
        addRecordType(sequenceRuleRecordType);
        this.records.add(sequenceRuleRecord);
        
        return;
    }
}
