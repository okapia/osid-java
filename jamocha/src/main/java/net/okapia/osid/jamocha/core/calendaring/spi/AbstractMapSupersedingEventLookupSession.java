//
// AbstractMapSupersedingEventLookupSession
//
//    A simple framework for providing a SupersedingEvent lookup service
//    backed by a fixed collection of superseding events.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a SupersedingEvent lookup service backed by a
 *  fixed collection of superseding events. The superseding events are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SupersedingEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSupersedingEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractSupersedingEventLookupSession
    implements org.osid.calendaring.SupersedingEventLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.SupersedingEvent> supersedingEvents = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.SupersedingEvent>());


    /**
     *  Makes a <code>SupersedingEvent</code> available in this session.
     *
     *  @param  supersedingEvent a superseding event
     *  @throws org.osid.NullArgumentException <code>supersedingEvent<code>
     *          is <code>null</code>
     */

    protected void putSupersedingEvent(org.osid.calendaring.SupersedingEvent supersedingEvent) {
        this.supersedingEvents.put(supersedingEvent.getId(), supersedingEvent);
        return;
    }


    /**
     *  Makes an array of superseding events available in this session.
     *
     *  @param  supersedingEvents an array of superseding events
     *  @throws org.osid.NullArgumentException <code>supersedingEvents<code>
     *          is <code>null</code>
     */

    protected void putSupersedingEvents(org.osid.calendaring.SupersedingEvent[] supersedingEvents) {
        putSupersedingEvents(java.util.Arrays.asList(supersedingEvents));
        return;
    }


    /**
     *  Makes a collection of superseding events available in this session.
     *
     *  @param  supersedingEvents a collection of superseding events
     *  @throws org.osid.NullArgumentException <code>supersedingEvents<code>
     *          is <code>null</code>
     */

    protected void putSupersedingEvents(java.util.Collection<? extends org.osid.calendaring.SupersedingEvent> supersedingEvents) {
        for (org.osid.calendaring.SupersedingEvent supersedingEvent : supersedingEvents) {
            this.supersedingEvents.put(supersedingEvent.getId(), supersedingEvent);
        }

        return;
    }


    /**
     *  Removes a SupersedingEvent from this session.
     *
     *  @param  supersedingEventId the <code>Id</code> of the superseding event
     *  @throws org.osid.NullArgumentException <code>supersedingEventId<code> is
     *          <code>null</code>
     */

    protected void removeSupersedingEvent(org.osid.id.Id supersedingEventId) {
        this.supersedingEvents.remove(supersedingEventId);
        return;
    }


    /**
     *  Gets the <code>SupersedingEvent</code> specified by its <code>Id</code>.
     *
     *  @param  supersedingEventId <code>Id</code> of the <code>SupersedingEvent</code>
     *  @return the supersedingEvent
     *  @throws org.osid.NotFoundException <code>supersedingEventId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>supersedingEventId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEvent getSupersedingEvent(org.osid.id.Id supersedingEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.SupersedingEvent supersedingEvent = this.supersedingEvents.get(supersedingEventId);
        if (supersedingEvent == null) {
            throw new org.osid.NotFoundException("supersedingEvent not found: " + supersedingEventId);
        }

        return (supersedingEvent);
    }


    /**
     *  Gets all <code>SupersedingEvents</code>. In plenary mode, the returned
     *  list contains all known supersedingEvents or an error
     *  results. Otherwise, the returned list may contain only those
     *  supersedingEvents that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>SupersedingEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.supersedingevent.ArraySupersedingEventList(this.supersedingEvents.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.supersedingEvents.clear();
        super.close();
        return;
    }
}
