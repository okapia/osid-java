//
// AbstractAssemblyEdgeQuery.java
//
//     An EdgeQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.topology.edge.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EdgeQuery that stores terms.
 */

public abstract class AbstractAssemblyEdgeQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.topology.EdgeQuery,
               org.osid.topology.EdgeQueryInspector,
               org.osid.topology.EdgeSearchOrder {

    private final java.util.Collection<org.osid.topology.records.EdgeQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.records.EdgeQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.records.EdgeSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEdgeQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEdgeQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    
    /**
     *  Sets the node <code> Id </code> for this query to match edges assigned 
     *  to nodes. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSourceNodeId(org.osid.id.Id nodeId, boolean match) {
        getAssembler().addIdTerm(getSourceNodeIdColumn(), nodeId, match);
        return;
    }


    /**
     *  Clears the node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceNodeIdTerms() {
        getAssembler().clearTerms(getSourceNodeIdColumn());
        return;
    }


    /**
     *  Gets the node <code> Id </code> terms. 
     *
     *  @return the node <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceNodeIdTerms() {
        return (getAssembler().getIdTerms(getSourceNodeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the source node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySourceNode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSourceNodeColumn(), style);
        return;
    }


    /**
     *  Gets the SourceNodeId column name.
     *
     * @return the column name
     */

    protected String getSourceNodeIdColumn() {
        return ("source_node_id");
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a node. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getSourceNodeQuery() {
        throw new org.osid.UnimplementedException("supportsSourceNodeQuery() is false");
    }


    /**
     *  Clears the node terms. 
     */

    @OSID @Override
    public void clearSourceNodeTerms() {
        getAssembler().clearTerms(getSourceNodeColumn());
        return;
    }


    /**
     *  Gets the node terms. 
     *
     *  @return the node terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getSourceNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Tests if a source node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a source node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getSourceNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSourceNodeSearchOrder() is false");
    }


    /**
     *  Gets the SourceNode column name.
     *
     * @return the column name
     */

    protected String getSourceNodeColumn() {
        return ("source_node");
    }


    /**
     *  Sets the related node <code> Id </code> for this query to match edges 
     *  assigned to nodes. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDestinationNodeId(org.osid.id.Id nodeId, boolean match) {
        getAssembler().addIdTerm(getDestinationNodeIdColumn(), nodeId, match);
        return;
    }


    /**
     *  Clears the related node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDestinationNodeIdTerms() {
        getAssembler().clearTerms(getDestinationNodeIdColumn());
        return;
    }


    /**
     *  Gets the related node <code> Id </code> terms. 
     *
     *  @return the node <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDestinationNodeIdTerms() {
        return (getAssembler().getIdTerms(getDestinationNodeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the destination 
     *  node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDestinationNode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDestinationNodeColumn(), style);
        return;
    }


    /**
     *  Gets the DestinationNodeId column name.
     *
     * @return the column name
     */

    protected String getDestinationNodeIdColumn() {
        return ("destination_node_id");
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a related node. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getDestinationNodeQuery() {
        throw new org.osid.UnimplementedException("supportsDestinationNodeQuery() is false");
    }


    /**
     *  Clears the related node terms. 
     */

    @OSID @Override
    public void clearDestinationNodeTerms() {
        getAssembler().clearTerms(getDestinationNodeColumn());
        return;
    }


    /**
     *  Gets the related node terms. 
     *
     *  @return the node terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getDestinationNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Tests if a destination node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a destination node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getDestinationNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDestinationNodeSearchOrder() is false");
    }


    /**
     *  Gets the DestinationNode column name.
     *
     * @return the column name
     */

    protected String getDestinationNodeColumn() {
        return ("destination_node");
    }


    /**
     *  Matches edges between the same node. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSameNode(boolean match) {
        getAssembler().addBooleanTerm(getSameNodeColumn(), match);
        return;
    }


    /**
     *  Clears the same node terms. 
     */

    @OSID @Override
    public void clearSameNodeTerms() {
        getAssembler().clearTerms(getSameNodeColumn());
        return;
    }


    /**
     *  Gets the same node terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSameNodeTerms() {
        return (getAssembler().getBooleanTerms(getSameNodeColumn()));
    }


    /**
     *  Gets the SameNode column name.
     *
     * @return the column name
     */

    protected String getSameNodeColumn() {
        return ("same_node");
    }


    /**
     *  Matches edge directionality. 
     *
     *  @param match <code> true </code> to match directional edges,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public void matchDirectional(boolean match) {
        getAssembler().addBooleanTerm(getDirectionalColumn(), match);
        return;
    }


    /**
     *  Clears the directional terms. 
     */

    @OSID @Override
    public void clearDirectionalTerms() {
        getAssembler().clearTerms(getDirectionalColumn());
        return;
    }


    /**
     *  Gets the directional terms. 
     *
     *  @return the directional terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDirectionalTerms() {
        return (getAssembler().getBooleanTerms(getDirectionalColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  diectionality. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDirectional(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDirectionalColumn(), style);
        return;
    }


    /**
     *  Gets the Directional column name.
     *
     *  @return the column name
     */

    protected String getDirectionalColumn() {
        return ("directional");
    }


    /**
     *  Matches bidirectional edges.
     *
     *  @param match <code> true </code> to match bi-directional edges,
     *          <code> false </code> to atch uni-directional edges
     */

    @OSID @Override
    public void matchBiDirectional(boolean match) {
        getAssembler().addBooleanTerm(getBiDirectionalColumn(), match);
        return;
    }


    /**
     *  Clears the bidirectional terms. 
     */

    @OSID @Override
    public void clearBiDirectionalTerms() {
        getAssembler().clearTerms(getBiDirectionalColumn());
        return;
    }


    /**
     *  Gets the bidirectional terms. 
     *
     *  @return the bidirectional terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getBiDirectionalTerms() {
        return (getAssembler().getBooleanTerms(getBiDirectionalColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  bi-diectional edges.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBiDirectional(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBiDirectionalColumn(), style);
        return;
    }


    /**
     *  Gets the BiDirectional column name.
     *
     *  @return the column name
     */

    protected String getBiDirectionalColumn() {
        return ("bi_directional");
    }


    /**
     *  Matches edges that have the specified cost inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchCost(java.math.BigDecimal from, java.math.BigDecimal to, 
                          boolean match) {
        getAssembler().addDecimalRangeTerm(getCostColumn(), from, to, match);
        return;
    }


    /**
     *  Matches edges that has any cost assigned. 
     *
     *  @param  match <code> true </code> to match edges with any cost, <code> 
     *          false </code> to match edges with no cost assigned 
     */

    @OSID @Override
    public void matchAnyCost(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getCostColumn(), match);
        return;
    }


    /**
     *  Clears the cost terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        getAssembler().clearTerms(getCostColumn());
        return;
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCostTerms() {
        return (getAssembler().getDecimalRangeTerms(getCostColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the edge cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCostColumn(), style);
        return;
    }


    /**
     *  Gets the Cost column name.
     *
     * @return the column name
     */

    protected String getCostColumn() {
        return ("cost");
    }


    /**
     *  Matches edges that have the specified distance inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchDistance(java.math.BigDecimal from, 
                              java.math.BigDecimal to, boolean match) {
        getAssembler().addDecimalRangeTerm(getDistanceColumn(), from, to, match);
        return;
    }


    /**
     *  Matches edges that has any distance assigned. 
     *
     *  @param  match <code> true </code> to match edges with any distance, 
     *          <code> false </code> to match edges with no distance assigned 
     */

    @OSID @Override
    public void matchAnyDistance(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getDistanceColumn(), match);
        return;
    }


    /**
     *  Clears the distance terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        getAssembler().clearTerms(getDistanceColumn());
        return;
    }


    /**
     *  Gets the distance terms. 
     *
     *  @return the distance terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDistanceTerms() {
        return (getAssembler().getDecimalRangeTerms(getDistanceColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the edge 
     *  distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistanceColumn(), style);
        return;
    }


    /**
     *  Gets the Distance column name.
     *
     * @return the column name
     */

    protected String getDistanceColumn() {
        return ("distance");
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match edges 
     *  assigned to graphs. 
     *
     *  @param  graphId the graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraphId(org.osid.id.Id graphId, boolean match) {
        getAssembler().addIdTerm(getGraphIdColumn(), graphId, match);
        return;
    }


    /**
     *  Clears the graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGraphIdTerms() {
        getAssembler().clearTerms(getGraphIdColumn());
        return;
    }


    /**
     *  Gets the graph <code> Id </code> terms. 
     *
     *  @return the graph <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraphIdTerms() {
        return (getAssembler().getIdTerms(getGraphIdColumn()));
    }


    /**
     *  Gets the GraphId column name.
     *
     * @return the column name
     */

    protected String getGraphIdColumn() {
        return ("graph_id");
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getGraphQuery() {
        throw new org.osid.UnimplementedException("supportsGraphQuery() is false");
    }


    /**
     *  Clears the graph terms. 
     */

    @OSID @Override
    public void clearGraphTerms() {
        getAssembler().clearTerms(getGraphColumn());
        return;
    }


    /**
     *  Gets the graph terms. 
     *
     *  @return the graph terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }


    /**
     *  Gets the Graph column name.
     *
     * @return the column name
     */

    protected String getGraphColumn() {
        return ("graph");
    }


    /**
     *  Tests if this edge supports the given record
     *  <code>Type</code>.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return <code>true</code> if the edgeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type edgeRecordType) {
        for (org.osid.topology.records.EdgeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  edgeRecordType the edge record type 
     *  @return the edge query record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.EdgeQueryRecord getEdgeQueryRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.EdgeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  edgeRecordType the edge record type 
     *  @return the edge query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.EdgeQueryInspectorRecord getEdgeQueryInspectorRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.EdgeQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param edgeRecordType the edge record type
     *  @return the edge search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.EdgeSearchOrderRecord getEdgeSearchOrderRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.EdgeSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this edge. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param edgeQueryRecord the edge query record
     *  @param edgeQueryInspectorRecord the edge query inspector
     *         record
     *  @param edgeSearchOrderRecord the edge search order record
     *  @param edgeRecordType edge record type
     *  @throws org.osid.NullArgumentException
     *          <code>edgeQueryRecord</code>,
     *          <code>edgeQueryInspectorRecord</code>,
     *          <code>edgeSearchOrderRecord</code> or
     *          <code>edgeRecordTypeedge</code> is
     *          <code>null</code>
     */
            
    protected void addEdgeRecords(org.osid.topology.records.EdgeQueryRecord edgeQueryRecord, 
                                      org.osid.topology.records.EdgeQueryInspectorRecord edgeQueryInspectorRecord, 
                                      org.osid.topology.records.EdgeSearchOrderRecord edgeSearchOrderRecord, 
                                      org.osid.type.Type edgeRecordType) {

        addRecordType(edgeRecordType);

        nullarg(edgeQueryRecord, "edge query record");
        nullarg(edgeQueryInspectorRecord, "edge query inspector record");
        nullarg(edgeSearchOrderRecord, "edge search odrer record");

        this.queryRecords.add(edgeQueryRecord);
        this.queryInspectorRecords.add(edgeQueryInspectorRecord);
        this.searchOrderRecords.add(edgeSearchOrderRecord);
        
        return;
    }
}
