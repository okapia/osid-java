//
// MutableIndexedMapProxyProcessEnablerLookupSession
//
//    Implements a ProcessEnabler lookup service backed by a collection of
//    processEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a ProcessEnabler lookup service backed by a collection of
 *  processEnablers. The process enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some processEnablers may be compatible
 *  with more types than are indicated through these processEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of process enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyProcessEnablerLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractIndexedMapProcessEnablerLookupSession
    implements org.osid.workflow.rules.ProcessEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProcessEnablerLookupSession} with
     *  no process enabler.
     *
     *  @param office the office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProcessEnablerLookupSession(org.osid.workflow.Office office,
                                                       org.osid.proxy.Proxy proxy) {
        setOffice(office);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProcessEnablerLookupSession} with
     *  a single process enabler.
     *
     *  @param office the office
     *  @param  processEnabler an process enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code processEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProcessEnablerLookupSession(org.osid.workflow.Office office,
                                                       org.osid.workflow.rules.ProcessEnabler processEnabler, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putProcessEnabler(processEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProcessEnablerLookupSession} using
     *  an array of process enablers.
     *
     *  @param office the office
     *  @param  processEnablers an array of process enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code processEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProcessEnablerLookupSession(org.osid.workflow.Office office,
                                                       org.osid.workflow.rules.ProcessEnabler[] processEnablers, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putProcessEnablers(processEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProcessEnablerLookupSession} using
     *  a collection of process enablers.
     *
     *  @param office the office
     *  @param  processEnablers a collection of process enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code processEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProcessEnablerLookupSession(org.osid.workflow.Office office,
                                                       java.util.Collection<? extends org.osid.workflow.rules.ProcessEnabler> processEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(office, proxy);
        putProcessEnablers(processEnablers);
        return;
    }

    
    /**
     *  Makes a {@code ProcessEnabler} available in this session.
     *
     *  @param  processEnabler a process enabler
     *  @throws org.osid.NullArgumentException {@code processEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putProcessEnabler(org.osid.workflow.rules.ProcessEnabler processEnabler) {
        super.putProcessEnabler(processEnabler);
        return;
    }


    /**
     *  Makes an array of process enablers available in this session.
     *
     *  @param  processEnablers an array of process enablers
     *  @throws org.osid.NullArgumentException {@code processEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putProcessEnablers(org.osid.workflow.rules.ProcessEnabler[] processEnablers) {
        super.putProcessEnablers(processEnablers);
        return;
    }


    /**
     *  Makes collection of process enablers available in this session.
     *
     *  @param  processEnablers a collection of process enablers
     *  @throws org.osid.NullArgumentException {@code processEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putProcessEnablers(java.util.Collection<? extends org.osid.workflow.rules.ProcessEnabler> processEnablers) {
        super.putProcessEnablers(processEnablers);
        return;
    }


    /**
     *  Removes a ProcessEnabler from this session.
     *
     *  @param processEnablerId the {@code Id} of the process enabler
     *  @throws org.osid.NullArgumentException {@code processEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProcessEnabler(org.osid.id.Id processEnablerId) {
        super.removeProcessEnabler(processEnablerId);
        return;
    }    
}
