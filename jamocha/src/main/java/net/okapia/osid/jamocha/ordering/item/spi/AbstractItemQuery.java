//
// AbstractItemQuery.java
//
//     A template for making an Item Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for items.
 */

public abstract class AbstractItemQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.ordering.ItemQuery {

    private final java.util.Collection<org.osid.ordering.records.ItemQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the order <code> Id </code> for this query to match orders 
     *  assigned to items. 
     *
     *  @param  orderId a order <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> orderId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOrderId(org.osid.id.Id orderId, boolean match) {
        return;
    }


    /**
     *  Clears the order <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrderIdTerms() {
        return;
    }


    /**
     *  Tests if a order query is available. 
     *
     *  @return <code> true </code> if a order query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. 
     *
     *  @return the order query 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuery getOrderQuery() {
        throw new org.osid.UnimplementedException("supportsOrderQuery() is false");
    }


    /**
     *  Clears the order terms. 
     */

    @OSID @Override
    public void clearOrderTerms() {
        return;
    }


    /**
     *  Matches items that are derived. 
     *
     *  @param  match <code> true </code> to match derived items, <code> false 
     *          </code> to match selected items 
     */

    @OSID @Override
    public void matchDerived(boolean match) {
        return;
    }


    /**
     *  Clears the derived terms. 
     */

    @OSID @Override
    public void clearDerivedTerms() {
        return;
    }


    /**
     *  Sets the product <code> Id </code> for this query. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        return;
    }


    /**
     *  Clears the product <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        return;
    }


    /**
     *  Tests if a product query is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        return;
    }


    /**
     *  Matches quantities between the given range inclusive. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchQuantity(long low, long high, boolean match) {
        return;
    }


    /**
     *  Clears the quantity terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        return;
    }


    /**
     *  Matches costs between the given range inclusive. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCost(org.osid.financials.Currency low, 
                          org.osid.financials.Currency high, boolean match) {
        return;
    }


    /**
     *  Clears the cost terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        return;
    }


    /**
     *  Matches minimum costs between the given range inclusive. 
     *
     *  @param  cost cost 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cost </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMinimumCost(org.osid.financials.Currency cost, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the minimum cost terms. 
     */

    @OSID @Override
    public void clearMinimumCostTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given item query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an item implementing the requested record.
     *
     *  @param itemRecordType an item record type
     *  @return the item query record
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ItemQueryRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item query. 
     *
     *  @param itemQueryRecord item query record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addItemQueryRecord(org.osid.ordering.records.ItemQueryRecord itemQueryRecord, 
                                          org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);
        nullarg(itemQueryRecord, "item query record");
        this.records.add(itemQueryRecord);        
        return;
    }
}
