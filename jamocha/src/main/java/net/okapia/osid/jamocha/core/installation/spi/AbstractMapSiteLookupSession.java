//
// AbstractMapSiteLookupSession
//
//    A simple framework for providing a Site lookup service
//    backed by a fixed collection of sites.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Site lookup service backed by a
 *  fixed collection of sites. The sites are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Sites</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSiteLookupSession
    extends net.okapia.osid.jamocha.installation.spi.AbstractSiteLookupSession
    implements org.osid.installation.SiteLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.installation.Site> sites = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.installation.Site>());


    /**
     *  Makes a <code>Site</code> available in this session.
     *
     *  @param  site a site
     *  @throws org.osid.NullArgumentException <code>site<code>
     *          is <code>null</code>
     */

    protected void putSite(org.osid.installation.Site site) {
        this.sites.put(site.getId(), site);
        return;
    }


    /**
     *  Makes an array of sites available in this session.
     *
     *  @param  sites an array of sites
     *  @throws org.osid.NullArgumentException <code>sites<code>
     *          is <code>null</code>
     */

    protected void putSites(org.osid.installation.Site[] sites) {
        putSites(java.util.Arrays.asList(sites));
        return;
    }


    /**
     *  Makes a collection of sites available in this session.
     *
     *  @param  sites a collection of sites
     *  @throws org.osid.NullArgumentException <code>sites<code>
     *          is <code>null</code>
     */

    protected void putSites(java.util.Collection<? extends org.osid.installation.Site> sites) {
        for (org.osid.installation.Site site : sites) {
            this.sites.put(site.getId(), site);
        }

        return;
    }


    /**
     *  Removes a Site from this session.
     *
     *  @param  siteId the <code>Id</code> of the site
     *  @throws org.osid.NullArgumentException <code>siteId<code> is
     *          <code>null</code>
     */

    protected void removeSite(org.osid.id.Id siteId) {
        this.sites.remove(siteId);
        return;
    }


    /**
     *  Gets the <code>Site</code> specified by its <code>Id</code>.
     *
     *  @param  siteId <code>Id</code> of the <code>Site</code>
     *  @return the site
     *  @throws org.osid.NotFoundException <code>siteId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>siteId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Site getSite(org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.installation.Site site = this.sites.get(siteId);
        if (site == null) {
            throw new org.osid.NotFoundException("site not found: " + siteId);
        }

        return (site);
    }


    /**
     *  Gets all <code>Sites</code>. In plenary mode, the returned
     *  list contains all known sites or an error
     *  results. Otherwise, the returned list may contain only those
     *  sites that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Sites</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.SiteList getSites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.site.ArraySiteList(this.sites.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.sites.clear();
        super.close();
        return;
    }
}
