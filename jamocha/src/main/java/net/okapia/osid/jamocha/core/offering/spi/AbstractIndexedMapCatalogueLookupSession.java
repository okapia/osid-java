//
// AbstractIndexedMapCatalogueLookupSession.java
//
//    A simple framework for providing a Catalogue lookup service
//    backed by a fixed collection of catalogues with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Catalogue lookup service backed by a
 *  fixed collection of catalogues. The catalogues are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some catalogues may be compatible
 *  with more types than are indicated through these catalogue
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Catalogues</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCatalogueLookupSession
    extends AbstractMapCatalogueLookupSession
    implements org.osid.offering.CatalogueLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.Catalogue> cataloguesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Catalogue>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.Catalogue> cataloguesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Catalogue>());


    /**
     *  Makes a <code>Catalogue</code> available in this session.
     *
     *  @param  catalogue a catalogue
     *  @throws org.osid.NullArgumentException <code>catalogue<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCatalogue(org.osid.offering.Catalogue catalogue) {
        super.putCatalogue(catalogue);

        this.cataloguesByGenus.put(catalogue.getGenusType(), catalogue);
        
        try (org.osid.type.TypeList types = catalogue.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cataloguesByRecord.put(types.getNextType(), catalogue);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of catalogues available in this session.
     *
     *  @param  catalogues an array of catalogues
     *  @throws org.osid.NullArgumentException <code>catalogues<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCatalogues(org.osid.offering.Catalogue[] catalogues) {
        for (org.osid.offering.Catalogue catalogue : catalogues) {
            putCatalogue(catalogue);
        }

        return;
    }


    /**
     *  Makes a collection of catalogues available in this session.
     *
     *  @param  catalogues a collection of catalogues
     *  @throws org.osid.NullArgumentException <code>catalogues<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCatalogues(java.util.Collection<? extends org.osid.offering.Catalogue> catalogues) {
        for (org.osid.offering.Catalogue catalogue : catalogues) {
            putCatalogue(catalogue);
        }

        return;
    }


    /**
     *  Removes a catalogue from this session.
     *
     *  @param catalogueId the <code>Id</code> of the catalogue
     *  @throws org.osid.NullArgumentException <code>catalogueId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCatalogue(org.osid.id.Id catalogueId) {
        org.osid.offering.Catalogue catalogue;
        try {
            catalogue = getCatalogue(catalogueId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.cataloguesByGenus.remove(catalogue.getGenusType());

        try (org.osid.type.TypeList types = catalogue.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cataloguesByRecord.remove(types.getNextType(), catalogue);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCatalogue(catalogueId);
        return;
    }


    /**
     *  Gets a <code>CatalogueList</code> corresponding to the given
     *  catalogue genus <code>Type</code> which does not include
     *  catalogues of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known catalogues or an error results. Otherwise,
     *  the returned list may contain only those catalogues that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  catalogueGenusType a catalogue genus type 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByGenusType(org.osid.type.Type catalogueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.catalogue.ArrayCatalogueList(this.cataloguesByGenus.get(catalogueGenusType)));
    }


    /**
     *  Gets a <code>CatalogueList</code> containing the given
     *  catalogue record <code>Type</code>. In plenary mode, the
     *  returned list contains all known catalogues or an error
     *  results. Otherwise, the returned list may contain only those
     *  catalogues that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  catalogueRecordType a catalogue record type 
     *  @return the returned <code>catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByRecordType(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.catalogue.ArrayCatalogueList(this.cataloguesByRecord.get(catalogueRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.cataloguesByGenus.clear();
        this.cataloguesByRecord.clear();

        super.close();

        return;
    }
}
