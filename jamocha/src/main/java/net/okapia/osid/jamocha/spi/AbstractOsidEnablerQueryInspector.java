//
// AbstractOsidEnablerQueryInspector.java
//
//     Defines an OsidEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an OsidEnablerQueryInspector to extend. 
 */

public abstract class AbstractOsidEnablerQueryInspector
    extends AbstractOsidRuleQueryInspector
    implements org.osid.OsidEnablerQueryInspector {

    private final OsidTemporalQueryInspector inspector = new OsidTemporalQueryInspector();
    

    /**
     *  Gets the schedule <code>Id</code> query terms.
     *
     *  @return the schedule <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the schedule query terms.
     *
     *  @return the schedule terms
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQueryInspector[] getScheduleTerms() {
        return (new org.osid.calendaring.ScheduleQueryInspector[0]);
    }


    /**
     *  Gets the event <code>Id</code> query terms.
     *
     *  @return the event <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event query terms.
     *
     *  @return the event terms
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the cyclic event <code>Id</code> query terms.
     *
     *  @return the cyclic event <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCyclicEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the cyclic event query terms.
     *
     *  @return the cyclic event terms
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQueryInspector[] getCyclicEventTerms() {
        return (new org.osid.calendaring.cycle.CyclicEventQueryInspector[0]);
    }


    /**
     *  Gets the demographic <code>Id</code> query terms.
     *
     *  @return the demographic <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDemographicIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the demographic query terms.
     *
     *  @return the demographic terms
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getDemographicTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the effective query terms.
     *
     *  @return the effective terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (this.inspector.getEffectiveTerms());
    }


    /**
     *  Gets the start date query terms.
     *
     *  @return the start date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (this.inspector.getStartDateTerms());
    }


    /**
     *  Gets the end date query terms.
     *
     *  @return the end date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (this.inspector.getEndDateTerms());
    }


    /**
     *  Gets the date query terms.
     *
     *  @return the date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (this.inspector.getDateTerms());
    }

    
    protected class OsidTemporalQueryInspector
        extends AbstractOsidTemporalQueryInspector
        implements org.osid.OsidTemporalQueryInspector {
    }
}

