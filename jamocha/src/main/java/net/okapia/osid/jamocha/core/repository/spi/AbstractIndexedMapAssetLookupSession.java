//
// AbstractIndexedMapAssetLookupSession.java
//
//    A simple framework for providing an Asset lookup service
//    backed by a fixed collection of assets with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Asset lookup service backed by a
 *  fixed collection of assets. The assets are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some assets may be compatible
 *  with more types than are indicated through these asset
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Assets</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAssetLookupSession
    extends AbstractMapAssetLookupSession
    implements org.osid.repository.AssetLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.repository.Asset> assetsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.Asset>());
    private final MultiMap<org.osid.type.Type, org.osid.repository.Asset> assetsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.Asset>());


    /**
     *  Makes an <code>Asset</code> available in this session.
     *
     *  @param  asset an asset
     *  @throws org.osid.NullArgumentException <code>asset<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAsset(org.osid.repository.Asset asset) {
        super.putAsset(asset);

        this.assetsByGenus.put(asset.getGenusType(), asset);
        
        try (org.osid.type.TypeList types = asset.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assetsByRecord.put(types.getNextType(), asset);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an asset from this session.
     *
     *  @param assetId the <code>Id</code> of the asset
     *  @throws org.osid.NullArgumentException <code>assetId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAsset(org.osid.id.Id assetId) {
        org.osid.repository.Asset asset;
        try {
            asset = getAsset(assetId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.assetsByGenus.remove(asset.getGenusType());

        try (org.osid.type.TypeList types = asset.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assetsByRecord.remove(types.getNextType(), asset);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAsset(assetId);
        return;
    }


    /**
     *  Gets an <code>AssetList</code> corresponding to the given
     *  asset genus <code>Type</code> which does not include
     *  assets of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known assets or an error results. Otherwise,
     *  the returned list may contain only those assets that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  assetGenusType an asset genus type 
     *  @return the returned <code>Asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByGenusType(org.osid.type.Type assetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.assetsByGenus.get(assetGenusType)));
    }


    /**
     *  Gets an <code>AssetList</code> containing the given
     *  asset record <code>Type</code>. In plenary mode, the
     *  returned list contains all known assets or an error
     *  results. Otherwise, the returned list may contain only those
     *  assets that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  assetRecordType an asset record type 
     *  @return the returned <code>asset</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssetsByRecordType(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.assetsByRecord.get(assetRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assetsByGenus.clear();
        this.assetsByRecord.clear();

        super.close();

        return;
    }
}
