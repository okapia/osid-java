//
// AbstractIndexedMapAssessmentTakenLookupSession.java
//
//    A simple framework for providing an AssessmentTaken lookup service
//    backed by a fixed collection of assessments taken with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AssessmentTaken lookup service backed by a
 *  fixed collection of assessments taken. The assessments taken are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some assessments taken may be compatible
 *  with more types than are indicated through these assessment taken
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentsTaken</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAssessmentTakenLookupSession
    extends AbstractMapAssessmentTakenLookupSession
    implements org.osid.assessment.AssessmentTakenLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.AssessmentTaken> assessmentsTakenByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.AssessmentTaken>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.AssessmentTaken> assessmentsTakenByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.AssessmentTaken>());


    /**
     *  Makes an <code>AssessmentTaken</code> available in this session.
     *
     *  @param  assessmentTaken an assessment taken
     *  @throws org.osid.NullArgumentException <code>assessmentTaken<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAssessmentTaken(org.osid.assessment.AssessmentTaken assessmentTaken) {
        super.putAssessmentTaken(assessmentTaken);

        this.assessmentsTakenByGenus.put(assessmentTaken.getGenusType(), assessmentTaken);
        
        try (org.osid.type.TypeList types = assessmentTaken.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentsTakenByRecord.put(types.getNextType(), assessmentTaken);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an assessment taken from this session.
     *
     *  @param assessmentTakenId the <code>Id</code> of the assessment taken
     *  @throws org.osid.NullArgumentException <code>assessmentTakenId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAssessmentTaken(org.osid.id.Id assessmentTakenId) {
        org.osid.assessment.AssessmentTaken assessmentTaken;
        try {
            assessmentTaken = getAssessmentTaken(assessmentTakenId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.assessmentsTakenByGenus.remove(assessmentTaken.getGenusType());

        try (org.osid.type.TypeList types = assessmentTaken.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentsTakenByRecord.remove(types.getNextType(), assessmentTaken);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAssessmentTaken(assessmentTakenId);
        return;
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  assessment taken genus <code>Type</code> which does not include
     *  assessments taken of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known assessments taken or an error results. Otherwise,
     *  the returned list may contain only those assessments taken that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  assessmentTakenGenusType an assessment taken genus type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.ArrayAssessmentTakenList(this.assessmentsTakenByGenus.get(assessmentTakenGenusType)));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> containing the given
     *  assessment taken record <code>Type</code>. In plenary mode, the
     *  returned list contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  assessmentTakenRecordType an assessment taken record type 
     *  @return the returned <code>assessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByRecordType(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.ArrayAssessmentTakenList(this.assessmentsTakenByRecord.get(assessmentTakenRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentsTakenByGenus.clear();
        this.assessmentsTakenByRecord.clear();

        super.close();

        return;
    }
}
