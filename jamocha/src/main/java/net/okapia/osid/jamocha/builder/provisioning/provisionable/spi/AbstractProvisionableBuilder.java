//
// AbstractProvisionable.java
//
//     Defines a Provisionable builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.provisionable.spi;


/**
 *  Defines a <code>Provisionable</code> builder.
 */

public abstract class AbstractProvisionableBuilder<T extends AbstractProvisionableBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.provisionable.ProvisionableMiter provisionable;


    /**
     *  Constructs a new <code>AbstractProvisionableBuilder</code>.
     *
     *  @param provisionable the provisionable to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProvisionableBuilder(net.okapia.osid.jamocha.builder.provisioning.provisionable.ProvisionableMiter provisionable) {
        super(provisionable);
        this.provisionable = provisionable;
        return;
    }


    /**
     *  Builds the provisionable.
     *
     *  @return the new provisionable
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.Provisionable build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.provisionable.ProvisionableValidator(getValidations())).validate(this.provisionable);
        return (new net.okapia.osid.jamocha.builder.provisioning.provisionable.ImmutableProvisionable(this.provisionable));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the provisionable miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.provisionable.ProvisionableMiter getMiter() {
        return (this.provisionable);
    }


    /**
     *  Sets the pool.
     *
     *  @param pool a pool
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>pool</code> is <code>null</code>
     */

    public T pool(org.osid.provisioning.Pool pool) {
        getMiter().setPool(pool);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the use count.
     *
     *  @param use the use count
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>use</code> is
     *          negative
     */

    public T use(long use) {
        getMiter().setUse(use);
        return (self());
    }


    /**
     *  Adds a Provisionable record.
     *
     *  @param record a provisionable record
     *  @param recordType the type of provisionable record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.records.ProvisionableRecord record, org.osid.type.Type recordType) {
        getMiter().addProvisionableRecord(record, recordType);
        return (self());
    }
}       


