//
// AbstractSupersedingEvent.java
//
//     Defines a SupersedingEvent.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.supersedingevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>SupersedingEvent</code>.
 */

public abstract class AbstractSupersedingEvent
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.calendaring.SupersedingEvent {

    private org.osid.calendaring.Event supersededEvent;
    private org.osid.calendaring.Event supersedingEvent;
    private org.osid.calendaring.DateTime supersededDate;

    private boolean supersedesByPosition = false;
    private long supersededEventPosition;

    private final java.util.Collection<org.osid.calendaring.records.SupersedingEventRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the event <code> Id </code> that is to be superseded. 
     *
     *  @return the superseding event <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSupersededEventId() {
        return (this.supersededEvent.getId());
    }


    /**
     *  Gets the event that is to be superseded. 
     *
     *  @return the superseding event 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getSupersededEvent()
        throws org.osid.OperationFailedException {

        return (this.supersededEvent);
    }


    /**
     *  Sets the superseded event.
     *
     *  @param event a superseded event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    protected void setSupersededEvent(org.osid.calendaring.Event event) {
        nullarg(event, "supserseded event");
        this.supersededEvent = event;
        return;
    }


    /**
     *  Gets the event <code> Id </code> that is superseding another.
     *
     *  @return the superseding event <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSupersedingEventId() {
        return (this.supersedingEvent.getId());
    }


    /**
     *  Gets the event that is superseding another. 
     *
     *  @return the superseding event 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getSupersedingEvent()
        throws org.osid.OperationFailedException {

        return (this.supersedingEvent);
    }


    /**
     *  Sets the superseding event.
     *
     *  @param event a superseding event
     *  @throws org.osid.NullArgumentException
     *          <code>event</code> is <code>null</code>
     */

    protected void setSupersedingEvent(org.osid.calendaring.Event event) {
        this.supersedingEvent = event;
        return;
    }


    /**
     *  Tests if the superseding event replaces an event within a
     *  recurring series offered at a specific date/time.
     *
     *  @return <code> true </code> if an event is superseded by date,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supersedesByDate() {
        return (this.supersededDate != null);
    }


    /**
     *  Gets the date of an event to replace if a recurring event is
     *  offered on that date.
     *
     *  @return the date of the event to replace 
     *  @throws org.osid.IllegalStateException <code>
     *          supersedesByDate() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSupersededDate() {
        if (!supersedesByDate()) {
            throw new org.osid.IllegalStateException("supersedesByDate() is false");
        }

        return (this.supersededDate);
    }


    /**
     *  Sets the superseded date.
     *
     *  @param date a superseded date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setSupersededDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.supersededDate = date;
        return;
    }


    /**
     *  Tests if the superseding event replaces an event within a
     *  recurring series identified by its denormalized position in
     *  the series. A negative number counts from the end of the
     *  series.
     *
     *  @return <code> true </code> if an event is superseded by position, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supersedesByPosition() {
        return (this.supersedesByPosition);
    }


    /**
     *  Gets the position in the denormalized recurring series of the
     *  event to replace. Positive numbers count from the start and
     *  negative numbers count from the end. Zero is invalid.
     *
     *  @return the position of the event to replace 
     *  @throws org.osid.IllegalStateException <code>
     *          supersedesByPosition() </code> is <code> false </code>
     */

    @OSID @Override
    public long getSupersededEventPosition() {
        if (!supersedesByPosition()) {
            throw new org.osid.IllegalStateException("supersedesByPosition() is false");
        }

        return (this.supersededEventPosition);
    }


    /**
     *  Sets the superseded event position.
     *
     *  @param position a superseded event position
     */

    protected void setSupersededEventPosition(long position) {
        this.supersededEventPosition = position;
        this.supersedesByPosition = true;

        return;
    }


    /**
     *  Tests if this superseding event supports the given record
     *  <code>Type</code>.
     *
     *  @param supersedingEventRecordType a superseding event record
     *         type
     *  @return <code>true</code> if the supersedingEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type supersedingEventRecordType) {
        for (org.osid.calendaring.records.SupersedingEventRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>SupersedingEvent</code> record <code>Type</code>.
     *
     *  @param supersedingEventRecordType the superseding event record
     *         type
     *  @return the superseding event record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventRecord getSupersedingEventRecord(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.SupersedingEventRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this superseding event. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param supersedingEventRecord the superseding event record
     *  @param supersedingEventRecordType superseding event record type
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecord</code> or
     *          <code>supersedingEventRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addSupersedingEventRecord(org.osid.calendaring.records.SupersedingEventRecord supersedingEventRecord, 
                                             org.osid.type.Type supersedingEventRecordType) {
        
        nullarg(supersedingEventRecord, "superseding event record");
        addRecordType(supersedingEventRecordType);
        this.records.add(supersedingEventRecord);
        
        return;
    }
}
