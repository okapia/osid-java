//
// AbstractMutableReading.java
//
//     Defines a mutable Reading.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.reading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Reading</code>.
 */

public abstract class AbstractMutableReading
    extends net.okapia.osid.jamocha.metering.reading.spi.AbstractReading
    implements org.osid.metering.Reading,
               net.okapia.osid.jamocha.builder.metering.reading.ReadingMiter {


    /**
     *  Sets the meter.
     *
     *  @param meter the meter
     *  @throws org.osid.NullArgumentException <code>meter</code> is
     *          <code>null</code>
     */

    @Override
    public void setMeter(org.osid.metering.Meter meter) {
        super.setMeter(meter);
        return;
    }


    /**
     *  Sets the metered object Id.
     *
     *  @param objectId the metered object Id
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     */

    @Override
    public void setMeteredObjectId(org.osid.id.Id objectId) {
        super.setMeteredObjectId(objectId);
        return;
    }
    

    /**
     *  Sets the amount.
     *
     *  @param amount the amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void setAmount(java.math.BigDecimal amount) {
        super.setAmount(amount);
        return;
    }
}


