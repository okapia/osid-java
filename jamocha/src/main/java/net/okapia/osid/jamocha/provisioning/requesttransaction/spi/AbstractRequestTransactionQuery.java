//
// AbstractRequestTransactionQuery.java
//
//     A template for making a RequestTransaction Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.requesttransaction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for request transactions.
 */

public abstract class AbstractRequestTransactionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.provisioning.RequestTransactionQuery {

    private final java.util.Collection<org.osid.provisioning.records.RequestTransactionQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        return;
    }


    /**
     *  Matches transactions with a submit date in the given range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSubmitDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the submit date query terms. 
     */

    @OSID @Override
    public void clearSubmitDateTerms() {
        return;
    }


    /**
     *  Sets the submitter <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmitterId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the submitter <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSubmitterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmitterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmitterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSubmitterQuery() {
        throw new org.osid.UnimplementedException("supportsSubmitterQuery() is false");
    }


    /**
     *  Clears the submitter query terms. 
     */

    @OSID @Override
    public void clearSubmitterTerms() {
        return;
    }


    /**
     *  Sets the submitting agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmittingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the submitting agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmittingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitting agent. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmittingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getSubmittingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsSubmittingAgentQuery() is false");
    }


    /**
     *  Clears the submitting agent query terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentTerms() {
        return;
    }


    /**
     *  Sets the request <code> Id </code> for this query. 
     *
     *  @param  requestId the request <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestId(org.osid.id.Id requestId, boolean match) {
        return;
    }


    /**
     *  Clears the request <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequestQuery </code> is available. 
     *
     *  @return <code> true </code> if a request query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (false);
    }


    /**
     *  Gets the query for a request. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the request query 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuery getRequestQuery() {
        throw new org.osid.UnimplementedException("supportsRequestQuery() is false");
    }


    /**
     *  Matches transactions with any request. 
     *
     *  @param  match <code> true </code> for to match transactions with any 
     *          request, match, <code> false </code> to match transaction with 
     *          no requests 
     */

    @OSID @Override
    public void matchAnyRequest(boolean match) {
        return;
    }


    /**
     *  Clears the request query terms. 
     */

    @OSID @Override
    public void clearRequestTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given request transaction query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a request transaction implementing the requested record.
     *
     *  @param requestTransactionRecordType a request transaction record type
     *  @return the request transaction query record
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestTransactionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestTransactionQueryRecord getRequestTransactionQueryRecord(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestTransactionQueryRecord record : this.records) {
            if (record.implementsRecordType(requestTransactionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestTransactionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this request transaction query. 
     *
     *  @param requestTransactionQueryRecord request transaction query record
     *  @param requestTransactionRecordType requestTransaction record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRequestTransactionQueryRecord(org.osid.provisioning.records.RequestTransactionQueryRecord requestTransactionQueryRecord, 
                                          org.osid.type.Type requestTransactionRecordType) {

        addRecordType(requestTransactionRecordType);
        nullarg(requestTransactionQueryRecord, "request transaction query record");
        this.records.add(requestTransactionQueryRecord);        
        return;
    }
}
