//
// AbstractAcademyQueryInspector.java
//
//     A template for making an AcademyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.academy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for academies.
 */

public abstract class AbstractAcademyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.recognition.AcademyQueryInspector {

    private final java.util.Collection<org.osid.recognition.records.AcademyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the conferral <code> Id </code> terms. 
     *
     *  @return the conferral <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConferralIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the conferral terms. 
     *
     *  @return the conferral terms 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQueryInspector[] getConferralTerms() {
        return (new org.osid.recognition.ConferralQueryInspector[0]);
    }


    /**
     *  Gets the award <code> Id </code> terms. 
     *
     *  @return the award <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the award terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the convocation <code> Id </code> terms. 
     *
     *  @return the convocation <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConvocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the convocation terms. 
     *
     *  @return the convocation terms 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQueryInspector[] getConvocationTerms() {
        return (new org.osid.recognition.ConvocationQueryInspector[0]);
    }


    /**
     *  Gets the ancestor academy <code> Id </code> terms. 
     *
     *  @return the ancestor academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAcademyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor academy terms. 
     *
     *  @return the ancestor academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAncestorAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }


    /**
     *  Gets the descendant academy <code> Id </code> terms. 
     *
     *  @return the descendant academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAcademyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant academy terms. 
     *
     *  @return the descendant academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getDescendantAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given academy query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an academy implementing the requested record.
     *
     *  @param academyRecordType an academy record type
     *  @return the academy query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(academyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AcademyQueryInspectorRecord getAcademyQueryInspectorRecord(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AcademyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(academyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(academyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this academy query. 
     *
     *  @param academyQueryInspectorRecord academy query inspector
     *         record
     *  @param academyRecordType academy record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAcademyQueryInspectorRecord(org.osid.recognition.records.AcademyQueryInspectorRecord academyQueryInspectorRecord, 
                                                   org.osid.type.Type academyRecordType) {

        addRecordType(academyRecordType);
        nullarg(academyRecordType, "academy record type");
        this.records.add(academyQueryInspectorRecord);        
        return;
    }
}
