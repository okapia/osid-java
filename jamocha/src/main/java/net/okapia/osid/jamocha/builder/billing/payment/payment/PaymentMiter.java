//
// PaymentMiter.java
//
//     Defines a Payment miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.payment;


/**
 *  Defines a <code>Payment</code> miter for use with the builders.
 */

public interface PaymentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.billing.payment.Payment {


    /**
     *  Sets the payer.
     *
     *  @param payer a payer
     *  @throws org.osid.NullArgumentException <code>payer</code> is
     *          <code>null</code>
     */

    public void setPayer(org.osid.billing.payment.Payer payer);


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public void setCustomer(org.osid.billing.Customer customer);


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public void setPeriod(org.osid.billing.Period period);


    /**
     *  Sets the payment date.
     *
     *  @param date a payment date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setPaymentDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the process date.
     *
     *  @param date a process date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setProcessDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setAmount(org.osid.financials.Currency amount);


    /**
     *  Adds a Payment record.
     *
     *  @param record a payment record
     *  @param recordType the type of payment record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPaymentRecord(org.osid.billing.payment.records.PaymentRecord record, org.osid.type.Type recordType);
}       


