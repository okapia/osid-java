//
// AbstractRaceProcessorSearch.java
//
//     A template for making a RaceProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing race processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRaceProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.rules.RaceProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.rules.RaceProcessorSearchOrder raceProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of race processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  raceProcessorIds list of race processors
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRaceProcessors(org.osid.id.IdList raceProcessorIds) {
        while (raceProcessorIds.hasNext()) {
            try {
                this.ids.add(raceProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRaceProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of race processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRaceProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  raceProcessorSearchOrder race processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>raceProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRaceProcessorResults(org.osid.voting.rules.RaceProcessorSearchOrder raceProcessorSearchOrder) {
	this.raceProcessorSearchOrder = raceProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.rules.RaceProcessorSearchOrder getRaceProcessorSearchOrder() {
	return (this.raceProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given race processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race processor implementing the requested record.
     *
     *  @param raceProcessorSearchRecordType a race processor search record
     *         type
     *  @return the race processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorSearchRecord getRaceProcessorSearchRecord(org.osid.type.Type raceProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.rules.records.RaceProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race processor search. 
     *
     *  @param raceProcessorSearchRecord race processor search record
     *  @param raceProcessorSearchRecordType raceProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceProcessorSearchRecord(org.osid.voting.rules.records.RaceProcessorSearchRecord raceProcessorSearchRecord, 
                                           org.osid.type.Type raceProcessorSearchRecordType) {

        addRecordType(raceProcessorSearchRecordType);
        this.records.add(raceProcessorSearchRecord);        
        return;
    }
}
