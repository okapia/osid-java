//
// AbstractGraphBatchFormList.java
//
//     Implements an AbstractGraphBatchFormList adapter.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.batch.graphbatchform.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AbstractGraphBatchFormList adapter template.
 */

public abstract class AbstractAdapterGraphBatchFormList
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidList
    implements org.osid.topology.batch.GraphBatchFormList {


    /**
     *  Constructs a new {@code AbstractAdapterGraphBatchFormList}.
     *
     *  @param list
     *  @throws org.osid.NullArgumentException {@code list} is 
     *          {@code null}
     */

    protected AbstractAdapterGraphBatchFormList(org.osid.OsidList list) {
        super(list);
        return;
    }


    /**
     *  Gets the next {@code GraphBatchForm} in this list. 
     *
     *  @return the next {@code GraphBatchForm} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code GraphBatchForm} is available before calling this
     *          method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public abstract org.osid.topology.batch.GraphBatchForm getNextGraphBatchForm()
        throws org.osid.OperationFailedException;


    /**
     *  Gets the next set of {@code GraphBatchForm} elements in this list
     *  which must be less than or equal to the return from {@code
     *  available()}.
     *
     *  @param n the number of {@code GraphBatchForm} elements requested
     *          which must be less than or equal to {@code
     *          available()}
     *  @return an array of {@code GraphBatchForm} elements. The
     *          length of the array is less than or equal to the
     *          number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list has been closed
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.InvalidArgumentException cardinal value is negative
     */

    @OSID @Override
    public org.osid.topology.batch.GraphBatchForm[] getNextGraphBatchForms(long n)
        throws org.osid.OperationFailedException {
        
        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        org.osid.topology.batch.GraphBatchForm[] ret = new org.osid.topology.batch.GraphBatchForm[(int) n];

        for (int i = 0; i < n; i++) {
            ret[i] = getNextGraphBatchForm();
        }

        return (ret);
    }
}
