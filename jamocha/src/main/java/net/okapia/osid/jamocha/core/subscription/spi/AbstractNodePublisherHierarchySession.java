//
// AbstractNodePublisherHierarchySession.java
//
//     Defines a Publisher hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a publisher hierarchy session for delivering a hierarchy
 *  of publishers using the PublisherNode interface.
 */

public abstract class AbstractNodePublisherHierarchySession
    extends net.okapia.osid.jamocha.subscription.spi.AbstractPublisherHierarchySession
    implements org.osid.subscription.PublisherHierarchySession {

    private java.util.Collection<org.osid.subscription.PublisherNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root publisher <code> Ids </code> in this hierarchy.
     *
     *  @return the root publisher <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootPublisherIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.subscription.publishernode.PublisherNodeToIdList(this.roots));
    }


    /**
     *  Gets the root publishers in the publisher hierarchy. A node
     *  with no parents is an orphan. While all publisher <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root publishers 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getRootPublishers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.subscription.publishernode.PublisherNodeToPublisherList(new net.okapia.osid.jamocha.subscription.publishernode.ArrayPublisherNodeList(this.roots)));
    }


    /**
     *  Adds a root publisher node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootPublisher(org.osid.subscription.PublisherNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root publisher nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootPublishers(java.util.Collection<org.osid.subscription.PublisherNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root publisher node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootPublisher(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.subscription.PublisherNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Publisher </code> has any parents. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @return <code> true </code> if the publisher has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentPublishers(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getPublisherNode(publisherId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  publisher.
     *
     *  @param  id an <code> Id </code> 
     *  @param  publisherId the <code> Id </code> of a publisher 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> publisherId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> publisherId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfPublisher(org.osid.id.Id id, org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.subscription.PublisherNodeList parents = getPublisherNode(publisherId).getParentPublisherNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextPublisherNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given publisher. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @return the parent <code> Ids </code> of the publisher 
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentPublisherIds(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.subscription.publisher.PublisherToIdList(getParentPublishers(publisherId)));
    }


    /**
     *  Gets the parents of the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> to query 
     *  @return the parents of the publisher 
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getParentPublishers(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.subscription.publishernode.PublisherNodeToPublisherList(getPublisherNode(publisherId).getParentPublisherNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  publisher.
     *
     *  @param  id an <code> Id </code> 
     *  @param  publisherId the Id of a publisher 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> publisherId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfPublisher(org.osid.id.Id id, org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfPublisher(id, publisherId)) {
            return (true);
        }

        try (org.osid.subscription.PublisherList parents = getParentPublishers(publisherId)) {
            while (parents.hasNext()) {
                if (isAncestorOfPublisher(id, parents.getNextPublisher().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a publisher has any children. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @return <code> true </code> if the <code> publisherId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildPublishers(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPublisherNode(publisherId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  publisher.
     *
     *  @param  id an <code> Id </code> 
     *  @param publisherId the <code> Id </code> of a 
     *         publisher
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> publisherId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> publisherId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfPublisher(org.osid.id.Id id, org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfPublisher(publisherId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  publisher.
     *
     *  @param  publisherId the <code> Id </code> to query 
     *  @return the children of the publisher 
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildPublisherIds(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.subscription.publisher.PublisherToIdList(getChildPublishers(publisherId)));
    }


    /**
     *  Gets the children of the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> to query 
     *  @return the children of the publisher 
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getChildPublishers(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.subscription.publishernode.PublisherNodeToPublisherList(getPublisherNode(publisherId).getChildPublisherNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  publisher.
     *
     *  @param  id an <code> Id </code> 
     *  @param publisherId the <code> Id </code> of a 
     *         publisher
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> publisherId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfPublisher(org.osid.id.Id id, org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfPublisher(publisherId, id)) {
            return (true);
        }

        try (org.osid.subscription.PublisherList children = getChildPublishers(publisherId)) {
            while (children.hasNext()) {
                if (isDescendantOfPublisher(id, children.getNextPublisher().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  publisher.
     *
     *  @param  publisherId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified publisher node 
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getPublisherNodeIds(org.osid.id.Id publisherId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.subscription.publishernode.PublisherNodeToNode(getPublisherNode(publisherId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given publisher.
     *
     *  @param  publisherId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified publisher node 
     *  @throws org.osid.NotFoundException <code> publisherId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> publisherId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherNode getPublisherNodes(org.osid.id.Id publisherId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPublisherNode(publisherId));
    }


    /**
     *  Closes this <code>PublisherHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a publisher node.
     *
     *  @param publisherId the id of the publisher node
     *  @throws org.osid.NotFoundException <code>publisherId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>publisherId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.subscription.PublisherNode getPublisherNode(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(publisherId, "publisher Id");
        for (org.osid.subscription.PublisherNode publisher : this.roots) {
            if (publisher.getId().equals(publisherId)) {
                return (publisher);
            }

            org.osid.subscription.PublisherNode r = findPublisher(publisher, publisherId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(publisherId + " is not found");
    }


    protected org.osid.subscription.PublisherNode findPublisher(org.osid.subscription.PublisherNode node, 
                                                                org.osid.id.Id publisherId) 
	throws org.osid.OperationFailedException {

        try (org.osid.subscription.PublisherNodeList children = node.getChildPublisherNodes()) {
            while (children.hasNext()) {
                org.osid.subscription.PublisherNode publisher = children.getNextPublisherNode();
                if (publisher.getId().equals(publisherId)) {
                    return (publisher);
                }
                
                publisher = findPublisher(publisher, publisherId);
                if (publisher != null) {
                    return (publisher);
                }
            }
        }

        return (null);
    }
}
