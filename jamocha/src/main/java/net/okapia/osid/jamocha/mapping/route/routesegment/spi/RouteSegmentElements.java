//
// RouteSegmentElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.routesegment.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RouteSegmentElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the RouteSegmentElement Id.
     *
     *  @return the route segment element Id
     */

    public static org.osid.id.Id getRouteSegmentEntityId() {
        return (makeEntityId("osid.mapping.route.RouteSegment"));
    }


    /**
     *  Gets the StartingInstructions element Id.
     *
     *  @return the StartingInstructions element Id
     */

    public static org.osid.id.Id getStartingInstructions() {
        return (makeElementId("osid.mapping.route.routesegment.StartingInstructions"));
    }


    /**
     *  Gets the EndingInstructions element Id.
     *
     *  @return the EndingInstructions element Id
     */

    public static org.osid.id.Id getEndingInstructions() {
        return (makeElementId("osid.mapping.route.routesegment.EndingInstructions"));
    }


    /**
     *  Gets the Distance element Id.
     *
     *  @return the Distance element Id
     */

    public static org.osid.id.Id getDistance() {
        return (makeElementId("osid.mapping.route.routesegment.Distance"));
    }


    /**
     *  Gets the ETA element Id.
     *
     *  @return the ETA element Id
     */

    public static org.osid.id.Id getETA() {
        return (makeElementId("osid.mapping.route.routesegment.ETA"));
    }


    /**
     *  Gets the PathId element Id.
     *
     *  @return the PathId element Id
     */

    public static org.osid.id.Id getPathId() {
        return (makeElementId("osid.mapping.route.routesegment.PathId"));
    }


    /**
     *  Gets the Path element Id.
     *
     *  @return the Path element Id
     */

    public static org.osid.id.Id getPath() {
        return (makeElementId("osid.mapping.route.routesegment.Path"));
    }


    /**
     *  Gets the RouteId element Id.
     *
     *  @return the RouteId element Id
     */

    public static org.osid.id.Id getRouteId() {
        return (makeQueryElementId("osid.mapping.route.routesegment.RouteId"));
    }


    /**
     *  Gets the Route element Id.
     *
     *  @return the Route element Id
     */

    public static org.osid.id.Id getRoute() {
        return (makeQueryElementId("osid.mapping.route.routesegment.Route"));
    }
}
