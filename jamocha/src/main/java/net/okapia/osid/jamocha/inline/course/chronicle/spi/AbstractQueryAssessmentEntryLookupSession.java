//
// AbstractQueryAssessmentEntryLookupSession.java
//
//    An inline adapter that maps an AssessmentEntryLookupSession to
//    an AssessmentEntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AssessmentEntryLookupSession to
 *  an AssessmentEntryQuerySession.
 */

public abstract class AbstractQueryAssessmentEntryLookupSession
    extends net.okapia.osid.jamocha.course.chronicle.spi.AbstractAssessmentEntryLookupSession
    implements org.osid.course.chronicle.AssessmentEntryLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.course.chronicle.AssessmentEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAssessmentEntryLookupSession.
     *
     *  @param querySession the underlying assessment entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAssessmentEntryLookupSession(org.osid.course.chronicle.AssessmentEntryQuerySession querySession) {
        nullarg(querySession, "assessment entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>AssessmentEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentEntries() {
        return (this.session.canSearchAssessmentEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessment entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only assessment entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAssessmentEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All assessment entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAssessmentEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>AssessmentEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AssessmentEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentEntryId <code>Id</code> of the
     *          <code>AssessmentEntry</code>
     *  @return the assessment entry
     *  @throws org.osid.NotFoundException <code>assessmentEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntry getAssessmentEntry(org.osid.id.Id assessmentEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchId(assessmentEntryId, true);
        org.osid.course.chronicle.AssessmentEntryList assessmentEntries = this.session.getAssessmentEntriesByQuery(query);
        if (assessmentEntries.hasNext()) {
            return (assessmentEntries.getNextAssessmentEntry());
        } 
        
        throw new org.osid.NotFoundException(assessmentEntryId + " not found");
    }


    /**
     *  Gets an <code>AssessmentEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, assessment entries are returned that are currently effective.
     *  In any effective mode, effective assessment entries and those currently expired
     *  are returned.
     *
     *  @param  assessmentEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByIds(org.osid.id.IdList assessmentEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();

        try (org.osid.id.IdList ids = assessmentEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentEntryList</code> corresponding to the given
     *  assessment entry genus <code>Type</code> which does not include
     *  assessment entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently effective.
     *  In any effective mode, effective assessment entries and those currently expired
     *  are returned.
     *
     *  @param  assessmentEntryGenusType an assessmentEntry genus type 
     *  @return the returned <code>AssessmentEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByGenusType(org.osid.type.Type assessmentEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchGenusType(assessmentEntryGenusType, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentEntryList</code> corresponding to the given
     *  assessment entry genus <code>Type</code> and include any additional
     *  assessment entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentEntryGenusType an assessmentEntry genus type 
     *  @return the returned <code>AssessmentEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByParentGenusType(org.osid.type.Type assessmentEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchParentGenusType(assessmentEntryGenusType, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentEntryList</code> containing the given
     *  assessment entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentEntryRecordType an assessmentEntry record type 
     *  @return the returned <code>AssessmentEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByRecordType(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchRecordType(assessmentEntryRecordType, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *  
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AssessmentEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }
        

    /**
     *  Gets a list of assessment entries corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>AssessmentEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets a list of assessment entries corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AssessmentEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets a list of assessment entries corresponding to an assessment
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  assessmentId the <code>Id</code> of the assessment
     *  @return the returned <code>AssessmentEntryList</code>
     *  @throws org.osid.NullArgumentException <code>assessmentId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForAssessment(org.osid.id.Id assessmentId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchAssessmentId(assessmentId, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets a list of assessment entries corresponding to an assessment
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  assessmentId the <code>Id</code> of the assessment
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AssessmentEntryList</code>
     *  @throws org.osid.NullArgumentException <code>assessmentId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForAssessmentOnDate(org.osid.id.Id assessmentId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchAssessmentId(assessmentId, true);
        query.matchDate(from, to, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets a list of assessment entries corresponding to student and assessment
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  assessmentId the <code>Id</code> of the assessment
     *  @return the returned <code>AssessmentEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudentAndAssessment(org.osid.id.Id resourceId,
                                                                                                     org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchAssessmentId(assessmentId, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets a list of assessment entries corresponding to student and assessment
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  assessmentId the <code>Id</code> of the assessment
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AssessmentEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>assessmentId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudentAndAssessmentOnDate(org.osid.id.Id resourceId,
                                                                                                           org.osid.id.Id assessmentId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchAssessmentId(assessmentId, true);
        query.matchDate(from, to, true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }

    
    /**
     *  Gets all <code>AssessmentEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>AssessmentEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.chronicle.AssessmentEntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAssessmentEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.chronicle.AssessmentEntryQuery getQuery() {
        org.osid.course.chronicle.AssessmentEntryQuery query = this.session.getAssessmentEntryQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
