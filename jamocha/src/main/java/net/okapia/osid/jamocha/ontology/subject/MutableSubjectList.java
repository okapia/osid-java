//
// MutableSubjectList.java
//
//     Implements a SubjectList. This list allows Subjects to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.subject;


/**
 *  <p>Implements a SubjectList. This list allows Subjects to be
 *  added after this subject has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this subject must
 *  invoke <code>eol()</code> when there are no more subjects to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>SubjectList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more subjects are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more subjects to be added.</p>
 */

public final class MutableSubjectList
    extends net.okapia.osid.jamocha.ontology.subject.spi.AbstractMutableSubjectList
    implements org.osid.ontology.SubjectList {


    /**
     *  Creates a new empty <code>MutableSubjectList</code>.
     */

    public MutableSubjectList() {
        super();
    }


    /**
     *  Creates a new <code>MutableSubjectList</code>.
     *
     *  @param subject a <code>Subject</code>
     *  @throws org.osid.NullArgumentException <code>subject</code>
     *          is <code>null</code>
     */

    public MutableSubjectList(org.osid.ontology.Subject subject) {
        super(subject);
        return;
    }


    /**
     *  Creates a new <code>MutableSubjectList</code>.
     *
     *  @param array an array of subjects
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableSubjectList(org.osid.ontology.Subject[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableSubjectList</code>.
     *
     *  @param collection a java.util.Collection of subjects
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableSubjectList(java.util.Collection<org.osid.ontology.Subject> collection) {
        super(collection);
        return;
    }
}
