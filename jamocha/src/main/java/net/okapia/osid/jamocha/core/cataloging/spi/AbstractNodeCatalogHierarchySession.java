//
// AbstractNodeCatalogHierarchySession.java
//
//     Defines a Catalog hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.cataloging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a catalog hierarchy session for delivering a hierarchy
 *  of catalogs using the CatalogNode interface.
 */

public abstract class AbstractNodeCatalogHierarchySession
    extends net.okapia.osid.jamocha.cataloging.spi.AbstractCatalogHierarchySession
    implements org.osid.cataloging.CatalogHierarchySession {

    private java.util.Collection<org.osid.cataloging.CatalogNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root catalog <code> Ids </code> in this hierarchy.
     *
     *  @return the root catalog <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootCatalogIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.CatalogNodeToIdList(this.roots));
    }


    /**
     *  Gets the root catalogs in the catalog hierarchy. A node
     *  with no parents is an orphan. While all catalog <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root catalogs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getRootCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.CatalogNodeToCatalogList(new net.okapia.osid.jamocha.cataloging.catalognode.ArrayCatalogNodeList(this.roots)));
    }


    /**
     *  Adds a root catalog node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootCatalog(org.osid.cataloging.CatalogNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root catalog nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootCatalogs(java.util.Collection<org.osid.cataloging.CatalogNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root catalog node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootCatalog(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.cataloging.CatalogNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Catalog </code> has any parents. 
     *
     *  @param  catalogId a catalog <code> Id </code> 
     *  @return <code> true </code> if the catalog has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentCatalogs(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getCatalogNode(catalogId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param  catalogId the <code> Id </code> of a catalog 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> catalogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> catalogId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfCatalog(org.osid.id.Id id, org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.cataloging.CatalogNodeList parents = getCatalogNode(catalogId).getParentCatalogNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextCatalogNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given catalog. 
     *
     *  @param  catalogId a catalog <code> Id </code> 
     *  @return the parent <code> Ids </code> of the catalog 
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentCatalogIds(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalog.CatalogToIdList(getParentCatalogs(catalogId)));
    }


    /**
     *  Gets the parents of the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> to query 
     *  @return the parents of the catalog 
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getParentCatalogs(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.CatalogNodeToCatalogList(getCatalogNode(catalogId).getParentCatalogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param  catalogId the Id of a catalog 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> catalogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfCatalog(org.osid.id.Id id, org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCatalog(id, catalogId)) {
            return (true);
        }

        try (org.osid.cataloging.CatalogList parents = getParentCatalogs(catalogId)) {
            while (parents.hasNext()) {
                if (isAncestorOfCatalog(id, parents.getNextCatalog().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a catalog has any children. 
     *
     *  @param  catalogId a catalog <code> Id </code> 
     *  @return <code> true </code> if the <code> catalogId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildCatalogs(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCatalogNode(catalogId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param catalogId the <code> Id </code> of a 
     *         catalog
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> catalogId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> catalogId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfCatalog(org.osid.id.Id id, org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfCatalog(catalogId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  catalog.
     *
     *  @param  catalogId the <code> Id </code> to query 
     *  @return the children of the catalog 
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildCatalogIds(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalog.CatalogToIdList(getChildCatalogs(catalogId)));
    }


    /**
     *  Gets the children of the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> to query 
     *  @return the children of the catalog 
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getChildCatalogs(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.CatalogNodeToCatalogList(getCatalogNode(catalogId).getChildCatalogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  catalog.
     *
     *  @param  id an <code> Id </code> 
     *  @param catalogId the <code> Id </code> of a 
     *         catalog
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> catalogId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfCatalog(org.osid.id.Id id, org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCatalog(catalogId, id)) {
            return (true);
        }

        try (org.osid.cataloging.CatalogList children = getChildCatalogs(catalogId)) {
            while (children.hasNext()) {
                if (isDescendantOfCatalog(id, children.getNextCatalog().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  catalog.
     *
     *  @param  catalogId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified catalog node 
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getCatalogNodeIds(org.osid.id.Id catalogId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.CatalogNodeToNode(getCatalogNode(catalogId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given catalog.
     *
     *  @param  catalogId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified catalog node 
     *  @throws org.osid.NotFoundException <code> catalogId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> catalogId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogNode getCatalogNodes(org.osid.id.Id catalogId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCatalogNode(catalogId));
    }


    /**
     *  Closes this <code>CatalogHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a catalog node.
     *
     *  @param catalogId the id of the catalog node
     *  @throws org.osid.NotFoundException <code>catalogId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>catalogId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.cataloging.CatalogNode getCatalogNode(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(catalogId, "catalog Id");
        for (org.osid.cataloging.CatalogNode catalog : this.roots) {
            if (catalog.getId().equals(catalogId)) {
                return (catalog);
            }

            org.osid.cataloging.CatalogNode r = findCatalog(catalog, catalogId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(catalogId + " is not found");
    }


    protected org.osid.cataloging.CatalogNode findCatalog(org.osid.cataloging.CatalogNode node, 
                                                          org.osid.id.Id catalogId) 
        throws org.osid.OperationFailedException {

        try (org.osid.cataloging.CatalogNodeList children = node.getChildCatalogNodes()) {
            while (children.hasNext()) {
                org.osid.cataloging.CatalogNode catalog = children.getNextCatalogNode();
                if (catalog.getId().equals(catalogId)) {
                    return (catalog);
                }
                
                catalog = findCatalog(catalog, catalogId);
                if (catalog != null) {
                    return (catalog);
                }
            }
        }

        return (null);
    }
}
