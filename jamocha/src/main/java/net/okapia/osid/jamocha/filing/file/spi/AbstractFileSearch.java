//
// AbstractFileSearch.java
//
//     A template for making a File Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.file.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing file searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractFileSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.filing.FileSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.filing.records.FileSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.filing.FileSearchOrder fileSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of files. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  fileIds list of files
     *  @throws org.osid.NullArgumentException
     *          <code>fileIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongFiles(org.osid.id.IdList fileIds) {
        while (fileIds.hasNext()) {
            try {
                this.ids.add(fileIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongFiles</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of file Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getFileIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  fileSearchOrder file search order 
     *  @throws org.osid.NullArgumentException
     *          <code>fileSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>fileSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderFileResults(org.osid.filing.FileSearchOrder fileSearchOrder) {
	this.fileSearchOrder = fileSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.filing.FileSearchOrder getFileSearchOrder() {
	return (this.fileSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given file search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a file implementing the requested record.
     *
     *  @param fileSearchRecordType a file search record
     *         type
     *  @return the file search record
     *  @throws org.osid.NullArgumentException
     *          <code>fileSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fileSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.FileSearchRecord getFileSearchRecord(org.osid.type.Type fileSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.filing.records.FileSearchRecord record : this.records) {
            if (record.implementsRecordType(fileSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fileSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this file search. 
     *
     *  @param fileSearchRecord file search record
     *  @param fileSearchRecordType file search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFileSearchRecord(org.osid.filing.records.FileSearchRecord fileSearchRecord, 
                                           org.osid.type.Type fileSearchRecordType) {

        addRecordType(fileSearchRecordType);
        this.records.add(fileSearchRecord);        
        return;
    }
}
