//
// AbstractImmutableContainable
//
//     Defines an immutable wrapper for an OSID Object.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an immutable wrapper for an OSID Object.
 */

public abstract class AbstractImmutableContainable
    implements org.osid.Containable {

    private final org.osid.Containable containable;


    /**
     *  Constructs a new <code>AbstractImmutableContainable</code>.
     *
     *  @param containable
     *  @throws org.osid.NullArgumentException <code>containable</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableContainable(org.osid.Containable containable) {
        nullarg(containable, "containable");
        this.containable = containable;
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an containable to compare
     *  @return <code> true </code> if the given containable is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public boolean equals(Object obj) {
        return (this.containable.equals(obj));
    }


    /**
     *  Returns a hash code value for this <code>Containable</code>
     *  based on the <code>Id</code>.
     *
     *  @return a hash code value for this containable
     */

    @Override
    public int hashCode() {
        return (this.containable.hashCode());
    }


    /**
     *  Returns a string representation of this Containable.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        return (this.containable.toString());
    }
}
