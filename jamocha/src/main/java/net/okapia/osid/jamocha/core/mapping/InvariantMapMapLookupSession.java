//
// InvariantMapMapLookupSession
//
//    Implements a Map lookup service backed by a fixed collection of
//    maps.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping;


/**
 *  Implements a Map lookup service backed by a fixed
 *  collection of maps. The maps are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapMapLookupSession
    extends net.okapia.osid.jamocha.core.mapping.spi.AbstractMapMapLookupSession
    implements org.osid.mapping.MapLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapMapLookupSession</code> with no
     *  maps.
     */

    public InvariantMapMapLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMapLookupSession</code> with a single
     *  map.
     *  
     *  @throws org.osid.NullArgumentException {@code map}
     *          is <code>null</code>
     */

    public InvariantMapMapLookupSession(org.osid.mapping.Map map) {
        putMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMapLookupSession</code> using an array
     *  of maps.
     *  
     *  @throws org.osid.NullArgumentException {@code maps}
     *          is <code>null</code>
     */

    public InvariantMapMapLookupSession(org.osid.mapping.Map[] maps) {
        putMaps(maps);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMapLookupSession</code> using a
     *  collection of maps.
     *
     *  @throws org.osid.NullArgumentException {@code maps}
     *          is <code>null</code>
     */

    public InvariantMapMapLookupSession(java.util.Collection<? extends org.osid.mapping.Map> maps) {
        putMaps(maps);
        return;
    }
}
