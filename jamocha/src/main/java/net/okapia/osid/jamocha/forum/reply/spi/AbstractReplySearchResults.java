//
// AbstractReplySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractReplySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.forum.ReplySearchResults {

    private org.osid.forum.ReplyList replies;
    private final org.osid.forum.ReplyQueryInspector inspector;
    private final java.util.Collection<org.osid.forum.records.ReplySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractReplySearchResults.
     *
     *  @param replies the result set
     *  @param replyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>replies</code>
     *          or <code>replyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractReplySearchResults(org.osid.forum.ReplyList replies,
                                            org.osid.forum.ReplyQueryInspector replyQueryInspector) {
        nullarg(replies, "replies");
        nullarg(replyQueryInspector, "reply query inspectpr");

        this.replies = replies;
        this.inspector = replyQueryInspector;

        return;
    }


    /**
     *  Gets the reply list resulting from a search.
     *
     *  @return a reply list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.forum.ReplyList getReplies() {
        if (this.replies == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.forum.ReplyList replies = this.replies;
        this.replies = null;
	return (replies);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.forum.ReplyQueryInspector getReplyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  reply search record <code> Type. </code> This method must
     *  be used to retrieve a reply implementing the requested
     *  record.
     *
     *  @param replySearchRecordType a reply search 
     *         record type 
     *  @return the reply search
     *  @throws org.osid.NullArgumentException
     *          <code>replySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(replySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplySearchResultsRecord getReplySearchResultsRecord(org.osid.type.Type replySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.forum.records.ReplySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(replySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(replySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record reply search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addReplyRecord(org.osid.forum.records.ReplySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "reply record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
