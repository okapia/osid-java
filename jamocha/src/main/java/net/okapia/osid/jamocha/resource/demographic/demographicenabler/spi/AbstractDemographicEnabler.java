//
// AbstractDemographicEnabler.java
//
//     Defines a DemographicEnabler.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographicenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>DemographicEnabler</code>.
 */

public abstract class AbstractDemographicEnabler
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnabler
    implements org.osid.resource.demographic.DemographicEnabler {

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicEnablerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this demographicEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  demographicEnablerRecordType a demographic enabler record type 
     *  @return <code>true</code> if the demographicEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type demographicEnablerRecordType) {
        for (org.osid.resource.demographic.records.DemographicEnablerRecord record : this.records) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>DemographicEnabler</code> record <code>Type</code>.
     *
     *  @param demographicEnablerRecordType the demographic enabler
     *         record type
     *  @return the demographic enabler record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerRecord getDemographicEnablerRecord(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicEnablerRecord record : this.records) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this demographic enabler. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param demographicEnablerRecord the demographic enabler record
     *  @param demographicEnablerRecordType demographic enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecord</code> or
     *          <code>demographicEnablerRecordTypedemographicEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addDemographicEnablerRecord(org.osid.resource.demographic.records.DemographicEnablerRecord demographicEnablerRecord, 
                                               org.osid.type.Type demographicEnablerRecordType) {
        
        nullarg(demographicEnablerRecord, "demographic enabler record");
        addRecordType(demographicEnablerRecordType);
        this.records.add(demographicEnablerRecord);
        
        return;
    }
}
