//
// AbstractQueryStockLookupSession.java
//
//    An inline adapter that maps a StockLookupSession to
//    a StockQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a StockLookupSession to
 *  a StockQuerySession.
 */

public abstract class AbstractQueryStockLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractStockLookupSession
    implements org.osid.inventory.StockLookupSession {

    private final org.osid.inventory.StockQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryStockLookupSession.
     *
     *  @param querySession the underlying stock query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryStockLookupSession(org.osid.inventory.StockQuerySession querySession) {
        nullarg(querySession, "stock query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Warehouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform <code>Stock</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStocks() {
        return (this.session.canSearchStocks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include stocks in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the <code>Stock</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Stock</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Stock</code> and
     *  retained for compatibility.
     *
     *  @param  stockId <code>Id</code> of the
     *          <code>Stock</code>
     *  @return the stock
     *  @throws org.osid.NotFoundException <code>stockId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stockId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.StockQuery query = getQuery();
        query.matchId(stockId, true);
        org.osid.inventory.StockList stocks = this.session.getStocksByQuery(query);
        if (stocks.hasNext()) {
            return (stocks.getNextStock());
        } 
        
        throw new org.osid.NotFoundException(stockId + " not found");
    }


    /**
     *  Gets a <code>StockList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stocks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Stocks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  stockIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stockIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByIds(org.osid.id.IdList stockIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.StockQuery query = getQuery();

        try (org.osid.id.IdList ids = stockIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getStocksByQuery(query));
    }


    /**
     *  Gets a <code>StockList</code> corresponding to the given
     *  stock genus <code>Type</code> which does not include
     *  stocks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stockGenusType a stock genus type 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByGenusType(org.osid.type.Type stockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.StockQuery query = getQuery();
        query.matchGenusType(stockGenusType, true);
        return (this.session.getStocksByQuery(query));
    }


    /**
     *  Gets a <code>StockList</code> corresponding to the given
     *  stock genus <code>Type</code> and include any additional
     *  stocks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stockGenusType a stock genus type 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByParentGenusType(org.osid.type.Type stockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.StockQuery query = getQuery();
        query.matchParentGenusType(stockGenusType, true);
        return (this.session.getStocksByQuery(query));
    }


    /**
     *  Gets a <code>StockList</code> containing the given
     *  stock record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stockRecordType a stock record type 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByRecordType(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.StockQuery query = getQuery();
        query.matchRecordType(stockRecordType, true);
        return (this.session.getStocksByQuery(query));
    }


    /**
     *  Gets a <code> StockList </code> containing for the given SKU. In 
     *  plenary mode, the returned list contains all known stocks or an error 
     *  results. Otherwise, the returned list may contain only those stocks 
     *  that are accessible through this session. 
     *
     *  @param  sku a stock keeping unit 
     *  @return the returned <code> Stock </code> list 
     *  @throws org.osid.NullArgumentException <code> sku </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksBySKU(String sku)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.StockQuery query = getQuery();
        query.matchSKU(sku, getStringMatchType(), true);
        return (this.session.getStocksByQuery(query));
    }

    
    /**
     *  Gets all <code>Stocks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Stocks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.StockQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getStocksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.inventory.StockQuery getQuery() {
        org.osid.inventory.StockQuery query = this.session.getStockQuery();
        return (query);
    }


    /**
     *  Gets the string match type for use in string queries.
     *
     *  @return a type
     */

    protected org.osid.type.Type getStringMatchType() {
        return (net.okapia.osid.primordium.types.search.StringMatchTypes.EXACT.getType());
    }
}
