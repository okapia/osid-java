//
// AbstractCourseOffering.java
//
//     Defines a CourseOffering.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>CourseOffering</code>.
 */

public abstract class AbstractCourseOffering
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.CourseOffering {

    private org.osid.course.Course course;
    private org.osid.course.Term term;
    private org.osid.locale.DisplayText title;
    private String number;

    private boolean hasSponsors          = false;
    private boolean isGraded             = false;
    private boolean requiresRegistration = false;

    private long minimumSeats;
    private long maximumSeats;
    private String url;

    private org.osid.locale.DisplayText scheduleInfo;
    private org.osid.calendaring.Event event;

    private final java.util.Collection<org.osid.grading.Grade> credits = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> instructors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.GradeSystem> gradingOptions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseOfferingRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the canonical course <code> Id </code> associated with this 
     *  course offering. 
     *
     *  @return the course <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.course.getId());
    }


    /**
     *  Gets the canonical course associated with this course offering. 
     *
     *  @return the course 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.course);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    protected void setCourse(org.osid.course.Course course) {
        nullarg(course, "course");
        this.course = course;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term </code> of this 
     *  offering. 
     *
     *  @return the <code> Term </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.term.getId());
    }


    /**
     *  Gets the <code> Term </code> of this offering. 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.term);
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    protected void setTerm(org.osid.course.Term term) {
        nullarg(term, "term");
        this.term = term;
        return;
    }


    /**
     *  Gets the formal title of this course. It may be the same as the 
     *  display name or it may be used to more formally label the course. A 
     *  display name might be Physics 102 where the title is Introduction to 
     *  Electromagentism. 
     *
     *  @return the course title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Gets the course number which is a label generally used to
     *  index the course offering in a catalog, such as T101 or
     *  16.004.
     *
     *  @return the course number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.number);
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setNumber(String number) {
        nullarg(number, "course number");
        this.number = number;
        return;
    }


    /**
     *  Gets the instructor <code> Ids. </code> If each activity has its own 
     *  instructor, the headlining instructors may be returned. 
     *
     *  @return the instructor <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getInstructorIds() {
        try {
            org.osid.resource.ResourceList instructors = getInstructors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(instructors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the instructors. If each activity has its own instructor, the 
     *  headlining instructors may be returned. 
     *
     *  @return the sponsors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getInstructors()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.instructors));
    }


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @throws org.osid.NullArgumentException
     *          <code>instructor</code> is <code>null</code>
     */

    protected void addInstructor(org.osid.resource.Resource instructor) {
        nullarg(instructor, "instructor");
        this.instructors.add(instructor);
        return;
    }


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    protected void setInstructors(java.util.Collection<org.osid.resource.Resource> instructors) {
        nullarg(instructors, "instructors");

        this.instructors.clear();
        this.instructors.addAll(instructors);

        return;
    }


    /**
     *  Tests if this course has a sponsor. 
     *
     *  @return <code> true </code> if this course has sponsors, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.hasSponsors);
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");

        this.sponsors.add(sponsor);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");

        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Gets the credit amount <code>Ids</code>.
     *
     *  @return the grading system <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCreditAmountIds() {
        try {
            org.osid.grading.GradeList credits = getCreditAmounts();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.grade.GradeToIdList(credits));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the credit amounts. 
     *
     *  @return the grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getCreditAmounts()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.grading.grade.ArrayGradeList(this.credits));
    }

   
    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void addCreditAmount(org.osid.grading.Grade amount) {
        nullarg(amount, "credit amount");
        this.credits.add(amount);
        return;
    }


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    protected void setCreditAmounts(java.util.Collection<org.osid.grading.Grade> amounts) {
        nullarg(amounts, "credit amounts");

        this.credits.clear();
        this.credits.addAll(amounts);

        return;
    }


    /**
     *  Tests if this course offering is graded. 
     *
     *  @return <code> true </code> if this course is graded, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.isGraded);
    }


    /**
     *  Gets the various grading option <code> Ids </code> available to 
     *  register in this course. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getGradingOptionIds() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        try {
            org.osid.grading.GradeSystemList gradingOptions = getGradingOptions();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.gradesystem.GradeSystemToIdList(gradingOptions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the various grading options available to register in this course. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradingOptions()
        throws org.osid.OperationFailedException {

        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.gradingOptions));
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    protected void addGradingOption(org.osid.grading.GradeSystem option) {
        nullarg(option, "grading option");

        this.gradingOptions.add(option);
        this.isGraded = true;

        return;
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException
     *          <code>options</code> is <code>null</code>
     */

    protected void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        nullarg(options, "grading options");

        this.gradingOptions.clear();
        this.gradingOptions.addAll(options);
        this.isGraded = true;

        return;
    }


    /**
     *  Tests if this course offering requires advanced registration. 
     *
     *  @return <code> true </code> if this course requires advance 
     *          registration, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean requiresRegistration() {
        return (this.requiresRegistration);
    }


    /**
     *  Sets the requires registration.
     *
     *  @param requires <code> true </code> if this course requires
     *          advance registration, <code> false </code> otherwise
     */

    protected void setRequiresRegistration(boolean requires) {
        this.requiresRegistration = requires;
        return;
    }


    /**
     *  Gets the minimum number of students this offering can have. 
     *
     *  @return the minimum seats 
     */

    @OSID @Override
    public long getMinimumSeats() {
        return (this.minimumSeats);
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    protected void setMinimumSeats(long seats) {
        cardinalarg(seats, "minimum seats");
        this.minimumSeats = minimumSeats;
        return;
    }


    /**
     *  Tests if this course offering has limited seating. 
     *
     *  @return <code> true </code> if this has limietd seating
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isSeatingLimited() {
        return (this.maximumSeats > 0);
    }


    /**
     *  Gets the maximum number of students this offering can have.
     *
     *  @return the maximum seats 
     *  @throws org.osid.IllegalStateException <code>
     *          requiresRegistration() </code> is <code> false </code>
     */

    @OSID @Override
    public long getMaximumSeats() {
        if (!requiresRegistration()) {
            throw new org.osid.IllegalStateException("requiresRegistration() is false");
        }

        return (this.maximumSeats);
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @throws org.osid.NullArgumentException <code>seats</code> is
     *          <code>null</code>
     */

    protected void setMaximumSeats(long seats) {
        this.maximumSeats = seats;
        return;
    }


    /**
     *  Gets an external resource, such as a class web site,
     *  associated with this offering.
     *
     *  @return a URL string 
     */

    @OSID @Override
    public String getURL() {
        return (this.url);
    }


    /**
     *  Sets the url.
     *
     *  @param url the url
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    protected void setURL(String url) {
        this.url = url;
        return;
    }


    /**
     *  Gets the an informational string for the course offering schedule. 
     *
     *  @return the schedule info 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getScheduleInfo() {
        return (this.scheduleInfo);
    }


    /**
     *  Sets the schedule info.
     *
     *  @param info a schedule info
     *  @throws org.osid.NullArgumentException
     *          <code>info</code> is <code>null</code>
     */

    protected void setScheduleInfo(org.osid.locale.DisplayText info) {
        nullarg(info, "schedule info");
        this.scheduleInfo = info;
        return;
    }


    /**
     *  Tests if a calendaring event is available for this course
     *  offering with the schedule details.
     *
     *  @return <code> true </code> if this course is graded, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean hasEvent() {
        return (this.event != null);
    }


    /**
     *  Gets the calendaring event <code> Id. </code> 
     *
     *  @return the event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEvent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEventId() {
        if (!hasEvent()) {
            throw new org.osid.IllegalStateException("hasEvent() is false");
        }

        return (this.event.getId());
    }


    /**
     *  Gets the calendaring event with the schedule details. 
     *
     *  @return an event 
     *  @throws org.osid.IllegalStateException <code> hasEvent()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent()
        throws org.osid.OperationFailedException {

        if (!hasEvent()) {
            throw new org.osid.IllegalStateException("hasEvent() is false");
        }

        return (this.event);
    }


    /**
     *  Sets the event.
     *
     *  @param event the event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    protected void setEvent(org.osid.calendaring.Event event) {
        nullarg(event, "event");
        this.event = event;
        return;
    }


    /**
     *  Tests if this courseOffering supports the given record
     *  <code>Type</code>.
     *
     *  @param  courseOfferingRecordType a course offering record type 
     *  @return <code>true</code> if the courseOfferingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseOfferingRecordType) {
        for (org.osid.course.records.CourseOfferingRecord record : this.records) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>CourseOffering</code> record <code>Type</code>.
     *
     *  @param  courseOfferingRecordType the course offering record type 
     *  @return the course offering record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingRecord getCourseOfferingRecord(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseOfferingRecord record : this.records) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseOfferingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course offering. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseOfferingRecord the course offering record
     *  @param courseOfferingRecordType course offering record type
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecord</code> or
     *          <code>courseOfferingRecordTypecourseOffering</code> is
     *          <code>null</code>
     */
            
    protected void addCourseOfferingRecord(org.osid.course.records.CourseOfferingRecord courseOfferingRecord, 
                                           org.osid.type.Type courseOfferingRecordType) {

        nullarg(courseOfferingRecord, "course offering record");
        addRecordType(courseOfferingRecordType);
        this.records.add(courseOfferingRecord);
        
        return;
    }
}
