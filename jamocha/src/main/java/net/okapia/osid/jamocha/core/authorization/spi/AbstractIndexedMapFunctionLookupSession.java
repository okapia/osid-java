//
// AbstractIndexedMapFunctionLookupSession.java
//
//    A simple framework for providing a Function lookup service
//    backed by a fixed collection of functions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Function lookup service backed by a
 *  fixed collection of functions. The functions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some functions may be compatible
 *  with more types than are indicated through these function
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Functions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapFunctionLookupSession
    extends AbstractMapFunctionLookupSession
    implements org.osid.authorization.FunctionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authorization.Function> functionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Function>());
    private final MultiMap<org.osid.type.Type, org.osid.authorization.Function> functionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Function>());


    /**
     *  Makes a <code>Function</code> available in this session.
     *
     *  @param  function a function
     *  @throws org.osid.NullArgumentException <code>function<code> is
     *          <code>null</code>
     */

    @Override
    protected void putFunction(org.osid.authorization.Function function) {
        super.putFunction(function);

        this.functionsByGenus.put(function.getGenusType(), function);
        
        try (org.osid.type.TypeList types = function.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.functionsByRecord.put(types.getNextType(), function);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a function from this session.
     *
     *  @param functionId the <code>Id</code> of the function
     *  @throws org.osid.NullArgumentException <code>functionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeFunction(org.osid.id.Id functionId) {
        org.osid.authorization.Function function;
        try {
            function = getFunction(functionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.functionsByGenus.remove(function.getGenusType());

        try (org.osid.type.TypeList types = function.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.functionsByRecord.remove(types.getNextType(), function);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeFunction(functionId);
        return;
    }


    /**
     *  Gets a <code>FunctionList</code> corresponding to the given
     *  function genus <code>Type</code> which does not include
     *  functions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known functions or an error results. Otherwise,
     *  the returned list may contain only those functions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  functionGenusType a function genus type 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByGenusType(org.osid.type.Type functionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.function.ArrayFunctionList(this.functionsByGenus.get(functionGenusType)));
    }


    /**
     *  Gets a <code>FunctionList</code> containing the given
     *  function record <code>Type</code>. In plenary mode, the
     *  returned list contains all known functions or an error
     *  results. Otherwise, the returned list may contain only those
     *  functions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  functionRecordType a function record type 
     *  @return the returned <code>function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByRecordType(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.function.ArrayFunctionList(this.functionsByRecord.get(functionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.functionsByGenus.clear();
        this.functionsByRecord.clear();

        super.close();

        return;
    }
}
