//
// AbstractAssemblyLeaseQuery.java
//
//     A LeaseQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.squatting.lease.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A LeaseQuery that stores terms.
 */

public abstract class AbstractAssemblyLeaseQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.room.squatting.LeaseQuery,
               org.osid.room.squatting.LeaseQueryInspector,
               org.osid.room.squatting.LeaseSearchOrder {

    private final java.util.Collection<org.osid.room.squatting.records.LeaseQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.squatting.records.LeaseQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.squatting.records.LeaseSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyLeaseQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyLeaseQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to leases. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getRoomIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        getAssembler().clearTerms(getRoomIdColumn());
        return;
    }


    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (getAssembler().getIdTerms(getRoomIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the room. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRoom(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRoomColumn(), style);
        return;
    }


    /**
     *  Gets the RoomId column name.
     *
     * @return the column name
     */

    protected String getRoomIdColumn() {
        return ("room_id");
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a lease. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        getAssembler().clearTerms(getRoomColumn());
        return;
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Tests if a room search order is available. 
     *
     *  @return <code> true </code> if a room search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSearchOrder() {
        return (false);
    }


    /**
     *  Gets the room search order. 
     *
     *  @return the room search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchOrder getRoomSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRoomSearchOrder() is false");
    }


    /**
     *  Gets the Room column name.
     *
     * @return the column name
     */

    protected String getRoomColumn() {
        return ("room");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match rooms 
     *  assigned to leases. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTenantId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getTenantIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the tenant <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTenantIdTerms() {
        getAssembler().clearTerms(getTenantIdColumn());
        return;
    }


    /**
     *  Gets the tenant <code> Id </code> terms. 
     *
     *  @return the tenant <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTenantIdTerms() {
        return (getAssembler().getIdTerms(getTenantIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the owner. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTenant(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTenantColumn(), style);
        return;
    }


    /**
     *  Gets the TenantId column name.
     *
     * @return the column name
     */

    protected String getTenantIdColumn() {
        return ("tenant_id");
    }


    /**
     *  Tests if a tenant query is available. 
     *
     *  @return <code> true </code> if a tenant query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTenantQuery() {
        return (false);
    }


    /**
     *  Gets the query for a tenant resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsTenantQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getTenantQuery() {
        throw new org.osid.UnimplementedException("supportsTenantQuery() is false");
    }


    /**
     *  Clears the tenant terms. 
     */

    @OSID @Override
    public void clearTenantTerms() {
        getAssembler().clearTerms(getTenantColumn());
        return;
    }


    /**
     *  Gets the tenant terms. 
     *
     *  @return the tenant terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getTenantTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a tenant resource search order is available. 
     *
     *  @return <code> true </code> if an tenant search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTenantSearchOrder() {
        return (false);
    }


    /**
     *  Gets the tenant resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTenantSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getTenantSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTenantSearchOrder() is false");
    }


    /**
     *  Gets the Tenant column name.
     *
     * @return the column name
     */

    protected String getTenantColumn() {
        return ("tenant");
    }


    /**
     *  Sets the lease <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        getAssembler().clearTerms(getCampusIdColumn());
        return;
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (getAssembler().getIdTerms(getCampusIdColumn()));
    }


    /**
     *  Gets the CampusId column name.
     *
     * @return the column name
     */

    protected String getCampusIdColumn() {
        return ("campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        getAssembler().clearTerms(getCampusColumn());
        return;
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the Campus column name.
     *
     * @return the column name
     */

    protected String getCampusColumn() {
        return ("campus");
    }


    /**
     *  Tests if this lease supports the given record
     *  <code>Type</code>.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return <code>true</code> if the leaseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type leaseRecordType) {
        for (org.osid.room.squatting.records.LeaseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  leaseRecordType the lease record type 
     *  @return the lease query record 
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(leaseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseQueryRecord getLeaseQueryRecord(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.LeaseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(leaseRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  leaseRecordType the lease record type 
     *  @return the lease query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(leaseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseQueryInspectorRecord getLeaseQueryInspectorRecord(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.LeaseQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(leaseRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param leaseRecordType the lease record type
     *  @return the lease search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(leaseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseSearchOrderRecord getLeaseSearchOrderRecord(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.LeaseSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(leaseRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this lease. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param leaseQueryRecord the lease query record
     *  @param leaseQueryInspectorRecord the lease query inspector
     *         record
     *  @param leaseSearchOrderRecord the lease search order record
     *  @param leaseRecordType lease record type
     *  @throws org.osid.NullArgumentException
     *          <code>leaseQueryRecord</code>,
     *          <code>leaseQueryInspectorRecord</code>,
     *          <code>leaseSearchOrderRecord</code> or
     *          <code>leaseRecordTypelease</code> is
     *          <code>null</code>
     */
            
    protected void addLeaseRecords(org.osid.room.squatting.records.LeaseQueryRecord leaseQueryRecord, 
                                      org.osid.room.squatting.records.LeaseQueryInspectorRecord leaseQueryInspectorRecord, 
                                      org.osid.room.squatting.records.LeaseSearchOrderRecord leaseSearchOrderRecord, 
                                      org.osid.type.Type leaseRecordType) {

        addRecordType(leaseRecordType);

        nullarg(leaseQueryRecord, "lease query record");
        nullarg(leaseQueryInspectorRecord, "lease query inspector record");
        nullarg(leaseSearchOrderRecord, "lease search odrer record");

        this.queryRecords.add(leaseQueryRecord);
        this.queryInspectorRecords.add(leaseQueryInspectorRecord);
        this.searchOrderRecords.add(leaseSearchOrderRecord);
        
        return;
    }
}
