//
// MutableIndexedMapPackageLookupSession
//
//    Implements a Package lookup service backed by a collection of
//    packages indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements a Package lookup service backed by a collection of
 *  packages. The packages are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some packages may be compatible
 *  with more types than are indicated through these package
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of packages can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPackageLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractIndexedMapPackageLookupSession
    implements org.osid.installation.PackageLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapPackageLookupSession} with no packages.
     *
     *  @param depot the depot
     *  @throws org.osid.NullArgumentException {@code depot}
     *          is {@code null}
     */

      public MutableIndexedMapPackageLookupSession(org.osid.installation.Depot depot) {
        setDepot(depot);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPackageLookupSession} with a
     *  single package.
     *  
     *  @param depot the depot
     *  @param  pkg a single pkg
     *  @throws org.osid.NullArgumentException {@code depot} or
     *          {@code pkg} is {@code null}
     */

    public MutableIndexedMapPackageLookupSession(org.osid.installation.Depot depot,
                                                  org.osid.installation.Package pkg) {
        this(depot);
        putPackage(pkg);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPackageLookupSession} using an
     *  array of packages.
     *
     *  @param depot the depot
     *  @param  packages an array of packages
     *  @throws org.osid.NullArgumentException {@code depot} or
     *          {@code packages} is {@code null}
     */

    public MutableIndexedMapPackageLookupSession(org.osid.installation.Depot depot,
                                                  org.osid.installation.Package[] packages) {
        this(depot);
        putPackages(packages);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPackageLookupSession} using a
     *  collection of packages.
     *
     *  @param depot the depot
     *  @param  packages a collection of packages
     *  @throws org.osid.NullArgumentException {@code depot} or
     *          {@code packages} is {@code null}
     */

    public MutableIndexedMapPackageLookupSession(org.osid.installation.Depot depot,
                                                  java.util.Collection<? extends org.osid.installation.Package> packages) {

        this(depot);
        putPackages(packages);
        return;
    }
    

    /**
     *  Makes a {@code Package} available in this session.
     *
     *  @param  pkg a package
     *  @throws org.osid.NullArgumentException {@code pkg{@code  is
     *          {@code null}
     */

    @Override
    public void putPackage(org.osid.installation.Package pkg) {
        super.putPackage(pkg);
        return;
    }


    /**
     *  Makes an array of packages available in this session.
     *
     *  @param  packages an array of packages
     *  @throws org.osid.NullArgumentException {@code packages{@code 
     *          is {@code null}
     */

    @Override
    public void putPackages(org.osid.installation.Package[] packages) {
        super.putPackages(packages);
        return;
    }


    /**
     *  Makes collection of packages available in this session.
     *
     *  @param  packages a collection of packages
     *  @throws org.osid.NullArgumentException {@code pkg{@code  is
     *          {@code null}
     */

    @Override
    public void putPackages(java.util.Collection<? extends org.osid.installation.Package> packages) {
        super.putPackages(packages);
        return;
    }


    /**
     *  Removes a Package from this session.
     *
     *  @param pkgId the {@code Id} of the package
     *  @throws org.osid.NullArgumentException {@code pkgId{@code  is
     *          {@code null}
     */

    @Override
    public void removePackage(org.osid.id.Id pkgId) {
        super.removePackage(pkgId);
        return;
    }    
}
