//
// AbstractPeriodQueryInspector.java
//
//     A template for making a PeriodQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.period.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for periods.
 */

public abstract class AbstractPeriodQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.billing.PeriodQueryInspector {

    private final java.util.Collection<org.osid.billing.records.PeriodQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the display label query terms. 
     *
     *  @return the display labelquery terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the open date query terms. 
     *
     *  @return the open date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getOpenDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the close date query terms. 
     *
     *  @return the close date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCloseDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the billing date query terms. 
     *
     *  @return the billing date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBillingDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the dur date query terms. 
     *
     *  @return the dur date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDueDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given period query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a period implementing the requested record.
     *
     *  @param periodRecordType a period record type
     *  @return the period query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(periodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.PeriodQueryInspectorRecord getPeriodQueryInspectorRecord(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.PeriodQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(periodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(periodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this period query. 
     *
     *  @param periodQueryInspectorRecord period query inspector
     *         record
     *  @param periodRecordType period record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPeriodQueryInspectorRecord(org.osid.billing.records.PeriodQueryInspectorRecord periodQueryInspectorRecord, 
                                                   org.osid.type.Type periodRecordType) {

        addRecordType(periodRecordType);
        nullarg(periodRecordType, "period record type");
        this.records.add(periodQueryInspectorRecord);        
        return;
    }
}
