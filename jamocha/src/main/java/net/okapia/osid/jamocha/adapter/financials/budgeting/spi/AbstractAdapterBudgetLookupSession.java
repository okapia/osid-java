//
// AbstractAdapterBudgetLookupSession.java
//
//    A Budget lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Budget lookup session adapter.
 */

public abstract class AbstractAdapterBudgetLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.financials.budgeting.BudgetLookupSession {

    private final org.osid.financials.budgeting.BudgetLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBudgetLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBudgetLookupSession(org.osid.financials.budgeting.BudgetLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Budget} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBudgets() {
        return (this.session.canLookupBudgets());
    }


    /**
     *  A complete view of the {@code Budget} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBudgetView() {
        this.session.useComparativeBudgetView();
        return;
    }


    /**
     *  A complete view of the {@code Budget} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBudgetView() {
        this.session.usePlenaryBudgetView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include budgets in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only budgets whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveBudgetView() {
        this.session.useEffectiveBudgetView();
        return;
    }
    

    /**
     *  All budgets of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveBudgetView() {
        this.session.useAnyEffectiveBudgetView();
        return;
    }

     
    /**
     *  Gets the {@code Budget} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Budget} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Budget} and
     *  retained for compatibility.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param budgetId {@code Id} of the {@code Budget}
     *  @return the budget
     *  @throws org.osid.NotFoundException {@code budgetId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code budgetId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.Budget getBudget(org.osid.id.Id budgetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudget(budgetId));
    }


    /**
     *  Gets a {@code BudgetList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  budgets specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Budgets} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param  budgetIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Budget} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code budgetIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByIds(org.osid.id.IdList budgetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsByIds(budgetIds));
    }


    /**
     *  Gets a {@code BudgetList} corresponding to the given
     *  budget genus {@code Type} which does not include
     *  budgets of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param  budgetGenusType a budget genus type 
     *  @return the returned {@code Budget} list
     *  @throws org.osid.NullArgumentException
     *          {@code budgetGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByGenusType(org.osid.type.Type budgetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsByGenusType(budgetGenusType));
    }


    /**
     *  Gets a {@code BudgetList} corresponding to the given
     *  budget genus {@code Type} and include any additional
     *  budgets with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param  budgetGenusType a budget genus type 
     *  @return the returned {@code Budget} list
     *  @throws org.osid.NullArgumentException
     *          {@code budgetGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByParentGenusType(org.osid.type.Type budgetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsByParentGenusType(budgetGenusType));
    }


    /**
     *  Gets a {@code BudgetList} containing the given
     *  budget record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @param  budgetRecordType a budget record type 
     *  @return the returned {@code Budget} list
     *  @throws org.osid.NullArgumentException
     *          {@code budgetRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByRecordType(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsByRecordType(budgetRecordType));
    }


    /**
     *  Gets a {@code BudgetList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible
     *  through this session.
     *  
     *  In active mode, budgets are returned that are currently
     *  active. In any status mode, active and inactive budgets
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Budget} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsOnDate(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsOnDate(from, to));
    }
        

    /**
     *  Gets a {@code BudgetList} for the given activity. 
     *  
     *  In plenary mode, the returned list contains all known mode,s
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity {@code Id} 
     *  @return the returned {@code Budget} list 
     *  @throws org.osid.NullArgumentException {@code activityId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsForActivity(activityId));
    }
     

    /**
     *  Gets a {@code BudgetList} for the given activity and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently effective 
     *  in addition to being effective in the given date range. In any 
     *  effective mode, effective budgets and those currently expired are 
     *  returned. 
     *
     *  @param  activityId an activity {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Budget} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code activityId, from
     *         or to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityOnDate(org.osid.id.Id activityId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsForActivityOnDate(activityId, from, to));
    }


    /**
     *  Gets a {@code BudgetList} for the given fiscal period. 
     *  
     *  In plenary mode, the returned list contains all known mode,s
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @return the returned {@code Budget} list 
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBudgetsForFiscalPeriod(fiscalPeriodId));
    }


    /**
     *  Gets a {@code BudgetList} for the given fiscal period and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Budget} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId, from or 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForFiscalPeriodOnDate(org.osid.id.Id fiscalPeriodId, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBudgetsForFiscalPeriodOnDate(fiscalPeriodId, from, to));
    }
        

    /**
     *  Gets a {@code BudgetList} for the given activity and fiscal
     *  period.
     *  
     *  In plenary mode, the returned list contains all known mode,s
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity {@code Id} 
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @return the returned {@code Budget} list 
     *  @throws org.osid.NullArgumentException {@code activityId} or 
     *          {@code fiscalPeriodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityAndFiscalPeriod(org.osid.id.Id activityId, 
                                                                                         org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBudgetsForActivityAndFiscalPeriod(activityId, fiscalPeriodId));
    }


    /**
     *  Gets a {@code BudgetList} for the given activity, fiscal
     *  period, and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity {@code Id} 
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Budget} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code activityId, 
     *          fiscalPeriodId, from or to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityAndFiscalPeriodOnDate(org.osid.id.Id activityId, 
                                                                                               org.osid.id.Id fiscalPeriodId, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgetsForActivityAndFiscalPeriodOnDate(activityId, fiscalPeriodId, from, to));
    }


    /**
     *  Gets all {@code Budgets}. 
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Budgets} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBudgets());
    }
}
