//
// AbstractImmutableFile.java
//
//     Wraps a mutable File to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.file.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>File</code> to hide modifiers. This
 *  wrapper provides an immutized File from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying file whose state changes are visible.
 */

public abstract class AbstractImmutableFile
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.filing.File {

    private final org.osid.filing.File file;


    /**
     *  Constructs a new <code>AbstractImmutableFile</code>.
     *
     *  @param file the file to immutablize
     *  @throws org.osid.NullArgumentException <code>file</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableFile(org.osid.filing.File file) {
        super(file);
        this.file = file;
        return;
    }

    
    /**
     *  Gets the name of this entry. The name does not include the path. If 
     *  this entry represents an alias, the name of the alias is returned. 
     *
     *  @return the entry name 
     */

    @OSID @Override
    public String getName() {
        return (this.file.getName());
    }
                                         

    /**
     *  Tests if this entry is an alias. 
     *
     *  @return <code> true </code> if this is an alias, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean isAlias() {
        return (this.file.isAlias());
    }


    /**
     *  Gets the full path of this entry. The path includes the name. Path 
     *  components are separated by a /. If this entry represents an alias, 
     *  the path to the alias is returned. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getPath() {
        return (this.file.getPath());
    }


    /**
     *  Gets the real path of this entry. The path includes the name. Path 
     *  components are separated by a /. If this entry represents an alias, 
     *  the full path to the target file or directory is returned. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getRealPath() {
        return (this.file.getRealPath());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> that owns this 
     *  entry. 
     *
     *  @return the <code> Agent Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOwnerId() {
        return (this.file.getOwnerId());
    }


    /**
     *  Gets the <code> Agent </code> that owns this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException authentication service not 
     *          available 
     */

    @OSID @Override
    public org.osid.authentication.Agent getOwner()
        throws org.osid.OperationFailedException {

        return (this.file.getOwner());
    }


    /**
     *  Gets the created time of this entry. 
     *
     *  @return the created time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedTime() {
        return (this.file.getCreatedTime());
    }


    /**
     *  Gets the last modified time of this entry. 
     *
     *  @return the last modified time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastModifiedTime() {
        return (this.file.getLastModifiedTime());
    }


    /**
     *  Gets the last accessed time of this entry. 
     *
     *  @return the last accessed time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastAccessTime() {
        return (this.file.getLastAccessTime());
    }
    

    /**
     *  Tests if this file has a known size. 
     *
     *  @return <code> true </code> if this file has a size, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSize() {
        return (this.file.hasSize());
    }


    /**
     *  Gets the size of this file in bytes if <code> hasSize() </code> is 
     *  <code> true. </code> 
     *
     *  @return the size of this file 
     *  @throws org.osid.IllegalStateException <code> hasSize() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getSize() {
        return (this.file.getSize());
    }


    /**
     *  Gets the file record corresponding to the given <code> File </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> fileRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(fileRecordType) </code> is <code> true </code> . 
     *
     *  @param  fileRecordType the type of the record to retrieve 
     *  @return the file record 
     *  @throws org.osid.NullArgumentException <code> fileRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(fileRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.records.FileRecord getFileRecord(org.osid.type.Type fileRecordType)
        throws org.osid.OperationFailedException {

        return (this.file.getFileRecord(fileRecordType));
    }
}

