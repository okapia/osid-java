//
// AbstractMapControllerLookupSession
//
//    A simple framework for providing a Controller lookup service
//    backed by a fixed collection of controllers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Controller lookup service backed by a
 *  fixed collection of controllers. The controllers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Controllers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapControllerLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractControllerLookupSession
    implements org.osid.control.ControllerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.Controller> controllers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.Controller>());


    /**
     *  Makes a <code>Controller</code> available in this session.
     *
     *  @param  controller a controller
     *  @throws org.osid.NullArgumentException <code>controller<code>
     *          is <code>null</code>
     */

    protected void putController(org.osid.control.Controller controller) {
        this.controllers.put(controller.getId(), controller);
        return;
    }


    /**
     *  Makes an array of controllers available in this session.
     *
     *  @param  controllers an array of controllers
     *  @throws org.osid.NullArgumentException <code>controllers<code>
     *          is <code>null</code>
     */

    protected void putControllers(org.osid.control.Controller[] controllers) {
        putControllers(java.util.Arrays.asList(controllers));
        return;
    }


    /**
     *  Makes a collection of controllers available in this session.
     *
     *  @param  controllers a collection of controllers
     *  @throws org.osid.NullArgumentException <code>controllers<code>
     *          is <code>null</code>
     */

    protected void putControllers(java.util.Collection<? extends org.osid.control.Controller> controllers) {
        for (org.osid.control.Controller controller : controllers) {
            this.controllers.put(controller.getId(), controller);
        }

        return;
    }


    /**
     *  Removes a Controller from this session.
     *
     *  @param  controllerId the <code>Id</code> of the controller
     *  @throws org.osid.NullArgumentException <code>controllerId<code> is
     *          <code>null</code>
     */

    protected void removeController(org.osid.id.Id controllerId) {
        this.controllers.remove(controllerId);
        return;
    }


    /**
     *  Gets the <code>Controller</code> specified by its <code>Id</code>.
     *
     *  @param  controllerId <code>Id</code> of the <code>Controller</code>
     *  @return the controller
     *  @throws org.osid.NotFoundException <code>controllerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>controllerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Controller getController(org.osid.id.Id controllerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.Controller controller = this.controllers.get(controllerId);
        if (controller == null) {
            throw new org.osid.NotFoundException("controller not found: " + controllerId);
        }

        return (controller);
    }


    /**
     *  Gets all <code>Controllers</code>. In plenary mode, the returned
     *  list contains all known controllers or an error
     *  results. Otherwise, the returned list may contain only those
     *  controllers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Controllers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.controller.ArrayControllerList(this.controllers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.controllers.clear();
        super.close();
        return;
    }
}
