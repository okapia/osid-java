//
// AbstractOrderValidator.java
//
//     Validates an Order.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.ordering.order.spi;


/**
 *  Validates an Order.
 */

public abstract class AbstractOrderValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractOrderValidator</code>.
     */

    protected AbstractOrderValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractOrderValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractOrderValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Order.
     *
     *  @param order an order to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>order</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.ordering.Order order) {
        super.validate(order);

        testNestedObject(order, "getCustomer");
        testNestedObjects(order, "getItemIds", "getItems");
        test(order.getTotalCost(), "getTotalCost()");

        testConditionalMethod(order, "getSubmitDate", order.isSubmitted(), "isSubmitted()");
        testConditionalMethod(order, "getSubmitterId", order.isSubmitted(), "isSubmitted()");
        testConditionalMethod(order, "getSubmitter", order.isSubmitted(), "isSubmitted()");
        testConditionalMethod(order, "getSubmittingAgentId", order.isSubmitted(), "isSubmitted()");
        testConditionalMethod(order, "getSubmittingAgent", order.isSubmitted(), "isSubmitted()");
        if (order.isSubmitted()) {
            testNestedObject(order, "getSubmitter");
            testNestedObject(order, "getSubmittingAgent");
        }

        testConditionalMethod(order, "getClosedDate", order.isClosed(), "isClosed()");
        testConditionalMethod(order, "getCloserId", order.isClosed(), "isClosed()");
        testConditionalMethod(order, "getCloser", order.isClosed(), "isClosed()");
        testConditionalMethod(order, "getClosingAgentId", order.isClosed(), "isClosed()");
        testConditionalMethod(order, "getClosingAgent", order.isClosed(), "isClosed()");
        if (order.isClosed()) {
            testNestedObject(order, "getCloser");
            testNestedObject(order, "getClosingAgent");
        }

        return;
    }
}
