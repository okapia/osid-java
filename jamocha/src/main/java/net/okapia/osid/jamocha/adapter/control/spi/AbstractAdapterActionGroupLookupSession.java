//
// AbstractAdapterActionGroupLookupSession.java
//
//    An ActionGroup lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An ActionGroup lookup session adapter.
 */

public abstract class AbstractAdapterActionGroupLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.ActionGroupLookupSession {

    private final org.osid.control.ActionGroupLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterActionGroupLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActionGroupLookupSession(org.osid.control.ActionGroupLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code ActionGroup} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupActionGroups() {
        return (this.session.canLookupActionGroups());
    }


    /**
     *  A complete view of the {@code ActionGroup} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActionGroupView() {
        this.session.useComparativeActionGroupView();
        return;
    }


    /**
     *  A complete view of the {@code ActionGroup} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActionGroupView() {
        this.session.usePlenaryActionGroupView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include action groups in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    
     
    /**
     *  Gets the {@code ActionGroup} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ActionGroup} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ActionGroup} and
     *  retained for compatibility.
     *
     *  @param actionGroupId {@code Id} of the {@code ActionGroup}
     *  @return the action group
     *  @throws org.osid.NotFoundException {@code actionGroupId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code actionGroupId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getActionGroup(org.osid.id.Id actionGroupId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionGroup(actionGroupId));
    }


    /**
     *  Gets an {@code ActionGroupList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  actionGroups specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ActionGroups} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  actionGroupIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ActionGroup} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code actionGroupIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByIds(org.osid.id.IdList actionGroupIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionGroupsByIds(actionGroupIds));
    }


    /**
     *  Gets an {@code ActionGroupList} corresponding to the given
     *  action group genus {@code Type} which does not include
     *  action groups of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  actionGroupGenusType an actionGroup genus type 
     *  @return the returned {@code ActionGroup} list
     *  @throws org.osid.NullArgumentException
     *          {@code actionGroupGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByGenusType(org.osid.type.Type actionGroupGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionGroupsByGenusType(actionGroupGenusType));
    }


    /**
     *  Gets an {@code ActionGroupList} corresponding to the given
     *  action group genus {@code Type} and include any additional
     *  action groups with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  actionGroupGenusType an actionGroup genus type 
     *  @return the returned {@code ActionGroup} list
     *  @throws org.osid.NullArgumentException
     *          {@code actionGroupGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByParentGenusType(org.osid.type.Type actionGroupGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionGroupsByParentGenusType(actionGroupGenusType));
    }


    /**
     *  Gets an {@code ActionGroupList} containing the given
     *  action group record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  actionGroupRecordType an actionGroup record type 
     *  @return the returned {@code ActionGroup} list
     *  @throws org.osid.NullArgumentException
     *          {@code actionGroupRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByRecordType(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionGroupsByRecordType(actionGroupRecordType));
    }


    /**
     *  Gets an {@code ActionGroupList} containing of the given {@code 
     *  Action.} 
     *  
     *  In plenary mode, the returned list contains all known action
     *  groups or an error results. Otherwise, the returned list may
     *  contain only those action groups that are accessible through
     *  this session.
     *
     *  @param  actionId an action {@code Id} 
     *  @return the returned {@code ActionGroup} list 
     *  @throws org.osid.NullArgumentException {@code actionId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByAction(org.osid.id.Id actionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionGroupsByAction(actionId));
    }


    /**
     *  Gets all {@code ActionGroups}. 
     *
     *  In plenary mode, the returned list contains all known action
     *  groups or an error results. Otherwise, the returned list may
     *  contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code ActionGroups} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroups()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionGroups());
    }
}
