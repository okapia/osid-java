//
// AbstractAdapterResultLookupSession.java
//
//    A Result lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Result lookup session adapter.
 */

public abstract class AbstractAdapterResultLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.ResultLookupSession {

    private final org.osid.offering.ResultLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterResultLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterResultLookupSession(org.osid.offering.ResultLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code Result} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupResults() {
        return (this.session.canLookupResults());
    }


    /**
     *  A complete view of the {@code Result} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResultView() {
        this.session.useComparativeResultView();
        return;
    }


    /**
     *  A complete view of the {@code Result} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResultView() {
        this.session.usePlenaryResultView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include results in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only results whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResultView() {
        this.session.useEffectiveResultView();
        return;
    }
    

    /**
     *  All results of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResultView() {
        this.session.useAnyEffectiveResultView();
        return;
    }

     
    /**
     *  Gets the {@code Result} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Result} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Result} and
     *  retained for compatibility.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param resultId {@code Id} of the {@code Result}
     *  @return the result
     *  @throws org.osid.NotFoundException {@code resultId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code resultId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Result getResult(org.osid.id.Id resultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResult(resultId));
    }


    /**
     *  Gets a {@code ResultList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  results specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Results} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Result} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code resultIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByIds(org.osid.id.IdList resultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsByIds(resultIds));
    }


    /**
     *  Gets a {@code ResultList} corresponding to the given
     *  result genus {@code Type} which does not include
     *  results of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned {@code Result} list
     *  @throws org.osid.NullArgumentException
     *          {@code resultGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsByGenusType(resultGenusType));
    }


    /**
     *  Gets a {@code ResultList} corresponding to the given
     *  result genus {@code Type} and include any additional
     *  results with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned {@code Result} list
     *  @throws org.osid.NullArgumentException
     *          {@code resultGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByParentGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsByParentGenusType(resultGenusType));
    }


    /**
     *  Gets a {@code ResultList} containing the given
     *  result record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultRecordType a result record type 
     *  @return the returned {@code Result} list
     *  @throws org.osid.NullArgumentException
     *          {@code resultRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByRecordType(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsByRecordType(resultRecordType));
    }


    /**
     *  Gets a {@code ResultList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible
     *  through this session.
     *  
     *  In active mode, results are returned that are currently
     *  active. In any status mode, active and inactive results
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Result} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ResultList getResultsOnDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsOnDate(from, to));
    }
        

    /**
     *  Gets an {@code ResultList} by genus type effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  resultGenusType a results genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ResultList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resultGenusType, from or 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeOnDate(org.osid.type.Type resultGenusType, 
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsByGenusTypeOnDate(resultGenusType, from, to));
    }


    /**
     *  Gets an {@code ResultList} for the given participant {@code
     *  Id}.
     *  
     *  In plenary mode, the returned list contains all of the results
     *  corresponding to the given participant, including duplicates,
     *  or an error results if an result is inaccessible. Otherwise,
     *  inaccessible {@code Results} may be omitted from the list.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant {@code Id} 
     *  @return the returned {@code ResultList} 
     *  @throws org.osid.NullArgumentException {@code participantId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsForParticipant(org.osid.id.Id participantId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsForParticipant(participantId));
    }

    
    /**
     *  Gets an {@code ResultList} for the given participant {@code
     *  Id}.
     *  
     *  {@code} In plenary mode, the returned list contains all of the
     *  results corresponding to the given participant, including
     *  duplicates, or an error results if an result is
     *  inaccessible. Otherwise, inaccessible {@code Results} may be
     *  omitted from the list.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant {@code Id} 
     *  @param  resultGenusType a results genus type 
     *  @return the returned {@code ResultList} 
     *  @throws org.osid.NullArgumentException {@code participantId} or 
     *          {@code resultGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeForParticipant(org.osid.id.Id participantId, 
                                                                            org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResultsByGenusTypeForParticipant(participantId, resultGenusType));
    }


    /**
     *  Gets a list of results for a participant and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ResultList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code participantId, from, 
     *         } or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsForParticipantOnDate(org.osid.id.Id participantId, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getResultsForParticipantOnDate(participantId, from, to));
    }


    /**
     *  Gets a list of results for a participant and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant {@code Id} 
     *  @param  resultGenusType a results genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ResultList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code participantId, 
     *          resultGenusType, from,} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeForParticipantOnDate(org.osid.id.Id participantId, 
                                                                                  org.osid.type.Type resultGenusType, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.getResultsByGenusTypeForParticipantOnDate(participantId, resultGenusType, from, to));
    }


    /**
     *  Gets all {@code Results}. 
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Results} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResults());
    }
}
