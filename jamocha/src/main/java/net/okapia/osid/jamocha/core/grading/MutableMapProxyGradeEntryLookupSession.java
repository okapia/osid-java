//
// MutableMapProxyGradeEntryLookupSession
//
//    Implements a GradeEntry lookup service backed by a collection of
//    gradeEntries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading;


/**
 *  Implements a GradeEntry lookup service backed by a collection of
 *  gradeEntries. The gradeEntries are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of grade entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyGradeEntryLookupSession
    extends net.okapia.osid.jamocha.core.grading.spi.AbstractMapGradeEntryLookupSession
    implements org.osid.grading.GradeEntryLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyGradeEntryLookupSession}
     *  with no grade entries.
     *
     *  @param gradebook the gradebook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyGradeEntryLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.proxy.Proxy proxy) {
        setGradebook(gradebook);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyGradeEntryLookupSession} with a
     *  single grade entry.
     *
     *  @param gradebook the gradebook
     *  @param gradeEntry a grade entry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeEntry}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyGradeEntryLookupSession(org.osid.grading.Gradebook gradebook,
                                                org.osid.grading.GradeEntry gradeEntry, org.osid.proxy.Proxy proxy) {
        this(gradebook, proxy);
        putGradeEntry(gradeEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyGradeEntryLookupSession} using an
     *  array of grade entries.
     *
     *  @param gradebook the gradebook
     *  @param gradeEntries an array of grade entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeEntries}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyGradeEntryLookupSession(org.osid.grading.Gradebook gradebook,
                                                org.osid.grading.GradeEntry[] gradeEntries, org.osid.proxy.Proxy proxy) {
        this(gradebook, proxy);
        putGradeEntries(gradeEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyGradeEntryLookupSession} using a
     *  collection of grade entries.
     *
     *  @param gradebook the gradebook
     *  @param gradeEntries a collection of grade entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeEntries}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyGradeEntryLookupSession(org.osid.grading.Gradebook gradebook,
                                                java.util.Collection<? extends org.osid.grading.GradeEntry> gradeEntries,
                                                org.osid.proxy.Proxy proxy) {
   
        this(gradebook, proxy);
        setSessionProxy(proxy);
        putGradeEntries(gradeEntries);
        return;
    }

    
    /**
     *  Makes a {@code GradeEntry} available in this session.
     *
     *  @param gradeEntry an grade entry
     *  @throws org.osid.NullArgumentException {@code gradeEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeEntry(org.osid.grading.GradeEntry gradeEntry) {
        super.putGradeEntry(gradeEntry);
        return;
    }


    /**
     *  Makes an array of gradeEntries available in this session.
     *
     *  @param gradeEntries an array of grade entries
     *  @throws org.osid.NullArgumentException {@code gradeEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeEntries(org.osid.grading.GradeEntry[] gradeEntries) {
        super.putGradeEntries(gradeEntries);
        return;
    }


    /**
     *  Makes collection of grade entries available in this session.
     *
     *  @param gradeEntries
     *  @throws org.osid.NullArgumentException {@code gradeEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeEntries(java.util.Collection<? extends org.osid.grading.GradeEntry> gradeEntries) {
        super.putGradeEntries(gradeEntries);
        return;
    }


    /**
     *  Removes a GradeEntry from this session.
     *
     *  @param gradeEntryId the {@code Id} of the grade entry
     *  @throws org.osid.NullArgumentException {@code gradeEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGradeEntry(org.osid.id.Id gradeEntryId) {
        super.removeGradeEntry(gradeEntryId);
        return;
    }    
}
