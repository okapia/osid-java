//
// AbstractInstallationQuery.java
//
//     A template for making an Installation Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for installations.
 */

public abstract class AbstractInstallationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.installation.InstallationQuery {

    private final java.util.Collection<org.osid.installation.records.InstallationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the site <code> Id </code> for this query. 
     *
     *  @param  siteId a site <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> siteId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSiteId(org.osid.id.Id siteId, boolean match) {
        return;
    }


    /**
     *  Clears the site <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSiteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SiteQuery </code> is available for querying sites. 
     *
     *  @return <code> true </code> if a site query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSiteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a site. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the site query 
     *  @throws org.osid.UnimplementedException <code> supportsSiteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteQuery getSiteQuery() {
        throw new org.osid.UnimplementedException("supportsSiteQuery() is false");
    }


    /**
     *  Clears the site query terms. 
     */

    @OSID @Override
    public void clearSiteTerms() {
        return;
    }


    /**
     *  Sets the package <code> Id </code> for this query. 
     *
     *  @param  packageId a package <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPackageId(org.osid.id.Id packageId, boolean match) {
        return;
    }


    /**
     *  Clears the package <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPackageIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a package. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getPackageQuery() {
        throw new org.osid.UnimplementedException("supportsPackageQuery() is false");
    }


    /**
     *  Clears the package query terms. 
     */

    @OSID @Override
    public void clearPackageTerms() {
        return;
    }


    /**
     *  Matches the install date between the given times inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is <code> 
     *          less than from </code> 
     */

    @OSID @Override
    public void matchInstallDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the install date query terms. 
     */

    @OSID @Override
    public void clearInstallDateTerms() {
        return;
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Matches the last checked date between the given times inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is <code> 
     *          less than from </code> 
     */

    @OSID @Override
    public void matchLastCheckDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the last check date query terms. 
     */

    @OSID @Override
    public void clearLastCheckDateTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given installation query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an installation implementing the requested record.
     *
     *  @param installationRecordType an installation record type
     *  @return the installation query record
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationQueryRecord getInstallationQueryRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationQueryRecord record : this.records) {
            if (record.implementsRecordType(installationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this installation query. 
     *
     *  @param installationQueryRecord installation query record
     *  @param installationRecordType installation record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstallationQueryRecord(org.osid.installation.records.InstallationQueryRecord installationQueryRecord, 
                                          org.osid.type.Type installationRecordType) {

        addRecordType(installationRecordType);
        nullarg(installationQueryRecord, "installation query record");
        this.records.add(installationQueryRecord);        
        return;
    }
}
