//
// AbstractAssemblyCompositionQuery.java
//
//     A CompositionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CompositionQuery that stores terms.
 */

public abstract class AbstractAssemblyCompositionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblySourceableOsidObjectQuery
    implements org.osid.repository.CompositionQuery,
               org.osid.repository.CompositionQueryInspector,
               org.osid.repository.CompositionSearchOrder {

    private final java.util.Collection<org.osid.repository.records.CompositionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.CompositionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.CompositionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidOperableQuery operableQuery;
    private final AssemblyOsidContainableQuery containableQuery;


    /** 
     *  Constructs a new <code>AbstractAssemblyCompositionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCompositionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.operableQuery = new AssemblyOsidOperableQuery(assembler);
        this.containableQuery = new AssemblyOsidContainableQuery(assembler);
        return;
    }
    

    /**
     *  Matches active operables.
     *
     *  @param match <code> true </code> to match active objects,
     *          <code> false </code> to match inactive objects
     */

    @OSID @Override
    public void matchActive(boolean match) {
        this.operableQuery.matchActive(match);
        return;
    }


    /**
     *  Clears all match active terms. 
     */

    @OSID @Override
    public void clearActiveTerms() {
        this.operableQuery.clearActiveTerms();
        return;
    }


    /**
     *  Gets the active query terms. 
     *
     *  @return the active terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getActiveTerms() {
        return (this.operableQuery.getActiveTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  active flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActive(org.osid.SearchOrderStyle style) {
        this.operableQuery.orderByActive(style);
        return;
    }


    /**
     *  Gets the column name for the active field.
     *
     *  @return the column name
     */

    protected String getActiveColumn() {
        return (this.operableQuery.getActiveColumn());
    }


    /**
     *  Matches enabled operables.
     *
     *  @param match <code> true </code> to match enabled objects,
     *          <code> false </code> to match inenabled objects
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        this.operableQuery.matchEnabled(match);
        return;
    }


    /**
     *  Clears all match enabled terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        this.operableQuery.clearEnabledTerms();
        return;
    }


    /**
     *  Gets the enabled query terms. 
     *
     *  @return the enabled terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (this.operableQuery.getEnabledTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  enabled flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnabled(org.osid.SearchOrderStyle style) {
        this.operableQuery.orderByEnabled(style);
        return;
    }


    /**
     *  Gets the column name for the enabled field.
     *
     *  @return the column name
     */

    protected String getEnabledColumn() {
        return (this.operableQuery.getEnabledColumn());
    }
    

    /**
     *  Matches disabled operables.
     *
     *  @param match <code> true </code> to match disabled objects,
     *          <code> false </code> to match indisabled objects
     */

    @OSID @Override
    public void matchDisabled(boolean match) {
        this.operableQuery.matchDisabled(match);
        return;
    }


    /**
     *  Clears all match disabled terms. 
     */

    @OSID @Override
    public void clearDisabledTerms() {
        this.operableQuery.clearDisabledTerms();
        return;
    }


    /**
     *  Gets the disabled query terms. 
     *
     *  @return the disabled terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDisabledTerms() {
        return (this.operableQuery.getDisabledTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  disabled flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisabled(org.osid.SearchOrderStyle style) {
        this.operableQuery.orderByDisabled(style);
        return;
    }


    /**
     *  Gets the column name for the disabled field.
     *
     *  @return the column name
     */

    protected String getDisabledColumn() {
        return (this.operableQuery.getDisabledColumn());
    }


    /**
     *  Matches operational operables.
     *
     *  @param match <code> true </code> to match operational objects,
     *          <code> false </code> to match inoperational objects
     */

    @OSID @Override
    public void matchOperational(boolean match) {
        this.operableQuery.matchOperational(match);
        return;
    }


    /**
     *  Clears all match operational terms. 
     */

    @OSID @Override
    public void clearOperationalTerms() {
        this.operableQuery.clearOperationalTerms();
        return;
    }


    /**
     *  Gets the operational query terms. 
     *
     *  @return the operational terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOperationalTerms() {
        return (this.operableQuery.getOperationalTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  operational flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOperational(org.osid.SearchOrderStyle style) {
        this.operableQuery.orderByOperational(style);
        return;
    }


    /**
     *  Gets the column name for the operational field.
     *
     *  @return the column name
     */

    protected String getOperationalColumn() {
        return (this.operableQuery.getOperationalColumn());
    }


    /**
     *  Match containables that are sequestered. 
     *
     *  @param  match <code> true </code> to match any sequestered 
     *          containables, <code> false </code> to match non-sequestered 
     *          containables 
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.containableQuery.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms. 
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.containableQuery.clearSequesteredTerms();
        return;
    }

    
    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.containableQuery.getSequesteredTerms());
    }

    
    /**
     *  Specifies a preference for ordering the result set by the sequestered 
     *  flag. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.containableQuery.orderBySequestered(style);
        return;
    }


    /**
     *  Gets the column name for the sequestered field.
     *
     *  @return the column name
     */

    protected String getSequesteredColumn() {
        return (this.containableQuery.getSequesteredColumn());
    }

    
    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getAssetIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        getAssembler().clearTerms(getAssetIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (getAssembler().getIdTerms(getAssetIdColumn()));
    }


    /**
     *  Gets the AssetId column name.
     *
     * @return the column name
     */

    protected String getAssetIdColumn() {
        return ("asset_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches compositions that has any asset mapping. 
     *
     *  @param  match <code> true </code> to match compositions with any 
     *          asset, <code> false </code> to match compositions with no 
     *          asset 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        getAssembler().addIdWildcardTerm(getAssetColumn(), match);
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        getAssembler().clearTerms(getAssetColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the Asset column name.
     *
     * @return the column name
     */

    protected String getAssetColumn() {
        return ("asset");
    }


    /**
     *  Sets the composition <code> Id </code> for this query to match 
     *  compositions that have the specified composition as an ancestor. 
     *
     *  @param  compositionId a composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingCompositionId(org.osid.id.Id compositionId, 
                                             boolean match) {
        getAssembler().addIdTerm(getContainingCompositionIdColumn(), compositionId, match);
        return;
    }


    /**
     *  Clears the containing composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingCompositionIdTerms() {
        getAssembler().clearTerms(getContainingCompositionIdColumn());
        return;
    }


    /**
     *  Gets the containing composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingCompositionIdTerms() {
        return (getAssembler().getIdTerms(getContainingCompositionIdColumn()));
    }


    /**
     *  Gets the ContainingCompositionId column name.
     *
     * @return the column name
     */

    protected String getContainingCompositionIdColumn() {
        return ("containing_composition_id");
    }


    /**
     *  Tests if an <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingCompositionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getContainingCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsContainingCompositionQuery() is false");
    }


    /**
     *  Matches compositions with any ancestor. 
     *
     *  @param  match <code> true </code> to match composition with any 
     *          ancestor, <code> false </code> to match root compositions 
     */

    @OSID @Override
    public void matchAnyContainingComposition(boolean match) {
        getAssembler().addIdWildcardTerm(getContainingCompositionColumn(), match);
        return;
    }


    /**
     *  Clears the containing composition terms. 
     */

    @OSID @Override
    public void clearContainingCompositionTerms() {
        getAssembler().clearTerms(getContainingCompositionColumn());
        return;
    }


    /**
     *  Gets the containing composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getContainingCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the ContainingComposition column name.
     *
     * @return the column name
     */

    protected String getContainingCompositionColumn() {
        return ("containing_composition");
    }


    /**
     *  Sets the composition <code> Id </code> for this query to match 
     *  compositions that contain the specified composition. 
     *
     *  @param  compositionId a composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedCompositionId(org.osid.id.Id compositionId, 
                                            boolean match) {
        getAssembler().addIdTerm(getContainedCompositionIdColumn(), compositionId, match);
        return;
    }


    /**
     *  Clears the contained composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainedCompositionIdTerms() {
        getAssembler().clearTerms(getContainedCompositionIdColumn());
        return;
    }


    /**
     *  Gets the contained composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainedCompositionIdTerms() {
        return (getAssembler().getIdTerms(getContainedCompositionIdColumn()));
    }


    /**
     *  Gets the ContainedCompositionId column name.
     *
     * @return the column name
     */

    protected String getContainedCompositionIdColumn() {
        return ("contained_composition_id");
    }


    /**
     *  Tests if an <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainedCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainedCompositionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getContainedCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsContainedCompositionQuery() is false");
    }


    /**
     *  Matches compositions that contain any other compositions. 
     *
     *  @param  match <code> true </code> to match composition with any 
     *          descendant, <code> false </code> to match leaf compositions 
     */

    @OSID @Override
    public void matchAnyContainedComposition(boolean match) {
        getAssembler().addIdWildcardTerm(getContainedCompositionColumn(), match);
        return;
    }


    /**
     *  Clears the contained composition terms. 
     */

    @OSID @Override
    public void clearContainedCompositionTerms() {
        getAssembler().clearTerms(getContainedCompositionColumn());
        return;
    }


    /**
     *  Gets the contained composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getContainedCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the ContainedComposition column name.
     *
     * @return the column name
     */

    protected String getContainedCompositionColumn() {
        return ("contained_composition");
    }


    /**
     *  Sets the repository <code> Id </code> for this query. 
     *
     *  @param  repositoryId the repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRepositoryId(org.osid.id.Id repositoryId, boolean match) {
        getAssembler().addIdTerm(getRepositoryIdColumn(), repositoryId, match);
        return;
    }


    /**
     *  Clears the repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRepositoryIdTerms() {
        getAssembler().clearTerms(getRepositoryIdColumn());
        return;
    }


    /**
     *  Gets the repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRepositoryIdTerms() {
        return (getAssembler().getIdTerms(getRepositoryIdColumn()));
    }


    /**
     *  Gets the RepositoryId column name.
     *
     * @return the column name
     */

    protected String getRepositoryIdColumn() {
        return ("repository_id");
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple queries can be retrieved for 
     *  a nested <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsRepositoryQuery() is false");
    }


    /**
     *  Clears the repository terms. 
     */

    @OSID @Override
    public void clearRepositoryTerms() {
        getAssembler().clearTerms(getRepositoryColumn());
        return;
    }


    /**
     *  Gets the repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }


    /**
     *  Gets the Repository column name.
     *
     * @return the column name
     */

    protected String getRepositoryColumn() {
        return ("repository");
    }


    /**
     *  Tests if this composition supports the given record
     *  <code>Type</code>.
     *
     *  @param  compositionRecordType a composition record type 
     *  @return <code>true</code> if the compositionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type compositionRecordType) {
        for (org.osid.repository.records.CompositionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  compositionRecordType the composition record type 
     *  @return the composition query record 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionQueryRecord getCompositionQueryRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.CompositionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  compositionRecordType the composition record type 
     *  @return the composition query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionQueryInspectorRecord getCompositionQueryInspectorRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.CompositionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param compositionRecordType the composition record type
     *  @return the composition search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionSearchOrderRecord getCompositionSearchOrderRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.CompositionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this composition. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param compositionQueryRecord the composition query record
     *  @param compositionQueryInspectorRecord the composition query inspector
     *         record
     *  @param compositionSearchOrderRecord the composition search order record
     *  @param compositionRecordType composition record type
     *  @throws org.osid.NullArgumentException
     *          <code>compositionQueryRecord</code>,
     *          <code>compositionQueryInspectorRecord</code>,
     *          <code>compositionSearchOrderRecord</code> or
     *          <code>compositionRecordTypecomposition</code> is
     *          <code>null</code>
     */
            
    protected void addCompositionRecords(org.osid.repository.records.CompositionQueryRecord compositionQueryRecord, 
                                      org.osid.repository.records.CompositionQueryInspectorRecord compositionQueryInspectorRecord, 
                                      org.osid.repository.records.CompositionSearchOrderRecord compositionSearchOrderRecord, 
                                      org.osid.type.Type compositionRecordType) {

        addRecordType(compositionRecordType);

        nullarg(compositionQueryRecord, "composition query record");
        nullarg(compositionQueryInspectorRecord, "composition query inspector record");
        nullarg(compositionSearchOrderRecord, "composition search odrer record");

        this.queryRecords.add(compositionQueryRecord);
        this.queryInspectorRecords.add(compositionQueryInspectorRecord);
        this.searchOrderRecords.add(compositionSearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidOperableQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidOperableQuery
        implements org.osid.OsidOperableQuery,
                   org.osid.OsidOperableQueryInspector,
                   org.osid.OsidOperableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidOperableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidOperableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the active field.
         *
         *  @return the column name
         */
        
        protected String getActiveColumn() {
            return (super.getActiveColumn());
        }


        /**
         *  Gets the column name for the enabled field.
         *
         *  @return the column name
         */
        
        protected String getEnabledColumn() {
            return (super.getEnabledColumn());
        }
    

        /**
         *  Gets the column name for the disabled field.
         *
         *  @return the column name
         */
        
        protected String getDisabledColumn() {
            return (super.getDisabledColumn());
        }

        
        /**
         *  Gets the column name for the operational field.
         *
         *  @return the column name
         */
        
        protected String getOperationalColumn() {
            return (super.getOperationalColumn());
        }
    }    

    
    protected class AssemblyOsidContainableQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidContainableQuery
        implements org.osid.OsidContainableQuery,
                   org.osid.OsidContainableQueryInspector,
                   org.osid.OsidContainableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidContainableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidContainableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the sequestered field.
         *
         *  @return the column name
         */
        
        protected String getSequesteredColumn() {
            return (super.getSequesteredColumn());
        }
    }       
}
