//
// AbstractFile.java
//
//     Defines a File builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.file.spi;


/**
 *  Defines a <code>File</code> builder.
 */

public abstract class AbstractFileBuilder<T extends AbstractFileBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.filing.file.FileMiter file;


    /**
     *  Constructs a new <code>AbstractFileBuilder</code>.
     *
     *  @param file the file to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractFileBuilder(net.okapia.osid.jamocha.builder.filing.file.FileMiter file) {
        super(file);
        this.file = file;
        return;
    }


    /**
     *  Sets the name.
     *  
     *  @param name the file name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    public T name(String name) {
        getMiter().setName(name);
        return (self());
    }


    /**
     *  Sets the alias flag.
     */

    public T alias() {
        getMiter().setAlias(true);
        return (self());
    }


    /**
     *  Unsets the alias flag.
     */

    public T unalias() {
        getMiter().setAlias(false);
        return (self());
    }


    /**
     *  Sets the path.
     *  
     *  @param path the file path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T path(String path) {
        getMiter().setPath(path);
        return (self());
    }


    /**
     *  Sets the real path.
     *  
     *  @param path the file real path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T realPath(String path) {
        getMiter().setRealPath(path);
        return (self());
    }


    /**
     *  Sets the owner.
     *  
     *  @param agent the file owner
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T owner(org.osid.authentication.Agent agent) {
        getMiter().setOwner(agent);
        return (self());
    }


    /**
     *  Sets the created time.
     *  
     *  @param time the file created time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T createdTime(org.osid.calendaring.DateTime time) {
        getMiter().setCreatedTime(time);
        return (self());
    }

 
    /**
     *  Sets the modified time.
     *  
     *  @param time the file modified time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T modifiedTime(org.osid.calendaring.DateTime time) {
        getMiter().setLastModifiedTime(time);
        return (self());
    }


    /**
     *  Sets the accessed time.
     *  
     *  @param time the file accessed time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T accessTime(org.osid.calendaring.DateTime time) {
        getMiter().setLastAccessTime(time);
        return (self());
    }

    
    /**
     *  Sets the size.
     *
     *  @param size the file size
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    public T size(long size) {
        getMiter().setSize(size);
        return (self());
    }


    /**
     *  Builds the file.
     *
     *  @return the new file
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.filing.File build() {
        (new net.okapia.osid.jamocha.builder.validator.filing.file.FileValidator(getValidations())).validate(this.file);
        return (new net.okapia.osid.jamocha.builder.filing.file.ImmutableFile(this.file));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the file miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.filing.file.FileMiter getMiter() {
        return (this.file);
    }


    /**
     *  Adds a File record.
     *
     *  @param record a file record
     *  @param recordType the type of file record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.filing.records.FileRecord record, org.osid.type.Type recordType) {
        getMiter().addFileRecord(record, recordType);
        return (self());
    }
}       


