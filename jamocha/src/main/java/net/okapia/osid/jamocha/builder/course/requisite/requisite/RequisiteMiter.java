//
// RequisiteMiter.java
//
//     Defines a Requisite miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.requisite;


/**
 *  Defines a <code>Requisite</code> miter for use with the builders.
 */

public interface RequisiteMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidEnablerMiter,
            net.okapia.osid.jamocha.builder.spi.ContainableMiter,
            org.osid.course.requisite.Requisite {


    /**
     *  Adds a requisite option.
     *
     *  @param option a requisite option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public void addRequisiteOption(org.osid.course.requisite.Requisite option);


    /**
     *  Sets all the requisite options.
     *
     *  @param options a collection of requisite options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public void setRequisiteOptions(java.util.Collection<org.osid.course.requisite.Requisite> options);


    /**
     *  Adds a course requirement.
     *
     *  @param requirement a course requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addCourseRequirement(org.osid.course.requisite.CourseRequirement requirement);


    /**
     *  Sets all the course requirements.
     *
     *  @param requirements a collection of course requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setCourseRequirements(java.util.Collection<org.osid.course.requisite.CourseRequirement> requirements);


    /**
     *  Adds a program requirement.
     *
     *  @param requirement a program requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addProgramRequirement(org.osid.course.requisite.ProgramRequirement requirement);


    /**
     *  Sets all the program requirements.
     *
     *  @param requirements a collection of program requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setProgramRequirements(java.util.Collection<org.osid.course.requisite.ProgramRequirement> requirements);


    /**
     *  Adds a credential requirement.
     *
     *  @param requirement a credential requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addCredentialRequirement(org.osid.course.requisite.CredentialRequirement requirement);


    /**
     *  Sets all the credential requirements.
     *
     *  @param requirements a collection of credential requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setCredentialRequirements(java.util.Collection<org.osid.course.requisite.CredentialRequirement> requirements);


    /**
     *  Adds a learning objective requirement.
     *
     *  @param requirement a learning objective requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addLearningObjectiveRequirement(org.osid.course.requisite.LearningObjectiveRequirement requirement);


    /**
     *  Sets all the learning objective requirements.
     *
     *  @param requirements a collection of learning objective requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setLearningObjectiveRequirements(java.util.Collection<org.osid.course.requisite.LearningObjectiveRequirement> requirements);


    /**
     *  Adds an award requirement.
     *
     *  @param requirement an award requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addAwardRequirement(org.osid.course.requisite.AwardRequirement requirement);


    /**
     *  Sets all the award requirements.
     *
     *  @param requirements a collection of award requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setAwardRequirements(java.util.Collection<org.osid.course.requisite.AwardRequirement> requirements);


    /**
     *  Adds an assessment requirement.
     *
     *  @param requirement an assessment requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addAssessmentRequirement(org.osid.course.requisite.AssessmentRequirement requirement);


    /**
     *  Sets all the assessment requirements.
     *
     *  @param requirements a collection of assessment requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setAssessmentRequirements(java.util.Collection<org.osid.course.requisite.AssessmentRequirement> requirements);


    /**
     *  Adds a Requisite record.
     *
     *  @param record a requisite record
     *  @param recordType the type of requisite record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRequisiteRecord(org.osid.course.requisite.records.RequisiteRecord record, org.osid.type.Type recordType);
}       


