//
// InvariantMapProxyCommitmentLookupSession
//
//    Implements a Commitment lookup service backed by a fixed
//    collection of commitments. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a Commitment lookup service backed by a fixed
 *  collection of commitments. The commitments are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCommitmentLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapCommitmentLookupSession
    implements org.osid.calendaring.CommitmentLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCommitmentLookupSession} with no
     *  commitments.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCommitmentLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCommitmentLookupSession} with a single
     *  commitment.
     *
     *  @param calendar the calendar
     *  @param commitment a single commitment
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code commitment} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCommitmentLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.Commitment commitment, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putCommitment(commitment);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCommitmentLookupSession} using
     *  an array of commitments.
     *
     *  @param calendar the calendar
     *  @param commitments an array of commitments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code commitments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCommitmentLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.Commitment[] commitments, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putCommitments(commitments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCommitmentLookupSession} using a
     *  collection of commitments.
     *
     *  @param calendar the calendar
     *  @param commitments a collection of commitments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code commitments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCommitmentLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.Commitment> commitments,
                                                  org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putCommitments(commitments);
        return;
    }
}
