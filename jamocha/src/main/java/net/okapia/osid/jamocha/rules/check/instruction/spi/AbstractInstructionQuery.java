//
// AbstractInstructionQuery.java
//
//     A template for making an Instruction Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.instruction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for instructions.
 */

public abstract class AbstractInstructionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.rules.check.InstructionQuery {

    private final java.util.Collection<org.osid.rules.check.records.InstructionQueryRecord> records = new java.util.ArrayList<>();

    private final OsidRelationshipQuery query = new OsidRelationshipQuery();

    
    /**
     *  Sets the agenda <code> Id </code> for this query. 
     *
     *  @param  agendaId the agenda <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agendaId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgendaId(org.osid.id.Id agendaId, boolean match) {
        return;
    }


    /**
     *  Clears the agenda <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgendaIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgendaQuery </code> is available. 
     *
     *  @return <code> true </code> if an agenda query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agenda. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agenda query 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuery getAgendaQuery() {
        throw new org.osid.UnimplementedException("supportsAgendaQuery() is false");
    }


    /**
     *  Clears the agenda query terms. 
     */

    @OSID @Override
    public void clearAgendaTerms() {
        return;
    }


    /**
     *  Sets the check <code> Id </code> for this query. 
     *
     *  @param  checkId the check <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checkId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCheckId(org.osid.id.Id checkId, boolean match) {
        return;
    }


    /**
     *  Clears the check <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCheckIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CheckQuery </code> is available. 
     *
     *  @return <code> true </code> if a check query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckQuery() {
        return (false);
    }


    /**
     *  Gets the query for a check. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the check query 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuery getCheckQuery() {
        throw new org.osid.UnimplementedException("supportsCheckQuery() is false");
    }


    /**
     *  Clears the check query terms. 
     */

    @OSID @Override
    public void clearCheckTerms() {
        return;
    }


    /**
     *  Matches instructions within the given position range inclusive. 
     *
     *  @param  message text to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> message or 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchMessage(String message, 
                             org.osid.type.Type stringMatchType, boolean match) {
        return;
    }


    /**
     *  Matches instructions that have any message. 
     *
     *  @param  match <code> true </code> to match instructions with any 
     *          message, <code> false </code> to match instructions with no 
     *          message 
     */

    @OSID @Override
    public void matchAnyMessage(boolean match) {
        return;
    }


    /**
     *  Clears the message query terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        return;
    }


    /**
     *  Matches warning instructions. 
     *
     *  @param  match <code> true </code> to match warning instructions, 
     *          <code> false </code> to match hard error instructions 
     */

    @OSID @Override
    public void matchWarning(boolean match) {
        return;
    }


    /**
     *  Clears the warning query terms. 
     */

    @OSID @Override
    public void clearWarningTerms() {
        return;
    }


    /**
     *  Matches continue-on-fail instructions. 
     *
     *  @param  match <code> true </code> to match continue-on-fail 
     *          instructions, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchContinueOnFail(boolean match) {
        return;
    }


    /**
     *  Clears the continue-on-fail query terms. 
     */

    @OSID @Override
    public void clearContinueOnFailTerms() {
        return;
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match instructions 
     *  assigned to engines. 
     *
     *  @param  engineId the engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        return;
    }


    /**
     *  Match the <code> Id </code> of the end reason state. 
     *
     *  @param  stateId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEndReasonId(org.osid.id.Id stateId, boolean match) {
        this.query.matchEndReasonId(stateId, match);
        return;
    }


    /**
     *  Clears all state <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEndReasonIdTerms() {
        this.query.clearEndReasonIdTerms();
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> for the rule is available. 
     *
     *  @return <code> true </code> if a end reason query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReasonQuery() {
        return (this.query.supportsEndReasonQuery());
    }


    /**
     *  Gets the query for the end reason state. Each retrieval performs a 
     *  boolean <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndReasonQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getEndReasonQuery(boolean match) {
        return (this.query.getEndReasonQuery(match));
    }

    
    /**
     *  Match any end reason state. 
     *
     *  @param  match <code> true </code> to match any state, <code> false 
     *          </code> to match no state 
     */

    @OSID @Override
    public void matchAnyEndReason(boolean match) {
        this.query.matchAnyEndReason(match);
        return;
    }


    /**
     *  Clears all end reason state terms. 
     */

    @OSID @Override
    public void clearEndReasonTerms() {
        this.query.clearEndReasonTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given instruction query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an instruction implementing the requested record.
     *
     *  @param instructionRecordType an instruction record type
     *  @return the instruction query record
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(instructionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionQueryRecord getInstructionQueryRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.InstructionQueryRecord record : this.records) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this instruction query. 
     *
     *  @param instructionQueryRecord instruction query record
     *  @param instructionRecordType instruction record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstructionQueryRecord(org.osid.rules.check.records.InstructionQueryRecord instructionQueryRecord, 
                                          org.osid.type.Type instructionRecordType) {

        addRecordType(instructionRecordType);
        nullarg(instructionQueryRecord, "instruction query record");
        this.records.add(instructionQueryRecord);        
        return;
    }


    protected class OsidRelationshipQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
        implements org.osid.OsidRelationshipQuery {

    }
}