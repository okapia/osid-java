//
// AbstractProfileEntry.java
//
//     Defines a ProfileEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.profile.profileentry.spi;


/**
 *  Defines a <code>ProfileEntry</code> builder.
 */

public abstract class AbstractProfileEntryBuilder<T extends AbstractProfileEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.profile.profileentry.ProfileEntryMiter profileEntry;


    /**
     *  Constructs a new <code>AbstractProfileEntryBuilder</code>.
     *
     *  @param profileEntry the profile entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProfileEntryBuilder(net.okapia.osid.jamocha.builder.profile.profileentry.ProfileEntryMiter profileEntry) {
        super(profileEntry);
        this.profileEntry = profileEntry;
        return;
    }


    /**
     *  Builds the profile entry.
     *
     *  @return the new profile entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.profile.ProfileEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.profile.profileentry.ProfileEntryValidator(getValidations())).validate(this.profileEntry);
        return (new net.okapia.osid.jamocha.builder.profile.profileentry.ImmutableProfileEntry(this.profileEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the profile entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.profile.profileentry.ProfileEntryMiter getMiter() {
        return (this.profileEntry);
    }


    /**
     *  Sets the implicit flag.
     *
     *  @return the builder
     */

    public T implicit() {
        getMiter().setImplicit(true);
        return (self());
    }


    /**
     *  Unsets the implicit flag.
     *
     *  @return the builder
     */

    public T explicit() {
        getMiter().setImplicit(false);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Sets the profile item.
     *
     *  @param item a profile item
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    public T profileItem(org.osid.profile.ProfileItem item) {
        getMiter().setProfileItem(item);
        return (self());
    }


    /**
     *  Adds a ProfileEntry record.
     *
     *  @param record a profile entry record
     *  @param recordType the type of profile entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.profile.records.ProfileEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addProfileEntryRecord(record, recordType);
        return (self());
    }
}       


