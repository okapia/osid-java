//
// ActivityElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activity.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActivityElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ActivityElement Id.
     *
     *  @return the activity element Id
     */

    public static org.osid.id.Id getActivityEntityId() {
        return (makeEntityId("osid.course.Activity"));
    }


    /**
     *  Gets the ActivityUnitId element Id.
     *
     *  @return the ActivityUnitId element Id
     */

    public static org.osid.id.Id getActivityUnitId() {
        return (makeElementId("osid.course.activity.ActivityUnitId"));
    }


    /**
     *  Gets the ActivityUnit element Id.
     *
     *  @return the ActivityUnit element Id
     */

    public static org.osid.id.Id getActivityUnit() {
        return (makeElementId("osid.course.activity.ActivityUnit"));
    }


    /**
     *  Gets the CourseOfferingId element Id.
     *
     *  @return the CourseOfferingId element Id
     */

    public static org.osid.id.Id getCourseOfferingId() {
        return (makeElementId("osid.course.activity.CourseOfferingId"));
    }


    /**
     *  Gets the CourseOffering element Id.
     *
     *  @return the CourseOffering element Id
     */

    public static org.osid.id.Id getCourseOffering() {
        return (makeElementId("osid.course.activity.CourseOffering"));
    }


    /**
     *  Gets the TermId element Id.
     *
     *  @return the TermId element Id
     */

    public static org.osid.id.Id getTermId() {
        return (makeElementId("osid.course.activity.TermId"));
    }


    /**
     *  Gets the Term element Id.
     *
     *  @return the Term element Id
     */

    public static org.osid.id.Id getTerm() {
        return (makeElementId("osid.course.activity.Term"));
    }


    /**
     *  Gets the ScheduleIds element Id.
     *
     *  @return the ScheduleIds element Id
     */

    public static org.osid.id.Id getScheduleIds() {
        return (makeElementId("osid.course.activity.ScheduleIds"));
    }


    /**
     *  Gets the Schedules element Id.
     *
     *  @return the Schedules element Id
     */

    public static org.osid.id.Id getSchedules() {
        return (makeElementId("osid.course.activity.Schedules"));
    }


    /**
     *  Gets the SupersedingActivityIds element Id.
     *
     *  @return the SupersedingActivityIds element Id
     */

    public static org.osid.id.Id getSupersedingActivityIds() {
        return (makeElementId("osid.course.activity.SupersedingActivityIds"));
    }


    /**
     *  Gets the SupersedingActivities element Id.
     *
     *  @return the SupersedingActivities element Id
     */

    public static org.osid.id.Id getSupersedingActivities() {
        return (makeElementId("osid.course.activity.SupersedingActivities"));
    }


    /**
     *  Gets the SpecificMeetingTimes element Id.
     *
     *  @return the SpecificMeetingTimes element Id
     */

    public static org.osid.id.Id getSpecificMeetingTimes() {
        return (makeElementId("osid.course.activity.SpecificMeetingTimes"));
    }


    /**
     *  Gets the Blackouts element Id.
     *
     *  @return the Blackouts element Id
     */

    public static org.osid.id.Id getBlackouts() {
        return (makeElementId("osid.course.activity.Blackouts"));
    }


    /**
     *  Gets the InstructorIds element Id.
     *
     *  @return the InstructorIds element Id
     */

    public static org.osid.id.Id getInstructorIds() {
        return (makeElementId("osid.course.activity.InstructorIds"));
    }


    /**
     *  Gets the Instructors element Id.
     *
     *  @return the Instructors element Id
     */

    public static org.osid.id.Id getInstructors() {
        return (makeElementId("osid.course.activity.Instructors"));
    }


    /**
     *  Gets the MinimumSeats element Id.
     *
     *  @return the MinimumSeats element Id
     */

    public static org.osid.id.Id getMinimumSeats() {
        return (makeElementId("osid.course.activity.MinimumSeats"));
    }


    /**
     *  Gets the MaximumSeats element Id.
     *
     *  @return the MaximumSeats element Id
     */

    public static org.osid.id.Id getMaximumSeats() {
        return (makeElementId("osid.course.activity.MaximumSeats"));
    }


    /**
     *  Gets the TotalTargetEffort element Id.
     *
     *  @return the TotalTargetEffort element Id
     */

    public static org.osid.id.Id getTotalTargetEffort() {
        return (makeElementId("osid.course.activity.TotalTargetEffort"));
    }


    /**
     *  Gets the TotalTargetContactTime element Id.
     *
     *  @return the TotalTargetContactTime element Id
     */

    public static org.osid.id.Id getTotalTargetContactTime() {
        return (makeElementId("osid.course.activity.TotalTargetContactTime"));
    }


    /**
     *  Gets the TotalTargetIndividualEffort element Id.
     *
     *  @return the TotalTargetIndividualEffort element Id
     */

    public static org.osid.id.Id getTotalTargetIndividualEffort() {
        return (makeElementId("osid.course.activity.TotalTargetIndividualEffort"));
    }


    /**
     *  Gets the WeeklyEffort element Id.
     *
     *  @return the WeeklyEffort element Id
     */

    public static org.osid.id.Id getWeeklyEffort() {
        return (makeElementId("osid.course.activity.WeeklyEffort"));
    }


    /**
     *  Gets the WeeklyContactTime element Id.
     *
     *  @return the WeeklyContactTime element Id
     */

    public static org.osid.id.Id getWeeklyContactTime() {
        return (makeElementId("osid.course.activity.WeeklyContactTime"));
    }


    /**
     *  Gets the WeeklyIndividualEffort element Id.
     *
     *  @return the WeeklyIndividualEffort element Id
     */

    public static org.osid.id.Id getWeeklyIndividualEffort() {
        return (makeElementId("osid.course.activity.WeeklyIndividualEffort"));
    }


    /**
     *  Gets the Implicit element Id.
     *
     *  @return the Implicit element Id
     */

    public static org.osid.id.Id getImplicit() {
        return (makeElementId("osid.course.activity.Implicit"));
    }


    /**
     *  Gets the MeetingTime element Id.
     *
     *  @return the MeetingTime element Id
     */

    public static org.osid.id.Id getMeetingTime() {
        return (makeQueryElementId("osid.course.activity.MeetingTime"));
    }


    /**
     *  Gets the MeetingTimeInclusive element Id.
     *
     *  @return the MeetingTimeInclusive element Id
     */

    public static org.osid.id.Id getMeetingTimeInclusive() {
        return (makeQueryElementId("osid.course.activity.MeetingTimeInclusive"));
    }


    /**
     *  Gets the MeetingLocationId element Id.
     *
     *  @return the MeetingLocationId element Id
     */

    public static org.osid.id.Id getMeetingLocationId() {
        return (makeQueryElementId("osid.course.activity.MeetingLocationId"));
    }


    /**
     *  Gets the MeetingLocation element Id.
     *
     *  @return the MeetingLocation element Id
     */

    public static org.osid.id.Id getMeetingLocation() {
        return (makeQueryElementId("osid.course.activity.MeetingLocation"));
    }


    /**
     *  Gets the BlackoutInclusive element Id.
     *
     *  @return the BlackoutInclusive element Id
     */

    public static org.osid.id.Id getBlackoutInclusive() {
        return (makeQueryElementId("osid.course.activity.BlackoutInclusive"));
    }


    /**
     *  Gets the Contact element Id.
     *
     *  @return the Contact element Id
     */

    public static org.osid.id.Id getContact() {
        return (makeElementId("osid.course.activity.Contact"));
    }


    /**
     *  Gets the RecurringWeekly element Id.
     *
     *  @return the RecurringWeekly element Id
     */

    public static org.osid.id.Id getRecurringWeekly() {
        return (makeElementId("osid.course.activity.RecurringWeekly"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.activity.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.activity.CourseCatalog"));
    }


    /**
     *  Gets the MaxmimumSeats element Id.
     *
     *  @return the MaxmimumSeats element Id
     */

    public static org.osid.id.Id getMaxmimumSeats() {
        return (makeElementId("osid.course.activity.MaxmimumSeats"));
    }
}
