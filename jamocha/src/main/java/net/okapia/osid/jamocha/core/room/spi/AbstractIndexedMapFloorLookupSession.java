//
// AbstractIndexedMapFloorLookupSession.java
//
//    A simple framework for providing a Floor lookup service
//    backed by a fixed collection of floors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Floor lookup service backed by a
 *  fixed collection of floors. The floors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some floors may be compatible
 *  with more types than are indicated through these floor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Floors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapFloorLookupSession
    extends AbstractMapFloorLookupSession
    implements org.osid.room.FloorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.Floor> floorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Floor>());
    private final MultiMap<org.osid.type.Type, org.osid.room.Floor> floorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Floor>());


    /**
     *  Makes a <code>Floor</code> available in this session.
     *
     *  @param  floor a floor
     *  @throws org.osid.NullArgumentException <code>floor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putFloor(org.osid.room.Floor floor) {
        super.putFloor(floor);

        this.floorsByGenus.put(floor.getGenusType(), floor);
        
        try (org.osid.type.TypeList types = floor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.floorsByRecord.put(types.getNextType(), floor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a floor from this session.
     *
     *  @param floorId the <code>Id</code> of the floor
     *  @throws org.osid.NullArgumentException <code>floorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeFloor(org.osid.id.Id floorId) {
        org.osid.room.Floor floor;
        try {
            floor = getFloor(floorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.floorsByGenus.remove(floor.getGenusType());

        try (org.osid.type.TypeList types = floor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.floorsByRecord.remove(types.getNextType(), floor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeFloor(floorId);
        return;
    }


    /**
     *  Gets a <code>FloorList</code> corresponding to the given
     *  floor genus <code>Type</code> which does not include
     *  floors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known floors or an error results. Otherwise,
     *  the returned list may contain only those floors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  floorGenusType a floor genus type 
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByGenusType(org.osid.type.Type floorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.floor.ArrayFloorList(this.floorsByGenus.get(floorGenusType)));
    }


    /**
     *  Gets a <code>FloorList</code> containing the given
     *  floor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known floors or an error
     *  results. Otherwise, the returned list may contain only those
     *  floors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  floorRecordType a floor record type 
     *  @return the returned <code>floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByRecordType(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.floor.ArrayFloorList(this.floorsByRecord.get(floorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.floorsByGenus.clear();
        this.floorsByRecord.clear();

        super.close();

        return;
    }
}
