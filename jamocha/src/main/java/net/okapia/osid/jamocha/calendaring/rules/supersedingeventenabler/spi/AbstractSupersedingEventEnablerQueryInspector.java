//
// AbstractSupersedingEventEnablerQueryInspector.java
//
//     A template for making a SupersedingEventEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for superseding event enablers.
 */

public abstract class AbstractSupersedingEventEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.calendaring.rules.SupersedingEventEnablerQueryInspector {

    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the superseding event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSupersedingEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the superseding event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQueryInspector[] getRuledSupersedingEventTerms() {
        return (new org.osid.calendaring.SupersedingEventQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given superseding event enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a superseding event enabler implementing the requested record.
     *
     *  @param supersedingEventEnablerRecordType a superseding event enabler record type
     *  @return the superseding event enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord getSupersedingEventEnablerQueryInspectorRecord(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this superseding event enabler query. 
     *
     *  @param supersedingEventEnablerQueryInspectorRecord superseding event enabler query inspector
     *         record
     *  @param supersedingEventEnablerRecordType supersedingEventEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSupersedingEventEnablerQueryInspectorRecord(org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord supersedingEventEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type supersedingEventEnablerRecordType) {

        addRecordType(supersedingEventEnablerRecordType);
        nullarg(supersedingEventEnablerRecordType, "superseding event enabler record type");
        this.records.add(supersedingEventEnablerQueryInspectorRecord);        
        return;
    }
}
