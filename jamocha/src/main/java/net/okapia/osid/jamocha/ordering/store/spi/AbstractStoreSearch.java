//
// AbstractStoreSearch.java
//
//     A template for making a Store Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.store.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing store searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractStoreSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ordering.StoreSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ordering.records.StoreSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ordering.StoreSearchOrder storeSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of stores. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  storeIds list of stores
     *  @throws org.osid.NullArgumentException
     *          <code>storeIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongStores(org.osid.id.IdList storeIds) {
        while (storeIds.hasNext()) {
            try {
                this.ids.add(storeIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongStores</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of store Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getStoreIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  storeSearchOrder store search order 
     *  @throws org.osid.NullArgumentException
     *          <code>storeSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>storeSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderStoreResults(org.osid.ordering.StoreSearchOrder storeSearchOrder) {
	this.storeSearchOrder = storeSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ordering.StoreSearchOrder getStoreSearchOrder() {
	return (this.storeSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given store search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a store implementing the requested record.
     *
     *  @param storeSearchRecordType a store search record
     *         type
     *  @return the store search record
     *  @throws org.osid.NullArgumentException
     *          <code>storeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(storeSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.StoreSearchRecord getStoreSearchRecord(org.osid.type.Type storeSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ordering.records.StoreSearchRecord record : this.records) {
            if (record.implementsRecordType(storeSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(storeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this store search. 
     *
     *  @param storeSearchRecord store search record
     *  @param storeSearchRecordType store search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStoreSearchRecord(org.osid.ordering.records.StoreSearchRecord storeSearchRecord, 
                                           org.osid.type.Type storeSearchRecordType) {

        addRecordType(storeSearchRecordType);
        this.records.add(storeSearchRecord);        
        return;
    }
}
