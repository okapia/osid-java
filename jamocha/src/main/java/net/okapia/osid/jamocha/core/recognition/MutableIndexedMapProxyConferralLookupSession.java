//
// MutableIndexedMapProxyConferralLookupSession
//
//    Implements a Conferral lookup service backed by a collection of
//    conferrals indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements a Conferral lookup service backed by a collection of
 *  conferrals. The conferrals are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some conferrals may be compatible
 *  with more types than are indicated through these conferral
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of conferrals can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyConferralLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractIndexedMapConferralLookupSession
    implements org.osid.recognition.ConferralLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyConferralLookupSession} with
     *  no conferral.
     *
     *  @param academy the academy
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyConferralLookupSession(org.osid.recognition.Academy academy,
                                                       org.osid.proxy.Proxy proxy) {
        setAcademy(academy);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyConferralLookupSession} with
     *  a single conferral.
     *
     *  @param academy the academy
     *  @param  conferral an conferral
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code conferral}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyConferralLookupSession(org.osid.recognition.Academy academy,
                                                       org.osid.recognition.Conferral conferral, org.osid.proxy.Proxy proxy) {

        this(academy, proxy);
        putConferral(conferral);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyConferralLookupSession} using
     *  an array of conferrals.
     *
     *  @param academy the academy
     *  @param  conferrals an array of conferrals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code conferrals}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyConferralLookupSession(org.osid.recognition.Academy academy,
                                                       org.osid.recognition.Conferral[] conferrals, org.osid.proxy.Proxy proxy) {

        this(academy, proxy);
        putConferrals(conferrals);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyConferralLookupSession} using
     *  a collection of conferrals.
     *
     *  @param academy the academy
     *  @param  conferrals a collection of conferrals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code conferrals}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyConferralLookupSession(org.osid.recognition.Academy academy,
                                                       java.util.Collection<? extends org.osid.recognition.Conferral> conferrals,
                                                       org.osid.proxy.Proxy proxy) {
        this(academy, proxy);
        putConferrals(conferrals);
        return;
    }

    
    /**
     *  Makes a {@code Conferral} available in this session.
     *
     *  @param  conferral a conferral
     *  @throws org.osid.NullArgumentException {@code conferral{@code 
     *          is {@code null}
     */

    @Override
    public void putConferral(org.osid.recognition.Conferral conferral) {
        super.putConferral(conferral);
        return;
    }


    /**
     *  Makes an array of conferrals available in this session.
     *
     *  @param  conferrals an array of conferrals
     *  @throws org.osid.NullArgumentException {@code conferrals{@code 
     *          is {@code null}
     */

    @Override
    public void putConferrals(org.osid.recognition.Conferral[] conferrals) {
        super.putConferrals(conferrals);
        return;
    }


    /**
     *  Makes collection of conferrals available in this session.
     *
     *  @param  conferrals a collection of conferrals
     *  @throws org.osid.NullArgumentException {@code conferral{@code 
     *          is {@code null}
     */

    @Override
    public void putConferrals(java.util.Collection<? extends org.osid.recognition.Conferral> conferrals) {
        super.putConferrals(conferrals);
        return;
    }


    /**
     *  Removes a Conferral from this session.
     *
     *  @param conferralId the {@code Id} of the conferral
     *  @throws org.osid.NullArgumentException {@code conferralId{@code  is
     *          {@code null}
     */

    @Override
    public void removeConferral(org.osid.id.Id conferralId) {
        super.removeConferral(conferralId);
        return;
    }    
}
