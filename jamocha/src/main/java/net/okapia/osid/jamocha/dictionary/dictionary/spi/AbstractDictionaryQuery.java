//
// AbstractDictionaryQuery.java
//
//     A template for making a Dictionary Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for dictionaries.
 */

public abstract class AbstractDictionaryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.dictionary.DictionaryQuery {

    private final java.util.Collection<org.osid.dictionary.records.DictionaryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the entry <code> Id </code>.
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievale produce a nested 
     *  boolean <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches dictionaries with any entry. 
     *
     *  @param  match <code> true </code> to match dictionaries with any 
     *          entry, <code> false </code> to match dictionaries with no 
     *          entry 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        return;
    }


    /**
     *  Sets the dictionary <code> Id </code> for to match dictionaries in 
     *  which the specified dictionary is an acestor. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorDictionaryId(org.osid.id.Id dictionaryId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the ancestor dictionary <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorDictionaryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DictionaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a dictionary query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorDictionaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ancestor dictionary. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the dictionary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorDictionaryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuery getAncestorDictionaryQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorDictionaryQuery() is false");
    }


    /**
     *  Matches a dictionary that has any ancestor. 
     *
     *  @param  match <code> true </code> to match dictionaries with any 
     *          ancestor dictionaries, <code> false </code> to match root 
     *          dictionaries 
     */

    @OSID @Override
    public void matchAnyAncestorDictionary(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor dictionary terms. 
     */

    @OSID @Override
    public void clearAncestorDictionaryTerms() {
        return;
    }


    /**
     *  Sets the dictionary <code> Id </code> for to match dictionaries in 
     *  which the specified dictionary is a descendant. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantDictionaryId(org.osid.id.Id dictionaryId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the dictionary <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantDictionaryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DictionaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a dictionary query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantDictionaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a descendant dictioonary. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the dictionary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantDictionaryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuery getDescendantDictionaryQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantDictionaryQuery() is false");
    }


    /**
     *  Matches a dictionary that has any descendant. 
     *
     *  @param  match <code> true </code> to match dictionaries with any 
     *          descendant dictionaries, <code> false </code> to match leaf 
     *          dictionaries 
     */

    @OSID @Override
    public void matchAnyDescendantDictionary(boolean match) {
        return;
    }


    /**
     *  Clears the dictionary terms. 
     */

    @OSID @Override
    public void clearDescendantDictionaryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given dictionary query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a dictionary implementing the requested record.
     *
     *  @param dictionaryRecordType a dictionary record type
     *  @return the dictionary query record
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dictionaryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.DictionaryQueryRecord getDictionaryQueryRecord(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.DictionaryQueryRecord record : this.records) {
            if (record.implementsRecordType(dictionaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dictionaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this dictionary query. 
     *
     *  @param dictionaryQueryRecord dictionary query record
     *  @param dictionaryRecordType dictionary record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDictionaryQueryRecord(org.osid.dictionary.records.DictionaryQueryRecord dictionaryQueryRecord, 
                                          org.osid.type.Type dictionaryRecordType) {

        addRecordType(dictionaryRecordType);
        nullarg(dictionaryQueryRecord, "dictionary query record");
        this.records.add(dictionaryQueryRecord);        
        return;
    }
}
