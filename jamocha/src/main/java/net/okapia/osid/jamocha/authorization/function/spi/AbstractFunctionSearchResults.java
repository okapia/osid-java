//
// AbstractFunctionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractFunctionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.authorization.FunctionSearchResults {

    private org.osid.authorization.FunctionList functions;
    private final org.osid.authorization.FunctionQueryInspector inspector;
    private final java.util.Collection<org.osid.authorization.records.FunctionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractFunctionSearchResults.
     *
     *  @param functions the result set
     *  @param functionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>functions</code>
     *          or <code>functionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractFunctionSearchResults(org.osid.authorization.FunctionList functions,
                                            org.osid.authorization.FunctionQueryInspector functionQueryInspector) {
        nullarg(functions, "functions");
        nullarg(functionQueryInspector, "function query inspectpr");

        this.functions = functions;
        this.inspector = functionQueryInspector;

        return;
    }


    /**
     *  Gets the function list resulting from a search.
     *
     *  @return a function list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctions() {
        if (this.functions == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.authorization.FunctionList functions = this.functions;
        this.functions = null;
	return (functions);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.authorization.FunctionQueryInspector getFunctionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  function search record <code> Type. </code> This method must
     *  be used to retrieve a function implementing the requested
     *  record.
     *
     *  @param functionSearchRecordType a function search 
     *         record type 
     *  @return the function search
     *  @throws org.osid.NullArgumentException
     *          <code>functionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(functionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionSearchResultsRecord getFunctionSearchResultsRecord(org.osid.type.Type functionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.authorization.records.FunctionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(functionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(functionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record function search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addFunctionRecord(org.osid.authorization.records.FunctionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "function record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
