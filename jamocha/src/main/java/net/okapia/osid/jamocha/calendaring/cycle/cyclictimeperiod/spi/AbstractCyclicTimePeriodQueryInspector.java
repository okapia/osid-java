//
// AbstractCyclicTimePeriodQueryInspector.java
//
//     A template for making a CyclicTimePeriodQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for cyclic time periods.
 */

public abstract class AbstractCyclicTimePeriodQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.calendaring.cycle.CyclicTimePeriodQueryInspector {

    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given cyclic time period query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a cyclic time period implementing the requested record.
     *
     *  @param cyclicTimePeriodRecordType a cyclic time period record type
     *  @return the cyclic time period query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicTimePeriodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord getCyclicTimePeriodQueryInspectorRecord(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(cyclicTimePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicTimePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this cyclic time period query. 
     *
     *  @param cyclicTimePeriodQueryInspectorRecord cyclic time period query inspector
     *         record
     *  @param cyclicTimePeriodRecordType cyclicTimePeriod record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCyclicTimePeriodQueryInspectorRecord(org.osid.calendaring.cycle.records.CyclicTimePeriodQueryInspectorRecord cyclicTimePeriodQueryInspectorRecord, 
                                                   org.osid.type.Type cyclicTimePeriodRecordType) {

        addRecordType(cyclicTimePeriodRecordType);
        nullarg(cyclicTimePeriodRecordType, "cyclic time period record type");
        this.records.add(cyclicTimePeriodQueryInspectorRecord);        
        return;
    }
}
