//
// AbstractFederatingFrontOfficeLookupSession.java
//
//     An abstract federating adapter for a FrontOfficeLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  FrontOfficeLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingFrontOfficeLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.tracking.FrontOfficeLookupSession>
    implements org.osid.tracking.FrontOfficeLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingFrontOfficeLookupSession</code>.
     */

    protected AbstractFederatingFrontOfficeLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.tracking.FrontOfficeLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>FrontOffice</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFrontOffices() {
        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            if (session.canLookupFrontOffices()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>FrontOffice</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFrontOfficeView() {
        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            session.useComparativeFrontOfficeView();
        }

        return;
    }


    /**
     *  A complete view of the <code>FrontOffice</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFrontOfficeView() {
        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            session.usePlenaryFrontOfficeView();
        }

        return;
    }

     
    /**
     *  Gets the <code>FrontOffice</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>FrontOffice</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>FrontOffice</code> and
     *  retained for compatibility.
     *
     *  @param  frontOfficeId <code>Id</code> of the
     *          <code>FrontOffice</code>
     *  @return the front office
     *  @throws org.osid.NotFoundException <code>frontOfficeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>frontOfficeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            try {
                return (session.getFrontOffice(frontOfficeId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(frontOfficeId + " not found");
    }


    /**
     *  Gets a <code>FrontOfficeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  frontOffices specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>FrontOffices</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  @param  frontOfficeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByIds(org.osid.id.IdList frontOfficeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.tracking.frontoffice.MutableFrontOfficeList ret = new net.okapia.osid.jamocha.tracking.frontoffice.MutableFrontOfficeList();

        try (org.osid.id.IdList ids = frontOfficeIds) {
            while (ids.hasNext()) {
                ret.addFrontOffice(getFrontOffice(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>FrontOfficeList</code> corresponding to the given
     *  front office genus <code>Type</code> which does not include
     *  front offices of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known front
     *  offices or an error results. Otherwise, the returned list may
     *  contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeGenusType a frontOffice genus type 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByGenusType(org.osid.type.Type frontOfficeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.FederatingFrontOfficeList ret = getFrontOfficeList();

        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            ret.addFrontOfficeList(session.getFrontOfficesByGenusType(frontOfficeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FrontOfficeList</code> corresponding to the given
     *  front office genus <code>Type</code> and include any additional
     *  front offices with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known front
     *  offices or an error results. Otherwise, the returned list may
     *  contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeGenusType a frontOffice genus type 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByParentGenusType(org.osid.type.Type frontOfficeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.FederatingFrontOfficeList ret = getFrontOfficeList();

        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            ret.addFrontOfficeList(session.getFrontOfficesByParentGenusType(frontOfficeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FrontOfficeList</code> containing the given
     *  front office record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known front
     *  offices or an error results. Otherwise, the returned list may
     *  contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeRecordType a frontOffice record type 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByRecordType(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.FederatingFrontOfficeList ret = getFrontOfficeList();

        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            ret.addFrontOfficeList(session.getFrontOfficesByRecordType(frontOfficeRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FrontOfficeList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known front offices or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  front offices that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>FrontOffice</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.FederatingFrontOfficeList ret = getFrontOfficeList();

        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            ret.addFrontOfficeList(session.getFrontOfficesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>FrontOffices</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  front offices or an error results. Otherwise, the returned list
     *  may contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>FrontOffices</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.FederatingFrontOfficeList ret = getFrontOfficeList();

        for (org.osid.tracking.FrontOfficeLookupSession session : getSessions()) {
            ret.addFrontOfficeList(session.getFrontOffices());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.FederatingFrontOfficeList getFrontOfficeList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.ParallelFrontOfficeList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.frontoffice.CompositeFrontOfficeList());
        }
    }
}
