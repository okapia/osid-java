//
// AbstractAuthorizationLookupSession.java
//
//    A starter implementation framework for providing an Authorization
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Authorization
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAuthorizations(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAuthorizationLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.authorization.AuthorizationLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private boolean implicit      = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();
    

    /**
     *  Gets the <code>Vault/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.vault.getId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.vault);
    }


    /**
     *  Sets the <code>Vault</code>.
     *
     *  @param  vault the vault for this session
     *  @throws org.osid.NullArgumentException <code>vault</code>
     *          is <code>null</code>
     */

    protected void setVault(org.osid.authorization.Vault vault) {
        nullarg(vault, "vault");
        this.vault = vault;
        return;
    }

    /**
     *  Tests if this user can perform <code>Authorization</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuthorizations() {
        return (true);
    }


    /**
     *  A complete view of the <code>Authorization</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuthorizationView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Authorization</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuthorizationView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include authorizations in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only authorizations whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveAuthorizationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All authorizations of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveAuthorizationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }


    /**
     *  Sets the view for methods in this session to implicit
     *  authorizations.  An implicit view will include authorizations
     *  derived from other authorizations as a result of the <code>
     *  Qualifier</code>, <code>Function</code>, or <code> Resource
     *  </code> hierarchies. This method is the opposite of <code>
     *  explicitAuthorizationView()</code>.
     */

    @OSID @Override
    public void useImplicitAuthorizationView() {
        this.implicit = true;
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit
     *  authorizations.  An explicit view includes only those
     *  authorizations that were explicitly defined and not
     *  implied. This method is the opposite of <code>
     *  implicitAuthorizationView(). </code>
     */

    @OSID @Override
    public void useExplicitAuthorizationView() {
        this.implicit = false;
        return;
    }

    
    /**
     *  Tests if an implicit or explicit view is set.
     *
     *  @return <code>true</code> if explicit and implicit
     *          authorizations should be returned</code>,
     *          <code>false</code> if only explicit authorizations
     */

    protected boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Gets the <code>Authorization</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Authorization</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>Authorization</code> and retained for compatibility.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective. In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  authorizationId <code>Id</code> of the
     *          <code>Authorization</code>
     *  @return the authorization
     *  @throws org.osid.NotFoundException <code>authorizationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>authorizationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Authorization getAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authorization.AuthorizationList authorizations = getAuthorizations()) {
            while (authorizations.hasNext()) {
                org.osid.authorization.Authorization authorization = authorizations.getNextAuthorization();
                if (authorization.getId().equals(authorizationId)) {
                    return (authorization);
                }
            }
        } 

        throw new org.osid.NotFoundException(authorizationId + " not found");
    }


    /**
     *  Gets a <code>AuthorizationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  authorizations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Authorizations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, authorizations are returned that are currently effective.
     *  In any effective mode, effective authorizations and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAuthorizations()</code>.
     *
     *  @param  authorizationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByIds(org.osid.id.IdList authorizationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.authorization.Authorization> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = authorizationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAuthorization(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("authorization " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.authorization.authorization.LinkedAuthorizationList(ret));
    }


    /**
     *  Gets a <code>AuthorizationList</code> corresponding to the
     *  given authorization genus <code>Type</code> which does not
     *  include authorizations of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAuthorizations()</code>.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationGenusFilterList(getAuthorizations(), authorizationGenusType));
    }


    /**
     *  Gets a <code>AuthorizationList</code> corresponding to the
     *  given authorization genus <code>Type</code> and include any
     *  additional authorizations with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuthorizations()</code>.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByParentGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuthorizationsByGenusType(authorizationGenusType));
    }


    /**
     *  Gets a <code>AuthorizationList</code> containing the given
     *  authorization record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, [hobjects are returned that are currently
     *  effective.  In any effective mode, effective authorizations
     *  and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuthorizations()</code>.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByRecordType(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationRecordFilterList(getAuthorizations(), authorizationRecordType));
    }


    /**
     *  Gets an <code> AuthorizationList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective. In any effective mode, active authorizations and those
     *  currently expired are returned.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> Authorization </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsOnDate(org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.TemporalAuthorizationFilterList(getAuthorizations(), from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilterList(new ResourceFilter(resourceId), getAuthorizations()));
     }


    /**
     *  Gets a list of authorizations corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceOnDate(org.osid.id.Id resourceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.TemporalAuthorizationFilterList(getAuthorizationsForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForAgent(org.osid.id.Id agentId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilterList(new AgentFilter(agentId), getAuthorizations()));
     }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentOnDate(org.osid.id.Id agentId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.TemporalAuthorizationFilterList(getAuthorizationsForAgent(agentId), from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>function</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForFunction(org.osid.id.Id functionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilterList(new FunctionFilter(functionId), getAuthorizations()));
     }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>function</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForFunctionOnDate(org.osid.id.Id functionId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.TemporalAuthorizationFilterList(getAuthorizationsForFunction(functionId), from, to));        
    }


    /**
     *  Gets a list of authorizations corresponding to resource and
     *  function <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code>,
     *          <code>function</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunction(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id functionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilterList(new FunctionFilter(functionId), getAuthorizationsForResource(resourceId)));
    }


    /**
     *  Gets a list of authorizations corresponding to resource and
     *  function <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>function</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunctionOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id functionId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.TemporalAuthorizationFilterList(getAuthorizationsForResourceAndFunction(resourceId, functionId), from, to));
    }


    /**
     *  Gets a list of authorizations corresponding to agent and
     *  function <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>function</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunction(org.osid.id.Id agentId,
                                                                                         org.osid.id.Id functionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilterList(new FunctionFilter(functionId), getAuthorizationsForAgent(agentId)));
    }


    /**
     *  Gets a list of authorizations corresponding to agent and
     *  function <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>function</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunctionOnDate(org.osid.id.Id agentId,
                                                                                                  org.osid.id.Id functionId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.TemporalAuthorizationFilterList(getAuthorizationsForAgentAndFunction(agentId, functionId), from, to));
    }

    
    /**
     *  Gets a list of <code> Authorizations </code> associated with a given
     *  qualifier. In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list may
     *  contain only those authorizations that are accessible through this
     *  session.
     *
     *  @param  qualifierId a qualifier <code> Id </code>
     *  @return the returned <code> Authorization list </code>
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByQualifier(org.osid.id.Id qualifierId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilterList(new QualifierFilter(qualifierId), getAuthorizations()));        
    }


    /**
     *  Gets the explicit <code> Authorization </code> that generated the
     *  given implicit authorization. If the given <code> Authorization
     *  </code> is explicit, then the same <code> Authorization </code> is
     *  returned.
     *
     *  @param  authorizationId an authorization
     *  @return the explicit <code> Authorization </code>
     *  @throws org.osid.NotFoundException <code> authorizationId </code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code> authorizationId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public abstract org.osid.authorization.Authorization getExplicitAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets all <code>Authorizations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Authorizations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.authorization.AuthorizationList getAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the authorization list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of authorizations
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.authorization.AuthorizationList filterAuthorizationsOnViews(org.osid.authorization.AuthorizationList list)
        throws org.osid.OperationFailedException {

        org.osid.authorization.AuthorizationList ret = list;
        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.authorization.authorization.EffectiveAuthorizationFilterList(ret);
        }

        if (!isImplicit()) {
            ret = new net.okapia.osid.jamocha.inline.filter.authorization.authorization.ImplicitAuthorizationFilterList(ret);
        }

        return (ret);        
    }


    public static class FunctionFilter
        implements net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilter {

        private final org.osid.id.Id functionId;

        
        /**
         *  Constructs a new <code>FunctionFilter</code>.
         *
         *  @param functionId the function to filter
         *  @throws org.osid.NullArgumentException
         *          <code>functionId</code> is <code>null</code>
         */

        public FunctionFilter(org.osid.id.Id functionId) {
            nullarg(functionId, "function Id");
            this.functionId = functionId;
            return;
        }


        /**
         *  Used by the AuthorizationFilterList to filter the authorization list
         *  based on function.
         *
         *  @param authorization the authorization
         *  @return <code>true</code> to pass the authorization,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.authorization.Authorization authorization) {
            return (authorization.getFunctionId().equals(this.functionId));
        }
    }


    public static class QualifierFilter
        implements net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilter {

        private final org.osid.id.Id qualifierId;

        
        /**
         *  Constructs a new <code>QualifierFilter</code>.
         *
         *  @param qualifierId the qualifier to filter
         *  @throws org.osid.NullArgumentException
         *          <code>qualifierId</code> is <code>null</code>
         */

        public QualifierFilter(org.osid.id.Id qualifierId) {
            nullarg(qualifierId, "qualifier Id");
            this.qualifierId = qualifierId;
            return;
        }


        /**
         *  Used by the AuthorizationFilterList to filter the authorization list
         *  based on qualifier.
         *
         *  @param authorization the authorization
         *  @return <code>true</code> to pass the authorization,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.authorization.Authorization authorization) {
            return (authorization.getQualifierId().equals(this.qualifierId));
        }
    }


    public static class ResourceFilter       
        implements net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the AuthorizationFilterList to filter the authorization list
         *  based on resource.
         *
         *  @param authorization the authorization
         *  @return <code>true</code> to pass the authorization,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.authorization.Authorization authorization) {
            if (authorization.hasResource()) {
                return (authorization.getResourceId().equals(this.resourceId));
            } else {
                return (false);
            }
        }
    }


    public static class AgentFilter
        implements net.okapia.osid.jamocha.inline.filter.authorization.authorization.AuthorizationFilter {

        private final org.osid.id.Id agentId;

        
        /**
         *  Constructs a new <code>AgentFilter</code>.
         *
         *  @param agentId the agent to filter
         *  @throws org.osid.NullArgumentException
         *          <code>agentId</code> is <code>null</code>
         */

        public AgentFilter(org.osid.id.Id agentId) {
            nullarg(agentId, "agent Id");
            this.agentId = agentId;
            return;
        }


        /**
         *  Used by the AuthorizationFilterList to filter the authorization list
         *  based on agent.
         *
         *  @param authorization the authorization
         *  @return <code>true</code> to pass the authorization,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.authorization.Authorization authorization) {
            if (authorization.hasAgent()) {
                return (authorization.getAgentId().equals(this.agentId));
            } else {
                return (false);
            }
        }
    }
}
