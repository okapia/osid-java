//
// AbstractProduct.java
//
//     Defines a Product.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Product</code>.
 */

public abstract class AbstractProduct
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.ordering.Product {

    private String code;
    private final java.util.Collection<org.osid.ordering.PriceSchedule> priceSchedules = new java.util.LinkedHashSet<>();
    private long availability = -1;

    private final java.util.Collection<org.osid.ordering.records.ProductRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the product code. 
     *
     *  @return a product code 
     */

    @OSID @Override
    public String getCode() {
        return (this.code);
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    protected void setCode(String code) {
        nullarg(code, "code");
        this.code = code;
        return;
    }


    /**
     *  Gets the price schedule <code> Ids. </code> 
     *
     *  @return a price schedule <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPriceScheduleIds() {
        try {
            org.osid.ordering.PriceScheduleList priceSchedules = getPriceSchedules();
            return (new net.okapia.osid.jamocha.adapter.converter.ordering.priceschedule.PriceScheduleToIdList(priceSchedules));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the price schedules. 
     *
     *  @return the price schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedules()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.ordering.priceschedule.ArrayPriceScheduleList(this.priceSchedules));
    }


    /**
     *  Adds a price schedule.
     *
     *  @param schedule a price schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    protected void addPriceSchedule(org.osid.ordering.PriceSchedule schedule) {
        nullarg(schedule, "price schedule");
        this.priceSchedules.add(schedule);
        return;
    }


    /**
     *  Sets all the price schedules.
     *
     *  @param schedules a collection of price schedules
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    protected void setPriceSchedules(java.util.Collection<org.osid.ordering.PriceSchedule> schedules) {
        nullarg(schedules, "price schedules");
        this.priceSchedules.clear();
        this.priceSchedules.addAll(schedules);
        return;
    }


    /**
     *  Tests if an availability is available for this product. 
     *
     *  @return <code> true </code> if an available is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasAvailability() {
        if (this.availability < 0) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the availability. 
     *
     *  @return the availability 
     *  @throws org.osid.IllegalStateException <code> hasAvailability() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getAvailability() {
        if (!hasAvailability()) {
            throw new org.osid.IllegalStateException("hasAvailability() is false");
        }

        return (this.availability);
    }


    /**
     *  Sets the availability.
     *
     *  @param availability an availability
     *  @throws org.osid.InvalidArgumentException
     *          <code>availability</code> is negative
     */

    protected void setAvailability(long availability) {
        cardinalarg(availability, "availability");
        this.availability = availability;
        return;
    }


    /**
     *  Tests if this product supports the given record
     *  <code>Type</code>.
     *
     *  @param  productRecordType a product record type 
     *  @return <code>true</code> if the productRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type productRecordType) {
        for (org.osid.ordering.records.ProductRecord record : this.records) {
            if (record.implementsRecordType(productRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Product</code> record <code>Type</code>.
     *
     *  @param  productRecordType the product record type 
     *  @return the product record 
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(productRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductRecord getProductRecord(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ProductRecord record : this.records) {
            if (record.implementsRecordType(productRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(productRecordType + " is not supported");
    }


    /**
     *  Adds a record to this product. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param productRecord the product record
     *  @param productRecordType product record type
     *  @throws org.osid.NullArgumentException
     *          <code>productRecord</code> or
     *          <code>productRecordTypeproduct</code> is
     *          <code>null</code>
     */
            
    protected void addProductRecord(org.osid.ordering.records.ProductRecord productRecord, 
                                    org.osid.type.Type productRecordType) {
        
        nullarg(productRecord, "product record");
        addRecordType(productRecordType);
        this.records.add(productRecord);
        
        return;
    }
}
