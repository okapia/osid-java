//
// MutableIndexedMapWorkLookupSession
//
//    Implements a Work lookup service backed by a collection of
//    works indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Work lookup service backed by a collection of
 *  works. The works are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some works may be compatible
 *  with more types than are indicated through these work
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of works can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapWorkLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractIndexedMapWorkLookupSession
    implements org.osid.resourcing.WorkLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapWorkLookupSession} with no works.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry}
     *          is {@code null}
     */

      public MutableIndexedMapWorkLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapWorkLookupSession} with a
     *  single work.
     *  
     *  @param foundry the foundry
     *  @param  work a single work
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code work} is {@code null}
     */

    public MutableIndexedMapWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Work work) {
        this(foundry);
        putWork(work);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapWorkLookupSession} using an
     *  array of works.
     *
     *  @param foundry the foundry
     *  @param  works an array of works
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code works} is {@code null}
     */

    public MutableIndexedMapWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Work[] works) {
        this(foundry);
        putWorks(works);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapWorkLookupSession} using a
     *  collection of works.
     *
     *  @param foundry the foundry
     *  @param  works a collection of works
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code works} is {@code null}
     */

    public MutableIndexedMapWorkLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Work> works) {

        this(foundry);
        putWorks(works);
        return;
    }
    

    /**
     *  Makes a {@code Work} available in this session.
     *
     *  @param  work a work
     *  @throws org.osid.NullArgumentException {@code work{@code  is
     *          {@code null}
     */

    @Override
    public void putWork(org.osid.resourcing.Work work) {
        super.putWork(work);
        return;
    }


    /**
     *  Makes an array of works available in this session.
     *
     *  @param  works an array of works
     *  @throws org.osid.NullArgumentException {@code works{@code 
     *          is {@code null}
     */

    @Override
    public void putWorks(org.osid.resourcing.Work[] works) {
        super.putWorks(works);
        return;
    }


    /**
     *  Makes collection of works available in this session.
     *
     *  @param  works a collection of works
     *  @throws org.osid.NullArgumentException {@code work{@code  is
     *          {@code null}
     */

    @Override
    public void putWorks(java.util.Collection<? extends org.osid.resourcing.Work> works) {
        super.putWorks(works);
        return;
    }


    /**
     *  Removes a Work from this session.
     *
     *  @param workId the {@code Id} of the work
     *  @throws org.osid.NullArgumentException {@code workId{@code  is
     *          {@code null}
     */

    @Override
    public void removeWork(org.osid.id.Id workId) {
        super.removeWork(workId);
        return;
    }    
}
