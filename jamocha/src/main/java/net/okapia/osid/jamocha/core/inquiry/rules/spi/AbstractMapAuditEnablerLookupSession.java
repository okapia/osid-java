//
// AbstractMapAuditEnablerLookupSession
//
//    A simple framework for providing an AuditEnabler lookup service
//    backed by a fixed collection of audit enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AuditEnabler lookup service backed by a
 *  fixed collection of audit enablers. The audit enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuditEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuditEnablerLookupSession
    extends net.okapia.osid.jamocha.inquiry.rules.spi.AbstractAuditEnablerLookupSession
    implements org.osid.inquiry.rules.AuditEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inquiry.rules.AuditEnabler> auditEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inquiry.rules.AuditEnabler>());


    /**
     *  Makes an <code>AuditEnabler</code> available in this session.
     *
     *  @param  auditEnabler an audit enabler
     *  @throws org.osid.NullArgumentException <code>auditEnabler<code>
     *          is <code>null</code>
     */

    protected void putAuditEnabler(org.osid.inquiry.rules.AuditEnabler auditEnabler) {
        this.auditEnablers.put(auditEnabler.getId(), auditEnabler);
        return;
    }


    /**
     *  Makes an array of audit enablers available in this session.
     *
     *  @param  auditEnablers an array of audit enablers
     *  @throws org.osid.NullArgumentException <code>auditEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuditEnablers(org.osid.inquiry.rules.AuditEnabler[] auditEnablers) {
        putAuditEnablers(java.util.Arrays.asList(auditEnablers));
        return;
    }


    /**
     *  Makes a collection of audit enablers available in this session.
     *
     *  @param  auditEnablers a collection of audit enablers
     *  @throws org.osid.NullArgumentException <code>auditEnablers<code>
     *          is <code>null</code>
     */

    protected void putAuditEnablers(java.util.Collection<? extends org.osid.inquiry.rules.AuditEnabler> auditEnablers) {
        for (org.osid.inquiry.rules.AuditEnabler auditEnabler : auditEnablers) {
            this.auditEnablers.put(auditEnabler.getId(), auditEnabler);
        }

        return;
    }


    /**
     *  Removes an AuditEnabler from this session.
     *
     *  @param  auditEnablerId the <code>Id</code> of the audit enabler
     *  @throws org.osid.NullArgumentException <code>auditEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeAuditEnabler(org.osid.id.Id auditEnablerId) {
        this.auditEnablers.remove(auditEnablerId);
        return;
    }


    /**
     *  Gets the <code>AuditEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  auditEnablerId <code>Id</code> of the <code>AuditEnabler</code>
     *  @return the auditEnabler
     *  @throws org.osid.NotFoundException <code>auditEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>auditEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnabler getAuditEnabler(org.osid.id.Id auditEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inquiry.rules.AuditEnabler auditEnabler = this.auditEnablers.get(auditEnablerId);
        if (auditEnabler == null) {
            throw new org.osid.NotFoundException("auditEnabler not found: " + auditEnablerId);
        }

        return (auditEnabler);
    }


    /**
     *  Gets all <code>AuditEnablers</code>. In plenary mode, the returned
     *  list contains all known auditEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  auditEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AuditEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerList getAuditEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.rules.auditenabler.ArrayAuditEnablerList(this.auditEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auditEnablers.clear();
        super.close();
        return;
    }
}
