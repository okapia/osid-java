//
// InlineCanonicalUnitEnablerNotifier.java
//
//     A callback interface for performing canonical unit enabler
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.rules.spi;


/**
 *  A callback interface for performing canonical unit enabler
 *  notifications.
 */

public interface InlineCanonicalUnitEnablerNotifier {



    /**
     *  Notifies the creation of a new canonical unit enabler.
     *
     *  @param canonicalUnitEnablerId the {@code Id} of the new 
     *         canonical unit enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId);


    /**
     *  Notifies the change of an updated canonical unit enabler.
     *
     *  @param canonicalUnitEnablerId the {@code Id} of the changed
     *         canonical unit enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId);


    /**
     *  Notifies the deletion of an canonical unit enabler.
     *
     *  @param canonicalUnitEnablerId the {@code Id} of the deleted
     *         canonical unit enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId);

}
