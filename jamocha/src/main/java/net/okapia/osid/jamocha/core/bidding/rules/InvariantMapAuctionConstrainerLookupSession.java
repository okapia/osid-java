//
// InvariantMapAuctionConstrainerLookupSession
//
//    Implements an AuctionConstrainer lookup service backed by a fixed collection of
//    auctionConstrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules;


/**
 *  Implements an AuctionConstrainer lookup service backed by a fixed
 *  collection of auction constrainers. The auction constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAuctionConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.bidding.rules.spi.AbstractMapAuctionConstrainerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionConstrainerLookupSession</code> with no
     *  auction constrainers.
     *  
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumnetException {@code auctionHouse} is
     *          {@code null}
     */

    public InvariantMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionConstrainerLookupSession</code> with a single
     *  auction constrainer.
     *  
     *  @param auctionHouse the auction house
     *  @param auctionConstrainer an single auction constrainer
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainer} is <code>null</code>
     */

      public InvariantMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.rules.AuctionConstrainer auctionConstrainer) {
        this(auctionHouse);
        putAuctionConstrainer(auctionConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionConstrainerLookupSession</code> using an array
     *  of auction constrainers.
     *  
     *  @param auctionHouse the auction house
     *  @param auctionConstrainers an array of auction constrainers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainers} is <code>null</code>
     */

      public InvariantMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.rules.AuctionConstrainer[] auctionConstrainers) {
        this(auctionHouse);
        putAuctionConstrainers(auctionConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionConstrainerLookupSession</code> using a
     *  collection of auction constrainers.
     *
     *  @param auctionHouse the auction house
     *  @param auctionConstrainers a collection of auction constrainers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainers} is <code>null</code>
     */

      public InvariantMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               java.util.Collection<? extends org.osid.bidding.rules.AuctionConstrainer> auctionConstrainers) {
        this(auctionHouse);
        putAuctionConstrainers(auctionConstrainers);
        return;
    }
}
