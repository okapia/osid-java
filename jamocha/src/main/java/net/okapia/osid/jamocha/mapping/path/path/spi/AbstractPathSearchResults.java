//
// AbstractPathSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPathSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.path.PathSearchResults {

    private org.osid.mapping.path.PathList paths;
    private final org.osid.mapping.path.PathQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.path.records.PathSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPathSearchResults.
     *
     *  @param paths the result set
     *  @param pathQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>paths</code>
     *          or <code>pathQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPathSearchResults(org.osid.mapping.path.PathList paths,
                                            org.osid.mapping.path.PathQueryInspector pathQueryInspector) {
        nullarg(paths, "paths");
        nullarg(pathQueryInspector, "path query inspectpr");

        this.paths = paths;
        this.inspector = pathQueryInspector;

        return;
    }


    /**
     *  Gets the path list resulting from a search.
     *
     *  @return a path list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPaths() {
        if (this.paths == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.path.PathList paths = this.paths;
        this.paths = null;
	return (paths);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.path.PathQueryInspector getPathQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  path search record <code> Type. </code> This method must
     *  be used to retrieve a path implementing the requested
     *  record.
     *
     *  @param pathSearchRecordType a path search 
     *         record type 
     *  @return the path search
     *  @throws org.osid.NullArgumentException
     *          <code>pathSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(pathSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.PathSearchResultsRecord getPathSearchResultsRecord(org.osid.type.Type pathSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.path.records.PathSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(pathSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(pathSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record path search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPathRecord(org.osid.mapping.path.records.PathSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "path record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
