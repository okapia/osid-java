//
// JobElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.job.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class JobElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the JobElement Id.
     *
     *  @return the job element Id
     */

    public static org.osid.id.Id getJobEntityId() {
        return (makeEntityId("osid.resourcing.Job"));
    }


    /**
     *  Gets the CompetencyIds element Id.
     *
     *  @return the CompetencyIds element Id
     */

    public static org.osid.id.Id getCompetencyIds() {
        return (makeElementId("osid.resourcing.job.CompetencyIds"));
    }


    /**
     *  Gets the Competencies element Id.
     *
     *  @return the Competencies element Id
     */

    public static org.osid.id.Id getCompetencies() {
        return (makeElementId("osid.resourcing.job.Competencies"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeQueryElementId("osid.resourcing.job.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeQueryElementId("osid.resourcing.job.Work"));
    }


    /**
     *  Gets the AvailabilityId element Id.
     *
     *  @return the AvailabilityId element Id
     */

    public static org.osid.id.Id getAvailabilityId() {
        return (makeQueryElementId("osid.resourcing.job.AvailabilityId"));
    }


    /**
     *  Gets the Availability element Id.
     *
     *  @return the Availability element Id
     */

    public static org.osid.id.Id getAvailability() {
        return (makeQueryElementId("osid.resourcing.job.Availability"));
    }


    /**
     *  Gets the FoundryId element Id.
     *
     *  @return the FoundryId element Id
     */

    public static org.osid.id.Id getFoundryId() {
        return (makeQueryElementId("osid.resourcing.job.FoundryId"));
    }


    /**
     *  Gets the Foundry element Id.
     *
     *  @return the Foundry element Id
     */

    public static org.osid.id.Id getFoundry() {
        return (makeQueryElementId("osid.resourcing.job.Foundry"));
    }
}
