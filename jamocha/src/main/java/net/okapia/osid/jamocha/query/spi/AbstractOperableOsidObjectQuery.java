//
// AbstractOperableOsidObjectQuery.java
//
//     An operable OsidObjectQuery with stored terms.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An operable OsidObjectQuery with stored terms.
 */

public abstract class AbstractOperableOsidObjectQuery
    extends AbstractOsidObjectQuery
    implements org.osid.OsidOperableQuery,
               org.osid.OsidObjectQuery {

    private final OsidOperableQuery query;


    /**
     *  Constructs a new <code>AbstractOperableOsidObjectQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractOperableOsidObjectQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        super(factory);
        this.query = new OsidOperableQuery(factory);
        return;
    }


    /**
     *  Matches active. 
     *
     *  @param  match <code> true </code> to match active, <code> false 
     *          </code> to match inactive 
     */

    @OSID @Override
    public void matchActive(boolean match) {
        this.query.matchActive(match);
        return;
    }


    /**
     *  Clears the active query terms. 
     */

    @OSID @Override
    public void clearActiveTerms() {
        this.query.clearActiveTerms();
        return;
    }


    /**
     *  Gets all the active query terms.
     *
     *  @return a collection of the active query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getActiveTerms() {
        return (this.query.getActiveTerms());
    }


    /**
     *  Matches administratively enabled. 
     *
     *  @param  match <code> true </code> to match administratively enabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        this.query.matchEnabled(match);
        return;
    }


    /**
     *  Clears the administratively enabled query terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        this.query.clearEnabledTerms();
        return;
    }


    /**
     *  Gets all the enabled query terms.
     *
     *  @return a collection of the enabled query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getEnabledTerms() {
        return (getEnabledTerms());
    }


    /**
     *  Matches administratively disabled. 
     *
     *  @param  match <code> true </code> to match administratively disabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchDisabled(boolean match) {
        this.query.matchDisabled(match);
        return;
    }


    /**
     *  Clears the administratively disabled query terms. 
     */

    @OSID @Override
    public void clearDisabledTerms() {
        this.query.clearDisabledTerms();
        return;
    }


    /**
     *  Gets all the disabled query terms.
     *
     *  @return a collection of the disabled query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getDisabledTerms() {
        return (this.query.getDisabledTerms());
    }


    /**
     *  Matches operational operables. 
     *
     *  @param  match <code> true </code> to match operational, <code> false 
     *          </code> to match not operational 
     */

    @OSID @Override
    public void matchOperational(boolean match) {
        this.query.matchOperational(match);
        return;
    }


    /**
     *  Clears the operational query terms. 
     */

    @OSID @Override
    public void clearOperationalTerms() {
        this.query.clearOperationalTerms();
        return;
    }


    /**
     *  Gets all the operational query terms.
     *
     *  @return a collection of the operational query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getOperationalTerms() {
        return (this.query.getOperationalTerms());
    }


    protected class OsidOperableQuery 
        extends AbstractOsidOperableQuery
        implements org.osid.OsidOperableQuery {


        /**
         *  Constructs a new <code>OsidOperableQuery</code>.
         *
         *  @param factory the term factory
         *  @throws org.osid.NullArgumentException <code>factory</code> is
         *          <code>null</code>
         */

        protected OsidOperableQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
            super(factory);
            return;
        }


        /**
         *  Matches active. 
         *
         *  @param  match <code> true </code> to match active, <code> false 
         *          </code> to match inactive 
         */

        @OSID @Override
        public void matchActive(boolean match) {
            super.matchActive(match);
            return;
        }


        /**
         *  Clears the active query terms. 
         */

        @OSID @Override
        public void clearActiveTerms() {
            super.clearActiveTerms();
            return;
        }


        /**
         *  Gets all the active query terms.
         *
         *  @return a collection of the active query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.BooleanTerm> getActiveTerms() {
            return (super.getActiveTerms());
        }


        /**
         *  Matches administratively enabled. 
         *
         *  @param  match <code> true </code> to match administratively enabled, 
         *          <code> false </code> otherwise 
         */

        @OSID @Override
        public void matchEnabled(boolean match) {
            super.matchEnabled(match);
            return;
        }


        /**
         *  Clears the administratively enabled query terms. 
         */

        @OSID @Override
        public void clearEnabledTerms() {
            super.clearEnabledTerms();
            return;
        }


        /**
         *  Gets all the enabled query terms.
         *
         *  @return a collection of the enabled query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.BooleanTerm> getEnabledTerms() {
            return (getEnabledTerms());
        }


        /**
         *  Matches administratively disabled. 
         *
         *  @param  match <code> true </code> to match administratively disabled, 
         *          <code> false </code> otherwise 
         */

        @OSID @Override
        public void matchDisabled(boolean match) {
            super.matchDisabled(match);
            return;
        }


        /**
         *  Clears the administratively disabled query terms. 
         */

        @OSID @Override
        public void clearDisabledTerms() {
            super.clearDisabledTerms();
            return;
        }


        /**
         *  Gets all the disabled query terms.
         *
         *  @return a collection of the disabled query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.BooleanTerm> getDisabledTerms() {
            return (super.getDisabledTerms());
        }


        /**
         *  Matches operational operables. 
         *
         *  @param  match <code> true </code> to match operational, <code> false 
         *          </code> to match not operational 
         */

        @OSID @Override
        public void matchOperational(boolean match) {
            super.matchOperational(match);
            return;
        }


        /**
         *  Clears the operational query terms. 
         */

        @OSID @Override
        public void clearOperationalTerms() {
            super.clearOperationalTerms();
            return;
        }


        /**
         *  Gets all the operational query terms.
         *
         *  @return a collection of the operational query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.BooleanTerm> getOperationalTerms() {
            return (super.getOperationalTerms());
        }
    }
}

