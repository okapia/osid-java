//
// AbstractAdapterSequenceRuleLookupSession.java
//
//    A SequenceRule lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SequenceRule lookup session adapter.
 */

public abstract class AbstractAdapterSequenceRuleLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {

    private final org.osid.assessment.authoring.SequenceRuleLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSequenceRuleLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSequenceRuleLookupSession(org.osid.assessment.authoring.SequenceRuleLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@code Bank} associated with this session.
     *
     *  @return the {@code Bank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@code SequenceRule} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSequenceRules() {
        return (this.session.canLookupSequenceRules());
    }


    /**
     *  A complete view of the {@code SequenceRule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSequenceRuleView() {
        this.session.useComparativeSequenceRuleView();
        return;
    }


    /**
     *  A complete view of the {@code SequenceRule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySequenceRuleView() {
        this.session.usePlenarySequenceRuleView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rules in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    

    /**
     *  Only active sequence rules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSequenceRuleView() {
        this.session.useActiveSequenceRuleView();
        return;
    }


    /**
     *  Active and inactive sequence rules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSequenceRuleView() {
        this.session.useAnyStatusSequenceRuleView();
        return;
    }
    
     
    /**
     *  Gets the {@code SequenceRule} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SequenceRule} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SequenceRule} and
     *  retained for compatibility.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param sequenceRuleId {@code Id} of the {@code SequenceRule}
     *  @return the sequence rule
     *  @throws org.osid.NotFoundException {@code sequenceRuleId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code sequenceRuleId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRule getSequenceRule(org.osid.id.Id sequenceRuleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRule(sequenceRuleId));
    }


    /**
     *  Gets a {@code SequenceRuleList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  sequenceRules specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code SequenceRules} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SequenceRule} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByIds(org.osid.id.IdList sequenceRuleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRulesByIds(sequenceRuleIds));
    }


    /**
     *  Gets a {@code SequenceRuleList} corresponding to the given
     *  sequence rule genus {@code Type} which does not include
     *  sequence rules of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned {@code SequenceRule} list
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRulesByGenusType(sequenceRuleGenusType));
    }


    /**
     *  Gets a {@code SequenceRuleList} corresponding to the given
     *  sequence rule genus {@code Type} and include any additional
     *  sequence rules with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned {@code SequenceRule} list
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleGenusType} is {@code }
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByParentGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRulesByParentGenusType(sequenceRuleGenusType));
    }


    /**
     *  Gets a {@code SequenceRuleList} containing the given
     *  sequence rule record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleRecordType a sequenceRule record type 
     *  @return the returned {@code SequenceRule} list
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByRecordType(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRulesByRecordType(sequenceRuleRecordType));
    }


    /**
     *  Gets a {@code SequenceRuleList} for the given source
     *  assessment part.
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentPartId an assessment part {@code Id} 
     *  @return the returned {@code SequenceRule} list 
     *  @throws org.osid.NullArgumentException {@code assessmentPartId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessmentPart(org.osid.id.Id assessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getSequenceRulesForAssessmentPart(assessmentPartId));
    }


    /**
     *  Gets a {@code SequenceRuleList} for the given target assessment 
     *  part. 
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  nextAssessmentPartId an assessment part {@code Id} 
     *  @return the returned {@code SequenceRule} list 
     *  @throws org.osid.NullArgumentException {@code
     *         nextAssessmentPartId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForNextAssessmentPart(org.osid.id.Id nextAssessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getSequenceRulesForNextAssessmentPart(nextAssessmentPartId));
    }


    /**
     *  Gets a {@code SequenceRuleList} for the given source and
     *  target assessment parts.
     *  
     *  {@code} In plenary mode, the returned list contains all known
     *  sequence rule or an error results. Otherwise, the returned
     *  list may contain only those sequence rule that are accessible
     *  through this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentPartId source assessment part {@code Id} 
     *  @param  nextAssessmentPartId target assessment part {@code Id} 
     *  @return the returned {@code SequenceRule} list 
     *  @throws org.osid.NullArgumentException {@code assessmentPartId} 
     *          or {@code nextAssessmentPartId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessmentParts(org.osid.id.Id assessmentPartId, 
                                                                                             org.osid.id.Id nextAssessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRulesForAssessmentParts(assessmentPartId, nextAssessmentPartId));
    }


    /**
     *  Gets a {@code SequenceRuleList} for an entire assessment. 
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentId an assessment {@code Id} 
     *  @return the returned {@code SequenceRule} list 
     *  @throws org.osid.NullArgumentException {@code assessmentId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getSequenceRulesForAssessment(assessmentId));
    }


    /**
     *  Gets all {@code SequenceRules}. 
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @return a list of {@code SequenceRules} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRules());
    }
}
