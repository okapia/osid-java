//
// AbstractProjectBatchFormList
//
//     Implements a filter for a ProjectBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.room.construction.batch.projectbatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ProjectBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedProjectBatchFormList
 *  to improve performance.
 */

public abstract class AbstractProjectBatchFormFilterList
    extends net.okapia.osid.jamocha.room.construction.batch.projectbatchform.spi.AbstractProjectBatchFormList
    implements org.osid.room.construction.batch.ProjectBatchFormList,
               net.okapia.osid.jamocha.inline.filter.room.construction.batch.projectbatchform.ProjectBatchFormFilter {

    private org.osid.room.construction.batch.ProjectBatchForm projectBatchForm;
    private final org.osid.room.construction.batch.ProjectBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractProjectBatchFormFilterList</code>.
     *
     *  @param projectBatchFormList a <code>ProjectBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>projectBatchFormList</code> is <code>null</code>
     */

    protected AbstractProjectBatchFormFilterList(org.osid.room.construction.batch.ProjectBatchFormList projectBatchFormList) {
        nullarg(projectBatchFormList, "project batch form list");
        this.list = projectBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.projectBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ProjectBatchForm </code> in this list. 
     *
     *  @return the next <code> ProjectBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ProjectBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.construction.batch.ProjectBatchForm getNextProjectBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.room.construction.batch.ProjectBatchForm projectBatchForm = this.projectBatchForm;
            this.projectBatchForm = null;
            return (projectBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in project batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.projectBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ProjectBatchForms.
     *
     *  @param projectBatchForm the project batch form to filter
     *  @return <code>true</code> if the project batch form passes the filter,
     *          <code>false</code> if the project batch form should be filtered
     */

    public abstract boolean pass(org.osid.room.construction.batch.ProjectBatchForm projectBatchForm);


    protected void prime() {
        if (this.projectBatchForm != null) {
            return;
        }

        org.osid.room.construction.batch.ProjectBatchForm projectBatchForm = null;

        while (this.list.hasNext()) {
            try {
                projectBatchForm = this.list.getNextProjectBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(projectBatchForm)) {
                this.projectBatchForm = projectBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
