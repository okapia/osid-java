//
// AbstractPool.java
//
//     Defines a Pool builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.pool.spi;


/**
 *  Defines a <code>Pool</code> builder.
 */

public abstract class AbstractPoolBuilder<T extends AbstractPoolBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.pool.PoolMiter pool;


    /**
     *  Constructs a new <code>AbstractPoolBuilder</code>.
     *
     *  @param pool the pool to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPoolBuilder(net.okapia.osid.jamocha.builder.provisioning.pool.PoolMiter pool) {
        super(pool);
        this.pool = pool;
        return;
    }


    /**
     *  Builds the pool.
     *
     *  @return the new pool
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.Pool build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.pool.PoolValidator(getValidations())).validate(this.pool);
        return (new net.okapia.osid.jamocha.builder.provisioning.pool.ImmutablePool(this.pool));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the pool miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.pool.PoolMiter getMiter() {
        return (this.pool);
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>broker</code> is
     *          <code>null</code>
     */

    public T broker(org.osid.provisioning.Broker broker) {
        getMiter().setBroker(broker);
        return (self());
    }


    /**
     *  Sets the size.
     *
     *  @param size a size
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    public T size(long size) {
        getMiter().setSize(size);
        return (self());
    }


    /**
     *  Adds a Pool record.
     *
     *  @param record a pool record
     *  @param recordType the type of pool record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.records.PoolRecord record, org.osid.type.Type recordType) {
        getMiter().addPoolRecord(record, recordType);
        return (self());
    }
}       


