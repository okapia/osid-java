//
// MutableBrokerList.java
//
//     Implements a BrokerList. This list allows Brokers to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.broker;


/**
 *  <p>Implements a BrokerList. This list allows Brokers to be
 *  added after this broker has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this broker must
 *  invoke <code>eol()</code> when there are no more brokers to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>BrokerList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more brokers are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more brokers to be added.</p>
 */

public final class MutableBrokerList
    extends net.okapia.osid.jamocha.provisioning.broker.spi.AbstractMutableBrokerList
    implements org.osid.provisioning.BrokerList {


    /**
     *  Creates a new empty <code>MutableBrokerList</code>.
     */

    public MutableBrokerList() {
        super();
    }


    /**
     *  Creates a new <code>MutableBrokerList</code>.
     *
     *  @param broker a <code>Broker</code>
     *  @throws org.osid.NullArgumentException <code>broker</code>
     *          is <code>null</code>
     */

    public MutableBrokerList(org.osid.provisioning.Broker broker) {
        super(broker);
        return;
    }


    /**
     *  Creates a new <code>MutableBrokerList</code>.
     *
     *  @param array an array of brokers
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableBrokerList(org.osid.provisioning.Broker[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableBrokerList</code>.
     *
     *  @param collection a java.util.Collection of brokers
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableBrokerList(java.util.Collection<org.osid.provisioning.Broker> collection) {
        super(collection);
        return;
    }
}
