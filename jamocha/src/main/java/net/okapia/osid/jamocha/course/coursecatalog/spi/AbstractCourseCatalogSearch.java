//
// AbstractCourseCatalogSearch.java
//
//     A template for making a CourseCatalog Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.coursecatalog.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing course catalog searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCourseCatalogSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.CourseCatalogSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseCatalogSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.CourseCatalogSearchOrder courseCatalogSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of course catalogs. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  courseCatalogIds list of course catalogs
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCourseCatalogs(org.osid.id.IdList courseCatalogIds) {
        while (courseCatalogIds.hasNext()) {
            try {
                this.ids.add(courseCatalogIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCourseCatalogs</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of course catalog Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCourseCatalogIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  courseCatalogSearchOrder course catalog search order 
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>courseCatalogSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCourseCatalogResults(org.osid.course.CourseCatalogSearchOrder courseCatalogSearchOrder) {
	this.courseCatalogSearchOrder = courseCatalogSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.CourseCatalogSearchOrder getCourseCatalogSearchOrder() {
	return (this.courseCatalogSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given course catalog search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a course catalog implementing the requested record.
     *
     *  @param courseCatalogSearchRecordType a course catalog search record
     *         type
     *  @return the course catalog search record
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseCatalogSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseCatalogSearchRecord getCourseCatalogSearchRecord(org.osid.type.Type courseCatalogSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.records.CourseCatalogSearchRecord record : this.records) {
            if (record.implementsRecordType(courseCatalogSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseCatalogSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course catalog search. 
     *
     *  @param courseCatalogSearchRecord course catalog search record
     *  @param courseCatalogSearchRecordType courseCatalog search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseCatalogSearchRecord(org.osid.course.records.CourseCatalogSearchRecord courseCatalogSearchRecord, 
                                           org.osid.type.Type courseCatalogSearchRecordType) {

        addRecordType(courseCatalogSearchRecordType);
        this.records.add(courseCatalogSearchRecord);        
        return;
    }
}
