//
// AbstractGradebookColumnSummary.java
//
//     Defines a GradebookColumnSummary builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.spi;


/**
 *  Defines a <code>GradebookColumnSummary</code> builder.
 */

public abstract class AbstractGradebookColumnSummaryBuilder<T extends AbstractGradebookColumnSummaryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.GradebookColumnSummaryMiter gradebookColumnSummary;


    /**
     *  Constructs a new <code>AbstractGradebookColumnSummaryBuilder</code>.
     *
     *  @param gradebookColumnSummary the gradebook column summary to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradebookColumnSummaryBuilder(net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.GradebookColumnSummaryMiter gradebookColumnSummary) {
        super(gradebookColumnSummary);
        this.gradebookColumnSummary = gradebookColumnSummary;
        return;
    }


    /**
     *  Builds the gradebook column summary.
     *
     *  @return the new gradebook column summary
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.GradebookColumnSummary build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.gradebookcolumnsummary.GradebookColumnSummaryValidator(getValidations())).validate(this.gradebookColumnSummary);
        return (new net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.ImmutableGradebookColumnSummary(this.gradebookColumnSummary));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the gradebook column summary miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.GradebookColumnSummaryMiter getMiter() {
        return (this.gradebookColumnSummary);
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public T gradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        getMiter().setGradebookColumn(gradebookColumn);
        return (self());
    }


    /**
     *  Sets the mean.
     *
     *  @param mean a mean
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>mean</code> is
     *          <code>null</code>
     */

    public T mean(java.math.BigDecimal mean) {
        getMiter().setMean(mean);
        return (self());
    }


    /**
     *  Sets the median.
     *
     *  @param median a median
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>median</code> is
     *          <code>null</code>
     */

    public T median(java.math.BigDecimal median) {
        getMiter().setMedian(median);
        return (self());
    }


    /**
     *  Sets the mode.
     *
     *  @param mode a mode
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>mode</code> is
     *          <code>null</code>
     */

    public T mode(java.math.BigDecimal mode) {
        getMiter().setMode(mode);
        return (self());
    }


    /**
     *  Sets the rms.
     *
     *  @param rms the root mean square
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>rms</code> is
     *          <code>null</code>
     */

    public T rms(java.math.BigDecimal rms) {
        getMiter().setRMS(rms);
        return (self());
    }


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation a standard deviation
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    public T standardDeviation(java.math.BigDecimal standardDeviation) {
        getMiter().setStandardDeviation(standardDeviation);
        return (self());
    }


    /**
     *  Sets the sum.
     *
     *  @param sum a sum
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sum</code> is
     *          <code>null</code>
     */

    public T sum(java.math.BigDecimal sum) {
        getMiter().setSum(sum);
        return (self());
    }


    /**
     *  Adds a GradebookColumnSummary record.
     *
     *  @param record a gradebook column summary record
     *  @param recordType the type of gradebook column summary record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.records.GradebookColumnSummaryRecord record, org.osid.type.Type recordType) {
        getMiter().addGradebookColumnSummaryRecord(record, recordType);
        return (self());
    }
}       


