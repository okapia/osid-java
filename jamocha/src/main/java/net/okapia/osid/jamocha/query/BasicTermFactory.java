//
// BasicTermFactory.java
//
//     A query term factory using the Primordium terms.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query;


/**
 *  A query term factory using the Primordium terms.
 */

public class BasicTermFactory
    implements TermFactory {


    /**
     *  Creates a boolean term.
     *
     *  @param b boolean value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a boolean term
     */

    @Override
    public org.osid.search.terms.BooleanTerm createBooleanTerm(boolean b, boolean match) {
        return (new net.okapia.osid.primordium.terms.BooleanTerm(b, match));
    }


    /**
     *  Creates a boolean wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a boolean term
     */

    @Override
    public org.osid.search.terms.BooleanTerm createBooleanWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.BooleanWildcardTerm(match));
    }


    /**
     *  Creates a byte term.
     *
     *  @param b the bytes
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match     
     *  @param partial <code>true</code> for a partial match,
     *         <code>false</code> for a complete match
     *  @return a byte term
     *  @throws org.osid.NullArgumentException <code>b</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.BytesTerm createBytesTerm(byte[] b, boolean match, 
                                                           boolean partial) {
        return (new net.okapia.osid.primordium.terms.BytesTerm(b, match, partial));
    }


    /**
     *  Creates a byte wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a byte term
     */

    @Override
    public org.osid.search.terms.BytesTerm createBytesWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.BytesWildcardTerm(match));
    }


    /**
     *  Creates a cardinal term.
     *
     *  @param c the cardinal value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a cardinal term
     *  @throws org.osid.InvalidArgumentException <code>c</code> is
     *          negative
     */

    @Override
    public org.osid.search.terms.CardinalTerm createCardinalTerm(long c, boolean match) {
        return (new net.okapia.osid.primordium.terms.CardinalTerm(c, match));
    }


    /**
     *  Creates a cardinal wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a cardinal term
     */

    @Override
    public org.osid.search.terms.CardinalTerm createCardinalWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.CardinalWildcardTerm(match));
    }


    /**
     *  Creates a cardinal range term.
     *
     *  @param from start of cardinal range
     *  @param to end of cardinal range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a cardinal range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>, or <code>from</code> or
     *          <code>to</code> is negative
     */

    @Override
    public org.osid.search.terms.CardinalRangeTerm createCardinalRangeTerm(long from, long to, boolean match) {
        return (new net.okapia.osid.primordium.terms.CardinalRangeTerm(from, to, match));
    }


    /**
     *  Creates a cardinal range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a cardinal range term
     */

    @Override
    public org.osid.search.terms.CardinalRangeTerm createCardinalRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.CardinalRangeWildcardTerm(match));
    }


    /**
     *  Creates a coordinate term.
     *
     *  @param coordinate the coordinate value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a coordinate term
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.CoordinateTerm createCoordinateTerm(org.osid.mapping.Coordinate coordinate,
                                                                     boolean match) {
        return (new net.okapia.osid.primordium.terms.CoordinateTerm(coordinate, match));
    }


    /**
     *  Creates a coordinate wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a coordinate term
     */

    @Override
    public org.osid.search.terms.CoordinateTerm createCoordinateWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.CoordinateWildcardTerm(match));
    }


    /**
     *  Creates a coordinate range term.
     *
     *  @param from start of coordinate range
     *  @param to end of coordinate range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a coordinate range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.CoordinateRangeTerm createCoordinateRangeTerm(org.osid.mapping.Coordinate from,
                                                                               org.osid.mapping.Coordinate to,
                                                                               boolean match) {
        return (new net.okapia.osid.primordium.terms.CoordinateRangeTerm(from, to, match));
    }


    /**
     *  Creates a coordinate range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a coordinate range term
     */

    @Override
    public org.osid.search.terms.CoordinateRangeTerm createCoordinateRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.CoordinateRangeWildcardTerm(match));
    }


    /**
     *  Creates a currency term.
     *
     *  @param currency the currency value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a currency term
     *  @throws org.osid.NullArgumentException <code>currency</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.CurrencyTerm createCurrencyTerm(org.osid.financials.Currency currency, 
                                                                 boolean match) {
        return (new net.okapia.osid.primordium.terms.CurrencyTerm(currency, match));
    }


    /**
     *  Creates a currency wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a currency term
     */

    @Override
    public org.osid.search.terms.CurrencyTerm createCurrencyWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.CurrencyWildcardTerm(match));
    }


    /**
     *  Creates a currency range term.
     *
     *  @param from start of currency range
     *  @param to end of currency range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a currency range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.CurrencyRangeTerm createCurrencyRangeTerm(org.osid.financials.Currency from,
                                                                           org.osid.financials.Currency to,
                                                                           boolean match) {
        return (new net.okapia.osid.primordium.terms.CurrencyRangeTerm(from, to, match));
    }


    /**
     *  Creates a currency range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a currency range term
     */

    @Override
    public org.osid.search.terms.CurrencyRangeTerm createCurrencyRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.CurrencyRangeWildcardTerm(match));
    }


    /**
     *  Creates a datetime term.
     *
     *  @param datetime the date time value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a datetime term
     *  @throws org.osid.NullArgumentException <code>datetime</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DateTimeTerm createDateTimeTerm(org.osid.calendaring.DateTime datetime, 
                                                                 boolean match) {
        return (new net.okapia.osid.primordium.terms.DateTimeTerm(datetime, match));
    }


    /**
     *  Creates a date time wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a date time term
     */

    @Override
    public org.osid.search.terms.DateTimeTerm createDateTimeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DateTimeWildcardTerm(match));
    }


    /**
     *  Creates a date time range term.
     *
     *  @param from start of datetime range
     *  @param to end of datetime range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a date time range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DateTimeRangeTerm createDateTimeRangeTerm(org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to,
                                                                           boolean match) {
        return (new net.okapia.osid.primordium.terms.DateTimeRangeTerm(from, to, match));
    }


    /**
     *  Creates a date time range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a date time range term
     */

    @Override
    public org.osid.search.terms.DateTimeRangeTerm createDateTimeRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DateTimeRangeWildcardTerm(match));
    }


    /**
     *  Creates a decimal term.
     *
     *  @param decimal the decimal value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a decimal term
     *  @throws org.osid.NullArgumentException <code>decimal</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DecimalTerm createDecimalTerm(java.math.BigDecimal decimal, boolean match) {
        return (new net.okapia.osid.primordium.terms.DecimalTerm(decimal, match));
    }


    /**
     *  Creates a decimal wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a decimal term
     */

    @Override
    public org.osid.search.terms.DecimalTerm createDecimalWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DecimalWildcardTerm(match));
    }


    /**
     *  Creates a decimal range term.
     *
     *  @param from start of decimal range
     *  @param to end of decimal range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a decimal range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DecimalRangeTerm createDecimalRangeTerm(java.math.BigDecimal from,
                                                                         java.math.BigDecimal to,
                                                                         boolean match) {
        return (new net.okapia.osid.primordium.terms.DecimalRangeTerm(from, to, match));
    }


    /**
     *  Creates a decimal range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a decimal range term
     */

    @Override
    public org.osid.search.terms.DecimalRangeTerm createDecimalRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DecimalRangeWildcardTerm(match));
    }


    /**
     *  Creates a distance term.
     *
     *  @param distance the distance value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a distance term
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DistanceTerm createDistanceTerm(org.osid.mapping.Distance distance, boolean match) {
        return (new net.okapia.osid.primordium.terms.DistanceTerm(distance, match));
    }


    /**
     *  Creates a distance wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a distance term
     */

    @Override
    public org.osid.search.terms.DistanceTerm createDistanceWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DistanceWildcardTerm(match));
    }


    /**
     *  Creates a distance range term.
     *
     *  @param from start of distance range
     *  @param to end of distance range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a distance range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DistanceRangeTerm createDistanceRangeTerm(org.osid.mapping.Distance from,
                                                                           org.osid.mapping.Distance to,
                                                                           boolean match) {
        return (new net.okapia.osid.primordium.terms.DistanceRangeTerm(from, to, match));
    }


    /**
     *  Creates a distance range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a distance range term
     */

    @Override
    public org.osid.search.terms.DistanceRangeTerm createDistanceRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DistanceRangeWildcardTerm(match));
    }


    /**
     *  Creates a duration term.
     *
     *  @param duration the duration value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a duration term
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DurationTerm createDurationTerm(org.osid.calendaring.Duration duration, 
                                                                 boolean match) {
        return (new net.okapia.osid.primordium.terms.DurationTerm(duration, match));
    }


    /**
     *  Creates a duration wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a duration term
     */

    @Override
    public org.osid.search.terms.DurationTerm createDurationWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DurationWildcardTerm(match));
    }


    /**
     *  Creates a duration range term.
     *
     *  @param from start of duration range
     *  @param to end of duration range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a duration range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.DurationRangeTerm createDurationRangeTerm(org.osid.calendaring.Duration from,
                                                                           org.osid.calendaring.Duration to,
                                                                           boolean match) {
        return (new net.okapia.osid.primordium.terms.DurationRangeTerm(from, to, match));
    }


    /**
     *  Creates a duration range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a duration range term
     */

    @Override
    public org.osid.search.terms.DurationRangeTerm createDurationRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.DurationRangeWildcardTerm(match));
    }


    /**
     *  Creates a heading term.
     *
     *  @param heading the heading value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a heading term
     *  @throws org.osid.NullArgumentException <code>heading</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.HeadingTerm createHeadingTerm(org.osid.mapping.Heading heading, boolean match) {
        return (new net.okapia.osid.primordium.terms.HeadingTerm(heading, match));
    }


    /**
     *  Creates a heading wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a heading term
     */

    @Override
    public org.osid.search.terms.HeadingTerm createHeadingWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.HeadingWildcardTerm(match));
    }


    /**
     *  Creates a heading range term.
     *
     *  @param from start of heading range
     *  @param to end of heading range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a heading range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.HeadingRangeTerm createHeadingRangeTerm(org.osid.mapping.Heading from,
                                                                         org.osid.mapping.Heading to,
                                                                         boolean match) {
        return (new net.okapia.osid.primordium.terms.HeadingRangeTerm(from, to, match));
    }


    /**
     *  Creates a heading range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a heading range term
     */

    @Override
    public org.osid.search.terms.HeadingRangeTerm createHeadingRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.HeadingRangeWildcardTerm(match));
    }


    /**
     *  Creates an Id term.
     *
     *  @param id the Id value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an Id term
     *  @throws org.osid.NullArgumentException <code>id</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.IdTerm createIdTerm(org.osid.id.Id id, boolean match) {
        return (new net.okapia.osid.primordium.terms.IdTerm(id, match));
    }


    /**
     *  Creates an Id wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an Id term
     */

    @Override
    public org.osid.search.terms.IdTerm createIdWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.IdWildcardTerm(match));
    }


    /**
     *  Creates an integer term.
     *
     *  @param i the integer value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an integer term
     */

    @Override
    public org.osid.search.terms.IntegerTerm createIntegerTerm(long i, boolean match) {
        return (new net.okapia.osid.primordium.terms.IntegerTerm(i, match));
    }


    /**
     *  Creates an integer wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an integer term
     */

    @Override
    public org.osid.search.terms.IntegerTerm createIntegerWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.IntegerWildcardTerm(match));
    }


    /**
     *  Creates an integer range term.
     *
     *  @param from start of integer range
     *  @param to end of integer range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an integer range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     */

    @Override
    public org.osid.search.terms.IntegerRangeTerm createIntegerRangeTerm(long from, long to, boolean match) {
        return (new net.okapia.osid.primordium.terms.IntegerRangeTerm(from, to, match));
    }


    /**
     *  Creates a integer range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an integer term
     */

    @Override
    public org.osid.search.terms.IntegerRangeTerm createIntegerRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.IntegerRangeWildcardTerm(match));
    }


    /**
     *  Creates an object term.
     *
     *  @param object the object value
     *  @param objectType the object type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an object term
     *  @throws org.osid.NullArgumentException <code>object</code> or
     *          <code>objectType</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.ObjectTerm createObjectTerm(Object object, 
                                                             org.osid.type.Type objectType, 
                                                             boolean match) {
        return (new net.okapia.osid.primordium.terms.ObjectTerm(object, objectType, match));
    }


    /**
     *  Creates an object wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return an object term
     */

    @Override
    public org.osid.search.terms.ObjectTerm createObjectWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.ObjectWildcardTerm(match));
    }


    /**
     *  Creates a spatial unit term.
     *
     *  @param unit the spatial unit value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a spatial unit term
     *  @throws org.osid.NullArgumentException <code>unit</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.SpatialUnitTerm createSpatialUnitTerm(org.osid.mapping.SpatialUnit unit,
                                                                       boolean match) {
        return (new net.okapia.osid.primordium.terms.SpatialUnitTerm(unit, match));
    }


    /**
     *  Creates a spatial unit wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a spatial unit term
     */

    @Override
    public org.osid.search.terms.SpatialUnitTerm createSpatialUnitWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.SpatialUnitWildcardTerm(match));
    }


    /**
     *  Creates a speed term.
     *
     *  @param speed the speed value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a speed term
     *  @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.SpeedTerm createSpeedTerm(org.osid.mapping.Speed speed, boolean match) {
        return (new net.okapia.osid.primordium.terms.SpeedTerm(speed, match));
    }


    /**
     *  Creates a speed wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a speed term
     */

    @Override
    public org.osid.search.terms.SpeedTerm createSpeedWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.SpeedWildcardTerm(match));
    }


    /**
     *  Creates a speed range term.
     *
     *  @param from start of speed range
     *  @param to end of speed range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a speed range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.SpeedRangeTerm createSpeedRangeTerm(org.osid.mapping.Speed from,
                                                                     org.osid.mapping.Speed to, 
                                                                     boolean match) {
        return (new net.okapia.osid.primordium.terms.SpeedRangeTerm(from, to, match));
    }


    /**
     *  Creates a speed range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a speed range term
     */

    @Override
    public org.osid.search.terms.SpeedRangeTerm createSpeedRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.SpeedRangeWildcardTerm(match));
    }


    /**
     *  Creates a string term.
     *
     *  @param string the string value
     *  @param stringMatchType the string match type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a string term
     *  @throws org.osid.NullArgumentException <code>string</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.StringTerm createStringTerm(String string, org.osid.type.Type stringMatchType,
                                                             boolean match) {
        return (new net.okapia.osid.primordium.terms.StringTerm(string, stringMatchType, match));
    }


    /**
     *  Creates a string wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a string term
     */

    @Override
    public org.osid.search.terms.StringTerm createStringWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.StringWildcardTerm(match));
    }


    /**
     *  Creates a syntax term.
     *
     *  @param syntax the syntax value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a syntax term
     *  @throws org.osid.NullArgumentException <code>syntax</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.SyntaxTerm createSyntaxTerm(org.osid.Syntax syntax, boolean match) {
        return (new net.okapia.osid.primordium.terms.SyntaxTerm(syntax, match));
    }


    /**
     *  Creates a syntax wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a syntax term
     */

    @Override
    public org.osid.search.terms.SyntaxTerm createSyntaxWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.SyntaxWildcardTerm(match));
    }


    /**
     *  Creates a time term.
     *
     *  @param time the time value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a time term
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.TimeTerm createTimeTerm(org.osid.calendaring.Time time, boolean match) {
        return (new net.okapia.osid.primordium.terms.TimeTerm(time, match));
    }


    /**
     *  Creates a time wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a time term
     */

    @Override
    public org.osid.search.terms.TimeTerm createTimeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.TimeWildcardTerm(match));
    }


    /**
     *  Creates a time range term.
     *
     *  @param from start of time range
     *  @param to end of time range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a time range term
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    @Override
    public org.osid.search.terms.TimeRangeTerm createTimeRangeTerm(org.osid.calendaring.Time from,
                                                                   org.osid.calendaring.Time to, 
                                                                   boolean match) {
        return (new net.okapia.osid.primordium.terms.TimeRangeTerm(from, to, match));
    }


    /**
     *  Creates a time range wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a time range term
     */

    @Override
    public org.osid.search.terms.TimeRangeTerm createTimeRangeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.TimeRangeWildcardTerm(match));
    }


    /**
     *  Creates a type term.
     *
     *  @param type the type value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a type term
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.TypeTerm createTypeTerm(org.osid.type.Type type, boolean match) {
        return (new net.okapia.osid.primordium.terms.TypeTerm(type, match));
    }


    /**
     *  Creates a type wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a type term
     */

    @Override
    public org.osid.search.terms.TypeTerm createTypeWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.TypeWildcardTerm(match));
    }


    /**
     *  Creates a version term.
     *
     *  @param version the version value
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a version term
     *  @throws org.osid.NullArgumentException <code>version</code>
     *          is <code>null</code>
     */

    @Override
    public org.osid.search.terms.VersionTerm createVersionTerm(org.osid.installation.Version version, boolean match) {
        return (new net.okapia.osid.primordium.terms.VersionTerm(version, match));
    }


    /**
     *  Creates a version wildcard term.
     *
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @return a version term
     */

    @Override
    public org.osid.search.terms.VersionTerm createVersionWildcardTerm(boolean match) {
        return (new net.okapia.osid.primordium.terms.VersionWildcardTerm(match));
    }
}
