//
// AbstractMapCanonicalUnitEnablerLookupSession
//
//    A simple framework for providing a CanonicalUnitEnabler lookup service
//    backed by a fixed collection of canonical unit enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CanonicalUnitEnabler lookup service backed by a
 *  fixed collection of canonical unit enablers. The canonical unit enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnitEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCanonicalUnitEnablerLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractCanonicalUnitEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.rules.CanonicalUnitEnabler> canonicalUnitEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.rules.CanonicalUnitEnabler>());


    /**
     *  Makes a <code>CanonicalUnitEnabler</code> available in this session.
     *
     *  @param  canonicalUnitEnabler a canonical unit enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnabler<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitEnabler(org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler) {
        this.canonicalUnitEnablers.put(canonicalUnitEnabler.getId(), canonicalUnitEnabler);
        return;
    }


    /**
     *  Makes an array of canonical unit enablers available in this session.
     *
     *  @param  canonicalUnitEnablers an array of canonical unit enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablers<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitEnablers(org.osid.offering.rules.CanonicalUnitEnabler[] canonicalUnitEnablers) {
        putCanonicalUnitEnablers(java.util.Arrays.asList(canonicalUnitEnablers));
        return;
    }


    /**
     *  Makes a collection of canonical unit enablers available in this session.
     *
     *  @param  canonicalUnitEnablers a collection of canonical unit enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablers<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitEnablers(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitEnabler> canonicalUnitEnablers) {
        for (org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler : canonicalUnitEnablers) {
            this.canonicalUnitEnablers.put(canonicalUnitEnabler.getId(), canonicalUnitEnabler);
        }

        return;
    }


    /**
     *  Removes a CanonicalUnitEnabler from this session.
     *
     *  @param  canonicalUnitEnablerId the <code>Id</code> of the canonical unit enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId) {
        this.canonicalUnitEnablers.remove(canonicalUnitEnablerId);
        return;
    }


    /**
     *  Gets the <code>CanonicalUnitEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  canonicalUnitEnablerId <code>Id</code> of the <code>CanonicalUnitEnabler</code>
     *  @return the canonicalUnitEnabler
     *  @throws org.osid.NotFoundException <code>canonicalUnitEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnabler getCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler = this.canonicalUnitEnablers.get(canonicalUnitEnablerId);
        if (canonicalUnitEnabler == null) {
            throw new org.osid.NotFoundException("canonicalUnitEnabler not found: " + canonicalUnitEnablerId);
        }

        return (canonicalUnitEnabler);
    }


    /**
     *  Gets all <code>CanonicalUnitEnablers</code>. In plenary mode, the returned
     *  list contains all known canonicalUnitEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonicalUnitEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CanonicalUnitEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.ArrayCanonicalUnitEnablerList(this.canonicalUnitEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitEnablers.clear();
        super.close();
        return;
    }
}
