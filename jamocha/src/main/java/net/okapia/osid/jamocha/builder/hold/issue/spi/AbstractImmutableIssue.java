//
// AbstractImmutableIssue.java
//
//     Wraps a mutable Issue to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.issue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Issue</code> to hide modifiers. This
 *  wrapper provides an immutized Issue from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying issue whose state changes are visible.
 */

public abstract class AbstractImmutableIssue
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.hold.Issue {

    private final org.osid.hold.Issue issue;


    /**
     *  Constructs a new <code>AbstractImmutableIssue</code>.
     *
     *  @param issue the issue to immutablize
     *  @throws org.osid.NullArgumentException <code>issue</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableIssue(org.osid.hold.Issue issue) {
        super(issue);
        this.issue = issue;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource that has authority for this 
     *  issue. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBureauId() {
        return (this.issue.getBureauId());
    }


    /**
     *  Gets the resource that has authority for this issue. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getBureau()
        throws org.osid.OperationFailedException {

        return (this.issue.getBureau());
    }


    /**
     *  Gets the issue record corresponding to the given <code> Issue </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> issueRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(issueRecordType) </code> is <code> true </code> . 
     *
     *  @param  issueRecordType the type of issue record to retrieve 
     *  @return the issue record 
     *  @throws org.osid.NullArgumentException <code> issueRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(issueRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.records.IssueRecord getIssueRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.issue.getIssueRecord(issueRecordType));
    }
}

