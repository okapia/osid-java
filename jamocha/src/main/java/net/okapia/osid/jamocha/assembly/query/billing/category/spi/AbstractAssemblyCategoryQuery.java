//
// AbstractAssemblyCategoryQuery.java
//
//     A CategoryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.category.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CategoryQuery that stores terms.
 */

public abstract class AbstractAssemblyCategoryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.billing.CategoryQuery,
               org.osid.billing.CategoryQueryInspector,
               org.osid.billing.CategorySearchOrder {

    private final java.util.Collection<org.osid.billing.records.CategoryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.CategoryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.CategorySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCategoryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCategoryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the entry <code> Id </code> for this query to match categories 
     *  that have a related entry. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        getAssembler().addIdTerm(getEntryIdColumn(), entryId, match);
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        getAssembler().clearTerms(getEntryIdColumn());
        return;
    }


    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (getAssembler().getIdTerms(getEntryIdColumn()));
    }


    /**
     *  Gets the EntryId column name.
     *
     * @return the column name
     */

    protected String getEntryIdColumn() {
        return ("entry_id");
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches any related entry. 
     *
     *  @param  match <code> true </code> to match categories with any entry, 
     *          <code> false </code> to match categories with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getEntryColumn(), match);
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        getAssembler().clearTerms(getEntryColumn());
        return;
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the entry query terms 
     */

    @OSID @Override
    public org.osid.billing.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.billing.EntryQueryInspector[0]);
    }


    /**
     *  Gets the Entry column name.
     *
     * @return the column name
     */

    protected String getEntryColumn() {
        return ("entry");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match categories 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this category supports the given record
     *  <code>Type</code>.
     *
     *  @param  categoryRecordType a category record type 
     *  @return <code>true</code> if the categoryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type categoryRecordType) {
        for (org.osid.billing.records.CategoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  categoryRecordType the category record type 
     *  @return the category query record 
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(categoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategoryQueryRecord getCategoryQueryRecord(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CategoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(categoryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  categoryRecordType the category record type 
     *  @return the category query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(categoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategoryQueryInspectorRecord getCategoryQueryInspectorRecord(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CategoryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(categoryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param categoryRecordType the category record type
     *  @return the category search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(categoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategorySearchOrderRecord getCategorySearchOrderRecord(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CategorySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(categoryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this category. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param categoryQueryRecord the category query record
     *  @param categoryQueryInspectorRecord the category query inspector
     *         record
     *  @param categorySearchOrderRecord the category search order record
     *  @param categoryRecordType category record type
     *  @throws org.osid.NullArgumentException
     *          <code>categoryQueryRecord</code>,
     *          <code>categoryQueryInspectorRecord</code>,
     *          <code>categorySearchOrderRecord</code> or
     *          <code>categoryRecordTypecategory</code> is
     *          <code>null</code>
     */
            
    protected void addCategoryRecords(org.osid.billing.records.CategoryQueryRecord categoryQueryRecord, 
                                      org.osid.billing.records.CategoryQueryInspectorRecord categoryQueryInspectorRecord, 
                                      org.osid.billing.records.CategorySearchOrderRecord categorySearchOrderRecord, 
                                      org.osid.type.Type categoryRecordType) {

        addRecordType(categoryRecordType);

        nullarg(categoryQueryRecord, "category query record");
        nullarg(categoryQueryInspectorRecord, "category query inspector record");
        nullarg(categorySearchOrderRecord, "category search odrer record");

        this.queryRecords.add(categoryQueryRecord);
        this.queryInspectorRecords.add(categoryQueryInspectorRecord);
        this.searchOrderRecords.add(categorySearchOrderRecord);
        
        return;
    }
}
