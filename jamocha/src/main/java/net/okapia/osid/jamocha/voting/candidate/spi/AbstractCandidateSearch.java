//
// AbstractCandidateSearch.java
//
//     A template for making a Candidate Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.candidate.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing candidate searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCandidateSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.CandidateSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.records.CandidateSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.CandidateSearchOrder candidateSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of candidates. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  candidateIds list of candidates
     *  @throws org.osid.NullArgumentException
     *          <code>candidateIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCandidates(org.osid.id.IdList candidateIds) {
        while (candidateIds.hasNext()) {
            try {
                this.ids.add(candidateIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCandidates</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of candidate Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCandidateIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  candidateSearchOrder candidate search order 
     *  @throws org.osid.NullArgumentException
     *          <code>candidateSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>candidateSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCandidateResults(org.osid.voting.CandidateSearchOrder candidateSearchOrder) {
	this.candidateSearchOrder = candidateSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.CandidateSearchOrder getCandidateSearchOrder() {
	return (this.candidateSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given candidate search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a candidate implementing the requested record.
     *
     *  @param candidateSearchRecordType a candidate search record
     *         type
     *  @return the candidate search record
     *  @throws org.osid.NullArgumentException
     *          <code>candidateSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(candidateSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.CandidateSearchRecord getCandidateSearchRecord(org.osid.type.Type candidateSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.records.CandidateSearchRecord record : this.records) {
            if (record.implementsRecordType(candidateSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(candidateSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this candidate search. 
     *
     *  @param candidateSearchRecord candidate search record
     *  @param candidateSearchRecordType candidate search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCandidateSearchRecord(org.osid.voting.records.CandidateSearchRecord candidateSearchRecord, 
                                           org.osid.type.Type candidateSearchRecordType) {

        addRecordType(candidateSearchRecordType);
        this.records.add(candidateSearchRecord);        
        return;
    }
}
