//
// AbstractFederatingVaultLookupSession.java
//
//     An abstract federating adapter for a VaultLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  VaultLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingVaultLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authorization.VaultLookupSession>
    implements org.osid.authorization.VaultLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingVaultLookupSession</code>.
     */

    protected AbstractFederatingVaultLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authorization.VaultLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Vault</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupVaults() {
        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            if (session.canLookupVaults()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Vault</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeVaultView() {
        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            session.useComparativeVaultView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Vault</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryVaultView() {
        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            session.usePlenaryVaultView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Vault</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Vault</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Vault</code> and
     *  retained for compatibility.
     *
     *  @param  vaultId <code>Id</code> of the
     *          <code>Vault</code>
     *  @return the vault
     *  @throws org.osid.NotFoundException <code>vaultId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>vaultId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            try {
                return (session.getVault(vaultId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(vaultId + " not found");
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  vaults specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Vaults</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  vaultIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>vaultIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByIds(org.osid.id.IdList vaultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authorization.vault.MutableVaultList ret = new net.okapia.osid.jamocha.authorization.vault.MutableVaultList();

        try (org.osid.id.IdList ids = vaultIds) {
            while (ids.hasNext()) {
                ret.addVault(getVault(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  vault genus <code>Type</code> which does not include
     *  vaults of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.vault.FederatingVaultList ret = getVaultList();

        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            ret.addVaultList(session.getVaultsByGenusType(vaultGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  vault genus <code>Type</code> and include any additional
     *  vaults with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByParentGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.vault.FederatingVaultList ret = getVaultList();

        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            ret.addVaultList(session.getVaultsByParentGenusType(vaultGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>VaultList</code> containing the given
     *  vault record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultRecordType a vault record type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByRecordType(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.vault.FederatingVaultList ret = getVaultList();

        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            ret.addVaultList(session.getVaultsByRecordType(vaultRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>VaultList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known vaults or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  vaults that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Vault</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.authorization.vault.FederatingVaultList ret = getVaultList();

        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            ret.addVaultList(session.getVaultsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Vaults</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Vaults</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.vault.FederatingVaultList ret = getVaultList();

        for (org.osid.authorization.VaultLookupSession session : getSessions()) {
            ret.addVaultList(session.getVaults());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authorization.vault.FederatingVaultList getVaultList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.vault.ParallelVaultList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.vault.CompositeVaultList());
        }
    }
}
