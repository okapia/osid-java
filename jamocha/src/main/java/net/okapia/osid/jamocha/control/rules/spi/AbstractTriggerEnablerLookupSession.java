//
// AbstractTriggerEnablerLookupSession.java
//
//    A starter implementation framework for providing a TriggerEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a TriggerEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getTriggerEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractTriggerEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.rules.TriggerEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();
    

    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>TriggerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTriggerEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>TriggerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTriggerEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>TriggerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTriggerEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include trigger enablers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active trigger enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTriggerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive trigger enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTriggerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>TriggerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>TriggerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>TriggerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, trigger enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  trigger enablers are returned.
     *
     *  @param  triggerEnablerId <code>Id</code> of the
     *          <code>TriggerEnabler</code>
     *  @return the trigger enabler
     *  @throws org.osid.NotFoundException <code>triggerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>triggerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnabler getTriggerEnabler(org.osid.id.Id triggerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.rules.TriggerEnablerList triggerEnablers = getTriggerEnablers()) {
            while (triggerEnablers.hasNext()) {
                org.osid.control.rules.TriggerEnabler triggerEnabler = triggerEnablers.getNextTriggerEnabler();
                if (triggerEnabler.getId().equals(triggerEnablerId)) {
                    return (triggerEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(triggerEnablerId + " not found");
    }


    /**
     *  Gets a <code>TriggerEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  triggerEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>TriggerEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, trigger enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  trigger enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getTriggerEnablers()</code>.
     *
     *  @param  triggerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>TriggerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByIds(org.osid.id.IdList triggerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.control.rules.TriggerEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = triggerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getTriggerEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("trigger enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.control.rules.triggerenabler.LinkedTriggerEnablerList(ret));
    }


    /**
     *  Gets a <code>TriggerEnablerList</code> corresponding to the given
     *  trigger enabler genus <code>Type</code> which does not include
     *  trigger enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  trigger enablers are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getTriggerEnablers()</code>.
     *
     *  @param  triggerEnablerGenusType a triggerEnabler genus type 
     *  @return the returned <code>TriggerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByGenusType(org.osid.type.Type triggerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.rules.triggerenabler.TriggerEnablerGenusFilterList(getTriggerEnablers(), triggerEnablerGenusType));
    }


    /**
     *  Gets a <code>TriggerEnablerList</code> corresponding to the given
     *  trigger enabler genus <code>Type</code> and include any additional
     *  trigger enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTriggerEnablers()</code>.
     *
     *  @param  triggerEnablerGenusType a triggerEnabler genus type 
     *  @return the returned <code>TriggerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByParentGenusType(org.osid.type.Type triggerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTriggerEnablersByGenusType(triggerEnablerGenusType));
    }


    /**
     *  Gets a <code>TriggerEnablerList</code> containing the given
     *  trigger enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTriggerEnablers()</code>.
     *
     *  @param  triggerEnablerRecordType a triggerEnabler record type 
     *  @return the returned <code>TriggerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByRecordType(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.rules.triggerenabler.TriggerEnablerRecordFilterList(getTriggerEnablers(), triggerEnablerRecordType));
    }


    /**
     *  Gets a <code>TriggerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible
     *  through this session.
     *  
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>TriggerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.rules.triggerenabler.TemporalTriggerEnablerFilterList(getTriggerEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>TriggerEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible
     *  through this session.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>TriggerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getTriggerEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>TriggerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  trigger enablers are returned.
     *
     *  @return a list of <code>TriggerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.control.rules.TriggerEnablerList getTriggerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the trigger enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of trigger enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.control.rules.TriggerEnablerList filterTriggerEnablersOnViews(org.osid.control.rules.TriggerEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.control.rules.TriggerEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.control.rules.triggerenabler.ActiveTriggerEnablerFilterList(ret);
        }

        return (ret);
    }
}
