//
// AbstractIndexedMapScheduleSlotLookupSession.java
//
//    A simple framework for providing a ScheduleSlot lookup service
//    backed by a fixed collection of schedule slots with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ScheduleSlot lookup service backed by a
 *  fixed collection of schedule slots. The schedule slots are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some schedule slots may be compatible
 *  with more types than are indicated through these schedule slot
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ScheduleSlots</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapScheduleSlotLookupSession
    extends AbstractMapScheduleSlotLookupSession
    implements org.osid.calendaring.ScheduleSlotLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.ScheduleSlot> scheduleSlotsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.ScheduleSlot>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.ScheduleSlot> scheduleSlotsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.ScheduleSlot>());


    /**
     *  Makes a <code>ScheduleSlot</code> available in this session.
     *
     *  @param  scheduleSlot a schedule slot
     *  @throws org.osid.NullArgumentException <code>scheduleSlot<code> is
     *          <code>null</code>
     */

    @Override
    protected void putScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot) {
        super.putScheduleSlot(scheduleSlot);

        this.scheduleSlotsByGenus.put(scheduleSlot.getGenusType(), scheduleSlot);
        
        try (org.osid.type.TypeList types = scheduleSlot.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.scheduleSlotsByRecord.put(types.getNextType(), scheduleSlot);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a schedule slot from this session.
     *
     *  @param scheduleSlotId the <code>Id</code> of the schedule slot
     *  @throws org.osid.NullArgumentException <code>scheduleSlotId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeScheduleSlot(org.osid.id.Id scheduleSlotId) {
        org.osid.calendaring.ScheduleSlot scheduleSlot;
        try {
            scheduleSlot = getScheduleSlot(scheduleSlotId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.scheduleSlotsByGenus.remove(scheduleSlot.getGenusType());

        try (org.osid.type.TypeList types = scheduleSlot.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.scheduleSlotsByRecord.remove(types.getNextType(), scheduleSlot);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeScheduleSlot(scheduleSlotId);
        return;
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  schedule slot genus <code>Type</code> which does not include
     *  schedule slots of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known schedule slots or an error results. Otherwise,
     *  the returned list may contain only those schedule slots that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  scheduleSlotGenusType a schedule slot genus type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.scheduleslot.ArrayScheduleSlotList(this.scheduleSlotsByGenus.get(scheduleSlotGenusType)));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> containing the given
     *  schedule slot record <code>Type</code>. In plenary mode, the
     *  returned list contains all known schedule slots or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedule slots that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  scheduleSlotRecordType a schedule slot record type 
     *  @return the returned <code>scheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByRecordType(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.scheduleslot.ArrayScheduleSlotList(this.scheduleSlotsByRecord.get(scheduleSlotRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.scheduleSlotsByGenus.clear();
        this.scheduleSlotsByRecord.clear();

        super.close();

        return;
    }
}
