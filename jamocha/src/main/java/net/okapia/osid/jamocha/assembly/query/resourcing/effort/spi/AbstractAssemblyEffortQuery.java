//
// AbstractAssemblyEffortQuery.java
//
//     An EffortQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.effort.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EffortQuery that stores terms.
 */

public abstract class AbstractAssemblyEffortQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.resourcing.EffortQuery,
               org.osid.resourcing.EffortQueryInspector,
               org.osid.resourcing.EffortSearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.EffortQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.EffortQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.EffortSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEffortQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEffortQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the commission <code> Id </code> for this query. 
     *
     *  @param  commissionId the commission <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commissionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommissionId(org.osid.id.Id commissionId, boolean match) {
        getAssembler().addIdTerm(getCommissionIdColumn(), commissionId, match);
        return;
    }


    /**
     *  Clears the commission <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCommissionIdTerms() {
        getAssembler().clearTerms(getCommissionIdColumn());
        return;
    }


    /**
     *  Gets the commission <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommissionIdTerms() {
        return (getAssembler().getIdTerms(getCommissionIdColumn()));
    }


    /**
     *  Orders the results by the commission. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCommission(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCommissionColumn(), style);
        return;
    }


    /**
     *  Gets the CommissionId column name.
     *
     * @return the column name
     */

    protected String getCommissionIdColumn() {
        return ("commission_id");
    }


    /**
     *  Tests if a <code> CommissionQuery </code> is available. 
     *
     *  @return <code> true </code> if a commission query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commission. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commission query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuery getCommissionQuery() {
        throw new org.osid.UnimplementedException("supportsCommissionQuery() is false");
    }


    /**
     *  Clears the commission query terms. 
     */

    @OSID @Override
    public void clearCommissionTerms() {
        getAssembler().clearTerms(getCommissionColumn());
        return;
    }


    /**
     *  Gets the commission query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQueryInspector[] getCommissionTerms() {
        return (new org.osid.resourcing.CommissionQueryInspector[0]);
    }


    /**
     *  Tests if a commission search order is available. 
     *
     *  @return <code> true </code> if a commission search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the commission search order. 
     *
     *  @return the commission search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCommissionSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSearchOrder getCommissionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCommissionSearchOrder() is false");
    }


    /**
     *  Gets the Commission column name.
     *
     * @return the column name
     */

    protected String getCommissionColumn() {
        return ("commission");
    }


    /**
     *  Matches the time spent between the given durations inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeSpent(org.osid.calendaring.Duration start, 
                               org.osid.calendaring.Duration end, 
                               boolean match) {
        getAssembler().addDurationRangeTerm(getTimeSpentColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the time spent query terms. 
     */

    @OSID @Override
    public void clearTimeSpentTerms() {
        getAssembler().clearTerms(getTimeSpentColumn());
        return;
    }


    /**
     *  Gets the time spent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTimeSpentTerms() {
        return (getAssembler().getDurationRangeTerms(getTimeSpentColumn()));
    }


    /**
     *  Orders the results by the time spent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeSpent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimeSpentColumn(), style);
        return;
    }


    /**
     *  Gets the TimeSpent column name.
     *
     * @return the column name
     */

    protected String getTimeSpentColumn() {
        return ("time_spent");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match efforts 
     *  assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this effort supports the given record
     *  <code>Type</code>.
     *
     *  @param  effortRecordType an effort record type 
     *  @return <code>true</code> if the effortRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type effortRecordType) {
        for (org.osid.resourcing.records.EffortQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(effortRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  effortRecordType the effort record type 
     *  @return the effort query record 
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(effortRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.EffortQueryRecord getEffortQueryRecord(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.EffortQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(effortRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(effortRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  effortRecordType the effort record type 
     *  @return the effort query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(effortRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.EffortQueryInspectorRecord getEffortQueryInspectorRecord(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.EffortQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(effortRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(effortRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param effortRecordType the effort record type
     *  @return the effort search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(effortRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.EffortSearchOrderRecord getEffortSearchOrderRecord(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.EffortSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(effortRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(effortRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this effort. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param effortQueryRecord the effort query record
     *  @param effortQueryInspectorRecord the effort query inspector
     *         record
     *  @param effortSearchOrderRecord the effort search order record
     *  @param effortRecordType effort record type
     *  @throws org.osid.NullArgumentException
     *          <code>effortQueryRecord</code>,
     *          <code>effortQueryInspectorRecord</code>,
     *          <code>effortSearchOrderRecord</code> or
     *          <code>effortRecordTypeeffort</code> is
     *          <code>null</code>
     */
            
    protected void addEffortRecords(org.osid.resourcing.records.EffortQueryRecord effortQueryRecord, 
                                      org.osid.resourcing.records.EffortQueryInspectorRecord effortQueryInspectorRecord, 
                                      org.osid.resourcing.records.EffortSearchOrderRecord effortSearchOrderRecord, 
                                      org.osid.type.Type effortRecordType) {

        addRecordType(effortRecordType);

        nullarg(effortQueryRecord, "effort query record");
        nullarg(effortQueryInspectorRecord, "effort query inspector record");
        nullarg(effortSearchOrderRecord, "effort search odrer record");

        this.queryRecords.add(effortQueryRecord);
        this.queryInspectorRecords.add(effortQueryInspectorRecord);
        this.searchOrderRecords.add(effortSearchOrderRecord);
        
        return;
    }
}
