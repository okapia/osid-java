//
// AbstractObjectiveBankSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objectivebank.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractObjectiveBankSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.learning.ObjectiveBankSearchResults {

    private org.osid.learning.ObjectiveBankList objectiveBanks;
    private final org.osid.learning.ObjectiveBankQueryInspector inspector;
    private final java.util.Collection<org.osid.learning.records.ObjectiveBankSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractObjectiveBankSearchResults.
     *
     *  @param objectiveBanks the result set
     *  @param objectiveBankQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>objectiveBanks</code>
     *          or <code>objectiveBankQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractObjectiveBankSearchResults(org.osid.learning.ObjectiveBankList objectiveBanks,
                                            org.osid.learning.ObjectiveBankQueryInspector objectiveBankQueryInspector) {
        nullarg(objectiveBanks, "objective banks");
        nullarg(objectiveBankQueryInspector, "objective bank query inspectpr");

        this.objectiveBanks = objectiveBanks;
        this.inspector = objectiveBankQueryInspector;

        return;
    }


    /**
     *  Gets the objective bank list resulting from a search.
     *
     *  @return an objective bank list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanks() {
        if (this.objectiveBanks == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.learning.ObjectiveBankList objectiveBanks = this.objectiveBanks;
        this.objectiveBanks = null;
	return (objectiveBanks);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.learning.ObjectiveBankQueryInspector getObjectiveBankQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  objective bank search record <code> Type. </code> This method must
     *  be used to retrieve an objectiveBank implementing the requested
     *  record.
     *
     *  @param objectiveBankSearchRecordType an objectiveBank search 
     *         record type 
     *  @return the objective bank search
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(objectiveBankSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveBankSearchResultsRecord getObjectiveBankSearchResultsRecord(org.osid.type.Type objectiveBankSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.learning.records.ObjectiveBankSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(objectiveBankSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(objectiveBankSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record objective bank search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addObjectiveBankRecord(org.osid.learning.records.ObjectiveBankSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "objective bank record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
