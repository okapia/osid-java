//
// AbstractFederatingScheduleLookupSession.java
//
//     An abstract federating adapter for a ScheduleLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ScheduleLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingScheduleLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.ScheduleLookupSession>
    implements org.osid.calendaring.ScheduleLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingScheduleLookupSession</code>.
     */

    protected AbstractFederatingScheduleLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.ScheduleLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>Schedule</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSchedules() {
        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            if (session.canLookupSchedules()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Schedule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeScheduleView() {
        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            session.useComparativeScheduleView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Schedule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryScheduleView() {
        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            session.usePlenaryScheduleView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedules in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Schedule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Schedule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Schedule</code> and
     *  retained for compatibility.
     *
     *  @param  scheduleId <code>Id</code> of the
     *          <code>Schedule</code>
     *  @return the schedule
     *  @throws org.osid.NotFoundException <code>scheduleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>scheduleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Schedule getSchedule(org.osid.id.Id scheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            try {
                return (session.getSchedule(scheduleId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(scheduleId + " not found");
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  schedules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Schedules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  scheduleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByIds(org.osid.id.IdList scheduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.schedule.MutableScheduleList ret = new net.okapia.osid.jamocha.calendaring.schedule.MutableScheduleList();

        try (org.osid.id.IdList ids = scheduleIds) {
            while (ids.hasNext()) {
                ret.addSchedule(getSchedule(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  schedule genus <code>Type</code> which does not include
     *  schedules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedulesByGenusType(scheduleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  schedule genus <code>Type</code> and include any additional
     *  schedules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByParentGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedulesByParentGenusType(scheduleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleList</code> containing the given
     *  schedule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByRecordType(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedulesByRecordType(scheduleRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ScheduleList </code> directly containing the
     *  given shedule slot. <code> </code> In plenary mode, the
     *  returned list contains all known schedule or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedule that are accessible through this session.
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @return the returned <code> Schedule </code> list 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedulesByScheduleSlot(scheduleSlotId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ScheduleList </code> containing the given
     *  location.  <code> </code> In plenary mode, the returned list
     *  contains all known schedule or an error results. Otherwise,
     *  the returned list may contain only those schedule that are
     *  accessible through this session.
     *
     *  @param  locationId a location <code> Id </code>
     *  @return the returned <code> Schedule </code> list
     *  @throws org.osid.NullArgumentException <code> locationId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedulesByLocation(locationId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleList</code> containing the given date. In
     *  plenary mode, the returned list contains all known schedule or
     *  an error results. Otherwise, the returned list may contain
     *  only those schedule that are accessible through this session.
     *
     *  Todo: this method only looks at the span of the schedule, n0t
     *  whether the date falls on a slot.
     *
     *  @param  date a date
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByDate(org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedulesByDate(date));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleList</code> contained by the given date
     *  range inclusive. In plenary mode, the returned
     *  list contains all known schedule or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedule that are accessible through this session.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByDateRange(org.osid.calendaring.DateTime from,
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedulesByDateRange(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Schedules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Schedules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList ret = getScheduleList();

        for (org.osid.calendaring.ScheduleLookupSession session : getSessions()) {
            ret.addScheduleList(session.getSchedules());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.FederatingScheduleList getScheduleList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.ParallelScheduleList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.schedule.CompositeScheduleList());
        }
    }
}
