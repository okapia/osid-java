//
// AbstractCustomerLookupSession.java
//
//    A starter implementation framework for providing a Customer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Customer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCustomers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCustomerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.billing.CustomerLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Customer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCustomers() {
        return (true);
    }


    /**
     *  A complete view of the <code>Customer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCustomerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Customer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCustomerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include customers in businesses which are
     *  children of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only customers whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveCustomerView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All customers of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCustomerView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Customer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Customer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Customer</code> and
     *  retained for compatibility.
     *
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param customerId <code>Id</code> of the <code>Customer</code>
     *  @return the customer
     *  @throws org.osid.NotFoundException <code>customerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>customerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer(org.osid.id.Id customerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.billing.CustomerList customers = getCustomers()) {
            while (customers.hasNext()) {
                org.osid.billing.Customer customer = customers.getNextCustomer();
                if (customer.getId().equals(customerId)) {
                    return (customer);
                }
            }
        } 

        throw new org.osid.NotFoundException(customerId + " not found");
    }


    /**
     *  Gets a <code>CustomerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  customers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Customers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCustomers()</code>.
     *
     *  @param  customerIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>customerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByIds(org.osid.id.IdList customerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Customer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = customerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCustomer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("customer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.billing.customer.LinkedCustomerList(ret));
    }


    /**
     *  Gets a <code>CustomerList</code> corresponding to the given
     *  customer genus <code>Type</code> which does not include
     *  customers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCustomers()</code>.
     *
     *  @param  customerGenusType a customer genus type 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByGenusType(org.osid.type.Type customerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerGenusFilterList(getCustomers(), customerGenusType));
    }


    /**
     *  Gets a <code>CustomerList</code> corresponding to the given
     *  customer genus <code>Type</code> and include any additional
     *  customers with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCustomers()</code>.
     *
     *  @param  customerGenusType a customer genus type 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByParentGenusType(org.osid.type.Type customerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCustomersByGenusType(customerGenusType));
    }


    /**
     *  Gets a <code>CustomerList</code> containing the given
     *  customer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCustomers()</code>.
     *
     *  @param  customerRecordType a customer record type 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByRecordType(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerRecordFilterList(getCustomers(), customerRecordType));
    }


    /**
     *  Gets a <code>CustomerList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *
     *  In effective mode, customers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective customers and those
     *  currently expired are returned.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>CustomerList</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersOnDate(org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.customer.TemporalCustomerFilterList(getCustomers(), from, to));
    }


    /**
     *  Gets a <code> CustomerList </code> related to the given
     *  customer number.
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  number a customer number
     *  @return the returned <code> CustomerList </code> list
     *  @throws org.osid.NullArgumentException <code> number </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerFilterList(new NumberFilter(number), getCustomers()));
    }


    /**
     *  Gets a <code>CustomerList</code> related to the given
     *  resource.
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @return the returned <code>CustomerList</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerFilterList(new ResourceFilter(resourceId), getCustomers()));
    }


    /**
     *  Gets a <code>CustomerList</code> of the given resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *
     *  In effective mode, customers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective customers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>CustomerList</code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByResourceOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.customer.TemporalCustomerFilterList(getCustomersByResource(resourceId), from, to));
    }


    /**
     *  Gets a <code>CustomerList</code> having the given activity.
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  activityId an activity <code> Id </code>
     *  @return the returned <code> CustomerList </code> list
     *  @throws org.osid.NullArgumentException <code> activityId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerFilterList(new ActivityFilter(activityId), getCustomers()));
    }


    /**
     *  Gets all <code>Customers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Customers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.billing.CustomerList getCustomers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the customer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of customers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.billing.CustomerList filterCustomersOnViews(org.osid.billing.CustomerList list)
        throws org.osid.OperationFailedException {
            
        org.osid.billing.CustomerList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.billing.customer.EffectiveCustomerFilterList(ret);
        }

        return (ret);
    }


    public static class NumberFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerFilter {

        private final String number;

        
        /**
         *  Constructs a new <code>NumberFilter</code>.
         *
         *  @param number the customer number to filter
         *  @throws org.osid.NullArgumentException <code>number</code>
         *          is <code>null</code>
         */

        public NumberFilter(String number) {
            nullarg(number, "number");
            this.number = number;
            return;
        }


        /**
         *  Used by the CustomerFilterList to filter the customer list
         *  based on customer number.
         *
         *  @param customer the customer
         *  @return <code>true</code> to pass the customer,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.billing.Customer customer) {
            return (customer.getCustomerNumber().equals(this.number));
        }
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the customer resource to filter
         *  @throws org.osid.NullArgumentException <code>resourceId</code>
         *          is <code>null</code>
         */

        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the CustomerFilterList to filter the customer list
         *  based on customer resource.
         *
         *  @param customer the customer
         *  @return <code>true</code> to pass the customer,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.billing.Customer customer) {
            return (customer.getResourceId().equals(this.resourceId));
        }
    }


    public static class ActivityFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.customer.CustomerFilter {

        private final org.osid.id.Id activityId;

        
        /**
         *  Constructs a new <code>ActivityFilter</code>.
         *
         *  @param activityId the customer activity to filter
         *  @throws org.osid.NullArgumentException <code>activityId</code>
         *          is <code>null</code>
         */

        public ActivityFilter(org.osid.id.Id activityId) {
            nullarg(activityId, "activity Id");
            this.activityId = activityId;
            return;
        }


        /**
         *  Used by the CustomerFilterList to filter the customer list
         *  based on customer activity.
         *
         *  @param customer the customer
         *  @return <code>true</code> to pass the customer,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.billing.Customer customer) {
            return (customer.getActivityId().equals(this.activityId));
        }
    }
}
