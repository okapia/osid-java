//
// AbstractPersonSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.person.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPersonSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.personnel.PersonSearchResults {

    private org.osid.personnel.PersonList persons;
    private final org.osid.personnel.PersonQueryInspector inspector;
    private final java.util.Collection<org.osid.personnel.records.PersonSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPersonSearchResults.
     *
     *  @param persons the result set
     *  @param personQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>persons</code>
     *          or <code>personQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPersonSearchResults(org.osid.personnel.PersonList persons,
                                            org.osid.personnel.PersonQueryInspector personQueryInspector) {
        nullarg(persons, "persons");
        nullarg(personQueryInspector, "person query inspectpr");

        this.persons = persons;
        this.inspector = personQueryInspector;

        return;
    }


    /**
     *  Gets the person list resulting from a search.
     *
     *  @return a person list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersons() {
        if (this.persons == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.personnel.PersonList persons = this.persons;
        this.persons = null;
	return (persons);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.personnel.PersonQueryInspector getPersonQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  person search record <code> Type. </code> This method must
     *  be used to retrieve a person implementing the requested
     *  record.
     *
     *  @param personSearchRecordType a person search 
     *         record type 
     *  @return the person search
     *  @throws org.osid.NullArgumentException
     *          <code>personSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(personSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PersonSearchResultsRecord getPersonSearchResultsRecord(org.osid.type.Type personSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.personnel.records.PersonSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(personSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(personSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record person search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPersonRecord(org.osid.personnel.records.PersonSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "person record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
