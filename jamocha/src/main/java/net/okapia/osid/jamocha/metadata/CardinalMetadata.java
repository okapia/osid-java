//
// CardinalMetadata.java
//
//     Defines a cardinal Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a cardinal Metadata.
 */

public final class CardinalMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractCardinalMetadata {


    /**
     *  Constructs a new single unlinked {@code CardinalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public CardinalMetadata(org.osid.id.Id elementId) {
        super(elementId, false, false);
        return;
    }


    /**
     *  Constructs a new unlinked {@code CardinalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array
     *         {@code false} if single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public CardinalMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code CardinalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array
     *         {@code false} if single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public CardinalMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }
    

    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *  @param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }

    
    /**
     *  Sets the cardinal range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    public void setCardinalRange(long min, long max) {
        super.setCardinalRange(min, max);
        return;
    }

    
    /**
     *  Sets the cardinal set.
     *
     *  @param values a collection of accepted cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setCardinalSet(java.util.Collection<Long> values) {
        super.setCardinalSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the cardinal set.
     *
     *  @param values a collection of accepted cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToCardinalSet(java.util.Collection<Long> values) {
        super.addToCardinalSet(values);
        return;
    }


    /**
     *  Adds a value to the cardinal set.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void addToCardinalSet(long value) {
        super.addToCardinalSet(value);
        return;
    }


    /**
     *  Removes a value from the cardinal set.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void removeFromCardinalSet(long value) {
        super.removeFromCardinalSet(value);
        return;
    }


    /**
     *  Clears the cardinal set.
     */

    public void clearCardinalSet() {
        super.clearCardinalSet();
        return;
    }
}
