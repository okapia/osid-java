//
// AbstractDistributorQueryInspector.java
//
//     A template for making a DistributorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.distributor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for distributors.
 */

public abstract class AbstractDistributorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.provisioning.DistributorQueryInspector {

    private final java.util.Collection<org.osid.provisioning.records.DistributorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.provisioning.QueueQueryInspector[0]);
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPoolIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Gets the provision <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProvisionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the provision query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQueryInspector[] getProvisionTerms() {
        return (new org.osid.provisioning.ProvisionQueryInspector[0]);
    }


    /**
     *  Gets the ancestor distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getAncestorDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the descendant distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDescendantDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given distributor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a distributor implementing the requested record.
     *
     *  @param distributorRecordType a distributor record type
     *  @return the distributor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(distributorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorQueryInspectorRecord getDistributorQueryInspectorRecord(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.DistributorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(distributorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(distributorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this distributor query. 
     *
     *  @param distributorQueryInspectorRecord distributor query inspector
     *         record
     *  @param distributorRecordType distributor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDistributorQueryInspectorRecord(org.osid.provisioning.records.DistributorQueryInspectorRecord distributorQueryInspectorRecord, 
                                                   org.osid.type.Type distributorRecordType) {

        addRecordType(distributorRecordType);
        nullarg(distributorRecordType, "distributor record type");
        this.records.add(distributorQueryInspectorRecord);        
        return;
    }
}
