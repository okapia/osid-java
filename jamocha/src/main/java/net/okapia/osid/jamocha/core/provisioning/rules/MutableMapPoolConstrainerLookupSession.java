//
// MutableMapPoolConstrainerLookupSession
//
//    Implements a PoolConstrainer lookup service backed by a collection of
//    poolConstrainers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolConstrainer lookup service backed by a collection of
 *  pool constrainers. The pool constrainers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of pool constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapPoolConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolConstrainerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {


    /**
     *  Constructs a new {@code MutableMapPoolConstrainerLookupSession}
     *  with no pool constrainers.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolConstrainerLookupSession} with a
     *  single poolConstrainer.
     *
     *  @param distributor the distributor  
     *  @param poolConstrainer a pool constrainer
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainer} is {@code null}
     */

    public MutableMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.PoolConstrainer poolConstrainer) {
        this(distributor);
        putPoolConstrainer(poolConstrainer);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolConstrainerLookupSession}
     *  using an array of pool constrainers.
     *
     *  @param distributor the distributor
     *  @param poolConstrainers an array of pool constrainers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainers} is {@code null}
     */

    public MutableMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.PoolConstrainer[] poolConstrainers) {
        this(distributor);
        putPoolConstrainers(poolConstrainers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolConstrainerLookupSession}
     *  using a collection of pool constrainers.
     *
     *  @param distributor the distributor
     *  @param poolConstrainers a collection of pool constrainers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainers} is {@code null}
     */

    public MutableMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.rules.PoolConstrainer> poolConstrainers) {

        this(distributor);
        putPoolConstrainers(poolConstrainers);
        return;
    }

    
    /**
     *  Makes a {@code PoolConstrainer} available in this session.
     *
     *  @param poolConstrainer a pool constrainer
     *  @throws org.osid.NullArgumentException {@code poolConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putPoolConstrainer(org.osid.provisioning.rules.PoolConstrainer poolConstrainer) {
        super.putPoolConstrainer(poolConstrainer);
        return;
    }


    /**
     *  Makes an array of pool constrainers available in this session.
     *
     *  @param poolConstrainers an array of pool constrainers
     *  @throws org.osid.NullArgumentException {@code poolConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putPoolConstrainers(org.osid.provisioning.rules.PoolConstrainer[] poolConstrainers) {
        super.putPoolConstrainers(poolConstrainers);
        return;
    }


    /**
     *  Makes collection of pool constrainers available in this session.
     *
     *  @param poolConstrainers a collection of pool constrainers
     *  @throws org.osid.NullArgumentException {@code poolConstrainers{@code  is
     *          {@code null}
     */

    @Override
    public void putPoolConstrainers(java.util.Collection<? extends org.osid.provisioning.rules.PoolConstrainer> poolConstrainers) {
        super.putPoolConstrainers(poolConstrainers);
        return;
    }


    /**
     *  Removes a PoolConstrainer from this session.
     *
     *  @param poolConstrainerId the {@code Id} of the pool constrainer
     *  @throws org.osid.NullArgumentException {@code poolConstrainerId{@code 
     *          is {@code null}
     */

    @Override
    public void removePoolConstrainer(org.osid.id.Id poolConstrainerId) {
        super.removePoolConstrainer(poolConstrainerId);
        return;
    }    
}
