//
// AccountElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.account.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AccountElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the AccountElement Id.
     *
     *  @return the account element Id
     */

    public static org.osid.id.Id getAccountEntityId() {
        return (makeEntityId("osid.financials.Account"));
    }


    /**
     *  Gets the Code element Id.
     *
     *  @return the Code element Id
     */

    public static org.osid.id.Id getCode() {
        return (makeElementId("osid.financials.account.Code"));
    }


    /**
     *  Gets the CreditBalance element Id.
     *
     *  @return the CreditBalance element Id
     */

    public static org.osid.id.Id getCreditBalance() {
        return (makeElementId("osid.financials.account.CreditBalance"));
    }


    /**
     *  Gets the Summary element Id.
     *
     *  @return the Summary element Id
     */

    public static org.osid.id.Id getSummary() {
        return (makeQueryElementId("osid.financials.account.Summary"));
    }


    /**
     *  Gets the AncestorAccountId element Id.
     *
     *  @return the AncestorAccountId element Id
     */

    public static org.osid.id.Id getAncestorAccountId() {
        return (makeQueryElementId("osid.financials.account.AncestorAccountId"));
    }


    /**
     *  Gets the AncestorAccount element Id.
     *
     *  @return the AncestorAccount element Id
     */

    public static org.osid.id.Id getAncestorAccount() {
        return (makeQueryElementId("osid.financials.account.AncestorAccount"));
    }


    /**
     *  Gets the DescendantAccountId element Id.
     *
     *  @return the DescendantAccountId element Id
     */

    public static org.osid.id.Id getDescendantAccountId() {
        return (makeQueryElementId("osid.financials.account.DescendantAccountId"));
    }


    /**
     *  Gets the DescendantAccount element Id.
     *
     *  @return the DescendantAccount element Id
     */

    public static org.osid.id.Id getDescendantAccount() {
        return (makeQueryElementId("osid.financials.account.DescendantAccount"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.financials.account.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.financials.account.Business"));
    }
}
