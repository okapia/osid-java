//
// AbstractRequisiteValidator.java
//
//     Validates a Requisite.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.requisite.requisite.spi;


/**
 *  Validates a Requisite.
 */

public abstract class AbstractRequisiteValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidEnablerValidator {


    /**
     *  Constructs a new <code>AbstractRequisiteValidator</code>.
     */

    protected AbstractRequisiteValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractRequisiteValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractRequisiteValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Requisite.
     *
     *  @param requisite a requisite to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>requisite</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.requisite.Requisite requisite) {
        super.validate(requisite);

        testConditionalMethod(requisite, "getRequisiteOptions", requisite.hasRequisiteOptions(), "hasRequisiteOptions()");
        testConditionalMethod(requisite, "getCourseRequirements", requisite.hasCourseRequirements(), "hasCourseRequirements()");
        testConditionalMethod(requisite, "getProgramRequirements", requisite.hasProgramRequirements(), "hasProgramRequirements()");
        testConditionalMethod(requisite, "getCredentialRequirements", requisite.hasCredentialRequirements(), "hasCredentialRequirements()");
        testConditionalMethod(requisite, "getLearningObjectiveRequirements", requisite.hasLearningObjectiveRequirements(), "hasLearningObjectiveRequirements()");
        testConditionalMethod(requisite, "getAssessmentRequirements", requisite.hasAssessmentRequirements(), "hasAssessmentRequirements()");
        testConditionalMethod(requisite, "getAwardRequirements", requisite.hasAwardRequirements(), "hasAwardRequirements()");

        return;
    }
}
