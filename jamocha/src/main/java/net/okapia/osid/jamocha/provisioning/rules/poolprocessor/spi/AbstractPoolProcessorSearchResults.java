//
// AbstractPoolProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPoolProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.PoolProcessorSearchResults {

    private org.osid.provisioning.rules.PoolProcessorList poolProcessors;
    private final org.osid.provisioning.rules.PoolProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPoolProcessorSearchResults.
     *
     *  @param poolProcessors the result set
     *  @param poolProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>poolProcessors</code>
     *          or <code>poolProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPoolProcessorSearchResults(org.osid.provisioning.rules.PoolProcessorList poolProcessors,
                                            org.osid.provisioning.rules.PoolProcessorQueryInspector poolProcessorQueryInspector) {
        nullarg(poolProcessors, "pool processors");
        nullarg(poolProcessorQueryInspector, "pool processor query inspectpr");

        this.poolProcessors = poolProcessors;
        this.inspector = poolProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the pool processor list resulting from a search.
     *
     *  @return a pool processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessors() {
        if (this.poolProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.PoolProcessorList poolProcessors = this.poolProcessors;
        this.poolProcessors = null;
	return (poolProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.PoolProcessorQueryInspector getPoolProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  pool processor search record <code> Type. </code> This method must
     *  be used to retrieve a poolProcessor implementing the requested
     *  record.
     *
     *  @param poolProcessorSearchRecordType a poolProcessor search 
     *         record type 
     *  @return the pool processor search
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(poolProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorSearchResultsRecord getPoolProcessorSearchResultsRecord(org.osid.type.Type poolProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.PoolProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(poolProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record pool processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPoolProcessorRecord(org.osid.provisioning.rules.records.PoolProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "pool processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
