//
// AbstractAdapterQueueProcessorLookupSession.java
//
//    A QueueProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A QueueProcessor lookup session adapter.
 */

public abstract class AbstractAdapterQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.QueueProcessorLookupSession {

    private final org.osid.provisioning.rules.QueueProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterQueueProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterQueueProcessorLookupSession(org.osid.provisioning.rules.QueueProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code QueueProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupQueueProcessors() {
        return (this.session.canLookupQueueProcessors());
    }


    /**
     *  A complete view of the {@code QueueProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueProcessorView() {
        this.session.useComparativeQueueProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code QueueProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueProcessorView() {
        this.session.usePlenaryQueueProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue processors in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active queue processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueProcessorView() {
        this.session.useActiveQueueProcessorView();
        return;
    }


    /**
     *  Active and inactive queue processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueProcessorView() {
        this.session.useAnyStatusQueueProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code QueueProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code QueueProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code QueueProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, queue processors are returned that are currently
     *  active. In any status mode, active and inactive queue processors
     *  are returned.
     *
     *  @param queueProcessorId {@code Id} of the {@code QueueProcessor}
     *  @return the queue processor
     *  @throws org.osid.NotFoundException {@code queueProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code queueProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessor getQueueProcessor(org.osid.id.Id queueProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessor(queueProcessorId));
    }


    /**
     *  Gets a {@code QueueProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queueProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code QueueProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, queue processors are returned that are currently
     *  active. In any status mode, active and inactive queue processors
     *  are returned.
     *
     *  @param  queueProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code QueueProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByIds(org.osid.id.IdList queueProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorsByIds(queueProcessorIds));
    }


    /**
     *  Gets a {@code QueueProcessorList} corresponding to the given
     *  queue processor genus {@code Type} which does not include
     *  queue processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  queue processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processors are returned that are currently
     *  active. In any status mode, active and inactive queue processors
     *  are returned.
     *
     *  @param  queueProcessorGenusType a queueProcessor genus type 
     *  @return the returned {@code QueueProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByGenusType(org.osid.type.Type queueProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorsByGenusType(queueProcessorGenusType));
    }


    /**
     *  Gets a {@code QueueProcessorList} corresponding to the given
     *  queue processor genus {@code Type} and include any additional
     *  queue processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  queue processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processors are returned that are currently
     *  active. In any status mode, active and inactive queue processors
     *  are returned.
     *
     *  @param  queueProcessorGenusType a queueProcessor genus type 
     *  @return the returned {@code QueueProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByParentGenusType(org.osid.type.Type queueProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorsByParentGenusType(queueProcessorGenusType));
    }


    /**
     *  Gets a {@code QueueProcessorList} containing the given
     *  queue processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  queue processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processors are returned that are currently
     *  active. In any status mode, active and inactive queue processors
     *  are returned.
     *
     *  @param  queueProcessorRecordType a queueProcessor record type 
     *  @return the returned {@code QueueProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByRecordType(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessorsByRecordType(queueProcessorRecordType));
    }


    /**
     *  Gets all {@code QueueProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  queue processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue processors are returned that are currently
     *  active. In any status mode, active and inactive queue processors
     *  are returned.
     *
     *  @return a list of {@code QueueProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueProcessors());
    }
}
