//
// AbstractAdapterBallotLookupSession.java
//
//    A Ballot lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Ballot lookup session adapter.
 */

public abstract class AbstractAdapterBallotLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.BallotLookupSession {

    private final org.osid.voting.BallotLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBallotLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBallotLookupSession(org.osid.voting.BallotLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code Ballot} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBallots() {
        return (this.session.canLookupBallots());
    }


    /**
     *  A complete view of the {@code Ballot} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBallotView() {
        this.session.useComparativeBallotView();
        return;
    }


    /**
     *  A complete view of the {@code Ballot} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBallotView() {
        this.session.usePlenaryBallotView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballots in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active ballots are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBallotView() {
        this.session.useActiveBallotView();
        return;
    }


    /**
     *  Active and inactive ballots are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotView() {
        this.session.useAnyStatusBallotView();
        return;
    }
    
     
    /**
     *  Gets the {@code Ballot} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Ballot} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Ballot} and
     *  retained for compatibility.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param ballotId {@code Id} of the {@code Ballot}
     *  @return the ballot
     *  @throws org.osid.NotFoundException {@code ballotId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code ballotId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Ballot getBallot(org.osid.id.Id ballotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallot(ballotId));
    }


    /**
     *  Gets a {@code BallotList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ballots specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Ballots} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param  ballotIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Ballot} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code ballotIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByIds(org.osid.id.IdList ballotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotsByIds(ballotIds));
    }


    /**
     *  Gets a {@code BallotList} corresponding to the given
     *  ballot genus {@code Type} which does not include
     *  ballots of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  ballots or an error results. Otherwise, the returned list
     *  may contain only those ballots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param  ballotGenusType a ballot genus type 
     *  @return the returned {@code Ballot} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByGenusType(org.osid.type.Type ballotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotsByGenusType(ballotGenusType));
    }


    /**
     *  Gets a {@code BallotList} corresponding to the given
     *  ballot genus {@code Type} and include any additional
     *  ballots with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  ballots or an error results. Otherwise, the returned list
     *  may contain only those ballots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param  ballotGenusType a ballot genus type 
     *  @return the returned {@code Ballot} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByParentGenusType(org.osid.type.Type ballotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotsByParentGenusType(ballotGenusType));
    }


    /**
     *  Gets a {@code BallotList} containing the given
     *  ballot record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ballots or an error results. Otherwise, the returned list
     *  may contain only those ballots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param  ballotRecordType a ballot record type 
     *  @return the returned {@code Ballot} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByRecordType(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotsByRecordType(ballotRecordType));
    }


    /**
     *  Gets a {@code BallotList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  ballots or an error results. Otherwise, the returned list
     *  may contain only those ballots that are accessible through
     *  this session.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Ballot} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotsByProvider(resourceId));
    }


    /**
     *  Gets a {@code BallotList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  ballots or an error results. Otherwise, the returned list
     *  may contain only those ballots that are accessible
     *  through this session.
     *  
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Ballot} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.BallotList getBallotsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotsOnDate(from, to));
    }
        

    /**
     *  Gets all {@code Ballots}. 
     *
     *  In plenary mode, the returned list contains all known
     *  ballots or an error results. Otherwise, the returned list
     *  may contain only those ballots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @return a list of {@code Ballots} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallots());
    }
}
