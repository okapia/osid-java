//
// AbstractQueryDemographicLookupSession.java
//
//    An inline adapter that maps a DemographicLookupSession to
//    a DemographicQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a DemographicLookupSession to
 *  a DemographicQuerySession.
 */

public abstract class AbstractQueryDemographicLookupSession
    extends net.okapia.osid.jamocha.resource.demographic.spi.AbstractDemographicLookupSession
    implements org.osid.resource.demographic.DemographicLookupSession {

    private boolean activeonly    = false;
    private final org.osid.resource.demographic.DemographicQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryDemographicLookupSession.
     *
     *  @param querySession the underlying demographic query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryDemographicLookupSession(org.osid.resource.demographic.DemographicQuerySession querySession) {
        nullarg(querySession, "demographic query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bin</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this 
     *  session.
     *
     *  @return the <code>Bin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform <code>Demographic</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDemographics() {
        return (this.session.canSearchDemographics());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include demographics in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    

    /**
     *  Only active demographics are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveDemographicView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive demographics are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDemographicView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Demographic</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Demographic</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Demographic</code> and
     *  retained for compatibility.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicId <code>Id</code> of the
     *          <code>Demographic</code>
     *  @return the demographic
     *  @throws org.osid.NotFoundException <code>demographicId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>demographicId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.Demographic getDemographic(org.osid.id.Id demographicId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicQuery query = getQuery();
        query.matchId(demographicId, true);
        org.osid.resource.demographic.DemographicList demographics = this.session.getDemographicsByQuery(query);
        if (demographics.hasNext()) {
            return (demographics.getNextDemographic());
        } 
        
        throw new org.osid.NotFoundException(demographicId + " not found");
    }


    /**
     *  Gets a <code>DemographicList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  demographics specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Demographics</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Demographic</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>demographicIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByIds(org.osid.id.IdList demographicIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicQuery query = getQuery();

        try (org.osid.id.IdList ids = demographicIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getDemographicsByQuery(query));
    }


    /**
     *  Gets a <code>DemographicList</code> corresponding to the given
     *  demographic genus <code>Type</code> which does not include
     *  demographics of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicGenusType a demographic genus type 
     *  @return the returned <code>Demographic</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByGenusType(org.osid.type.Type demographicGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicQuery query = getQuery();
        query.matchGenusType(demographicGenusType, true);
        return (this.session.getDemographicsByQuery(query));
    }


    /**
     *  Gets a <code>DemographicList</code> corresponding to the given
     *  demographic genus <code>Type</code> and include any additional
     *  demographics with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicGenusType a demographic genus type 
     *  @return the returned <code>Demographic</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByParentGenusType(org.osid.type.Type demographicGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicQuery query = getQuery();
        query.matchParentGenusType(demographicGenusType, true);
        return (this.session.getDemographicsByQuery(query));
    }


    /**
     *  Gets a <code>DemographicList</code> containing the given
     *  demographic record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicRecordType a demographic record type 
     *  @return the returned <code>Demographic</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByRecordType(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicQuery query = getQuery();
        query.matchRecordType(demographicRecordType, true);
        return (this.session.getDemographicsByQuery(query));
    }

    
    /**
     *  Gets all <code>Demographics</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @return a list of <code>Demographics</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographics()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getDemographicsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resource.demographic.DemographicQuery getQuery() {
        org.osid.resource.demographic.DemographicQuery query = this.session.getDemographicQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
