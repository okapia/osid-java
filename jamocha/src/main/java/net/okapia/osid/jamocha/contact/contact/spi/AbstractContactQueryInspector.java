//
// AbstractContactQueryInspector.java
//
//     A template for making a ContactQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for contacts.
 */

public abstract class AbstractContactQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.contact.ContactQueryInspector {

    private final java.util.Collection<org.osid.contact.records.ContactQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the reference <code> Id </code> terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddresseeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getAddresseeTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the address <code> Id </code> terms. 
     *
     *  @return the address <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the address terms. 
     *
     *  @return the address terms 
     */

    @OSID @Override
    public org.osid.contact.AddressQueryInspector[] getAddressTerms() {
        return (new org.osid.contact.AddressQueryInspector[0]);
    }


    /**
     *  Gets the address book <code> Id </code> terms. 
     *
     *  @return the address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the address book terms. 
     *
     *  @return the address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given contact query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a contact implementing the requested record.
     *
     *  @param contactRecordType a contact record type
     *  @return the contact query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactQueryInspectorRecord getContactQueryInspectorRecord(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.ContactQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(contactRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactRecordType + " is not supported");
    }


    /**
     *  Adds a record to this contact query. 
     *
     *  @param contactQueryInspectorRecord contact query inspector
     *         record
     *  @param contactRecordType contact record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addContactQueryInspectorRecord(org.osid.contact.records.ContactQueryInspectorRecord contactQueryInspectorRecord, 
                                                   org.osid.type.Type contactRecordType) {

        addRecordType(contactRecordType);
        nullarg(contactRecordType, "contact record type");
        this.records.add(contactQueryInspectorRecord);        
        return;
    }
}
