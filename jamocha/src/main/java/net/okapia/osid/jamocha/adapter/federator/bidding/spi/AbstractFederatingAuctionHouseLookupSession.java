//
// AbstractFederatingAuctionHouseLookupSession.java
//
//     An abstract federating adapter for an AuctionHouseLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AuctionHouseLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.bidding.AuctionHouseLookupSession>
    implements org.osid.bidding.AuctionHouseLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingAuctionHouseLookupSession</code>.
     */

    protected AbstractFederatingAuctionHouseLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.bidding.AuctionHouseLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>AuctionHouse</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionHouses() {
        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            if (session.canLookupAuctionHouses()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AuctionHouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionHouseView() {
        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            session.useComparativeAuctionHouseView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AuctionHouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionHouseView() {
        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            session.usePlenaryAuctionHouseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AuctionHouse</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionHouse</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionHouse</code> and
     *  retained for compatibility.
     *
     *  @param  auctionHouseId <code>Id</code> of the
     *          <code>AuctionHouse</code>
     *  @return the auction house
     *  @throws org.osid.NotFoundException <code>auctionHouseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionHouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            try {
                return (session.getAuctionHouse(auctionHouseId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(auctionHouseId + " not found");
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionHouses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuctionHouses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  auctionHouseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByIds(org.osid.id.IdList auctionHouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.bidding.auctionhouse.MutableAuctionHouseList ret = new net.okapia.osid.jamocha.bidding.auctionhouse.MutableAuctionHouseList();

        try (org.osid.id.IdList ids = auctionHouseIds) {
            while (ids.hasNext()) {
                ret.addAuctionHouse(getAuctionHouse(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  auction house genus <code>Type</code> which does not include
     *  auction houses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.FederatingAuctionHouseList ret = getAuctionHouseList();

        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            ret.addAuctionHouseList(session.getAuctionHousesByGenusType(auctionHouseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  auction house genus <code>Type</code> and include any additional
     *  auction houses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByParentGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.FederatingAuctionHouseList ret = getAuctionHouseList();

        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            ret.addAuctionHouseList(session.getAuctionHousesByParentGenusType(auctionHouseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionHouseList</code> containing the given
     *  auction house record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseRecordType an auctionHouse record type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByRecordType(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.FederatingAuctionHouseList ret = getAuctionHouseList();

        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            ret.addAuctionHouseList(session.getAuctionHousesByRecordType(auctionHouseRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionHouseList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known auction houses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  auction houses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>AuctionHouse</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.FederatingAuctionHouseList ret = getAuctionHouseList();

        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            ret.addAuctionHouseList(session.getAuctionHousesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AuctionHouses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AuctionHouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.FederatingAuctionHouseList ret = getAuctionHouseList();

        for (org.osid.bidding.AuctionHouseLookupSession session : getSessions()) {
            ret.addAuctionHouseList(session.getAuctionHouses());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.FederatingAuctionHouseList getAuctionHouseList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.ParallelAuctionHouseList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.auctionhouse.CompositeAuctionHouseList());
        }
    }
}
