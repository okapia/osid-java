//
// ActionGroupElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.actiongroup;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActionGroupElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ActionGroupElement Id.
     *
     *  @return the action group element Id
     */

    public static org.osid.id.Id getActionGroupEntityId() {
        return (makeEntityId("osid.control.ActionGroup"));
    }


    /**
     *  Gets the ActionIds element Id.
     *
     *  @return the ActionIds element Id
     */

    public static org.osid.id.Id getActionIds() {
        return (makeElementId("osid.control.actiongroup.ActionIds"));
    }


    /**
     *  Gets the Actions element Id.
     *
     *  @return the Actions element Id
     */

    public static org.osid.id.Id getActions() {
        return (makeElementId("osid.control.actiongroup.Actions"));
    }


    /**
     *  Gets the SystemId element Id.
     *
     *  @return the SystemId element Id
     */

    public static org.osid.id.Id getSystemId() {
        return (makeQueryElementId("osid.control.actiongroup.SystemId"));
    }


    /**
     *  Gets the System element Id.
     *
     *  @return the System element Id
     */

    public static org.osid.id.Id getSystem() {
        return (makeQueryElementId("osid.control.actiongroup.System"));
    }
}
