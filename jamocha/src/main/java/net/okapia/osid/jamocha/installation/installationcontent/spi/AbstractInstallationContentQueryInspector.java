//
// AbstractInstallationContentQueryInspector.java
//
//     A template for making an InstallationContentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installationcontent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for installation contents.
 */

public abstract class AbstractInstallationContentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.installation.InstallationContentQueryInspector {

    private final java.util.Collection<org.osid.installation.records.InstallationContentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the data length query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getDataLengthTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the data query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BytesTerm[] getDataTerms() {
        return (new org.osid.search.terms.BytesTerm[0]);
    }



    /**
     *  Gets the record corresponding to the given installation content query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an installation content implementing the requested record.
     *
     *  @param installationContentRecordType an installation content record type
     *  @return the installation content query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>installationContentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationContentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationContentQueryInspectorRecord getInstallationContentQueryInspectorRecord(org.osid.type.Type installationContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationContentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(installationContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationContentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this installation content query. 
     *
     *  @param installationContentQueryInspectorRecord installation content query inspector
     *         record
     *  @param installationContentRecordType installationContent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstallationContentQueryInspectorRecord(org.osid.installation.records.InstallationContentQueryInspectorRecord installationContentQueryInspectorRecord, 
                                                   org.osid.type.Type installationContentRecordType) {

        addRecordType(installationContentRecordType);
        nullarg(installationContentRecordType, "installation content record type");
        this.records.add(installationContentQueryInspectorRecord);        
        return;
    }
}
