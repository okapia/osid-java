//
// AbstractNodeQueryInspector.java
//
//     A template for making a NodeQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.node.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for nodes.
 */

public abstract class AbstractNodeQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.topology.NodeQueryInspector {

    private final java.util.Collection<org.osid.topology.records.NodeQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the edge <code> Id </code> terms. 
     *
     *  @return the edge <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEdgeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the edge terms. 
     *
     *  @return the edge terms 
     */

    @OSID @Override
    public org.osid.topology.EdgeQueryInspector[] getEdgeTerms() {
        return (new org.osid.topology.EdgeQueryInspector[0]);
    }


    /**
     *  Gets the graph <code> Id </code> terms. 
     *
     *  @return the graph <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraphIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the graph terms. 
     *
     *  @return the graph terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given node query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a node implementing the requested record.
     *
     *  @param nodeRecordType a node record type
     *  @return the node query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>nodeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(nodeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.NodeQueryInspectorRecord getNodeQueryInspectorRecord(org.osid.type.Type nodeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.NodeQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(nodeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(nodeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this node query. 
     *
     *  @param nodeQueryInspectorRecord node query inspector
     *         record
     *  @param nodeRecordType node record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addNodeQueryInspectorRecord(org.osid.topology.records.NodeQueryInspectorRecord nodeQueryInspectorRecord, 
                                                   org.osid.type.Type nodeRecordType) {

        addRecordType(nodeRecordType);
        nullarg(nodeRecordType, "node record type");
        this.records.add(nodeQueryInspectorRecord);        
        return;
    }
}
