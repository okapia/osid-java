//
// AbstractGradebookColumnLookupSession.java
//
//    A starter implementation framework for providing a GradebookColumn
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a GradebookColumn
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getGradebookColumns(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractGradebookColumnLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.grading.GradebookColumnLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();
    

    /**
     *  Gets the <code>Gradebook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the <code>Gradebook</code>.
     *
     *  @param  gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can perform <code>GradebookColumn</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradebookColumns() {
        return (true);
    }


    /**
     *  A complete view of the <code>GradebookColumn</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradebookColumnView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>GradebookColumn</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradebookColumnView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include gradebook columns in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>GradebookColumn</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradebookColumn</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradebookColumn</code> and
     *  retained for compatibility.
     *
     *  @param  gradebookColumnId <code>Id</code> of the
     *          <code>GradebookColumn</code>
     *  @return the gradebook column
     *  @throws org.osid.NotFoundException <code>gradebookColumnId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.grading.GradebookColumnList gradebookColumns = getGradebookColumns()) {
            while (gradebookColumns.hasNext()) {
                org.osid.grading.GradebookColumn gradebookColumn = gradebookColumns.getNextGradebookColumn();
                if (gradebookColumn.getId().equals(gradebookColumnId)) {
                    return (gradebookColumn);
                }
            }
        } 

        throw new org.osid.NotFoundException(gradebookColumnId + " not found");
    }


    /**
     *  Gets a <code>GradebookColumnList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradebookColumns specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>GradebookColumns</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getGradebookColumns()</code>.
     *
     *  @param  gradebookColumnIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradebookColumn</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByIds(org.osid.id.IdList gradebookColumnIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.grading.GradebookColumn> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = gradebookColumnIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getGradebookColumn(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("gradebook column " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.grading.gradebookcolumn.LinkedGradebookColumnList(ret));
    }


    /**
     *  Gets a <code>GradebookColumnList</code> corresponding to the given
     *  gradebook column genus <code>Type</code> which does not include
     *  gradebook columns of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getGradebookColumns()</code>.
     *
     *  @param  gradebookColumnGenusType a gradebookColumn genus type 
     *  @return the returned <code>GradebookColumn</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByGenusType(org.osid.type.Type gradebookColumnGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradebookcolumn.GradebookColumnGenusFilterList(getGradebookColumns(), gradebookColumnGenusType));
    }


    /**
     *  Gets a <code>GradebookColumnList</code> corresponding to the given
     *  gradebook column genus <code>Type</code> and include any additional
     *  gradebook columns with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradebookColumns()</code>.
     *
     *  @param  gradebookColumnGenusType a gradebookColumn genus type 
     *  @return the returned <code>GradebookColumn</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByParentGenusType(org.osid.type.Type gradebookColumnGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGradebookColumnsByGenusType(gradebookColumnGenusType));
    }


    /**
     *  Gets a <code>GradebookColumnList</code> containing the given
     *  gradebook column record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradebookColumns()</code>.
     *
     *  @param  gradebookColumnRecordType a gradebookColumn record type 
     *  @return the returned <code>GradebookColumn</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByRecordType(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradebookcolumn.GradebookColumnRecordFilterList(getGradebookColumns(), gradebookColumnRecordType));
    }


    /**
     *  Gets all <code>GradebookColumns</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>GradebookColumns</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.grading.GradebookColumnList getGradebookColumns()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Tests if a summary entry is available. 
     *
     *  @return <code>true</code> if a summary entry is available,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean supportsSummary() {
        return (false);
    }


    /**
     *  Gets the <code>GradebookColumnSummary</code> for summary
     *  results.
     *
     *  @param gradebookColumnId <code>Id</code> of the
     *          <code>GradebookColumn</code>
     *  @return the gradebook column summary 
     *  @throws org.osid.NotFoundException <code>gradebookColumnId</code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnimplementedException
     *          <code>hasSummary()</code> is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummary getGradebookColumnSummary(org.osid.id.Id gradebookColumnId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("supportsSummary() is false");
    }


    /**
     *  Filters the gradebook column list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of gradebook columns
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.grading.GradebookColumnList filterGradebookColumnsOnViews(org.osid.grading.GradebookColumnList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
