//
// AbstractAuthorizationManager.java
//
//     An adapter for a AuthorizationManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AuthorizationManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAuthorizationManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.authorization.AuthorizationManager>
    implements org.osid.authorization.AuthorizationManager {


    /**
     *  Constructs a new {@code AbstractAdapterAuthorizationManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAuthorizationManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAuthorizationManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAuthorizationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of an authorization service which is the 
     *  basic service for checking authorizations. 
     *
     *  @return <code> true </code> if authorization is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorization() {
        return (getAdapteeManager().supportsAuthorization());
    }


    /**
     *  Tests if an authorization lookup service is supported. An 
     *  authorization lookup service defines methods to access authorizations. 
     *
     *  @return true if authorization lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationLookup() {
        return (getAdapteeManager().supportsAuthorizationLookup());
    }


    /**
     *  Tests if an authorization query service is supported. 
     *
     *  @return <code> true </code> if authorization query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (getAdapteeManager().supportsAuthorizationQuery());
    }


    /**
     *  Tests if an authorization search service is supported. 
     *
     *  @return <code> true </code> if authorization search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationSearch() {
        return (getAdapteeManager().supportsAuthorizationSearch());
    }


    /**
     *  Tests if an authorization administrative service is supported. 
     *
     *  @return <code> true </code> if authorization admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationAdmin() {
        return (getAdapteeManager().supportsAuthorizationAdmin());
    }


    /**
     *  Tests if authorization notification is supported. Messages may be sent 
     *  when authorizations are created, modified, or deleted. 
     *
     *  @return <code> true </code> if authorization notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationNotification() {
        return (getAdapteeManager().supportsAuthorizationNotification());
    }


    /**
     *  Tests if an authorization to vault lookup session is available. 
     *
     *  @return <code> true </code> if authorization vault lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationVault() {
        return (getAdapteeManager().supportsAuthorizationVault());
    }


    /**
     *  Tests if an authorization to vault assignment session is available. 
     *
     *  @return <code> true </code> if authorization vault assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationVaultAssignment() {
        return (getAdapteeManager().supportsAuthorizationVaultAssignment());
    }


    /**
     *  Tests if an authorization smart vaulting session is available. 
     *
     *  @return <code> true </code> if authorization smart vaulting is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationSmartVault() {
        return (getAdapteeManager().supportsAuthorizationSmartVault());
    }


    /**
     *  Tests if a function lookup service is supported. A function lookup 
     *  service defines methods to access authorization functions. 
     *
     *  @return <code> true </code> if function lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionLookup() {
        return (getAdapteeManager().supportsFunctionLookup());
    }


    /**
     *  Tests if a function query service is supported. 
     *
     *  @return <code> true </code> if function query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionQuery() {
        return (getAdapteeManager().supportsFunctionQuery());
    }


    /**
     *  Tests if a function search service is supported. 
     *
     *  @return <code> true </code> if function search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionSearch() {
        return (getAdapteeManager().supportsFunctionSearch());
    }


    /**
     *  Tests if a function administrative service is supported. 
     *
     *  @return <code> true </code> if function admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionAdmin() {
        return (getAdapteeManager().supportsFunctionAdmin());
    }


    /**
     *  Tests if function notification is supported. Messages may be sent when 
     *  functions are created, modified, or deleted. 
     *
     *  @return <code> true </code> if function notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionNotification() {
        return (getAdapteeManager().supportsFunctionNotification());
    }


    /**
     *  Tests if a function to vault lookup session is available. 
     *
     *  @return <code> true </code> if function vault lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionVault() {
        return (getAdapteeManager().supportsFunctionVault());
    }


    /**
     *  Tests if a function to vault assignment session is available. 
     *
     *  @return <code> true </code> if function vault assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionVaultAssignment() {
        return (getAdapteeManager().supportsFunctionVaultAssignment());
    }


    /**
     *  Tests if a function smart vaulting session is available. 
     *
     *  @return <code> true </code> if function smart vaulting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionSmartVault() {
        return (getAdapteeManager().supportsFunctionSmartVault());
    }


    /**
     *  Tests if a qualifier lookup service is supported. A function lookup 
     *  service defines methods to access authorization qualifiers. 
     *
     *  @return <code> true </code> if qualifier lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierLookup() {
        return (getAdapteeManager().supportsQualifierLookup());
    }


    /**
     *  Tests if a qualifier query service is supported. 
     *
     *  @return <code> true </code> if qualifier query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierQuery() {
        return (getAdapteeManager().supportsQualifierQuery());
    }


    /**
     *  Tests if a qualifier search service is supported. 
     *
     *  @return <code> true </code> if qualifier search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierSearch() {
        return (getAdapteeManager().supportsQualifierSearch());
    }


    /**
     *  Tests if a qualifier administrative service is supported. 
     *
     *  @return <code> true </code> if qualifier admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierAdmin() {
        return (getAdapteeManager().supportsQualifierAdmin());
    }


    /**
     *  Tests if qualifier notification is supported. Messages may be sent 
     *  when qualifiers are created, modified, or deleted. 
     *
     *  @return <code> true </code> if qualifier notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierNotification() {
        return (getAdapteeManager().supportsQualifierNotification());
    }


    /**
     *  Tests if a qualifier hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a qualifier hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchy() {
        return (getAdapteeManager().supportsQualifierHierarchy());
    }


    /**
     *  Tests if qualifier hierarchy design is supported. 
     *
     *  @return <code> true </code> if a qualifier hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchyDesign() {
        return (getAdapteeManager().supportsQualifierHierarchyDesign());
    }


    /**
     *  Tests if a qualifier to vault lookup session is available. 
     *
     *  @return <code> true </code> if qualifier vault lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierVault() {
        return (getAdapteeManager().supportsQualifierVault());
    }


    /**
     *  Tests if a qualifier to vault assignment session is available. 
     *
     *  @return <code> true </code> if qualifier vault assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierVaultAssignment() {
        return (getAdapteeManager().supportsQualifierVaultAssignment());
    }


    /**
     *  Tests if a qualifier smart vaulting session is available. 
     *
     *  @return <code> true </code> if qualifier smart vault session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierSmartVault() {
        return (getAdapteeManager().supportsQualifierSmartVault());
    }


    /**
     *  Tests if a vault lookup service is supported. A vault lookup service 
     *  defines methods to access authorization vaults. 
     *
     *  @return <code> true </code> if function lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultLookup() {
        return (getAdapteeManager().supportsVaultLookup());
    }


    /**
     *  Tests if a vault query service is supported. 
     *
     *  @return <code> true </code> if vault query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (getAdapteeManager().supportsVaultQuery());
    }


    /**
     *  Tests if a vault search service is supported. 
     *
     *  @return <code> true </code> if vault search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultSearch() {
        return (getAdapteeManager().supportsVaultSearch());
    }


    /**
     *  Tests if a vault administrative service is supported. 
     *
     *  @return <code> true </code> if vault admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultAdmin() {
        return (getAdapteeManager().supportsVaultAdmin());
    }


    /**
     *  Tests if vault notification is supported. Messages may be sent when 
     *  vaults are created, modified, or deleted. 
     *
     *  @return <code> true </code> if vault notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultNotification() {
        return (getAdapteeManager().supportsVaultNotification());
    }


    /**
     *  Tests if a vault hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a vault hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultHierarchy() {
        return (getAdapteeManager().supportsVaultHierarchy());
    }


    /**
     *  Tests if vault hierarchy design is supported. 
     *
     *  @return <code> true </code> if a function hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultHierarchyDesign() {
        return (getAdapteeManager().supportsVaultHierarchyDesign());
    }


    /**
     *  Tests if an authorization batch service is supported. 
     *
     *  @return <code> true </code> if an authorization batch service design 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizatinBatch() {
        return (getAdapteeManager().supportsAuthorizatinBatch());
    }


    /**
     *  Tests if an authorization rules service is supported. 
     *
     *  @return <code> true </code> if an authorization rules service design 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizatinRules() {
        return (getAdapteeManager().supportsAuthorizatinRules());
    }


    /**
     *  Gets the supported <code> Authorization </code> record types. 
     *
     *  @return a list containing the supported authorization record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationRecordTypes() {
        return (getAdapteeManager().getAuthorizationRecordTypes());
    }


    /**
     *  Tests if the given authorization record type is supported. 
     *
     *  @param  authorizationRecordType a <code> Type </code> indicating an 
     *          authorization record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> authorizationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationRecordType(org.osid.type.Type authorizationRecordType) {
        return (getAdapteeManager().supportsAuthorizationRecordType(authorizationRecordType));
    }


    /**
     *  Gets the supported <code> Authorization </code> search record types. 
     *
     *  @return a list containing the supported authorization search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationSearchRecordTypes() {
        return (getAdapteeManager().getAuthorizationSearchRecordTypes());
    }


    /**
     *  Tests if the given authorization search record type is supported. 
     *
     *  @param  authorizationSearchRecordType a <code> Type </code> indicating 
     *          an authorization search record type 
     *  @return <code> true </code> if the given search record Type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationSearchRecordType(org.osid.type.Type authorizationSearchRecordType) {
        return (getAdapteeManager().supportsAuthorizationSearchRecordType(authorizationSearchRecordType));
    }


    /**
     *  Gets the supported <code> Function </code> record types. 
     *
     *  @return a list containing the supported <code> Function </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFunctionRecordTypes() {
        return (getAdapteeManager().getFunctionRecordTypes());
    }


    /**
     *  Tests if the given <code> Function </code> record type is supported. 
     *
     *  @param  functionRecordType a <code> Type </code> indicating a <code> 
     *          Function </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> functionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFunctionRecordType(org.osid.type.Type functionRecordType) {
        return (getAdapteeManager().supportsFunctionRecordType(functionRecordType));
    }


    /**
     *  Gets the supported <code> Function </code> search record types. 
     *
     *  @return a list containing the supported <code> Function </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFunctionSearchRecordTypes() {
        return (getAdapteeManager().getFunctionSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Function </code> search record type is 
     *  supported. 
     *
     *  @param  functionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Function </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> functionSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFunctionSearchRecordType(org.osid.type.Type functionSearchRecordType) {
        return (getAdapteeManager().supportsFunctionSearchRecordType(functionSearchRecordType));
    }


    /**
     *  Gets the supported <code> Qualifier </code> record types. 
     *
     *  @return a list containing the supported <code> Qualifier </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQualifierRecordTypes() {
        return (getAdapteeManager().getQualifierRecordTypes());
    }


    /**
     *  Tests if the given <code> Qualifier </code> record type is supported. 
     *
     *  @param  qualifierRecordType a <code> Type </code> indicating a <code> 
     *          Qualifier </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> qualifierRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQualifierRecordType(org.osid.type.Type qualifierRecordType) {
        return (getAdapteeManager().supportsQualifierRecordType(qualifierRecordType));
    }


    /**
     *  Gets the supported <code> Qualifier </code> search record types. 
     *
     *  @return a list containing the supported <code> Qualifier </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQualifierSearchRecordTypes() {
        return (getAdapteeManager().getQualifierSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Qualifier </code> search record type is 
     *  supported. 
     *
     *  @param  qualifierSearchRecordType a <code> Type </code> indicating a 
     *          <code> Qualifier </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          qualifierSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQualifierSearchRecordType(org.osid.type.Type qualifierSearchRecordType) {
        return (getAdapteeManager().supportsQualifierSearchRecordType(qualifierSearchRecordType));
    }


    /**
     *  Gets the supported <code> Vault </code> record types. 
     *
     *  @return a list containing the supported <code> Vault </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVaultRecordTypes() {
        return (getAdapteeManager().getVaultRecordTypes());
    }


    /**
     *  Tests if the given <code> Vault </code> record type is supported. 
     *
     *  @param  vaultRecordType a <code> Type </code> indicating a <code> 
     *          Vault </code> type 
     *  @return <code> true </code> if the given vault record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> vaultRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVaultRecordType(org.osid.type.Type vaultRecordType) {
        return (getAdapteeManager().supportsVaultRecordType(vaultRecordType));
    }


    /**
     *  Gets the supported vault search record types. 
     *
     *  @return a list containing the supported <code> Vault </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVaultSearchRecordTypes() {
        return (getAdapteeManager().getVaultSearchRecordTypes());
    }


    /**
     *  Tests if the given vault search record type is supported. 
     *
     *  @param  vaultSearchRecordType a <code> Type </code> indicating a 
     *          <code> Vault </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> vaultSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVaultSearchRecordType(org.osid.type.Type vaultSearchRecordType) {
        return (getAdapteeManager().supportsVaultSearchRecordType(vaultSearchRecordType));
    }


    /**
     *  Gets the supported <code> AuthorizationCondition </code> record types. 
     *
     *  @return a list containing the supported <code> AuthorizationCondition 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationConditionRecordTypes() {
        return (getAdapteeManager().getAuthorizationConditionRecordTypes());
    }


    /**
     *  Tests if the given <code> AuthorizationCondition </code> record type 
     *  is supported. 
     *
     *  @param  authorizationConditionRecordType a <code> Type </code> 
     *          indicating an <code> AuthorizationCondition </code> record 
     *          type 
     *  @return <code> true </code> if the given authorization condition 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationConditionRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationConditionRecordType(org.osid.type.Type authorizationConditionRecordType) {
        return (getAdapteeManager().supportsAuthorizationConditionRecordType(authorizationConditionRecordType));
    }


    /**
     *  Gets an <code> AuthorizationSession </code> which is responsible for 
     *  performing authorization checks. 
     *
     *  @return an authorization session for this service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuthorization() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSession getAuthorizationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationSession());
    }


    /**
     *  Gets an <code> AuthorizationSession </code> which is responsible for 
     *  performing authorization checks for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAuthorization() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSession getAuthorizationSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  lookup service. 
     *
     *  @return an <code> AuthorizationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationLookupSession getAuthorizationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationLookupSession getAuthorizationLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationLookupSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  query service. 
     *
     *  @return an <code> AuthorizationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuerySession getAuthorizationQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuerySession getAuthorizationQuerySessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationQuerySessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  search service. 
     *
     *  @return an <code> AuthorizationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSearchSession getAuthorizationSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSearchSession getAuthorizationSearchSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationSearchSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  administration service. 
     *
     *  @return an <code> AuthorizationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationAdminSession getAuthorizationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  admin service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationAdminSession getAuthorizationAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationAdminSessionForVault(vaultId));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  authorization changes. 
     *
     *  @param  authorizationReceiver the authorization receiver 
     *  @return an <code> AuthorizationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> authorizationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationNotificationSession getAuthorizationNotificationSession(org.osid.authorization.AuthorizationReceiver authorizationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationNotificationSession(authorizationReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  notification service for the given vault. 
     *
     *  @param  authorizationReceiver the authorization receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> authorizationReceiver 
     *          </code> or <code> vaultId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationNotificationSession getAuthorizationNotificationSessionForVault(org.osid.authorization.AuthorizationReceiver authorizationReceiver, 
                                                                                                               org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationNotificationSessionForVault(authorizationReceiver, vaultId));
    }


    /**
     *  Gets the session for retrieving authorization to vault mappings. 
     *
     *  @return an <code> AuthorizationVaultSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationVaultSession getAuthorizationVaultSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationVaultSession());
    }


    /**
     *  Gets the session for assigning authorizations to vault mappings. 
     *
     *  @return a <code> AuthorizationVaultAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationVaultAssignmentSession getAuthorizationVaultAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationVaultAssignmentSession());
    }


    /**
     *  Gets the session for managing dynamic authorization vaults. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> AuthorizationSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSmartVault() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSmartVaultSession getAuthorizationSmartVaultSession(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationSmartVaultSession(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  lookup service. 
     *
     *  @return a <code> FunctionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionLookupSession getFunctionLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> a FunctionLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionLookupSession getFunctionLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionLookupSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function query 
     *  service. 
     *
     *  @return a <code> FunctionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuerySession getFunctionQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function query 
     *  service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> FunctionQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuerySession getFunctionQuerySessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionQuerySessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  search service. 
     *
     *  @return a <code> FunctionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchSession getFunctionSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> FunctionSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchSession getFunctionSearchSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionSearchSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  administration service. 
     *
     *  @return a <code> FunctionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionAdminSession getFunctionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function admin 
     *  service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> a FunctionAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionAdminSession getFunctionAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionAdminSessionForVault(vaultId));
    }


    /**
     *  Gets the notification session for notifications pertaining to function 
     *  changes. 
     *
     *  @param  functionReceiver the function receiver 
     *  @return a <code> FunctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> functionReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionNotificationSession getFunctionNotificationSession(org.osid.authorization.FunctionReceiver functionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionNotificationSession(functionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  notification service for the given vault. 
     *
     *  @param  functionReceiver the function receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> a FunctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> functionReceiver </code> 
     *          or <code> vaultId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionNotificationSession getFunctionNotificationSessionForVault(org.osid.authorization.FunctionReceiver functionReceiver, 
                                                                                                     org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionNotificationSessionForVault(functionReceiver, vaultId));
    }


    /**
     *  Gets the session for retrieving function to vault mappings. 
     *
     *  @return a <code> FunctionVaultSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionVault() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionVaultSession getFunctionVaultSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionVaultSession());
    }


    /**
     *  Gets the session for assigning function to vault mappings. 
     *
     *  @return a <code> FunctionVaultAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionVaultAssignmentSession getFunctionVaultAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionVaultAssignmentSession());
    }


    /**
     *  Gets the session associated with the function smart vault for the 
     *  given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> FunctionSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSmartVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSmartVaultSession getFunctionSmartVaultSession(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionSmartVaultSession(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  lookup service. 
     *
     *  @return a <code> QualifierLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierLookupSession getQualifierLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierLookupSession getQualifierLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierLookupSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  query service. 
     *
     *  @return a <code> QualifierQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuerySession getQualifierQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuerySession getQualifierQuerySessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierQuerySessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  search service. 
     *
     *  @return a <code> QualifierSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchSession getQualifierSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchSession getQualifierSearchSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierSearchSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  administration service. 
     *
     *  @return a <code> QualifierAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierAdminSession getQualifierAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  admin service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierAdminSession getQualifierAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierAdminSessionForVault(vaultId));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  qualifier changes. 
     *
     *  @param  qualifierReceiver the qualifier receiver 
     *  @return a <code> QualifierNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> qualifierReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierNotificationSession getQualifierNotificationSession(org.osid.authorization.QualifierReceiver qualifierReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierNotificationSession(qualifierReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  notification service for the given vault. 
     *
     *  @param  qualifierReceiver the qualifier receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> qualifierReceiver 
     *          </code> or <code> vaultId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierNotificationSession getQualifierNotificationSessionForVault(org.osid.authorization.QualifierReceiver qualifierReceiver, 
                                                                                                       org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierNotificationSessionForVault(qualifierReceiver, vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  hierarchy traversal service. The authorization service uses distinct 
     *  hierarchies that can be managed through a Hierarchy OSID. 
     *
     *  @param  qualifierHierarchyId the <code> Id </code> of a qualifier 
     *          hierarchy 
     *  @return a <code> QualifierHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> qualifierHierarchyId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierHierarchySession getQualifierHierarchySession(org.osid.id.Id qualifierHierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierHierarchySession(qualifierHierarchyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  hierarchy design service. 
     *
     *  @param  qualifierHierarchyId the <code> Id </code> of a qualifier 
     *          hierarchy 
     *  @return a <code> QualifierHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> qualifierHierarchyId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierHierarchyDesignSession getQualifierHierarchyDesignSession(org.osid.id.Id qualifierHierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierHierarchyDesignSession(qualifierHierarchyId));
    }


    /**
     *  Gets the session for retrieving qualifier to vault mappings. 
     *
     *  @return a <code> QualifierVaultSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierVaultSession getQualifierVaultSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierVaultSession());
    }


    /**
     *  Gets the session for assigning qualifier to vault mappings. 
     *
     *  @return a <code> QualifierVaultAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierVaultSession getQualifierVaultAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierVaultAssignmentSession());
    }


    /**
     *  Gets the session associated with the qualifier smart vault for the 
     *  given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSmartVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSmartVaultSession getQualifierSmartVaultSession(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierSmartVaultSession(vaultId));
    }


    /**
     *  Gets the OsidSession associated with the vault lookup service. 
     *
     *  @return a <code> VaultLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultLookupSession getVaultLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultLookupSession());
    }


    /**
     *  Gets the OsidSession associated with the vault query service. 
     *
     *  @return a <code> VaultQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuerySession getVaultQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultQuerySession());
    }


    /**
     *  Gets the OsidSession associated with the vault search service. 
     *
     *  @return a <code> VaultSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultSearchSession getVaultSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultSearchSession());
    }


    /**
     *  Gets the OsidSession associated with the vault administration service. 
     *
     *  @return a <code> VaultAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultAdmin() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultAdminSession getVaultAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultAdminSession());
    }


    /**
     *  Gets the notification session for notifications pertaining to vault 
     *  service changes. 
     *
     *  @param  vaultreceiver the vault receiver 
     *  @return a <code> VaultNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> vaultReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultNotificationSession getVaultNotificationSession(org.osid.authorization.VaultReceiver vaultreceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultNotificationSession(vaultreceiver));
    }


    /**
     *  Gets the session traversing vault hierarchies. 
     *
     *  @return a <code> VaultHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultHierarchySession getVaultHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultHierarchySession());
    }


    /**
     *  Gets the session designing vault hierarchies. 
     *
     *  @return a <code> VaultHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultHierarchyDesignSession getVaultHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultHierarchyDesignSession());
    }


    /**
     *  Gets an <code> AuthorizationBatchManager. </code> 
     *
     *  @return an <code> AuthorizationBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatch() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchManager getAuthorizationBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationBatchManager());
    }


    /**
     *  Gets an <code> AuthorizationRulesManager. </code> 
     *
     *  @return an <code> AuthorizationRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationRules() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationRulesManager getAuthorizationRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
