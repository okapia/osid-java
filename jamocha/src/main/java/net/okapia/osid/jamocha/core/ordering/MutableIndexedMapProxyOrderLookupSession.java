//
// MutableIndexedMapProxyOrderLookupSession
//
//    Implements an Order lookup service backed by a collection of
//    orders indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements an Order lookup service backed by a collection of
 *  orders. The orders are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some orders may be compatible
 *  with more types than are indicated through these order
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of orders can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyOrderLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractIndexedMapOrderLookupSession
    implements org.osid.ordering.OrderLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyOrderLookupSession} with
     *  no order.
     *
     *  @param store the store
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                       org.osid.proxy.Proxy proxy) {
        setStore(store);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyOrderLookupSession} with
     *  a single order.
     *
     *  @param store the store
     *  @param  order an order
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code order}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                       org.osid.ordering.Order order, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putOrder(order);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyOrderLookupSession} using
     *  an array of orders.
     *
     *  @param store the store
     *  @param  orders an array of orders
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code orders}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                       org.osid.ordering.Order[] orders, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putOrders(orders);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyOrderLookupSession} using
     *  a collection of orders.
     *
     *  @param store the store
     *  @param  orders a collection of orders
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code orders}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                       java.util.Collection<? extends org.osid.ordering.Order> orders,
                                                       org.osid.proxy.Proxy proxy) {
        this(store, proxy);
        putOrders(orders);
        return;
    }

    
    /**
     *  Makes an {@code Order} available in this session.
     *
     *  @param  order an order
     *  @throws org.osid.NullArgumentException {@code order{@code 
     *          is {@code null}
     */

    @Override
    public void putOrder(org.osid.ordering.Order order) {
        super.putOrder(order);
        return;
    }


    /**
     *  Makes an array of orders available in this session.
     *
     *  @param  orders an array of orders
     *  @throws org.osid.NullArgumentException {@code orders{@code 
     *          is {@code null}
     */

    @Override
    public void putOrders(org.osid.ordering.Order[] orders) {
        super.putOrders(orders);
        return;
    }


    /**
     *  Makes collection of orders available in this session.
     *
     *  @param  orders a collection of orders
     *  @throws org.osid.NullArgumentException {@code order{@code 
     *          is {@code null}
     */

    @Override
    public void putOrders(java.util.Collection<? extends org.osid.ordering.Order> orders) {
        super.putOrders(orders);
        return;
    }


    /**
     *  Removes an Order from this session.
     *
     *  @param orderId the {@code Id} of the order
     *  @throws org.osid.NullArgumentException {@code orderId{@code  is
     *          {@code null}
     */

    @Override
    public void removeOrder(org.osid.id.Id orderId) {
        super.removeOrder(orderId);
        return;
    }    
}
