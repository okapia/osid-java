//
// AbstractAssemblyAssetContentQuery.java
//
//     An AssetContentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.repository.assetcontent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssetContentQuery that stores terms.
 */

public abstract class AbstractAssemblyAssetContentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.repository.AssetContentQuery,
               org.osid.repository.AssetContentQueryInspector {

    private final java.util.Collection<org.osid.repository.records.AssetContentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.AssetContentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAssetContentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAssetContentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the accessibility types for this query. Supplying multiple types 
     *  behaves like a boolean OR among the elements. 
     *
     *  @param  accessibilityType an accessibilityType 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accessibilityType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAccessibilityType(org.osid.type.Type accessibilityType, 
                                       boolean match) {
        getAssembler().addTypeTerm(getAccessibilityTypeColumn(), accessibilityType, match);
        return;
    }


    /**
     *  Matches asset content that has any accessibility type. 
     *
     *  @param  match <code> true </code> to match content with any 
     *          accessibility type, <code> false </code> to match content with 
     *          no accessibility type 
     */

    @OSID @Override
    public void matchAnyAccessibilityType(boolean match) {
        getAssembler().addTypeWildcardTerm(getAccessibilityTypeColumn(), match);
        return;
    }


    /**
     *  Clears the accessibility terms. 
     */

    @OSID @Override
    public void clearAccessibilityTypeTerms() {
        getAssembler().clearTerms(getAccessibilityTypeColumn());
        return;
    }


    /**
     *  Gets the accesibility type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getAccessibilityTypeTerms() {
        return (getAssembler().getTypeTerms(getAccessibilityTypeColumn()));
    }


    /**
     *  Gets the AccessibilityType column name.
     *
     * @return the column name
     */

    protected String getAccessibilityTypeColumn() {
        return ("accessibility_type");
    }


    /**
     *  Matches content whose length of the data in bytes are inclusive of the 
     *  given range. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchDataLength(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getDataLengthColumn(), low, high, match);
        return;
    }


    /**
     *  Matches content that has any data length. 
     *
     *  @param  match <code> true </code> to match content with any data 
     *          length, <code> false </code> to match content with no data 
     *          length 
     */

    @OSID @Override
    public void matchAnyDataLength(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getDataLengthColumn(), match);
        return;
    }


    /**
     *  Clears the data length terms. 
     */

    @OSID @Override
    public void clearDataLengthTerms() {
        getAssembler().clearTerms(getDataLengthColumn());
        return;
    }


    /**
     *  Gets the data length query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getDataLengthTerms() {
        return (getAssembler().getCardinalRangeTerms(getDataLengthColumn()));
    }


    /**
     *  Gets the DataLength column name.
     *
     * @return the column name
     */

    protected String getDataLengthColumn() {
        return ("data_length");
    }


    /**
     *  Matches data in this content. 
     *
     *  @param  data list of matching strings 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @param  partial <code> true </code> for a partial match, <code> false 
     *          </code> for a complete match 
     *  @throws org.osid.NullArgumentException <code> data </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchData(byte[] data, boolean match, boolean partial) {
        getAssembler().addBytesTerm(getDataColumn(), data, match, partial);
        return;
    }


    /**
     *  Matches content that has any data. 
     *
     *  @param  match <code> true </code> to match content with any data, 
     *          <code> false </code> to match content with no data 
     */

    @OSID @Override
    public void matchAnyData(boolean match) {
        getAssembler().addBytesWildcardTerm(getDataColumn(), match);
        return;
    }


    /**
     *  Clears the data terms. 
     */

    @OSID @Override
    public void clearDataTerms() {
        getAssembler().clearTerms(getDataColumn());
        return;
    }


    /**
     *  Gets the data query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BytesTerm[] getDataTerms() {
        return (getAssembler().getBytesTerms(getDataColumn()));
    }


    /**
     *  Gets the Data column name.
     *
     * @return the column name
     */

    protected String getDataColumn() {
        return ("data");
    }


    /**
     *  Sets the url for this query. Supplying multiple strings behaves like a 
     *  boolean <code> OR </code> among the elements each which must 
     *  correspond to the <code> stringMatchType. </code> 
     *
     *  @param  url url string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(url) </code> is <code> false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        getAssembler().addStringTerm(getURLColumn(), url, stringMatchType, match);
        return;
    }


    /**
     *  Matches content that has any url. 
     *
     *  @param  match <code> true </code> to match content with any url, 
     *          <code> false </code> to match content with no url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        getAssembler().addStringWildcardTerm(getURLColumn(), match);
        return;
    }


    /**
     *  Clears the url terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        getAssembler().clearTerms(getURLColumn());
        return;
    }


    /**
     *  Gets the url query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getURLTerms() {
        return (getAssembler().getStringTerms(getURLColumn()));
    }


    /**
     *  Gets the URL column name.
     *
     * @return the column name
     */

    protected String getURLColumn() {
        return ("u_rl");
    }


    /**
     *  Tests if this assetContent supports the given record
     *  <code>Type</code>.
     *
     *  @param  assetContentRecordType an asset content record type 
     *  @return <code>true</code> if the assetContentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assetContentRecordType) {
        for (org.osid.repository.records.AssetContentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assetContentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  assetContentRecordType the asset content record type 
     *  @return the asset content query record 
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetContentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetContentQueryRecord getAssetContentQueryRecord(org.osid.type.Type assetContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetContentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assetContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetContentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  assetContentRecordType the asset content record type 
     *  @return the asset content query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetContentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetContentQueryInspectorRecord getAssetContentQueryInspectorRecord(org.osid.type.Type assetContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetContentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(assetContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetContentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this asset content. 
     *
     *  This method registers the quer and, query inspector
     *  records. Additional types may be registered with this object
     *  using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assetContentQueryRecord the asset content query record
     *  @param assetContentQueryInspectorRecord the asset content query inspector
     *         record
     *  @param assetContentRecordType asset content record type
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentQueryRecord</code>,
     *          <code>assetContentQueryInspectorRecord</code>, or
     *          <code>assetContentRecordTypeassetContent</code> is
     *          <code>null</code>
     */
            
    protected void addAssetContentRecords(org.osid.repository.records.AssetContentQueryRecord assetContentQueryRecord, 
                                      org.osid.repository.records.AssetContentQueryInspectorRecord assetContentQueryInspectorRecord, 
                                      org.osid.type.Type assetContentRecordType) {

        addRecordType(assetContentRecordType);

        nullarg(assetContentQueryRecord, "asset content query record");
        nullarg(assetContentQueryInspectorRecord, "asset content query inspector record");

        this.queryRecords.add(assetContentQueryRecord);
        this.queryInspectorRecords.add(assetContentQueryInspectorRecord);
        
        return;
    }
}
