//
// AbstractAwardSearch.java
//
//     A template for making an Award Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.award.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing award searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAwardSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.recognition.AwardSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.recognition.records.AwardSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.recognition.AwardSearchOrder awardSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of awards. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  awardIds list of awards
     *  @throws org.osid.NullArgumentException
     *          <code>awardIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAwards(org.osid.id.IdList awardIds) {
        while (awardIds.hasNext()) {
            try {
                this.ids.add(awardIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAwards</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of award Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAwardIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  awardSearchOrder award search order 
     *  @throws org.osid.NullArgumentException
     *          <code>awardSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>awardSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAwardResults(org.osid.recognition.AwardSearchOrder awardSearchOrder) {
	this.awardSearchOrder = awardSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.recognition.AwardSearchOrder getAwardSearchOrder() {
	return (this.awardSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given award search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an award implementing the requested record.
     *
     *  @param awardSearchRecordType an award search record
     *         type
     *  @return the award search record
     *  @throws org.osid.NullArgumentException
     *          <code>awardSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AwardSearchRecord getAwardSearchRecord(org.osid.type.Type awardSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.recognition.records.AwardSearchRecord record : this.records) {
            if (record.implementsRecordType(awardSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this award search. 
     *
     *  @param awardSearchRecord award search record
     *  @param awardSearchRecordType award search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAwardSearchRecord(org.osid.recognition.records.AwardSearchRecord awardSearchRecord, 
                                           org.osid.type.Type awardSearchRecordType) {

        addRecordType(awardSearchRecordType);
        this.records.add(awardSearchRecord);        
        return;
    }
}
