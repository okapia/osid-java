//
// AbstractFederatingConferralLookupSession.java
//
//     An abstract federating adapter for a ConferralLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ConferralLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingConferralLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.recognition.ConferralLookupSession>
    implements org.osid.recognition.ConferralLookupSession {

    private boolean parallel = false;
    private org.osid.recognition.Academy academy = new net.okapia.osid.jamocha.nil.recognition.academy.UnknownAcademy();


    /**
     *  Constructs a new <code>AbstractFederatingConferralLookupSession</code>.
     */

    protected AbstractFederatingConferralLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.recognition.ConferralLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Academy/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.academy.getId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this 
     *  session.
     *
     *  @return the <code>Academy</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.academy);
    }


    /**
     *  Sets the <code>Academy</code>.
     *
     *  @param  academy the academy for this session
     *  @throws org.osid.NullArgumentException <code>academy</code>
     *          is <code>null</code>
     */

    protected void setAcademy(org.osid.recognition.Academy academy) {
        nullarg(academy, "academy");
        this.academy = academy;
        return;
    }


    /**
     *  Tests if this user can perform <code>Conferral</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupConferrals() {
        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            if (session.canLookupConferrals()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Conferral</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeConferralView() {
        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            session.useComparativeConferralView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Conferral</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryConferralView() {
        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            session.usePlenaryConferralView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include conferrals in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            session.useFederatedAcademyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            session.useIsolatedAcademyView();
        }

        return;
    }


    /**
     *  Only conferrals whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveConferralView() {
        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            session.useEffectiveConferralView();
        }

        return;
    }


    /**
     *  All conferrals of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveConferralView() {
        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            session.useAnyEffectiveConferralView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Conferral</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Conferral</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Conferral</code> and
     *  retained for compatibility.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralId <code>Id</code> of the
     *          <code>Conferral</code>
     *  @return the conferral
     *  @throws org.osid.NotFoundException <code>conferralId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>conferralId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Conferral getConferral(org.osid.id.Id conferralId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            try {
                return (session.getConferral(conferralId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(conferralId + " not found");
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  conferrals specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Conferrals</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>conferralIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByIds(org.osid.id.IdList conferralIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.recognition.conferral.MutableConferralList ret = new net.okapia.osid.jamocha.recognition.conferral.MutableConferralList();

        try (org.osid.id.IdList ids = conferralIds) {
            while (ids.hasNext()) {
                ret.addConferral(getConferral(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  conferral genus <code>Type</code> which does not include
     *  conferrals of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralGenusType a conferral genus type 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsByGenusType(conferralGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  conferral genus <code>Type</code> and include any additional
     *  conferrals with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralGenusType a conferral genus type 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByParentGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsByParentGenusType(conferralGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ConferralList</code> containing the given
     *  conferral record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralRecordType a conferral record type 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByRecordType(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsByRecordType(conferralRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ConferralList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *  
     *  In active mode, conferrals are returned that are currently
     *  active. In any status mode, active and inactive conferrals
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Conferral</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsOnDate(org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of conferrals corresponding to a recipient
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsForRecipient(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsForRecipient(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to a recipient
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsForRecipientOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to an award
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsForAward(org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsForAward(awardId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to an award
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForAwardOnDate(org.osid.id.Id awardId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsForAwardOnDate(awardId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAward(org.osid.id.Id resourceId,
                                                                                org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsForRecipientAndAward(resourceId, awardId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAwardOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.id.Id awardId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsForRecipientAndAwardOnDate(resourceId, awardId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to a reference
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the reference
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsByReference(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsByReference(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to a reference
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByReferenceOnDate(org.osid.id.Id resourceId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsByReferenceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to an convocation
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  convocationId the <code>Id</code> of the convocation
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>convocationId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsByConvocation(org.osid.id.Id convocationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsByConvocation(convocationId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of conferrals corresponding to an convocation
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  convocationId the <code>Id</code> of the convocation
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>convocationId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByConvocationOnDate(org.osid.id.Id convocationId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferralsByConvocationOnDate(convocationId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Conferrals</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Conferrals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferrals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList ret = getConferralList();

        for (org.osid.recognition.ConferralLookupSession session : getSessions()) {
            ret.addConferralList(session.getConferrals());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.recognition.conferral.FederatingConferralList getConferralList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.conferral.ParallelConferralList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.conferral.CompositeConferralList());
        }
    }
}
