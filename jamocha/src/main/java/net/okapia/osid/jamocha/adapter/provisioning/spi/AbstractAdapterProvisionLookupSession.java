//
// AbstractAdapterProvisionLookupSession.java
//
//    A Provision lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Provision lookup session adapter.
 */

public abstract class AbstractAdapterProvisionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.ProvisionLookupSession {

    private final org.osid.provisioning.ProvisionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProvisionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProvisionLookupSession(org.osid.provisioning.ProvisionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code Provision} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProvisions() {
        return (this.session.canLookupProvisions());
    }


    /**
     *  A complete view of the {@code Provision} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProvisionView() {
        this.session.useComparativeProvisionView();
        return;
    }


    /**
     *  A complete view of the {@code Provision} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProvisionView() {
        this.session.usePlenaryProvisionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisions in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only provisions whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProvisionView() {
        this.session.useEffectiveProvisionView();
        return;
    }
    

    /**
     *  All provisions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProvisionView() {
        this.session.useAnyEffectiveProvisionView();
        return;
    }

     
    /**
     *  Gets the {@code Provision} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Provision} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Provision} and
     *  retained for compatibility.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param provisionId {@code Id} of the {@code Provision}
     *  @return the provision
     *  @throws org.osid.NotFoundException {@code provisionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code provisionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getProvision(org.osid.id.Id provisionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvision(provisionId));
    }


    /**
     *  Gets a {@code ProvisionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  provisions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Provisions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Provision} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code provisionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByIds(org.osid.id.IdList provisionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsByIds(provisionIds));
    }


    /**
     *  Gets a {@code ProvisionList} corresponding to the given
     *  provision genus {@code Type} which does not include
     *  provisions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned {@code Provision} list
     *  @throws org.osid.NullArgumentException
     *          {@code provisionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsByGenusType(provisionGenusType));
    }


    /**
     *  Gets a {@code ProvisionList} corresponding to the given
     *  provision genus {@code Type} and include any additional
     *  provisions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned {@code Provision} list
     *  @throws org.osid.NullArgumentException
     *          {@code provisionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByParentGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsByParentGenusType(provisionGenusType));
    }


    /**
     *  Gets a {@code ProvisionList} containing the given
     *  provision record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return the returned {@code Provision} list
     *  @throws org.osid.NullArgumentException
     *          {@code provisionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByRecordType(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsByRecordType(provisionRecordType));
    }


    /**
     *  Gets a {@code ProvisionList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *  
     *  In active mode, provisions are returned that are currently
     *  active. In any status mode, active and inactive provisions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Provision} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsOnDate(from, to));
    }
        

    /**
     *  Gets a list of provisions for a supplied broker. {@code} 
     *  
     *  {@code} In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *  
     *  In effective mode, provisions are returned that are currently
     *  effective. In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  brokerId a broker {@code Id} 
     *  @return the returned {@code Provision} list 
     *  @throws org.osid.NullArgumentException {@code brokerId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForBroker(brokerId));
    }


    /**
     *  Gets a list of provisions for a supplied broker. and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *  
     *  In effective mode, provisions are returned that are currently 
     *  effective in addition to being effective in the guven date range. In 
     *  any effective mode, effective provisions and those currently expired 
     *  are returned. 
     *
     *  @param  brokerId a broker {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Provision} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code brokerId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForBrokerOnDate(org.osid.id.Id brokerId, 
                                                                            org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForBrokerOnDate(brokerId, from, to));
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionableId the {@code Id} of the provisionable
     *  @return the returned {@code ProvisionList}
     *  @throws org.osid.NullArgumentException {@code provisionableId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForProvisionable(provisionableId));
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the {@code Id} of the provisionable
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProvisionList}
     *  @throws org.osid.NullArgumentException {@code provisionableId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableOnDate(org.osid.id.Id provisionableId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForProvisionableOnDate(provisionableId, from, to));
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the recipient
     *  @return the returned {@code ProvisionList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForRecipient(resourceId));
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProvisionList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForRecipientOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and recipient
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the {@code Id} of the provisionable
     *  @param  resourceId the {@code Id} of the recipient
     *  @return the returned {@code ProvisionList}
     *  @throws org.osid.NullArgumentException {@code provisionableId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipient(org.osid.id.Id provisionableId,
                                                                        org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForProvisionableAndRecipient(provisionableId, resourceId));
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and recipient
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective. In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProvisionList}
     *  @throws org.osid.NullArgumentException {@code provisionableId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipientOnDate(org.osid.id.Id provisionableId,
                                                                              org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForProvisionableAndRecipientOnDate(provisionableId, resourceId, from, to));
    }


    /**
     *  Gets a list of provisions for a request.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *  
     *  In effective mode, provisions are returned that are currently
     *  effective. In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  requestId a request {@code Id} 
     *  @return the returned {@code Provision} list 
     *  @throws org.osid.NullArgumentException {@code requestId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForRequest(org.osid.id.Id requestId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisionsForRequest(requestId));
    }


    /**
     *  Gets all {@code Provisions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Provisions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProvisions());
    }
}
