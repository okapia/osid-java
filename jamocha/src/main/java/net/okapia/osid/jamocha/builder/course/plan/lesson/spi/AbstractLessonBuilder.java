//
// AbstractLesson.java
//
//     Defines a Lesson builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.lesson.spi;


/**
 *  Defines a <code>Lesson</code> builder.
 */

public abstract class AbstractLessonBuilder<T extends AbstractLessonBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.plan.lesson.LessonMiter lesson;


    /**
     *  Constructs a new <code>AbstractLessonBuilder</code>.
     *
     *  @param lesson the lesson to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractLessonBuilder(net.okapia.osid.jamocha.builder.course.plan.lesson.LessonMiter lesson) {
        super(lesson);
        this.lesson = lesson;
        return;
    }


    /**
     *  Builds the lesson.
     *
     *  @return the new lesson
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.plan.Lesson build() {
        (new net.okapia.osid.jamocha.builder.validator.course.plan.lesson.LessonValidator(getValidations())).validate(this.lesson);
        return (new net.okapia.osid.jamocha.builder.course.plan.lesson.ImmutableLesson(this.lesson));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the lesson miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.plan.lesson.LessonMiter getMiter() {
        return (this.lesson);
    }


    /**
     *  Sets the plan.
     *
     *  @param plan a plan
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>plan</code> is
     *          <code>null</code>
     */

    public T plan(org.osid.course.plan.Plan plan) {
        getMiter().setPlan(plan);
        return (self());
    }


    /**
     *  Sets the docet.
     *
     *  @param docet a docet
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>docet</code> is
     *          <code>null</code>
     */

    public T docet(org.osid.course.syllabus.Docet docet) {
        getMiter().setDocet(docet);
        return (self());
    }


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T activity(org.osid.course.Activity activity) {
        getMiter().addActivity(activity);
        return (self());
    }


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    public T activities(java.util.Collection<org.osid.course.Activity> activities) {
        getMiter().setActivities(activities);
        return (self());
    }


    /**
     *  Sets the planned start time.
     *
     *  @param time a planned start time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T plannedStartTime(org.osid.calendaring.Duration time) {
        getMiter().setPlannedStartTime(time);
        return (self());
    }


    /**
     *  Sets the actual start time.
     *
     *  @param time an actual start time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T actualStartTime(org.osid.calendaring.Duration time) {
        getMiter().setActualStartTime(time);
        return (self());
    }


    /**
     *  Sets the actual starting activity.
     *
     *  @param activity an actual starting activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T actualStartingActivity(org.osid.course.Activity activity) {
        getMiter().setActualStartingActivity(activity);
        return (self());
    }


    /**
     *  Marks this lesson as complete.
     *
     *  @return the builder
     */

    public T complete() {
        getMiter().setComplete(true);
        return(self());
    }


    /**
     *  Marks this lesson as incomplete.
     *
     *  @return the builder
     */

    public T incomplete() {
        getMiter().setComplete(false);
        return(self());
    }


    /**
     *  Marks this lesson as skipped.
     *
     *  @return the builder
     */

    public T skipped() {
        getMiter().setSkipped(true);
        return(self());
    }


    /**
     *  Marks this lesson as not skipped.
     *
     *  @return the builder
     */

    public T notSskipped() {
        getMiter().setSkipped(false);
        return(self());
    }


    /**
     *  Sets the actual end time.
     *
     *  @param time an actual end time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T actualEndTime(org.osid.calendaring.Duration time) {
        getMiter().setActualEndTime(time);
        return (self());
    }


    /**
     *  Sets the actual ending activity.
     *
     *  @param activity an actual ending activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T actualEndingActivity(org.osid.course.Activity activity) {
        getMiter().setActualEndingActivity(activity);
        return (self());
    }


    /**
     *  Sets the actual time spent.
     *
     *  @param timeSpent an actual time spent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>timeSpent</code>
     *          is <code>null</code>
     */

    public T actualTimeSpent(org.osid.calendaring.Duration timeSpent) {
        getMiter().setActualTimeSpent(timeSpent);
        return (self());
    }


    /**
     *  Adds a Lesson record.
     *
     *  @param record a lesson record
     *  @param recordType the type of lesson record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.plan.records.LessonRecord record, org.osid.type.Type recordType) {
        getMiter().addLessonRecord(record, recordType);
        return (self());
    }
}       


