//
// AbstractMapActionEnablerLookupSession
//
//    A simple framework for providing an ActionEnabler lookup service
//    backed by a fixed collection of action enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an ActionEnabler lookup service backed by a
 *  fixed collection of action enablers. The action enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActionEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapActionEnablerLookupSession
    extends net.okapia.osid.jamocha.control.rules.spi.AbstractActionEnablerLookupSession
    implements org.osid.control.rules.ActionEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.rules.ActionEnabler> actionEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.rules.ActionEnabler>());


    /**
     *  Makes an <code>ActionEnabler</code> available in this session.
     *
     *  @param  actionEnabler an action enabler
     *  @throws org.osid.NullArgumentException <code>actionEnabler<code>
     *          is <code>null</code>
     */

    protected void putActionEnabler(org.osid.control.rules.ActionEnabler actionEnabler) {
        this.actionEnablers.put(actionEnabler.getId(), actionEnabler);
        return;
    }


    /**
     *  Makes an array of action enablers available in this session.
     *
     *  @param  actionEnablers an array of action enablers
     *  @throws org.osid.NullArgumentException <code>actionEnablers<code>
     *          is <code>null</code>
     */

    protected void putActionEnablers(org.osid.control.rules.ActionEnabler[] actionEnablers) {
        putActionEnablers(java.util.Arrays.asList(actionEnablers));
        return;
    }


    /**
     *  Makes a collection of action enablers available in this session.
     *
     *  @param  actionEnablers a collection of action enablers
     *  @throws org.osid.NullArgumentException <code>actionEnablers<code>
     *          is <code>null</code>
     */

    protected void putActionEnablers(java.util.Collection<? extends org.osid.control.rules.ActionEnabler> actionEnablers) {
        for (org.osid.control.rules.ActionEnabler actionEnabler : actionEnablers) {
            this.actionEnablers.put(actionEnabler.getId(), actionEnabler);
        }

        return;
    }


    /**
     *  Removes an ActionEnabler from this session.
     *
     *  @param  actionEnablerId the <code>Id</code> of the action enabler
     *  @throws org.osid.NullArgumentException <code>actionEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeActionEnabler(org.osid.id.Id actionEnablerId) {
        this.actionEnablers.remove(actionEnablerId);
        return;
    }


    /**
     *  Gets the <code>ActionEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  actionEnablerId <code>Id</code> of the <code>ActionEnabler</code>
     *  @return the actionEnabler
     *  @throws org.osid.NotFoundException <code>actionEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>actionEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnabler getActionEnabler(org.osid.id.Id actionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.rules.ActionEnabler actionEnabler = this.actionEnablers.get(actionEnablerId);
        if (actionEnabler == null) {
            throw new org.osid.NotFoundException("actionEnabler not found: " + actionEnablerId);
        }

        return (actionEnabler);
    }


    /**
     *  Gets all <code>ActionEnablers</code>. In plenary mode, the returned
     *  list contains all known actionEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  actionEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ActionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.actionenabler.ArrayActionEnablerList(this.actionEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.actionEnablers.clear();
        super.close();
        return;
    }
}
