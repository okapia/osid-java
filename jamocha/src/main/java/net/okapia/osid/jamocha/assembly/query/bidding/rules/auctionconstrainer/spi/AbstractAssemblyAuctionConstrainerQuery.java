//
// AbstractAssemblyAuctionConstrainerQuery.java
//
//     An AuctionConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.bidding.rules.auctionconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuctionConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyAuctionConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.bidding.rules.AuctionConstrainerQuery,
               org.osid.bidding.rules.AuctionConstrainerQueryInspector,
               org.osid.bidding.rules.AuctionConstrainerSearchOrder {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAuctionConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuctionConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to the auction. 
     *
     *  @param  auctionId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuctionId(org.osid.id.Id auctionId, boolean match) {
        getAssembler().addIdTerm(getRuledAuctionIdColumn(), auctionId, match);
        return;
    }


    /**
     *  Clears the auction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionIdTerms() {
        getAssembler().clearTerms(getRuledAuctionIdColumn());
        return;
    }


    /**
     *  Gets the auction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuctionIdTerms() {
        return (getAssembler().getIdTerms(getRuledAuctionIdColumn()));
    }


    /**
     *  Gets the RuledAuctionId column name.
     *
     * @return the column name
     */

    protected String getRuledAuctionIdColumn() {
        return ("ruled_auction_id");
    }


    /**
     *  Tests if an <code> AuctionQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the auction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuctionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuery getRuledAuctionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuctionQuery() is false");
    }


    /**
     *  Matches mapped to any auction. 
     *
     *  @param  match <code> true </code> for mapped to any auction, <code> 
     *          false </code> to match mapped to no auction 
     */

    @OSID @Override
    public void matchAnyRuledAuction(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledAuctionColumn(), match);
        return;
    }


    /**
     *  Clears the auction query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionTerms() {
        getAssembler().clearTerms(getRuledAuctionColumn());
        return;
    }


    /**
     *  Gets the auction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQueryInspector[] getRuledAuctionTerms() {
        return (new org.osid.bidding.AuctionQueryInspector[0]);
    }


    /**
     *  Gets the RuledAuction column name.
     *
     * @return the column name
     */

    protected String getRuledAuctionColumn() {
        return ("ruled_auction");
    }


    /**
     *  Matches mapped to the auction house. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAuctionHouseIdColumn(), auctionHouseId, match);
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        getAssembler().clearTerms(getAuctionHouseIdColumn());
        return;
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (getAssembler().getIdTerms(getAuctionHouseIdColumn()));
    }


    /**
     *  Gets the AuctionHouseId column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseIdColumn() {
        return ("auction_house_id");
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        getAssembler().clearTerms(getAuctionHouseColumn());
        return;
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the AuctionHouse column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseColumn() {
        return ("auction_house");
    }


    /**
     *  Tests if this auctionConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  auctionConstrainerRecordType an auction constrainer record type 
     *  @return <code>true</code> if the auctionConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auctionConstrainerRecordType) {
        for (org.osid.bidding.rules.records.AuctionConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  auctionConstrainerRecordType the auction constrainer record type 
     *  @return the auction constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerQueryRecord getAuctionConstrainerQueryRecord(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  auctionConstrainerRecordType the auction constrainer record type 
     *  @return the auction constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord getAuctionConstrainerQueryInspectorRecord(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(auctionConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param auctionConstrainerRecordType the auction constrainer record type
     *  @return the auction constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerSearchOrderRecord getAuctionConstrainerSearchOrderRecord(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(auctionConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this auction constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auctionConstrainerQueryRecord the auction constrainer query record
     *  @param auctionConstrainerQueryInspectorRecord the auction constrainer query inspector
     *         record
     *  @param auctionConstrainerSearchOrderRecord the auction constrainer search order record
     *  @param auctionConstrainerRecordType auction constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerQueryRecord</code>,
     *          <code>auctionConstrainerQueryInspectorRecord</code>,
     *          <code>auctionConstrainerSearchOrderRecord</code> or
     *          <code>auctionConstrainerRecordTypeauctionConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addAuctionConstrainerRecords(org.osid.bidding.rules.records.AuctionConstrainerQueryRecord auctionConstrainerQueryRecord, 
                                      org.osid.bidding.rules.records.AuctionConstrainerQueryInspectorRecord auctionConstrainerQueryInspectorRecord, 
                                      org.osid.bidding.rules.records.AuctionConstrainerSearchOrderRecord auctionConstrainerSearchOrderRecord, 
                                      org.osid.type.Type auctionConstrainerRecordType) {

        addRecordType(auctionConstrainerRecordType);

        nullarg(auctionConstrainerQueryRecord, "auction constrainer query record");
        nullarg(auctionConstrainerQueryInspectorRecord, "auction constrainer query inspector record");
        nullarg(auctionConstrainerSearchOrderRecord, "auction constrainer search odrer record");

        this.queryRecords.add(auctionConstrainerQueryRecord);
        this.queryInspectorRecords.add(auctionConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(auctionConstrainerSearchOrderRecord);
        
        return;
    }
}
