//
// AbstractBillingBatchManager.java
//
//     An adapter for a BillingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a BillingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterBillingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.billing.batch.BillingBatchManager>
    implements org.osid.billing.batch.BillingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterBillingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterBillingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterBillingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterBillingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of customers is available. 
     *
     *  @return <code> true </code> if a customer bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerBatchAdmin() {
        return (getAdapteeManager().supportsCustomerBatchAdmin());
    }


    /**
     *  Tests if bulk administration of items is available. 
     *
     *  @return <code> true </code> if an item bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBatchAdmin() {
        return (getAdapteeManager().supportsItemBatchAdmin());
    }


    /**
     *  Tests if bulk administration of entries is available. 
     *
     *  @return <code> true </code> if an entry bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBatchAdmin() {
        return (getAdapteeManager().supportsEntryBatchAdmin());
    }


    /**
     *  Tests if bulk administration of categories is available. 
     *
     *  @return <code> true </code> if a category bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryBatchAdmin() {
        return (getAdapteeManager().supportsCategoryBatchAdmin());
    }


    /**
     *  Tests if bulk administration of periods is available. 
     *
     *  @return <code> true </code> if a period bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodBatchAdmin() {
        return (getAdapteeManager().supportsPeriodBatchAdmin());
    }


    /**
     *  Tests if bulk administration of businesses is available. 
     *
     *  @return <code> true </code> if a business bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessBatchAdmin() {
        return (getAdapteeManager().supportsBusinessBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk customer 
     *  administration service. 
     *
     *  @return a <code> CustomerBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.CustomerBatchAdminSession getCustomerBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk customer 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CustomerBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.CustomerBatchAdminSession getCustomerBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCustomerBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service. 
     *
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.ItemBatchAdminSession getItemBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service for the given business 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.ItemBatchAdminSession getItemBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service. 
     *
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.EntryBatchAdminSession getEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.EntryBatchAdminSession getEntryBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk category 
     *  administration service. 
     *
     *  @return a <code> CategoryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.CategoryBatchAdminSession getCategoryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk category 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> CategoryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategoryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.CategoryBatchAdminSession getCategoryBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCategoryBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk period 
     *  administration service. 
     *
     *  @return a <code> PeriodBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.PeriodBatchAdminSession getPeriodBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk period 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PeriodBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.PeriodBatchAdminSession getPeriodBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPeriodBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk business 
     *  administration service. 
     *
     *  @return a <code> BusinessBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBusinessBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.batch.BusinessBatchAdminSession getBusinessBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBusinessBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
