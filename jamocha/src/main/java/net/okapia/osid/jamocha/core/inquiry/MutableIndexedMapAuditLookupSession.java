//
// MutableIndexedMapAuditLookupSession
//
//    Implements an Audit lookup service backed by a collection of
//    audits indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements an Audit lookup service backed by a collection of
 *  audits. The audits are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some audits may be compatible
 *  with more types than are indicated through these audit
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of audits can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAuditLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractIndexedMapAuditLookupSession
    implements org.osid.inquiry.AuditLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAuditLookupSession} with no audits.
     *
     *  @param inquest the inquest
     *  @throws org.osid.NullArgumentException {@code inquest}
     *          is {@code null}
     */

      public MutableIndexedMapAuditLookupSession(org.osid.inquiry.Inquest inquest) {
        setInquest(inquest);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuditLookupSession} with a
     *  single audit.
     *  
     *  @param inquest the inquest
     *  @param  audit an single audit
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code audit} is {@code null}
     */

    public MutableIndexedMapAuditLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.Audit audit) {
        this(inquest);
        putAudit(audit);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuditLookupSession} using an
     *  array of audits.
     *
     *  @param inquest the inquest
     *  @param  audits an array of audits
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code audits} is {@code null}
     */

    public MutableIndexedMapAuditLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.Audit[] audits) {
        this(inquest);
        putAudits(audits);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuditLookupSession} using a
     *  collection of audits.
     *
     *  @param inquest the inquest
     *  @param  audits a collection of audits
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code audits} is {@code null}
     */

    public MutableIndexedMapAuditLookupSession(org.osid.inquiry.Inquest inquest,
                                                  java.util.Collection<? extends org.osid.inquiry.Audit> audits) {

        this(inquest);
        putAudits(audits);
        return;
    }
    

    /**
     *  Makes an {@code Audit} available in this session.
     *
     *  @param  audit an audit
     *  @throws org.osid.NullArgumentException {@code audit{@code  is
     *          {@code null}
     */

    @Override
    public void putAudit(org.osid.inquiry.Audit audit) {
        super.putAudit(audit);
        return;
    }


    /**
     *  Makes an array of audits available in this session.
     *
     *  @param  audits an array of audits
     *  @throws org.osid.NullArgumentException {@code audits{@code 
     *          is {@code null}
     */

    @Override
    public void putAudits(org.osid.inquiry.Audit[] audits) {
        super.putAudits(audits);
        return;
    }


    /**
     *  Makes collection of audits available in this session.
     *
     *  @param  audits a collection of audits
     *  @throws org.osid.NullArgumentException {@code audit{@code  is
     *          {@code null}
     */

    @Override
    public void putAudits(java.util.Collection<? extends org.osid.inquiry.Audit> audits) {
        super.putAudits(audits);
        return;
    }


    /**
     *  Removes an Audit from this session.
     *
     *  @param auditId the {@code Id} of the audit
     *  @throws org.osid.NullArgumentException {@code auditId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAudit(org.osid.id.Id auditId) {
        super.removeAudit(auditId);
        return;
    }    
}
