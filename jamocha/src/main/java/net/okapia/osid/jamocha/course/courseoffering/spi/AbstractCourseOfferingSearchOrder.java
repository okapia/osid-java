//
// AbstractCourseOfferingSearchOdrer.java
//
//     Defines a CourseOfferingSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CourseOfferingSearchOrder}.
 */

public abstract class AbstractCourseOfferingSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.CourseOfferingSearchOrder {

    private final java.util.Collection<org.osid.course.records.CourseOfferingSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by course. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by term. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerm(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a term order is available. 
     *
     *  @return <code> true </code> if a term order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearchOrder() {
        return (false);
    }


    /**
     *  Gets the term order. 
     *
     *  @return the term search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchOrder getTermSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTermSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by course offering 
     *  title. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by course offering 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by course offerings 
     *  requiring registration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiresRegistration(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumSeats(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumSeats(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by url. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByURL(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by schedule info. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleInfo(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEvent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an event search order is available. 
     *
     *  @return <code> true </code> if an event order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the event order. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEventSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  courseOfferingRecordType a course offering record type 
     *  @return {@code true} if the courseOfferingRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code courseOfferingRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseOfferingRecordType) {
        for (org.osid.course.records.CourseOfferingSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  courseOfferingRecordType the course offering record type 
     *  @return the course offering search order record
     *  @throws org.osid.NullArgumentException
     *          {@code courseOfferingRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(courseOfferingRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingSearchOrderRecord getCourseOfferingSearchOrderRecord(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseOfferingSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseOfferingRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this course offering. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseOfferingRecord the course offering search odrer record
     *  @param courseOfferingRecordType course offering record type
     *  @throws org.osid.NullArgumentException
     *          {@code courseOfferingRecord} or
     *          {@code courseOfferingRecordTypecourseOffering} is
     *          {@code null}
     */
            
    protected void addCourseOfferingRecord(org.osid.course.records.CourseOfferingSearchOrderRecord courseOfferingSearchOrderRecord, 
                                     org.osid.type.Type courseOfferingRecordType) {

        addRecordType(courseOfferingRecordType);
        this.records.add(courseOfferingSearchOrderRecord);
        
        return;
    }
}
