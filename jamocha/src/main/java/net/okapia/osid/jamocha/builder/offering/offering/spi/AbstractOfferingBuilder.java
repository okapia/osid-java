//
// AbstractOffering.java
//
//     Defines an Offering builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.offering.spi;


/**
 *  Defines an <code>Offering</code> builder.
 */

public abstract class AbstractOfferingBuilder<T extends AbstractOfferingBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.offering.offering.OfferingMiter offering;


    /**
     *  Constructs a new <code>AbstractOfferingBuilder</code>.
     *
     *  @param offering the offering to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractOfferingBuilder(net.okapia.osid.jamocha.builder.offering.offering.OfferingMiter offering) {
        super(offering);
        this.offering = offering;
        return;
    }


    /**
     *  Builds the offering.
     *
     *  @return the new offering
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>offering</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.offering.Offering build() {
        (new net.okapia.osid.jamocha.builder.validator.offering.offering.OfferingValidator(getValidations())).validate(this.offering);
        return (new net.okapia.osid.jamocha.builder.offering.offering.ImmutableOffering(this.offering));
    }


    /**
     *  Gets the offering. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new offering
     */

    @Override
    public net.okapia.osid.jamocha.builder.offering.offering.OfferingMiter getMiter() {
        return (this.offering);
    }


    /**
     *  Sets the canonical unit.
     *
     *  @param canonicalUnit a canonical unit
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnit</code> is <code>null</code>
     */

    public T canonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit) {
        getMiter().setCanonicalUnit(canonicalUnit);
        return (self());
    }


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriod</code> is <code>null</code>
     */

    public T timePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        getMiter().setTimePeriod(timePeriod);
        return (self());
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    public T code(String code) {
        getMiter().setCode(code);
        return (self());
    }


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    public T resultOption(org.osid.grading.GradeSystem resultOption) {
        getMiter().addResultOption(resultOption);
        return (self());
    }


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    public T resultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions) {
        getMiter().setResultOptions(resultOptions);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>schedule</code> is <code>null</code>
     */

    public T schedule(org.osid.calendaring.Schedule schedule) {
        getMiter().addSchedule(schedule);
        return (self());
    }


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>schedules</code> is <code>null</code>
     */

    public T schedules(java.util.Collection<org.osid.calendaring.Schedule> schedules) {
        getMiter().setSchedules(schedules);
        return (self());
    }


    /**
     *  Adds an Offering record.
     *
     *  @param record an offering record
     *  @param recordType the type of offering record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.offering.records.OfferingRecord record, org.osid.type.Type recordType) {
        getMiter().addOfferingRecord(record, recordType);
        return (self());
    }
}       


