//
// AbstractAuditEnablerNotificationSession.java
//
//     A template for making AuditEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code AuditEnabler} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code AuditEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for audit enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractAuditEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inquiry.rules.AuditEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();


    /**
     *  Gets the {@code Inquest} {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Inquest Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.inquest.getId());
    }

    
    /**
     *  Gets the {@code Inquest} associated with this session.
     *
     *  @return the {@code Inquest} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.inquest);
    }


    /**
     *  Sets the {@code Inquest}.
     *
     *  @param inquest the inquest for this session
     *  @throws org.osid.NullArgumentException {@code inquest}
     *          is {@code null}
     */

    protected void setInquest(org.osid.inquiry.Inquest inquest) {
        nullarg(inquest, "inquest");
        this.inquest = inquest;
        return;
    }


    /**
     *  Tests if this user can register for {@code AuditEnabler}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForAuditEnablerNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for audit enablers in inquests
     *  which are children of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeAuditEnablerNotification() </code>.
     */

    @OSID @Override
    public void reliableAuditEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableAuditEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge an audit enabler notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeAuditEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }
    

    /**
     *  Register for notifications of new audit enablers. {@code
     *  AuditEnablerReceiver.newAuditEnabler()} is invoked when an new
     *  {@code AuditEnabler} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewAuditEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated audit enablers. {@code
     *  AuditEnablerReceiver.changedAuditEnabler()} is invoked when an
     *  audit enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAuditEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated audit enabler. {@code
     *  AuditEnablerReceiver.changedAuditEnabler()} is invoked when
     *  the specified audit enabler is changed.
     *
     *  @param auditEnablerId the {@code Id} of the {@code AuditEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code auditEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedAuditEnabler(org.osid.id.Id auditEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted audit enablers. {@code
     *  AuditEnablerReceiver.deletedAuditEnabler()} is invoked when an
     *  audit enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAuditEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted audit enabler. {@code
     *  AuditEnablerReceiver.deletedAuditEnabler()} is invoked when
     *  the specified audit enabler is deleted.
     *
     *  @param auditEnablerId the {@code Id} of the
     *          {@code AuditEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code auditEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedAuditEnabler(org.osid.id.Id auditEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
