//
// InvariantMapProxyFoundryLookupSession
//
//    Implements a Foundry lookup service backed by a fixed
//    collection of foundries. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Foundry lookup service backed by a fixed
 *  collection of foundries. The foundries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyFoundryLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapFoundryLookupSession
    implements org.osid.resourcing.FoundryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFoundryLookupSession} with no
     *  foundries.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyFoundryLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyFoundryLookupSession} with a
     *  single foundry.
     *
     *  @param foundry a single foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFoundryLookupSession(org.osid.resourcing.Foundry foundry, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyFoundryLookupSession} using
     *  an array of foundries.
     *
     *  @param foundries an array of foundries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundries} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFoundryLookupSession(org.osid.resourcing.Foundry[] foundries, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putFoundries(foundries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFoundryLookupSession} using a
     *  collection of foundries.
     *
     *  @param foundries a collection of foundries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundries} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFoundryLookupSession(java.util.Collection<? extends org.osid.resourcing.Foundry> foundries,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putFoundries(foundries);
        return;
    }
}
