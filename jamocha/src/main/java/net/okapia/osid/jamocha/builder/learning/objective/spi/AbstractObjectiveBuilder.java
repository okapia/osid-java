//
// AbstractObjective.java
//
//     Defines an Objective builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.objective.spi;


/**
 *  Defines an <code>Objective</code> builder.
 */

public abstract class AbstractObjectiveBuilder<T extends AbstractObjectiveBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.learning.objective.ObjectiveMiter objective;


    /**
     *  Constructs a new <code>AbstractObjectiveBuilder</code>.
     *
     *  @param objective the objective to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractObjectiveBuilder(net.okapia.osid.jamocha.builder.learning.objective.ObjectiveMiter objective) {
        super(objective);
        this.objective = objective;
        return;
    }


    /**
     *  Builds the objective.
     *
     *  @return the new objective
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.learning.Objective build() {
        (new net.okapia.osid.jamocha.builder.validator.learning.objective.ObjectiveValidator(getValidations())).validate(this.objective);
        return (new net.okapia.osid.jamocha.builder.learning.objective.ImmutableObjective(this.objective));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the objective miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.learning.objective.ObjectiveMiter getMiter() {
        return (this.objective);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    public T assessment(org.osid.assessment.Assessment assessment) {
        getMiter().setAssessment(assessment);
        return (self());
    }


    /**
     *  Sets the knowledge category.
     *
     *  @param category a knowledge category
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>category</code>
     *          is <code>null</code>
     */

    public T knowledgeCategory(org.osid.grading.Grade category) {
        getMiter().setKnowledgeCategory(category);
        return (self());
    }


    /**
     *  Sets the cognitive process.
     *
     *  @param cognitiveProcess a cognitive process
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>cognitiveProcess</code> is <code>null</code>
     */

    public T cognitiveProcess(org.osid.grading.Grade cognitiveProcess) {
        getMiter().setCognitiveProcess(cognitiveProcess);
        return (self());
    }


    /**
     *  Adds an Objective record.
     *
     *  @param record an objective record
     *  @param recordType the type of objective record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.learning.records.ObjectiveRecord record, org.osid.type.Type recordType) {
        getMiter().addObjectiveRecord(record, recordType);
        return (self());
    }
}       


