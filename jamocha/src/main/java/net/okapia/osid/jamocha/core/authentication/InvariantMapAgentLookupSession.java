//
// InvariantMapAgentLookupSession
//
//    Implements an Agent lookup service backed by a fixed collection of
//    agents.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication;


/**
 *  Implements an Agent lookup service backed by a fixed
 *  collection of agents. The agents are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAgentLookupSession
    extends net.okapia.osid.jamocha.core.authentication.spi.AbstractMapAgentLookupSession
    implements org.osid.authentication.AgentLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAgentLookupSession</code> with no
     *  agents.
     *  
     *  @param agency the agency
     *  @throws org.osid.NullArgumnetException {@code agency} is
     *          {@code null}
     */

    public InvariantMapAgentLookupSession(org.osid.authentication.Agency agency) {
        setAgency(agency);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgentLookupSession</code> with a single
     *  agent.
     *  
     *  @param agency the agency
     *  @param agent an single agent
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code agent} is <code>null</code>
     */

      public InvariantMapAgentLookupSession(org.osid.authentication.Agency agency,
                                               org.osid.authentication.Agent agent) {
        this(agency);
        putAgent(agent);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgentLookupSession</code> using an array
     *  of agents.
     *  
     *  @param agency the agency
     *  @param agents an array of agents
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code agents} is <code>null</code>
     */

      public InvariantMapAgentLookupSession(org.osid.authentication.Agency agency,
                                               org.osid.authentication.Agent[] agents) {
        this(agency);
        putAgents(agents);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgentLookupSession</code> using a
     *  collection of agents.
     *
     *  @param agency the agency
     *  @param agents a collection of agents
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code agents} is <code>null</code>
     */

      public InvariantMapAgentLookupSession(org.osid.authentication.Agency agency,
                                               java.util.Collection<? extends org.osid.authentication.Agent> agents) {
        this(agency);
        putAgents(agents);
        return;
    }
}
