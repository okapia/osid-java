//
// AbstractMapPositionLookupSession
//
//    A simple framework for providing a Position lookup service
//    backed by a fixed collection of positions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Position lookup service backed by a
 *  fixed collection of positions. The positions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Positions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPositionLookupSession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractPositionLookupSession
    implements org.osid.personnel.PositionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.personnel.Position> positions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.personnel.Position>());


    /**
     *  Makes a <code>Position</code> available in this session.
     *
     *  @param  position a position
     *  @throws org.osid.NullArgumentException <code>position<code>
     *          is <code>null</code>
     */

    protected void putPosition(org.osid.personnel.Position position) {
        this.positions.put(position.getId(), position);
        return;
    }


    /**
     *  Makes an array of positions available in this session.
     *
     *  @param  positions an array of positions
     *  @throws org.osid.NullArgumentException <code>positions<code>
     *          is <code>null</code>
     */

    protected void putPositions(org.osid.personnel.Position[] positions) {
        putPositions(java.util.Arrays.asList(positions));
        return;
    }


    /**
     *  Makes a collection of positions available in this session.
     *
     *  @param  positions a collection of positions
     *  @throws org.osid.NullArgumentException <code>positions<code>
     *          is <code>null</code>
     */

    protected void putPositions(java.util.Collection<? extends org.osid.personnel.Position> positions) {
        for (org.osid.personnel.Position position : positions) {
            this.positions.put(position.getId(), position);
        }

        return;
    }


    /**
     *  Removes a Position from this session.
     *
     *  @param  positionId the <code>Id</code> of the position
     *  @throws org.osid.NullArgumentException <code>positionId<code> is
     *          <code>null</code>
     */

    protected void removePosition(org.osid.id.Id positionId) {
        this.positions.remove(positionId);
        return;
    }


    /**
     *  Gets the <code>Position</code> specified by its <code>Id</code>.
     *
     *  @param  positionId <code>Id</code> of the <code>Position</code>
     *  @return the position
     *  @throws org.osid.NotFoundException <code>positionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>positionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Position getPosition(org.osid.id.Id positionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.personnel.Position position = this.positions.get(positionId);
        if (position == null) {
            throw new org.osid.NotFoundException("position not found: " + positionId);
        }

        return (position);
    }


    /**
     *  Gets all <code>Positions</code>. In plenary mode, the returned
     *  list contains all known positions or an error
     *  results. Otherwise, the returned list may contain only those
     *  positions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Positions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.position.ArrayPositionList(this.positions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.positions.clear();
        super.close();
        return;
    }
}
