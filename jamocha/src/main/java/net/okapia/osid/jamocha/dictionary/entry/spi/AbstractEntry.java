//
// AbstractEntry.java
//
//     Defines an Entry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Entry</code>.
 */

public abstract class AbstractEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.dictionary.Entry {

    private org.osid.type.Type keyType;
    private java.lang.Object key;
    private org.osid.type.Type valueType;
    private java.lang.Object value;

    private final java.util.Collection<org.osid.dictionary.records.EntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Type </code> of this entry key. 
     *
     *  @return the key <code> Type </code> 
     */

    @OSID @Override
    public org.osid.type.Type getKeyType() {
        return (this.keyType);
    }


    /**
     *  Sets the key type.
     *
     *  @param keyType a key type
     *  @throws org.osid.NullArgumentException <code>keyType</code> is
     *          <code>null</code>
     */

    protected void setKeyType(org.osid.type.Type keyType) {
        nullarg(keyType, "key type");
        this.keyType = keyType;
        return;
    }


    /**
     *  Gets the key of this entry. 
     *
     *  @return the key 
     */

    @OSID @Override
    public java.lang.Object getKey() {
        return (this.key);
    }


    /**
     *  Sets the key.
     *
     *  @param key a key
     *  @throws org.osid.NullArgumentException <code>key</code> is
     *          <code>null</code>
     */

    protected void setKey(java.lang.Object key) {
        nullarg(key, "key");
        this.key = key;
        return;
    }


    /**
     *  Gets the <code> Type </code> of this entry value. 
     *
     *  @return the value <code> Type </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueType() {
        return (this.valueType);
    }


    /**
     *  Sets the value type.
     *
     *  @param valueType a value type
     *  @throws org.osid.NullArgumentException <code>valueType</code>
     *          is <code>null</code>
     */

    protected void setValueType(org.osid.type.Type valueType) {
        nullarg(valueType, "value type");
        this.valueType = valueType;
        return;
    }


    /**
     *  Gets the value in this entry. 
     *
     *  @return the value 
     */

    @OSID @Override
    public java.lang.Object getValue() {
        return (this.value);
    }


    /**
     *  Sets the value.
     *
     *  @param value a value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    protected void setValue(java.lang.Object value) {
        nullarg(value, "value");
        this.value = value;
        return;
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.dictionary.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.EntryRecord getEntryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryRecord the entry record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecord</code> or
     *          <code>entryRecordTypeentry</code> is
     *          <code>null</code>
     */
            
    protected void addEntryRecord(org.osid.dictionary.records.EntryRecord entryRecord, 
                                     org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);
        this.records.add(entryRecord);
        
        return;
    }
}
