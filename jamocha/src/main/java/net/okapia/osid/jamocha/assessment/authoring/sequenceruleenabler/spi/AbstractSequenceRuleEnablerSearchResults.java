//
// AbstractSequenceRuleEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSequenceRuleEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.assessment.authoring.SequenceRuleEnablerSearchResults {

    private org.osid.assessment.authoring.SequenceRuleEnablerList sequenceRuleEnablers;
    private final org.osid.assessment.authoring.SequenceRuleEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSequenceRuleEnablerSearchResults.
     *
     *  @param sequenceRuleEnablers the result set
     *  @param sequenceRuleEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablers</code>
     *          or <code>sequenceRuleEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSequenceRuleEnablerSearchResults(org.osid.assessment.authoring.SequenceRuleEnablerList sequenceRuleEnablers,
                                            org.osid.assessment.authoring.SequenceRuleEnablerQueryInspector sequenceRuleEnablerQueryInspector) {
        nullarg(sequenceRuleEnablers, "sequence rule enablers");
        nullarg(sequenceRuleEnablerQueryInspector, "sequence rule enabler query inspectpr");

        this.sequenceRuleEnablers = sequenceRuleEnablers;
        this.inspector = sequenceRuleEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the sequence rule enabler list resulting from a search.
     *
     *  @return a sequence rule enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablers() {
        if (this.sequenceRuleEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.assessment.authoring.SequenceRuleEnablerList sequenceRuleEnablers = this.sequenceRuleEnablers;
        this.sequenceRuleEnablers = null;
	return (sequenceRuleEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.assessment.authoring.SequenceRuleEnablerQueryInspector getSequenceRuleEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  sequence rule enabler search record <code> Type. </code> This method must
     *  be used to retrieve a sequenceRuleEnabler implementing the requested
     *  record.
     *
     *  @param sequenceRuleEnablerSearchRecordType a sequenceRuleEnabler search 
     *         record type 
     *  @return the sequence rule enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(sequenceRuleEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleEnablerSearchResultsRecord getSequenceRuleEnablerSearchResultsRecord(org.osid.type.Type sequenceRuleEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.assessment.authoring.records.SequenceRuleEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(sequenceRuleEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record sequence rule enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSequenceRuleEnablerRecord(org.osid.assessment.authoring.records.SequenceRuleEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "sequence rule enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
