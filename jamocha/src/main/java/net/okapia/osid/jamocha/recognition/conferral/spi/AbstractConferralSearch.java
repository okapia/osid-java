//
// AbstractConferralSearch.java
//
//     A template for making a Conferral Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing conferral searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractConferralSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.recognition.ConferralSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.recognition.records.ConferralSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.recognition.ConferralSearchOrder conferralSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of conferrals. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  conferralIds list of conferrals
     *  @throws org.osid.NullArgumentException
     *          <code>conferralIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongConferrals(org.osid.id.IdList conferralIds) {
        while (conferralIds.hasNext()) {
            try {
                this.ids.add(conferralIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongConferrals</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of conferral Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getConferralIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  conferralSearchOrder conferral search order 
     *  @throws org.osid.NullArgumentException
     *          <code>conferralSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>conferralSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderConferralResults(org.osid.recognition.ConferralSearchOrder conferralSearchOrder) {
	this.conferralSearchOrder = conferralSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.recognition.ConferralSearchOrder getConferralSearchOrder() {
	return (this.conferralSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given conferral search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a conferral implementing the requested record.
     *
     *  @param conferralSearchRecordType a conferral search record
     *         type
     *  @return the conferral search record
     *  @throws org.osid.NullArgumentException
     *          <code>conferralSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(conferralSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralSearchRecord getConferralSearchRecord(org.osid.type.Type conferralSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.recognition.records.ConferralSearchRecord record : this.records) {
            if (record.implementsRecordType(conferralSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this conferral search. 
     *
     *  @param conferralSearchRecord conferral search record
     *  @param conferralSearchRecordType conferral search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConferralSearchRecord(org.osid.recognition.records.ConferralSearchRecord conferralSearchRecord, 
                                           org.osid.type.Type conferralSearchRecordType) {

        addRecordType(conferralSearchRecordType);
        this.records.add(conferralSearchRecord);        
        return;
    }
}
