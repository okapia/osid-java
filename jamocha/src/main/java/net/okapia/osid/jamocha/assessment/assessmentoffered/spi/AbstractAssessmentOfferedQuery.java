//
// AbstractAssessmentOfferedQuery.java
//
//     A template for making an AssessmentOffered Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for assessments offered.
 */

public abstract class AbstractAssessmentOfferedQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.assessment.AssessmentOfferedQuery {

    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the level grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears all level <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches an assessment offered that has any level assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any level, 
     *          <code> false </code> to match offerings with no levsls 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        return;
    }


    /**
     *  Match sequential assessments. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchItemsSequential(boolean match) {
        return;
    }


    /**
     *  Clears all sequential terms. 
     */

    @OSID @Override
    public void clearItemsSequentialTerms() {
        return;
    }


    /**
     *  Match shuffled item assessments. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchItemsShuffled(boolean match) {
        return;
    }


    /**
     *  Clears all shuffled terms. 
     */

    @OSID @Override
    public void clearItemsShuffledTerms() {
        return;
    }


    /**
     *  Matches assessments whose start time falls between the specified range 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchStartTime(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        return;
    }


    /**
     *  Matches offerings that has any start time assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any start 
     *          time, <code> false </code> to match offerings with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyStartTime(boolean match) {
        return;
    }


    /**
     *  Clears all scheduled terms. 
     */

    @OSID @Override
    public void clearStartTimeTerms() {
        return;
    }


    /**
     *  Matches assessments whose end time falls between the specified range 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDeadline(org.osid.calendaring.DateTime start, 
                              org.osid.calendaring.DateTime end, boolean match) {
        return;
    }


    /**
     *  Matches offerings that have any deadline assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any 
     *          deadline, <code> false </code> to match offerings with no 
     *          deadline 
     */

    @OSID @Override
    public void matchAnyDeadline(boolean match) {
        return;
    }


    /**
     *  Clears all deadline terms. 
     */

    @OSID @Override
    public void clearDeadlineTerms() {
        return;
    }


    /**
     *  Matches assessments whose duration falls between the specified range 
     *  inclusive. 
     *
     *  @param  low start range of duration 
     *  @param  high end range of duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        return;
    }


    /**
     *  Matches offerings that have any duration assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any 
     *          duration, <code> false </code> to match offerings with no 
     *          duration 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        return;
    }


    /**
     *  Clears all duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScoreSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears all grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScoreSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getScoreSystemQuery() {
        throw new org.osid.UnimplementedException("supportsScoreSystemQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade system assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade 
     *          system, <code> false </code> to match assessments with no 
     *          grade system 
     */

    @OSID @Override
    public void matchAnyScoreSystem(boolean match) {
        return;
    }


    /**
     *  Clears all grade system terms. 
     */

    @OSID @Override
    public void clearScoreSystemTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears all grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradeSystemQuery() {
        throw new org.osid.UnimplementedException("supportsGradeSystemQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade system assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade 
     *          system, <code> false </code> to match assessments with no 
     *          grade system 
     */

    @OSID @Override
    public void matchAnyGradeSystem(boolean match) {
        return;
    }


    /**
     *  Clears all grade system terms. 
     */

    @OSID @Override
    public void clearGradeSystemTerms() {
        return;
    }


    /**
     *  Sets the rubric assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment offered <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRubricId(org.osid.id.Id assessmentOfferedId, 
                              boolean match) {
        return;
    }


    /**
     *  Clears all rubric assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRubricIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if a rubric assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rubric assessment. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> supportsRubricQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getRubricQuery() {
        throw new org.osid.UnimplementedException("supportsRubricQuery() is false");
    }


    /**
     *  Matches an assessment offered that has any rubric assessment assigned. 
     *
     *  @param  match <code> true </code> to match assessments offered with 
     *          any rubric, <code> false </code> to match assessments offered 
     *          with no rubric 
     */

    @OSID @Override
    public void matchAnyRubric(boolean match) {
        return;
    }


    /**
     *  Clears all rubric assessment terms. 
     */

    @OSID @Override
    public void clearRubricTerms() {
        return;
    }


    /**
     *  Sets the assessment taken <code> Id </code> for this query. 
     *
     *  @param  assessmentTakenId an assessment taken <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentTakenId(org.osid.id.Id assessmentTakenId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears all assessment taken <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentTakenQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment taken query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment taken. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment taken query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuery getAssessmentTakenQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentTakenQuery() is false");
    }


    /**
     *  Matches offerings that have any taken assessment version. 
     *
     *  @param  match <code> true </code> to match offerings with any taken 
     *          assessment, <code> false </code> to match offerings with no 
     *          assessmen taken 
     */

    @OSID @Override
    public void matchAnyAssessmentTaken(boolean match) {
        return;
    }


    /**
     *  Clears all assessment taken terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenTerms() {
        return;
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given assessment offered query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment offered implementing the requested record.
     *
     *  @param assessmentOfferedRecordType an assessment offered record type
     *  @return the assessment offered query record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentOfferedRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedQueryRecord getAssessmentOfferedQueryRecord(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentOfferedQueryRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment offered query. 
     *
     *  @param assessmentOfferedQueryRecord assessment offered query record
     *  @param assessmentOfferedRecordType assessmentOffered record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentOfferedQueryRecord(org.osid.assessment.records.AssessmentOfferedQueryRecord assessmentOfferedQueryRecord, 
                                          org.osid.type.Type assessmentOfferedRecordType) {

        addRecordType(assessmentOfferedRecordType);
        nullarg(assessmentOfferedQueryRecord, "assessment offered query record");
        this.records.add(assessmentOfferedQueryRecord);        
        return;
    }
}
