//
// DocetMiter.java
//
//     Defines a Docet miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.syllabus.docet;


/**
 *  Defines a <code>Docet</code> miter for use with the builders.
 */

public interface DocetMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.syllabus.Docet {


    /**
     *  Sets the module.
     *
     *  @param module a module
     *  @throws org.osid.NullArgumentException <code>module</code> is
     *          <code>null</code>
     */

    public void setModule(org.osid.course.syllabus.Module module);


    /**
     *  Sets the activity unit.
     *
     *  @param activityUnit an activity unit
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnit</code> is <code>null</code>
     */

    public void setActivityUnit(org.osid.course.ActivityUnit activityUnit);


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public void addLearningObjective(org.osid.learning.Objective objective);


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    public void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives);


    /**
     *  Sets the in class flag.
     *
     *  @param inClass <code>true</code> if this occurs within an
     *         activity, <code>false</code> if occurs outside a
     *         convened activity
     */

    public void setInClass(boolean inClass);


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setDuration(org.osid.calendaring.Duration duration);


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public void addAsset(org.osid.repository.Asset asset);


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    public void setAssets(java.util.Collection<org.osid.repository.Asset> assets);


    /**
     *  Adds an assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    public void addAssessment(org.osid.assessment.Assessment assessment);


    /**
     *  Sets all the assessments.
     *
     *  @param assessments a collection of assessments
     *  @throws org.osid.NullArgumentException
     *          <code>assessments</code> is <code>null</code>
     */

    public void setAssessments(java.util.Collection<org.osid.assessment.Assessment> assessments);


    /**
     *  Adds a Docet record.
     *
     *  @param record a docet record
     *  @param recordType the type of docet record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addDocetRecord(org.osid.course.syllabus.records.DocetRecord record, org.osid.type.Type recordType);
}       


