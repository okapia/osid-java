//
// AbstractRecurringEventSearch.java
//
//     A template for making a RecurringEvent Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.recurringevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing recurring event searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRecurringEventSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.RecurringEventSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.RecurringEventSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.RecurringEventSearchOrder recurringEventSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of recurring events. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  recurringEventIds list of recurring events
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRecurringEvents(org.osid.id.IdList recurringEventIds) {
        while (recurringEventIds.hasNext()) {
            try {
                this.ids.add(recurringEventIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRecurringEvents</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of recurring event Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRecurringEventIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  recurringEventSearchOrder recurring event search order 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>recurringEventSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRecurringEventResults(org.osid.calendaring.RecurringEventSearchOrder recurringEventSearchOrder) {
	this.recurringEventSearchOrder = recurringEventSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.RecurringEventSearchOrder getRecurringEventSearchOrder() {
	return (this.recurringEventSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given recurring event search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a recurring event implementing the requested record.
     *
     *  @param recurringEventSearchRecordType a recurring event search record
     *         type
     *  @return the recurring event search record
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventSearchRecord getRecurringEventSearchRecord(org.osid.type.Type recurringEventSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.RecurringEventSearchRecord record : this.records) {
            if (record.implementsRecordType(recurringEventSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recurring event search. 
     *
     *  @param recurringEventSearchRecord recurring event search record
     *  @param recurringEventSearchRecordType recurringEvent search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecurringEventSearchRecord(org.osid.calendaring.records.RecurringEventSearchRecord recurringEventSearchRecord, 
                                           org.osid.type.Type recurringEventSearchRecordType) {

        addRecordType(recurringEventSearchRecordType);
        this.records.add(recurringEventSearchRecord);        
        return;
    }
}
