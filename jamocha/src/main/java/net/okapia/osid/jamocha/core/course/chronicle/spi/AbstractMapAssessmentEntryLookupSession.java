//
// AbstractMapAssessmentEntryLookupSession
//
//    A simple framework for providing an AssessmentEntry lookup service
//    backed by a fixed collection of assessment entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AssessmentEntry lookup service backed by a
 *  fixed collection of assessment entries. The assessment entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAssessmentEntryLookupSession
    extends net.okapia.osid.jamocha.course.chronicle.spi.AbstractAssessmentEntryLookupSession
    implements org.osid.course.chronicle.AssessmentEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.chronicle.AssessmentEntry> assessmentEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.chronicle.AssessmentEntry>());


    /**
     *  Makes an <code>AssessmentEntry</code> available in this session.
     *
     *  @param  assessmentEntry an assessment entry
     *  @throws org.osid.NullArgumentException <code>assessmentEntry<code>
     *          is <code>null</code>
     */

    protected void putAssessmentEntry(org.osid.course.chronicle.AssessmentEntry assessmentEntry) {
        this.assessmentEntries.put(assessmentEntry.getId(), assessmentEntry);
        return;
    }


    /**
     *  Makes an array of assessment entries available in this session.
     *
     *  @param  assessmentEntries an array of assessment entries
     *  @throws org.osid.NullArgumentException <code>assessmentEntries<code>
     *          is <code>null</code>
     */

    protected void putAssessmentEntries(org.osid.course.chronicle.AssessmentEntry[] assessmentEntries) {
        putAssessmentEntries(java.util.Arrays.asList(assessmentEntries));
        return;
    }


    /**
     *  Makes a collection of assessment entries available in this session.
     *
     *  @param  assessmentEntries a collection of assessment entries
     *  @throws org.osid.NullArgumentException <code>assessmentEntries<code>
     *          is <code>null</code>
     */

    protected void putAssessmentEntries(java.util.Collection<? extends org.osid.course.chronicle.AssessmentEntry> assessmentEntries) {
        for (org.osid.course.chronicle.AssessmentEntry assessmentEntry : assessmentEntries) {
            this.assessmentEntries.put(assessmentEntry.getId(), assessmentEntry);
        }

        return;
    }


    /**
     *  Removes an AssessmentEntry from this session.
     *
     *  @param  assessmentEntryId the <code>Id</code> of the assessment entry
     *  @throws org.osid.NullArgumentException <code>assessmentEntryId<code> is
     *          <code>null</code>
     */

    protected void removeAssessmentEntry(org.osid.id.Id assessmentEntryId) {
        this.assessmentEntries.remove(assessmentEntryId);
        return;
    }


    /**
     *  Gets the <code>AssessmentEntry</code> specified by its <code>Id</code>.
     *
     *  @param  assessmentEntryId <code>Id</code> of the <code>AssessmentEntry</code>
     *  @return the assessmentEntry
     *  @throws org.osid.NotFoundException <code>assessmentEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>assessmentEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntry getAssessmentEntry(org.osid.id.Id assessmentEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.AssessmentEntry assessmentEntry = this.assessmentEntries.get(assessmentEntryId);
        if (assessmentEntry == null) {
            throw new org.osid.NotFoundException("assessmentEntry not found: " + assessmentEntryId);
        }

        return (assessmentEntry);
    }


    /**
     *  Gets all <code>AssessmentEntries</code>. In plenary mode, the returned
     *  list contains all known assessmentEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessmentEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AssessmentEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.assessmententry.ArrayAssessmentEntryList(this.assessmentEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentEntries.clear();
        super.close();
        return;
    }
}
