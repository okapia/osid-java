//
// AbstractProfileEntry.java
//
//     Defines a ProfileEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ProfileEntry</code>.
 */

public abstract class AbstractProfileEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.profile.ProfileEntry {

    private boolean implicit = false;
    private org.osid.resource.Resource resource;
    private org.osid.authentication.Agent agent;
    private org.osid.profile.ProfileItem profileItem;

    private final java.util.Collection<org.osid.profile.records.ProfileEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this profile entry is implicitly generated. 
     *
     *  @return <code> true </code> if this profile entry is implicit,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this profile entry is
     *         implicit, <code> false </code> otherwise
     */

    protected void setImplicit(boolean implicit) {
        this.implicit = implicit;
        return;
    }


    /**
     *  Tests if this profile entry has a <code> Resource. </code> 
     *
     *  @return <code> true </code> if this profile entry has a <code> 
     *          Resource, </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasResource() {
        return (this.resource != null);
    }


    /**
     *  Gets the <code> resource Id </code> for this profile entry. 
     *
     *  @return the <code> Resource Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        if (!hasResource()) {
            throw new org.osid.IllegalStateException("hasResource() is false");
        }

        return (this.resource.getId());
    }


    /**
     *  Gets the <code> Resource </code> for this profile ebtry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        if (!hasResource()) {
            throw new org.osid.IllegalStateException("hasResource() is false");
        }

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this profile entry has an <code> Agent. </code> An implied 
     *  profile entry may have an <code> Agent </code> in addition to a 
     *  specified <code> Resource. </code> 
     *
     *  @return <code> true </code> if this profile entry has an <code> Agent, 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAgent() {
        return (this.agent != null);
    }


    /**
     *  Gets the <code> Agent Id </code> for this profile entry. 
     *
     *  @return the <code> Agent Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        if (!hasAgent()) {
            throw new org.osid.IllegalStateException("hasAgent() is false");
        }

        return (this.agent.getId());
    }


    /**
     *  Gets the <code> Agent </code> for this profile entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        if (!hasAgent()) {
            throw new org.osid.IllegalStateException("hasAgent() is false");
        }

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Gets the <code> ProfileItem </code> <code> Id </code> for this profile 
     *  entry. 
     *
     *  @return the profile item <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProfileItemId() {
        return (this.profileItem.getId());
    }


    /**
     *  Gets the <code> ProfileItem </code> for this profile entry. 
     *
     *  @return the profile item 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.profile.ProfileItem getProfileItem()
        throws org.osid.OperationFailedException {

        return (this.profileItem);
    }


    /**
     *  Sets the profile item.
     *
     *  @param item a profile item
     *  @throws org.osid.NullArgumentException
     *          <code>item</code> is <code>null</code>
     */

    protected void setProfileItem(org.osid.profile.ProfileItem item) {
        nullarg(item, "profile item");
        this.profileItem = item;
        return;
    }

    /**
     *  Tests if this profileEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  profileEntryRecordType a profile entry record type 
     *  @return <code>true</code> if the profileEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type profileEntryRecordType) {
        for (org.osid.profile.records.ProfileEntryRecord record : this.records) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ProfileEntry</code> record <code>Type</code>.
     *
     *  @param  profileEntryRecordType the profile entry record type 
     *  @return the profile entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntryRecord getProfileEntryRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileEntryRecord record : this.records) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param profileEntryRecord the profile entry record
     *  @param profileEntryRecordType profile entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecord</code> or
     *          <code>profileEntryRecordTypeprofileEntry</code> is
     *          <code>null</code>
     */
            
    protected void addProfileEntryRecord(org.osid.profile.records.ProfileEntryRecord profileEntryRecord, 
                                         org.osid.type.Type profileEntryRecordType) {
        
        nullarg(profileEntryRecord, "profile entry record");
        addRecordType(profileEntryRecordType);
        this.records.add(profileEntryRecord);
        
        return;
    }
}
