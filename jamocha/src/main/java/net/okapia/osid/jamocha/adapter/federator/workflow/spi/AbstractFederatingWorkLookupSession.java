//
// AbstractFederatingWorkLookupSession.java
//
//     An abstract federating adapter for a WorkLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  WorkLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingWorkLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.workflow.WorkLookupSession>
    implements org.osid.workflow.WorkLookupSession {

    private boolean parallel = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();


    /**
     *  Constructs a new <code>AbstractFederatingWorkLookupSession</code>.
     */

    protected AbstractFederatingWorkLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.workflow.WorkLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>Work</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWorks() {
        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            if (session.canLookupWorks()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Work</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWorkView() {
        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            session.useComparativeWorkView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Work</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWorkView() {
        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            session.usePlenaryWorkView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include works in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            session.useFederatedOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            session.useIsolatedOfficeView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Work</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Work</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Work</code> and
     *  retained for compatibility.
     *
     *  @param  workId <code>Id</code> of the
     *          <code>Work</code>
     *  @return the work
     *  @throws org.osid.NotFoundException <code>workId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>workId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Work getWork(org.osid.id.Id workId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            try {
                return (session.getWork(workId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(workId + " not found");
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the works
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Works</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  workIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>workIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByIds(org.osid.id.IdList workIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.workflow.work.MutableWorkList ret = new net.okapia.osid.jamocha.workflow.work.MutableWorkList();

        try (org.osid.id.IdList ids = workIds) {
            while (ids.hasNext()) {
                ret.addWork(getWork(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given work
     *  genus <code>Type</code> which does not include works of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.work.FederatingWorkList ret = getWorkList();

        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            ret.addWorkList(session.getWorksByGenusType(workGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given work
     *  genus <code>Type</code> and include any additional works with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByParentGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.work.FederatingWorkList ret = getWorkList();

        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            ret.addWorkList(session.getWorksByParentGenusType(workGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>WorkList</code> containing the given work record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @param  workRecordType a work record type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByRecordType(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.work.FederatingWorkList ret = getWorkList();

        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            ret.addWorkList(session.getWorksByRecordType(workRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Works</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  works or an error results. Otherwise, the returned list
     *  may contain only those works that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Works</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.work.FederatingWorkList ret = getWorkList();

        for (org.osid.workflow.WorkLookupSession session : getSessions()) {
            ret.addWorkList(session.getWorks());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.workflow.work.FederatingWorkList getWorkList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.work.ParallelWorkList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.work.CompositeWorkList());
        }
    }
}
