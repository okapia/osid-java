//
// AbstractMutableProduct.java
//
//     Defines a mutable Product.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Product</code>.
 */

public abstract class AbstractMutableProduct
    extends net.okapia.osid.jamocha.ordering.product.spi.AbstractProduct
    implements org.osid.ordering.Product,
               net.okapia.osid.jamocha.builder.ordering.product.ProductMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this product. 
     *
     *  @param record product record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addProductRecord(org.osid.ordering.records.ProductRecord record, org.osid.type.Type recordType) {
        super.addProductRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this product.
     *
     *  @param displayName the name for this product
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this product.
     *
     *  @param description the description of this product
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    @Override
    public void setCode(String code) {
        super.setCode(code);
        return;
    }


    /**
     *  Adds a price schedule.
     *
     *  @param schedule a price schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    @Override
    public void addPriceSchedule(org.osid.ordering.PriceSchedule schedule) {
        super.addPriceSchedule(schedule);
        return;
    }


    /**
     *  Sets all the price schedules.
     *
     *  @param schedules a collection of price schedules
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    @Override
    public void setPriceSchedules(java.util.Collection<org.osid.ordering.PriceSchedule> schedules) {
        super.setPriceSchedules(schedules);
        return;
    }


    /**
     *  Sets the availability.
     *
     *  @param availability an availability
     *  @throws org.osid.InvalidArgumentException
     *          <code>availability</code> is negative
     */

    @Override
    public void setAvailability(long availability) {
        super.setAvailability(availability);
        return;
    }
}

