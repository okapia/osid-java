//
// RegistrationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.registration.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RegistrationElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the RegistrationElement Id.
     *
     *  @return the registration element Id
     */

    public static org.osid.id.Id getRegistrationEntityId() {
        return (makeEntityId("osid.course.registration.Registration"));
    }


    /**
     *  Gets the ActivityBundleId element Id.
     *
     *  @return the ActivityBundleId element Id
     */

    public static org.osid.id.Id getActivityBundleId() {
        return (makeElementId("osid.course.registration.registration.ActivityBundleId"));
    }


    /**
     *  Gets the ActivityBundle element Id.
     *
     *  @return the ActivityBundle element Id
     */

    public static org.osid.id.Id getActivityBundle() {
        return (makeElementId("osid.course.registration.registration.ActivityBundle"));
    }


    /**
     *  Gets the StudentId element Id.
     *
     *  @return the StudentId element Id
     */

    public static org.osid.id.Id getStudentId() {
        return (makeElementId("osid.course.registration.registration.StudentId"));
    }


    /**
     *  Gets the Student element Id.
     *
     *  @return the Student element Id
     */

    public static org.osid.id.Id getStudent() {
        return (makeElementId("osid.course.registration.registration.Student"));
    }


    /**
     *  Gets the Credits element Id.
     *
     *  @return the Credits element Id
     */

    public static org.osid.id.Id getCredits() {
        return (makeElementId("osid.course.registration.registration.Credits"));
    }


    /**
     *  Gets the GradingOptionId element Id.
     *
     *  @return the GradingOptionId element Id
     */

    public static org.osid.id.Id getGradingOptionId() {
        return (makeElementId("osid.course.registration.registration.GradingOptionId"));
    }


    /**
     *  Gets the GradingOption element Id.
     *
     *  @return the GradingOption element Id
     */

    public static org.osid.id.Id getGradingOption() {
        return (makeElementId("osid.course.registration.registration.GradingOption"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.registration.registration.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.registration.registration.CourseCatalog"));
    }
}
