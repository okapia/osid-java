//
// AbstractCredentialEntrySearch.java
//
//     A template for making a CredentialEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.credentialentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing credential entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCredentialEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.chronicle.CredentialEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.chronicle.CredentialEntrySearchOrder credentialEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of credential entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  credentialEntryIds list of credential entries
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCredentialEntries(org.osid.id.IdList credentialEntryIds) {
        while (credentialEntryIds.hasNext()) {
            try {
                this.ids.add(credentialEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCredentialEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of credential entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCredentialEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  credentialEntrySearchOrder credential entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>credentialEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCredentialEntryResults(org.osid.course.chronicle.CredentialEntrySearchOrder credentialEntrySearchOrder) {
	this.credentialEntrySearchOrder = credentialEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.chronicle.CredentialEntrySearchOrder getCredentialEntrySearchOrder() {
	return (this.credentialEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given credential entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a credential entry implementing the requested record.
     *
     *  @param credentialEntrySearchRecordType a credential entry search record
     *         type
     *  @return the credential entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntrySearchRecord getCredentialEntrySearchRecord(org.osid.type.Type credentialEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.chronicle.records.CredentialEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(credentialEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credential entry search. 
     *
     *  @param credentialEntrySearchRecord credential entry search record
     *  @param credentialEntrySearchRecordType credentialEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCredentialEntrySearchRecord(org.osid.course.chronicle.records.CredentialEntrySearchRecord credentialEntrySearchRecord, 
                                           org.osid.type.Type credentialEntrySearchRecordType) {

        addRecordType(credentialEntrySearchRecordType);
        this.records.add(credentialEntrySearchRecord);        
        return;
    }
}
