//
// AbstractUpdateResponseValidator.java
//
//     Validates an UpdateResponse.
//
//
// Tom Coppeto
// Okapia
// 20 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.transaction.batch.updateresponse.spi;


/**
 *  Validates an UpdateResponse.
 */

public abstract class AbstractUpdateResponseValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidCatalogValidator {


    /**
     *  Constructs a new <code>AbstractUpdateResponseValidator</code>.
     */

    protected AbstractUpdateResponseValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractUpdateResponseValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractUpdateResponseValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an UpdateResponse.
     *
     *  @param updateResponse an update response to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>updateResponse</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.transaction.batch.UpdateResponse updateResponse) {
        super.validate(updateResponse);

        test(updateResponse.getFormId(), "getFormId");
        test(updateResponse.getUpdatedId(), "getUpdatedId");
        testConditionalMethod(updateResponse, "getErrorMessage", !updateResponse.isSuccessful(), "!isSuccessful()");
        
        return;
    }
}
