//
// AbstractOsidGovernatorValidator.java
//
//     Validates OsidGovernators.
//
//
// Tom Coppeto
// Okapia
// 20 September 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;


/**
 *  Validates an OsidGovernator.
 */

public abstract class AbstractOsidGovernatorValidator
    extends AbstractSourceableOsidObjectValidator {

    private final OperableValidator validator;


    /**
     *  Constructs a new <code>AbstractOsidGovernatorValidator</code>.
     */

    protected AbstractOsidGovernatorValidator() {
        this.validator = new OperableValidator();
        return;
    }


    /**
     *  Constructs a new <code>AbstractOsidGovernatorValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractOsidGovernatorValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        this.validator = new OperableValidator(validation);
        return;
    }

    
    /**
     *  Validates an OsidGovernator.
     *
     *  @param governator the governator to valdiate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>governator</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public void validate(org.osid.OsidGovernator governator) {
        super.validate(governator);

        if (!(governator instanceof org.osid.Operable)) {
            throw new org.osid.UnsupportedException("governator not an Operable");
        }

        this.validator.validate((org.osid.Operable) governator);
        return;
    }


    protected class OperableValidator
        extends AbstractOperableValidator {

        protected OperableValidator() {
            return;
        }


        protected OperableValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
            super(validation);
            return;
        }

        
        @Override
        public void validate(org.osid.Operable operable) {
            super.validate(operable);
            return;
        }
    }
}
