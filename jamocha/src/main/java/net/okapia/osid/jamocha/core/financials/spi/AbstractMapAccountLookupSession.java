//
// AbstractMapAccountLookupSession
//
//    A simple framework for providing an Account lookup service
//    backed by a fixed collection of accounts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Account lookup service backed by a
 *  fixed collection of accounts. The accounts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Accounts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAccountLookupSession
    extends net.okapia.osid.jamocha.financials.spi.AbstractAccountLookupSession
    implements org.osid.financials.AccountLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.financials.Account> accounts = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.financials.Account>());


    /**
     *  Makes an <code>Account</code> available in this session.
     *
     *  @param  account an account
     *  @throws org.osid.NullArgumentException <code>account<code>
     *          is <code>null</code>
     */

    protected void putAccount(org.osid.financials.Account account) {
        this.accounts.put(account.getId(), account);
        return;
    }


    /**
     *  Makes an array of accounts available in this session.
     *
     *  @param  accounts an array of accounts
     *  @throws org.osid.NullArgumentException <code>accounts<code>
     *          is <code>null</code>
     */

    protected void putAccounts(org.osid.financials.Account[] accounts) {
        putAccounts(java.util.Arrays.asList(accounts));
        return;
    }


    /**
     *  Makes a collection of accounts available in this session.
     *
     *  @param  accounts a collection of accounts
     *  @throws org.osid.NullArgumentException <code>accounts<code>
     *          is <code>null</code>
     */

    protected void putAccounts(java.util.Collection<? extends org.osid.financials.Account> accounts) {
        for (org.osid.financials.Account account : accounts) {
            this.accounts.put(account.getId(), account);
        }

        return;
    }


    /**
     *  Removes an Account from this session.
     *
     *  @param  accountId the <code>Id</code> of the account
     *  @throws org.osid.NullArgumentException <code>accountId<code> is
     *          <code>null</code>
     */

    protected void removeAccount(org.osid.id.Id accountId) {
        this.accounts.remove(accountId);
        return;
    }


    /**
     *  Gets the <code>Account</code> specified by its <code>Id</code>.
     *
     *  @param  accountId <code>Id</code> of the <code>Account</code>
     *  @return the account
     *  @throws org.osid.NotFoundException <code>accountId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>accountId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.financials.Account account = this.accounts.get(accountId);
        if (account == null) {
            throw new org.osid.NotFoundException("account not found: " + accountId);
        }

        return (account);
    }


    /**
     *  Gets all <code>Accounts</code>. In plenary mode, the returned
     *  list contains all known accounts or an error
     *  results. Otherwise, the returned list may contain only those
     *  accounts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Accounts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccounts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.account.ArrayAccountList(this.accounts.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.accounts.clear();
        super.close();
        return;
    }
}
