//
// AbstractPersonnelBatchProxyManager.java
//
//     An adapter for a PersonnelBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a PersonnelBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterPersonnelBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.personnel.batch.PersonnelBatchProxyManager>
    implements org.osid.personnel.batch.PersonnelBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterPersonnelBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterPersonnelBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterPersonnelBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterPersonnelBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of persons is available. 
     *
     *  @return <code> true </code> if a person bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonBatchAdmin() {
        return (getAdapteeManager().supportsPersonBatchAdmin());
    }


    /**
     *  Tests if bulk administration of organizations is available. 
     *
     *  @return <code> true </code> if an organization bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationBatchAdmin() {
        return (getAdapteeManager().supportsOrganizationBatchAdmin());
    }


    /**
     *  Tests if bulk administration of positions is available. 
     *
     *  @return <code> true </code> if a position bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionBatchAdmin() {
        return (getAdapteeManager().supportsPositionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of appointments is available. 
     *
     *  @return <code> true </code> if an appointment bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentBatchAdmin() {
        return (getAdapteeManager().supportsAppointmentBatchAdmin());
    }


    /**
     *  Tests if bulk administration of realms is available. 
     *
     *  @return <code> true </code> if a realm bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmBatchAdmin() {
        return (getAdapteeManager().supportsRealmBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk person 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonBatchAdminSession getPersonBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk person 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonBatchAdminSession getPersonBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonBatchAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  organization administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.OrganizationBatchAdminSession getOrganizationBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  organization administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.OrganizationBatchAdminSession getOrganizationBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationBatchAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk position 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PositionBatchAdminSession getPositionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk position 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PositionBatchAdminSession getPositionBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionBatchAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  appointment administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.AppointmentBatchAdminSession getAppointmentBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  appointment administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.AppointmentBatchAdminSession getAppointmentBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentBatchAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk realm 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.RealmBatchAdminSession getRealmBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
