//
// AbstractObjectiveSearch.java
//
//     A template for making an Objective Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing objective searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractObjectiveSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.learning.ObjectiveSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.learning.records.ObjectiveSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.learning.ObjectiveSearchOrder objectiveSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of objectives. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  objectiveIds list of objectives
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongObjectives(org.osid.id.IdList objectiveIds) {
        while (objectiveIds.hasNext()) {
            try {
                this.ids.add(objectiveIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongObjectives</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of objective Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getObjectiveIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  objectiveSearchOrder objective search order 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>objectiveSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderObjectiveResults(org.osid.learning.ObjectiveSearchOrder objectiveSearchOrder) {
	this.objectiveSearchOrder = objectiveSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.learning.ObjectiveSearchOrder getObjectiveSearchOrder() {
	return (this.objectiveSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given objective search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an objective implementing the requested record.
     *
     *  @param objectiveSearchRecordType an objective search record
     *         type
     *  @return the objective search record
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveSearchRecord getObjectiveSearchRecord(org.osid.type.Type objectiveSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.learning.records.ObjectiveSearchRecord record : this.records) {
            if (record.implementsRecordType(objectiveSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this objective search. 
     *
     *  @param objectiveSearchRecord objective search record
     *  @param objectiveSearchRecordType objective search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObjectiveSearchRecord(org.osid.learning.records.ObjectiveSearchRecord objectiveSearchRecord, 
                                           org.osid.type.Type objectiveSearchRecordType) {

        addRecordType(objectiveSearchRecordType);
        this.records.add(objectiveSearchRecord);        
        return;
    }
}
