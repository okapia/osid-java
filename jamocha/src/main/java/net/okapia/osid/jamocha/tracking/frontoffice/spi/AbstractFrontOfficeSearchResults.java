//
// AbstractFrontOfficeSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.frontoffice.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractFrontOfficeSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.tracking.FrontOfficeSearchResults {

    private org.osid.tracking.FrontOfficeList frontOffices;
    private final org.osid.tracking.FrontOfficeQueryInspector inspector;
    private final java.util.Collection<org.osid.tracking.records.FrontOfficeSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractFrontOfficeSearchResults.
     *
     *  @param frontOffices the result set
     *  @param frontOfficeQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>frontOffices</code>
     *          or <code>frontOfficeQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractFrontOfficeSearchResults(org.osid.tracking.FrontOfficeList frontOffices,
                                            org.osid.tracking.FrontOfficeQueryInspector frontOfficeQueryInspector) {
        nullarg(frontOffices, "front offices");
        nullarg(frontOfficeQueryInspector, "front office query inspectpr");

        this.frontOffices = frontOffices;
        this.inspector = frontOfficeQueryInspector;

        return;
    }


    /**
     *  Gets the front office list resulting from a search.
     *
     *  @return a front office list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOffices() {
        if (this.frontOffices == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.tracking.FrontOfficeList frontOffices = this.frontOffices;
        this.frontOffices = null;
	return (frontOffices);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.tracking.FrontOfficeQueryInspector getFrontOfficeQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  front office search record <code> Type. </code> This method must
     *  be used to retrieve a frontOffice implementing the requested
     *  record.
     *
     *  @param frontOfficeSearchRecordType a frontOffice search 
     *         record type 
     *  @return the front office search
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(frontOfficeSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.FrontOfficeSearchResultsRecord getFrontOfficeSearchResultsRecord(org.osid.type.Type frontOfficeSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.tracking.records.FrontOfficeSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(frontOfficeSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(frontOfficeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record front office search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addFrontOfficeRecord(org.osid.tracking.records.FrontOfficeSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "front office record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
