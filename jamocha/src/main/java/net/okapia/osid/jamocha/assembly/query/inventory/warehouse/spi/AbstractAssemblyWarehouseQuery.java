//
// AbstractAssemblyWarehouseQuery.java
//
//     A WarehouseQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inventory.warehouse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A WarehouseQuery that stores terms.
 */

public abstract class AbstractAssemblyWarehouseQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.inventory.WarehouseQuery,
               org.osid.inventory.WarehouseQueryInspector,
               org.osid.inventory.WarehouseSearchOrder {

    private final java.util.Collection<org.osid.inventory.records.WarehouseQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.WarehouseQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.WarehouseSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyWarehouseQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyWarehouseQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches warehouses that have any item. 
     *
     *  @param  match <code> true </code> to match warehouses with any item, 
     *          <code> false </code> to match warehouses with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the item query terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.inventory.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.inventory.ItemQueryInspector[0]);
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the catalog <code> Id </code> for this query. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        getAssembler().addIdTerm(getStockIdColumn(), stockId, match);
        return;
    }


    /**
     *  Clears the stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        getAssembler().clearTerms(getStockIdColumn());
        return;
    }


    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the stock <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (getAssembler().getIdTerms(getStockIdColumn()));
    }


    /**
     *  Gets the StockId column name.
     *
     * @return the column name
     */

    protected String getStockIdColumn() {
        return ("stock_id");
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Matches warehouses that have any stock. 
     *
     *  @param  match <code> true </code> to match courses with any stock, 
     *          <code> false </code> to match courses with no stock 
     */

    @OSID @Override
    public void matchAnyStock(boolean match) {
        getAssembler().addIdWildcardTerm(getStockColumn(), match);
        return;
    }


    /**
     *  Clears the stock query terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        getAssembler().clearTerms(getStockColumn());
        return;
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the stock query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the Stock column name.
     *
     * @return the column name
     */

    protected String getStockColumn() {
        return ("stock");
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId a model <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        getAssembler().addIdTerm(getModelIdColumn(), modelId, match);
        return;
    }


    /**
     *  Clears the model <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        getAssembler().clearTerms(getModelIdColumn());
        return;
    }


    /**
     *  Gets the model <code> Id </code> query terms. 
     *
     *  @return the model <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModelIdTerms() {
        return (getAssembler().getIdTerms(getModelIdColumn()));
    }


    /**
     *  Gets the ModelId column name.
     *
     * @return the column name
     */

    protected String getModelIdColumn() {
        return ("model_id");
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inventory. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches any related model. 
     *
     *  @param  match <code> true </code> to match warehouses with any model, 
     *          <code> false </code> to match warehouses with no models 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        getAssembler().addIdWildcardTerm(getModelColumn(), match);
        return;
    }


    /**
     *  Clears the model terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        getAssembler().clearTerms(getModelColumn());
        return;
    }


    /**
     *  Gets the model query terms. 
     *
     *  @return the model query terms 
     */

    @OSID @Override
    public org.osid.inventory.ModelQueryInspector[] getModelTerms() {
        return (new org.osid.inventory.ModelQueryInspector[0]);
    }


    /**
     *  Gets the Model column name.
     *
     * @return the column name
     */

    protected String getModelColumn() {
        return ("model");
    }


    /**
     *  Sets the inventory <code> Id </code> for this query. 
     *
     *  @param  itemId an inventory <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInventoryId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getInventoryIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the inventory <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInventoryIdTerms() {
        getAssembler().clearTerms(getInventoryIdColumn());
        return;
    }


    /**
     *  Gets the inventory <code> Id </code> query terms. 
     *
     *  @return the inventory <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInventoryIdTerms() {
        return (getAssembler().getIdTerms(getInventoryIdColumn()));
    }


    /**
     *  Gets the InventoryId column name.
     *
     * @return the column name
     */

    protected String getInventoryIdColumn() {
        return ("inventory_id");
    }


    /**
     *  Tests if an <code> InventoryQuery </code> is available. 
     *
     *  @return <code> true </code> if an inventory query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inventory. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inventory query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuery getInventoryQuery() {
        throw new org.osid.UnimplementedException("supportsInventoryQuery() is false");
    }


    /**
     *  Matches warehouses that have any inventory. 
     *
     *  @param  match <code> true </code> to match warehouses with any 
     *          inventory, <code> false </code> to match warehouses with no 
     *          inventories 
     */

    @OSID @Override
    public void matchAnyInventory(boolean match) {
        getAssembler().addIdWildcardTerm(getInventoryColumn(), match);
        return;
    }


    /**
     *  Clears the inventory query terms. 
     */

    @OSID @Override
    public void clearInventoryTerms() {
        getAssembler().clearTerms(getInventoryColumn());
        return;
    }


    /**
     *  Gets the inventory query terms. 
     *
     *  @return the inventory query terms 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQueryInspector[] getInventoryTerms() {
        return (new org.osid.inventory.InventoryQueryInspector[0]);
    }


    /**
     *  Gets the Inventory column name.
     *
     * @return the column name
     */

    protected String getInventoryColumn() {
        return ("inventory");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match 
     *  warehouses that have the specified warehouse as an ancestor. 
     *
     *  @param  warehouseId a warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorWarehouseId(org.osid.id.Id warehouseId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the ancestor warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorWarehouseIdTerms() {
        getAssembler().clearTerms(getAncestorWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the ancestor warehouse <code> Id </code> query terms. 
     *
     *  @return the ancestor warehouse <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getAncestorWarehouseIdColumn()));
    }


    /**
     *  Gets the AncestorWarehouseId column name.
     *
     * @return the column name
     */

    protected String getAncestorWarehouseIdColumn() {
        return ("ancestor_warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorWarehouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getAncestorWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorWarehouseQuery() is false");
    }


    /**
     *  Matches warehouses with any warehouse ancestor. 
     *
     *  @param  match <code> true </code> to match warehouses with any 
     *          ancestor, <code> false </code> to match root warehouses 
     */

    @OSID @Override
    public void matchAnyAncestorWarehouse(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorWarehouseColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor warehouse query terms. 
     */

    @OSID @Override
    public void clearAncestorWarehouseTerms() {
        getAssembler().clearTerms(getAncestorWarehouseColumn());
        return;
    }


    /**
     *  Gets the ancestor warehouse query terms. 
     *
     *  @return the ancestor warehouse terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getAncestorWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the AncestorWarehouse column name.
     *
     * @return the column name
     */

    protected String getAncestorWarehouseColumn() {
        return ("ancestor_warehouse");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match 
     *  warehouses that have the specified warehouse as an descendant. 
     *
     *  @param  warehouseId a warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantWarehouseId(org.osid.id.Id warehouseId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the descendant warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantWarehouseIdTerms() {
        getAssembler().clearTerms(getDescendantWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the descendant warehouse <code> Id </code> query terms. 
     *
     *  @return the descendant warehouse <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getDescendantWarehouseIdColumn()));
    }


    /**
     *  Gets the DescendantWarehouseId column name.
     *
     * @return the column name
     */

    protected String getDescendantWarehouseIdColumn() {
        return ("descendant_warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantWarehouseQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getDescendantWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantWarehouseQuery() is false");
    }


    /**
     *  Matches warehouses with any descendant warehouse. 
     *
     *  @param  match <code> true </code> to match warehouses with any 
     *          descendant, <code> false </code> to match leaf warehouses 
     */

    @OSID @Override
    public void matchAnyDescendantWarehouse(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantWarehouseColumn(), match);
        return;
    }


    /**
     *  Clears the descendant warehouse query terms. 
     */

    @OSID @Override
    public void clearDescendantWarehouseTerms() {
        getAssembler().clearTerms(getDescendantWarehouseColumn());
        return;
    }


    /**
     *  Gets the descendant warehouse query terms. 
     *
     *  @return the descendant warehouse terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getDescendantWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the DescendantWarehouse column name.
     *
     * @return the column name
     */

    protected String getDescendantWarehouseColumn() {
        return ("descendant_warehouse");
    }


    /**
     *  Tests if this warehouse supports the given record
     *  <code>Type</code>.
     *
     *  @param  warehouseRecordType a warehouse record type 
     *  @return <code>true</code> if the warehouseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type warehouseRecordType) {
        for (org.osid.inventory.records.WarehouseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(warehouseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  warehouseRecordType the warehouse record type 
     *  @return the warehouse query record 
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(warehouseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.WarehouseQueryRecord getWarehouseQueryRecord(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.WarehouseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(warehouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(warehouseRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  warehouseRecordType the warehouse record type 
     *  @return the warehouse query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(warehouseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.WarehouseQueryInspectorRecord getWarehouseQueryInspectorRecord(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.WarehouseQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(warehouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(warehouseRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param warehouseRecordType the warehouse record type
     *  @return the warehouse search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(warehouseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.WarehouseSearchOrderRecord getWarehouseSearchOrderRecord(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.WarehouseSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(warehouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(warehouseRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this warehouse. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param warehouseQueryRecord the warehouse query record
     *  @param warehouseQueryInspectorRecord the warehouse query inspector
     *         record
     *  @param warehouseSearchOrderRecord the warehouse search order record
     *  @param warehouseRecordType warehouse record type
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseQueryRecord</code>,
     *          <code>warehouseQueryInspectorRecord</code>,
     *          <code>warehouseSearchOrderRecord</code> or
     *          <code>warehouseRecordTypewarehouse</code> is
     *          <code>null</code>
     */
            
    protected void addWarehouseRecords(org.osid.inventory.records.WarehouseQueryRecord warehouseQueryRecord, 
                                      org.osid.inventory.records.WarehouseQueryInspectorRecord warehouseQueryInspectorRecord, 
                                      org.osid.inventory.records.WarehouseSearchOrderRecord warehouseSearchOrderRecord, 
                                      org.osid.type.Type warehouseRecordType) {

        addRecordType(warehouseRecordType);

        nullarg(warehouseQueryRecord, "warehouse query record");
        nullarg(warehouseQueryInspectorRecord, "warehouse query inspector record");
        nullarg(warehouseSearchOrderRecord, "warehouse search odrer record");

        this.queryRecords.add(warehouseQueryRecord);
        this.queryInspectorRecords.add(warehouseQueryInspectorRecord);
        this.searchOrderRecords.add(warehouseSearchOrderRecord);
        
        return;
    }
}
