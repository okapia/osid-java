//
// AbstractAssemblyRenovationQuery.java
//
//     A RenovationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.construction.renovation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RenovationQuery that stores terms.
 */

public abstract class AbstractAssemblyRenovationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.room.construction.RenovationQuery,
               org.osid.room.construction.RenovationQueryInspector,
               org.osid.room.construction.RenovationSearchOrder {

    private final java.util.Collection<org.osid.room.construction.records.RenovationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.construction.records.RenovationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.construction.records.RenovationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRenovationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRenovationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to renovations. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getRoomIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        getAssembler().clearTerms(getRoomIdColumn());
        return;
    }


    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (getAssembler().getIdTerms(getRoomIdColumn()));
    }


    /**
     *  Gets the RoomId column name.
     *
     * @return the column name
     */

    protected String getRoomIdColumn() {
        return ("room_id");
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a renovation. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        getAssembler().clearTerms(getRoomColumn());
        return;
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the Room column name.
     *
     * @return the column name
     */

    protected String getRoomColumn() {
        return ("room");
    }


    /**
     *  Matches a cost within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCost(org.osid.financials.Currency low, 
                          org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getCostColumn(), low, high, match);
        return;
    }


    /**
     *  Matches any cost. 
     *
     *  @param  match <code> true </code> to match projects with any cost 
     *          assigned, <code> false </code> to match buildings with no cost 
     *          assigned 
     */

    @OSID @Override
    public void matchAnyCost(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getCostColumn(), match);
        return;
    }


    /**
     *  Clears the cost terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        getAssembler().clearTerms(getCostColumn());
        return;
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (getAssembler().getCurrencyRangeTerms(getCostColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCostColumn(), style);
        return;
    }


    /**
     *  Gets the Cost column name.
     *
     * @return the column name
     */

    protected String getCostColumn() {
        return ("cost");
    }


    /**
     *  Sets the renovation <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        getAssembler().clearTerms(getCampusIdColumn());
        return;
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (getAssembler().getIdTerms(getCampusIdColumn()));
    }


    /**
     *  Gets the CampusId column name.
     *
     * @return the column name
     */

    protected String getCampusIdColumn() {
        return ("campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        getAssembler().clearTerms(getCampusColumn());
        return;
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the Campus column name.
     *
     * @return the column name
     */

    protected String getCampusColumn() {
        return ("campus");
    }


    /**
     *  Tests if this renovation supports the given record
     *  <code>Type</code>.
     *
     *  @param  renovationRecordType a renovation record type 
     *  @return <code>true</code> if the renovationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type renovationRecordType) {
        for (org.osid.room.construction.records.RenovationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(renovationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  renovationRecordType the renovation record type 
     *  @return the renovation query record 
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(renovationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationQueryRecord getRenovationQueryRecord(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.RenovationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(renovationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(renovationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  renovationRecordType the renovation record type 
     *  @return the renovation query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(renovationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationQueryInspectorRecord getRenovationQueryInspectorRecord(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.RenovationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(renovationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(renovationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param renovationRecordType the renovation record type
     *  @return the renovation search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(renovationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationSearchOrderRecord getRenovationSearchOrderRecord(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.RenovationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(renovationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(renovationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this renovation. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param renovationQueryRecord the renovation query record
     *  @param renovationQueryInspectorRecord the renovation query inspector
     *         record
     *  @param renovationSearchOrderRecord the renovation search order record
     *  @param renovationRecordType renovation record type
     *  @throws org.osid.NullArgumentException
     *          <code>renovationQueryRecord</code>,
     *          <code>renovationQueryInspectorRecord</code>,
     *          <code>renovationSearchOrderRecord</code> or
     *          <code>renovationRecordTyperenovation</code> is
     *          <code>null</code>
     */
            
    protected void addRenovationRecords(org.osid.room.construction.records.RenovationQueryRecord renovationQueryRecord, 
                                      org.osid.room.construction.records.RenovationQueryInspectorRecord renovationQueryInspectorRecord, 
                                      org.osid.room.construction.records.RenovationSearchOrderRecord renovationSearchOrderRecord, 
                                      org.osid.type.Type renovationRecordType) {

        addRecordType(renovationRecordType);

        nullarg(renovationQueryRecord, "renovation query record");
        nullarg(renovationQueryInspectorRecord, "renovation query inspector record");
        nullarg(renovationSearchOrderRecord, "renovation search odrer record");

        this.queryRecords.add(renovationQueryRecord);
        this.queryInspectorRecords.add(renovationQueryInspectorRecord);
        this.searchOrderRecords.add(renovationSearchOrderRecord);
        
        return;
    }
}
