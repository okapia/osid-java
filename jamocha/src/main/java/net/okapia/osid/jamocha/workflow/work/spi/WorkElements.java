//
// WorkElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.work.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class WorkElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the WorkElement Id.
     *
     *  @return the work element Id
     */

    public static org.osid.id.Id getWorkEntityId() {
        return (makeEntityId("osid.workflow.Work"));
    }


    /**
     *  Gets the Suspended element Id.
     *
     *  @return the Suspended element Id
     */

    public static org.osid.id.Id getSuspended() {
        return (makeQueryElementId("osid.workflow.work.Suspended"));
    }


    /**
     *  Gets the ProcessId element Id.
     *
     *  @return the ProcessId element Id
     */

    public static org.osid.id.Id getProcessId() {
        return (makeQueryElementId("osid.workflow.work.ProcessId"));
    }


    /**
     *  Gets the Process element Id.
     *
     *  @return the Process element Id
     */

    public static org.osid.id.Id getProcess() {
        return (makeQueryElementId("osid.workflow.work.Process"));
    }


    /**
     *  Gets the StepId element Id.
     *
     *  @return the StepId element Id
     */

    public static org.osid.id.Id getStepId() {
        return (makeQueryElementId("osid.workflow.work.StepId"));
    }


    /**
     *  Gets the Step element Id.
     *
     *  @return the Step element Id
     */

    public static org.osid.id.Id getStep() {
        return (makeQueryElementId("osid.workflow.work.Step"));
    }


    /**
     *  Gets the OfficeId element Id.
     *
     *  @return the OfficeId element Id
     */

    public static org.osid.id.Id getOfficeId() {
        return (makeQueryElementId("osid.workflow.work.OfficeId"));
    }


    /**
     *  Gets the Office element Id.
     *
     *  @return the Office element Id
     */

    public static org.osid.id.Id getOffice() {
        return (makeQueryElementId("osid.workflow.work.Office"));
    }
}
