//
// InvariantMapAssessmentPartLookupSession
//
//    Implements an AssessmentPart lookup service backed by a fixed collection of
//    assessmentParts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements an AssessmentPart lookup service backed by a fixed
 *  collection of assessment parts. The assessment parts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractMapAssessmentPartLookupSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentPartLookupSession</code> with no
     *  assessment parts.
     *  
     *  @param bank the bank
     *  @throws org.osid.NullArgumnetException {@code bank} is
     *          {@code null}
     */

    public InvariantMapAssessmentPartLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentPartLookupSession</code> with a single
     *  assessment part.
     *  
     *  @param bank the bank
     *  @param assessmentPart an single assessment part
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentPart} is <code>null</code>
     */

      public InvariantMapAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        this(bank);
        putAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentPartLookupSession</code> using an array
     *  of assessment parts.
     *  
     *  @param bank the bank
     *  @param assessmentParts an array of assessment parts
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentParts} is <code>null</code>
     */

      public InvariantMapAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.authoring.AssessmentPart[] assessmentParts) {
        this(bank);
        putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentPartLookupSession</code> using a
     *  collection of assessment parts.
     *
     *  @param bank the bank
     *  @param assessmentParts a collection of assessment parts
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentParts} is <code>null</code>
     */

      public InvariantMapAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                               java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts) {
        this(bank);
        putAssessmentParts(assessmentParts);
        return;
    }
}
