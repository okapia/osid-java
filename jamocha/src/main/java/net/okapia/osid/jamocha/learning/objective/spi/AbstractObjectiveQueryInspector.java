//
// AbstractObjectiveQueryInspector.java
//
//     A template for making an ObjectiveQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for objectives.
 */

public abstract class AbstractObjectiveQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.learning.ObjectiveQueryInspector {

    private final java.util.Collection<org.osid.learning.records.ObjectiveQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssessmentTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the knowledge category <code> Id </code> query terms. 
     *
     *  @return the knowledge category <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getKnowledgeCategoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the knowledge category query terms. 
     *
     *  @return the knowledge category terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getKnowledgeCategoryTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the cognitive process <code> Id </code> query terms. 
     *
     *  @return the cognitive process <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCognitiveProcessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the cognitive process query terms. 
     *
     *  @return the cognitive process terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getCognitiveProcessTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the requisite objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequisiteObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the requisite objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getRequisiteObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the requisite objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependentObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the requisite objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getDependentObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the equivalent objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEquivalentObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the equivalent objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getEquivalentObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the ancestor objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getAncestorObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the descendant objective <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant objective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getDescendantObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.learning.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.learning.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the objective bank <code> Id </code> query terms. 
     *
     *  @return the objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective bank query terms. 
     *
     *  @return the objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given objective query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an objective implementing the requested record.
     *
     *  @param objectiveRecordType an objective record type
     *  @return the objective query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveQueryInspectorRecord getObjectiveQueryInspectorRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveRecordType + " is not supported");
    }


    /**
     *  Adds a record to this objective query. 
     *
     *  @param objectiveQueryInspectorRecord objective query inspector
     *         record
     *  @param objectiveRecordType objective record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObjectiveQueryInspectorRecord(org.osid.learning.records.ObjectiveQueryInspectorRecord objectiveQueryInspectorRecord, 
                                                   org.osid.type.Type objectiveRecordType) {

        addRecordType(objectiveRecordType);
        nullarg(objectiveRecordType, "objective record type");
        this.records.add(objectiveQueryInspectorRecord);        
        return;
    }
}
