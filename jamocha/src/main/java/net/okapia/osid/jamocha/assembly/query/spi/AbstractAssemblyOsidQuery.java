//
// AbstractAssemblyOsidQuery.java
//
//     An OsidQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OsidQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidQuery
    implements org.osid.OsidQuery,
               org.osid.OsidQueryInspector,
               org.osid.OsidSearchOrder {

    private final net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler;

    
    /** 
     *  Constructs a new <code>AbstractAssemblyOsidQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        nullarg(assembler, "assembler");
        this.assembler = assembler;
        return;
    }
    

    /**
     *  Gets the query assembler.
     *
     *  @return the query assembler
     */

    protected net.okapia.osid.jamocha.assembly.query.QueryAssembler getAssembler() {
        return (this.assembler);
    }


    /**
     *  Gets the string matching types supported. A string match type
     *  specifies the syntax of the string query, such as matching a
     *  word or including a wildcard or regular expression.
     *
     *  @return a list containing the supported string match types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStringMatchTypes() {
        return (getAssembler().getStringMatchTypes());
    }


    /**
     *  Tests if the given string matching type is supported. 
     *
     *  @param  stringMatchType a <code> Type </code> indicating a string match 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code>stringMatchType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean supportsStringMatchType(org.osid.type.Type stringMatchType) {
        return (getAssembler().supportsStringMatchType(stringMatchType));
    }


    /**
     *  Adds a keyword to match. Multiple keywords can be added to perform a
     *  boolean <code> OR </code> among them. A keyword may be applied to any
     *  of the elements defined in this object such as the display name,
     *  description or any method defined in an interface implemented by this
     *  object.
     *
     *  @param  keyword keyword to match
     *  @param  stringMatchType the string match type
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> keyword </code> or
     *          <code> stringMatchType </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException <code>
     *          supportsStringMatchType(stringMatchType) </code> is <code>
     *  false </code>
     */

    @OSID @Override
    public void matchKeyword(String keyword, org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getKeywordColumn(), keyword, stringMatchType, match);
        return;
    }


    /**
     *  Clears all keyword terms. 
     */

    @OSID @Override
    public void clearKeywordTerms() {
        getAssembler().clearTerms(getKeywordColumn());
        return;
    }


    /**
     *  Gets the keyword query terms. 
     *
     *  @return the keyword terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getKeywordTerms() {
        return (getAssembler().getStringTerms(getKeywordColumn()));
    }


    /**
     *  Gets the column name for the keyword field.
     *
     *  @return the column name
     */

    protected String getKeywordColumn() {
        return ("keyword");
    }


    /**
     *  Matches any object. 
     *
     *  @param match <code> true </code> to match any object, <code>
     *         false </code> to match no objectsx
     */

    @OSID @Override
    public void matchAny(boolean match) {
        getAssembler().addBooleanTerm(getAnyColumn(), match);
        return;
    }


    /**
     *  Clears the match any terms. 
     */

    @OSID @Override
    public void clearAnyTerms() {
        getAssembler().clearTerms(getAnyColumn());
        return;
    }


    /**
     *  Gets the any query terms. 
     *
     *  @return the any terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAnyTerms() {
        return (getAssembler().getBooleanTerms(getAnyColumn()));
    }


    /**
     *  Gets the column name for the any field.
     *
     *  @return the column name
     */

    protected String getAnyColumn() {
        return ("any");
    }
}
