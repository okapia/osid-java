//
// AbstractBlog.java
//
//     Defines a BlogNode within an in core hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract class for managing a hierarchy of blog
 *  nodes in core.
 */

public abstract class AbstractBlogNode
    extends net.okapia.osid.jamocha.spi.AbstractOsidNode
    implements org.osid.blogging.BlogNode,
               org.osid.hierarchy.Node {

    private final org.osid.blogging.Blog blog;
    private final java.util.Collection<org.osid.blogging.BlogNode> parents  = new java.util.HashSet<org.osid.blogging.BlogNode>();
    private final java.util.Collection<org.osid.blogging.BlogNode> children = new java.util.HashSet<org.osid.blogging.BlogNode>();


    /**
     *  Constructs a new <code>AbstractBlogNode</code> from a
     *  single blog.
     *
     *  @param blog the blog
     *  @throws org.osid.NullArgumentException <code>blog</code> is 
     *          <code>null</code>.
     */

    protected AbstractBlogNode(org.osid.blogging.Blog blog) {
        setId(blog.getId());
        this.blog = blog;
        return;
    }


    /**
     *  Constructs a new <code>AbstractBlogNode</code> from a
     *  single blog.
     *
     *  @param blog the blog
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>blog</code> is 
     *          <code>null</code>.
     */

    protected AbstractBlogNode(org.osid.blogging.Blog blog, boolean root, boolean leaf) {
        this(blog);

        if (root) {
            root();
        } else {
            unroot();
        }

        if (leaf) {
            leaf();
        } else {
            unleaf();
        }
        
        return;
    }


    /**
     *  Adds a parent to this blog.
     *
     *  @param node the parent blog node to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    protected void addParent(org.osid.blogging.BlogNode node) {
        nullarg(node, "node");

        if (isRoot()) {
            throw new org.osid.IllegalStateException(getId() + " is a root");
        }

        this.parents.add(node);
        return;
    }


    /**
     *  Adds a child to this blog.
     *
     *  @param node the child blog node to add
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void addChild(org.osid.blogging.BlogNode node) {
        nullarg(node, "blog node");
        this.children.add(node);
        return;
    }


    /**
     *  Gets the <code> Blog </code> at this node.
     *
     *  @return the blog represented by this node
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog() {
        return (this.blog);
    }


    /**
     *  Tests if any parents are available in this node structure. There may 
     *  be no more parents in this node structure however there may be parents 
     *  that exist in the hierarchy. 
     *
     *  @return <code> true </code> if this node has parents, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean hasParents() {
        return (this.parents.size() > 0);
    }


    /**
     *  Tests if any children are available in this node structure. There may 
     *  be no more children available in this node structure but this node may 
     *  have children in the hierarchy. 
     *
     *  @return <code> true </code> if this node has children, <code>
     *          false </code> otherwise
     */
    
    @OSID @Override
    public boolean hasChildren() {
        return (this.children.size() > 0);
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of this node
     */

    @OSID @Override
    public org.osid.id.IdList getParentIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToIdList(this.parents));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getParents() {
        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToNodeList(getParentBlogNodes()));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.blogging.BlogNodeList getParentBlogNodes() {
        return (new net.okapia.osid.jamocha.blogging.blognode.ArrayBlogNodeList(this.parents));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of this node
     */

    @OSID @Override
    public org.osid.id.IdList getChildIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToIdList(this.children));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getChildren() {
        return (new net.okapia.osid.jamocha.adapter.converter.blogging.blognode.BlogNodeToNodeList(getChildBlogNodes()));
    }


    /**
     *  Gets the child nodes of this blog.
     *
     *  @return the child nodes of this blog
     */

    @OSID @Override
    public org.osid.blogging.BlogNodeList getChildBlogNodes() {
        return (new net.okapia.osid.jamocha.blogging.blognode.ArrayBlogNodeList(this.children));
    }
}
