//
// AbstractMapContactLookupSession
//
//    A simple framework for providing a Contact lookup service
//    backed by a fixed collection of contacts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Contact lookup service backed by a
 *  fixed collection of contacts. The contacts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Contacts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapContactLookupSession
    extends net.okapia.osid.jamocha.contact.spi.AbstractContactLookupSession
    implements org.osid.contact.ContactLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.contact.Contact> contacts = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.contact.Contact>());


    /**
     *  Makes a <code>Contact</code> available in this session.
     *
     *  @param  contact a contact
     *  @throws org.osid.NullArgumentException <code>contact<code>
     *          is <code>null</code>
     */

    protected void putContact(org.osid.contact.Contact contact) {
        this.contacts.put(contact.getId(), contact);
        return;
    }


    /**
     *  Makes an array of contacts available in this session.
     *
     *  @param  contacts an array of contacts
     *  @throws org.osid.NullArgumentException <code>contacts<code>
     *          is <code>null</code>
     */

    protected void putContacts(org.osid.contact.Contact[] contacts) {
        putContacts(java.util.Arrays.asList(contacts));
        return;
    }


    /**
     *  Makes a collection of contacts available in this session.
     *
     *  @param  contacts a collection of contacts
     *  @throws org.osid.NullArgumentException <code>contacts<code>
     *          is <code>null</code>
     */

    protected void putContacts(java.util.Collection<? extends org.osid.contact.Contact> contacts) {
        for (org.osid.contact.Contact contact : contacts) {
            this.contacts.put(contact.getId(), contact);
        }

        return;
    }


    /**
     *  Removes a Contact from this session.
     *
     *  @param  contactId the <code>Id</code> of the contact
     *  @throws org.osid.NullArgumentException <code>contactId<code> is
     *          <code>null</code>
     */

    protected void removeContact(org.osid.id.Id contactId) {
        this.contacts.remove(contactId);
        return;
    }


    /**
     *  Gets the <code>Contact</code> specified by its <code>Id</code>.
     *
     *  @param  contactId <code>Id</code> of the <code>Contact</code>
     *  @return the contact
     *  @throws org.osid.NotFoundException <code>contactId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>contactId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Contact getContact(org.osid.id.Id contactId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.contact.Contact contact = this.contacts.get(contactId);
        if (contact == null) {
            throw new org.osid.NotFoundException("contact not found: " + contactId);
        }

        return (contact);
    }


    /**
     *  Gets all <code>Contacts</code>. In plenary mode, the returned
     *  list contains all known contacts or an error
     *  results. Otherwise, the returned list may contain only those
     *  contacts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Contacts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContacts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.contact.ArrayContactList(this.contacts.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.contacts.clear();
        super.close();
        return;
    }
}
