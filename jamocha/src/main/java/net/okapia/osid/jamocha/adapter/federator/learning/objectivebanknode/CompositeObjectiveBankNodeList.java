//
// CompositeObjectiveBankNodeList.java
//
//     Implements an ObjectiveBankNodeList. This list allows 
//     ObjectiveBankNodeLists to be added in serial. This is not as 
//     efficient as the ParallelObjectiveBankNodeList but 
//     preserves the ordering.
//
//
// Tom Coppeto
// Okapia
// 17 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.learning.objectivebanknode;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements an <code>ObjectiveBankNodeList</code>. This list allows
 *  ObjectiveBankNodeLists to be added a bit at a time and are read off in
 *  serial. <code>noMore</code> must be invoked when all the lists
 *  have been added.
 *
 *  Two error handlers are offered. A hard error throws an exception
 *  for any error received by an underlying list. A soft error ignores
 *  errors in underlying lists and proceeds to the next list.
 */

public class CompositeObjectiveBankNodeList
    extends net.okapia.osid.jamocha.learning.objectivebanknode.spi.AbstractObjectiveBankNodeList
    implements org.osid.learning.ObjectiveBankNodeList,
               net.okapia.osid.jamocha.adapter.federator.learning.objectivebanknode.FederatingObjectiveBankNodeList {

    private final java.util.List<org.osid.learning.ObjectiveBankNodeList> lists = java.util.Collections.synchronizedList(new java.util.LinkedList<org.osid.learning.ObjectiveBankNodeList>());
    private org.osid.learning.ObjectiveBankNodeList active;

    private boolean soft = false;
    private boolean eol = false;


    /**
     *  Creates a new empty <code>CompositeObjectiveBankNodeList</code>. The
     *  underlying errors fail hard.
     */

    public CompositeObjectiveBankNodeList() {
        return;
    }


    /**
     *  Creates a new empty <code>CompositeObjectiveBankNodeList</code>.
     *
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     */

    public CompositeObjectiveBankNodeList(boolean soft) {
        this.soft = soft;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        primeList();
        if (this.active == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        primeList();
        if (this.active == null) {
            return (0);
        } else {
            return (this.active.available());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *  
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        long skipped = 0;

        while (skipped < n) {
            primeList();
            if (this.active == null) {
                return;
            }
            
            long s = this.active.available();
            if ((s + skipped) > n) {
                s = n - skipped;
            }
                        
            this.active.skip(s);
            skipped += s;
        }

        return;
    }


    /**
     *  Gets the next <code> ObjectiveBankNode </code> in this list.
     *
     *  @return the next <code> ObjectiveBankNode </code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code> ObjectiveBankNode </code> is available
     *          before calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankNode getNextObjectiveBankNode()
        throws org.osid.OperationFailedException {

        while (true) {
            primeList();
            if (this.active != null) {
                try {
                    return (this.active.getNextObjectiveBankNode());
                } catch (org.osid.OperationFailedException ofe) {
                    if (!this.soft) {
                        throw ofe;
                    }
                }
            } else {
                throw new org.osid.OperationFailedException("unable to get a next element");
            }
        }
    }

        
    /**
     *  Gets the next set of <code> ObjectiveBankNode </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param n the number of <code> ObjectiveBankNode </code> elements
     *          requested which must be less than or equal to <code>
     *          available() </code>
     *  @return an array of <code> ObjectiveBankNode </code> elements. <code>
     *          </code> The length of the array is less than or equal
     *          to the number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankNode[] getNextObjectiveBankNodes(long n)
        throws org.osid.OperationFailedException {
        
        java.util.List<org.osid.learning.ObjectiveBankNode> ret = new java.util.ArrayList<>((int) n);
        long count = 0;

        while (count < n) {
            primeList();
            if (this.active == null) {
                break;
            }

            try {
                for (org.osid.learning.ObjectiveBankNode objectiveBankNode : this.active.getNextObjectiveBankNodes(this.active.available())) {
                    ret.add(objectiveBankNode);
                    ++count;
                }
            } catch (org.osid.OperationFailedException ofe) {
                if (!this.soft) {
                    throw ofe;
                }
            }
        }

        return (ret.toArray(new org.osid.learning.ObjectiveBankNode[ret.size()]));      
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {

        for (org.osid.learning.ObjectiveBankNodeList list : this.lists) {
            try {
                list.close();
            } catch (Exception e) {}
        }

        this.active  = null;
        this.eol     = true;
        this.lists.clear();

        return;
    }

        
    /**
     *  Adds an <code>ObjectiveBankNodeList</code> to this
     *  <code>ObjectiveBankNodeList</code>.
     *
     *  @param objectiveBankNodeList the <code>ObjectiveBankNodeList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>objectiveBankNodeList</code>
     *          is <code>null</code>
     */

    @Override
    public void addObjectiveBankNodeList(org.osid.learning.ObjectiveBankNodeList objectiveBankNodeList) {
        nullarg(objectiveBankNodeList, "objectivebanknode list");

        synchronized (this.lists) {
            this.lists.add(objectiveBankNodeList);
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Adds a collection of <code>ObjectiveBankNodeLists</code> to this
     *  <code>ObjectiveBankNodeList</code>.
     *
     *  @param objectiveBankNodeLists the <code>ObjectiveBankNodeList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankNodeLists</code> is <code>null</code>
     */

    @Override
    public void addObjectiveBankNodeLists(java.util.Collection<org.osid.learning.ObjectiveBankNodeList> objectiveBankNodeLists) {
        nullarg(objectiveBankNodeLists, "objectivebanknode lists");

        for (org.osid.learning.ObjectiveBankNodeList list : objectiveBankNodeLists) {
            addObjectiveBankNodeList(list);
        }

        return;
    }


    /**
     *  No more. Invoked when no more lists are to be added.
     */

    @Override
    public void noMore() {
        this.eol = true;
        synchronized (this.lists) {
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    @Override
    public boolean isOperational() {
        if ((this.lists.size() == 0) && this.eol) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        if (this.lists.size() == 0) {
            return (true);
        }

        return (false);
    }


    private void primeList() {
        while (true) {
            if (this.active == null) {
                if ((this.lists.size() == 0) && (this.eol)) {
                    return;
                }
                
                this.active = this.lists.get(0);
            }
            
            if (this.active.hasNext()) {
                return;
            }
            
            this.lists.remove(0);
            this.active = null;
            
            synchronized (this.lists) {                 
                if (!this.eol && (this.lists.size() == 0)) {
                    try {
                        this.lists.wait();
                    } catch (InterruptedException ie) { }
                }
            }
        }
    }
}
