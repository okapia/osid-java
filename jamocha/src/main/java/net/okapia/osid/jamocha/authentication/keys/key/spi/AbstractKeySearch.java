//
// AbstractKeySearch.java
//
//     A template for making a Key Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing key searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractKeySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.authentication.keys.KeySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.authentication.keys.records.KeySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.authentication.keys.KeySearchOrder keySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of keys. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  keyIds list of keys
     *  @throws org.osid.NullArgumentException
     *          <code>keyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongKeys(org.osid.id.IdList keyIds) {
        while (keyIds.hasNext()) {
            try {
                this.ids.add(keyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongKeys</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of key Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getKeyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  keySearchOrder key search order 
     *  @throws org.osid.NullArgumentException
     *          <code>keySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>keySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderKeyResults(org.osid.authentication.keys.KeySearchOrder keySearchOrder) {
	this.keySearchOrder = keySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.authentication.keys.KeySearchOrder getKeySearchOrder() {
	return (this.keySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given key search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a key implementing the requested record.
     *
     *  @param keySearchRecordType a key search record
     *         type
     *  @return the key search record
     *  @throws org.osid.NullArgumentException
     *          <code>keySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(keySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeySearchRecord getKeySearchRecord(org.osid.type.Type keySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.authentication.keys.records.KeySearchRecord record : this.records) {
            if (record.implementsRecordType(keySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this key search. 
     *
     *  @param keySearchRecord key search record
     *  @param keySearchRecordType key search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addKeySearchRecord(org.osid.authentication.keys.records.KeySearchRecord keySearchRecord, 
                                           org.osid.type.Type keySearchRecordType) {

        addRecordType(keySearchRecordType);
        this.records.add(keySearchRecord);        
        return;
    }
}
