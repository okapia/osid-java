//
// AbstractObjectSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;


/**
 *  An abstract class for OsidSearchResults.
 *
 *  This interface provides a means to capture results of a search and
 *  is used as a vehicle to perform a search within a previous result
 *  set. An example of searching withina result set: 
 *
 *  <pre>
 *       OsidSearch os = session.getObjectSearch();
 *       
 *       OsidQuery query;
 *       query = session.getObjectQuery();
 *       query.matchDescription("*food*", wildcardStringMatchType, true);
 *       ObjectSearchResults results = session.getObjectBySearch(query, os);
 *       
 *       // get new search inteface and reference previous result set
 *       os = session.getObjectSearch();
 *       os.searchWithinResults(results);
 *       
 *       query = session.getObjectQuery();
 *       query.matchDisplayName("pickles", wordStringMatchType, true);
 *       results = session.getObjectsBySearch(query, os);
 *       OsidList pickles = results.getObjectList();
 *  </pre>
 */

public abstract class AbstractOsidSearchResults
    implements org.osid.OsidSearchResults {

    private long size = 0;
    private final Types recordTypes = new TypeSet();
    private final java.util.Map<org.osid.type.Type, java.util.Collection<org.osid.Property>> properties = java.util.Collections.synchronizedMap(new java.util.HashMap<org.osid.type.Type, java.util.Collection<org.osid.Property>>());


    /**
     *  Returns the size of a result set from a search query. This number 
     *  serves as an estimate to provide feedback for refining search queries 
     *  and may not be the number of elements available through an <code> 
     *  OsidList. </code> 
     *
     *  @return the result size 
     */

    @OSID @Override
    public long getResultSize() {
	return (size);
    }


    /**
     *  Sets the result size.
     *
     *  @param size
     *  @throws org.osid.InvalidArgumentException <code>size</code>
     *          is negative
     */

    protected void setResultSize(long size) {
	cardinalarg(size, "size");
	this.size = size;
	return;
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return an empty list
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
	return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recordTypes.toCollection()));
    }


    /**
     *  Tests if this object supports the given record <code> Type. </code> 
     *
     *  @param recordType a type 
     *  @return <code>true</code> if the type is supported,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
	return (this.recordTypes.contains(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
	this.recordTypes.add(recordType);
	return;
    }


    /**
     *  Gets a list of all properties of this object including those
     *  corresponding to data within this object's records. Properties
     *  provide a means for applications to display a representation
     *  of the contents of an object without understanding its record
     *  interface specifications. Applications needing to examine a
     *  specific property or perform updates should use the methods
     *  defined by the object's record <code>Type</code>.
     *
     *  @return a list of properties 
     */
    
    @OSID @Override
    public org.osid.PropertyList getProperties()
        throws org.osid.OperationFailedException,
	       org.osid.PermissionDeniedException {

	java.util.List<org.osid.Property> properties = new java.util.ArrayList<org.osid.Property>();

	for (java.util.Collection<org.osid.Property> set : this.properties.values()) {
	    properties.addAll(set);
	}

	return (new net.okapia.osid.jamocha.property.ArrayPropertyList(properties));
    }


    /**
     *  This abstract class supports no record types. Override to
     *  support a record.
     *
     *  @param recordType the record type corresponding to the
     *         properties set to retrieve
     *  @return a list of properties 
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.PropertyList getPropertiesByRecordType(org.osid.type.Type recordType)
        throws org.osid.OperationFailedException,
	org.osid.PermissionDeniedException {

	if (!hasRecordType(recordType)) {
	    throw new org.osid.UnsupportedException("record type not supported");
	}

	java.util.Collection<org.osid.Property> set = this.properties.get(recordType);
	if (set == null) {
	    return (new net.okapia.osid.jamocha.nil.property.EmptyPropertyList());
	} else {
	    return (new net.okapia.osid.jamocha.property.ArrayPropertyList(set));
	}
    }

    
    protected void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
	nullarg(set, "set");
	nullarg(recordType, "record type");
	this.properties.put(recordType, set);
	return;
    }    
}
