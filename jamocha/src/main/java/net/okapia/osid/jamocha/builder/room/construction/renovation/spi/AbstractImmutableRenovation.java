//
// AbstractImmutableRenovation.java
//
//     Wraps a mutable Renovation to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.construction.renovation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Renovation</code> to hide modifiers. This
 *  wrapper provides an immutized Renovation from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying renovation whose state changes are visible.
 */

public abstract class AbstractImmutableRenovation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.room.construction.Renovation {

    private final org.osid.room.construction.Renovation renovation;


    /**
     *  Constructs a new <code>AbstractImmutableRenovation</code>.
     *
     *  @param renovation the renovation to immutablize
     *  @throws org.osid.NullArgumentException <code>renovation</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRenovation(org.osid.room.construction.Renovation renovation) {
        super(renovation);
        this.renovation = renovation;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the affected rooms. 
     *
     *  @return the room <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRoomIds() {
        return (this.renovation.getRoomIds());
    }


    /**
     *  Gets the affected rooms. 
     *
     *  @return the rooms 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.RoomList getRooms()
        throws org.osid.OperationFailedException {

        return (this.renovation.getRooms());
    }


    /**
     *  Tests if this renovation has a cost. 
     *
     *  @return <code> true </code> if a cost is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCost() {
        return (this.renovation.hasCost());
    }


    /**
     *  Gets the cost for this renovation. 
     *
     *  @return the cost 
     *  @throws org.osid.IllegalStateException <code> hasCost() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCost() {
        return (this.renovation.getCost());
    }


    /**
     *  Gets the renovation record corresponding to the given <code> 
     *  Renovation </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  renovationRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(renovationRecordType) </code> is <code> true </code> . 
     *
     *  @param  renovationRecordType the type of renovation record to retrieve 
     *  @return the renovation record 
     *  @throws org.osid.NullArgumentException <code> renovationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(renovationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationRecord getRenovationRecord(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException {

        return (this.renovation.getRenovationRecord(renovationRecordType));
    }
}

