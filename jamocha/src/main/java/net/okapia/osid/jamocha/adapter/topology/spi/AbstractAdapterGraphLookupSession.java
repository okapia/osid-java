//
// AbstractAdapterGraphLookupSession.java
//
//    A Graph lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Graph lookup session adapter.
 */

public abstract class AbstractAdapterGraphLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.topology.GraphLookupSession {

    private final org.osid.topology.GraphLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterGraphLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGraphLookupSession(org.osid.topology.GraphLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Graph} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupGraphs() {
        return (this.session.canLookupGraphs());
    }


    /**
     *  A complete view of the {@code Graph} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGraphView() {
        this.session.useComparativeGraphView();
        return;
    }


    /**
     *  A complete view of the {@code Graph} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGraphView() {
        this.session.usePlenaryGraphView();
        return;
    }

     
    /**
     *  Gets the {@code Graph} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Graph} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Graph} and
     *  retained for compatibility.
     *
     *  @param graphId {@code Id} of the {@code Graph}
     *  @return the graph
     *  @throws org.osid.NotFoundException {@code graphId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code graphId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGraph(graphId));
    }


    /**
     *  Gets a {@code GraphList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  graphs specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Graphs} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  graphIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Graph} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code graphIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByIds(org.osid.id.IdList graphIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGraphsByIds(graphIds));
    }


    /**
     *  Gets a {@code GraphList} corresponding to the given
     *  graph genus {@code Type} which does not include
     *  graphs of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned {@code Graph} list
     *  @throws org.osid.NullArgumentException
     *          {@code graphGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGraphsByGenusType(graphGenusType));
    }


    /**
     *  Gets a {@code GraphList} corresponding to the given
     *  graph genus {@code Type} and include any additional
     *  graphs with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned {@code Graph} list
     *  @throws org.osid.NullArgumentException
     *          {@code graphGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByParentGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGraphsByParentGenusType(graphGenusType));
    }


    /**
     *  Gets a {@code GraphList} containing the given
     *  graph record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphRecordType a graph record type 
     *  @return the returned {@code Graph} list
     *  @throws org.osid.NullArgumentException
     *          {@code graphRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByRecordType(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGraphsByRecordType(graphRecordType));
    }


    /**
     *  Gets a {@code GraphList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Graph} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGraphsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Graphs}. 
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Graphs} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGraphs());
    }
}
