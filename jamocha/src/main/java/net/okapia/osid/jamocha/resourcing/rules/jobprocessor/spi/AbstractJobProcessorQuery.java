//
// AbstractJobProcessorQuery.java
//
//     A template for making a JobProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for job processors.
 */

public abstract class AbstractJobProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.resourcing.rules.JobProcessorQuery {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to the job. 
     *
     *  @param  foundryId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledJobId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the job <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledJobIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsRuledJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getRuledJobQuery() {
        throw new org.osid.UnimplementedException("supportsRuledJobQuery() is false");
    }


    /**
     *  Matches mapped to any job. 
     *
     *  @param  match <code> true </code> for mapped to any job, <code> false 
     *          </code> to match mapped to no job 
     */

    @OSID @Override
    public void matchAnyRuledJob(boolean match) {
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearRuledJobTerms() {
        return;
    }


    /**
     *  Matches mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given job processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a job processor implementing the requested record.
     *
     *  @param jobProcessorRecordType a job processor record type
     *  @return the job processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorQueryRecord getJobProcessorQueryRecord(org.osid.type.Type jobProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(jobProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job processor query. 
     *
     *  @param jobProcessorQueryRecord job processor query record
     *  @param jobProcessorRecordType jobProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobProcessorQueryRecord(org.osid.resourcing.rules.records.JobProcessorQueryRecord jobProcessorQueryRecord, 
                                          org.osid.type.Type jobProcessorRecordType) {

        addRecordType(jobProcessorRecordType);
        nullarg(jobProcessorQueryRecord, "job processor query record");
        this.records.add(jobProcessorQueryRecord);        
        return;
    }
}
