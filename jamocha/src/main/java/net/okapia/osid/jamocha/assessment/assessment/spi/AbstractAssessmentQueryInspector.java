//
// AbstractAssessmentQueryInspector.java
//
//     A template for making an AssessmentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for assessments.
 */

public abstract class AbstractAssessmentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.assessment.AssessmentQueryInspector {

    private final java.util.Collection<org.osid.assessment.records.AssessmentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the level <code> Id </code> query terms. 
     *
     *  @return the level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the level query terms. 
     *
     *  @return the level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRubricIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getRubricTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.assessment.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.assessment.ItemQueryInspector[0]);
    }


    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentOfferedIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment offered query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getAssessmentOfferedTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Gets the assessment taken <code> Id </code> query terms. 
     *
     *  @return the assessment taken <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentTakenIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment taken query terms. 
     *
     *  @return the assessment taken terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQueryInspector[] getAssessmentTakenTerms() {
        return (new org.osid.assessment.AssessmentTakenQueryInspector[0]);
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given assessment query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an assessment implementing the requested record.
     *
     *  @param assessmentRecordType an assessment record type
     *  @return the assessment query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentQueryInspectorRecord getAssessmentQueryInspectorRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment query. 
     *
     *  @param assessmentQueryInspectorRecord assessment query inspector
     *         record
     *  @param assessmentRecordType assessment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentQueryInspectorRecord(org.osid.assessment.records.AssessmentQueryInspectorRecord assessmentQueryInspectorRecord, 
                                                   org.osid.type.Type assessmentRecordType) {

        addRecordType(assessmentRecordType);
        nullarg(assessmentRecordType, "assessment record type");
        this.records.add(assessmentQueryInspectorRecord);        
        return;
    }
}
