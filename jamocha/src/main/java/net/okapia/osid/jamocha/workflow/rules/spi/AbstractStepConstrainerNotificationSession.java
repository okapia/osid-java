//
// AbstractStepConstrainerNotificationSession.java
//
//     A template for making StepConstrainerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code StepConstrainer} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code StepConstrainer} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for step constrainer entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractStepConstrainerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.rules.StepConstrainerNotificationSession {

    private boolean federated = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();


    /**
     *  Gets the {@code Office} {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Office Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }

    
    /**
     *  Gets the {@code Office} associated with this session.
     *
     *  @return the {@code Office} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the {@code Office}.
     *
     *  @param office the office for this session
     *  @throws org.osid.NullArgumentException {@code office}
     *          is {@code null}
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can register for {@code StepConstrainer}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForStepConstrainerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeStepConstrainerNotification() </code>.
     */

    @OSID @Override
    public void reliableStepConstrainerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableStepConstrainerNotifications() {
        return;
    }


    /**
     *  Acknowledge a step constrainer notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeStepConstrainerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainers in offices which are
     *  children of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new step constrainers. {@code
     *  StepConstrainerReceiver.newStepConstrainer()} is invoked when
     *  a new {@code StepConstrainer} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewStepConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated step
     *  constrainers. {@code
     *  StepConstrainerReceiver.changedStepConstrainer()} is invoked
     *  when a step constrainer is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedStepConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated step
     *  constrainer. {@code
     *  StepConstrainerReceiver.changedStepConstrainer()} is invoked
     *  when the specified step constrainer is changed.
     *
     *  @param stepConstrainerId the {@code Id} of the {@code StepConstrainer} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code stepConstrainerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedStepConstrainer(org.osid.id.Id stepConstrainerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted step
     *  constrainers. {@code
     *  StepConstrainerReceiver.deletedStepConstrainer()} is invoked
     *  when a step constrainer is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedStepConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted step
     *  constrainer. {@code
     *  StepConstrainerReceiver.deletedStepConstrainer()} is invoked
     *  when the specified step constrainer is deleted.
     *
     *  @param stepConstrainerId the {@code Id} of the
     *          {@code StepConstrainer} to monitor
     *  @throws org.osid.NullArgumentException {@code stepConstrainerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedStepConstrainer(org.osid.id.Id stepConstrainerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
