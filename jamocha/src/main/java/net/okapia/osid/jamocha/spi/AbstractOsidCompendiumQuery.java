//
// AbstractOsidCompendiumQuery.java
//
//     Defines an OsidCompendiumQuery.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an OsidCompendiumQuery.
 */

public abstract class AbstractOsidCompendiumQuery
    extends AbstractOsidObjectQuery
    implements org.osid.OsidCompendiumQuery {


    /**
     *  Matches compendiums whose start date falls in between the given dates 
     *  inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        return;
    }


    /**
     *  Matches compendiums with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        return;
    }

    
    /**
     *  Matches compendiums whose effective end date falls in between the given 
     *  dates inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end, 
                             boolean match) {
        return;
    }


    /**
     *  Matches compendiums with any end date set. 
     *
     *  @param match <code> true </code> to match any end date, <code>
     *         false </code> to match no start date
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        return;
    }

    
    /**
     *  Match reports that are interpolated. 
     *
     *  @param  match <code> true </code> to match any interpolated reports, 
     *          <code> false </code> to match non-interpolated reports 
     */

    @OSID @Override
    public void matchInterpolated(boolean match) {
        return;
    }


    /**
     *  Clears the interpolated query terms. 
     */

    @OSID @Override
    public void clearInterpolatedTerms() {
        return;
    }


    /**
     *  Match reports that are extrapolated. 
     *
     *  @param  match <code> true </code> to match any extrapolated reports, 
     *          <code> false </code> to match non-extrapolated reports 
     */

    @OSID @Override
    public void matchExtrapolated(boolean match) {
        return;
    }


    /**
     *  Clears the extrapolated query terms. 
     */

    @OSID @Override
    public void clearExtrapolatedTerms() {
        return;
    }
}