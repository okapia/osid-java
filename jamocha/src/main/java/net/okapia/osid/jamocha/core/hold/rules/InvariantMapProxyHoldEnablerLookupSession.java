//
// InvariantMapProxyHoldEnablerLookupSession
//
//    Implements a HoldEnabler lookup service backed by a fixed
//    collection of holdEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.rules;


/**
 *  Implements a HoldEnabler lookup service backed by a fixed
 *  collection of hold enablers. The hold enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyHoldEnablerLookupSession
    extends net.okapia.osid.jamocha.core.hold.rules.spi.AbstractMapHoldEnablerLookupSession
    implements org.osid.hold.rules.HoldEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyHoldEnablerLookupSession} with no
     *  hold enablers.
     *
     *  @param oubliette the oubliette
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.proxy.Proxy proxy) {
        setOubliette(oubliette);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyHoldEnablerLookupSession} with a single
     *  hold enabler.
     *
     *  @param oubliette the oubliette
     *  @param holdEnabler a single hold enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code holdEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.hold.rules.HoldEnabler holdEnabler, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putHoldEnabler(holdEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyHoldEnablerLookupSession} using
     *  an array of hold enablers.
     *
     *  @param oubliette the oubliette
     *  @param holdEnablers an array of hold enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code holdEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.hold.rules.HoldEnabler[] holdEnablers, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putHoldEnablers(holdEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyHoldEnablerLookupSession} using a
     *  collection of hold enablers.
     *
     *  @param oubliette the oubliette
     *  @param holdEnablers a collection of hold enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code holdEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                  java.util.Collection<? extends org.osid.hold.rules.HoldEnabler> holdEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putHoldEnablers(holdEnablers);
        return;
    }
}
