//
// AbstractPriceSchedule.java
//
//     Defines a PriceSchedule builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.priceschedule.spi;


/**
 *  Defines a <code>PriceSchedule</code> builder.
 */

public abstract class AbstractPriceScheduleBuilder<T extends AbstractPriceScheduleBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ordering.priceschedule.PriceScheduleMiter priceSchedule;


    /**
     *  Constructs a new <code>AbstractPriceScheduleBuilder</code>.
     *
     *  @param priceSchedule the price schedule to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPriceScheduleBuilder(net.okapia.osid.jamocha.builder.ordering.priceschedule.PriceScheduleMiter priceSchedule) {
        super(priceSchedule);
        this.priceSchedule = priceSchedule;
        return;
    }


    /**
     *  Builds the price schedule.
     *
     *  @return the new price schedule
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ordering.PriceSchedule build() {
        (new net.okapia.osid.jamocha.builder.validator.ordering.priceschedule.PriceScheduleValidator(getValidations())).validate(this.priceSchedule);
        return (new net.okapia.osid.jamocha.builder.ordering.priceschedule.ImmutablePriceSchedule(this.priceSchedule));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the price schedule miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ordering.priceschedule.PriceScheduleMiter getMiter() {
        return (this.priceSchedule);
    }


    /**
     *  Adds a price.
     *
     *  @param price a price
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    public T price(org.osid.ordering.Price price) {
        getMiter().addPrice(price);
        return (self());
    }


    /**
     *  Sets all the prices.
     *
     *  @param prices a collection of prices
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>prices</code> is <code>null</code>
     */

    public T prices(java.util.Collection<org.osid.ordering.Price> prices) {
        getMiter().setPrices(prices);
        return (self());
    }


    /**
     *  Adds a PriceSchedule record.
     *
     *  @param record a price schedule record
     *  @param recordType the type of price schedule record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ordering.records.PriceScheduleRecord record, org.osid.type.Type recordType) {
        getMiter().addPriceScheduleRecord(record, recordType);
        return (self());
    }
}       


