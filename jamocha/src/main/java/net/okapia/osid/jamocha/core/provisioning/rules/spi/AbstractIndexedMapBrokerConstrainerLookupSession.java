//
// AbstractIndexedMapBrokerConstrainerLookupSession.java
//
//    A simple framework for providing a BrokerConstrainer lookup service
//    backed by a fixed collection of broker constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a BrokerConstrainer lookup service backed by a
 *  fixed collection of broker constrainers. The broker constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some broker constrainers may be compatible
 *  with more types than are indicated through these broker constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BrokerConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBrokerConstrainerLookupSession
    extends AbstractMapBrokerConstrainerLookupSession
    implements org.osid.provisioning.rules.BrokerConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerConstrainer> brokerConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerConstrainer> brokerConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerConstrainer>());


    /**
     *  Makes a <code>BrokerConstrainer</code> available in this session.
     *
     *  @param  brokerConstrainer a broker constrainer
     *  @throws org.osid.NullArgumentException <code>brokerConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBrokerConstrainer(org.osid.provisioning.rules.BrokerConstrainer brokerConstrainer) {
        super.putBrokerConstrainer(brokerConstrainer);

        this.brokerConstrainersByGenus.put(brokerConstrainer.getGenusType(), brokerConstrainer);
        
        try (org.osid.type.TypeList types = brokerConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerConstrainersByRecord.put(types.getNextType(), brokerConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a broker constrainer from this session.
     *
     *  @param brokerConstrainerId the <code>Id</code> of the broker constrainer
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBrokerConstrainer(org.osid.id.Id brokerConstrainerId) {
        org.osid.provisioning.rules.BrokerConstrainer brokerConstrainer;
        try {
            brokerConstrainer = getBrokerConstrainer(brokerConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.brokerConstrainersByGenus.remove(brokerConstrainer.getGenusType());

        try (org.osid.type.TypeList types = brokerConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerConstrainersByRecord.remove(types.getNextType(), brokerConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBrokerConstrainer(brokerConstrainerId);
        return;
    }


    /**
     *  Gets a <code>BrokerConstrainerList</code> corresponding to the given
     *  broker constrainer genus <code>Type</code> which does not include
     *  broker constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known broker constrainers or an error results. Otherwise,
     *  the returned list may contain only those broker constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  brokerConstrainerGenusType a broker constrainer genus type 
     *  @return the returned <code>BrokerConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByGenusType(org.osid.type.Type brokerConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerconstrainer.ArrayBrokerConstrainerList(this.brokerConstrainersByGenus.get(brokerConstrainerGenusType)));
    }


    /**
     *  Gets a <code>BrokerConstrainerList</code> containing the given
     *  broker constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known broker constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  broker constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  brokerConstrainerRecordType a broker constrainer record type 
     *  @return the returned <code>brokerConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByRecordType(org.osid.type.Type brokerConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerconstrainer.ArrayBrokerConstrainerList(this.brokerConstrainersByRecord.get(brokerConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerConstrainersByGenus.clear();
        this.brokerConstrainersByRecord.clear();

        super.close();

        return;
    }
}
