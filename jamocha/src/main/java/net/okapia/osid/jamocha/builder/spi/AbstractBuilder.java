//
// AbstractBuilder.java
//
//     The root builder.
//
//
// Tom Coppeto
// Okapia
// 20 September 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import net.okapia.osid.jamocha.builder.validator.Validation;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines the root builder.
 */

public abstract class AbstractBuilder<T extends AbstractBuilder<? extends T>> {

    private final java.util.EnumSet<Validation> validations = java.util.EnumSet.of(Validation.ALL);


    /**
     *  Adds a validation type. If the existing validations include
     *  ALL or NONE, then the set is cleared before setting the given
     *  validation. If the supplied validation is ALL or NONE, then
     *  the previous validations in the set are cleared.
     *  
     *  @param validation the validation type
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public T validation(Validation validation) {
        nullarg(validation, "validation");

        if (this.validations.contains(Validation.ALL) || 
            this.validations.contains(Validation.NONE)) {
            this.validations.clear();
        }

        if ((validation == Validation.ALL) || (validation == Validation.NONE)) {
            this.validations.clear();
        }

        this.validations.add(validation);
        return (self());
    }


    /**
     *  Gets the validation set.
     *
     *  @return the validation set
     */

    protected java.util.EnumSet<Validation> getValidations() {
        return (this.validations);
    }


    /**
     *  Builds the object.
     *
     *  @return the new object
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>object</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public abstract Object build();


    /**
     *  Gets the object. This method is used to get the raw object for
     *  further updates. Use <code>build()</code> to finalize and
     *  validate construction.
     *
     *  @return the new object
     */

    public abstract Miter getMiter();


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return ObjectBuilder
     */

    protected abstract T self();
}
