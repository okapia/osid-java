//
// AbstractCalendaringBatchProxyManager.java
//
//     An adapter for a CalendaringBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CalendaringBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCalendaringBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.calendaring.batch.CalendaringBatchProxyManager>
    implements org.osid.calendaring.batch.CalendaringBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCalendaringBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCalendaringBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of events is available. 
     *
     *  @return <code> true </code> if an event bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventBatchAdmin() {
        return (getAdapteeManager().supportsEventBatchAdmin());
    }


    /**
     *  Tests if bulk administration of recurring events is available. 
     *
     *  @return <code> true </code> if a recurring event bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventBatchAdmin() {
        return (getAdapteeManager().supportsRecurringEventBatchAdmin());
    }


    /**
     *  Tests if bulk administration of offset events is available. 
     *
     *  @return <code> true </code> if an offset event bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventBatchAdmin() {
        return (getAdapteeManager().supportsOffsetEventBatchAdmin());
    }


    /**
     *  Tests if bulk administration of schedules is available. 
     *
     *  @return <code> true </code> if a schedule bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleBatchAdmin() {
        return (getAdapteeManager().supportsScheduleBatchAdmin());
    }


    /**
     *  Tests if bulk administration of schedule slots is available. 
     *
     *  @return <code> true </code> if an schedule slot bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotBatchAdmin() {
        return (getAdapteeManager().supportsScheduleSlotBatchAdmin());
    }


    /**
     *  Tests if bulk administration of commitments is available. 
     *
     *  @return <code> true </code> if a commitment bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentBatchAdmin() {
        return (getAdapteeManager().supportsCommitmentBatchAdmin());
    }


    /**
     *  Tests if bulk administration of time periods is available. 
     *
     *  @return <code> true </code> if a time period bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodBatchAdmin() {
        return (getAdapteeManager().supportsTimePeriodBatchAdmin());
    }


    /**
     *  Tests if bulk administration of calendars is available. 
     *
     *  @return <code> true </code> if a calendar bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarBatchAdmin() {
        return (getAdapteeManager().supportsCalendarBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk event 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.EventBatchAdminSession getEventBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk event 
     *  administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> EventBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.EventBatchAdminSession getEventBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recurring 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.RecurringEventBatchAdminSession getRecurringEventBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recurring 
     *  event administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.RecurringEventBatchAdminSession getRecurringEventBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecurringEventBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk offset 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.OffsetEventBatchAdminSession getOffsetEventBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk offset 
     *  event administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.OffsetEventBatchAdminSession getOffsetEventBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOffsetEventBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk schedule 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.ScheduleBatchAdminSession getScheduleBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk schedule 
     *  administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.ScheduleBatchAdminSession getScheduleBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk schedule 
     *  slot administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.ScheduleSlotBatchAdminSession getScheduleSlotBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk schedule 
     *  slot administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.ScheduleSlotBatchAdminSession getScheduleSlotBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getScheduleSlotBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  commitment administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.CommitmentBatchAdminSession getCommitmentBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  commitment administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.CommitmentBatchAdminSession getCommitmentBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommitmentBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk time 
     *  period administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.TimePeriodBatchAdminSession getTimePeriodBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk time 
     *  period administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.TimePeriodBatchAdminSession getTimePeriodBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk calendar 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.CalendarBatchAdminSession getCalendarBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
