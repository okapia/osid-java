//
// AbstractResponseSearchOdrer.java
//
//     Defines a ResponseSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.response.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ResponseSearchOrder}.
 */

public abstract class AbstractResponseSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.inquiry.ResponseSearchOrder {

    private final java.util.Collection<org.osid.inquiry.records.ResponseSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by inquiry. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInquiry(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an inquiry search order is available. 
     *
     *  @return <code> true </code> if an inquiry search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquirySearchOrder() {
        return (false);
    }


    /**
     *  Gets the inquiry search order. 
     *
     *  @return the inquiry search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInquirySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchOrder getInquirySearchOrder() {
        throw new org.osid.UnimplementedException("supportsInquirySearchOrder() is false");
    }


    /**
     *  Orders the results by responder resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResponder(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a responder resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the responder resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResponderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResponderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResponderSearchOrder() is false");
    }


    /**
     *  Orders the results by responding agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRespondingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a responding agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRespondingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the responding agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRespondingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getRespondingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRespondingAgentSearchOrder() is false");
    }


    /**
     *  Orders the results by affirmative responses. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAffirmative(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  responseRecordType a response record type 
     *  @return {@code true} if the responseRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code responseRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type responseRecordType) {
        for (org.osid.inquiry.records.ResponseSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  responseRecordType the response record type 
     *  @return the response search order record
     *  @throws org.osid.NullArgumentException
     *          {@code responseRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(responseRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseSearchOrderRecord getResponseSearchOrderRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.ResponseSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this response. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param responseRecord the response search odrer record
     *  @param responseRecordType response record type
     *  @throws org.osid.NullArgumentException
     *          {@code responseRecord} or
     *          {@code responseRecordTyperesponse} is
     *          {@code null}
     */
            
    protected void addResponseRecord(org.osid.inquiry.records.ResponseSearchOrderRecord responseSearchOrderRecord, 
                                     org.osid.type.Type responseRecordType) {

        addRecordType(responseRecordType);
        this.records.add(responseSearchOrderRecord);
        
        return;
    }
}
