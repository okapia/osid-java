//
// AbstractNodeAddressBookHierarchySession.java
//
//     Defines an AddressBook hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an address book hierarchy session for delivering a hierarchy
 *  of address books using the AddressBookNode interface.
 */

public abstract class AbstractNodeAddressBookHierarchySession
    extends net.okapia.osid.jamocha.contact.spi.AbstractAddressBookHierarchySession
    implements org.osid.contact.AddressBookHierarchySession {

    private java.util.Collection<org.osid.contact.AddressBookNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root address book <code> Ids </code> in this hierarchy.
     *
     *  @return the root address book <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootAddressBookIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.contact.addressbooknode.AddressBookNodeToIdList(this.roots));
    }


    /**
     *  Gets the root address books in the address book hierarchy. A node
     *  with no parents is an orphan. While all address book <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root address books 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getRootAddressBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.contact.addressbooknode.AddressBookNodeToAddressBookList(new net.okapia.osid.jamocha.contact.addressbooknode.ArrayAddressBookNodeList(this.roots)));
    }


    /**
     *  Adds a root address book node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootAddressBook(org.osid.contact.AddressBookNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root address book nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootAddressBooks(java.util.Collection<org.osid.contact.AddressBookNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root address book node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootAddressBook(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.contact.AddressBookNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> AddressBook </code> has any parents. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @return <code> true </code> if the address book has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentAddressBooks(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getAddressBookNode(addressBookId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  address book.
     *
     *  @param  id an <code> Id </code> 
     *  @param  addressBookId the <code> Id </code> of an address book 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> addressBookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> addressBookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfAddressBook(org.osid.id.Id id, org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.contact.AddressBookNodeList parents = getAddressBookNode(addressBookId).getParentAddressBookNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextAddressBookNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given address book. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @return the parent <code> Ids </code> of the address book 
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentAddressBookIds(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.contact.addressbook.AddressBookToIdList(getParentAddressBooks(addressBookId)));
    }


    /**
     *  Gets the parents of the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> to query 
     *  @return the parents of the address book 
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getParentAddressBooks(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.contact.addressbooknode.AddressBookNodeToAddressBookList(getAddressBookNode(addressBookId).getParentAddressBookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  address book.
     *
     *  @param  id an <code> Id </code> 
     *  @param  addressBookId the Id of an address book 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> addressBookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfAddressBook(org.osid.id.Id id, org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAddressBook(id, addressBookId)) {
            return (true);
        }

        try (org.osid.contact.AddressBookList parents = getParentAddressBooks(addressBookId)) {
            while (parents.hasNext()) {
                if (isAncestorOfAddressBook(id, parents.getNextAddressBook().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an address book has any children. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @return <code> true </code> if the <code> addressBookId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildAddressBooks(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAddressBookNode(addressBookId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  address book.
     *
     *  @param  id an <code> Id </code> 
     *  @param addressBookId the <code> Id </code> of an 
     *         address book
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> addressBookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> addressBookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfAddressBook(org.osid.id.Id id, org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfAddressBook(addressBookId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  address book.
     *
     *  @param  addressBookId the <code> Id </code> to query 
     *  @return the children of the address book 
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAddressBookIds(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.contact.addressbook.AddressBookToIdList(getChildAddressBooks(addressBookId)));
    }


    /**
     *  Gets the children of the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> to query 
     *  @return the children of the address book 
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getChildAddressBooks(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.contact.addressbooknode.AddressBookNodeToAddressBookList(getAddressBookNode(addressBookId).getChildAddressBookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  address book.
     *
     *  @param  id an <code> Id </code> 
     *  @param addressBookId the <code> Id </code> of an 
     *         address book
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> addressBookId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfAddressBook(org.osid.id.Id id, org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAddressBook(addressBookId, id)) {
            return (true);
        }

        try (org.osid.contact.AddressBookList children = getChildAddressBooks(addressBookId)) {
            while (children.hasNext()) {
                if (isDescendantOfAddressBook(id, children.getNextAddressBook().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  address book.
     *
     *  @param  addressBookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified address book node 
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getAddressBookNodeIds(org.osid.id.Id addressBookId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.contact.addressbooknode.AddressBookNodeToNode(getAddressBookNode(addressBookId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given address book.
     *
     *  @param  addressBookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified address book node 
     *  @throws org.osid.NotFoundException <code> addressBookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> addressBookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookNode getAddressBookNodes(org.osid.id.Id addressBookId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAddressBookNode(addressBookId));
    }


    /**
     *  Closes this <code>AddressBookHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an address book node.
     *
     *  @param addressBookId the id of the address book node
     *  @throws org.osid.NotFoundException <code>addressBookId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>addressBookId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.contact.AddressBookNode getAddressBookNode(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(addressBookId, "address book Id");
        for (org.osid.contact.AddressBookNode addressBook : this.roots) {
            if (addressBook.getId().equals(addressBookId)) {
                return (addressBook);
            }

            org.osid.contact.AddressBookNode r = findAddressBook(addressBook, addressBookId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(addressBookId + " is not found");
    }


    protected org.osid.contact.AddressBookNode findAddressBook(org.osid.contact.AddressBookNode node, 
                                                            org.osid.id.Id addressBookId) 
	throws org.osid.OperationFailedException {

        try (org.osid.contact.AddressBookNodeList children = node.getChildAddressBookNodes()) {
            while (children.hasNext()) {
                org.osid.contact.AddressBookNode addressBook = children.getNextAddressBookNode();
                if (addressBook.getId().equals(addressBookId)) {
                    return (addressBook);
                }
                
                addressBook = findAddressBook(addressBook, addressBookId);
                if (addressBook != null) {
                    return (addressBook);
                }
            }
        }

        return (null);
    }
}
