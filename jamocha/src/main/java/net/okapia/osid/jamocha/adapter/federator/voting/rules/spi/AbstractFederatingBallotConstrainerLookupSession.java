//
// AbstractFederatingBallotConstrainerLookupSession.java
//
//     An abstract federating adapter for a BallotConstrainerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BallotConstrainerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBallotConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.rules.BallotConstrainerLookupSession>
    implements org.osid.voting.rules.BallotConstrainerLookupSession {

    private boolean parallel = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Constructs a new <code>AbstractFederatingBallotConstrainerLookupSession</code>.
     */

    protected AbstractFederatingBallotConstrainerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.rules.BallotConstrainerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>BallotConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBallotConstrainers() {
        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            if (session.canLookupBallotConstrainers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>BallotConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBallotConstrainerView() {
        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            session.useComparativeBallotConstrainerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>BallotConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBallotConstrainerView() {
        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            session.usePlenaryBallotConstrainerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            session.useFederatedPollsView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            session.useIsolatedPollsView();
        }

        return;
    }


    /**
     *  Only active ballot constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBallotConstrainerView() {
        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            session.useActiveBallotConstrainerView();
        }

        return;
    }


    /**
     *  Active and inactive ballot constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotConstrainerView() {
        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            session.useAnyStatusBallotConstrainerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>BallotConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BallotConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>BallotConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @param  ballotConstrainerId <code>Id</code> of the
     *          <code>BallotConstrainer</code>
     *  @return the ballot constrainer
     *  @throws org.osid.NotFoundException <code>ballotConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainer getBallotConstrainer(org.osid.id.Id ballotConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            try {
                return (session.getBallotConstrainer(ballotConstrainerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(ballotConstrainerId + " not found");
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ballotConstrainers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>BallotConstrainers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByIds(org.osid.id.IdList ballotConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.rules.ballotconstrainer.MutableBallotConstrainerList ret = new net.okapia.osid.jamocha.voting.rules.ballotconstrainer.MutableBallotConstrainerList();

        try (org.osid.id.IdList ids = ballotConstrainerIds) {
            while (ids.hasNext()) {
                ret.addBallotConstrainer(getBallotConstrainer(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the
     *  given ballot constrainer genus <code>Type</code> which does
     *  not include ballot constrainers of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.ballotconstrainer.FederatingBallotConstrainerList ret = getBallotConstrainerList();

        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            ret.addBallotConstrainerList(session.getBallotConstrainersByGenusType(ballotConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the
     *  given ballot constrainer genus <code>Type</code> and include
     *  any additional ballot constrainers with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByParentGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.ballotconstrainer.FederatingBallotConstrainerList ret = getBallotConstrainerList();

        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            ret.addBallotConstrainerList(session.getBallotConstrainersByParentGenusType(ballotConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> containing the given
     *  ballot constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known ballot
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, ballot constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  ballot constrainers are returned.
     *
     *  @param  ballotConstrainerRecordType a ballotConstrainer record type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByRecordType(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.ballotconstrainer.FederatingBallotConstrainerList ret = getBallotConstrainerList();

        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            ret.addBallotConstrainerList(session.getBallotConstrainersByRecordType(ballotConstrainerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>BallotConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @return a list of <code>BallotConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.ballotconstrainer.FederatingBallotConstrainerList ret = getBallotConstrainerList();

        for (org.osid.voting.rules.BallotConstrainerLookupSession session : getSessions()) {
            ret.addBallotConstrainerList(session.getBallotConstrainers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.rules.ballotconstrainer.FederatingBallotConstrainerList getBallotConstrainerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.ballotconstrainer.ParallelBallotConstrainerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.ballotconstrainer.CompositeBallotConstrainerList());
        }
    }
}
