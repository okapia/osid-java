//
// MutableIndexedMapProfileItemLookupSession
//
//    Implements a ProfileItem lookup service backed by a collection of
//    profileItems indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile;


/**
 *  Implements a ProfileItem lookup service backed by a collection of
 *  profile items. The profile items are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some profile items may be compatible
 *  with more types than are indicated through these profile item
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of profile items can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProfileItemLookupSession
    extends net.okapia.osid.jamocha.core.profile.spi.AbstractIndexedMapProfileItemLookupSession
    implements org.osid.profile.ProfileItemLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapProfileItemLookupSession} with no profile items.
     *
     *  @param profile the profile
     *  @throws org.osid.NullArgumentException {@code profile}
     *          is {@code null}
     */

      public MutableIndexedMapProfileItemLookupSession(org.osid.profile.Profile profile) {
        setProfile(profile);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProfileItemLookupSession} with a
     *  single profile item.
     *  
     *  @param profile the profile
     *  @param  profileItem a single profileItem
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code profileItem} is {@code null}
     */

    public MutableIndexedMapProfileItemLookupSession(org.osid.profile.Profile profile,
                                                  org.osid.profile.ProfileItem profileItem) {
        this(profile);
        putProfileItem(profileItem);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProfileItemLookupSession} using an
     *  array of profile items.
     *
     *  @param profile the profile
     *  @param  profileItems an array of profile items
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code profileItems} is {@code null}
     */

    public MutableIndexedMapProfileItemLookupSession(org.osid.profile.Profile profile,
                                                  org.osid.profile.ProfileItem[] profileItems) {
        this(profile);
        putProfileItems(profileItems);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProfileItemLookupSession} using a
     *  collection of profile items.
     *
     *  @param profile the profile
     *  @param  profileItems a collection of profile items
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code profileItems} is {@code null}
     */

    public MutableIndexedMapProfileItemLookupSession(org.osid.profile.Profile profile,
                                                  java.util.Collection<? extends org.osid.profile.ProfileItem> profileItems) {

        this(profile);
        putProfileItems(profileItems);
        return;
    }
    

    /**
     *  Makes a {@code ProfileItem} available in this session.
     *
     *  @param  profileItem a profile item
     *  @throws org.osid.NullArgumentException {@code profileItem{@code  is
     *          {@code null}
     */

    @Override
    public void putProfileItem(org.osid.profile.ProfileItem profileItem) {
        super.putProfileItem(profileItem);
        return;
    }


    /**
     *  Makes an array of profile items available in this session.
     *
     *  @param  profileItems an array of profile items
     *  @throws org.osid.NullArgumentException {@code profileItems{@code 
     *          is {@code null}
     */

    @Override
    public void putProfileItems(org.osid.profile.ProfileItem[] profileItems) {
        super.putProfileItems(profileItems);
        return;
    }


    /**
     *  Makes collection of profile items available in this session.
     *
     *  @param  profileItems a collection of profile items
     *  @throws org.osid.NullArgumentException {@code profileItem{@code  is
     *          {@code null}
     */

    @Override
    public void putProfileItems(java.util.Collection<? extends org.osid.profile.ProfileItem> profileItems) {
        super.putProfileItems(profileItems);
        return;
    }


    /**
     *  Removes a ProfileItem from this session.
     *
     *  @param profileItemId the {@code Id} of the profile item
     *  @throws org.osid.NullArgumentException {@code profileItemId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProfileItem(org.osid.id.Id profileItemId) {
        super.removeProfileItem(profileItemId);
        return;
    }    
}
