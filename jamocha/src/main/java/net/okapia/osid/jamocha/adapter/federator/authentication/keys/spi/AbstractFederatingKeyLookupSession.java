//
// AbstractFederatingKeyLookupSession.java
//
//     An abstract federating adapter for a KeyLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  KeyLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingKeyLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authentication.keys.KeyLookupSession>
    implements org.osid.authentication.keys.KeyLookupSession {

    private boolean parallel = false;
    private org.osid.authentication.Agency agency = new net.okapia.osid.jamocha.nil.authentication.agency.UnknownAgency();


    /**
     *  Constructs a new <code>AbstractFederatingKeyLookupSession</code>.
     */

    protected AbstractFederatingKeyLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authentication.keys.KeyLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Agency/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Agency Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.agency.getId());
    }


    /**
     *  Gets the <code>Agency</code> associated with this 
     *  session.
     *
     *  @return the <code>Agency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.agency);
    }


    /**
     *  Sets the <code>Agency</code>.
     *
     *  @param  agency the agency for this session
     *  @throws org.osid.NullArgumentException <code>agency</code>
     *          is <code>null</code>
     */

    protected void setAgency(org.osid.authentication.Agency agency) {
        nullarg(agency, "agency");
        this.agency = agency;
        return;
    }


    /**
     *  Tests if this user can perform <code>Key</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupKeys() {
        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            if (session.canLookupKeys()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Key</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeKeyView() {
        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            session.useComparativeKeyView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Key</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryKeyView() {
        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            session.usePlenaryKeyView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include keys in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            session.useFederatedAgencyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            session.useIsolatedAgencyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Key</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Key</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Key</code> and
     *  retained for compatibility.
     *
     *  @param  keyId <code>Id</code> of the
     *          <code>Key</code>
     *  @return the key
     *  @throws org.osid.NotFoundException <code>keyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>keyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKey(org.osid.id.Id keyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            try {
                return (session.getKey(keyId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(keyId + " not found");
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  keys specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Keys</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  keyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>keyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByIds(org.osid.id.IdList keyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authentication.keys.key.MutableKeyList ret = new net.okapia.osid.jamocha.authentication.keys.key.MutableKeyList();

        try (org.osid.id.IdList ids = keyIds) {
            while (ids.hasNext()) {
                ret.addKey(getKey(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  key genus <code>Type</code> which does not include
     *  keys of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyGenusType a key genus type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.keys.key.FederatingKeyList ret = getKeyList();

        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            ret.addKeyList(session.getKeysByGenusType(keyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  key genus <code>Type</code> and include any additional
     *  keys with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyGenusType a key genus type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByParentGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.keys.key.FederatingKeyList ret = getKeyList();

        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            ret.addKeyList(session.getKeysByParentGenusType(keyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>KeyList</code> containing the given
     *  key record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyRecordType a key record type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByRecordType(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.keys.key.FederatingKeyList ret = getKeyList();

        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            ret.addKeyList(session.getKeysByRecordType(keyRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the agent key.
     *
     *  @param  agentId the <code> Id </code> of the <code> Agent </code>
     *  @return the key
     *  @throws org.osid.NotFoundException <code> agentId </code> is not found
     *          or no key exists
     *  @throws org.osid.NullArgumentException <code> agentId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKeyForAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            try {
                return (session.getKeyForAgent(agentId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(agentId + " not found");
    }


    /**
     *  Gets all <code>Keys</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Keys</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeys()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.keys.key.FederatingKeyList ret = getKeyList();

        for (org.osid.authentication.keys.KeyLookupSession session : getSessions()) {
            ret.addKeyList(session.getKeys());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authentication.keys.key.FederatingKeyList getKeyList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authentication.keys.key.ParallelKeyList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authentication.keys.key.CompositeKeyList());
        }
    }
}
