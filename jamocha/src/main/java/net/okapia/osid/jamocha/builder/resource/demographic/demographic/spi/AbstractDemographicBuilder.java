//
// AbstractDemographic.java
//
//     Defines a Demographic builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.demographic.demographic.spi;


/**
 *  Defines a <code>Demographic</code> builder.
 */

public abstract class AbstractDemographicBuilder<T extends AbstractDemographicBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resource.demographic.demographic.DemographicMiter demographic;


    /**
     *  Constructs a new <code>AbstractDemographicBuilder</code>.
     *
     *  @param demographic the demographic to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDemographicBuilder(net.okapia.osid.jamocha.builder.resource.demographic.demographic.DemographicMiter demographic) {
        super(demographic);
        this.demographic = demographic;
        return;
    }


    /**
     *  Builds the demographic.
     *
     *  @return the new demographic
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resource.demographic.Demographic build() {
        (new net.okapia.osid.jamocha.builder.validator.resource.demographic.demographic.DemographicValidator(getValidations())).validate(this.demographic);
        return (new net.okapia.osid.jamocha.builder.resource.demographic.demographic.ImmutableDemographic(this.demographic));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the demographic miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resource.demographic.demographic.DemographicMiter getMiter() {
        return (this.demographic);
    }


    /**
     *  Adds an included demographic.
     *
     *  @param demographic an included demographic
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public T includedDemographic(org.osid.resource.demographic.Demographic demographic) {
        getMiter().addIncludedDemographic(demographic);
        return (self());
    }


    /**
     *  Sets all the included demographics.
     *
     *  @param demographics a collection of included demographics
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public T includedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        getMiter().setIncludedDemographics(demographics);
        return (self());
    }


    /**
     *  Adds an included intersecting demographic.
     *
     *  @param demographic an included intersecting demographic
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public T includedIntersectingDemographic(org.osid.resource.demographic.Demographic demographic) {
        getMiter().addIncludedIntersectingDemographic(demographic);
        return (self());
    }


    /**
     *  Sets all the included intersecting demographics.
     *
     *  @param demographics a collection of included intersecting demographics
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public T includedIntersectingDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        getMiter().setIncludedIntersectingDemographics(demographics);
        return (self());
    }


    /**
     *  Adds an included exclusive demographic.
     *
     *  @param demographic an included exclusive demographic
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public T includedExclusiveDemographic(org.osid.resource.demographic.Demographic demographic) {
        getMiter().addIncludedExclusiveDemographic(demographic);
        return (self());
    }


    /**
     *  Sets all the included exclusive demographics.
     *
     *  @param demographics a collection of included exclusive demographics
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public T includedExclusiveDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        getMiter().setIncludedExclusiveDemographics(demographics);
        return (self());
    }


    /**
     *  Adds an excluded demographic.
     *
     *  @param demographic an excluded demographic
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public T excludedDemographic(org.osid.resource.demographic.Demographic demographic) {
        getMiter().addExcludedDemographic(demographic);
        return (self());
    }


    /**
     *  Sets all the excluded demographics.
     *
     *  @param demographics a collection of excluded demographics
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public T excludedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        getMiter().setExcludedDemographics(demographics);
        return (self());
    }


    /**
     *  Adds an included resource.
     *
     *  @param resource an included resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T includedResource(org.osid.resource.Resource resource) {
        getMiter().addIncludedResource(resource);
        return (self());
    }


    /**
     *  Sets all the included resources.
     *
     *  @param resources a collection of included resources
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    public T includedResources(java.util.Collection<org.osid.resource.Resource> resources) {
        getMiter().setIncludedResources(resources);
        return (self());
    }


    /**
     *  Adds an excluded resource.
     *
     *  @param resource an excluded resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T excludedResource(org.osid.resource.Resource resource) {
        getMiter().addExcludedResource(resource);
        return (self());
    }


    /**
     *  Sets all the excluded resources.
     *
     *  @param resources a collection of excluded resources
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    public T excludedResources(java.util.Collection<org.osid.resource.Resource> resources) {
        getMiter().setExcludedResources(resources);
        return (self());
    }


    /**
     *  Adds a Demographic record.
     *
     *  @param record a demographic record
     *  @param recordType the type of demographic record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resource.demographic.records.DemographicRecord record, org.osid.type.Type recordType) {
        getMiter().addDemographicRecord(record, recordType);
        return (self());
    }
}       


