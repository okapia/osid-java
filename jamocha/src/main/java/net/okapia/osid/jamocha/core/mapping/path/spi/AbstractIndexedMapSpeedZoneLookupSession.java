//
// AbstractIndexedMapSpeedZoneLookupSession.java
//
//    A simple framework for providing a SpeedZone lookup service
//    backed by a fixed collection of speed zones with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SpeedZone lookup service backed by a
 *  fixed collection of speed zones. The speed zones are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some speed zones may be compatible
 *  with more types than are indicated through these speed zone
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SpeedZones</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSpeedZoneLookupSession
    extends AbstractMapSpeedZoneLookupSession
    implements org.osid.mapping.path.SpeedZoneLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.SpeedZone> speedZonesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.SpeedZone>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.SpeedZone> speedZonesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.SpeedZone>());


    /**
     *  Makes a <code>SpeedZone</code> available in this session.
     *
     *  @param  speedZone a speed zone
     *  @throws org.osid.NullArgumentException <code>speedZone<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSpeedZone(org.osid.mapping.path.SpeedZone speedZone) {
        super.putSpeedZone(speedZone);

        this.speedZonesByGenus.put(speedZone.getGenusType(), speedZone);
        
        try (org.osid.type.TypeList types = speedZone.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.speedZonesByRecord.put(types.getNextType(), speedZone);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a speed zone from this session.
     *
     *  @param speedZoneId the <code>Id</code> of the speed zone
     *  @throws org.osid.NullArgumentException <code>speedZoneId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSpeedZone(org.osid.id.Id speedZoneId) {
        org.osid.mapping.path.SpeedZone speedZone;
        try {
            speedZone = getSpeedZone(speedZoneId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.speedZonesByGenus.remove(speedZone.getGenusType());

        try (org.osid.type.TypeList types = speedZone.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.speedZonesByRecord.remove(types.getNextType(), speedZone);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSpeedZone(speedZoneId);
        return;
    }


    /**
     *  Gets a <code>SpeedZoneList</code> corresponding to the given
     *  speed zone genus <code>Type</code> which does not include
     *  speed zones of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known speed zones or an error results. Otherwise,
     *  the returned list may contain only those speed zones that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  speedZoneGenusType a speed zone genus type 
     *  @return the returned <code>SpeedZone</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByGenusType(org.osid.type.Type speedZoneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.speedzone.ArraySpeedZoneList(this.speedZonesByGenus.get(speedZoneGenusType)));
    }


    /**
     *  Gets a <code>SpeedZoneList</code> containing the given
     *  speed zone record <code>Type</code>. In plenary mode, the
     *  returned list contains all known speed zones or an error
     *  results. Otherwise, the returned list may contain only those
     *  speed zones that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  speedZoneRecordType a speed zone record type 
     *  @return the returned <code>speedZone</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZonesByRecordType(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.speedzone.ArraySpeedZoneList(this.speedZonesByRecord.get(speedZoneRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.speedZonesByGenus.clear();
        this.speedZonesByRecord.clear();

        super.close();

        return;
    }
}
