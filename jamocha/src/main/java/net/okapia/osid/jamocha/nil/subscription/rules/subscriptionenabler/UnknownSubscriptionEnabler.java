//
// UnknownSubscriptionEnabler.java
//
//     Defines an unknown SubscriptionEnabler.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.subscription.rules.subscriptionenabler;


/**
 *  Defines an unknown <code>SubscriptionEnabler</code>.
 */

public final class UnknownSubscriptionEnabler
    extends net.okapia.osid.jamocha.nil.subscription.rules.subscriptionenabler.spi.AbstractUnknownSubscriptionEnabler
    implements org.osid.subscription.rules.SubscriptionEnabler {


    /**
     *  Constructs a new <code>UnknownSubscriptionEnabler</code>.
     */

    public UnknownSubscriptionEnabler() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownSubscriptionEnabler</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownSubscriptionEnabler(boolean optional) {
        super(optional);
        addSubscriptionEnablerRecord(new SubscriptionEnablerRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown SubscriptionEnabler.
     *
     *  @return an unknown SubscriptionEnabler
     */

    public static org.osid.subscription.rules.SubscriptionEnabler create() {
        return (net.okapia.osid.jamocha.builder.validator.subscription.rules.subscriptionenabler.SubscriptionEnablerValidator.validateSubscriptionEnabler(new UnknownSubscriptionEnabler()));
    }


    public class SubscriptionEnablerRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.subscription.rules.records.SubscriptionEnablerRecord {

        
        protected SubscriptionEnablerRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
