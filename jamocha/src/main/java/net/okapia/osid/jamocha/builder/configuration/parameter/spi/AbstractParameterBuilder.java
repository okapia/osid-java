//
// AbstractParameter.java
//
//     Defines a Parameter builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.parameter.spi;


/**
 *  Defines a <code>Parameter</code> builder.
 */

public abstract class AbstractParameterBuilder<T extends AbstractParameterBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.configuration.parameter.ParameterMiter parameter;


    /**
     *  Constructs a new <code>AbstractParameterBuilder</code>.
     *
     *  @param parameter the parameter to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractParameterBuilder(net.okapia.osid.jamocha.builder.configuration.parameter.ParameterMiter parameter) {
        super(parameter);
        this.parameter = parameter;
        return;
    }


    /**
     *  Builds the parameter.
     *
     *  @return the new parameter
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.configuration.Parameter build() {
        (new net.okapia.osid.jamocha.builder.validator.configuration.parameter.ParameterValidator(getValidations())).validate(this.parameter);
        return (new net.okapia.osid.jamocha.builder.configuration.parameter.ImmutableParameter(this.parameter));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the parameter miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.configuration.parameter.ParameterMiter getMiter() {
        return (this.parameter);
    }


    /**
     *  Sets the value syntax.
     *
     *  @param syntax a value syntax
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>syntax</code> is
     *          <code>null</code>
     */

    public T valueSyntax(org.osid.Syntax syntax) {
        getMiter().setValueSyntax(syntax);
        return (self());
    }


    /**
     *  Sets the value coordinate type.
     *
     *  @param coordinateType a value coordinate type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code> is <code>null</code>
     */

    public T valueCoordinateType(org.osid.type.Type coordinateType) {
        getMiter().setValueCoordinateType(coordinateType);
        return (self());
    }


    /**
     *  Sets the value heading type.
     *
     *  @param headingType a value heading type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>headingType</code> is <code>null</code>
     */

    public T valueHeadingType(org.osid.type.Type headingType) {
        getMiter().setValueHeadingType(headingType);
        return (self());
    }


    /**
     *  Sets the value spatial unit record type.
     *
     *  @param spatialUnitRecordType a value spatial unit record type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnitRecordType</code> is
     *          <code>null</code>
     */

    public T valueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        getMiter().setValueSpatialUnitRecordType(spatialUnitRecordType);
        return (self());
    }


    /**
     *  Sets the value object type.
     *
     *  @param objectType a value object type
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objectType</code>
     *          is <code>null</code>
     */

    public T valueObjectType(org.osid.type.Type objectType) {
        getMiter().setValueObjectType(objectType);
        return (self());
    }


    /**
     *  Sets the version scheme type.
     *
     *  @param versionType a value version type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>versionType</code> is <code>null</code>
     */

    public T valueVersionScheme(org.osid.type.Type versionType) {
        getMiter().setValueVersionScheme(versionType);
        return (self());
    }


    /**
     *  Sets the shuffled flag.
     *
     *  @return the builder
     */

    public T shuffleValues() {
        getMiter().setShuffled(true);
        return (self());
    }


    /**
     *  Unsets the shuffled flag.
     *
     *  @return the builder
     */

    public T unShuffleValues() {
        getMiter().setShuffled(false);
        return (self());
    }


    /**
     *  Adds a Parameter record.
     *
     *  @param record a parameter record
     *  @param recordType the type of parameter record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.configuration.records.ParameterRecord record, org.osid.type.Type recordType) {
        getMiter().addParameterRecord(record, recordType);
        return (self());
    }
}       


