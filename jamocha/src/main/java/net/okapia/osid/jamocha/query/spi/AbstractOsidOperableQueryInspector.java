//
// AbstractOsidOperableQueryInspector.java
//
//     Defines an OsidOperableQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidOperableQueryInspector
    extends AbstractOsidQueryInspector
    implements org.osid.OsidOperableQueryInspector {
    
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> activeTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> enabledTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> disabledTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> operationalTerms = new java.util.LinkedHashSet<>();

    /**
     *  Gets the active query terms.
     *
     *  @return the active terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getActiveTerms() {
        return (this.activeTerms.toArray(new org.osid.search.terms.BooleanTerm[this.activeTerms.size()]));
    }


    /**
     *  Adds an active term.
     *
     *  @param term an active term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addActiveTerm(org.osid.search.terms.BooleanTerm term) {
        nullarg(term, "active term");
        this.activeTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of active terms.
     *
     *  @param terms a collection of active terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addActiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        nullarg(terms, "active terms");
        this.activeTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an active term.
     *
     *  @param active the active flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addActiveTerm(boolean active, boolean match) {
        this.activeTerms.add(new net.okapia.osid.primordium.terms.BooleanTerm(active, match));
        return;
    }


    /**
     *  Gets the enabled query terms.
     *
     *  @return the enabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (this.enabledTerms.toArray(new org.osid.search.terms.BooleanTerm[this.enabledTerms.size()]));
    }


    /**
     *  Adds an enabled term.
     *
     *  @param term an enabled term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEnabledTerm(org.osid.search.terms.BooleanTerm term) {
        nullarg(term, "enabled term");
        this.enabledTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of enabled terms.
     *
     *  @param terms a collection of enabled terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addEnabledTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        nullarg(terms, "enabled terms");
        this.enabledTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an enabled term.
     *
     *  @param enabled the enabled flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addEnabledTerm(boolean enabled, boolean match) {
        this.enabledTerms.add(new net.okapia.osid.primordium.terms.BooleanTerm(enabled, match));
        return;
    }


    /**
     *  Gets the disabled query terms.
     *
     *  @return the disabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDisabledTerms() {
        return (this.disabledTerms.toArray(new org.osid.search.terms.BooleanTerm[this.disabledTerms.size()]));
    }


    /**
     *  Adds a disabled term.
     *
     *  @param term a disabled term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDisabledTerm(org.osid.search.terms.BooleanTerm term) {
        nullarg(term, "disabled term");
        this.disabledTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of disabled terms.
     *
     *  @param terms a collection of disabled terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDisabledTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        nullarg(terms, "disabled terms");
        this.disabledTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a disabled term.
     *
     *  @param disabled the disabled flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addDisabledTerm(boolean disabled, boolean match) {
        this.disabledTerms.add(new net.okapia.osid.primordium.terms.BooleanTerm(disabled, match));
        return;
    }


    /**
     *  Gets the operational query terms.
     *
     *  @return the operational terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOperationalTerms() {
        return (this.operationalTerms.toArray(new org.osid.search.terms.BooleanTerm[this.operationalTerms.size()]));
    }


    /**
     *  Adds an operational term.
     *
     *  @param term an operational term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addOperationalTerm(org.osid.search.terms.BooleanTerm term) {
        nullarg(term, "operational term");
        this.operationalTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of operational terms.
     *
     *  @param terms a collection of operational terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addOperationalTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        nullarg(terms, "operational terms");
        this.operationalTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an operational term.
     *
     *  @param operational the operational flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addOperationalTerm(boolean operational, boolean match) {
        this.operationalTerms.add(new net.okapia.osid.primordium.terms.BooleanTerm(operational, match));
        return;
    }
}
