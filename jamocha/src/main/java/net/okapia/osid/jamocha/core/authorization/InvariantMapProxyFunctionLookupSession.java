//
// InvariantMapProxyFunctionLookupSession
//
//    Implements a Function lookup service backed by a fixed
//    collection of functions. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements a Function lookup service backed by a fixed
 *  collection of functions. The functions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyFunctionLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractMapFunctionLookupSession
    implements org.osid.authorization.FunctionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFunctionLookupSession} with no
     *  functions.
     *
     *  @param vault the vault
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.proxy.Proxy proxy) {
        setVault(vault);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyFunctionLookupSession} with a single
     *  function.
     *
     *  @param vault the vault
     *  @param function a single function
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code function} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.authorization.Function function, org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putFunction(function);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyFunctionLookupSession} using
     *  an array of functions.
     *
     *  @param vault the vault
     *  @param functions an array of functions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code functions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.authorization.Function[] functions, org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putFunctions(functions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFunctionLookupSession} using a
     *  collection of functions.
     *
     *  @param vault the vault
     *  @param functions a collection of functions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code functions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                  java.util.Collection<? extends org.osid.authorization.Function> functions,
                                                  org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putFunctions(functions);
        return;
    }
}
