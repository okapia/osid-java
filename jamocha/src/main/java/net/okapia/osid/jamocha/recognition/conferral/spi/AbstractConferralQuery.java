//
// AbstractConferralQuery.java
//
//     A template for making a Conferral Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for conferrals.
 */

public abstract class AbstractConferralQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.recognition.ConferralQuery {

    private final java.util.Collection<org.osid.recognition.records.ConferralQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets an award <code> Id. </code> 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        return;
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        return;
    }


    /**
     *  Sets a reference <code> Id. </code> 
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> referenceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id referenceId, boolean match) {
        return;
    }


    /**
     *  Matches any reference. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyReferenceId(boolean match) {
        return;
    }


    /**
     *  Clears the reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        return;
    }


    /**
     *  Sets a convocaton <code> Id. </code> 
     *
     *  @param  convocationId a convocaton <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> convocationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConvocationId(org.osid.id.Id convocationId, boolean match) {
        return;
    }


    /**
     *  Clears the convocaton <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConvocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConvocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a convocaton query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a convocaton query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the convocaton query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuery getConvocationQuery() {
        throw new org.osid.UnimplementedException("supportsConvocationQuery() is false");
    }


    /**
     *  Matches any convocaton. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyConvocation(boolean match) {
        return;
    }


    /**
     *  Clears the convocaton terms. 
     */

    @OSID @Override
    public void clearConvocationTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match conferrals 
     *  assigned to academies. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAcademyId(org.osid.id.Id academyId, boolean match) {
        return;
    }


    /**
     *  Clears the academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAcademyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAcademyQuery() is false");
    }


    /**
     *  Clears the academy terms. 
     */

    @OSID @Override
    public void clearAcademyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given conferral query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a conferral implementing the requested record.
     *
     *  @param conferralRecordType a conferral record type
     *  @return the conferral query record
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(conferralRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralQueryRecord getConferralQueryRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConferralQueryRecord record : this.records) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralRecordType + " is not supported");
    }


    /**
     *  Adds a record to this conferral query. 
     *
     *  @param conferralQueryRecord conferral query record
     *  @param conferralRecordType conferral record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConferralQueryRecord(org.osid.recognition.records.ConferralQueryRecord conferralQueryRecord, 
                                          org.osid.type.Type conferralRecordType) {

        addRecordType(conferralRecordType);
        nullarg(conferralQueryRecord, "conferral query record");
        this.records.add(conferralQueryRecord);        
        return;
    }
}
