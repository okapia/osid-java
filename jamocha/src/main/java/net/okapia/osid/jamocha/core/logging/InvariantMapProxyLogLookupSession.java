//
// InvariantMapProxyLogLookupSession
//
//    Implements a Log lookup service backed by a fixed
//    collection of logs. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.logging;


/**
 *  Implements a Log lookup service backed by a fixed
 *  collection of logs. The logs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyLogLookupSession
    extends net.okapia.osid.jamocha.core.logging.spi.AbstractMapLogLookupSession
    implements org.osid.logging.LogLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyLogLookupSession} with no
     *  logs.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyLogLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyLogLookupSession} with a
     *  single log.
     *
     *  @param log a single log
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code log} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyLogLookupSession(org.osid.logging.Log log, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putLog(log);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyLogLookupSession} using
     *  an array of logs.
     *
     *  @param logs an array of logs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code logs} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyLogLookupSession(org.osid.logging.Log[] logs, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putLogs(logs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyLogLookupSession} using a
     *  collection of logs.
     *
     *  @param logs a collection of logs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code logs} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyLogLookupSession(java.util.Collection<? extends org.osid.logging.Log> logs,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putLogs(logs);
        return;
    }
}
