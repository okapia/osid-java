//
// AbstractAdapterOublietteLookupSession.java
//
//    An Oubliette lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Oubliette lookup session adapter.
 */

public abstract class AbstractAdapterOublietteLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hold.OublietteLookupSession {

    private final org.osid.hold.OublietteLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOublietteLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOublietteLookupSession(org.osid.hold.OublietteLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Oubliette} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOubliettes() {
        return (this.session.canLookupOubliettes());
    }


    /**
     *  A complete view of the {@code Oubliette} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOublietteView() {
        this.session.useComparativeOublietteView();
        return;
    }


    /**
     *  A complete view of the {@code Oubliette} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOublietteView() {
        this.session.usePlenaryOublietteView();
        return;
    }

     
    /**
     *  Gets the {@code Oubliette} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Oubliette} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Oubliette} and
     *  retained for compatibility.
     *
     *  @param oublietteId {@code Id} of the {@code Oubliette}
     *  @return the oubliette
     *  @throws org.osid.NotFoundException {@code oublietteId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code oublietteId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOubliette(oublietteId));
    }


    /**
     *  Gets an {@code OublietteList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  oubliettes specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Oubliettes} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  oublietteIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Oubliette} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code oublietteIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByIds(org.osid.id.IdList oublietteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOubliettesByIds(oublietteIds));
    }


    /**
     *  Gets an {@code OublietteList} corresponding to the given
     *  oubliette genus {@code Type} which does not include
     *  oubliettes of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  oublietteGenusType an oubliette genus type 
     *  @return the returned {@code Oubliette} list
     *  @throws org.osid.NullArgumentException
     *          {@code oublietteGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByGenusType(org.osid.type.Type oublietteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOubliettesByGenusType(oublietteGenusType));
    }


    /**
     *  Gets an {@code OublietteList} corresponding to the given
     *  oubliette genus {@code Type} and include any additional
     *  oubliettes with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  oublietteGenusType an oubliette genus type 
     *  @return the returned {@code Oubliette} list
     *  @throws org.osid.NullArgumentException
     *          {@code oublietteGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByParentGenusType(org.osid.type.Type oublietteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOubliettesByParentGenusType(oublietteGenusType));
    }


    /**
     *  Gets an {@code OublietteList} containing the given
     *  oubliette record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  oublietteRecordType an oubliette record type 
     *  @return the returned {@code Oubliette} list
     *  @throws org.osid.NullArgumentException
     *          {@code oublietteRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByRecordType(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOubliettesByRecordType(oublietteRecordType));
    }


    /**
     *  Gets an {@code OublietteList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Oubliette} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOubliettesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Oubliettes}. 
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Oubliettes} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOubliettes());
    }
}
