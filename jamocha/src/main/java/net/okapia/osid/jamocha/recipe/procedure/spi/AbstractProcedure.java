//
// AbstractProcedure.java
//
//     Defines a Procedure.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.procedure.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Procedure</code>.
 */

public abstract class AbstractProcedure
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.recipe.Procedure {

    private final java.util.Collection<org.osid.learning.Objective> learningObjectives = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.recipe.records.ProcedureRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Ids </code> of the learning objectives required to 
     *  execute this procedure. 
     *
     *  @return the objective <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        try {
            org.osid.learning.ObjectiveList learningObjectives = getLearningObjectives();
            return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(learningObjectives));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the learning objectives required to execute this procedure. 
     *
     *  @return the objectives 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.learning.objective.ArrayObjectiveList(this.learningObjectives));
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException
     *          <code>objective</code> is <code>null</code>
     */

    protected void addLearningObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "learning objective");
        this.learningObjectives.add(objective);
        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    protected void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        nullarg(objectives, "learning objectives");
        this.learningObjectives.clear();
        this.learningObjectives.addAll(objectives);
        return;
    }


    /**
     *  Tests if this procedure supports the given record
     *  <code>Type</code>.
     *
     *  @param  procedureRecordType a procedure record type 
     *  @return <code>true</code> if the procedureRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type procedureRecordType) {
        for (org.osid.recipe.records.ProcedureRecord record : this.records) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Procedure</code> record <code>Type</code>.
     *
     *  @param  procedureRecordType the procedure record type 
     *  @return the procedure record 
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(procedureRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureRecord getProcedureRecord(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.ProcedureRecord record : this.records) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(procedureRecordType + " is not supported");
    }


    /**
     *  Adds a record to this procedure. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param procedureRecord the procedure record
     *  @param procedureRecordType procedure record type
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecord</code> or
     *          <code>procedureRecordTypeprocedure</code> is
     *          <code>null</code>
     */
            
    protected void addProcedureRecord(org.osid.recipe.records.ProcedureRecord procedureRecord, 
                                      org.osid.type.Type procedureRecordType) {

        nullarg(procedureRecord, "procedure record");
        addRecordType(procedureRecordType);
        this.records.add(procedureRecord);
        
        return;
    }
}
