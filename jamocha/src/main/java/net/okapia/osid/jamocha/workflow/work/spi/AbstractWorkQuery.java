//
// AbstractWorkQuery.java
//
//     A template for making a Work Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.work.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for works.
 */

public abstract class AbstractWorkQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.workflow.WorkQuery {

    private final java.util.Collection<org.osid.workflow.records.WorkQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches suspended work. 
     *
     *  @param match <code> true </code> to match suspended work, <code> false 
     *               </code> otherwise 
     */

    @OSID @Override
    public void matchSuspended(boolean match) {
        return;
    }


    /**
     *  Clears the suspended query terms. 
     */

    @OSID @Override
    public void clearSuspendedTerms() {
        return;
    }


    /**
     *  Sets the process <code> Id </code> for this query. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        return;
    }


    /**
     *  Clears the process <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        return;
    }


    /**
     *  Sets the step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStepId(org.osid.id.Id stepId, boolean match) {
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStepIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> supportsStepyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getStepQuery() {
        throw new org.osid.UnimplementedException("supportsStepQuery() is false");
    }


    /**
     *  Matches processes that have any step. 
     *
     *  @param  match <code> true </code> to match processes with any step, 
     *          <code> false </code> to match processes with no step 
     */

    @OSID @Override
    public void matchAnyStep(boolean match) {
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearStepTerms() {
        return;
    }


    /**
     *  Sets the office <code> Id </code> for this query to match works 
     *  assigned to offices. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given work query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a work implementing the requested record.
     *
     *  @param workRecordType a work record type
     *  @return the work query record
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.WorkQueryRecord getWorkQueryRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.WorkQueryRecord record : this.records) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Adds a record to this work query. 
     *
     *  @param workQueryRecord work query record
     *  @param workRecordType work record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addWorkQueryRecord(org.osid.workflow.records.WorkQueryRecord workQueryRecord, 
                                          org.osid.type.Type workRecordType) {

        addRecordType(workRecordType);
        nullarg(workQueryRecord, "work query record");
        this.records.add(workQueryRecord);        
        return;
    }
}
