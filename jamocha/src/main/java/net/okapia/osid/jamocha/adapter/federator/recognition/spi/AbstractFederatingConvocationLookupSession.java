//
// AbstractFederatingConvocationLookupSession.java
//
//     An abstract federating adapter for a ConvocationLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ConvocationLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingConvocationLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.recognition.ConvocationLookupSession>
    implements org.osid.recognition.ConvocationLookupSession {

    private boolean parallel = false;
    private org.osid.recognition.Academy academy = new net.okapia.osid.jamocha.nil.recognition.academy.UnknownAcademy();


    /**
     *  Constructs a new <code>AbstractFederatingConvocationLookupSession</code>.
     */

    protected AbstractFederatingConvocationLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.recognition.ConvocationLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Academy/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.academy.getId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this 
     *  session.
     *
     *  @return the <code>Academy</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.academy);
    }


    /**
     *  Sets the <code>Academy</code>.
     *
     *  @param  academy the academy for this session
     *  @throws org.osid.NullArgumentException <code>academy</code>
     *          is <code>null</code>
     */

    protected void setAcademy(org.osid.recognition.Academy academy) {
        nullarg(academy, "academy");
        this.academy = academy;
        return;
    }


    /**
     *  Tests if this user can perform <code>Convocation</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupConvocations() {
        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            if (session.canLookupConvocations()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Convocation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeConvocationView() {
        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            session.useComparativeConvocationView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Convocation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryConvocationView() {
        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            session.usePlenaryConvocationView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include convocations in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            session.useFederatedAcademyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            session.useIsolatedAcademyView();
        }

        return;
    }


    /**
     *  Only active convocations are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveConvocationView() {
        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            session.useActiveConvocationView();
        }

        return;
    }


    /**
     *  Active and inactive convocations are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusConvocationView() {
        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            session.useAnyStatusConvocationView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Convocation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Convocation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Convocation</code> and
     *  retained for compatibility.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationId <code>Id</code> of the
     *          <code>Convocation</code>
     *  @return the convocation
     *  @throws org.osid.NotFoundException <code>convocationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>convocationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Convocation getConvocation(org.osid.id.Id convocationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            try {
                return (session.getConvocation(convocationId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(convocationId + " not found");
    }


    /**
     *  Gets a <code>ConvocationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  convocations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Convocations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>convocationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByIds(org.osid.id.IdList convocationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.recognition.convocation.MutableConvocationList ret = new net.okapia.osid.jamocha.recognition.convocation.MutableConvocationList();

        try (org.osid.id.IdList ids = convocationIds) {
            while (ids.hasNext()) {
                ret.addConvocation(getConvocation(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ConvocationList</code> corresponding to the given
     *  convocation genus <code>Type</code> which does not include
     *  convocations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationGenusType a convocation genus type 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByGenusType(org.osid.type.Type convocationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocationsByGenusType(convocationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ConvocationList</code> corresponding to the given
     *  convocation genus <code>Type</code> and include any additional
     *  convocations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationGenusType a convocation genus type 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByParentGenusType(org.osid.type.Type convocationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocationsByParentGenusType(convocationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ConvocationList</code> containing the given
     *  convocation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  convocationRecordType a convocation record type 
     *  @return the returned <code>Convocation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByRecordType(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocationsByRecordType(convocationRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ConvocationList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known convocations or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  convocations that are accessible through this session. 
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Convocation</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocationsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of convocations with a date within the given date
     *  range inclusive. In plenary mode, the returned list contains
     *  all known convocations or an error results. Otherwise, the
     *  returned list may contain only those convocations that are
     *  accessible through this session.
     *
     *  @param  from the starting date
     *  @param  to the ending date
     *  @return the returned <code> ConvocationList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByDate(org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocationsByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all convocations corresponding to an award
     *  <code>Id</code>. In plenary mode, the returned list contains
     *  all known convocations or an error results. Otherwise, the
     *  returned list may contain only those convocations that are
     *  accessible through this session.
     *
     *  @param  awardId the <code> Id </code> of the award
     *  @return the returned <code> ConvocationList </code>
     *  @throws org.osid.NullArgumentException <code> awardId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByAward(org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocationsByAward(awardId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a list of all convocations corresponding to a time period
     *  <code>Id</code>. In plenary mode, the returned list contains
     *  all known convocations or an error results. Otherwise, the
     *  returned list may contain only those convocations that are
     *  accessible through this session.
     *
     *  @param  timePeriodId the <code> Id </code> of the time period
     *  @return the returned <code> ConvocationList </code>
     *  @throws org.osid.NullArgumentException <code> timePeriod </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocationsByTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocationsByTimePeriod(timePeriodId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Convocations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  convocations or an error results. Otherwise, the returned list
     *  may contain only those convocations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, convocations are returned that are currently
     *  active. In any status mode, active and inactive convocations
     *  are returned.
     *
     *  @return a list of <code>Convocations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList ret = getConvocationList();

        for (org.osid.recognition.ConvocationLookupSession session : getSessions()) {
            ret.addConvocationList(session.getConvocations());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.recognition.convocation.FederatingConvocationList getConvocationList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.convocation.ParallelConvocationList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.convocation.CompositeConvocationList());
        }
    }
}
