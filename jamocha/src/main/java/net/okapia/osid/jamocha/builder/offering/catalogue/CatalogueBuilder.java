//
// Catalogue.java
//
//     Defines a Catalogue builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.catalogue;


/**
 *  Defines a <code>Catalogue</code> builder.
 */

public final class CatalogueBuilder
    extends net.okapia.osid.jamocha.builder.offering.catalogue.spi.AbstractCatalogueBuilder<CatalogueBuilder> {
    

    /**
     *  Constructs a new <code>CatalogueBuilder</code> using a
     *  <code>MutableCatalogue</code>.
     */

    public CatalogueBuilder() {
        super(new MutableCatalogue());
        return;
    }


    /**
     *  Constructs a new <code>CatalogueBuilder</code> using the given
     *  mutable catalogue.
     * 
     *  @param catalogue
     */

    public CatalogueBuilder(CatalogueMiter catalogue) {
        super(catalogue);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return CatalogueBuilder
     */

    @Override
    protected CatalogueBuilder self() {
        return (this);
    }
}       


