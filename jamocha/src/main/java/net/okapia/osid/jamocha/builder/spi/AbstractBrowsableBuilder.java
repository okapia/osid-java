//
// AbstractBrowsableBuilder.java
//
//     Defines a builder for a Browsable.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Defines the Browsable builder.
 */

public abstract class AbstractBrowsableBuilder<T extends AbstractBrowsableBuilder<? extends T>>
    extends AbstractExtensibleBuilder<T> {

    private final BrowsableMiter browsable;


    /**
     *  Creates a new <code>AbstractBrowsableBuilder</code>.
     *
     *  @param browsable a browsable miter interface
     *  @throws org.osid.NullArgumentException <code>browsable</code> is
     *          <code>null</code>
     */

    protected AbstractBrowsableBuilder(ExtensibleMiter browsable) {
        super(browsable);

        if (!(browsable instanceof BrowsableMiter)) {
            throw new org.osid.UnsupportedException("browsable not a browsable");
        }

        this.browsable = (BrowsableMiter) browsable;
        return;
    }

    
    /**
     *  Adds a properties set.
     *
     *  @param properties a collection of properties
     *  @param recordType the type associated with this set of
     *         properties
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>properties</code>
     *          or <code>recordType</code> is <code>null</code>
     */
    
    public T properties(java.util.Collection<org.osid.Property> properties, org.osid.type.Type recordType) {
        this.browsable.addProperties(properties, recordType);
        return (self());
    }
}
