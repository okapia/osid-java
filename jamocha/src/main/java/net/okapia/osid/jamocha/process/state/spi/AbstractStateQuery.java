//
// AbstractStateQuery.java
//
//     A template for making a State Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.state.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for states.
 */

public abstract class AbstractStateQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.process.StateQuery {

    private final java.util.Collection<org.osid.process.records.StateQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the process <code> Id </code> for this query. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        return;
    }


    /**
     *  Clears the process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Clears the process terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given state query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a state implementing the requested record.
     *
     *  @param stateRecordType a state record type
     *  @return the state query record
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stateRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.StateQueryRecord getStateQueryRecord(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.StateQueryRecord record : this.records) {
            if (record.implementsRecordType(stateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stateRecordType + " is not supported");
    }


    /**
     *  Adds a record to this state query. 
     *
     *  @param stateQueryRecord state query record
     *  @param stateRecordType state record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStateQueryRecord(org.osid.process.records.StateQueryRecord stateQueryRecord, 
                                          org.osid.type.Type stateRecordType) {

        addRecordType(stateRecordType);
        nullarg(stateQueryRecord, "state query record");
        this.records.add(stateQueryRecord);        
        return;
    }
}
