//
// AbstractAdapterRecurringEventLookupSession.java
//
//    A RecurringEvent lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RecurringEvent lookup session adapter.
 */

public abstract class AbstractAdapterRecurringEventLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.RecurringEventLookupSession {

    private final org.osid.calendaring.RecurringEventLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRecurringEventLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRecurringEventLookupSession(org.osid.calendaring.RecurringEventLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code RecurringEvent} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRecurringEvents() {
        return (this.session.canLookupRecurringEvents());
    }


    /**
     *  A complete view of the {@code RecurringEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRecurringEventView() {
        this.session.useComparativeRecurringEventView();
        return;
    }


    /**
     *  A complete view of the {@code RecurringEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRecurringEventView() {
        this.session.usePlenaryRecurringEventView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recurring events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active recurring events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRecurringEventView() {
        this.session.useActiveRecurringEventView();
        return;
    }


    /**
     *  Active and inactive recurring events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRecurringEventView() {
        this.session.useAnyStatusRecurringEventView();
        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  recurring events.
     */

    @OSID @Override
    public void useSequesteredRecurringEventView() {
        this.session.useSequesteredRecurringEventView();
        return;
    }


    /**
     *  All recurring events are returned including sequestered recurring events.
     */

    @OSID @Override
    public void useUnsequesteredRecurringEventView() {
        this.session.useUnsequesteredRecurringEventView();
        return;
    }

     
    /**
     *  Gets the {@code RecurringEvent} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code RecurringEvent} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code RecurringEvent} and
     *  retained for compatibility.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param recurringEventId {@code Id} of the {@code RecurringEvent}
     *  @return the recurring event
     *  @throws org.osid.NotFoundException {@code recurringEventId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code recurringEventId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEvent getRecurringEvent(org.osid.id.Id recurringEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEvent(recurringEventId));
    }


    /**
     *  Gets a {@code RecurringEventList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recurringEvents specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code RecurringEvents} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RecurringEvent} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByIds(org.osid.id.IdList recurringEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventsByIds(recurringEventIds));
    }


    /**
     *  Gets a {@code RecurringEventList} corresponding to the given
     *  recurring event genus {@code Type} which does not include
     *  recurring events of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventGenusType a recurringEvent genus type 
     *  @return the returned {@code RecurringEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByGenusType(org.osid.type.Type recurringEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventsByGenusType(recurringEventGenusType));
    }


    /**
     *  Gets a {@code RecurringEventList} corresponding to the given
     *  recurring event genus {@code Type} and include any additional
     *  recurring events with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventGenusType a recurringEvent genus type 
     *  @return the returned {@code RecurringEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByParentGenusType(org.osid.type.Type recurringEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventsByParentGenusType(recurringEventGenusType));
    }


    /**
     *  Gets a {@code RecurringEventList} containing the given
     *  recurring event record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @param  recurringEventRecordType a recurringEvent record type 
     *  @return the returned {@code RecurringEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByRecordType(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventsByRecordType(recurringEventRecordType));
    }


    /**
     *  Gets the <code> RecurringEvents </code> containing the given schedule 
     *  slot.
     *  
     *  In plenary mode, the returned list contains all matching
     *  recurring events or an error results. Otherwise, the returned
     *  list may contain only those recurring events that are
     *  accessible through this session.
     *  
     *  In active mode, recurring events are returned that are currently 
     *  active. In any status mode, active and inactive recurring events are 
     *  returned. 
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @return the returned <code> RecurringEvent </code> list 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventsByScheduleSlot(scheduleSlotId));
    }


    /**
     *  Gets all {@code RecurringEvents}. 
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring events are returned that are currently
     *  active. In any status mode, active and inactive recurring events
     *  are returned.
     *
     *  @return a list of {@code RecurringEvents} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEvents());
    }
}
