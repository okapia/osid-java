//
// AbstractAssemblyTriggerEnablerQuery.java
//
//     A TriggerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.rules.triggerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TriggerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyTriggerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.control.rules.TriggerEnablerQuery,
               org.osid.control.rules.TriggerEnablerQueryInspector,
               org.osid.control.rules.TriggerEnablerSearchOrder {

    private final java.util.Collection<org.osid.control.rules.records.TriggerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.TriggerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyTriggerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyTriggerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the trigger. 
     *
     *  @param  triggerId the trigger <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> triggerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledTriggerId(org.osid.id.Id triggerId, boolean match) {
        getAssembler().addIdTerm(getRuledTriggerIdColumn(), triggerId, match);
        return;
    }


    /**
     *  Clears the trigger <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledTriggerIdTerms() {
        getAssembler().clearTerms(getRuledTriggerIdColumn());
        return;
    }


    /**
     *  Gets the trigger <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledTriggerIdTerms() {
        return (getAssembler().getIdTerms(getRuledTriggerIdColumn()));
    }


    /**
     *  Gets the RuledTriggerId column name.
     *
     * @return the column name
     */

    protected String getRuledTriggerIdColumn() {
        return ("ruled_trigger_id");
    }


    /**
     *  Tests if a <code> TriggerQuery </code> is available. 
     *
     *  @return <code> true </code> if a trigger query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledTriggerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a trigger. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the trigger query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledTriggerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuery getRuledTriggerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledTriggerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any trigger. 
     *
     *  @param  match <code> true </code> for enablers mapped to any trigger, 
     *          <code> false </code> to match enablers mapped to no triggers 
     */

    @OSID @Override
    public void matchAnyRuledTrigger(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledTriggerColumn(), match);
        return;
    }


    /**
     *  Clears the trigger query terms. 
     */

    @OSID @Override
    public void clearRuledTriggerTerms() {
        getAssembler().clearTerms(getRuledTriggerColumn());
        return;
    }


    /**
     *  Gets the trigger query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.TriggerQueryInspector[] getRuledTriggerTerms() {
        return (new org.osid.control.TriggerQueryInspector[0]);
    }


    /**
     *  Gets the RuledTrigger column name.
     *
     * @return the column name
     */

    protected String getRuledTriggerColumn() {
        return ("ruled_trigger");
    }


    /**
     *  Matches enablers mapped to the system. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this triggerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  triggerEnablerRecordType a trigger enabler record type 
     *  @return <code>true</code> if the triggerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type triggerEnablerRecordType) {
        for (org.osid.control.rules.records.TriggerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(triggerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  triggerEnablerRecordType the trigger enabler record type 
     *  @return the trigger enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.TriggerEnablerQueryRecord getTriggerEnablerQueryRecord(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.TriggerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(triggerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  triggerEnablerRecordType the trigger enabler record type 
     *  @return the trigger enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord getTriggerEnablerQueryInspectorRecord(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(triggerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param triggerEnablerRecordType the trigger enabler record type
     *  @return the trigger enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.TriggerEnablerSearchOrderRecord getTriggerEnablerSearchOrderRecord(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.TriggerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(triggerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this trigger enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param triggerEnablerQueryRecord the trigger enabler query record
     *  @param triggerEnablerQueryInspectorRecord the trigger enabler query inspector
     *         record
     *  @param triggerEnablerSearchOrderRecord the trigger enabler search order record
     *  @param triggerEnablerRecordType trigger enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerQueryRecord</code>,
     *          <code>triggerEnablerQueryInspectorRecord</code>,
     *          <code>triggerEnablerSearchOrderRecord</code> or
     *          <code>triggerEnablerRecordTypetriggerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addTriggerEnablerRecords(org.osid.control.rules.records.TriggerEnablerQueryRecord triggerEnablerQueryRecord, 
                                      org.osid.control.rules.records.TriggerEnablerQueryInspectorRecord triggerEnablerQueryInspectorRecord, 
                                      org.osid.control.rules.records.TriggerEnablerSearchOrderRecord triggerEnablerSearchOrderRecord, 
                                      org.osid.type.Type triggerEnablerRecordType) {

        addRecordType(triggerEnablerRecordType);

        nullarg(triggerEnablerQueryRecord, "trigger enabler query record");
        nullarg(triggerEnablerQueryInspectorRecord, "trigger enabler query inspector record");
        nullarg(triggerEnablerSearchOrderRecord, "trigger enabler search odrer record");

        this.queryRecords.add(triggerEnablerQueryRecord);
        this.queryInspectorRecords.add(triggerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(triggerEnablerSearchOrderRecord);
        
        return;
    }
}
