//
// InvariantIndexedMapOffsetEventEnablerLookupSession
//
//    Implements an OffsetEventEnabler lookup service backed by a fixed
//    collection of offsetEventEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements an OffsetEventEnabler lookup service backed by a fixed
 *  collection of offset event enablers. The offset event enablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some offset event enablers may be compatible
 *  with more types than are indicated through these offset event enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapOffsetEventEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractIndexedMapOffsetEventEnablerLookupSession
    implements org.osid.calendaring.rules.OffsetEventEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapOffsetEventEnablerLookupSession} using an
     *  array of offsetEventEnablers.
     *
     *  @param calendar the calendar
     *  @param offsetEventEnablers an array of offset event enablers
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEventEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                    org.osid.calendaring.rules.OffsetEventEnabler[] offsetEventEnablers) {

        setCalendar(calendar);
        putOffsetEventEnablers(offsetEventEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapOffsetEventEnablerLookupSession} using a
     *  collection of offset event enablers.
     *
     *  @param calendar the calendar
     *  @param offsetEventEnablers a collection of offset event enablers
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEventEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                    java.util.Collection<? extends org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablers) {

        setCalendar(calendar);
        putOffsetEventEnablers(offsetEventEnablers);
        return;
    }
}
