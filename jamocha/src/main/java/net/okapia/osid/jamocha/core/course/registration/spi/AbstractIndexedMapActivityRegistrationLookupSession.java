//
// AbstractIndexedMapActivityRegistrationLookupSession.java
//
//    A simple framework for providing an ActivityRegistration lookup service
//    backed by a fixed collection of activity registrations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an ActivityRegistration lookup service backed by a
 *  fixed collection of activity registrations. The activity registrations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some activity registrations may be compatible
 *  with more types than are indicated through these activity registration
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActivityRegistrations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapActivityRegistrationLookupSession
    extends AbstractMapActivityRegistrationLookupSession
    implements org.osid.course.registration.ActivityRegistrationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.registration.ActivityRegistration> activityRegistrationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.registration.ActivityRegistration>());
    private final MultiMap<org.osid.type.Type, org.osid.course.registration.ActivityRegistration> activityRegistrationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.registration.ActivityRegistration>());


    /**
     *  Makes an <code>ActivityRegistration</code> available in this session.
     *
     *  @param  activityRegistration an activity registration
     *  @throws org.osid.NullArgumentException <code>activityRegistration<code> is
     *          <code>null</code>
     */

    @Override
    protected void putActivityRegistration(org.osid.course.registration.ActivityRegistration activityRegistration) {
        super.putActivityRegistration(activityRegistration);

        this.activityRegistrationsByGenus.put(activityRegistration.getGenusType(), activityRegistration);
        
        try (org.osid.type.TypeList types = activityRegistration.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activityRegistrationsByRecord.put(types.getNextType(), activityRegistration);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an activity registration from this session.
     *
     *  @param activityRegistrationId the <code>Id</code> of the activity registration
     *  @throws org.osid.NullArgumentException <code>activityRegistrationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeActivityRegistration(org.osid.id.Id activityRegistrationId) {
        org.osid.course.registration.ActivityRegistration activityRegistration;
        try {
            activityRegistration = getActivityRegistration(activityRegistrationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.activityRegistrationsByGenus.remove(activityRegistration.getGenusType());

        try (org.osid.type.TypeList types = activityRegistration.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.activityRegistrationsByRecord.remove(types.getNextType(), activityRegistration);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeActivityRegistration(activityRegistrationId);
        return;
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> corresponding to the given
     *  activity registration genus <code>Type</code> which does not include
     *  activity registrations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known activity registrations or an error results. Otherwise,
     *  the returned list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  activityRegistrationGenusType an activity registration genus type 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByGenusType(org.osid.type.Type activityRegistrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.activityregistration.ArrayActivityRegistrationList(this.activityRegistrationsByGenus.get(activityRegistrationGenusType)));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> containing the given
     *  activity registration record <code>Type</code>. In plenary mode, the
     *  returned list contains all known activity registrations or an error
     *  results. Otherwise, the returned list may contain only those
     *  activity registrations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  activityRegistrationRecordType an activity registration record type 
     *  @return the returned <code>activityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByRecordType(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.activityregistration.ArrayActivityRegistrationList(this.activityRegistrationsByRecord.get(activityRegistrationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activityRegistrationsByGenus.clear();
        this.activityRegistrationsByRecord.clear();

        super.close();

        return;
    }
}
