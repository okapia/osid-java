//
// AbstractAssemblyActivityUnitQuery.java
//
//     An ActivityUnitQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActivityUnitQuery that stores terms.
 */

public abstract class AbstractAssemblyActivityUnitQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.course.ActivityUnitQuery,
               org.osid.course.ActivityUnitQueryInspector,
               org.osid.course.ActivityUnitSearchOrder {

    private final java.util.Collection<org.osid.course.records.ActivityUnitQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.ActivityUnitQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.ActivityUnitSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActivityUnitQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActivityUnitQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the course <code> Id </code> for this query to match activity 
     *  units that have a related course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseColumn(), style);
        return;
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course query terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Matches activity units with a total effort between the given durations 
     *  inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetEffort(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getTotalTargetEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity unit that has any total effort assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          total effort, <code> false </code> to match activity units 
     *          with no total effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getTotalTargetEffortColumn(), match);
        return;
    }


    /**
     *  Clears the total effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetEffortTerms() {
        getAssembler().clearTerms(getTotalTargetEffortColumn());
        return;
    }


    /**
     *  Gets the total effort query terms. 
     *
     *  @return the total effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalTargetEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the total 
     *  effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalTargetEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalTargetEffortColumn(), style);
        return;
    }


    /**
     *  Gets the TotalTargetEffort column name.
     *
     * @return the column name
     */

    protected String getTotalTargetEffortColumn() {
        return ("total_target_effort");
    }


    /**
     *  Matches activity units that are contact activities. 
     *
     *  @param  match <code> true </code> to match activity units that have 
     *          contact, <code> false </code> to match activity units with no 
     *          contact 
     */

    @OSID @Override
    public void matchContact(boolean match) {
        getAssembler().addBooleanTerm(getContactColumn(), match);
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        getAssembler().clearTerms(getContactColumn());
        return;
    }


    /**
     *  Gets the contact query terms. 
     *
     *  @return the contact query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getContactTerms() {
        return (getAssembler().getBooleanTerms(getContactColumn()));
    }


    /**
     *  Gets the Contact column name.
     *
     * @return the column name
     */

    protected String getContactColumn() {
        return ("contact");
    }


    /**
     *  Matches activity units with a total contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetContactTime(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        getAssembler().addDurationRangeTerm(getTotalTargetContactTimeColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity unit that has any total contact assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          total contatc, <code> false </code> to match activity units 
     *          with no total contact 
     */

    @OSID @Override
    public void matchAnyTotalTargetContactTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getTotalTargetContactTimeColumn(), match);
        return;
    }


    /**
     *  Clears the total contact terms. 
     */

    @OSID @Override
    public void clearTotalTargetContactTimeTerms() {
        getAssembler().clearTerms(getTotalTargetContactTimeColumn());
        return;
    }


    /**
     *  Gets the total contact time query terms. 
     *
     *  @return the total contact time query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetContactTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalTargetContactTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the total 
     *  contact. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalTargetContactTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalTargetContactTimeColumn(), style);
        return;
    }


    /**
     *  Gets the TotalTargetContactTime column name.
     *
     * @return the column name
     */

    protected String getTotalTargetContactTimeColumn() {
        return ("total_target_contact_time");
    }


    /**
     *  Matches activity units with a individual effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetIndividualEffort(org.osid.calendaring.Duration min, 
                                                 org.osid.calendaring.Duration max, 
                                                 boolean match) {
        getAssembler().addDurationRangeTerm(getTotalTargetIndividualEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity unit that has any individual effort assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          individual effort, <code> false </code> to match activity 
     *          units with no individual effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetIndividualEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getTotalTargetIndividualEffortColumn(), match);
        return;
    }


    /**
     *  Clears the individual effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetIndividualEffortTerms() {
        getAssembler().clearTerms(getTotalTargetIndividualEffortColumn());
        return;
    }


    /**
     *  Gets the total individual effort query terms. 
     *
     *  @return the total individual effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetIndividualEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalTargetIndividualEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by total individual 
     *  effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalTargetIndividualEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalTargetIndividualEffortColumn(), style);
        return;
    }


    /**
     *  Gets the TotalTargetIndividualEffort column name.
     *
     * @return the column name
     */

    protected String getTotalTargetIndividualEffortColumn() {
        return ("total_target_individual_effort");
    }


    /**
     *  Matches activity units that recur weekly. 
     *
     *  @param  match <code> true </code> to match activity units that recur 
     *          weekly, <code> false </code> to match activity units with no 
     *          weekly recurrance 
     */

    @OSID @Override
    public void matchRecurringWeekly(boolean match) {
        getAssembler().addBooleanTerm(getRecurringWeeklyColumn(), match);
        return;
    }


    /**
     *  Clears the recurring weekly terms. 
     */

    @OSID @Override
    public void clearRecurringWeeklyTerms() {
        getAssembler().clearTerms(getRecurringWeeklyColumn());
        return;
    }


    /**
     *  Gets the recurring weekly query terms. 
     *
     *  @return the recurring weekly query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRecurringWeeklyTerms() {
        return (getAssembler().getBooleanTerms(getRecurringWeeklyColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by weekly 
     *  recurring. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecurringWeekly(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecurringWeeklyColumn(), style);
        return;
    }


    /**
     *  Gets the RecurringWeekly column name.
     *
     * @return the column name
     */

    protected String getRecurringWeeklyColumn() {
        return ("recurring_weekly");
    }


    /**
     *  Matches activity units with a weekly effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyEffort(org.osid.calendaring.Duration min, 
                                  org.osid.calendaring.Duration max, 
                                  boolean match) {
        getAssembler().addDurationRangeTerm(getWeeklyEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity unit that has any weekly effort assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          weekly effort, <code> false </code> to match activity units 
     *          with no weekly effort 
     */

    @OSID @Override
    public void matchAnyWeeklyEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getWeeklyEffortColumn(), match);
        return;
    }


    /**
     *  Clears the weekly effort terms. 
     */

    @OSID @Override
    public void clearWeeklyEffortTerms() {
        getAssembler().clearTerms(getWeeklyEffortColumn());
        return;
    }


    /**
     *  Gets the weekly effort query terms. 
     *
     *  @return the weekly effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getWeeklyEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the weekly 
     *  effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeeklyEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeeklyEffortColumn(), style);
        return;
    }


    /**
     *  Gets the WeeklyEffort column name.
     *
     * @return the column name
     */

    protected String getWeeklyEffortColumn() {
        return ("weekly_effort");
    }


    /**
     *  Matches activity units with a weekly contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyContactTime(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getWeeklyContactTimeColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity unit that has any weekly contact time assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          weekly contact time, <code> false </code> to match activity 
     *          units with no weekly contact time 
     */

    @OSID @Override
    public void matchAnyWeeklyContactTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getWeeklyContactTimeColumn(), match);
        return;
    }


    /**
     *  Clears the weekly contact time terms. 
     */

    @OSID @Override
    public void clearWeeklyContactTimeTerms() {
        getAssembler().clearTerms(getWeeklyContactTimeColumn());
        return;
    }


    /**
     *  Gets the weekly contact time query terms. 
     *
     *  @return the weekly contact time query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyContactTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getWeeklyContactTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the weekly 
     *  contact. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeeklyContactTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeeklyContactTimeColumn(), style);
        return;
    }


    /**
     *  Gets the WeeklyContactTime column name.
     *
     * @return the column name
     */

    protected String getWeeklyContactTimeColumn() {
        return ("weekly_contact_time");
    }


    /**
     *  Matches activity units with a weekly individual effort between the 
     *  given durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyIndividualEffort(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        getAssembler().addDurationRangeTerm(getWeeklyIndividualEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity unit that has any weekly individual effort 
     *  assigned. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          weekly individual effort, <code> false </code> to match 
     *          activity units with no weekly individual effort 
     */

    @OSID @Override
    public void matchAnyWeeklyIndividualEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getWeeklyIndividualEffortColumn(), match);
        return;
    }


    /**
     *  Clears the weekly individual effort terms. 
     */

    @OSID @Override
    public void clearWeeklyIndividualEffortTerms() {
        getAssembler().clearTerms(getWeeklyIndividualEffortColumn());
        return;
    }


    /**
     *  Gets the weekly individual effort query terms. 
     *
     *  @return the weekly individual effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyIndividualEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getWeeklyIndividualEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by weekly 
     *  individual effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeeklyIndividualEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeeklyIndividualEffortColumn(), style);
        return;
    }


    /**
     *  Gets the WeeklyIndividualEffort column name.
     *
     * @return the column name
     */

    protected String getWeeklyIndividualEffortColumn() {
        return ("weekly_individual_effort");
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getLearningObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        getAssembler().clearTerms(getLearningObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getLearningObjectiveIdColumn()));
    }


    /**
     *  Gets the LearningObjectiveId column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveIdColumn() {
        return ("learning_objective_id");
    }


    /**
     *  Tests if a <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches activity units that have any learning objective. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          learning objective, <code> false </code> to match activity 
     *          units with no learning objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getLearningObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        getAssembler().clearTerms(getLearningObjectiveColumn());
        return;
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the LearningObjective column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveColumn() {
        return ("learning_objective");
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activity 
     *  units that have a related activity. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches activity units that have any activity. 
     *
     *  @param  match <code> true </code> to match activity units with any 
     *          activity. <code> false </code> to match activity units with no 
     *          activity 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  courses assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this activityUnit supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityUnitRecordType an activity unit record type 
     *  @return <code>true</code> if the activityUnitRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityUnitRecordType) {
        for (org.osid.course.records.ActivityUnitQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  activityUnitRecordType the activity unit record type 
     *  @return the activity unit query record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitQueryRecord getActivityUnitQueryRecord(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityUnitQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityUnitRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  activityUnitRecordType the activity unit record type 
     *  @return the activity unit query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitQueryInspectorRecord getActivityUnitQueryInspectorRecord(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityUnitQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityUnitRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param activityUnitRecordType the activity unit record type
     *  @return the activity unit search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitSearchOrderRecord getActivityUnitSearchOrderRecord(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityUnitSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityUnitRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this activity unit. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityUnitQueryRecord the activity unit query record
     *  @param activityUnitQueryInspectorRecord the activity unit query inspector
     *         record
     *  @param activityUnitSearchOrderRecord the activity unit search order record
     *  @param activityUnitRecordType activity unit record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitQueryRecord</code>,
     *          <code>activityUnitQueryInspectorRecord</code>,
     *          <code>activityUnitSearchOrderRecord</code> or
     *          <code>activityUnitRecordTypeactivityUnit</code> is
     *          <code>null</code>
     */
            
    protected void addActivityUnitRecords(org.osid.course.records.ActivityUnitQueryRecord activityUnitQueryRecord, 
                                      org.osid.course.records.ActivityUnitQueryInspectorRecord activityUnitQueryInspectorRecord, 
                                      org.osid.course.records.ActivityUnitSearchOrderRecord activityUnitSearchOrderRecord, 
                                      org.osid.type.Type activityUnitRecordType) {

        addRecordType(activityUnitRecordType);

        nullarg(activityUnitQueryRecord, "activity unit query record");
        nullarg(activityUnitQueryInspectorRecord, "activity unit query inspector record");
        nullarg(activityUnitSearchOrderRecord, "activity unit search odrer record");

        this.queryRecords.add(activityUnitQueryRecord);
        this.queryInspectorRecords.add(activityUnitQueryInspectorRecord);
        this.searchOrderRecords.add(activityUnitSearchOrderRecord);
        
        return;
    }
}
