//
// AbstractProficiencyLookupSession.java
//
//    A starter implementation framework for providing a Proficiency
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Proficiency
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProficiencies(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProficiencyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ProficiencyLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();
    

    /**
     *  Gets the <code>ObjectiveBank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>ObjectiveBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.objectiveBank.getId());
    }


    /**
     *  Gets the <code>ObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the <code>ObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.objectiveBank);
    }


    /**
     *  Sets the <code>ObjectiveBank</code>.
     *
     *  @param  objectiveBank the objective bank for this session
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>
     */

    protected void setObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        nullarg(objectiveBank, "objective bank");
        this.objectiveBank = objectiveBank;
        return;
    }


    /**
     *  Tests if this user can perform <code>Proficiency</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProficiencies() {
        return (true);
    }


    /**
     *  A complete view of the <code>Proficiency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProficiencyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Proficiency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProficiencyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include proficiencies in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only proficiencies whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveProficiencyView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All proficiencies of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProficiencyView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Proficiency</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Proficiency</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Proficiency</code> and
     *  retained for compatibility.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  @param  proficiencyId <code>Id</code> of the
     *          <code>Proficiency</code>
     *  @return the proficiency
     *  @throws org.osid.NotFoundException <code>proficiencyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>proficiencyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Proficiency getProficiency(org.osid.id.Id proficiencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.learning.ProficiencyList proficiencies = getProficiencies()) {
            while (proficiencies.hasNext()) {
                org.osid.learning.Proficiency proficiency = proficiencies.getNextProficiency();
                if (proficiency.getId().equals(proficiencyId)) {
                    return (proficiency);
                }
            }
        } 

        throw new org.osid.NotFoundException(proficiencyId + " not found");
    }


    /**
     *  Gets a <code>ProficiencyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  proficiencies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Proficiencies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProficiencies()</code>.
     *
     *  @param  proficiencyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Proficiency</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByIds(org.osid.id.IdList proficiencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.learning.Proficiency> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = proficiencyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProficiency(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("proficiency " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.learning.proficiency.LinkedProficiencyList(ret));
    }


    /**
     *  Gets a <code>ProficiencyList</code> corresponding to the given
     *  proficiency genus <code>Type</code> which does not include
     *  proficiencies of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProficiencies()</code>.
     *
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned <code>Proficiency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyGenusFilterList(getProficiencies(), proficiencyGenusType));
    }


    /**
     *  Gets a <code>ProficiencyList</code> corresponding to the given
     *  proficiency genus <code>Type</code> and include any additional
     *  proficiencies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProficiencies()</code>.
     *
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned <code>Proficiency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByParentGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProficienciesByGenusType(proficiencyGenusType));
    }


    /**
     *  Gets a <code>ProficiencyList</code> containing the given
     *  proficiency record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProficiencies()</code>.
     *
     *  @param  proficiencyRecordType a proficiency record type 
     *  @return the returned <code>Proficiency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByRecordType(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyRecordFilterList(getProficiencies(), proficiencyRecordType));
    }


    /**
     *  Gets a <code>ProficiencyList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In active mode, proficiencies are returned that are currently
     *  active. In any status mode, active and inactive proficiencies
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.TemporalProficiencyFilterList(getProficiencies(), from, to));
    }


    /**
     *  Gets a <code>ProficiencyList</code> by genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In active mode, proficiencies are returned that are currently
     *  active. In any status mode, active and inactive proficiencies
     *  are returned.
     *
     *  @param proficiencyGenusType a proficiency genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyGenusType</code>, <code>from</code>,
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeOnDate(org.osid.type.Type proficiencyGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.TemporalProficiencyFilterList(getProficienciesByGenusType(proficiencyGenusType), from, to));
    }
        

    /**
     *  Gets a list of proficiencies corresponding to an objective
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId the <code>Id</code> of the objective
     *  @return the returned <code>ProficiencyList</code>
     *  @throws org.osid.NullArgumentException <code>objectiveId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.learning.ProficiencyList getProficienciesForObjective(org.osid.id.Id objectiveId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyFilterList(new ObjectiveFilter(objectiveId), getProficiencies()));
    }


    /**
     *  Gets a list of proficiencies corresponding to an objective
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId the <code>Id</code> of the objective
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProficiencyList</code>
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectiveOnDate(org.osid.id.Id objectiveId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.TemporalProficiencyFilterList(getProficienciesForObjective(objectiveId), from, to));
    }


    /**
     *  Gets a <code>ProficiencyList</code> relating to the given
     *  objective and proficiency genus <code>Type</code>.
     *  
     *  In plenary mode, the returned list contains all known proficiencies or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  proficiencies that are accessible through this session. 
     *  
     *  In effective mode, proficiencies are returned that are currently 
     *  effective. In any effective mode, effective proficiencies and those 
     *  currently expired are returned. 
     *
     *  @param  objectiveId an objective <code>Id</code> 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveId</code> or
     *          <code>proficiencyGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjective(org.osid.id.Id objectiveId, 
                                                                                     org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyGenusFilterList(getProficienciesForObjective(objectiveId), proficiencyGenusType));
    }        


    /**
     *  Gets a <code>ProficiencyList</code> of the given proficiency
     *  genus type relating to the given objective effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId an objective <code>Id</code> 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveId</code>,
     *          <code>proficiencyGenusType</code>, <code>from</code>,
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjectiveOnDate(org.osid.id.Id objectiveId, 
                                                                                           org.osid.type.Type proficiencyGenusType, 
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyGenusFilterList(getProficienciesForObjectiveOnDate(objectiveId, from, to), proficiencyGenusType));
    }        


    /**
     *  Gets a <code>ProficiencyList</code> relating to the given
     *  objectives.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveIds the objective <code>Ids</code> 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveIds</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectives(org.osid.id.IdList objectiveIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.proficiency.CompositeProficiencyList ret = new net.okapia.osid.jamocha.adapter.federator.learning.proficiency.CompositeProficiencyList();

        try (org.osid.id.IdList ids = objectiveIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                ret.addProficiencyList(getProficienciesForObjective(id));
            }
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of proficiencies corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ProficiencyList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.learning.ProficiencyList getProficienciesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyFilterList(new ResourceFilter(resourceId), getProficiencies()));
    }


    /**
     *  Gets a list of proficiencies corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProficiencyList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForResourceOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.TemporalProficiencyFilterList(getProficienciesForResource(resourceId), from, to));
    }


    /**
     *  Gets a <code>ProficiencyList</code> relating to the given
     *  resource and proficiency genus <code>Type</code> .
     *  
     *  In plenary mode, the returned list contains all known proficiencies or 
     *  an error results. Otherwise, the returned list may contain only those 
     *  proficiencies that are accessible through this session. 
     *  
     *  In effective mode, proficiencies are returned that are currently 
     *  effective. In any effective mode, effective proficiencies and those 
     *  currently expired are returned. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>proficiencyGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                    org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyGenusFilterList(getProficienciesForResource(resourceId), proficiencyGenusType));
    }        


    /**
     *  Gets a <code>ProficiencyList</code> of the given proficiency
     *  genus type relating to the given resource effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>proficiencyGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                          org.osid.type.Type proficiencyGenusType, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyGenusFilterList(getProficienciesForResourceOnDate(resourceId, from, to), proficiencyGenusType));
    }


    /**
     *  Gets a <code>ProficiencyList</code> relating to the given
     *  resources.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceIds the resource <code>Ids</code> 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceIds</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForResources(org.osid.id.IdList resourceIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.proficiency.CompositeProficiencyList ret = new net.okapia.osid.jamocha.adapter.federator.learning.proficiency.CompositeProficiencyList();

        try (org.osid.id.IdList ids = resourceIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                ret.addProficiencyList(getProficienciesForResource(id));
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of proficiencies corresponding to objective and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId the <code>Id</code> of the objective
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ProficiencyList</code>
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectiveAndResource(org.osid.id.Id objectiveId,
                                                                                     org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyFilterList(new ResourceFilter(resourceId), getProficienciesForObjective(objectiveId)));
    }


    /**
     *  Gets a list of proficiencies corresponding to objective and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProficiencyList</code>
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectiveAndResourceOnDate(org.osid.id.Id objectiveId,
                                                                                           org.osid.id.Id resourceId,
                                                                                           org.osid.calendaring.DateTime from,
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.TemporalProficiencyFilterList(getProficienciesForObjectiveAndResource(objectiveId, resourceId), from, to));
    }


    /**
     *  Gets a <code>ProficiencyList</code> of the given genus type
     *  relating to the given objective and resource.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId an objective <code>Id</code> 
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveId</code>, <code>resourceId</code> or
     *          <code>proficiencyGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjectiveAndResource(org.osid.id.Id objectiveId, 
                                                                                                org.osid.id.Id resourceId, 
                                                                                                org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyGenusFilterList(getProficienciesForObjectiveAndResource(objectiveId, resourceId), proficiencyGenusType));
    }


    /**
     *  Gets a <code>ProficiencyList</code> of the given genus type
     *  relating to the given resource and objective effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId an objective <code>Id</code> 
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Proficiency</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveId</code>, <code>resourceId</code>,
     *          <code>proficiencyGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjectiveAndResourceOnDate(org.osid.id.Id objectiveId, 
                                                                                                      org.osid.id.Id resourceId, 
                                                                                                      org.osid.type.Type proficiencyGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyGenusFilterList(getProficienciesForObjectiveAndResourceOnDate(objectiveId, resourceId, from, to), proficiencyGenusType));
    }


    /**
     *  Gets all <code>Proficiencies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Proficiencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.learning.ProficiencyList getProficiencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the proficiency list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of proficiencies
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.learning.ProficiencyList filterProficienciesOnViews(org.osid.learning.ProficiencyList list)
        throws org.osid.OperationFailedException {

        org.osid.learning.ProficiencyList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.learning.proficiency.EffectiveProficiencyFilterList(ret);
        }

        return (ret);
    }


    public static class ObjectiveFilter
        implements net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyFilter {
         
        private final org.osid.id.Id objectiveId;
         
         
        /**
         *  Constructs a new <code>ObjectiveFilter</code>.
         *
         *  @param objectiveId the objective to filter
         *  @throws org.osid.NullArgumentException
         *          <code>objectiveId</code> is <code>null</code>
         */
        
        public ObjectiveFilter(org.osid.id.Id objectiveId) {
            nullarg(objectiveId, "objective Id");
            this.objectiveId = objectiveId;
            return;
        }

         
        /**
         *  Used by the ProficiencyFilterList to filter the 
         *  proficiency list based on objective.
         *
         *  @param proficiency the proficiency
         *  @return <code>true</code> to pass the proficiency,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.learning.Proficiency proficiency) {
            return (proficiency.getObjectiveId().equals(this.objectiveId));
        }
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.learning.proficiency.ProficiencyFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ProficiencyFilterList to filter the 
         *  proficiency list based on resource.
         *
         *  @param proficiency the proficiency
         *  @return <code>true</code> to pass the proficiency,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.learning.Proficiency proficiency) {
            return (proficiency.getResourceId().equals(this.resourceId));
        }
    }
}
