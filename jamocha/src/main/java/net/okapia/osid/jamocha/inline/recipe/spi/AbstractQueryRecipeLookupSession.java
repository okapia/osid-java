//
// AbstractQueryRecipeLookupSession.java
//
//    An inline adapter that maps a RecipeLookupSession to
//    a RecipeQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RecipeLookupSession to
 *  a RecipeQuerySession.
 */

public abstract class AbstractQueryRecipeLookupSession
    extends net.okapia.osid.jamocha.recipe.spi.AbstractRecipeLookupSession
    implements org.osid.recipe.RecipeLookupSession {

    private final org.osid.recipe.RecipeQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRecipeLookupSession.
     *
     *  @param querySession the underlying recipe query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRecipeLookupSession(org.osid.recipe.RecipeQuerySession querySession) {
        nullarg(querySession, "recipe query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Cookbook</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Cookbook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.session.getCookbookId());
    }


    /**
     *  Gets the <code>Cookbook</code> associated with this 
     *  session.
     *
     *  @return the <code>Cookbook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCookbook());
    }


    /**
     *  Tests if this user can perform <code>Recipe</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRecipes() {
        return (this.session.canSearchRecipes());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recipes in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.session.useFederatedCookbookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        this.session.useIsolatedCookbookView();
        return;
    }
    
     
    /**
     *  Gets the <code>Recipe</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Recipe</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Recipe</code> and
     *  retained for compatibility.
     *
     *  @param  recipeId <code>Id</code> of the
     *          <code>Recipe</code>
     *  @return the recipe
     *  @throws org.osid.NotFoundException <code>recipeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>recipeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Recipe getRecipe(org.osid.id.Id recipeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.RecipeQuery query = getQuery();
        query.matchId(recipeId, true);
        org.osid.recipe.RecipeList recipes = this.session.getRecipesByQuery(query);
        if (recipes.hasNext()) {
            return (recipes.getNextRecipe());
        } 
        
        throw new org.osid.NotFoundException(recipeId + " not found");
    }


    /**
     *  Gets a <code>RecipeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recipes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Recipes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  recipeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>recipeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByIds(org.osid.id.IdList recipeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.RecipeQuery query = getQuery();

        try (org.osid.id.IdList ids = recipeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRecipesByQuery(query));
    }


    /**
     *  Gets a <code>RecipeList</code> corresponding to the given
     *  recipe genus <code>Type</code> which does not include
     *  recipes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeGenusType a recipe genus type 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByGenusType(org.osid.type.Type recipeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.RecipeQuery query = getQuery();
        query.matchGenusType(recipeGenusType, true);
        return (this.session.getRecipesByQuery(query));
    }


    /**
     *  Gets a <code>RecipeList</code> corresponding to the given
     *  recipe genus <code>Type</code> and include any additional
     *  recipes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeGenusType a recipe genus type 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByParentGenusType(org.osid.type.Type recipeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.RecipeQuery query = getQuery();
        query.matchParentGenusType(recipeGenusType, true);
        return (this.session.getRecipesByQuery(query));
    }


    /**
     *  Gets a <code>RecipeList</code> containing the given
     *  recipe record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recipeRecordType a recipe record type 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByRecordType(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.RecipeQuery query = getQuery();
        query.matchRecordType(recipeRecordType, true);
        return (this.session.getRecipesByQuery(query));
    }


    /**
     *  Gets a <code>RecipeList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known recipes or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  recipes that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Recipe</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.RecipeQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getRecipesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Recipes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  recipes or an error results. Otherwise, the returned list
     *  may contain only those recipes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Recipes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.RecipeQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRecipesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.recipe.RecipeQuery getQuery() {
        org.osid.recipe.RecipeQuery query = this.session.getRecipeQuery();        
        return (query);
    }
}
