//
// AbstractItemSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractItemSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.assessment.ItemSearchResults {

    private org.osid.assessment.ItemList items;
    private final org.osid.assessment.ItemQueryInspector inspector;
    private final java.util.Collection<org.osid.assessment.records.ItemSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractItemSearchResults.
     *
     *  @param items the result set
     *  @param itemQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>items</code>
     *          or <code>itemQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractItemSearchResults(org.osid.assessment.ItemList items,
                                            org.osid.assessment.ItemQueryInspector itemQueryInspector) {
        nullarg(items, "items");
        nullarg(itemQueryInspector, "item query inspectpr");

        this.items = items;
        this.inspector = itemQueryInspector;

        return;
    }


    /**
     *  Gets the item list resulting from a search.
     *
     *  @return an item list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItems() {
        if (this.items == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.assessment.ItemList items = this.items;
        this.items = null;
	return (items);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.assessment.ItemQueryInspector getItemQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  item search record <code> Type. </code> This method must
     *  be used to retrieve an item implementing the requested
     *  record.
     *
     *  @param itemSearchRecordType an item search 
     *         record type 
     *  @return the item search
     *  @throws org.osid.NullArgumentException
     *          <code>itemSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(itemSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.ItemSearchResultsRecord getItemSearchResultsRecord(org.osid.type.Type itemSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.assessment.records.ItemSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(itemSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(itemSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record item search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addItemRecord(org.osid.assessment.records.ItemSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "item record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
