//
// AbstractOrderingProxyManager.java
//
//     An adapter for a OrderingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OrderingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOrderingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.ordering.OrderingProxyManager>
    implements org.osid.ordering.OrderingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterOrderingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOrderingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOrderingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOrderingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any item federation is exposed. Federation is exposed when a 
     *  specific item may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of items 
     *  appears as a single item. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a my order lookup service. 
     *
     *  @return <code> true </code> if my order lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyOrder() {
        return (getAdapteeManager().supportsMyOrder());
    }


    /**
     *  Tests for the availability of an order lookup service. 
     *
     *  @return <code> true </code> if order lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderLookup() {
        return (getAdapteeManager().supportsOrderLookup());
    }


    /**
     *  Tests if querying orders is available. 
     *
     *  @return <code> true </code> if order query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (getAdapteeManager().supportsOrderQuery());
    }


    /**
     *  Tests if searching for orders is available. 
     *
     *  @return <code> true </code> if order search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderSearch() {
        return (getAdapteeManager().supportsOrderSearch());
    }


    /**
     *  Tests if searching for orders is available. 
     *
     *  @return <code> true </code> if order search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderAdmin() {
        return (getAdapteeManager().supportsOrderAdmin());
    }


    /**
     *  Tests if order notification is available. 
     *
     *  @return <code> true </code> if order notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderNotification() {
        return (getAdapteeManager().supportsOrderNotification());
    }


    /**
     *  Tests if an order to store lookup session is available. 
     *
     *  @return <code> true </code> if order store lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderStore() {
        return (getAdapteeManager().supportsOrderStore());
    }


    /**
     *  Tests if an order to store assignment session is available. 
     *
     *  @return <code> true </code> if order store assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderStoreAssignment() {
        return (getAdapteeManager().supportsOrderStoreAssignment());
    }


    /**
     *  Tests if an order smart store session is available. 
     *
     *  @return <code> true </code> if order smart store is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderSmartStore() {
        return (getAdapteeManager().supportsOrderSmartStore());
    }


    /**
     *  Tests if an item admin session is available. 
     *
     *  @return <code> true </code> if an item admin session is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemAdmin() {
        return (getAdapteeManager().supportsItemAdmin());
    }


    /**
     *  Tests for the availability of a product lookup service. 
     *
     *  @return <code> true </code> if product lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductLookup() {
        return (getAdapteeManager().supportsProductLookup());
    }


    /**
     *  Tests if querying products is available. 
     *
     *  @return <code> true </code> if product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (getAdapteeManager().supportsProductQuery());
    }


    /**
     *  Tests if searching for products is available. 
     *
     *  @return <code> true </code> if product search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductSearch() {
        return (getAdapteeManager().supportsProductSearch());
    }


    /**
     *  Tests for the availability of a product administrative service for 
     *  creating and deleting products. 
     *
     *  @return <code> true </code> if product administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductAdmin() {
        return (getAdapteeManager().supportsProductAdmin());
    }


    /**
     *  Tests for the availability of a product notification service. 
     *
     *  @return <code> true </code> if product notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductNotification() {
        return (getAdapteeManager().supportsProductNotification());
    }


    /**
     *  Tests if a product to store lookup session is available. 
     *
     *  @return <code> true </code> if product store lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductStore() {
        return (getAdapteeManager().supportsProductStore());
    }


    /**
     *  Tests if a product to store assignment session is available. 
     *
     *  @return <code> true </code> if product store assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductStoreAssignment() {
        return (getAdapteeManager().supportsProductStoreAssignment());
    }


    /**
     *  Tests if a product smart store session is available. 
     *
     *  @return <code> true </code> if product smart store is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductSmartStore() {
        return (getAdapteeManager().supportsProductSmartStore());
    }


    /**
     *  Tests for the availability of a price schedule lookup service. 
     *
     *  @return <code> true </code> if price schedule lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleLookup() {
        return (getAdapteeManager().supportsPriceScheduleLookup());
    }


    /**
     *  Tests if querying price schedules is available. 
     *
     *  @return <code> true </code> if price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (getAdapteeManager().supportsPriceScheduleQuery());
    }


    /**
     *  Tests if searching for price schedules is available. 
     *
     *  @return <code> true </code> if price schedule search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSearch() {
        return (getAdapteeManager().supportsPriceScheduleSearch());
    }


    /**
     *  Tests for the availability of a price schedule administrative service 
     *  for creating and deleting price schedules. 
     *
     *  @return <code> true </code> if price schedule administration is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleAdmin() {
        return (getAdapteeManager().supportsPriceScheduleAdmin());
    }


    /**
     *  Tests for the availability of a price schedule notification service. 
     *
     *  @return <code> true </code> if price schedule notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleNotification() {
        return (getAdapteeManager().supportsPriceScheduleNotification());
    }


    /**
     *  Tests if a price schedule to store lookup session is available. 
     *
     *  @return <code> true </code> if price schedule store lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleStore() {
        return (getAdapteeManager().supportsPriceScheduleStore());
    }


    /**
     *  Tests if a price schedule to store assignment session is available. 
     *
     *  @return <code> true </code> if price schedule store assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleStoreAssignment() {
        return (getAdapteeManager().supportsPriceScheduleStoreAssignment());
    }


    /**
     *  Tests if a price schedule smart store session is available. 
     *
     *  @return <code> true </code> if price schedule smart store is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSmartStore() {
        return (getAdapteeManager().supportsPriceScheduleSmartStore());
    }


    /**
     *  Tests for the availability of an store lookup service. 
     *
     *  @return <code> true </code> if store lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreLookup() {
        return (getAdapteeManager().supportsStoreLookup());
    }


    /**
     *  Tests if querying stores is available. 
     *
     *  @return <code> true </code> if store query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (getAdapteeManager().supportsStoreQuery());
    }


    /**
     *  Tests if searching for stores is available. 
     *
     *  @return <code> true </code> if store search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreSearch() {
        return (getAdapteeManager().supportsStoreSearch());
    }


    /**
     *  Tests for the availability of a store administrative service for 
     *  creating and deleting stores. 
     *
     *  @return <code> true </code> if store administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreAdmin() {
        return (getAdapteeManager().supportsStoreAdmin());
    }


    /**
     *  Tests for the availability of a store notification service. 
     *
     *  @return <code> true </code> if store notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreNotification() {
        return (getAdapteeManager().supportsStoreNotification());
    }


    /**
     *  Tests for the availability of a store hierarchy traversal service. 
     *
     *  @return <code> true </code> if store hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreHierarchy() {
        return (getAdapteeManager().supportsStoreHierarchy());
    }


    /**
     *  Tests for the availability of a store hierarchy design service. 
     *
     *  @return <code> true </code> if store hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreHierarchyDesign() {
        return (getAdapteeManager().supportsStoreHierarchyDesign());
    }


    /**
     *  Tests for the availability of an ordering batch service. 
     *
     *  @return <code> true </code> if an ordering batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingBatch() {
        return (getAdapteeManager().supportsOrderingBatch());
    }


    /**
     *  Tests for the availability of an ordering rules service. 
     *
     *  @return <code> true </code> if an ordering rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingRules() {
        return (getAdapteeManager().supportsOrderingRules());
    }


    /**
     *  Gets the supported <code> Order </code> record types. 
     *
     *  @return a list containing the supported order record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrderRecordTypes() {
        return (getAdapteeManager().getOrderRecordTypes());
    }


    /**
     *  Tests if the given <code> Order </code> record type is supported. 
     *
     *  @param  orderRecordType a <code> Type </code> indicating an <code> 
     *          Order </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> orderRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrderRecordType(org.osid.type.Type orderRecordType) {
        return (getAdapteeManager().supportsOrderRecordType(orderRecordType));
    }


    /**
     *  Gets the supported order search record types. 
     *
     *  @return a list containing the supported order search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrderSearchRecordTypes() {
        return (getAdapteeManager().getOrderSearchRecordTypes());
    }


    /**
     *  Tests if the given order search record type is supported. 
     *
     *  @param  orderSearchRecordType a <code> Type </code> indicating an 
     *          order record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> orderSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrderSearchRecordType(org.osid.type.Type orderSearchRecordType) {
        return (getAdapteeManager().supportsOrderSearchRecordType(orderSearchRecordType));
    }


    /**
     *  Gets the supported <code> Item </code> record types. 
     *
     *  @return a list containing the supported item record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemRecordTypes() {
        return (getAdapteeManager().getItemRecordTypes());
    }


    /**
     *  Tests if the given <code> Item </code> record type is supported. 
     *
     *  @param  itemRecordType a <code> Type </code> indicating a <code> Item 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemRecordType(org.osid.type.Type itemRecordType) {
        return (getAdapteeManager().supportsItemRecordType(itemRecordType));
    }


    /**
     *  Gets the supported item search record types. 
     *
     *  @return a list containing the supported item search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemSearchRecordTypes() {
        return (getAdapteeManager().getItemSearchRecordTypes());
    }


    /**
     *  Tests if the given item search record type is supported. 
     *
     *  @param  itemSearchRecordType a <code> Type </code> indicating an item 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        return (getAdapteeManager().supportsItemSearchRecordType(itemSearchRecordType));
    }


    /**
     *  Gets the supported <code> Product </code> record types. 
     *
     *  @return a list containing the supported product record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProductRecordTypes() {
        return (getAdapteeManager().getProductRecordTypes());
    }


    /**
     *  Tests if the given <code> Product </code> record type is supported. 
     *
     *  @param  productRecordType a <code> Type </code> indicating a <code> 
     *          Product </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> productRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProductRecordType(org.osid.type.Type productRecordType) {
        return (getAdapteeManager().supportsProductRecordType(productRecordType));
    }


    /**
     *  Gets the supported product search record types. 
     *
     *  @return a list containing the supported product search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProductSearchRecordTypes() {
        return (getAdapteeManager().getProductSearchRecordTypes());
    }


    /**
     *  Tests if the given product search record type is supported. 
     *
     *  @param  productSearchRecordType a <code> Type </code> indicating a 
     *          product record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> productSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProductSearchRecordType(org.osid.type.Type productSearchRecordType) {
        return (getAdapteeManager().supportsProductSearchRecordType(productSearchRecordType));
    }


    /**
     *  Gets the supported <code> PriceSchedule </code> record types. 
     *
     *  @return a list containing the supported price schedule record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceScheduleRecordTypes() {
        return (getAdapteeManager().getPriceScheduleRecordTypes());
    }


    /**
     *  Tests if the given <code> PriceSchedule </code> record type is 
     *  supported. 
     *
     *  @param  priceScheduleRecordType a <code> Type </code> indicating a 
     *          <code> PriceSchedule </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priceScheduleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceScheduleRecordType(org.osid.type.Type priceScheduleRecordType) {
        return (getAdapteeManager().supportsPriceScheduleRecordType(priceScheduleRecordType));
    }


    /**
     *  Gets the supported price schedule search record types. 
     *
     *  @return a list containing the supported price schedule search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceScheduleSearchRecordTypes() {
        return (getAdapteeManager().getPriceScheduleSearchRecordTypes());
    }


    /**
     *  Tests if the given price schedule search record type is supported. 
     *
     *  @param  priceScheduleSearchRecordType a <code> Type </code> indicating 
     *          a price schedule record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          priceScheduleSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceScheduleSearchRecordType(org.osid.type.Type priceScheduleSearchRecordType) {
        return (getAdapteeManager().supportsPriceScheduleSearchRecordType(priceScheduleSearchRecordType));
    }


    /**
     *  Gets the supported <code> Price </code> record types. 
     *
     *  @return a list containing the supported price record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceRecordTypes() {
        return (getAdapteeManager().getPriceRecordTypes());
    }


    /**
     *  Tests if the given <code> Price </code> record type is supported. 
     *
     *  @param  priceRecordType a <code> Type </code> indicating a <code> 
     *          Price </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priceRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceRecordType(org.osid.type.Type priceRecordType) {
        return (getAdapteeManager().supportsPriceRecordType(priceRecordType));
    }


    /**
     *  Gets the supported <code> Store </code> record types. 
     *
     *  @return a list containing the supported store record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStoreRecordTypes() {
        return (getAdapteeManager().getStoreRecordTypes());
    }


    /**
     *  Tests if the given <code> Store </code> record type is supported. 
     *
     *  @param  storeRecordType a <code> Type </code> indicating a <code> 
     *          Store </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> storeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStoreRecordType(org.osid.type.Type storeRecordType) {
        return (getAdapteeManager().supportsStoreRecordType(storeRecordType));
    }


    /**
     *  Gets the supported store search record types. 
     *
     *  @return a list containing the supported store search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStoreSearchRecordTypes() {
        return (getAdapteeManager().getStoreSearchRecordTypes());
    }


    /**
     *  Tests if the given store search record type is supported. 
     *
     *  @param  storeSearchRecordType a <code> Type </code> indicating a store 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> storeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStoreSearchRecordType(org.osid.type.Type storeSearchRecordType) {
        return (getAdapteeManager().supportsStoreSearchRecordType(storeSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the authenticated agent. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> My </code> Order <code> Session </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyOrder() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.MyOrderSession getMyOrderSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyOrderSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the authenticated agent for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MyOrderSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyOrder() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.MyOrderSession getMyOrderSessionForStore(org.osid.id.Id storeId, 
                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyOrderSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderLookupSession getOrderLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order lookup 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderLookupSession getOrderLookupSessionForStore(org.osid.id.Id storeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderLookupSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuerySession getOrderQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order query 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuerySession getOrderQuerySessionForStore(org.osid.id.Id storeId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderQuerySessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchSession getOrderSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order search 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchSession getOrderSearchSessionForStore(org.osid.id.Id storeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderSearchSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderAdminSession getOrderAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderAdminSession getOrderAdminSessionForStore(org.osid.id.Id storeId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderAdminSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  notification service. 
     *
     *  @param  orderReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OrderNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> orderReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderNotificationSession getOrderNotificationSession(org.osid.ordering.OrderReceiver orderReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderNotificationSession(orderReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the order 
     *  notification service for the given store. 
     *
     *  @param  orderReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrderNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> orderReceiver, storeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderNotificationSession getOrderNotificationSessionForStore(org.osid.ordering.OrderReceiver orderReceiver, 
                                                                                          org.osid.id.Id storeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderNotificationSessionForStore(orderReceiver, storeId, proxy));
    }


    /**
     *  Gets the session for retrieving odrer to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderStore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderStoreSession getOrderStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderStoreSession(proxy));
    }


    /**
     *  Gets the session for assigning order to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrderStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderStoreAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderStoreAssignmentSession getOrderStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderStoreAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the order smart store for the given 
     *  store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @param  proxy a proxy 
     *  @return an <code> OrderSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderSmartStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSmartStoreSession getOrderSmartStoreSession(org.osid.id.Id storeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderSmartStoreSession(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  adminsitartive service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemAdminSession getItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item admin 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemAdminSession getItemAdminSessionForStore(org.osid.id.Id storeId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductLookupSession getProductLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProductLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product lookup 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductLookupSession getProductLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProductLookupSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuerySession getProductQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProductQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product query 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuerySession getProductQuerySessionForStore(org.osid.id.Id storeId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProductQuerySessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchSession getProductSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProductSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product search 
     *  service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchSession getProductSearchSessionForStore(org.osid.id.Id storeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProductSearchSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductAdminSession getProductAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProductAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  administrative service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductAdminSession getProductAdminSessionForStore(org.osid.id.Id storeId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProductAdminSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  notification service. 
     *
     *  @param  productReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProductNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> producteReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductNotificationSession getProductNotificationSession(org.osid.ordering.ProductReceiver productReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProductNotificationSession(productReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the product 
     *  notification service for the given store. 
     *
     *  @param  productReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProductNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> productReceiver, storeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductNotificationSession getProductNotificationSessionForStore(org.osid.ordering.ProductReceiver productReceiver, 
                                                                                              org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProductNotificationSessionForStore(productReceiver, storeId, proxy));
    }


    /**
     *  Gets the session for retrieving product to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProductStore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductStoreSession getProductStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProductStoreSession(proxy));
    }


    /**
     *  Gets the session for assigning product to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProductStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductStoreAssignmentSession gerProductStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().gerProductStoreAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the product smart store for the given 
     *  store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @param  proxy a proxy 
     *  @return a <code> ProductSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductSmartStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSmartStoreSession getProductSmartStoreSession(org.osid.id.Id storeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProductSmartStoreSession(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleLookupSession getPriceScheduleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  lookup service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleLookupSession getPriceScheduleLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleLookupSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuerySession getPriceScheduleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  query service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuerySession getPriceScheduleQuerySessionForStore(org.osid.id.Id storeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleQuerySessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchSession getPriceScheduleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  search service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSearchSession getPriceScheduleSearchSessionForStore(org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleSearchSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleAdminSession getPriceScheduleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  administrative service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleAdminSession getPriceScheduleAdminSessionForStore(org.osid.id.Id storeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleAdminSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  notification service. 
     *
     *  @param  priceScheduleReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> priceScheduleReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleNotificationSession getPriceScheduleNotificationSession(org.osid.ordering.PriceScheduleReceiver priceScheduleReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleNotificationSession(priceScheduleReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price schedule 
     *  notification service for the given store. 
     *
     *  @param  priceScheduleReceiver the receiver 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> priceScheduleReceiver, 
     *          storeId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleNotificationSession getPriceScheduleNotificationSessionForStore(org.osid.ordering.PriceScheduleReceiver priceScheduleReceiver, 
                                                                                                          org.osid.id.Id storeId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleNotificationSessionForStore(priceScheduleReceiver, storeId, proxy));
    }


    /**
     *  Gets the session for retrieving price schedule to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleStoreSession getPriceScheduleStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleStoreSession(proxy));
    }


    /**
     *  Gets the session for assigning price schedule to store mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleStoreAssignmentSession getPriceScheduleStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleStoreAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the price schedule smart store for 
     *  the given store. 
     *
     *  @param  storeId the <code> Id </code> of the store 
     *  @param  proxy a proxy 
     *  @return a <code> PriceScheduleSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException <code> storeId </code> not found 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleSmartStore() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleSmartStoreSession getPriceScheduleSmartStoreSession(org.osid.id.Id storeId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleSmartStoreSession(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreLookupSession getStoreLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuerySession getStoreQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreSearchSession getStoreSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStoreAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreAdminSession getStoreAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  notification service. 
     *
     *  @param  storeReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> StoreNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> storeReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreNotificationSession getStoreNotificationSession(org.osid.ordering.StoreReceiver storeReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreNotificationSession(storeReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreHierarchySession getStoreHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the store 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StoreHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreHierarchyDesignSession getStoreHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> OrderingBatchProxyManager. </code> 
     *
     *  @return an <code> OrderingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.OrderingBatchProxyManager getOrderingBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderingBatchProxyManager());
    }


    /**
     *  Gets the <code> OrderingRulesProxyManager. </code> 
     *
     *  @return an <code> OrderingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOrderingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.OrderingRulesProxyManager getOrderingRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderingRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
