//
// RequestMiter.java
//
//     Defines a Request miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.request;


/**
 *  Defines a <code>Request</code> miter for use with the builders.
 */

public interface RequestMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.provisioning.Request {


    /**
     *  Sets the request transaction.
     *
     *  @param transaction a request transaction
     *  @throws org.osid.NullArgumentException
     *          <code>transaction</code> is <code>null</code>
     */

    public void setRequestTransaction(org.osid.provisioning.RequestTransaction transaction);


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException <code>queue</code> is
     *          <code>null</code>
     */

    public void setQueue(org.osid.provisioning.Queue queue);


    /**
     *  Sets the request date.
     *
     *  @param date a request date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setRequestDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the requester.
     *
     *  @param requester a requester
     *  @throws org.osid.NullArgumentException <code>requester</code>
     *          is <code>null</code>
     */

    public void setRequester(org.osid.resource.Resource requester);


    /**
     *  Sets the requesting agent.
     *
     *  @param agent a requesting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setRequestingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the pool.
     *
     *  @param pool a pool
     *  @throws org.osid.NullArgumentException <code>pool</code> is
     *          <code>null</code>
     */

    public void setPool(org.osid.provisioning.Pool pool);


    /**
     *  Adds a requested provisionable.
     *
     *  @param provisionable a requested provisionable
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    public void addRequestedProvisionable(org.osid.provisioning.Provisionable provisionable);


    /**
     *  Sets all the requested provisionables.
     *
     *  @param provisionables a collection of requested provisionables
     *  @throws org.osid.NullArgumentException
     *          <code>provisionables</code> is <code>null</code>
     */

    public void setRequestedProvisionables(java.util.Collection<org.osid.provisioning.Provisionable> provisionables);


    /**
     *  Sets the exchange provision.
     *
     *  @param provision an exchange provision
     *  @throws org.osid.NullArgumentException <code>provision</code>
     *          is <code>null</code>
     */

    public void setExchangeProvision(org.osid.provisioning.Provision provision);


    /**
     *  Sets the origin provision.
     *
     *  @param provision an origin provision
     *  @throws org.osid.NullArgumentException <code>provision</code>
     *          is <code>null</code>
     */

    public void setOriginProvision(org.osid.provisioning.Provision provision);


    /**
     *  Sets the position.
     *
     *  @param position a position
     *  @throws org.osid.InvalidArgumentException
     *          <code>position</code> is negative
     */

    public void setPosition(long position);


    /**
     *  Sets the ewa.
     *
     *  @param ewa the estimated waiting time
     *  @throws org.osid.NullArgumentException <code>ewa</code> is
     *          <code>null</code>
     */

    public void setEWA(org.osid.calendaring.Duration ewa);


    /**
     *  Adds a Request record.
     *
     *  @param record a request record
     *  @param recordType the type of request record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRequestRecord(org.osid.provisioning.records.RequestRecord record, org.osid.type.Type recordType);
}       


