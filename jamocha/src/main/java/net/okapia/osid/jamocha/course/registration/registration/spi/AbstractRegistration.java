//
// AbstractRegistration.java
//
//     Defines a Registration.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Registration</code>.
 */

public abstract class AbstractRegistration
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.registration.Registration {

    private org.osid.course.registration.ActivityBundle activityBundle;
    private org.osid.resource.Resource student;
    private org.osid.grading.GradeSystem gradingOption;

    private final java.util.Collection<java.math.BigDecimal> credits = new java.util.LinkedHashSet<>();;
    private final java.util.Collection<org.osid.course.registration.records.RegistrationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the activity bundle <code> Id </code> associated with this 
     *  registration. 
     *
     *  @return the activity bundle <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityBundleId() {
        return (this.activityBundle.getId());
    }


    /**
     *  Gets the activity bundle associated with this registration. 
     *
     *  @return the activity bundle 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundle getActivityBundle()
        throws org.osid.OperationFailedException {

        return (this.activityBundle);
    }


    /**
     *  Sets the activity bundle.
     *
     *  @param activityBundle an activity bundle
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundle</code> is <code>null</code>
     */

    protected void setActivityBundle(org.osid.course.registration.ActivityBundle activityBundle) {
        nullarg(activityBundle, "activity bundle");
        this.activityBundle = activityBundle;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the student <code> Resource. </code> 
     *
     *  @return the <code> Student </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the student <code> Resource. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Tests if this registration defines credits to be earned. 
     *
     *  @return <code> true </code> if this registration has credits,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean definesCredits() {
        return ((this.credits != null) && (this.credits.size() > 0));
    }


    /**
     *  Gets the number of credits the student is registered to earn. Multiple 
     *  credit options indicates a set of credits to be determined at the 
     *  completion of the course. 
     *
     *  @return the number of credits 
     *  @throws org.osid.IllegalStateException <code> definesCredits()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal[] getCredits() {
        if (!definesCredits()) {
            throw new org.osid.IllegalStateException("definesCredits() is false");
        }

        return (this.credits.toArray(new java.math.BigDecimal[this.credits.size()]));
    }


    /**
     *  Adds a credits value.
     *
     *  @param credits the credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void addCredits(java.math.BigDecimal credits) {
        nullarg(credits, "credits");
        this.credits.add(credits);
        return;
    }


    /**
     *  Sets the credits.
     *
     *  @param credits the credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void setCredits(java.util.Collection<java.math.BigDecimal> credits) {
        nullarg(credits, "credits");

        this.credits.clear();
        this.credits.addAll(credits);

        return;
    }


    /**
     *  Tests if this registration includes a specific grading option. 
     *
     *  @return <code> true </code> if this course is graded, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.gradingOption != null);
    }


    /**
     *  Gets the grading option <code> Id </code> for this registration. 
     *
     *  @return the grading option <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradingOptionId() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.gradingOption.getId());
    }


    /**
     *  Gets the grading option for this registration. 
     *
     *  @return a grading system 
     *  @throws org.osid.IllegalStateException <code> isGraded()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradingOption()
        throws org.osid.OperationFailedException {

        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.gradingOption);
    }


    /**
     *  Sets the grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    protected void setGradingOption(org.osid.grading.GradeSystem option) {
        nullarg(option, "grading option");
        this.gradingOption = option;
        return;
    }


    /**
     *  Tests if this registration supports the given record
     *  <code>Type</code>.
     *
     *  @param  registrationRecordType a registration record type 
     *  @return <code>true</code> if the registrationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type registrationRecordType) {
        for (org.osid.course.registration.records.RegistrationRecord record : this.records) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Registration</code> record <code>Type</code>.
     *
     *  @param  registrationRecordType the registration record type 
     *  @return the registration record 
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(registrationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationRecord getRegistrationRecord(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.RegistrationRecord record : this.records) {
            if (record.implementsRecordType(registrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(registrationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this registration. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param registrationRecord the registration record
     *  @param registrationRecordType registration record type
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecord</code> or
     *          <code>registrationRecordTyperegistration</code> is
     *          <code>null</code>
     */
            
    protected void addRegistrationRecord(org.osid.course.registration.records.RegistrationRecord registrationRecord, 
                                         org.osid.type.Type registrationRecordType) {

        nullarg(registrationRecord, "registration record");
        addRecordType(registrationRecordType);
        this.records.add(registrationRecord);
        
        return;
    }
}
