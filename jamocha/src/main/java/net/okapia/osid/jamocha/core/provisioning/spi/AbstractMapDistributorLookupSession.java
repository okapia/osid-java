//
// AbstractMapDistributorLookupSession
//
//    A simple framework for providing a Distributor lookup service
//    backed by a fixed collection of distributors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Distributor lookup service backed by a
 *  fixed collection of distributors. The distributors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Distributors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDistributorLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractDistributorLookupSession
    implements org.osid.provisioning.DistributorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.Distributor> distributors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.Distributor>());


    /**
     *  Makes a <code>Distributor</code> available in this session.
     *
     *  @param  distributor a distributor
     *  @throws org.osid.NullArgumentException <code>distributor<code>
     *          is <code>null</code>
     */

    protected void putDistributor(org.osid.provisioning.Distributor distributor) {
        this.distributors.put(distributor.getId(), distributor);
        return;
    }


    /**
     *  Makes an array of distributors available in this session.
     *
     *  @param  distributors an array of distributors
     *  @throws org.osid.NullArgumentException <code>distributors<code>
     *          is <code>null</code>
     */

    protected void putDistributors(org.osid.provisioning.Distributor[] distributors) {
        putDistributors(java.util.Arrays.asList(distributors));
        return;
    }


    /**
     *  Makes a collection of distributors available in this session.
     *
     *  @param  distributors a collection of distributors
     *  @throws org.osid.NullArgumentException <code>distributors<code>
     *          is <code>null</code>
     */

    protected void putDistributors(java.util.Collection<? extends org.osid.provisioning.Distributor> distributors) {
        for (org.osid.provisioning.Distributor distributor : distributors) {
            this.distributors.put(distributor.getId(), distributor);
        }

        return;
    }


    /**
     *  Removes a Distributor from this session.
     *
     *  @param  distributorId the <code>Id</code> of the distributor
     *  @throws org.osid.NullArgumentException <code>distributorId<code> is
     *          <code>null</code>
     */

    protected void removeDistributor(org.osid.id.Id distributorId) {
        this.distributors.remove(distributorId);
        return;
    }


    /**
     *  Gets the <code>Distributor</code> specified by its <code>Id</code>.
     *
     *  @param  distributorId <code>Id</code> of the <code>Distributor</code>
     *  @return the distributor
     *  @throws org.osid.NotFoundException <code>distributorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>distributorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.Distributor distributor = this.distributors.get(distributorId);
        if (distributor == null) {
            throw new org.osid.NotFoundException("distributor not found: " + distributorId);
        }

        return (distributor);
    }


    /**
     *  Gets all <code>Distributors</code>. In plenary mode, the returned
     *  list contains all known distributors or an error
     *  results. Otherwise, the returned list may contain only those
     *  distributors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Distributors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.distributor.ArrayDistributorList(this.distributors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.distributors.clear();
        super.close();
        return;
    }
}
