//
// AbstractAssemblyCreditQuery.java
//
//     A CreditQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.acknowledgement.credit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CreditQuery that stores terms.
 */

public abstract class AbstractAssemblyCreditQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.acknowledgement.CreditQuery,
               org.osid.acknowledgement.CreditQueryInspector,
               org.osid.acknowledgement.CreditSearchOrder {

    private final java.util.Collection<org.osid.acknowledgement.records.CreditQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.acknowledgement.records.CreditQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.acknowledgement.records.CreditSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCreditQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCreditQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets reference <code> Id. </code> 
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> referenceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id referenceId, boolean match) {
        getAssembler().addIdTerm(getReferenceIdColumn(), referenceId, match);
        return;
    }


    /**
     *  Clears all reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        getAssembler().clearTerms(getReferenceIdColumn());
        return;
    }


    /**
     *  Gets the reference <code> Id </code> query terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (getAssembler().getIdTerms(getReferenceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the reference. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReference(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReferenceIdColumn(), style);
        return;
    }


    /**
     *  Gets the ReferenceId column name.
     *
     * @return the column name
     */

    protected String getReferenceIdColumn() {
        return ("reference_id");
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears all resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the billing <code> Id </code> for this query to match credits 
     *  assigned to billings. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBillingId(org.osid.id.Id billingId, boolean match) {
        getAssembler().addIdTerm(getBillingIdColumn(), billingId, match);
        return;
    }


    /**
     *  Clears all billing <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBillingIdTerms() {
        getAssembler().clearTerms(getBillingIdColumn());
        return;
    }


    /**
     *  Gets the billing <code> Id </code> query terms. 
     *
     *  @return the billing <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBillingIdTerms() {
        return (getAssembler().getIdTerms(getBillingIdColumn()));
    }


    /**
     *  Gets the BillingId column name.
     *
     * @return the column name
     */

    protected String getBillingIdColumn() {
        return ("billing_id");
    }


    /**
     *  Tests if a <code> BillingQuery </code> is available. 
     *
     *  @return <code> true </code> if a billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the billing query 
     *  @throws org.osid.UnimplementedException <code> supportsBillingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuery getBillingQuery() {
        throw new org.osid.UnimplementedException("supportsBillingQuery() is false");
    }


    /**
     *  Clears all billing terms. 
     */

    @OSID @Override
    public void clearBillingTerms() {
        getAssembler().clearTerms(getBillingColumn());
        return;
    }


    /**
     *  Gets the billing query terms. 
     *
     *  @return the billing terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQueryInspector[] getBillingTerms() {
        return (new org.osid.acknowledgement.BillingQueryInspector[0]);
    }


    /**
     *  Gets the Billing column name.
     *
     * @return the column name
     */

    protected String getBillingColumn() {
        return ("billing");
    }


    /**
     *  Tests if this credit supports the given record
     *  <code>Type</code>.
     *
     *  @param  creditRecordType a credit record type 
     *  @return <code>true</code> if the creditRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type creditRecordType) {
        for (org.osid.acknowledgement.records.CreditQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(creditRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  creditRecordType the credit record type 
     *  @return the credit query record 
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(creditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditQueryRecord getCreditQueryRecord(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.CreditQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(creditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(creditRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  creditRecordType the credit record type 
     *  @return the credit query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(creditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditQueryInspectorRecord getCreditQueryInspectorRecord(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.CreditQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(creditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(creditRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param creditRecordType the credit record type
     *  @return the credit search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(creditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditSearchOrderRecord getCreditSearchOrderRecord(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.CreditSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(creditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(creditRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this credit. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param creditQueryRecord the credit query record
     *  @param creditQueryInspectorRecord the credit query inspector
     *         record
     *  @param creditSearchOrderRecord the credit search order record
     *  @param creditRecordType credit record type
     *  @throws org.osid.NullArgumentException
     *          <code>creditQueryRecord</code>,
     *          <code>creditQueryInspectorRecord</code>,
     *          <code>creditSearchOrderRecord</code> or
     *          <code>creditRecordTypecredit</code> is
     *          <code>null</code>
     */
            
    protected void addCreditRecords(org.osid.acknowledgement.records.CreditQueryRecord creditQueryRecord, 
                                      org.osid.acknowledgement.records.CreditQueryInspectorRecord creditQueryInspectorRecord, 
                                      org.osid.acknowledgement.records.CreditSearchOrderRecord creditSearchOrderRecord, 
                                      org.osid.type.Type creditRecordType) {

        addRecordType(creditRecordType);

        nullarg(creditQueryRecord, "credit query record");
        nullarg(creditQueryInspectorRecord, "credit query inspector record");
        nullarg(creditSearchOrderRecord, "credit search odrer record");

        this.queryRecords.add(creditQueryRecord);
        this.queryInspectorRecords.add(creditQueryInspectorRecord);
        this.searchOrderRecords.add(creditSearchOrderRecord);
        
        return;
    }
}
