//
// PriceElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.price.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PriceElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the PriceElement Id.
     *
     *  @return the price element Id
     */

    public static org.osid.id.Id getPriceEntityId() {
        return (makeEntityId("osid.ordering.Price"));
    }


    /**
     *  Gets the PriceScheduleId element Id.
     *
     *  @return the PriceScheduleId element Id
     */

    public static org.osid.id.Id getPriceScheduleId() {
        return (makeElementId("osid.ordering.price.PriceScheduleId"));
    }


    /**
     *  Gets the PriceSchedule element Id.
     *
     *  @return the PriceSchedule element Id
     */

    public static org.osid.id.Id getPriceSchedule() {
        return (makeElementId("osid.ordering.price.PriceSchedule"));
    }


    /**
     *  Gets the MinimumQuantity element Id.
     *
     *  @return the MinimumQuantity element Id
     */

    public static org.osid.id.Id getMinimumQuantity() {
        return (makeElementId("osid.ordering.price.MinimumQuantity"));
    }


    /**
     *  Gets the MaximumQuantity element Id.
     *
     *  @return the MaximumQuantity element Id
     */

    public static org.osid.id.Id getMaximumQuantity() {
        return (makeElementId("osid.ordering.price.MaximumQuantity"));
    }


    /**
     *  Gets the DemographicId element Id.
     *
     *  @return the DemographicId element Id
     */

    public static org.osid.id.Id getDemographicId() {
        return (makeElementId("osid.ordering.price.DemographicId"));
    }


    /**
     *  Gets the Demographic element Id.
     *
     *  @return the Demographic element Id
     */

    public static org.osid.id.Id getDemographic() {
        return (makeElementId("osid.ordering.price.Demographic"));
    }


    /**
     *  Gets the Amount element Id.
     *
     *  @return the Amount element Id
     */

    public static org.osid.id.Id getAmount() {
        return (makeElementId("osid.ordering.price.Amount"));
    }


    /**
     *  Gets the RecurringInterval element Id.
     *
     *  @return the RecurringInterval element Id
     */

    public static org.osid.id.Id getRecurringInterval() {
        return (makeElementId("osid.ordering.price.RecurringInterval"));
    }


    /**
     *  Gets the MinimumAmount element Id.
     *
     *  @return the MinimumAmount element Id
     */

    public static org.osid.id.Id getMinimumAmount() {
        return (makeQueryElementId("osid.ordering.price.MinimumAmount"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeQueryElementId("osid.ordering.price.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeQueryElementId("osid.ordering.price.Item"));
    }


    /**
     *  Gets the StoreId element Id.
     *
     *  @return the StoreId element Id
     */

    public static org.osid.id.Id getStoreId() {
        return (makeQueryElementId("osid.ordering.price.StoreId"));
    }


    /**
     *  Gets the Store element Id.
     *
     *  @return the Store element Id
     */

    public static org.osid.id.Id getStore() {
        return (makeQueryElementId("osid.ordering.price.Store"));
    }
}
