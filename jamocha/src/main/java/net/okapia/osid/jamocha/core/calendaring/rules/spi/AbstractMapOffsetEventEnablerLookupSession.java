//
// AbstractMapOffsetEventEnablerLookupSession
//
//    A simple framework for providing an OffsetEventEnabler lookup service
//    backed by a fixed collection of offset event enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an OffsetEventEnabler lookup service backed by a
 *  fixed collection of offset event enablers. The offset event enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OffsetEventEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOffsetEventEnablerLookupSession
    extends net.okapia.osid.jamocha.calendaring.rules.spi.AbstractOffsetEventEnablerLookupSession
    implements org.osid.calendaring.rules.OffsetEventEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.rules.OffsetEventEnabler>());


    /**
     *  Makes an <code>OffsetEventEnabler</code> available in this session.
     *
     *  @param  offsetEventEnabler an offset event enabler
     *  @throws org.osid.NullArgumentException <code>offsetEventEnabler<code>
     *          is <code>null</code>
     */

    protected void putOffsetEventEnabler(org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler) {
        this.offsetEventEnablers.put(offsetEventEnabler.getId(), offsetEventEnabler);
        return;
    }


    /**
     *  Makes an array of offset event enablers available in this session.
     *
     *  @param  offsetEventEnablers an array of offset event enablers
     *  @throws org.osid.NullArgumentException <code>offsetEventEnablers<code>
     *          is <code>null</code>
     */

    protected void putOffsetEventEnablers(org.osid.calendaring.rules.OffsetEventEnabler[] offsetEventEnablers) {
        putOffsetEventEnablers(java.util.Arrays.asList(offsetEventEnablers));
        return;
    }


    /**
     *  Makes a collection of offset event enablers available in this session.
     *
     *  @param  offsetEventEnablers a collection of offset event enablers
     *  @throws org.osid.NullArgumentException <code>offsetEventEnablers<code>
     *          is <code>null</code>
     */

    protected void putOffsetEventEnablers(java.util.Collection<? extends org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablers) {
        for (org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler : offsetEventEnablers) {
            this.offsetEventEnablers.put(offsetEventEnabler.getId(), offsetEventEnabler);
        }

        return;
    }


    /**
     *  Removes an OffsetEventEnabler from this session.
     *
     *  @param  offsetEventEnablerId the <code>Id</code> of the offset event enabler
     *  @throws org.osid.NullArgumentException <code>offsetEventEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeOffsetEventEnabler(org.osid.id.Id offsetEventEnablerId) {
        this.offsetEventEnablers.remove(offsetEventEnablerId);
        return;
    }


    /**
     *  Gets the <code>OffsetEventEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  offsetEventEnablerId <code>Id</code> of the <code>OffsetEventEnabler</code>
     *  @return the offsetEventEnabler
     *  @throws org.osid.NotFoundException <code>offsetEventEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>offsetEventEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnabler getOffsetEventEnabler(org.osid.id.Id offsetEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler = this.offsetEventEnablers.get(offsetEventEnablerId);
        if (offsetEventEnabler == null) {
            throw new org.osid.NotFoundException("offsetEventEnabler not found: " + offsetEventEnablerId);
        }

        return (offsetEventEnabler);
    }


    /**
     *  Gets all <code>OffsetEventEnablers</code>. In plenary mode, the returned
     *  list contains all known offsetEventEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  offsetEventEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>OffsetEventEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.ArrayOffsetEventEnablerList(this.offsetEventEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offsetEventEnablers.clear();
        super.close();
        return;
    }
}
