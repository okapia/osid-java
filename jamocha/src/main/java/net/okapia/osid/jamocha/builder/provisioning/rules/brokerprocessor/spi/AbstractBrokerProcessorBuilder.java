//
// AbstractBrokerProcessor.java
//
//     Defines a BrokerProcessor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessor.spi;


/**
 *  Defines a <code>BrokerProcessor</code> builder.
 */

public abstract class AbstractBrokerProcessorBuilder<T extends AbstractBrokerProcessorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidProcessorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessor.BrokerProcessorMiter brokerProcessor;


    /**
     *  Constructs a new <code>AbstractBrokerProcessorBuilder</code>.
     *
     *  @param brokerProcessor the broker processor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBrokerProcessorBuilder(net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessor.BrokerProcessorMiter brokerProcessor) {
        super(brokerProcessor);
        this.brokerProcessor = brokerProcessor;
        return;
    }


    /**
     *  Builds the broker processor.
     *
     *  @return the new broker processor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.rules.BrokerProcessor build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.rules.brokerprocessor.BrokerProcessorValidator(getValidations())).validate(this.brokerProcessor);
        return (new net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessor.ImmutableBrokerProcessor(this.brokerProcessor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the broker processor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessor.BrokerProcessorMiter getMiter() {
        return (this.brokerProcessor);
    }


    /**
     *  Sets the fixed lease duration.
     *
     *  @param duration a fixed lease duration
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public T fixedLeaseDuration(org.osid.calendaring.Duration duration) {
        getMiter().setFixedLeaseDuration(duration);
        return (self());
    }


    /**
     *  Adds a BrokerProcessor record.
     *
     *  @param record a broker processor record
     *  @param recordType the type of broker processor record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.rules.records.BrokerProcessorRecord record, org.osid.type.Type recordType) {
        getMiter().addBrokerProcessorRecord(record, recordType);
        return (self());
    }
}       


