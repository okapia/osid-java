//
// MutableIndexedMapFileLookupSession
//
//    Implements a File lookup service backed by a collection of
//    files indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing;


/**
 *  Implements a File lookup service backed by a collection of
 *  files. The files are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some files may be compatible
 *  with more types than are indicated through these file
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of files can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapFileLookupSession
    extends net.okapia.osid.jamocha.core.filing.spi.AbstractIndexedMapFileLookupSession
    implements org.osid.filing.FileLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapFileLookupSession} with no files.
     *
     *  @param directory the directory
     *  @throws org.osid.NullArgumentException {@code directory}
     *          is {@code null}
     */

      public MutableIndexedMapFileLookupSession(org.osid.filing.Directory directory) {
        setDirectory(directory);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFileLookupSession} with a
     *  single file.
     *  
     *  @param directory the directory
     *  @param  file a single file
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code file} is {@code null}
     */

    public MutableIndexedMapFileLookupSession(org.osid.filing.Directory directory,
                                                  org.osid.filing.File file) {
        this(directory);
        putFile(file);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFileLookupSession} using an
     *  array of files.
     *
     *  @param directory the directory
     *  @param  files an array of files
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code files} is {@code null}
     */

    public MutableIndexedMapFileLookupSession(org.osid.filing.Directory directory,
                                                  org.osid.filing.File[] files) {
        this(directory);
        putFiles(files);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFileLookupSession} using a
     *  collection of files.
     *
     *  @param directory the directory
     *  @param  files a collection of files
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code files} is {@code null}
     */

    public MutableIndexedMapFileLookupSession(org.osid.filing.Directory directory,
                                                  java.util.Collection<? extends org.osid.filing.File> files) {

        this(directory);
        putFiles(files);
        return;
    }
    

    /**
     *  Makes a {@code File} available in this session.
     *
     *  @param  file a file
     *  @throws org.osid.NullArgumentException {@code file{@code  is
     *          {@code null}
     */

    @Override
    public void putFile(org.osid.filing.File file) {
        super.putFile(file);
        return;
    }


    /**
     *  Makes an array of files available in this session.
     *
     *  @param  files an array of files
     *  @throws org.osid.NullArgumentException {@code files{@code 
     *          is {@code null}
     */

    @Override
    public void putFiles(org.osid.filing.File[] files) {
        super.putFiles(files);
        return;
    }


    /**
     *  Makes collection of files available in this session.
     *
     *  @param  files a collection of files
     *  @throws org.osid.NullArgumentException {@code file{@code  is
     *          {@code null}
     */

    @Override
    public void putFiles(java.util.Collection<? extends org.osid.filing.File> files) {
        super.putFiles(files);
        return;
    }


    /**
     *  Removes a File from this session.
     *
     *  @param fileId the {@code Id} of the file
     *  @throws org.osid.NullArgumentException {@code fileId{@code  is
     *          {@code null}
     */

    @Override
    public void removeFile(org.osid.id.Id fileId) {
        super.removeFile(fileId);
        return;
    }    
}
