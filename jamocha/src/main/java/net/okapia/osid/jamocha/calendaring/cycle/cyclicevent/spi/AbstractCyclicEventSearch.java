//
// AbstractCyclicEventSearch.java
//
//     A template for making a CyclicEvent Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing cyclic event searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCyclicEventSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.cycle.CyclicEventSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicEventSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.cycle.CyclicEventSearchOrder cyclicEventSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of cyclic events. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  cyclicEventIds list of cyclic events
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCyclicEvents(org.osid.id.IdList cyclicEventIds) {
        while (cyclicEventIds.hasNext()) {
            try {
                this.ids.add(cyclicEventIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCyclicEvents</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of cyclic event Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCyclicEventIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  cyclicEventSearchOrder cyclic event search order 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>cyclicEventSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCyclicEventResults(org.osid.calendaring.cycle.CyclicEventSearchOrder cyclicEventSearchOrder) {
	this.cyclicEventSearchOrder = cyclicEventSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.cycle.CyclicEventSearchOrder getCyclicEventSearchOrder() {
	return (this.cyclicEventSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given cyclic event search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a cyclic event implementing the requested record.
     *
     *  @param cyclicEventSearchRecordType a cyclic event search record
     *         type
     *  @return the cyclic event search record
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicEventSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicEventSearchRecord getCyclicEventSearchRecord(org.osid.type.Type cyclicEventSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.cycle.records.CyclicEventSearchRecord record : this.records) {
            if (record.implementsRecordType(cyclicEventSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicEventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this cyclic event search. 
     *
     *  @param cyclicEventSearchRecord cyclic event search record
     *  @param cyclicEventSearchRecordType cyclicEvent search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCyclicEventSearchRecord(org.osid.calendaring.cycle.records.CyclicEventSearchRecord cyclicEventSearchRecord, 
                                           org.osid.type.Type cyclicEventSearchRecordType) {

        addRecordType(cyclicEventSearchRecordType);
        this.records.add(cyclicEventSearchRecord);        
        return;
    }
}
