//
// MutableIndexedMapProxyIssueLookupSession
//
//    Implements an Issue lookup service backed by a collection of
//    issues indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold;


/**
 *  Implements an Issue lookup service backed by a collection of
 *  issues. The issues are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some issues may be compatible
 *  with more types than are indicated through these issue
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of issues can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyIssueLookupSession
    extends net.okapia.osid.jamocha.core.hold.spi.AbstractIndexedMapIssueLookupSession
    implements org.osid.hold.IssueLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyIssueLookupSession} with
     *  no issue.
     *
     *  @param oubliette the oubliette
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                       org.osid.proxy.Proxy proxy) {
        setOubliette(oubliette);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyIssueLookupSession} with
     *  a single issue.
     *
     *  @param oubliette the oubliette
     *  @param  issue an issue
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code issue}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                       org.osid.hold.Issue issue, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putIssue(issue);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyIssueLookupSession} using
     *  an array of issues.
     *
     *  @param oubliette the oubliette
     *  @param  issues an array of issues
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code issues}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                       org.osid.hold.Issue[] issues, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putIssues(issues);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyIssueLookupSession} using
     *  a collection of issues.
     *
     *  @param oubliette the oubliette
     *  @param  issues a collection of issues
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code issues}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyIssueLookupSession(org.osid.hold.Oubliette oubliette,
                                                       java.util.Collection<? extends org.osid.hold.Issue> issues,
                                                       org.osid.proxy.Proxy proxy) {
        this(oubliette, proxy);
        putIssues(issues);
        return;
    }

    
    /**
     *  Makes an {@code Issue} available in this session.
     *
     *  @param  issue an issue
     *  @throws org.osid.NullArgumentException {@code issue{@code 
     *          is {@code null}
     */

    @Override
    public void putIssue(org.osid.hold.Issue issue) {
        super.putIssue(issue);
        return;
    }


    /**
     *  Makes an array of issues available in this session.
     *
     *  @param  issues an array of issues
     *  @throws org.osid.NullArgumentException {@code issues{@code 
     *          is {@code null}
     */

    @Override
    public void putIssues(org.osid.hold.Issue[] issues) {
        super.putIssues(issues);
        return;
    }


    /**
     *  Makes collection of issues available in this session.
     *
     *  @param  issues a collection of issues
     *  @throws org.osid.NullArgumentException {@code issue{@code 
     *          is {@code null}
     */

    @Override
    public void putIssues(java.util.Collection<? extends org.osid.hold.Issue> issues) {
        super.putIssues(issues);
        return;
    }


    /**
     *  Removes an Issue from this session.
     *
     *  @param issueId the {@code Id} of the issue
     *  @throws org.osid.NullArgumentException {@code issueId{@code  is
     *          {@code null}
     */

    @Override
    public void removeIssue(org.osid.id.Id issueId) {
        super.removeIssue(issueId);
        return;
    }    
}
