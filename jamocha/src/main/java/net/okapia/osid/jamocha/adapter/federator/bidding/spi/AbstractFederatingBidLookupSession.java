//
// AbstractFederatingBidLookupSession.java
//
//     An abstract federating adapter for a BidLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BidLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBidLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.bidding.BidLookupSession>
    implements org.osid.bidding.BidLookupSession {

    private boolean parallel = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();


    /**
     *  Constructs a new <code>AbstractFederatingBidLookupSession</code>.
     */

    protected AbstractFederatingBidLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.bidding.BidLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Bid</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBids() {
        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            if (session.canLookupBids()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Bid</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBidView() {
        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            session.useComparativeBidView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Bid</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBidView() {
        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            session.usePlenaryBidView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include bids in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            session.useFederatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            session.useIsolatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Only bids whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveBidView() {
        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            session.useEffectiveBidView();
        }

        return;
    }


    /**
     *  All bids of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveBidView() {
        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            session.useAnyEffectiveBidView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Bid</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Bid</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Bid</code> and
     *  retained for compatibility.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidId <code>Id</code> of the
     *          <code>Bid</code>
     *  @return the bid
     *  @throws org.osid.NotFoundException <code>bidId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>bidId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Bid getBid(org.osid.id.Id bidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            try {
                return (session.getBid(bidId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(bidId + " not found");
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  bids specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Bids</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, bids are returned that are currently effective.
     *  In any effective mode, effective bids and those currently expired
     *  are returned.
     *
     *  @param  bidIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>bidIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByIds(org.osid.id.IdList bidIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.bidding.bid.MutableBidList ret = new net.okapia.osid.jamocha.bidding.bid.MutableBidList();

        try (org.osid.id.IdList ids = bidIds) {
            while (ids.hasNext()) {
                ret.addBid(getBid(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  bid genus <code>Type</code> which does not include
     *  bids of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently effective.
     *  In any effective mode, effective bids and those currently expired
     *  are returned.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsByGenusType(bidGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  bid genus <code>Type</code> and include any additional
     *  bids with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByParentGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsByParentGenusType(bidGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BidList</code> containing the given
     *  bid record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidRecordType a bid record type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByRecordType(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsByRecordType(bidRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BidList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *  
     *  In active mode, bids are returned that are currently
     *  active. In any status mode, active and inactive bids
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Bid</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.BidList getBidsOnDate(org.osid.calendaring.DateTime from, 
                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of bids corresponding to an auction
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.bidding.BidList getBidsForAuction(org.osid.id.Id auctionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsForAuction(auctionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of bids corresponding to an auction
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionOnDate(org.osid.id.Id auctionId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsForAuctionOnDate(auctionId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.bidding.BidList getBidsForBidder(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsForBidder(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForBidderOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsForBidderOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidder(org.osid.id.Id auctionId,
                                                               org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsForAuctionAndBidder(auctionId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidderOnDate(org.osid.id.Id auctionId,
                                                                     org.osid.id.Id resourceId,
                                                                     org.osid.calendaring.DateTime from,
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBidsForAuctionAndBidderOnDate(auctionId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all winning <code>Bids</code>.
     *
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session. In
     *  both cases, the order of the set is by start effective date.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @return a list of <code> Bids </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getWinningBids());
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all winning bids effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session. In
     *  both cases, the order of the set is by the start of the
     *  effective date.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsOnDate(org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getWinningBidsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all winning bids for an auction.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  auctionId an auction <code> Id </code>
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getWinningBidsForAuction(auctionId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a list of winning bids for an auction and effectiveduring
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  auctionId an auction <code> Id </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> auctionId, from, </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuctionOnDate(org.osid.id.Id auctionId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getWinningBidsForAuctionOnDate(auctionId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all winning bids for a bidder.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resourceId <code> Id </code>
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getWinningBidsForBidder(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of winning bids for a bidder and effective during the
     *  entire given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known bids or an error
     *  results. Otherwise, the returned list may contain only those bids that
     *  are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently effective. In
     *  any effective mode, effective bids and those currently expired are
     *  returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned C <code> ommission </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidderOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getWinningBidsForBidderOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>Bids</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Bids</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList ret = getBidList();

        for (org.osid.bidding.BidLookupSession session : getSessions()) {
            ret.addBidList(session.getBids());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.bidding.bid.FederatingBidList getBidList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.bid.ParallelBidList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.bid.CompositeBidList());
        }
    }
}
