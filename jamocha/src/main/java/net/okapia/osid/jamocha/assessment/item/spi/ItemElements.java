//
// ItemElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.item.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class ItemElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ItemElement Id.
     *
     *  @return the item element Id
     */

    public static org.osid.id.Id getItemEntityId() {
        return (makeEntityId("osid.assessment.Item"));
    }


    /**
     *  Gets the LearningObjectiveIds element Id.
     *
     *  @return the LearningObjectiveIds element Id
     */

    public static org.osid.id.Id getLearningObjectiveIds() {
        return (makeElementId("osid.assessment.item.LearningObjectiveIds"));
    }


    /**
     *  Gets the LearningObjectives element Id.
     *
     *  @return the LearningObjectives element Id
     */

    public static org.osid.id.Id getLearningObjectives() {
        return (makeElementId("osid.assessment.item.LearningObjectives"));
    }


    /**
     *  Gets the QuestionId element Id.
     *
     *  @return the QuestionId element Id
     */

    public static org.osid.id.Id getQuestionId() {
        return (makeElementId("osid.assessment.item.QuestionId"));
    }


    /**
     *  Gets the Question element Id.
     *
     *  @return the Question element Id
     */

    public static org.osid.id.Id getQuestion() {
        return (makeElementId("osid.assessment.item.Question"));
    }


    /**
     *  Gets the AnswerIds element Id.
     *
     *  @return the AnswerIds element Id
     */

    public static org.osid.id.Id getAnswerIds() {
        return (makeElementId("osid.assessment.item.AnswerIds"));
    }


    /**
     *  Gets the Answers element Id.
     *
     *  @return the Answers element Id
     */

    public static org.osid.id.Id getAnswers() {
        return (makeElementId("osid.assessment.item.Answers"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeQueryElementId("osid.assessment.item.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeQueryElementId("osid.assessment.item.Assessment"));
    }


    /**
     *  Gets the BankId element Id.
     *
     *  @return the BankId element Id
     */

    public static org.osid.id.Id getBankId() {
        return (makeQueryElementId("osid.assessment.item.BankId"));
    }


    /**
     *  Gets the Bank element Id.
     *
     *  @return the Bank element Id
     */

    public static org.osid.id.Id getBank() {
        return (makeQueryElementId("osid.assessment.item.Bank"));
    }
}
