//
// AbstractImmutableProgramRequirement.java
//
//     Wraps a mutable ProgramRequirement to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.programrequirement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ProgramRequirement</code> to hide modifiers. This
 *  wrapper provides an immutized ProgramRequirement from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying programRequirement whose state changes are visible.
 */

public abstract class AbstractImmutableProgramRequirement
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.course.requisite.ProgramRequirement {

    private final org.osid.course.requisite.ProgramRequirement programRequirement;


    /**
     *  Constructs a new <code>AbstractImmutableProgramRequirement</code>.
     *
     *  @param programRequirement the program requirement to immutablize
     *  @throws org.osid.NullArgumentException <code>programRequirement</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProgramRequirement(org.osid.course.requisite.ProgramRequirement programRequirement) {
        super(programRequirement);
        this.programRequirement = programRequirement;
        return;
    }


    /**
     *  Gets any <code> Requisites </code> that may be substituted in place of 
     *  this <code> ProgramRequirement. </code> All <code> Requisites </code> 
     *  must be satisifed to be a substitute for this program requirement. 
     *  Inactive <code> Requisites </code> are not evaluated but if no 
     *  applicable requisite exists, then the alternate requisite is not 
     *  satisifed. 
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.programRequirement.getAltRequisites());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.programRequirement.getProgramId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.programRequirement.getProgram());
    }


    /**
     *  Tests if this requirement requires completion of the program. 
     *
     *  @return <code> true </code> if a completion of the program is 
     *          required, <code> false </code> if enrollment in the program is 
     *          required 
     */

    @OSID @Override
    public boolean requiresCompletion() {
        return (this.programRequirement.requiresCompletion());
    }


    /**
     *  Tests if the program must be completed within the required duration. 
     *
     *  @return <code> true </code> if the program has to be completed within 
     *          a required time, <code> false </code> if it could have been 
     *          completed at any time in the past 
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.programRequirement.hasTimeframe());
    }


    /**
     *  Gets the timeframe in which the program has to be completed. A 
     *  negative duration indicates the program had to be completed within the 
     *  specified amount of time in the past. A posiitive duration indicates 
     *  the program must be completed within the specified amount of time in 
     *  the future. A zero duration indicates the program must be completed in 
     *  the current term. 
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        return (this.programRequirement.getTimeframe());
    }


    /**
     *  Tests if a minimum GPA above passing is required in the completion of 
     *  the program or maintained at this level during enrollment. 
     *
     *  @return <code> true </code> if a minimum gpa is required, <code> false 
     *          </code> if the course just has to be passed 
     */

    @OSID @Override
    public boolean hasMinimumGPA() {
        return (this.programRequirement.hasMinimumGPA());
    }


    /**
     *  Gets the scoring system <code> Id </code> for the minimum GPA. 
     *
     *  @return the scoring system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGPA() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumGPASystemId() {
        return (this.programRequirement.getMinimumGPASystemId());
    }


    /**
     *  Gets the scoring system for the minimum GPA. 
     *
     *  @return the scoring system 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGPA() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getMinimumGPASystem()
        throws org.osid.OperationFailedException {

        return (this.programRequirement.getMinimumGPASystem());
    }


    /**
     *  Gets the minimum GPA. 
     *
     *  @return the minimum gpa 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGPA() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumGPA() {
        return (this.programRequirement.getMinimumGPA());
    }


    /**
     *  Tests if a minimum credits earned in the program is required. 
     *
     *  @return <code> true </code> if a minimum credits is required, <code> 
     *          false </code> otehrwise 
     */

    @OSID @Override
    public boolean hasMinimumEarnedCredits() {
        return (this.programRequirement.hasMinimumEarnedCredits());
    }


    /**
     *  Gets the minimum earned credits. 
     *
     *  @return the minimum credits 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasMinimumEarnedCredits() </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumEarnedCredits() {
        return (this.programRequirement.getMinimumEarnedCredits());
    }


    /**
     *  Gets the program requirement record corresponding to the given <code> 
     *  ProgramRequirement </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> programRequirementRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(programRequirementRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  programRequirementRecordType the type of program requirement 
     *          record to retrieve 
     *  @return the program requirement record 
     *  @throws org.osid.NullArgumentException <code> 
     *          programRequirementRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(programRequirementRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.records.ProgramRequirementRecord getProgramRequirementRecord(org.osid.type.Type programRequirementRecordType)
        throws org.osid.OperationFailedException {

        return (this.programRequirement.getProgramRequirementRecord(programRequirementRecordType));
    }
}

