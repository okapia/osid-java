//
// AbstractImmutableRelevancy.java
//
//     Wraps a mutable Relevancy to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ontology.relevancy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Relevancy</code> to hide modifiers. This
 *  wrapper provides an immutized Relevancy from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying relevancy whose state changes are visible.
 */

public abstract class AbstractImmutableRelevancy
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.ontology.Relevancy {

    private final org.osid.ontology.Relevancy relevancy;


    /**
     *  Constructs a new <code>AbstractImmutableRelevancy</code>.
     *
     *  @param relevancy the relevancy to immutablize
     *  @throws org.osid.NullArgumentException <code>relevancy</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRelevancy(org.osid.ontology.Relevancy relevancy) {
        super(relevancy);
        this.relevancy = relevancy;
        return;
    }


    /**
     *  Gets the subject <code> Id. </code> 
     *
     *  @return the subject <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubjectId() {
        return (this.relevancy.getSubjectId());
    }


    /**
     *  Gets the <code> Subject. </code> 
     *
     *  @return the subject 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ontology.Subject getSubject()
        throws org.osid.OperationFailedException {

        return (this.relevancy.getSubject());
    }


    /**
     *  Gets the <code> Id </code> mapped to this <code> Subject. </code> 
     *
     *  @return a mapped <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMappedId() {
        return (this.relevancy.getMappedId());
    }


    /**
     *  Gets the relevancy record corresponding to the given <code> Relevancy 
     *  </code> record <code> Type. </code> This method must is to retrieve an 
     *  object implementing the requested record. The <code> 
     *  relevancyRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(relevancyRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  relevancyRecordType the type of the record to retrieve 
     *  @return the relevancy record 
     *  @throws org.osid.NullArgumentException <code> relevancyRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(relevancyRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancyRecord getRelevancyRecord(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException {

        return (this.relevancy.getRelevancyRecord(relevancyRecordType));
    }
}

