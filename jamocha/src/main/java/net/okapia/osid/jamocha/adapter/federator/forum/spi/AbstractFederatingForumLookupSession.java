//
// AbstractFederatingForumLookupSession.java
//
//     An abstract federating adapter for a ForumLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ForumLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingForumLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.forum.ForumLookupSession>
    implements org.osid.forum.ForumLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingForumLookupSession</code>.
     */

    protected AbstractFederatingForumLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.forum.ForumLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Forum</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupForums() {
        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            if (session.canLookupForums()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Forum</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeForumView() {
        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            session.useComparativeForumView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Forum</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryForumView() {
        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            session.usePlenaryForumView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Forum</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Forum</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Forum</code> and
     *  retained for compatibility.
     *
     *  @param  forumId <code>Id</code> of the
     *          <code>Forum</code>
     *  @return the forum
     *  @throws org.osid.NotFoundException <code>forumId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>forumId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            try {
                return (session.getForum(forumId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(forumId + " not found");
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  forums specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Forums</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  forumIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>forumIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByIds(org.osid.id.IdList forumIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.forum.forum.MutableForumList ret = new net.okapia.osid.jamocha.forum.forum.MutableForumList();

        try (org.osid.id.IdList ids = forumIds) {
            while (ids.hasNext()) {
                ret.addForum(getForum(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  forum genus <code>Type</code> which does not include
     *  forums of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.forum.FederatingForumList ret = getForumList();

        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            ret.addForumList(session.getForumsByGenusType(forumGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  forum genus <code>Type</code> and include any additional
     *  forums with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByParentGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.forum.FederatingForumList ret = getForumList();

        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            ret.addForumList(session.getForumsByParentGenusType(forumGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ForumList</code> containing the given
     *  forum record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  forumRecordType a forum record type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByRecordType(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.forum.FederatingForumList ret = getForumList();

        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            ret.addForumList(session.getForumsByRecordType(forumRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ForumList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known forums or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  forums that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Forum</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.forum.forum.FederatingForumList ret = getForumList();

        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            ret.addForumList(session.getForumsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Forums</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  forums or an error results. Otherwise, the returned list
     *  may contain only those forums that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Forums</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForums()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.forum.forum.FederatingForumList ret = getForumList();

        for (org.osid.forum.ForumLookupSession session : getSessions()) {
            ret.addForumList(session.getForums());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.forum.forum.FederatingForumList getForumList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.forum.forum.ParallelForumList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.forum.forum.CompositeForumList());
        }
    }
}
