//
// AbstractProcessEnablerSearch.java
//
//     A template for making a ProcessEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.processenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing process enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProcessEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.workflow.rules.ProcessEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.ProcessEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.workflow.rules.ProcessEnablerSearchOrder processEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of process enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  processEnablerIds list of process enablers
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProcessEnablers(org.osid.id.IdList processEnablerIds) {
        while (processEnablerIds.hasNext()) {
            try {
                this.ids.add(processEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProcessEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of process enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProcessEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  processEnablerSearchOrder process enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>processEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProcessEnablerResults(org.osid.workflow.rules.ProcessEnablerSearchOrder processEnablerSearchOrder) {
	this.processEnablerSearchOrder = processEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.workflow.rules.ProcessEnablerSearchOrder getProcessEnablerSearchOrder() {
	return (this.processEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given process enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a process enabler implementing the requested record.
     *
     *  @param processEnablerSearchRecordType a process enabler search record
     *         type
     *  @return the process enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.ProcessEnablerSearchRecord getProcessEnablerSearchRecord(org.osid.type.Type processEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.workflow.rules.records.ProcessEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(processEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this process enabler search. 
     *
     *  @param processEnablerSearchRecord process enabler search record
     *  @param processEnablerSearchRecordType processEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcessEnablerSearchRecord(org.osid.workflow.rules.records.ProcessEnablerSearchRecord processEnablerSearchRecord, 
                                           org.osid.type.Type processEnablerSearchRecordType) {

        addRecordType(processEnablerSearchRecordType);
        this.records.add(processEnablerSearchRecord);        
        return;
    }
}
