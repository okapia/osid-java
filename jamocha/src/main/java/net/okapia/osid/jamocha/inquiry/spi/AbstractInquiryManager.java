//
// AbstractInquiryManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInquiryManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.inquiry.InquiryManager,
               org.osid.inquiry.InquiryProxyManager {

    private final Types inquiryRecordTypes                 = new TypeRefSet();
    private final Types inquirySearchRecordTypes           = new TypeRefSet();

    private final Types auditRecordTypes                   = new TypeRefSet();
    private final Types auditSearchRecordTypes             = new TypeRefSet();

    private final Types responseRecordTypes                = new TypeRefSet();
    private final Types responseSearchRecordTypes          = new TypeRefSet();

    private final Types inquestRecordTypes                 = new TypeRefSet();
    private final Types inquestSearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractInquiryManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInquiryManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any action group federation is exposed. Federation is exposed 
     *  when a specific action group may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of action groups appears as a single action group. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if an acknowledgement service is supported for the current 
     *  agent. 
     *
     *  @return <code> true </code> if acknowledgement is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgement() {
        return (false);
    }


    /**
     *  Tests if an acknowledgement notification service is supported for the 
     *  current agent. 
     *
     *  @return <code> true </code> if an acknowledgement notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementNotification() {
        return (false);
    }


    /**
     *  Tests if looking up inquiries is supported. 
     *
     *  @return <code> true </code> if inquiry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryLookup() {
        return (false);
    }


    /**
     *  Tests if querying inquiries is supported. 
     *
     *  @return <code> true </code> if inquiry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryQuery() {
        return (false);
    }


    /**
     *  Tests if searching inquiries is supported. 
     *
     *  @return <code> true </code> if inquiry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquirySearch() {
        return (false);
    }


    /**
     *  Tests if an inquiry administrative service is supported. 
     *
     *  @return <code> true </code> if inquiry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryAdmin() {
        return (false);
    }


    /**
     *  Tests if an inquiry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if inquiry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryNotification() {
        return (false);
    }


    /**
     *  Tests if an inquiry inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an inquiry inquest lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryInquest() {
        return (false);
    }


    /**
     *  Tests if an inquiry inquest assignment service is supported. 
     *
     *  @return <code> true </code> if an inquiry to inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryInquestAssignment() {
        return (false);
    }


    /**
     *  Tests if an inquiry smart inquest service is supported. 
     *
     *  @return <code> true </code> if a smart inquest service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquirySmartInquest() {
        return (false);
    }


    /**
     *  Tests if looking up audits is supported. 
     *
     *  @return <code> true </code> if audit lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditLookup() {
        return (false);
    }


    /**
     *  Tests if querying audits is supported. 
     *
     *  @return <code> true </code> if audit query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditQuery() {
        return (false);
    }


    /**
     *  Tests if searching audits is supported. 
     *
     *  @return <code> true </code> if audit search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditSearch() {
        return (false);
    }


    /**
     *  Tests if audit administrative service is supported. 
     *
     *  @return <code> true </code> if audit administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditAdmin() {
        return (false);
    }


    /**
     *  Tests if an audit notification service is supported. 
     *
     *  @return <code> true </code> if audit notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditNotification() {
        return (false);
    }


    /**
     *  Tests if an audit inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit inquest lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditInquest() {
        return (false);
    }


    /**
     *  Tests if an audit inquest service is supported. 
     *
     *  @return <code> true </code> if audit to inquest assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditInquestAssignment() {
        return (false);
    }


    /**
     *  Tests if an audit smart inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit smart inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditSmartInquest() {
        return (false);
    }


    /**
     *  Tests if looking up responses is supported. 
     *
     *  @return <code> true </code> if response lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseLookup() {
        return (false);
    }


    /**
     *  Tests if querying responses is supported. 
     *
     *  @return <code> true </code> if response query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseQuery() {
        return (false);
    }


    /**
     *  Tests if searching responses is supported. 
     *
     *  @return <code> true </code> if response search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseSearch() {
        return (false);
    }


    /**
     *  Tests if response <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if response administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseAdmin() {
        return (false);
    }


    /**
     *  Tests if a response <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if response notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseNotification() {
        return (false);
    }


    /**
     *  Tests if a response inquest lookup service is supported. 
     *
     *  @return <code> true </code> if a response inquest lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseInquest() {
        return (false);
    }


    /**
     *  Tests if a response inquest assignment service is supported. 
     *
     *  @return <code> true </code> if a response to inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseInquestAssignment() {
        return (false);
    }


    /**
     *  Tests if a response smart inquest service is supported. 
     *
     *  @return <code> true </code> if a response smart inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseSmartInquest() {
        return (false);
    }


    /**
     *  Tests if looking up inquests is supported. 
     *
     *  @return <code> true </code> if inquest lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestLookup() {
        return (false);
    }


    /**
     *  Tests if querying inquests is supported. 
     *
     *  @return <code> true </code> if an inquest query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Tests if searching inquests is supported. 
     *
     *  @return <code> true </code> if inquest search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestSearch() {
        return (false);
    }


    /**
     *  Tests if inquest administrative service is supported. 
     *
     *  @return <code> true </code> if inquest administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestAdmin() {
        return (false);
    }


    /**
     *  Tests if an inquest <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if inquest notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of an inquest hierarchy traversal service. 
     *
     *  @return <code> true </code> if inquest hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of an inquest hierarchy design service. 
     *
     *  @return <code> true </code> if inquest hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a inquiry batch service. 
     *
     *  @return <code> true </code> if inquiry batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a inquiry rules service. 
     *
     *  @return <code> true </code> if inquiry rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Inquiry </code> record types. 
     *
     *  @return a list containing the supported <code> Inquiry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquiryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inquiryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Inquiry </code> record type is supported. 
     *
     *  @param  inquiryRecordType a <code> Type </code> indicating a <code> 
     *          Inquiry </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquiryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquiryRecordType(org.osid.type.Type inquiryRecordType) {
        return (this.inquiryRecordTypes.contains(inquiryRecordType));
    }


    /**
     *  Adds support for an inquiry record type.
     *
     *  @param inquiryRecordType an inquiry record type
     *  @throws org.osid.NullArgumentException
     *  <code>inquiryRecordType</code> is <code>null</code>
     */

    protected void addInquiryRecordType(org.osid.type.Type inquiryRecordType) {
        this.inquiryRecordTypes.add(inquiryRecordType);
        return;
    }


    /**
     *  Removes support for an inquiry record type.
     *
     *  @param inquiryRecordType an inquiry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inquiryRecordType</code> is <code>null</code>
     */

    protected void removeInquiryRecordType(org.osid.type.Type inquiryRecordType) {
        this.inquiryRecordTypes.remove(inquiryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Inquiry </code> search types. 
     *
     *  @return a list containing the supported <code> Inquiry </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquirySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inquirySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Inquiry </code> search type is supported. 
     *
     *  @param  inquirySearchRecordType a <code> Type </code> indicating a 
     *          <code> Inquiry </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquirySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquirySearchRecordType(org.osid.type.Type inquirySearchRecordType) {
        return (this.inquirySearchRecordTypes.contains(inquirySearchRecordType));
    }


    /**
     *  Adds support for an inquiry search record type.
     *
     *  @param inquirySearchRecordType an inquiry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>inquirySearchRecordType</code> is <code>null</code>
     */

    protected void addInquirySearchRecordType(org.osid.type.Type inquirySearchRecordType) {
        this.inquirySearchRecordTypes.add(inquirySearchRecordType);
        return;
    }


    /**
     *  Removes support for an inquiry search record type.
     *
     *  @param inquirySearchRecordType an inquiry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inquirySearchRecordType</code> is <code>null</code>
     */

    protected void removeInquirySearchRecordType(org.osid.type.Type inquirySearchRecordType) {
        this.inquirySearchRecordTypes.remove(inquirySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Audit </code> record types. 
     *
     *  @return a list containing the supported <code> Audit </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auditRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Audit </code> record type is supported. 
     *
     *  @param  auditRecordType a <code> Type </code> indicating a <code> 
     *          Audit </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auditRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditRecordType(org.osid.type.Type auditRecordType) {
        return (this.auditRecordTypes.contains(auditRecordType));
    }


    /**
     *  Adds support for an audit record type.
     *
     *  @param auditRecordType an audit record type
     *  @throws org.osid.NullArgumentException
     *  <code>auditRecordType</code> is <code>null</code>
     */

    protected void addAuditRecordType(org.osid.type.Type auditRecordType) {
        this.auditRecordTypes.add(auditRecordType);
        return;
    }


    /**
     *  Removes support for an audit record type.
     *
     *  @param auditRecordType an audit record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auditRecordType</code> is <code>null</code>
     */

    protected void removeAuditRecordType(org.osid.type.Type auditRecordType) {
        this.auditRecordTypes.remove(auditRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Audit </code> search record types. 
     *
     *  @return a list containing the supported <code> Audit </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auditSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Audit </code> search record type is 
     *  supported. 
     *
     *  @param  auditSearchRecordType a <code> Type </code> indicating a 
     *          <code> Audit </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auditSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditSearchRecordType(org.osid.type.Type auditSearchRecordType) {
        return (this.auditSearchRecordTypes.contains(auditSearchRecordType));
    }


    /**
     *  Adds support for an audit search record type.
     *
     *  @param auditSearchRecordType an audit search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auditSearchRecordType</code> is <code>null</code>
     */

    protected void addAuditSearchRecordType(org.osid.type.Type auditSearchRecordType) {
        this.auditSearchRecordTypes.add(auditSearchRecordType);
        return;
    }


    /**
     *  Removes support for an audit search record type.
     *
     *  @param auditSearchRecordType an audit search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auditSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuditSearchRecordType(org.osid.type.Type auditSearchRecordType) {
        this.auditSearchRecordTypes.remove(auditSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Response </code> record types. 
     *
     *  @return a list containing the supported <code> Response </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.responseRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Response </code> record type is supported. 
     *
     *  @param  responseRecordType a <code> Type </code> indicating a <code> 
     *          Response </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseRecordType(org.osid.type.Type responseRecordType) {
        return (this.responseRecordTypes.contains(responseRecordType));
    }


    /**
     *  Adds support for a response record type.
     *
     *  @param responseRecordType a response record type
     *  @throws org.osid.NullArgumentException
     *  <code>responseRecordType</code> is <code>null</code>
     */

    protected void addResponseRecordType(org.osid.type.Type responseRecordType) {
        this.responseRecordTypes.add(responseRecordType);
        return;
    }


    /**
     *  Removes support for a response record type.
     *
     *  @param responseRecordType a response record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>responseRecordType</code> is <code>null</code>
     */

    protected void removeResponseRecordType(org.osid.type.Type responseRecordType) {
        this.responseRecordTypes.remove(responseRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Response </code> search types. 
     *
     *  @return a list containing the supported <code> Response </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.responseSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Response </code> search type is supported. 
     *
     *  @param  responseSearchRecordType a <code> Type </code> indicating a 
     *          <code> Response </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseSearchRecordType(org.osid.type.Type responseSearchRecordType) {
        return (this.responseSearchRecordTypes.contains(responseSearchRecordType));
    }


    /**
     *  Adds support for a response search record type.
     *
     *  @param responseSearchRecordType a response search record type
     *  @throws org.osid.NullArgumentException
     *  <code>responseSearchRecordType</code> is <code>null</code>
     */

    protected void addResponseSearchRecordType(org.osid.type.Type responseSearchRecordType) {
        this.responseSearchRecordTypes.add(responseSearchRecordType);
        return;
    }


    /**
     *  Removes support for a response search record type.
     *
     *  @param responseSearchRecordType a response search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>responseSearchRecordType</code> is <code>null</code>
     */

    protected void removeResponseSearchRecordType(org.osid.type.Type responseSearchRecordType) {
        this.responseSearchRecordTypes.remove(responseSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Inquest </code> record types. 
     *
     *  @return a list containing the supported <code> Inquest </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquestRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inquestRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Inquest </code> record type is supported. 
     *
     *  @param  inquestRecordType a <code> Type </code> indicating a <code> 
     *          Inquest </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquestRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquestRecordType(org.osid.type.Type inquestRecordType) {
        return (this.inquestRecordTypes.contains(inquestRecordType));
    }


    /**
     *  Adds support for an inquest record type.
     *
     *  @param inquestRecordType an inquest record type
     *  @throws org.osid.NullArgumentException
     *  <code>inquestRecordType</code> is <code>null</code>
     */

    protected void addInquestRecordType(org.osid.type.Type inquestRecordType) {
        this.inquestRecordTypes.add(inquestRecordType);
        return;
    }


    /**
     *  Removes support for an inquest record type.
     *
     *  @param inquestRecordType an inquest record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inquestRecordType</code> is <code>null</code>
     */

    protected void removeInquestRecordType(org.osid.type.Type inquestRecordType) {
        this.inquestRecordTypes.remove(inquestRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Inquest </code> search record types. 
     *
     *  @return a list containing the supported <code> Inquest </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquestSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inquestSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Inquest </code> search record type is 
     *  supported. 
     *
     *  @param  inquestSearchRecordType a <code> Type </code> indicating a 
     *          <code> Inquest </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquestSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquestSearchRecordType(org.osid.type.Type inquestSearchRecordType) {
        return (this.inquestSearchRecordTypes.contains(inquestSearchRecordType));
    }


    /**
     *  Adds support for an inquest search record type.
     *
     *  @param inquestSearchRecordType an inquest search record type
     *  @throws org.osid.NullArgumentException
     *  <code>inquestSearchRecordType</code> is <code>null</code>
     */

    protected void addInquestSearchRecordType(org.osid.type.Type inquestSearchRecordType) {
        this.inquestSearchRecordTypes.add(inquestSearchRecordType);
        return;
    }


    /**
     *  Removes support for an inquest search record type.
     *
     *  @param inquestSearchRecordType an inquest search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inquestSearchRecordType</code> is <code>null</code>
     */

    protected void removeInquestSearchRecordType(org.osid.type.Type inquestSearchRecordType) {
        this.inquestSearchRecordTypes.remove(inquestSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement service. 
     *
     *  @return an <code> AcknowledgementSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementSession getAcknowledgementSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAcknowledgementSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AcknowledgementSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgemen() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementSession getAcknowledgementSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAcknowledgementSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @return an <code> AcknowledgementSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgement() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementSession getAcknowledgementSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAcknowledgementSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AcknowledgementSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgemen() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementSession getAcknowledgementSessionForInquest(org.osid.id.Id inquestId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAcknowledgementSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement notification service for inquiries related to the 
     *  authentciated agent. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @return an <code> AcknowledgementNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementNotificationSession getAcknowledgementNotificationSession(org.osid.inquiry.InquiryReceiver inquiryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAcknowledgementNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement notification service for resources related to the 
     *  authenticated agent. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AcknowledgementNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgemenNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementNotificationSession getAcknowledgementNotificationSession(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAcknowledgementNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement notification service for the given inquest for 
     *  inquiries related to the authentciated agent. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AcknowledgementNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementNotificationSession getAcknowledgementNotificationSessionForInquest(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                                               org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAcknowledgementNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the 
     *  acknowledgement notification service for the given inquest for 
     *  resources related to the authenticated agent. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AcknowledgementNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver, 
     *          inquestId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgemenNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AcknowledgementNotificationSession getAcknowledgementNotificationSessionForInquest(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                                               org.osid.id.Id inquestId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAcknowledgementNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry lookup 
     *  service. 
     *
     *  @return an <code> InquiryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryLookupSession getInquiryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryLookupSession getInquiryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry lookup 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryLookupSession getInquiryLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry lookup 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryLookupSession getInquiryLookupSessionForInquest(org.osid.id.Id inquestId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry query 
     *  service. 
     *
     *  @return an <code> InquiryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuerySession getInquiryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuerySession getInquiryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuerySession getInquiryQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuerySession getInquiryQuerySessionForInquest(org.osid.id.Id inquestId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry search 
     *  service. 
     *
     *  @return an <code> InquirySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquirySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchSession getInquirySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquirySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the
     *  inquiry search service.
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquirySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code>
     *          supportsInquirySearch() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchSession getInquirySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquirySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry search 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquirySearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquirySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchSession getInquirySearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquirySearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry search 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquirySearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquirySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySearchSession getInquirySearchSessionForInquest(org.osid.id.Id inquestId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquirySearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  administration service. 
     *
     *  @return an <code> InquiryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryAdminSession getInquiryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryAdminSession getInquiryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryAdminSession getInquiryAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryAdminSession getInquiryAdminSessionForInquest(org.osid.id.Id inquestId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  notification service. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @return an <code> InquiryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryNotificationSession getInquiryNotificationSession(org.osid.inquiry.InquiryReceiver inquiryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  notification service. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryNotificationSession getInquiryNotificationSession(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  notification service for the given inquest. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver </code> 
     *          or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryNotificationSession getInquiryNotificationSessionForInquest(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                               org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  notification service for the given inquest. 
     *
     *  @param  inquiryReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquiryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryReceiver, 
     *          inquestId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryNotificationSession getInquiryNotificationSessionForInquest(org.osid.inquiry.InquiryReceiver inquiryReceiver, 
                                                                                               org.osid.id.Id inquestId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inquiry/inquest 
     *  mappings. 
     *
     *  @return an <code> InquiryInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryInquestSession getInquiryInquestSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inquiry/inquest 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryInquestSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryInquestSession getInquiryInquestSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  inquiries to inquests. 
     *
     *  @return an <code> InquiryInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryInquestAssignmentSession getInquiryInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  inquiries to inquests. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquiryInquestAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryInquestAssignmentSession getInquiryInquestAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage inquiry smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquirySmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquirySmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySmartInquestSession getInquirySmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquirySmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage inquiry smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InquirySmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquirySmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquirySmartInquestSession getInquirySmartInquestSession(org.osid.id.Id inquestId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquirySmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit lookup 
     *  service. 
     *
     *  @return an <code> AuditLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditLookupSession getAuditLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditLookupSession getAuditLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit lookup 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditLookupSession getAuditLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit lookup 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditLookupSession getAuditLookupSessionForInquest(org.osid.id.Id inquestId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit query 
     *  service. 
     *
     *  @return an <code> AuditQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuerySession getAuditQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuerySession getAuditQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuerySession getAuditQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuerySession getAuditQuerySessionForInquest(org.osid.id.Id inquestId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit search 
     *  service. 
     *
     *  @return an <code> AuditSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchSession getAuditSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchSession getAuditSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit search 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditSearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchSession getAuditSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit search 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditSearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchSession getAuditSearchSessionForInquest(org.osid.id.Id inquestId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  administration service. 
     *
     *  @return an <code> AuditAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditAdminSession getAuditAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditAdminSession getAuditAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditAdminSession getAuditAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @param  proxy a proxy 
     *  @return an <code> AuditAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditAdminSession getAuditAdminSessionForInquest(org.osid.id.Id inquestId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditAdminSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  notification service. 
     *
     *  @param  auditReceiver the notification callback 
     *  @return an <code> AuditNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auditReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditNotificationSession getAuditNotificationSession(org.osid.inquiry.AuditReceiver auditReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  notification service. 
     *
     *  @param  auditReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuditNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auditReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditNotificationSession getAuditNotificationSession(org.osid.inquiry.AuditReceiver auditReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  notification service for the given inquest. 
     *
     *  @param  auditReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auditReceiver </code> or 
     *          <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditNotificationSession getAuditNotificationSessionForInquest(org.osid.inquiry.AuditReceiver auditReceiver, 
                                                                                           org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit 
     *  notification service for the given inquest. 
     *
     *  @param  auditReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuditNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auditReceiver, inquestId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditNotificationSession getAuditNotificationSessionForInquest(org.osid.inquiry.AuditReceiver auditReceiver, 
                                                                                           org.osid.id.Id inquestId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup audit/inquest mappings. 
     *
     *  @return an <code> AuditInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditInquest() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditInquestSession getAuditInquestSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup audit/inquest mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditInquestSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuditInquest() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditInquestSession getAuditInquestSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning audits 
     *  to inquests. 
     *
     *  @return an <code> AuditInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditInquestAssignmentSession getAuditInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning audits 
     *  to inquests. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuditInquestAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditInquestAssignmentSession getAuditInquestAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage audit smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @return an <code> AuditSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditSmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSmartInquestSession getAuditSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getAuditSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage audit smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @param  proxy a proxy 
     *  @return an <code> AuditSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditSmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSmartInquestSession getAuditSmartInquestSession(org.osid.id.Id inquestId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getAuditSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  lookup service. 
     *
     *  @return a <code> ResponseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseLookupSession getResponseLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResponseLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseLookupSession getResponseLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @return a <code> ResponseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseLookupSession getResponseLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the inquest 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseLookupSession getResponseLookupSessionForInquest(org.osid.id.Id inquestId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseLookupSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response query 
     *  service. 
     *
     *  @return a <code> ResponseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuerySession getResponseQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResponseQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuerySession getResponseQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuerySession getResponseQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response query 
     *  service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseQuerySession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuerySession getResponseQuerySessionForInquest(org.osid.id.Id inquestId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseQuerySessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  search service. 
     *
     *  @return a <code> ResponseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSearchSession getResponseSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResponseSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSearchSession getResponseSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  search service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseSearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSearchSession getResponseSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  search service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseSearchSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSearchSession getResponseSearchSessionForInquest(org.osid.id.Id inquestId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseSearchSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  administration service. 
     *
     *  @return a <code> ResponseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseAdminSession getResponseAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResponseAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseAdminSession getResponseAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Dostributor 
     *          </code> 
     *  @return a <code> ResponseAdminSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseAdminSession getResponseAdminSessionForInput(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseAdminSessionForInput not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  administration service for the given input. 
     *
     *  @param  inputId the <code> Id </code> of the <code> Input </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseAdminSession </code> 
     *  @throws org.osid.NotFoundException no input found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResponseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseAdminSession getResponseAdminSessionForInput(org.osid.id.Id inputId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseAdminSessionForInput not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  notification service. 
     *
     *  @param  responseReceiver the notification callback 
     *  @return a <code> ResponseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> responseReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseNotificationSession getResponseNotificationSession(org.osid.inquiry.ResponseReceiver responseReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  notification service. 
     *
     *  @param  responseReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> responseReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseNotificationSession getResponseNotificationSession(org.osid.inquiry.ResponseReceiver responseReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  notification service for the given inquest. 
     *
     *  @param  responseReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> responseReceiver </code> 
     *          or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseNotificationSession getResponseNotificationSessionForInquest(org.osid.inquiry.ResponseReceiver responseReceiver, 
                                                                                                 org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the response 
     *  notification service for the given inquest. 
     *
     *  @param  responseReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> responseReceiver, 
     *          </code> <code> inquestId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseNotificationSession getResponseNotificationSessionForInquest(org.osid.inquiry.ResponseReceiver responseReceiver, 
                                                                                                 org.osid.id.Id inquestId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseNotificationSessionForInquest not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup response/inquest 
     *  mappings. 
     *
     *  @return a <code> ResponseInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseInquestSession getResponseInquestSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup response/inquest 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResponseInquestSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseInquestSession getResponseInquestSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  responses to inquests. 
     *
     *  @return a <code> ResponseInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseInquestAssignmentSession getResponseInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  responses to inquests. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResponseInquestAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseInquestAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseInquestAssignmentSession getResponseInquestAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseInquestAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return a <code> ResponseSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSmartInquestSession getResponseSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getResponseSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResponseSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponseSmartInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseSmartInquestSession getResponseSmartInquestSession(org.osid.id.Id inquestId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getResponseSmartInquestSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest lookup 
     *  service. 
     *
     *  @return an <code> InquestLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestLookupSession getInquestLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquestLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquestLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestLookupSession getInquestLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquestLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest query 
     *  service. 
     *
     *  @return an <code> InquestQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuerySession getInquestQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquestQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquestQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuerySession getInquestQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquestQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest search 
     *  service. 
     *
     *  @return an <code> InquestSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestSearchSession getInquestSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquestSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquestSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestSearchSession getInquestSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquestSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  administrative service. 
     *
     *  @return an <code> InquestAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestAdminSession getInquestAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquestAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquestAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquestAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestAdminSession getInquestAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquestAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  notification service. 
     *
     *  @param  inquestReceiver the notification callback 
     *  @return an <code> InquestNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquestReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestNotificationSession getInquestNotificationSession(org.osid.inquiry.InquestReceiver inquestReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquestNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  notification service. 
     *
     *  @param  inquestReceiver the notification callback 
     *  @param  proxy an proxy 
     *  @return an <code> InquestNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquestReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestNotificationSession getInquestNotificationSession(org.osid.inquiry.InquestReceiver inquestReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquestNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  hierarchy service. 
     *
     *  @return an <code> InquestHierarchySession </code> for inquests 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestHierarchySession getInquestHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquestHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InquestHierarchySession </code> for inquests 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestHierarchySession getInquestHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquestHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  hierarchy design service. 
     *
     *  @return an <code> HierarchyDesignSession </code> for inquests 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestHierarchyDesignSession getInquestHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquestHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquest 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for inquests 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquestHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestHierarchyDesignSession getInquestHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquestHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> InquiryBatchManager. </code> 
     *
     *  @return an <code> InquiryBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquiryBatchManager getInquiryBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryBatchManager not implemented");
    }


    /**
     *  Gets the <code> InquiryBatchProxyManager. </code> 
     *
     *  @return an <code> InquiryBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.batch.InquiryBatchProxyManager getInquiryBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> InquiryRulesManager. </code> 
     *
     *  @return an <code> InquiryRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryRulesManager getInquiryRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryManager.getInquiryRulesManager not implemented");
    }


    /**
     *  Gets the <code> InquiryRulesProxyManager. </code> 
     *
     *  @return an <code> InquiryRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryRulesProxyManager getInquiryRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inquiry.InquiryProxyManager.getInquiryRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.inquiryRecordTypes.clear();
        this.inquiryRecordTypes.clear();

        this.inquirySearchRecordTypes.clear();
        this.inquirySearchRecordTypes.clear();

        this.auditRecordTypes.clear();
        this.auditRecordTypes.clear();

        this.auditSearchRecordTypes.clear();
        this.auditSearchRecordTypes.clear();

        this.responseRecordTypes.clear();
        this.responseRecordTypes.clear();

        this.responseSearchRecordTypes.clear();
        this.responseSearchRecordTypes.clear();

        this.inquestRecordTypes.clear();
        this.inquestRecordTypes.clear();

        this.inquestSearchRecordTypes.clear();
        this.inquestSearchRecordTypes.clear();

        return;
    }
}
