//
// AbstractMapCyclicTimePeriodLookupSession
//
//    A simple framework for providing a CyclicTimePeriod lookup service
//    backed by a fixed collection of cyclic time periods.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CyclicTimePeriod lookup service backed by a
 *  fixed collection of cyclic time periods. The cyclic time periods are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CyclicTimePeriods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.calendaring.cycle.spi.AbstractCyclicTimePeriodLookupSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriods = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.cycle.CyclicTimePeriod>());


    /**
     *  Makes a <code>CyclicTimePeriod</code> available in this session.
     *
     *  @param  cyclicTimePeriod a cyclic time period
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriod<code>
     *          is <code>null</code>
     */

    protected void putCyclicTimePeriod(org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod) {
        this.cyclicTimePeriods.put(cyclicTimePeriod.getId(), cyclicTimePeriod);
        return;
    }


    /**
     *  Makes an array of cyclic time periods available in this session.
     *
     *  @param  cyclicTimePeriods an array of cyclic time periods
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriods<code>
     *          is <code>null</code>
     */

    protected void putCyclicTimePeriods(org.osid.calendaring.cycle.CyclicTimePeriod[] cyclicTimePeriods) {
        putCyclicTimePeriods(java.util.Arrays.asList(cyclicTimePeriods));
        return;
    }


    /**
     *  Makes a collection of cyclic time periods available in this session.
     *
     *  @param  cyclicTimePeriods a collection of cyclic time periods
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriods<code>
     *          is <code>null</code>
     */

    protected void putCyclicTimePeriods(java.util.Collection<? extends org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriods) {
        for (org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod : cyclicTimePeriods) {
            this.cyclicTimePeriods.put(cyclicTimePeriod.getId(), cyclicTimePeriod);
        }

        return;
    }


    /**
     *  Removes a CyclicTimePeriod from this session.
     *
     *  @param  cyclicTimePeriodId the <code>Id</code> of the cyclic time period
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriodId<code> is
     *          <code>null</code>
     */

    protected void removeCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId) {
        this.cyclicTimePeriods.remove(cyclicTimePeriodId);
        return;
    }


    /**
     *  Gets the <code>CyclicTimePeriod</code> specified by its <code>Id</code>.
     *
     *  @param  cyclicTimePeriodId <code>Id</code> of the <code>CyclicTimePeriod</code>
     *  @return the cyclicTimePeriod
     *  @throws org.osid.NotFoundException <code>cyclicTimePeriodId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriodId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriod getCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod = this.cyclicTimePeriods.get(cyclicTimePeriodId);
        if (cyclicTimePeriod == null) {
            throw new org.osid.NotFoundException("cyclicTimePeriod not found: " + cyclicTimePeriodId);
        }

        return (cyclicTimePeriod);
    }


    /**
     *  Gets all <code>CyclicTimePeriods</code>. In plenary mode, the returned
     *  list contains all known cyclicTimePeriods or an error
     *  results. Otherwise, the returned list may contain only those
     *  cyclicTimePeriods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CyclicTimePeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.ArrayCyclicTimePeriodList(this.cyclicTimePeriods.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.cyclicTimePeriods.clear();
        super.close();
        return;
    }
}
