//
// AbstractMutableProgramEntry.java
//
//     Defines a mutable ProgramEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>ProgramEntry</code>.
 */

public abstract class AbstractMutableProgramEntry
    extends net.okapia.osid.jamocha.course.chronicle.programentry.spi.AbstractProgramEntry
    implements org.osid.course.chronicle.ProgramEntry,
               net.okapia.osid.jamocha.builder.course.chronicle.programentry.ProgramEntryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this program entry. 
     *
     *  @param record program entry record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addProgramEntryRecord(org.osid.course.chronicle.records.ProgramEntryRecord record, org.osid.type.Type recordType) {
        super.addProgramEntryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this program entry is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this program entry ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this program entry.
     *
     *  @param displayName the name for this program entry
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this program entry.
     *
     *  @param description the description of this program entry
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this program entry
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    @Override
    public void setStudent(org.osid.resource.Resource student) {
        super.setStudent(student);
        return;
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    @Override
    public void setProgram(org.osid.course.program.Program program) {
        super.setProgram(program);
        return;
    }


    /**
     *  Sets the admission date.
     *
     *  @param date an admission date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setAdmissionDate(org.osid.calendaring.DateTime date) {
        super.setAdmissionDate(date);
        return;
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    @Override
    public void setTerm(org.osid.course.Term term) {
        super.setTerm(term);
        return;
    }


    /**
     *  Sets the credit scale.
     *
     *  @param scale a credit scale
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreditScale(org.osid.grading.GradeSystem scale) {
        super.setCreditScale(scale);
        return;
    }


    /**
     *  Sets the credits earned.
     *
     *  @param credits a credits earned
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreditsEarned(java.math.BigDecimal credits) {
        super.setCreditsEarned(credits);
        return;
    }


    /**
     *  Sets the GPA scale.
     *
     *  @param scale a GPA scale
     *  @throws org.osid.NullArgumentException <code>scale</code>
     *          is <code>null</code>
     */

    @Override
    public void setGPAScale(org.osid.grading.GradeSystem scale) {
        super.setGPAScale(scale);
        return;
    }


    /**
     *  Sets the GPA.
     *
     *  @param gpa a gpa
     *  @throws org.osid.NullArgumentException <code>gpa</code> is
     *          <code>null</code>
     */

    @Override
    public void setGPA(java.math.BigDecimal gpa) {
        super.setGPA(gpa);
        return;
    }


    /**
     *  Adds an enrollment.
     *
     *  @param enrollment an enrollment
     *  @throws org.osid.NullArgumentException
     *          <code>enrollment</code> is <code>null</code>
     */

    @Override
    public void addEnrollment(org.osid.course.program.Enrollment enrollment) {
        super.addEnrollment(enrollment);
        return;
    }


    /**
     *  Sets all the enrollments.
     *
     *  @param enrollments a collection of enrollments
     *  @throws org.osid.NullArgumentException
     *          <code>enrollments</code> is <code>null</code>
     */

    @Override
    public void setEnrollments(java.util.Collection<org.osid.course.program.Enrollment> enrollments) {
        super.setEnrollments(enrollments);
        return;
    }
}

