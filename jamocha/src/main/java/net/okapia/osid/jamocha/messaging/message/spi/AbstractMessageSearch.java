//
// AbstractMessageSearch.java
//
//     A template for making a Message Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.message.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing message searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractMessageSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.messaging.MessageSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.messaging.records.MessageSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.messaging.MessageSearchOrder messageSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of messages. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  messageIds list of messages
     *  @throws org.osid.NullArgumentException
     *          <code>messageIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongMessages(org.osid.id.IdList messageIds) {
        while (messageIds.hasNext()) {
            try {
                this.ids.add(messageIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongMessages</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of message Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getMessageIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  messageSearchOrder message search order 
     *  @throws org.osid.NullArgumentException
     *          <code>messageSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>messageSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderMessageResults(org.osid.messaging.MessageSearchOrder messageSearchOrder) {
	this.messageSearchOrder = messageSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.messaging.MessageSearchOrder getMessageSearchOrder() {
	return (this.messageSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given message search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a message implementing the requested record.
     *
     *  @param messageSearchRecordType a message search record
     *         type
     *  @return the message search record
     *  @throws org.osid.NullArgumentException
     *          <code>messageSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(messageSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MessageSearchRecord getMessageSearchRecord(org.osid.type.Type messageSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.messaging.records.MessageSearchRecord record : this.records) {
            if (record.implementsRecordType(messageSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(messageSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this message search. 
     *
     *  @param messageSearchRecord message search record
     *  @param messageSearchRecordType message search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMessageSearchRecord(org.osid.messaging.records.MessageSearchRecord messageSearchRecord, 
                                           org.osid.type.Type messageSearchRecordType) {

        addRecordType(messageSearchRecordType);
        this.records.add(messageSearchRecord);        
        return;
    }
}
