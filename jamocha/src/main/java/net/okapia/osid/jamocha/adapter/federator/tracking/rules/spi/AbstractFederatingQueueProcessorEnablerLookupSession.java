//
// AbstractFederatingQueueProcessorEnablerLookupSession.java
//
//     An abstract federating adapter for a QueueProcessorEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  QueueProcessorEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingQueueProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.tracking.rules.QueueProcessorEnablerLookupSession>
    implements org.osid.tracking.rules.QueueProcessorEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();


    /**
     *  Constructs a new <code>AbstractFederatingQueueProcessorEnablerLookupSession</code>.
     */

    protected AbstractFederatingQueueProcessorEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.tracking.rules.QueueProcessorEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this 
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>QueueProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueueProcessorEnablers() {
        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            if (session.canLookupQueueProcessorEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>QueueProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueProcessorEnablerView() {
        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            session.useComparativeQueueProcessorEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>QueueProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueProcessorEnablerView() {
        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            session.usePlenaryQueueProcessorEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue processor enablers in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            session.useFederatedFrontOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            session.useIsolatedFrontOfficeView();
        }

        return;
    }


    /**
     *  Only active queue processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueProcessorEnablerView() {
        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            session.useActiveQueueProcessorEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive queue processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueProcessorEnablerView() {
        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            session.useAnyStatusQueueProcessorEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>QueueProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>QueueProcessorEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>QueueProcessorEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @param  queueProcessorEnablerId <code>Id</code> of the
     *          <code>QueueProcessorEnabler</code>
     *  @return the queue processor enabler
     *  @throws org.osid.NotFoundException <code>queueProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnabler getQueueProcessorEnabler(org.osid.id.Id queueProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            try {
                return (session.getQueueProcessorEnabler(queueProcessorEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(queueProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>QueueProcessorEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  queueProcessorEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>QueueProcessorEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @param  queueProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>QueueProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablersByIds(org.osid.id.IdList queueProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.tracking.rules.queueprocessorenabler.MutableQueueProcessorEnablerList ret = new net.okapia.osid.jamocha.tracking.rules.queueprocessorenabler.MutableQueueProcessorEnablerList();

        try (org.osid.id.IdList ids = queueProcessorEnablerIds) {
            while (ids.hasNext()) {
                ret.addQueueProcessorEnabler(getQueueProcessorEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>QueueProcessorEnablerList</code> corresponding to
     *  the given queue processor enabler genus <code>Type</code>
     *  which does not include queue processor enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those queue processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @param  queueProcessorEnablerGenusType a queueProcessorEnabler genus type 
     *  @return the returned <code>QueueProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablersByGenusType(org.osid.type.Type queueProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.FederatingQueueProcessorEnablerList ret = getQueueProcessorEnablerList();

        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            ret.addQueueProcessorEnablerList(session.getQueueProcessorEnablersByGenusType(queueProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueProcessorEnablerList</code> corresponding to
     *  the given queue processor enabler genus <code>Type</code> and
     *  include any additional queue processor enablers with genus
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those queue processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @param  queueProcessorEnablerGenusType a queueProcessorEnabler genus type 
     *  @return the returned <code>QueueProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablersByParentGenusType(org.osid.type.Type queueProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.FederatingQueueProcessorEnablerList ret = getQueueProcessorEnablerList();

        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            ret.addQueueProcessorEnablerList(session.getQueueProcessorEnablersByParentGenusType(queueProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueProcessorEnablerList</code> containing the
     *  given queue processor enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known queue
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those queue processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @param  queueProcessorEnablerRecordType a queueProcessorEnabler record type 
     *  @return the returned <code>QueueProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablersByRecordType(org.osid.type.Type queueProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.FederatingQueueProcessorEnablerList ret = getQueueProcessorEnablerList();

        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            ret.addQueueProcessorEnablerList(session.getQueueProcessorEnablersByRecordType(queueProcessorEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueProcessorEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known queue
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those queue processor enablers
     *  that are accessible through this session.
     *  
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>QueueProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.FederatingQueueProcessorEnablerList ret = getQueueProcessorEnablerList();

        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            ret.addQueueProcessorEnablerList(session.getQueueProcessorEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>QueueProcessorEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known queue
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those queue processor enablers
     *  that are accessible through this session.
     *
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>QueueProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.FederatingQueueProcessorEnablerList ret = getQueueProcessorEnablerList();

        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            ret.addQueueProcessorEnablerList(session.getQueueProcessorEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>QueueProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known queue
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those queue processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, queue processor enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processor enablers are returned.
     *
     *  @return a list of <code>QueueProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.FederatingQueueProcessorEnablerList ret = getQueueProcessorEnablerList();

        for (org.osid.tracking.rules.QueueProcessorEnablerLookupSession session : getSessions()) {
            ret.addQueueProcessorEnablerList(session.getQueueProcessorEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.FederatingQueueProcessorEnablerList getQueueProcessorEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.ParallelQueueProcessorEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueprocessorenabler.CompositeQueueProcessorEnablerList());
        }
    }
}
