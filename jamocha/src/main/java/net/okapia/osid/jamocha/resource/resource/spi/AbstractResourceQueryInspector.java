//
// AbstractResourceQueryInspector.java
//
//     A template for making a ResourceQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for resources.
 */

public abstract class AbstractResourceQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.resource.ResourceQueryInspector {

    private final java.util.Collection<org.osid.resource.records.ResourceQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getGroupTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDemographicTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the containing group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingGroupIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the containing group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getContainingGroupTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAvatarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAvatarTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the resource relationship <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceRelationshipIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource relationship query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQueryInspector[] getResourceRelationshipTerms() {
        return (new org.osid.resource.ResourceRelationshipQueryInspector[0]);
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given resource query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a resource implementing the requested record.
     *
     *  @param resourceRecordType a resource record type
     *  @return the resource query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceQueryInspectorRecord getResourceQueryInspectorRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource query. 
     *
     *  @param resourceQueryInspectorRecord resource query inspector
     *         record
     *  @param resourceRecordType resource record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResourceQueryInspectorRecord(org.osid.resource.records.ResourceQueryInspectorRecord resourceQueryInspectorRecord, 
                                                   org.osid.type.Type resourceRecordType) {

        addRecordType(resourceRecordType);
        nullarg(resourceRecordType, "resource record type");
        this.records.add(resourceQueryInspectorRecord);        
        return;
    }
}
