//
// AbstractIndexedMapSystemLookupSession.java
//
//    A simple framework for providing a System lookup service
//    backed by a fixed collection of systems with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a System lookup service backed by a
 *  fixed collection of systems. The systems are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some systems may be compatible
 *  with more types than are indicated through these system
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Systems</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSystemLookupSession
    extends AbstractMapSystemLookupSession
    implements org.osid.control.SystemLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.System> systemsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.System>());
    private final MultiMap<org.osid.type.Type, org.osid.control.System> systemsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.System>());


    /**
     *  Makes a <code>System</code> available in this session.
     *
     *  @param  system a system
     *  @throws org.osid.NullArgumentException <code>system<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSystem(org.osid.control.System system) {
        super.putSystem(system);

        this.systemsByGenus.put(system.getGenusType(), system);
        
        try (org.osid.type.TypeList types = system.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.systemsByRecord.put(types.getNextType(), system);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of systems available in this session.
     *
     *  @param  systems an array of systems
     *  @throws org.osid.NullArgumentException <code>systems<code>
     *          is <code>null</code>
     */

    @Override
    protected void putSystems(org.osid.control.System[] systems) {
        for (org.osid.control.System system : systems) {
            putSystem(system);
        }

        return;
    }


    /**
     *  Makes a collection of systems available in this session.
     *
     *  @param  systems a collection of systems
     *  @throws org.osid.NullArgumentException <code>systems<code>
     *          is <code>null</code>
     */

    @Override
    protected void putSystems(java.util.Collection<? extends org.osid.control.System> systems) {
        for (org.osid.control.System system : systems) {
            putSystem(system);
        }

        return;
    }


    /**
     *  Removes a system from this session.
     *
     *  @param systemId the <code>Id</code> of the system
     *  @throws org.osid.NullArgumentException <code>systemId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSystem(org.osid.id.Id systemId) {
        org.osid.control.System system;
        try {
            system = getSystem(systemId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.systemsByGenus.remove(system.getGenusType());

        try (org.osid.type.TypeList types = system.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.systemsByRecord.remove(types.getNextType(), system);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSystem(systemId);
        return;
    }


    /**
     *  Gets a <code>SystemList</code> corresponding to the given
     *  system genus <code>Type</code> which does not include
     *  systems of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known systems or an error results. Otherwise,
     *  the returned list may contain only those systems that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  systemGenusType a system genus type 
     *  @return the returned <code>System</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>systemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByGenusType(org.osid.type.Type systemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.system.ArraySystemList(this.systemsByGenus.get(systemGenusType)));
    }


    /**
     *  Gets a <code>SystemList</code> containing the given
     *  system record <code>Type</code>. In plenary mode, the
     *  returned list contains all known systems or an error
     *  results. Otherwise, the returned list may contain only those
     *  systems that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  systemRecordType a system record type 
     *  @return the returned <code>system</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByRecordType(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.system.ArraySystemList(this.systemsByRecord.get(systemRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.systemsByGenus.clear();
        this.systemsByRecord.clear();

        super.close();

        return;
    }
}
