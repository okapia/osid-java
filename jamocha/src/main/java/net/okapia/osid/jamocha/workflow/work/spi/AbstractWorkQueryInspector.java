//
// AbstractWorkQueryInspector.java
//
//     A template for making a WorkQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.work.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for works.
 */

public abstract class AbstractWorkQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.workflow.WorkQueryInspector {

    private final java.util.Collection<org.osid.workflow.records.WorkQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the suspended query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSuspendedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the process <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the process query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQueryInspector[] getProcessTerms() {
        return (new org.osid.workflow.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStepIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given work query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a work implementing the requested record.
     *
     *  @param workRecordType a work record type
     *  @return the work query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.WorkQueryInspectorRecord getWorkQueryInspectorRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.WorkQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Adds a record to this work query. 
     *
     *  @param workQueryInspectorRecord work query inspector
     *         record
     *  @param workRecordType work record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addWorkQueryInspectorRecord(org.osid.workflow.records.WorkQueryInspectorRecord workQueryInspectorRecord, 
                                                   org.osid.type.Type workRecordType) {

        addRecordType(workRecordType);
        nullarg(workRecordType, "work record type");
        this.records.add(workQueryInspectorRecord);        
        return;
    }
}
