//
// AbstractMutableCost.java
//
//     Defines a mutable Cost.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.cost.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Cost</code>.
 */

public abstract class AbstractMutableCost
    extends net.okapia.osid.jamocha.ordering.cost.spi.AbstractCost
    implements org.osid.ordering.Cost,
               net.okapia.osid.jamocha.builder.ordering.cost.CostMiter {


    /** 
     *  Sets the schedule.
     *
     *  @param schedule the schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    @Override
    public void setPriceSchedule(org.osid.ordering.PriceSchedule schedule) {
        super.setPriceSchedule(schedule);
        return;
    }

    
    /** 
     *  Sets the amount.
     *
     *  @param amount the amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void setAmount(org.osid.financials.Currency amount) {
        super.setAmount(amount);
        return;
    }
}

