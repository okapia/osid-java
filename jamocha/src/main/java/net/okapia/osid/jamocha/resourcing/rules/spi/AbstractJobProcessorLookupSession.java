//
// AbstractJobProcessorLookupSession.java
//
//    A starter implementation framework for providing a JobProcessor
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a JobProcessor
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getJobProcessors(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractJobProcessorLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.rules.JobProcessorLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>JobProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJobProcessors() {
        return (true);
    }


    /**
     *  A complete view of the <code>JobProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobProcessorView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>JobProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobProcessorView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job processors in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active job processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive job processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>JobProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JobProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>JobProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorId <code>Id</code> of the
     *          <code>JobProcessor</code>
     *  @return the job processor
     *  @throws org.osid.NotFoundException <code>jobProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>jobProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessor getJobProcessor(org.osid.id.Id jobProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.rules.JobProcessorList jobProcessors = getJobProcessors()) {
            while (jobProcessors.hasNext()) {
                org.osid.resourcing.rules.JobProcessor jobProcessor = jobProcessors.getNextJobProcessor();
                if (jobProcessor.getId().equals(jobProcessorId)) {
                    return (jobProcessor);
                }
            }
        } 

        throw new org.osid.NotFoundException(jobProcessorId + " not found");
    }


    /**
     *  Gets a <code>JobProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>JobProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getJobProcessors()</code>.
     *
     *  @param  jobProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByIds(org.osid.id.IdList jobProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.rules.JobProcessor> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = jobProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getJobProcessor(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("job processor " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessor.LinkedJobProcessorList(ret));
    }


    /**
     *  Gets a <code>JobProcessorList</code> corresponding to the given
     *  job processor genus <code>Type</code> which does not include
     *  job processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getJobProcessors()</code>.
     *
     *  @param  jobProcessorGenusType a jobProcessor genus type 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByGenusType(org.osid.type.Type jobProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessor.JobProcessorGenusFilterList(getJobProcessors(), jobProcessorGenusType));
    }


    /**
     *  Gets a <code>JobProcessorList</code> corresponding to the given
     *  job processor genus <code>Type</code> and include any additional
     *  job processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJobProcessors()</code>.
     *
     *  @param  jobProcessorGenusType a jobProcessor genus type 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByParentGenusType(org.osid.type.Type jobProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getJobProcessorsByGenusType(jobProcessorGenusType));
    }


    /**
     *  Gets a <code>JobProcessorList</code> containing the given
     *  job processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJobProcessors()</code>.
     *
     *  @param  jobProcessorRecordType a jobProcessor record type 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByRecordType(org.osid.type.Type jobProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessor.JobProcessorRecordFilterList(getJobProcessors(), jobProcessorRecordType));
    }


    /**
     *  Gets all <code>JobProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @return a list of <code>JobProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.rules.JobProcessorList getJobProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the job processor list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of job processors
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.rules.JobProcessorList filterJobProcessorsOnViews(org.osid.resourcing.rules.JobProcessorList list)
        throws org.osid.OperationFailedException {

        org.osid.resourcing.rules.JobProcessorList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessor.ActiveJobProcessorFilterList(ret);
        }

        return (ret);
    }
}
