//
// AbstractObstacleEnablerSearch.java
//
//     A template for making an ObstacleEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing obstacle enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractObstacleEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.mapping.path.rules.ObstacleEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.mapping.path.rules.records.ObstacleEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.mapping.path.rules.ObstacleEnablerSearchOrder obstacleEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of obstacle enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  obstacleEnablerIds list of obstacle enablers
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongObstacleEnablers(org.osid.id.IdList obstacleEnablerIds) {
        while (obstacleEnablerIds.hasNext()) {
            try {
                this.ids.add(obstacleEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongObstacleEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of obstacle enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getObstacleEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  obstacleEnablerSearchOrder obstacle enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>obstacleEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderObstacleEnablerResults(org.osid.mapping.path.rules.ObstacleEnablerSearchOrder obstacleEnablerSearchOrder) {
	this.obstacleEnablerSearchOrder = obstacleEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.mapping.path.rules.ObstacleEnablerSearchOrder getObstacleEnablerSearchOrder() {
	return (this.obstacleEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given obstacle enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an obstacle enabler implementing the requested record.
     *
     *  @param obstacleEnablerSearchRecordType an obstacle enabler search record
     *         type
     *  @return the obstacle enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.ObstacleEnablerSearchRecord getObstacleEnablerSearchRecord(org.osid.type.Type obstacleEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.mapping.path.rules.records.ObstacleEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(obstacleEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this obstacle enabler search. 
     *
     *  @param obstacleEnablerSearchRecord obstacle enabler search record
     *  @param obstacleEnablerSearchRecordType obstacleEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObstacleEnablerSearchRecord(org.osid.mapping.path.rules.records.ObstacleEnablerSearchRecord obstacleEnablerSearchRecord, 
                                           org.osid.type.Type obstacleEnablerSearchRecordType) {

        addRecordType(obstacleEnablerSearchRecordType);
        this.records.add(obstacleEnablerSearchRecord);        
        return;
    }
}
