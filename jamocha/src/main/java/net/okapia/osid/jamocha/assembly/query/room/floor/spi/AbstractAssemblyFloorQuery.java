//
// AbstractAssemblyFloorQuery.java
//
//     A FloorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.floor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FloorQuery that stores terms.
 */

public abstract class AbstractAssemblyFloorQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.room.FloorQuery,
               org.osid.room.FloorQueryInspector,
               org.osid.room.FloorSearchOrder {

    private final java.util.Collection<org.osid.room.records.FloorQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.FloorQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.FloorSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyFloorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyFloorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the building <code> Id </code> for this query to match floors 
     *  assigned to buildings. 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        getAssembler().addIdTerm(getBuildingIdColumn(), buildingId, match);
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        getAssembler().clearTerms(getBuildingIdColumn());
        return;
    }


    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (getAssembler().getIdTerms(getBuildingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the building. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuilding(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBuildingColumn(), style);
        return;
    }


    /**
     *  Gets the BuildingId column name.
     *
     * @return the column name
     */

    protected String getBuildingIdColumn() {
        return ("building_id");
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        getAssembler().clearTerms(getBuildingColumn());
        return;
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Tests if a building order is available. 
     *
     *  @return <code> true </code> if a building order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the building order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBuildingSearchOrder() is false");
    }


    /**
     *  Gets the Building column name.
     *
     * @return the column name
     */

    protected String getBuildingColumn() {
        return ("building");
    }


    /**
     *  Sets a floor number. 
     *
     *  @param  number a number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        getAssembler().addStringTerm(getNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches any floor number. 
     *
     *  @param  match <code> true </code> to match floors with any number, 
     *          <code> false </code> to match floors with no number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getNumberColumn(), match);
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        getAssembler().clearTerms(getNumberColumn());
        return;
    }


    /**
     *  Gets the terms terms. 
     *
     *  @return the terms terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (getAssembler().getStringTerms(getNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the building 
     *  number. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumberColumn(), style);
        return;
    }


    /**
     *  Gets the Number column name.
     *
     * @return the column name
     */

    protected String getNumberColumn() {
        return ("number");
    }


    /**
     *  Matches an area within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchGrossArea(java.math.BigDecimal low, 
                               java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getGrossAreaColumn(), low, high, match);
        return;
    }


    /**
     *  Matches any area. 
     *
     *  @param  match <code> true </code> to match floors with any area, 
     *          <code> false </code> to match floors with no area assigned 
     */

    @OSID @Override
    public void matchAnyGrossArea(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getGrossAreaColumn(), match);
        return;
    }


    /**
     *  Clears the area terms. 
     */

    @OSID @Override
    public void clearGrossAreaTerms() {
        getAssembler().clearTerms(getGrossAreaColumn());
        return;
    }


    /**
     *  Gets the gross area terms. 
     *
     *  @return the gross area terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getGrossAreaTerms() {
        return (getAssembler().getDecimalRangeTerms(getGrossAreaColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the gross area. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrossArea(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGrossAreaColumn(), style);
        return;
    }


    /**
     *  Gets the GrossArea column name.
     *
     * @return the column name
     */

    protected String getGrossAreaColumn() {
        return ("gross_area");
    }


    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to floors. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getRoomIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        getAssembler().clearTerms(getRoomIdColumn());
        return;
    }


    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (getAssembler().getIdTerms(getRoomIdColumn()));
    }


    /**
     *  Gets the RoomId column name.
     *
     * @return the column name
     */

    protected String getRoomIdColumn() {
        return ("room_id");
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a floor. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Matches floors. with any room. 
     *
     *  @param  match <code> true </code> to match floors with any room, 
     *          <code> false </code> to match floors with no rooms 
     */

    @OSID @Override
    public void matchAnyRoom(boolean match) {
        getAssembler().addIdWildcardTerm(getRoomColumn(), match);
        return;
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        getAssembler().clearTerms(getRoomColumn());
        return;
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the Room column name.
     *
     * @return the column name
     */

    protected String getRoomColumn() {
        return ("room");
    }


    /**
     *  Sets the floor <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        getAssembler().clearTerms(getCampusIdColumn());
        return;
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (getAssembler().getIdTerms(getCampusIdColumn()));
    }


    /**
     *  Gets the CampusId column name.
     *
     * @return the column name
     */

    protected String getCampusIdColumn() {
        return ("campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        getAssembler().clearTerms(getCampusColumn());
        return;
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the Campus column name.
     *
     * @return the column name
     */

    protected String getCampusColumn() {
        return ("campus");
    }


    /**
     *  Tests if this floor supports the given record
     *  <code>Type</code>.
     *
     *  @param  floorRecordType a floor record type 
     *  @return <code>true</code> if the floorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type floorRecordType) {
        for (org.osid.room.records.FloorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(floorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  floorRecordType the floor record type 
     *  @return the floor query record 
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(floorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorQueryRecord getFloorQueryRecord(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.FloorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(floorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(floorRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  floorRecordType the floor record type 
     *  @return the floor query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(floorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorQueryInspectorRecord getFloorQueryInspectorRecord(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.FloorQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(floorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(floorRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param floorRecordType the floor record type
     *  @return the floor search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(floorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorSearchOrderRecord getFloorSearchOrderRecord(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.FloorSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(floorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(floorRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this floor. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param floorQueryRecord the floor query record
     *  @param floorQueryInspectorRecord the floor query inspector
     *         record
     *  @param floorSearchOrderRecord the floor search order record
     *  @param floorRecordType floor record type
     *  @throws org.osid.NullArgumentException
     *          <code>floorQueryRecord</code>,
     *          <code>floorQueryInspectorRecord</code>,
     *          <code>floorSearchOrderRecord</code> or
     *          <code>floorRecordTypefloor</code> is
     *          <code>null</code>
     */
            
    protected void addFloorRecords(org.osid.room.records.FloorQueryRecord floorQueryRecord, 
                                      org.osid.room.records.FloorQueryInspectorRecord floorQueryInspectorRecord, 
                                      org.osid.room.records.FloorSearchOrderRecord floorSearchOrderRecord, 
                                      org.osid.type.Type floorRecordType) {

        addRecordType(floorRecordType);

        nullarg(floorQueryRecord, "floor query record");
        nullarg(floorQueryInspectorRecord, "floor query inspector record");
        nullarg(floorSearchOrderRecord, "floor search odrer record");

        this.queryRecords.add(floorQueryRecord);
        this.queryInspectorRecords.add(floorQueryInspectorRecord);
        this.searchOrderRecords.add(floorSearchOrderRecord);
        
        return;
    }
}
