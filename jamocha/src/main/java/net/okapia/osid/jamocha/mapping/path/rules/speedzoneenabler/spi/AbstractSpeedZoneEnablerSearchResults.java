//
// AbstractSpeedZoneEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.speedzoneenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSpeedZoneEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.path.rules.SpeedZoneEnablerSearchResults {

    private org.osid.mapping.path.rules.SpeedZoneEnablerList speedZoneEnablers;
    private final org.osid.mapping.path.rules.SpeedZoneEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSpeedZoneEnablerSearchResults.
     *
     *  @param speedZoneEnablers the result set
     *  @param speedZoneEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablers</code>
     *          or <code>speedZoneEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSpeedZoneEnablerSearchResults(org.osid.mapping.path.rules.SpeedZoneEnablerList speedZoneEnablers,
                                            org.osid.mapping.path.rules.SpeedZoneEnablerQueryInspector speedZoneEnablerQueryInspector) {
        nullarg(speedZoneEnablers, "speed zone enablers");
        nullarg(speedZoneEnablerQueryInspector, "speed zone enabler query inspectpr");

        this.speedZoneEnablers = speedZoneEnablers;
        this.inspector = speedZoneEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the speed zone enabler list resulting from a search.
     *
     *  @return a speed zone enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablers() {
        if (this.speedZoneEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.path.rules.SpeedZoneEnablerList speedZoneEnablers = this.speedZoneEnablers;
        this.speedZoneEnablers = null;
	return (speedZoneEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.path.rules.SpeedZoneEnablerQueryInspector getSpeedZoneEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  speed zone enabler search record <code> Type. </code> This method must
     *  be used to retrieve a speedZoneEnabler implementing the requested
     *  record.
     *
     *  @param speedZoneEnablerSearchRecordType a speedZoneEnabler search 
     *         record type 
     *  @return the speed zone enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(speedZoneEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchResultsRecord getSpeedZoneEnablerSearchResultsRecord(org.osid.type.Type speedZoneEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(speedZoneEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(speedZoneEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record speed zone enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSpeedZoneEnablerRecord(org.osid.mapping.path.rules.records.SpeedZoneEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "speed zone enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
