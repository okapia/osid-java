//
// AbstractAuctionProcessorQueryInspector.java
//
//     A template for making an AuctionProcessorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for auction processors.
 */

public abstract class AbstractAuctionProcessorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQueryInspector
    implements org.osid.bidding.rules.AuctionProcessorQueryInspector {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the auction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuctionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the auction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQueryInspector[] getRuledAuctionTerms() {
        return (new org.osid.bidding.AuctionQueryInspector[0]);
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given auction processor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an auction processor implementing the requested record.
     *
     *  @param auctionProcessorRecordType an auction processor record type
     *  @return the auction processor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorQueryInspectorRecord getAuctionProcessorQueryInspectorRecord(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionProcessorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction processor query. 
     *
     *  @param auctionProcessorQueryInspectorRecord auction processor query inspector
     *         record
     *  @param auctionProcessorRecordType auctionProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionProcessorQueryInspectorRecord(org.osid.bidding.rules.records.AuctionProcessorQueryInspectorRecord auctionProcessorQueryInspectorRecord, 
                                                   org.osid.type.Type auctionProcessorRecordType) {

        addRecordType(auctionProcessorRecordType);
        nullarg(auctionProcessorRecordType, "auction processor record type");
        this.records.add(auctionProcessorQueryInspectorRecord);        
        return;
    }
}
