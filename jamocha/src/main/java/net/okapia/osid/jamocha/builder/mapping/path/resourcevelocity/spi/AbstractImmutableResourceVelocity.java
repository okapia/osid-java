//
// AbstractImmutableResourceVelocity.java
//
//     Wraps a mutable ResourceVelocity to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ResourceVelocity</code> to hide
 *  modifiers. This wrapper provides an immutized ResourceVelocity
 *  from the point of view external to the builder. Methods are passed
 *  through to the underlying resource velocity whose state changes
 *  are visible.
 */

public abstract class AbstractImmutableResourceVelocity
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCompendium
    implements org.osid.mapping.path.ResourceVelocity {

    private final org.osid.mapping.path.ResourceVelocity resourceVelocity;


    /**
     *  Constructs a new
     *  <code>AbstractImmutableResourceVelocity</code>.
     *
     *  @param resourceVelocity the resource velocity to immutablize
     *  @throws org.osid.NullArgumentException
     *          <code>resourceVelocity</code> is <code>null</code>
     */

    protected AbstractImmutableResourceVelocity(org.osid.mapping.path.ResourceVelocity resourceVelocity) {
        super(resourceVelocity);
        this.resourceVelocity = resourceVelocity;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource on the route. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resourceVelocity.getResourceId());
    }


    /**
     *  Gets the resource on the route. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {
        
        return (this.resourceVelocity.getResource());
    }


    /**
     *  Gets the current speed.
     *
     *  @return the current speed
     */

    @OSID @Override
    public org.osid.mapping.Speed getSpeed() {
        return (this.resourceVelocity.getSpeed());
    }


    /**
     *  Gets the current position.
     *
     *  @return the current coordinate
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getPosition() {
        return (this.resourceVelocity.getPosition());
    }


    /**
     *  Gets the current heading.
     *
     *  @return the current heading
     */

    @OSID @Override
    public org.osid.mapping.Heading getHeading() {
        return (this.resourceVelocity.getHeading());
    }


    /**
     *  Tests if the resource is on a designated path.
     *
     *  @return <code> true </code> if the resource is on a designated path,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isOnPath() {
        return (this.resourceVelocity.isOnPath());
    }


    /**
     *  Gets the <code> Path </code> <code> Id. </code>
     *
     *  @return the path <code> Id </code>
     *  @throws org.osid.IllegalStateException <code>isOnPath()</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.resourceVelocity.getPathId());
    }


    /**
     *  Gets the <code> Path. </code>
     *
     *  @return the path
     *  @throws org.osid.IllegalStateException <code>isOnPath()</code>
     *          is <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.resourceVelocity.getPath());
    }


    /**
     *  Gets the resource velocity record corresponding to the given
     *  <code> ResourceVelocity </code> record <code> Type. </code>
     *  This method is used to retrieve an object implementing the
     *  requested record.. The <code> resourceVelocityRecordType
     *  </code> may be the <code> Type </code> returned in <code>
     *  getRecordTypes() </code> or any of its parents in a <code>
     *  Type </code> hierarchy where <code>
     *  hasRecordType(resourceVelocityRecordType) </code> is <code>
     *  true </code> .
     *
     *  @param resourceVelocityRecordType the type of resource
     *         velocity record to retrieve
     *  @return the resource velocity record 
     *  @throws org.osid.NullArgumentException <code> resourceVelocityRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(resourceVelocityRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.records.ResourceVelocityRecord getResourceVelocityRecord(org.osid.type.Type resourceVelocityRecordType)
        throws org.osid.OperationFailedException {

        return (this.resourceVelocity.getResourceVelocityRecord(resourceVelocityRecordType));
    }
}

