//
// AbstractInventoryProxyManager.java
//
//     An adapter for a InventoryProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InventoryProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInventoryProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.inventory.InventoryProxyManager>
    implements org.osid.inventory.InventoryProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterInventoryProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInventoryProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInventoryProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInventoryProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up items is supported. 
     *
     *  @return <code> true </code> if item lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemLookup() {
        return (getAdapteeManager().supportsItemLookup());
    }


    /**
     *  Tests if querying items is supported. 
     *
     *  @return <code> true </code> if item query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (getAdapteeManager().supportsItemQuery());
    }


    /**
     *  Tests if searching items is supported. 
     *
     *  @return <code> true </code> if item search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearch() {
        return (getAdapteeManager().supportsItemSearch());
    }


    /**
     *  Tests if an item <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if item administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemAdmin() {
        return (getAdapteeManager().supportsItemAdmin());
    }


    /**
     *  Tests if an item <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if item notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemNotification() {
        return (getAdapteeManager().supportsItemNotification());
    }


    /**
     *  Tests if an item cataloging service is supported. 
     *
     *  @return <code> true </code> if item catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemWarehouse() {
        return (getAdapteeManager().supportsItemWarehouse());
    }


    /**
     *  Tests if an item cataloging service is supported. A cataloging service 
     *  maps items to catalogs. 
     *
     *  @return <code> true </code> if item cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemWarehouseAssignment() {
        return (getAdapteeManager().supportsItemWarehouseAssignment());
    }


    /**
     *  Tests if an item smart warehouse session is available. 
     *
     *  @return <code> true </code> if an item smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSmartWarehouse() {
        return (getAdapteeManager().supportsItemSmartWarehouse());
    }


    /**
     *  Tests if looking up stocks is supported. 
     *
     *  @return <code> true </code> if stock lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockLookup() {
        return (getAdapteeManager().supportsStockLookup());
    }


    /**
     *  Tests if querying stocks is supported. 
     *
     *  @return <code> true </code> if stock query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (getAdapteeManager().supportsStockQuery());
    }


    /**
     *  Tests if searching stocks is supported. 
     *
     *  @return <code> true </code> if stock search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSearch() {
        return (getAdapteeManager().supportsStockSearch());
    }


    /**
     *  Tests if stock administrative service is supported. 
     *
     *  @return <code> true </code> if stock administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockAdmin() {
        return (getAdapteeManager().supportsStockAdmin());
    }


    /**
     *  Tests if a stock <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if stock notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockNotification() {
        return (getAdapteeManager().supportsStockNotification());
    }


    /**
     *  Tests if a stock cataloging service is supported. 
     *
     *  @return <code> true </code> if stock catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockWarehouse() {
        return (getAdapteeManager().supportsStockWarehouse());
    }


    /**
     *  Tests if a stock cataloging service is supported. A cataloging service 
     *  maps stocks to catalogs. 
     *
     *  @return <code> true </code> if stock cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockWarehouseAssignment() {
        return (getAdapteeManager().supportsStockWarehouseAssignment());
    }


    /**
     *  Tests if a stock smart warehouse session is available. 
     *
     *  @return <code> true </code> if a stock smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSmartWarehouse() {
        return (getAdapteeManager().supportsStockSmartWarehouse());
    }


    /**
     *  Tests for the availability of a stock hierarchy traversal service. 
     *
     *  @return <code> true </code> if stock hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockHierarchy() {
        return (getAdapteeManager().supportsStockHierarchy());
    }


    /**
     *  Tests for the availability of a stock hierarchy design service. 
     *
     *  @return <code> true </code> if stock hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockHierarchyDesign() {
        return (getAdapteeManager().supportsStockHierarchyDesign());
    }


    /**
     *  Tests if looking up models is supported. 
     *
     *  @return <code> true </code> if model lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelLookup() {
        return (getAdapteeManager().supportsModelLookup());
    }


    /**
     *  Tests if querying models is supported. 
     *
     *  @return <code> true </code> if model query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (getAdapteeManager().supportsModelQuery());
    }


    /**
     *  Tests if searching models is supported. 
     *
     *  @return <code> true </code> if model search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelSearch() {
        return (getAdapteeManager().supportsModelSearch());
    }


    /**
     *  Tests if a stock <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if model administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelAdmin() {
        return (getAdapteeManager().supportsModelAdmin());
    }


    /**
     *  Tests if a model <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if model notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelNotification() {
        return (getAdapteeManager().supportsModelNotification());
    }


    /**
     *  Tests if a model cataloging service is supported. 
     *
     *  @return <code> true </code> if model catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelWarehouse() {
        return (getAdapteeManager().supportsModelWarehouse());
    }


    /**
     *  Tests if a model cataloging service is supported. A cataloging service 
     *  maps models to catalogs. 
     *
     *  @return <code> true </code> if model cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelWarehouseAssignment() {
        return (getAdapteeManager().supportsModelWarehouseAssignment());
    }


    /**
     *  Tests if a model smart warehouse session is available. 
     *
     *  @return <code> true </code> if a model smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelSmartWarehouse() {
        return (getAdapteeManager().supportsModelSmartWarehouse());
    }


    /**
     *  Tests if looking up inventories is supported. 
     *
     *  @return <code> true </code> if inventory lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryLookup() {
        return (getAdapteeManager().supportsInventoryLookup());
    }


    /**
     *  Tests if querying inventories is supported. 
     *
     *  @return <code> true </code> if inventory query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryQuery() {
        return (getAdapteeManager().supportsInventoryQuery());
    }


    /**
     *  Tests if searching inventories is supported. 
     *
     *  @return <code> true </code> if inventory search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventorySearch() {
        return (getAdapteeManager().supportsInventorySearch());
    }


    /**
     *  Tests if inventory administrative service is supported. 
     *
     *  @return <code> true </code> if inventory administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryAdmin() {
        return (getAdapteeManager().supportsInventoryAdmin());
    }


    /**
     *  Tests if an inventory <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if inventory notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryNotification() {
        return (getAdapteeManager().supportsInventoryNotification());
    }


    /**
     *  Tests if an inventory cataloging service is supported. 
     *
     *  @return <code> true </code> if inventory catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryWarehouse() {
        return (getAdapteeManager().supportsInventoryWarehouse());
    }


    /**
     *  Tests if an inventory cataloging service is supported. A cataloging 
     *  service maps inventories to catalogs. 
     *
     *  @return <code> true </code> if inventory cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryWarehouseAssignment() {
        return (getAdapteeManager().supportsInventoryWarehouseAssignment());
    }


    /**
     *  Tests if an inventory smart warehouse session is available. 
     *
     *  @return <code> true </code> if an inventory smart warehouse session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventorySmartWarehouse() {
        return (getAdapteeManager().supportsInventorySmartWarehouse());
    }


    /**
     *  Tests if looking up warehouses is supported. 
     *
     *  @return <code> true </code> if warehouse lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseLookup() {
        return (getAdapteeManager().supportsWarehouseLookup());
    }


    /**
     *  Tests if searching warehouses is supported. 
     *
     *  @return <code> true </code> if warehouse search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseSearch() {
        return (getAdapteeManager().supportsWarehouseSearch());
    }


    /**
     *  Tests if querying warehouses is supported. 
     *
     *  @return <code> true </code> if warehouse query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (getAdapteeManager().supportsWarehouseQuery());
    }


    /**
     *  Tests if warehouse administrative service is supported. 
     *
     *  @return <code> true </code> if warehouse administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseAdmin() {
        return (getAdapteeManager().supportsWarehouseAdmin());
    }


    /**
     *  Tests if a warehouse <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if warehouse notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseNotification() {
        return (getAdapteeManager().supportsWarehouseNotification());
    }


    /**
     *  Tests for the availability of a warehouse hierarchy traversal service. 
     *
     *  @return <code> true </code> if warehouse hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseHierarchy() {
        return (getAdapteeManager().supportsWarehouseHierarchy());
    }


    /**
     *  Tests for the availability of a warehouse hierarchy design service. 
     *
     *  @return <code> true </code> if warehouse hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseHierarchyDesign() {
        return (getAdapteeManager().supportsWarehouseHierarchyDesign());
    }


    /**
     *  Tests for the availability of a inventory batch service. 
     *
     *  @return <code> true </code> if a inventory batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryBatch() {
        return (getAdapteeManager().supportsInventoryBatch());
    }


    /**
     *  Tests for the availability of a inventory shipment service. 
     *
     *  @return <code> true </code> if a inventory shipment service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryShipment() {
        return (getAdapteeManager().supportsInventoryShipment());
    }


    /**
     *  Gets the supported <code> Item </code> record types. 
     *
     *  @return a list containing the supported <code> Item </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemRecordTypes() {
        return (getAdapteeManager().getItemRecordTypes());
    }


    /**
     *  Tests if the given <code> Item </code> record type is supported. 
     *
     *  @param  itemRecordType a <code> Type </code> indicating an <code> Item 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemRecordType(org.osid.type.Type itemRecordType) {
        return (getAdapteeManager().supportsItemRecordType(itemRecordType));
    }


    /**
     *  Gets the supported <code> Item </code> search record types. 
     *
     *  @return a list containing the supported <code> Item </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemSearchRecordTypes() {
        return (getAdapteeManager().getItemSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Item </code> search record type is 
     *  supported. 
     *
     *  @param  itemSearchRecordType a <code> Type </code> indicating an 
     *          <code> Item </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        return (getAdapteeManager().supportsItemSearchRecordType(itemSearchRecordType));
    }


    /**
     *  Gets the supported <code> Stock </code> record types. 
     *
     *  @return a list containing the supported <code> Stock </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStockRecordTypes() {
        return (getAdapteeManager().getStockRecordTypes());
    }


    /**
     *  Tests if the given <code> Stock </code> record type is supported. 
     *
     *  @param  stockRecordType a <code> Type </code> indicating an <code> 
     *          Stock </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stockRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStockRecordType(org.osid.type.Type stockRecordType) {
        return (getAdapteeManager().supportsStockRecordType(stockRecordType));
    }


    /**
     *  Gets the supported <code> Stock </code> search record types. 
     *
     *  @return a list containing the supported <code> Stock </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStockSearchRecordTypes() {
        return (getAdapteeManager().getStockSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Stock </code> search record type is 
     *  supported. 
     *
     *  @param  stockSearchRecordType a <code> Type </code> indicating an 
     *          <code> Stock </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stockSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStockSearchRecordType(org.osid.type.Type stockSearchRecordType) {
        return (getAdapteeManager().supportsStockSearchRecordType(stockSearchRecordType));
    }


    /**
     *  Gets the supported <code> Model </code> record types. 
     *
     *  @return a list containing the supported <code> Model </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModelRecordTypes() {
        return (getAdapteeManager().getModelRecordTypes());
    }


    /**
     *  Tests if the given <code> Model </code> record type is supported. 
     *
     *  @param  modelRecordType a <code> Type </code> indicating an <code> 
     *          Model </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> modelRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModelRecordType(org.osid.type.Type modelRecordType) {
        return (getAdapteeManager().supportsModelRecordType(modelRecordType));
    }


    /**
     *  Gets the supported <code> Model </code> search record types. 
     *
     *  @return a list containing the supported <code> Model </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getModelSearchRecordTypes() {
        return (getAdapteeManager().getModelSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Model </code> search record type is 
     *  supported. 
     *
     *  @param  modelSearchRecordType a <code> Type </code> indicating an 
     *          <code> Model </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> modelSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsModelSearchRecordType(org.osid.type.Type modelSearchRecordType) {
        return (getAdapteeManager().supportsModelSearchRecordType(modelSearchRecordType));
    }


    /**
     *  Gets the supported <code> Inventory </code> record types. 
     *
     *  @return a list containing the supported <code> Inventory </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInventoryRecordTypes() {
        return (getAdapteeManager().getInventoryRecordTypes());
    }


    /**
     *  Tests if the given <code> Inventory </code> record type is supported. 
     *
     *  @param  inventoryRecordType a <code> Type </code> indicating a <code> 
     *          Inventory </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inventoryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInventoryRecordType(org.osid.type.Type inventoryRecordType) {
        return (getAdapteeManager().supportsInventoryRecordType(inventoryRecordType));
    }


    /**
     *  Gets the supported <code> Inventory </code> search record types. 
     *
     *  @return a list containing the supported <code> Inventory </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInventorySearchRecordTypes() {
        return (getAdapteeManager().getInventorySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Inventory </code> search record type is 
     *  supported. 
     *
     *  @param  inventorySearchRecordType a <code> Type </code> indicating a 
     *          <code> Inventory </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          inventorySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInventorySearchRecordType(org.osid.type.Type inventorySearchRecordType) {
        return (getAdapteeManager().supportsInventorySearchRecordType(inventorySearchRecordType));
    }


    /**
     *  Gets the supported <code> Warehouse </code> record types. 
     *
     *  @return a list containing the supported <code> Warehouse </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWarehouseRecordTypes() {
        return (getAdapteeManager().getWarehouseRecordTypes());
    }


    /**
     *  Tests if the given <code> Warehouse </code> record type is supported. 
     *
     *  @param  warehouseRecordType a <code> Type </code> indicating an <code> 
     *          Warehouse </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> warehouseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWarehouseRecordType(org.osid.type.Type warehouseRecordType) {
        return (getAdapteeManager().supportsWarehouseRecordType(warehouseRecordType));
    }


    /**
     *  Gets the supported <code> Warehouse </code> search record types. 
     *
     *  @return a list containing the supported <code> Warehouse </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWarehouseSearchRecordTypes() {
        return (getAdapteeManager().getWarehouseSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Warehouse </code> search record type is 
     *  supported. 
     *
     *  @param  warehouseSearchRecordType a <code> Type </code> indicating an 
     *          <code> Warehouse </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          warehousesearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWarehouseSearchRecordType(org.osid.type.Type warehouseSearchRecordType) {
        return (getAdapteeManager().supportsWarehouseSearchRecordType(warehouseSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemLookupSession getItemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemLookupSession getItemLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemLookupSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuerySession getItemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuerySession getItemQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemQuerySessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchSession getItemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchSession getItemSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSearchSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemAdminSession getItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemAdminSession getItemAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemNotificationSession getItemNotificationSession(org.osid.inventory.ItemReceiver itemReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemNotificationSession(itemReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service for the given warehouse. 
     *
     *  @param  itemReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemNotificationSession getItemNotificationSessionForWarehouse(org.osid.inventory.ItemReceiver itemReceiver, 
                                                                                             org.osid.id.Id warehouseId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemNotificationSessionForWarehouse(itemReceiver, warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup item/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemWarehouseSession getItemWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemWarehouseSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning items to 
     *  warehouses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ItemCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemWarehouseAssignmentSession getItemWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemWarehouseAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> ItemSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSmartWarehouseSession getItemSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSmartWarehouseSession(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockLookupSession getStockLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @param  proxy proxy 
     *  @return a <code> StockLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockLookupSession getStockLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockLookupSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuerySession getStockQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuerySession getStockQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockQuerySessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchSession getStockSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchSession getStockSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockSearchSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockAdminSession getStockAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStockAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockAdminSession getStockAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockAdminSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  notification service. 
     *
     *  @param  stockReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> StockNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stockReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockNotificationSession getStockNotificationSession(org.osid.inventory.StockReceiver stockReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockNotificationSession(stockReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock 
     *  notification service for the given warehouse. 
     *
     *  @param  stockReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> stockReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockNotificationSession getStockNotificationSessionForWarehouse(org.osid.inventory.StockReceiver stockReceiver, 
                                                                                               org.osid.id.Id warehouseId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockNotificationSessionForWarehouse(stockReceiver, warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup stock/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockWarehouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockWarehouseSession getStockWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockWarehouseSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning stocks 
     *  to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockWarehouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockWarehouseAssignmentSession getStockWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockWarehouseAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the stock smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSmartWarehouseSession getStockSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockSmartWarehouseSession(warehouseId, proxy));
    }


    /**
     *  Gets the stock hierarchy traversal session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchySession getStockHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockHierarchySession(proxy));
    }


    /**
     *  Gets the stock hierarchy traversal session for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchySession getStockHierarchySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockHierarchySessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the stock hierarchy design session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchyDesignSession getStockHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the stock hierarchy design session for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> StockHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockHierarchyDesignSession getStockHierarchyDesignSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockHierarchyDesignSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelLookupSession getModelLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model lookup 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the warehouse 
     *  @param  proxy proxy 
     *  @return a <code> ModelLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelLookupSession getModelLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModelLookupSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuerySession getModelQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model query 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuerySession getModelQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModelQuerySessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchSession getModelSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model search 
     *  service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelSearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchSession getModelSearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModelSearchSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelAdminSession getModelAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsModelAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelAdminSession getModelAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModelAdminSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  notification service. 
     *
     *  @param  modelReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ModelNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> modelReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelNotificationSession getModelNotificationSession(org.osid.inventory.ModelReceiver modelReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelNotificationSession(modelReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model 
     *  notification service for the given warehouse. 
     *
     *  @param  modelReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> modelReceiver, 
     *          warehouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelNotificationSession getModelNotificationSessionForWarehouse(org.osid.inventory.ModelReceiver modelReceiver, 
                                                                                               org.osid.id.Id warehouseId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModelNotificationSessionForWarehouse(modelReceiver, warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup model/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelWarehouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelWarehouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelWarehouseSession getModelWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelWarehouseSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning models 
     *  to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ModelWarehouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelWarehouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelWarehouseAssignmentSession getModelWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelWarehouseAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the model smart 
     *  warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> ModelSmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelSmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSmartWarehouseSession getModelSmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModelSmartWarehouseSession(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryLookupSession getInventoryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  lookup service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventoryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryLookupSession getInventoryLookupSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryLookupSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> InventoryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuerySession getInventoryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  query service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> InventoryQuerySession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQuerySession getInventoryQuerySessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryQuerySessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventorySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySearchSession getInventorySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventorySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  search service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventorySearchSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySearchSession getInventorySearchSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInventorySearchSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryAdminSession getInventoryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventoryAdminSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          warehouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryAdminSession getInventoryAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryAdminSessionForWarehouse(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  notification service. 
     *
     *  @param  inventoryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> InventoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryNotificationSession getInventoryNotificationSession(org.osid.inventory.InventoryReceiver inventoryReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryNotificationSession(inventoryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  notification service for the given warehouse. 
     *
     *  @param  inventoryReceiver the notification callback 
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> InventoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryReceiver, 
     *          warehouseId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryNotificationSession getInventoryNotificationSessionForWarehouse(org.osid.inventory.InventoryReceiver inventoryReceiver, 
                                                                                                       org.osid.id.Id warehouseId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryNotificationSessionForWarehouse(inventoryReceiver, warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inventory/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryWarehouseSession getInventoryWarehouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryWarehouseSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  inventories to warehouses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> InventoryCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryWarehouseAssignmentSession getInventoryWarehouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryWarehouseAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inventory 
     *  smart warehouse service. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> InventorySmartWarehouseSession </code> 
     *  @throws org.osid.NotFoundException no warehouse found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventorySmartWarehouse() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventorySmartWarehouseSession getInventorySmartWarehouseSession(org.osid.id.Id warehouseId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInventorySmartWarehouseSession(warehouseId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseLookupSession getWarehouseLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWarehouseLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuerySession getWarehouseQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWarehouseQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseSearchSession getWarehouseSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWarehouseSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  administrative service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseAdminSession getWarehouseAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWarehouseAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  notification service. 
     *
     *  @param  warehouseReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> WarehouseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseNotificationSession getWarehouseNotificationSession(org.osid.inventory.WarehouseReceiver warehouseReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWarehouseNotificationSession(warehouseReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  hierarchy service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> WarehouseHierarchySession </code> for warehouses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseHierarchySession getWarehouseHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWarehouseHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the warehouse 
     *  hierarchy design service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> HierarchyDesignSession </code> for warehouses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseHierarchyDesignSession getWarehouseHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWarehouseHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> InventoryBatchProxyManager. </code> 
     *
     *  @return a <code> InventoryBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.InventoryBatchProxyManager getInventoryBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryBatchProxyManager());
    }


    /**
     *  Gets the <code> InventoryShipmentProxyManager. </code> 
     *
     *  @return a <code> InventoryShipmentProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryShipment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.InventoryShipmentProxyManager getInventoryShipmentProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryShipmentProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
