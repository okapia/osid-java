//
// AbstractDocetQuery.java
//
//     A template for making a Docet Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for docets.
 */

public abstract class AbstractDocetQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.syllabus.DocetQuery {

    private final java.util.Collection<org.osid.course.syllabus.records.DocetQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the module <code> Id </code> for this query. 
     *
     *  @param  moduleId a module <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> moduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModuleId(org.osid.id.Id moduleId, boolean match) {
        return;
    }


    /**
     *  Clears the module <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModuleIdTerms() {
        return;
    }


    /**
     *  Tests if a module query is available. 
     *
     *  @return <code> true </code> if a module query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a module. 
     *
     *  @return the module query 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuery getModuleQuery() {
        throw new org.osid.UnimplementedException("supportsModuleQuery() is false");
    }


    /**
     *  Clears the module terms. 
     */

    @OSID @Override
    public void clearModuleTerms() {
        return;
    }


    /**
     *  Sets the activity unit <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id activityUnitId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        return;
    }


    /**
     *  Tests if an activity unit query is available. 
     *
     *  @return <code> true </code> if an activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Clears the activity unit terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        return;
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  objectives. 
     *
     *  @return <code> true </code> if an robjective query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches a docet that has any objective assigned. 
     *
     *  @param  match <code> true </code> to match docets with any objective, 
     *          <code> false </code> to match docets with no objective 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        return;
    }


    /**
     *  Clears the objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        return;
    }


    /**
     *  Matches docets that occur in a class. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInClass(boolean match) {
        return;
    }


    /**
     *  Clears the in class terms. 
     */

    @OSID @Override
    public void clearInClassTerms() {
        return;
    }


    /**
     *  Matches durations between the given durations inclusive. 
     *
     *  @param  from starting duration 
     *  @param  to ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration from, 
                              org.osid.calendaring.Duration to, boolean match) {
        return;
    }


    /**
     *  Matches a docet that has any duration assigned. 
     *
     *  @param  match <code> true </code> to match docets with any duration, 
     *          <code> false </code> to match docets with no duration 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        return;
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId an asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an robjective query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches a docet that has any asset assigned. 
     *
     *  @param  match <code> true </code> to match docets with any asset, 
     *          <code> false </code> to match docets with no asset 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        return;
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches an objective that has any assessment assigned. 
     *
     *  @param  match <code> true </code> to match docets with any assessment, 
     *          <code> false </code> to match docets with no assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the docet <code> Id </code> for this query to match syllabi 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given docet query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a docet implementing the requested record.
     *
     *  @param docetRecordType a docet record type
     *  @return the docet query record
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(docetRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetQueryRecord getDocetQueryRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.DocetQueryRecord record : this.records) {
            if (record.implementsRecordType(docetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this docet query. 
     *
     *  @param docetQueryRecord docet query record
     *  @param docetRecordType docet record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDocetQueryRecord(org.osid.course.syllabus.records.DocetQueryRecord docetQueryRecord, 
                                          org.osid.type.Type docetRecordType) {

        addRecordType(docetRecordType);
        nullarg(docetQueryRecord, "docet query record");
        this.records.add(docetQueryRecord);        
        return;
    }
}
