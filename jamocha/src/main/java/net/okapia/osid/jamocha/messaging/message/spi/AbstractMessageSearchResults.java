//
// AbstractMessageSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.message.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractMessageSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.messaging.MessageSearchResults {

    private org.osid.messaging.MessageList messages;
    private final org.osid.messaging.MessageQueryInspector inspector;
    private final java.util.Collection<org.osid.messaging.records.MessageSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractMessageSearchResults.
     *
     *  @param messages the result set
     *  @param messageQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>messages</code>
     *          or <code>messageQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractMessageSearchResults(org.osid.messaging.MessageList messages,
                                            org.osid.messaging.MessageQueryInspector messageQueryInspector) {
        nullarg(messages, "messages");
        nullarg(messageQueryInspector, "message query inspectpr");

        this.messages = messages;
        this.inspector = messageQueryInspector;

        return;
    }


    /**
     *  Gets the message list resulting from a search.
     *
     *  @return a message list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessages() {
        if (this.messages == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.messaging.MessageList messages = this.messages;
        this.messages = null;
	return (messages);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.messaging.MessageQueryInspector getMessageQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  message search record <code> Type. </code> This method must
     *  be used to retrieve a message implementing the requested
     *  record.
     *
     *  @param messageSearchRecordType a message search 
     *         record type 
     *  @return the message search
     *  @throws org.osid.NullArgumentException
     *          <code>messageSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(messageSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MessageSearchResultsRecord getMessageSearchResultsRecord(org.osid.type.Type messageSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.messaging.records.MessageSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(messageSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(messageSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record message search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addMessageRecord(org.osid.messaging.records.MessageSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "message record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
