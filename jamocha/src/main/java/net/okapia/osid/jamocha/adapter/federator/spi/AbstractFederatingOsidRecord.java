//
// AbstractFederatingOsidRecord
//
//     This adapter federates one or more OsidRecord interfaces.
//
//
// Tom Coppeto
// OnTapSolutions
// 30 November 2010
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.spi;


/**
 *  This adapter federates one or more given OsidRecord
 *  interfaces of the same type. Method invocations are passed to the
 *  underlying interfaces.
 *
 *  This is a simple tool that is intended for use on methods that do
 *  not return information, such as conditions and proxies defining
 *  input-only methods.
 */

public abstract class AbstractFederatingOsidRecord<T extends org.osid.OsidRecord>
    extends AbstractFederator<T>
    implements java.lang.reflect.InvocationHandler {
    

    /**
     *  Constructs a new <code>AbstractFederatingOsidRecord</code>
     *
     *  @param delegate the underlying object
     *  @throws org.osid.NullArgumentException <code>delegate</code>
     *          id <code>null</code>
     */

    protected AbstractFederatingOsidRecord(T delegate) {
	super(delegate);
	return;
    }


    /**
     *  Constructs a new <code>AbstractOsidRecord</code>
     *
     *  @param delegates the underlying objects
     *  @throws org.osid.NullArgumentException <code>delegates</code>
     *          id <code>null</code>
     */

    protected AbstractFederatingOsidRecord(java.util.Collection<T> delegates) {
	super(delegates);
	return;
    }


    /**
     *  Adds a delegate.
     *
     *  @param delegate the underlying object
     *  @throws org.osid.NullArgumentException <code>delegate</code>
     *          id <code>null</code>
     */

    protected void add(T delegate) {
	super.add(delegate);
	return;
    }
}
    
