//
// GradeSystemTransformMiter.java
//
//     Defines a GradeSystemTransform miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.transform.gradesystemtransform;


/**
 *  Defines a <code>GradeSystemTransform</code> miter for use with the builders.
 */

public interface GradeSystemTransformMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.grading.transform.GradeSystemTransform {


    /**
     *  Sets the source grade system.
     *
     *  @param system a source grade system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public void setSourceGradeSystem(org.osid.grading.GradeSystem system);


    /**
     *  Sets the target grade system.
     *
     *  @param system a target grade system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public void setTargetGradeSystem(org.osid.grading.GradeSystem system);


    /**
     *  Sets the normalizes input scores flag.
     *
     *  @param normalizes <code> true </code> of a normalization is
     *         performed, <code> false </code> otherwise
     */

    public void setNormalizesInputScores(boolean normalizes);


    /**
     *  Adds a grade map.
     *
     *  @param map a grade map
     *  @throws org.osid.NullArgumentException <code>map</code> is
     *          <code>null</code>
     */

    public void addGradeMap(org.osid.grading.transform.GradeMap map);


    /**
     *  Sets all the grade maps.
     *
     *  @param maps a collection of grade maps
     *  @throws org.osid.NullArgumentException <code>maps</code> is
     *          <code>null</code>
     */

    public void setGradeMaps(java.util.Collection<org.osid.grading.transform.GradeMap> maps);


    /**
     *  Adds a GradeSystemTransform record.
     *
     *  @param record a gradeSystemTransform record
     *  @param recordType the type of gradeSystemTransform record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addGradeSystemTransformRecord(org.osid.grading.transform.records.GradeSystemTransformRecord record, org.osid.type.Type recordType);
}       


