//
// MutableIndexedMapCompositionEnablerLookupSession
//
//    Implements a CompositionEnabler lookup service backed by a collection of
//    compositionEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.rules;


/**
 *  Implements a CompositionEnabler lookup service backed by a collection of
 *  composition enablers. The composition enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some composition enablers may be compatible
 *  with more types than are indicated through these composition enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of composition enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCompositionEnablerLookupSession
    extends net.okapia.osid.jamocha.core.repository.rules.spi.AbstractIndexedMapCompositionEnablerLookupSession
    implements org.osid.repository.rules.CompositionEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCompositionEnablerLookupSession} with no composition enablers.
     *
     *  @param repository the repository
     *  @throws org.osid.NullArgumentException {@code repository}
     *          is {@code null}
     */

      public MutableIndexedMapCompositionEnablerLookupSession(org.osid.repository.Repository repository) {
        setRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompositionEnablerLookupSession} with a
     *  single composition enabler.
     *  
     *  @param repository the repository
     *  @param  compositionEnabler a single compositionEnabler
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositionEnabler} is {@code null}
     */

    public MutableIndexedMapCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.repository.rules.CompositionEnabler compositionEnabler) {
        this(repository);
        putCompositionEnabler(compositionEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompositionEnablerLookupSession} using an
     *  array of composition enablers.
     *
     *  @param repository the repository
     *  @param  compositionEnablers an array of composition enablers
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositionEnablers} is {@code null}
     */

    public MutableIndexedMapCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.repository.rules.CompositionEnabler[] compositionEnablers) {
        this(repository);
        putCompositionEnablers(compositionEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompositionEnablerLookupSession} using a
     *  collection of composition enablers.
     *
     *  @param repository the repository
     *  @param  compositionEnablers a collection of composition enablers
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositionEnablers} is {@code null}
     */

    public MutableIndexedMapCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                                  java.util.Collection<? extends org.osid.repository.rules.CompositionEnabler> compositionEnablers) {

        this(repository);
        putCompositionEnablers(compositionEnablers);
        return;
    }
    

    /**
     *  Makes a {@code CompositionEnabler} available in this session.
     *
     *  @param  compositionEnabler a composition enabler
     *  @throws org.osid.NullArgumentException {@code compositionEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putCompositionEnabler(org.osid.repository.rules.CompositionEnabler compositionEnabler) {
        super.putCompositionEnabler(compositionEnabler);
        return;
    }


    /**
     *  Makes an array of composition enablers available in this session.
     *
     *  @param  compositionEnablers an array of composition enablers
     *  @throws org.osid.NullArgumentException {@code compositionEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putCompositionEnablers(org.osid.repository.rules.CompositionEnabler[] compositionEnablers) {
        super.putCompositionEnablers(compositionEnablers);
        return;
    }


    /**
     *  Makes collection of composition enablers available in this session.
     *
     *  @param  compositionEnablers a collection of composition enablers
     *  @throws org.osid.NullArgumentException {@code compositionEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putCompositionEnablers(java.util.Collection<? extends org.osid.repository.rules.CompositionEnabler> compositionEnablers) {
        super.putCompositionEnablers(compositionEnablers);
        return;
    }


    /**
     *  Removes a CompositionEnabler from this session.
     *
     *  @param compositionEnablerId the {@code Id} of the composition enabler
     *  @throws org.osid.NullArgumentException {@code compositionEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCompositionEnabler(org.osid.id.Id compositionEnablerId) {
        super.removeCompositionEnabler(compositionEnablerId);
        return;
    }    
}
