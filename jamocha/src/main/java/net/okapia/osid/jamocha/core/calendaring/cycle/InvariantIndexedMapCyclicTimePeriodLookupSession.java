//
// InvariantIndexedMapCyclicTimePeriodLookupSession
//
//    Implements a CyclicTimePeriod lookup service backed by a fixed
//    collection of cyclicTimePeriods indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle;


/**
 *  Implements a CyclicTimePeriod lookup service backed by a fixed
 *  collection of cyclic time periods. The cyclic time periods are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some cyclic time periods may be compatible
 *  with more types than are indicated through these cyclic time period
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.cycle.spi.AbstractIndexedMapCyclicTimePeriodLookupSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapCyclicTimePeriodLookupSession} using an
     *  array of cyclicTimePeriods.
     *
     *  @param calendar the calendar
     *  @param cyclicTimePeriods an array of cyclic time periods
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code cyclicTimePeriods} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                    org.osid.calendaring.cycle.CyclicTimePeriod[] cyclicTimePeriods) {

        setCalendar(calendar);
        putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapCyclicTimePeriodLookupSession} using a
     *  collection of cyclic time periods.
     *
     *  @param calendar the calendar
     *  @param cyclicTimePeriods a collection of cyclic time periods
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code cyclicTimePeriods} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                    java.util.Collection<? extends org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriods) {

        setCalendar(calendar);
        putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }
}
