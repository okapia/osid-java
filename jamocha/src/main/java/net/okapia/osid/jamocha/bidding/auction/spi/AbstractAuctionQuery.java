//
// AbstractAuctionQuery.java
//
//     A template for making an Auction Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for auctions.
 */

public abstract class AbstractAuctionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.bidding.AuctionQuery {

    private final java.util.Collection<org.osid.bidding.records.AuctionQueryRecord> records = new java.util.ArrayList<>();
    private final OsidTemporalQuery query = new OsidTemporalQuery();

    
    /**
     *  Matches the currency type. 
     *
     *  @param  currencyType a currency type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCurrencyType(org.osid.type.Type currencyType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches auctions with any currency type set. 
     *
     *  @param  match <code> true </code> to match auctions with any currency 
     *          type, <code> false </code> to match auctions with no currency 
     *          type 
     */

    @OSID @Override
    public void matchAnyCurrencyType(boolean match) {
        return;
    }


    /**
     *  Clears the currency type terms. 
     */

    @OSID @Override
    public void clearCurrencyTypeTerms() {
        return;
    }


    /**
     *  Matches the minimum bidders between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchMinimumBidders(long start, long end, boolean match) {
        return;
    }


    /**
     *  Matches auctions with any minimum bidders set. 
     *
     *  @param  match <code> true </code> to match auctions with any minimum 
     *          bidders, <code> false </code> to match auctions with no 
     *          minimum bidders 
     */

    @OSID @Override
    public void matchAnyMinimumBidders(boolean match) {
        return;
    }


    /**
     *  Clears the minimum bidders terms. 
     */

    @OSID @Override
    public void clearMinimumBiddersTerms() {
        return;
    }


    /**
     *  Matches sealed auctions. 
     *
     *  @param  match <code> true </code> to match sealed auctions, <code> 
     *          false </code> to match visible bid auctions 
     */

    @OSID @Override
    public void matchSealed(boolean match) {
        return;
    }


    /**
     *  Clears the sealed terms. 
     */

    @OSID @Override
    public void clearSealedTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSellerId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSellerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSellerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSellerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSellerQuery() {
        throw new org.osid.UnimplementedException("supportsSellerQuery() is false");
    }


    /**
     *  Matches auctions with any seller set. 
     *
     *  @param  match <code> true </code> to match auctions with any seller, 
     *          <code> false </code> to match auctions with no seller 
     */

    @OSID @Override
    public void matchAnySeller(boolean match) {
        return;
    }


    /**
     *  Clears the seller query terms. 
     */

    @OSID @Override
    public void clearSellerTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches auctions with any item set. 
     *
     *  @param  match <code> true </code> to match auctions with any item, 
     *          <code> false </code> to match auctions with no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the seller query terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Matches the lot size between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchLotSize(long start, long end, boolean match) {
        return;
    }


    /**
     *  Matches auctions with any lot size set. 
     *
     *  @param  match <code> true </code> to match auctions with any lot size, 
     *          <code> false </code> to match auctions with no lot size set 
     */

    @OSID @Override
    public void matchAnyLotSize(boolean match) {
        return;
    }


    /**
     *  Clears the lot size terms. 
     */

    @OSID @Override
    public void clearLotSizeTerms() {
        return;
    }


    /**
     *  Matches the remaining items between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchRemainingItems(long start, long end, boolean match) {
        return;
    }


    /**
     *  Clears the remaining items terms. 
     */

    @OSID @Override
    public void clearRemainingItemsTerms() {
        return;
    }


    /**
     *  Matches the item limit between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchItemLimit(long start, long end, boolean match) {
        return;
    }


    /**
     *  Matches auctions with any item limit. 
     *
     *  @param  match <code> true </code> to match auctions with any item 
     *          limit, <code> false </code> to match auctions with no item 
     *          limit 
     */

    @OSID @Override
    public void matchAnyItemLimit(boolean match) {
        return;
    }


    /**
     *  Clears the item limit terms. 
     */

    @OSID @Override
    public void clearItemLimitTerms() {
        return;
    }


    /**
     *  Matches the starting price between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartingPrice(org.osid.financials.Currency start, 
                                   org.osid.financials.Currency end, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches auctions with any starting price. 
     *
     *  @param  match <code> true </code> to match auctions with any starting 
     *          price, <code> false </code> to match auctions with no starting 
     *          price 
     */

    @OSID @Override
    public void matchAnyStartingPrice(boolean match) {
        return;
    }


    /**
     *  Clears the starting price terms. 
     */

    @OSID @Override
    public void clearStartingPriceTerms() {
        return;
    }


    /**
     *  Matches the price increment between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceIncrement(org.osid.financials.Currency start, 
                                    org.osid.financials.Currency end, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches auctions with any price increment. 
     *
     *  @param  match <code> true </code> to match auctions with any price 
     *          increment, <code> false </code> to match auctions with no 
     *          price increment 
     */

    @OSID @Override
    public void matchAnyPriceIncrement(boolean match) {
        return;
    }


    /**
     *  Clears the price increment terms. 
     */

    @OSID @Override
    public void clearPriceIncrementTerms() {
        return;
    }


    /**
     *  Matches the reserve price between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReservePrice(org.osid.financials.Currency start, 
                                  org.osid.financials.Currency end, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches auctions with any reserve price. 
     *
     *  @param  match <code> true </code> to match auctions with any reserve 
     *          price, <code> false </code> to match auctions with no reserve 
     *          price 
     */

    @OSID @Override
    public void matchAnyReservePrice(boolean match) {
        return;
    }


    /**
     *  Clears the reserve price terms. 
     */

    @OSID @Override
    public void clearReservePriceTerms() {
        return;
    }


    /**
     *  Matches the buyout price between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBuyoutPrice(org.osid.financials.Currency start, 
                                 org.osid.financials.Currency end, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches auctions with any buyout price. 
     *
     *  @param  match <code> true </code> to match auctions with any buyout 
     *          price, <code> false </code> to match auctions with no buyout 
     *          price 
     */

    @OSID @Override
    public void matchAnyBuyoutPrice(boolean match) {
        return;
    }


    /**
     *  Clears the buyout price terms. 
     */

    @OSID @Override
    public void clearBuyoutPriceTerms() {
        return;
    }


    /**
     *  Sets the bid <code> Id </code> for this query. 
     *
     *  @param  bidId the bid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bidId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBidId(org.osid.id.Id bidId, boolean match) {
        return;
    }


    /**
     *  Clears the bid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBidIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BidQuery </code> is available. 
     *
     *  @return <code> true </code> if a bid query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bid. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bid query 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuery getBidQuery() {
        throw new org.osid.UnimplementedException("supportsBidQuery() is false");
    }


    /**
     *  Clears the bid query terms. 
     */

    @OSID @Override
    public void clearBidTerms() {
        return;
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match 
     *  auctions assigned to auction houses. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        return;
    }


    /**
     *  Match effective objects where the current date falls within the start
     *  and end dates inclusive.
     *
     *  @param  match <code> true </code> to match any effective, <code> false
     *          </code> to match ineffective
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        this.query.matchEffective(match);
        return;
    }


    /**
     *  Clears the effective query terms.
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        this.query.clearEffectiveTerms();
        return;
    }


    /**
     *  Matches temporals whose start date falls in between the given dates
     *  inclusive.
     *
     *  @param  start start of date range
     *  @param  end end of date range
     *  @param  match <code> true </code> if a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less
     *          than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code>
     *          end </code> is <code> null </code>
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start,
                               org.osid.calendaring.DateTime end,
                               boolean match) {
        this.query.matchStartDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set.
     *
     *  @param  match <code> true </code> to match any start date, <code>
     *          false </code> to match no start date
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        this.query.matchAnyStartDate(match);
        return;
    }


    /**
     *  Clears the start date query terms.
     */

    @OSID @Override
    public void clearStartDateTerms() {
        this.query.clearStartDateTerms();
        return;
    }


    /**
     *  Matches temporals whose effective end date falls in between the given
     *  dates inclusive.
     *
     *  @param  start start of date range
     *  @param  end end of date range
     *  @param  match <code> true </code> if a positive match, <code> false
     *          </code> for negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less
     *          than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code>
     *          end </code> is <code> null </code>
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start,
                             org.osid.calendaring.DateTime end,
                             boolean match) {

        this.query.matchEndDate(start, end, match);
        return;
    }

    
    /**
     *  Matches temporals with any end date set.
     *
     *  @param match <code> true </code> to match any end date, <code>
     *         false </code> to match no start date
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        this.query.matchAnyEndDate(match);
        return;
    }


    /**
     *  Clears the end date query terms.
     */

    @OSID @Override
    public void clearEndDateTerms() {
        this.query.clearEndDateTerms();
        return;
    }


    /**
     *  Matches temporals where the given date is included within the
     *  start and end dates inclusive.
     *
     *  @param  from start of date range
     *  @param  to start of date range
     *  @param  match <code> true </code> if a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.InvalidArgumentException
     *          <code>from</code> is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code>
     *          or <code>to</code> is <code>null</code>
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from,
                          org.osid.calendaring.DateTime to,
                          boolean match) {
        this.query.matchDate(from, to, match);
        return;

    }


    /**
     *  Clears the date query terms.
     */

    @OSID @Override
    public void clearDateTerms() {
        this.query.clearDateTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given auction query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction implementing the requested record.
     *
     *  @param auctionRecordType an auction record type
     *  @return the auction query record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionQueryRecord getAuctionQueryRecord(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionQueryRecord record : this.records) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction query. 
     *
     *  @param auctionQueryRecord auction query record
     *  @param auctionRecordType auction record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionQueryRecord(org.osid.bidding.records.AuctionQueryRecord auctionQueryRecord, 
                                         org.osid.type.Type auctionRecordType) {

        addRecordType(auctionRecordType);
        nullarg(auctionQueryRecord, "auction query record");
        this.records.add(auctionQueryRecord);        
        return;
    }


    protected class OsidTemporalQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidTemporalQuery
        implements org.osid.OsidTemporalQuery {
    }
}
