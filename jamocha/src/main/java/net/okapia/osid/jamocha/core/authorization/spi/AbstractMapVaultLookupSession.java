//
// AbstractMapVaultLookupSession
//
//    A simple framework for providing a Vault lookup service
//    backed by a fixed collection of vaults.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Vault lookup service backed by a
 *  fixed collection of vaults. The vaults are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Vaults</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapVaultLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractVaultLookupSession
    implements org.osid.authorization.VaultLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authorization.Vault> vaults = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authorization.Vault>());


    /**
     *  Makes a <code>Vault</code> available in this session.
     *
     *  @param  vault a vault
     *  @throws org.osid.NullArgumentException <code>vault<code>
     *          is <code>null</code>
     */

    protected void putVault(org.osid.authorization.Vault vault) {
        this.vaults.put(vault.getId(), vault);
        return;
    }


    /**
     *  Makes an array of vaults available in this session.
     *
     *  @param  vaults an array of vaults
     *  @throws org.osid.NullArgumentException <code>vaults<code>
     *          is <code>null</code>
     */

    protected void putVaults(org.osid.authorization.Vault[] vaults) {
        putVaults(java.util.Arrays.asList(vaults));
        return;
    }


    /**
     *  Makes a collection of vaults available in this session.
     *
     *  @param  vaults a collection of vaults
     *  @throws org.osid.NullArgumentException <code>vaults<code>
     *          is <code>null</code>
     */

    protected void putVaults(java.util.Collection<? extends org.osid.authorization.Vault> vaults) {
        for (org.osid.authorization.Vault vault : vaults) {
            this.vaults.put(vault.getId(), vault);
        }

        return;
    }


    /**
     *  Removes a Vault from this session.
     *
     *  @param  vaultId the <code>Id</code> of the vault
     *  @throws org.osid.NullArgumentException <code>vaultId<code> is
     *          <code>null</code>
     */

    protected void removeVault(org.osid.id.Id vaultId) {
        this.vaults.remove(vaultId);
        return;
    }


    /**
     *  Gets the <code>Vault</code> specified by its <code>Id</code>.
     *
     *  @param  vaultId <code>Id</code> of the <code>Vault</code>
     *  @return the vault
     *  @throws org.osid.NotFoundException <code>vaultId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>vaultId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authorization.Vault vault = this.vaults.get(vaultId);
        if (vault == null) {
            throw new org.osid.NotFoundException("vault not found: " + vaultId);
        }

        return (vault);
    }


    /**
     *  Gets all <code>Vaults</code>. In plenary mode, the returned
     *  list contains all known vaults or an error
     *  results. Otherwise, the returned list may contain only those
     *  vaults that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Vaults</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.vault.ArrayVaultList(this.vaults.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.vaults.clear();
        super.close();
        return;
    }
}
