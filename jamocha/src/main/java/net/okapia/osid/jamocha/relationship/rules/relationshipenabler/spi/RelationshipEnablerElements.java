//
// RelationshipEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.relationshipenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RelationshipEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the RelationshipEnablerElement Id.
     *
     *  @return the relationship enabler element Id
     */

    public static org.osid.id.Id getRelationshipEnablerEntityId() {
        return (makeEntityId("osid.relationship.rules.RelationshipEnabler"));
    }


    /**
     *  Gets the RuledRelationshipId element Id.
     *
     *  @return the RuledRelationshipId element Id
     */

    public static org.osid.id.Id getRuledRelationshipId() {
        return (makeQueryElementId("osid.relationship.rules.relationshipenabler.RuledRelationshipId"));
    }


    /**
     *  Gets the RuledRelationship element Id.
     *
     *  @return the RuledRelationship element Id
     */

    public static org.osid.id.Id getRuledRelationship() {
        return (makeQueryElementId("osid.relationship.rules.relationshipenabler.RuledRelationship"));
    }


    /**
     *  Gets the FamilyId element Id.
     *
     *  @return the FamilyId element Id
     */

    public static org.osid.id.Id getFamilyId() {
        return (makeQueryElementId("osid.relationship.rules.relationshipenabler.FamilyId"));
    }


    /**
     *  Gets the Family element Id.
     *
     *  @return the Family element Id
     */

    public static org.osid.id.Id getFamily() {
        return (makeQueryElementId("osid.relationship.rules.relationshipenabler.Family"));
    }
}
