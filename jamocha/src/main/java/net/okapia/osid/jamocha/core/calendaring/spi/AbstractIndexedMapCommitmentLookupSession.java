//
// AbstractIndexedMapCommitmentLookupSession.java
//
//    A simple framework for providing a Commitment lookup service
//    backed by a fixed collection of commitments with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Commitment lookup service backed by a
 *  fixed collection of commitments. The commitments are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some commitments may be compatible
 *  with more types than are indicated through these commitment
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Commitments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCommitmentLookupSession
    extends AbstractMapCommitmentLookupSession
    implements org.osid.calendaring.CommitmentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Commitment> commitmentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Commitment>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.Commitment> commitmentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.Commitment>());


    /**
     *  Makes a <code>Commitment</code> available in this session.
     *
     *  @param  commitment a commitment
     *  @throws org.osid.NullArgumentException <code>commitment<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCommitment(org.osid.calendaring.Commitment commitment) {
        super.putCommitment(commitment);

        this.commitmentsByGenus.put(commitment.getGenusType(), commitment);
        
        try (org.osid.type.TypeList types = commitment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commitmentsByRecord.put(types.getNextType(), commitment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a commitment from this session.
     *
     *  @param commitmentId the <code>Id</code> of the commitment
     *  @throws org.osid.NullArgumentException <code>commitmentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCommitment(org.osid.id.Id commitmentId) {
        org.osid.calendaring.Commitment commitment;
        try {
            commitment = getCommitment(commitmentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.commitmentsByGenus.remove(commitment.getGenusType());

        try (org.osid.type.TypeList types = commitment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commitmentsByRecord.remove(types.getNextType(), commitment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCommitment(commitmentId);
        return;
    }


    /**
     *  Gets a <code>CommitmentList</code> corresponding to the given
     *  commitment genus <code>Type</code> which does not include
     *  commitments of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known commitments or an error results. Otherwise,
     *  the returned list may contain only those commitments that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  commitmentGenusType a commitment genus type 
     *  @return the returned <code>Commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByGenusType(org.osid.type.Type commitmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.commitment.ArrayCommitmentList(this.commitmentsByGenus.get(commitmentGenusType)));
    }


    /**
     *  Gets a <code>CommitmentList</code> containing the given
     *  commitment record <code>Type</code>. In plenary mode, the
     *  returned list contains all known commitments or an error
     *  results. Otherwise, the returned list may contain only those
     *  commitments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  commitmentRecordType a commitment record type 
     *  @return the returned <code>commitment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitmentsByRecordType(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.commitment.ArrayCommitmentList(this.commitmentsByRecord.get(commitmentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commitmentsByGenus.clear();
        this.commitmentsByRecord.clear();

        super.close();

        return;
    }
}
