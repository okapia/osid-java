//
// AbstractMapSignalLookupSession
//
//    A simple framework for providing a Signal lookup service
//    backed by a fixed collection of signals.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Signal lookup service backed by a
 *  fixed collection of signals. The signals are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Signals</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSignalLookupSession
    extends net.okapia.osid.jamocha.mapping.path.spi.AbstractSignalLookupSession
    implements org.osid.mapping.path.SignalLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.Signal> signals = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.Signal>());


    /**
     *  Makes a <code>Signal</code> available in this session.
     *
     *  @param  signal a signal
     *  @throws org.osid.NullArgumentException <code>signal<code>
     *          is <code>null</code>
     */

    protected void putSignal(org.osid.mapping.path.Signal signal) {
        this.signals.put(signal.getId(), signal);
        return;
    }


    /**
     *  Makes an array of signals available in this session.
     *
     *  @param  signals an array of signals
     *  @throws org.osid.NullArgumentException <code>signals<code>
     *          is <code>null</code>
     */

    protected void putSignals(org.osid.mapping.path.Signal[] signals) {
        putSignals(java.util.Arrays.asList(signals));
        return;
    }


    /**
     *  Makes a collection of signals available in this session.
     *
     *  @param  signals a collection of signals
     *  @throws org.osid.NullArgumentException <code>signals<code>
     *          is <code>null</code>
     */

    protected void putSignals(java.util.Collection<? extends org.osid.mapping.path.Signal> signals) {
        for (org.osid.mapping.path.Signal signal : signals) {
            this.signals.put(signal.getId(), signal);
        }

        return;
    }


    /**
     *  Removes a Signal from this session.
     *
     *  @param  signalId the <code>Id</code> of the signal
     *  @throws org.osid.NullArgumentException <code>signalId<code> is
     *          <code>null</code>
     */

    protected void removeSignal(org.osid.id.Id signalId) {
        this.signals.remove(signalId);
        return;
    }


    /**
     *  Gets the <code>Signal</code> specified by its <code>Id</code>.
     *
     *  @param  signalId <code>Id</code> of the <code>Signal</code>
     *  @return the signal
     *  @throws org.osid.NotFoundException <code>signalId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>signalId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Signal getSignal(org.osid.id.Id signalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.Signal signal = this.signals.get(signalId);
        if (signal == null) {
            throw new org.osid.NotFoundException("signal not found: " + signalId);
        }

        return (signal);
    }


    /**
     *  Gets all <code>Signals</code>. In plenary mode, the returned
     *  list contains all known signals or an error
     *  results. Otherwise, the returned list may contain only those
     *  signals that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Signals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.signal.ArraySignalList(this.signals.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.signals.clear();
        super.close();
        return;
    }
}
