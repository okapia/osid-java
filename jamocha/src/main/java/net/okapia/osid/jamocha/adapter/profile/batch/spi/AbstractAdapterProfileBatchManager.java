//
// AbstractProfileBatchManager.java
//
//     An adapter for a ProfileBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ProfileBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterProfileBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.profile.batch.ProfileBatchManager>
    implements org.osid.profile.batch.ProfileBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterProfileBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterProfileBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterProfileBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterProfileBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of profile entries is available. 
     *
     *  @return <code> true </code> if a profile entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryBatchAdmin() {
        return (getAdapteeManager().supportsProfileEntryBatchAdmin());
    }


    /**
     *  Tests if bulk administration of profile items is available. 
     *
     *  @return <code> true </code> if a profile item bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemBatchAdmin() {
        return (getAdapteeManager().supportsProfileItemBatchAdmin());
    }


    /**
     *  Tests if bulk administration of profiles is available. 
     *
     *  @return <code> true </code> if a profile bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileBatchAdmin() {
        return (getAdapteeManager().supportsProfileBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk profile 
     *  entry administration service. 
     *
     *  @return a <code> ProfileEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileEntryBatchAdminSession getProfileEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk profile 
     *  entry administration service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileEntryBatchAdminSession getProfileEntryBatchAdminSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryBatchAdminSessionForProfile(profileId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk profile 
     *  item administration service. 
     *
     *  @return a <code> ProfileItemBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileItemBatchAdminSession getProfileItemBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk profile 
     *  item administration service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileItemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileItemBatchAdminSession getProfileItemBatchAdminSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemBatchAdminSessionForProfile(profileId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk profile 
     *  administration service. 
     *
     *  @return a <code> ProfileBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileBatchAdminSession getProfileBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
