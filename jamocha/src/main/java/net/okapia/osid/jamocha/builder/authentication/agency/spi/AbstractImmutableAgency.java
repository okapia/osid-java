//
// AbstractImmutableAgency.java
//
//     Wraps a mutable Agency to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.agency.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Agency</code> to hide modifiers. This
 *  wrapper provides an immutized Agency from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying agency whose state changes are visible.
 */

public abstract class AbstractImmutableAgency
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.authentication.Agency {

    private final org.osid.authentication.Agency agency;


    /**
     *  Constructs a new <code>AbstractImmutableAgency</code>.
     *
     *  @param agency the agency to immutablize
     *  @throws org.osid.NullArgumentException <code>agency</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAgency(org.osid.authentication.Agency agency) {
        super(agency);
        this.agency = agency;
        return;
    }


    /**
     *  Gets the agency record corresponding to the given <code> Agency 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> agencyRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(agencyRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  agencyRecordType an agency record type 
     *  @return the agency record 
     *  @throws org.osid.NullArgumentException <code> agencyRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(agencyRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.records.AgencyRecord getAgencyRecord(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException {

        return (this.agency.getAgencyRecord(agencyRecordType));
    }
}

