//
// AbstractMapLessonLookupSession
//
//    A simple framework for providing a Lesson lookup service
//    backed by a fixed collection of lessons.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Lesson lookup service backed by a
 *  fixed collection of lessons. The lessons are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Lessons</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapLessonLookupSession
    extends net.okapia.osid.jamocha.course.plan.spi.AbstractLessonLookupSession
    implements org.osid.course.plan.LessonLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.plan.Lesson> lessons = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.plan.Lesson>());


    /**
     *  Makes a <code>Lesson</code> available in this session.
     *
     *  @param  lesson a lesson
     *  @throws org.osid.NullArgumentException <code>lesson<code>
     *          is <code>null</code>
     */

    protected void putLesson(org.osid.course.plan.Lesson lesson) {
        this.lessons.put(lesson.getId(), lesson);
        return;
    }


    /**
     *  Makes an array of lessons available in this session.
     *
     *  @param  lessons an array of lessons
     *  @throws org.osid.NullArgumentException <code>lessons<code>
     *          is <code>null</code>
     */

    protected void putLessons(org.osid.course.plan.Lesson[] lessons) {
        putLessons(java.util.Arrays.asList(lessons));
        return;
    }


    /**
     *  Makes a collection of lessons available in this session.
     *
     *  @param  lessons a collection of lessons
     *  @throws org.osid.NullArgumentException <code>lessons<code>
     *          is <code>null</code>
     */

    protected void putLessons(java.util.Collection<? extends org.osid.course.plan.Lesson> lessons) {
        for (org.osid.course.plan.Lesson lesson : lessons) {
            this.lessons.put(lesson.getId(), lesson);
        }

        return;
    }


    /**
     *  Removes a Lesson from this session.
     *
     *  @param  lessonId the <code>Id</code> of the lesson
     *  @throws org.osid.NullArgumentException <code>lessonId<code> is
     *          <code>null</code>
     */

    protected void removeLesson(org.osid.id.Id lessonId) {
        this.lessons.remove(lessonId);
        return;
    }


    /**
     *  Gets the <code>Lesson</code> specified by its <code>Id</code>.
     *
     *  @param  lessonId <code>Id</code> of the <code>Lesson</code>
     *  @return the lesson
     *  @throws org.osid.NotFoundException <code>lessonId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>lessonId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.Lesson getLesson(org.osid.id.Id lessonId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.plan.Lesson lesson = this.lessons.get(lessonId);
        if (lesson == null) {
            throw new org.osid.NotFoundException("lesson not found: " + lessonId);
        }

        return (lesson);
    }


    /**
     *  Gets all <code>Lessons</code>. In plenary mode, the returned
     *  list contains all known lessons or an error
     *  results. Otherwise, the returned list may contain only those
     *  lessons that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Lessons</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessons()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.plan.lesson.ArrayLessonList(this.lessons.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.lessons.clear();
        super.close();
        return;
    }
}
