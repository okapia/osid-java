//
// AbstractLessonAnchor.java
//
//     Defines a LessonAnchor.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.lessonanchor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>LessonAnchor</code>.
 */

public abstract class AbstractLessonAnchor
    implements org.osid.course.plan.LessonAnchor {

    private org.osid.course.plan.Lesson lesson;
    private org.osid.course.Activity activity;
    private org.osid.calendaring.Duration duration;


    /**
     *  Gets the <code> Id </code> of the lesson. 
     *
     *  @return the lesson <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLessonId() {
        return (this.lesson.getId());
    }


    /**
     *  Gets the lesson. 
     *
     *  @return the lesson 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.plan.Lesson getLesson()
        throws org.osid.OperationFailedException {

        return (this.lesson);
    }


    /**
     *  Sets the lesson.
     *
     *  @param lesson the lesson
     *  @throws org.osid.NullArgumentException <code>lesson</code> is
     *          <code>null</code>
     */

    protected void setLesson(org.osid.course.plan.Lesson lesson) {
        nullarg(lesson, "lesson");
        this.lesson = lesson;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the activity. 
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.activity.getId());
    }


    /**
     *  Gets the activity. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.activity);
    }


    /**
     *  Sets the activity.
     *
     *  @param activity the activity
     *  @throws org.osid.NullArgumentException <code>activity</code> is
     *          <code>null</code>
     */

    protected void setActivity(org.osid.course.Activity activity) {
        nullarg(activity, "activity");
        this.activity = activity;
        return;
    }


    /**
     *  Gets the time offset from the start of the acttvity where the anchor 
     *  exists. 
     *
     *  @return a duration from the start of the activity 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTime() {
        return (this.duration);
    }


    /**
     *  Sets the time offset.
     *
     *  @param duration the duration from the start of the activity
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    protected void setTime(org.osid.calendaring.Duration duration) {
        nullarg(duration, "time offset");
        this.duration = duration;
        return;
    }
}
