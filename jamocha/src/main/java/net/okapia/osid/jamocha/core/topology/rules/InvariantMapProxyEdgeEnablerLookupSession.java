//
// InvariantMapProxyEdgeEnablerLookupSession
//
//    Implements an EdgeEnabler lookup service backed by a fixed
//    collection of edgeEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.rules;


/**
 *  Implements an EdgeEnabler lookup service backed by a fixed
 *  collection of edge enablers. The edge enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyEdgeEnablerLookupSession
    extends net.okapia.osid.jamocha.core.topology.rules.spi.AbstractMapEdgeEnablerLookupSession
    implements org.osid.topology.rules.EdgeEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyEdgeEnablerLookupSession} with no
     *  edge enablers.
     *
     *  @param graph the graph
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyEdgeEnablerLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.proxy.Proxy proxy) {
        setGraph(graph);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyEdgeEnablerLookupSession} with a single
     *  edge enabler.
     *
     *  @param graph the graph
     *  @param edgeEnabler an single edge enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code edgeEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyEdgeEnablerLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.topology.rules.EdgeEnabler edgeEnabler, org.osid.proxy.Proxy proxy) {

        this(graph, proxy);
        putEdgeEnabler(edgeEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyEdgeEnablerLookupSession} using
     *  an array of edge enablers.
     *
     *  @param graph the graph
     *  @param edgeEnablers an array of edge enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code edgeEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyEdgeEnablerLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.topology.rules.EdgeEnabler[] edgeEnablers, org.osid.proxy.Proxy proxy) {

        this(graph, proxy);
        putEdgeEnablers(edgeEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyEdgeEnablerLookupSession} using a
     *  collection of edge enablers.
     *
     *  @param graph the graph
     *  @param edgeEnablers a collection of edge enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code edgeEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyEdgeEnablerLookupSession(org.osid.topology.Graph graph,
                                                  java.util.Collection<? extends org.osid.topology.rules.EdgeEnabler> edgeEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(graph, proxy);
        putEdgeEnablers(edgeEnablers);
        return;
    }
}
