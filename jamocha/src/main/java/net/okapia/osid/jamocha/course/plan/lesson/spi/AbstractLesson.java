//
// AbstractLesson.java
//
//     Defines a Lesson.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Lesson</code>.
 */

public abstract class AbstractLesson
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.plan.Lesson {

    private org.osid.course.plan.Plan plan;
    private org.osid.course.syllabus.Docet docet;

    private org.osid.calendaring.Duration plannedStartTime;
    private org.osid.calendaring.Duration startTime;
    private org.osid.course.Activity startingActivity;
    private org.osid.calendaring.Duration endTime;
    private org.osid.course.Activity endingActivity;
    private org.osid.calendaring.Duration timeSpent;

    private boolean skipped  = false;
    private boolean complete = false;

    private final java.util.Collection<org.osid.course.Activity> activities = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.plan.records.LessonRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the plan. 
     *
     *  @return the plan <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPlanId() {
        return (this.plan.getId());
    }


    /**
     *  Gets the plan. 
     *
     *  @return the plan 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.plan.Plan getPlan()
        throws org.osid.OperationFailedException {

        return (this.plan);
    }


    /**
     *  Sets the plan.
     *
     *  @param plan a plan
     *  @throws org.osid.NullArgumentException <code>plan</code> is
     *          <code>null</code>
     */

    protected void setPlan(org.osid.course.plan.Plan plan) {
        nullarg(plan, "plan");
        this.plan = plan;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the docet. 
     *
     *  @return the lesson <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDocetId() {
        return (this.docet.getId());
    }


    /**
     *  Gets the docet. 
     *
     *  @return the docet 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.Docet getDocet()
        throws org.osid.OperationFailedException {

        return (this.docet);
    }


    /**
     *  Sets the docet.
     *
     *  @param docet a docet
     *  @throws org.osid.NullArgumentException <code>docet</code> is
     *          <code>null</code>
     */

    protected void setDocet(org.osid.course.syllabus.Docet docet) {
        nullarg(docet, "docet");
        this.docet = docet;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the activities to which this lesson 
     *  applies. 
     *
     *  @return the activity <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getActivityIds() {
        try {
            org.osid.course.ActivityList activities = getActivities();
            return (new net.okapia.osid.jamocha.adapter.converter.course.activity.ActivityToIdList(activities));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the activities to which this lesson applies. A Lesson may span 
     *  multiple scheduled activities. 
     *
     *  @return the activities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.course.activity.ArrayActivityList(this.activities));
    }


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected void addActivity(org.osid.course.Activity activity) {
        nullarg(activity, "activity");
        this.activities.add(activity);
        return;
    }


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    protected void setActivities(java.util.Collection<org.osid.course.Activity> activities) {
        nullarg(activities, "activities");
        this.activities.clear();
        this.activities.addAll(activities);
        return;
    }


    /**
     *  Gets the planned start time within the first activity as deteremined 
     *  by the <code> Lesson </code> scheduling. The time expressed as a 
     *  duration relative to the starting time of the first activity. 
     *
     *  @return the starting time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getPlannedStartTime() {
        return (this.plannedStartTime);
    }


    /**
     *  Sets the planned start time.
     *
     *  @param time a planned start time
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    protected void setPlannedStartTime(org.osid.calendaring.Duration time) {
        nullarg(time, "planned start time");
        this.plannedStartTime = time;
        return;
    }


    /**
     *  Tests if this lesson has begun. <code> hasBegun() </code> is
     *  <code> true </code> for completed lessons. A lesson in
     *  progress is one where <code> hasBegun() </code> is <code> true
     *  </code> and <code> isComplete() </code> or <code> isSkipped()
     *  </code> is <code> false.  </code>
     *
     *  @return <code>true</code> if this lesson has begun,
     *          <code>false</code> if not yet begun
     */

    @OSID @Override
    public boolean hasBegun() {
        return ((this.startTime != null) && (this.startingActivity != null));
    }


    /**
     *  Gets the actual start time. The time expressed as a duration
     *  relative to the starting time of the actual starting activity.
     *
     *  @return the actual starting time
     *  @throws org.osid.IllegalStateException <code> hasBegun()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getActualStartTime() {
        if (!hasBegun()) {
            throw new org.osid.IllegalStateException("hasBegun() is false");
        }

        return (this.startTime);
    }


    /**
     *  Sets the actual start time.
     *
     *  @param time an actual start time
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    protected void setActualStartTime(org.osid.calendaring.Duration time) {
        nullarg(time, "actual start time");
        this.startTime = time;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the activity when this lesson
     *  actually began.
     *
     *  @return the starting activity <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasBegun()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getActualStartingActivityId() {
        if (!hasBegun()) {
            throw new org.osid.IllegalStateException("hasBegun() is false");
        }

        return (this.startingActivity.getId());
    }


    /**
     *  Gets the activity when this lesson actually began. 
     *
     *  @return the starting activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.IllegalStateException <code> hasBegun()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.course.Activity getActualStartingActivity()
        throws org.osid.OperationFailedException {

        if (!hasBegun()) {
            throw new org.osid.IllegalStateException("hasBegun() is false");
        }

        return (this.startingActivity);
    }


    /**
     *  Sets the actual starting activity.
     *
     *  @param activity an actual starting activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected void setActualStartingActivity(org.osid.course.Activity activity) {
        nullarg(activity, "actual starting activity");
        this.startingActivity = activity;
        return;
    }


    /**
     *  Tests if this lesson has been marked as completed. 
     *
     *  @return <code>true</code> if this lesson is complete,
     *          <code>false</code> if not completed
     *  @throws org.osid.IllegalStateException <code>hasBegun()</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public boolean isComplete() {
        if (!hasBegun()) {
            throw new org.osid.IllegalStateException("hasBegun() is false");
        }

        return (this.complete);
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code>true</code> if this lesson is complete,
     *         <code>false</code> if not completed
     */

    protected void setComplete(boolean complete) {
        if (complete) {
            this.skipped = false;
        }

        this.complete = complete;        
        return;
    }


    /**
     *  Tests if this lesson has been marked as skipped. A skipped
     *  lesson may have been partially undertaken but <code>
     *  isComplete() </code> remains <code> false. </code>
     *
     *  @return <code>true</code> if this lesson is skipped, 
     *          <code>false</code> if not completed 
     */

    @OSID @Override
    public boolean isSkipped() {
        return (this.skipped);
    }


    /**
     *  Sets the skipped flag.
     *
     *  @param skipped <code>true</code> if this lesson is skipped,
     *         <code>false</code> if not completed
     */

    protected void setSkipped(boolean skipped) {
        if (skipped) {
            this.complete = false;
        }

        this.skipped = skipped;
        return;
    }


    /**
     *  Gets the actual completion time. The time expressed as a
     *  duration relative to the starting time of the ending activity.
     *
     *  @return the actual end time 
     *  @throws org.osid.IllegalStateException <code> isComplete()
     *          </code> and <code> isSkipped() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getActualEndTime() {
        if (!isSkipped() && !isComplete()) {
            throw new org.osid.IllegalStateException("isSkipped() and isComplete() is false");
        }

        return (this.endTime);
    }


    /**
     *  Sets the actual end time.
     *
     *  @param time an actual end time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setActualEndTime(org.osid.calendaring.Duration time) {
        nullarg(time, "end time");
        this.endTime = time;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the activity when this lesson
     *  actually completed.
     *
     *  @return the ending activity <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isComplete()
     *          </code> or <code> isSkipped() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.id.Id getActualEndingActivityId() {
        if (!isSkipped() && !isComplete()) {
            throw new org.osid.IllegalStateException("isSkipped() and isComplete() is false");
        }

        return (this.endingActivity.getId());
    }


    /**
     *  Gets the activity when this lesson actually completed.
     *
     *  @return the ending activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.IllegalStateException <code> isComplete()
     *          </code> or <code> isSkipped() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.course.Activity getActualEndingActivity()
        throws org.osid.OperationFailedException {

        if (!isSkipped() && !isComplete()) {
            throw new org.osid.IllegalStateException("isSkipped() and isComplete() is false");
        }

        return (this.endingActivity);
    }


    /**
     *  Sets the actual ending activity.
     *
     *  @param activity an actual ending activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected void setActualEndingActivity(org.osid.course.Activity activity) {
        nullarg(activity, "actual ending activity");
        this.endingActivity = activity;
        return;
    }


    /**
     *  Gets the actual duration of this lesson if it has completed,
     *  in progress, or skipped.
     *
     *  @return the actual time spent
     *  @throws org.osid.IllegalStateException <code> hasBegun()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getActualTimeSpent() {
        if (!hasBegun()) {
            throw new org.osid.IllegalStateException("hasBegun() is false");
        }

        return (this.timeSpent);
    }


    /**
     *  Sets the actual time spent.
     *
     *  @param timeSpent an actual time spent
     *  @throws org.osid.NullArgumentException
     *          <code>timeSpent</code> is <code>null</code>
     */

    protected void setActualTimeSpent(org.osid.calendaring.Duration timeSpent) {
        nullarg(timeSpent, "actual time spent");
        this.timeSpent = timeSpent;
        return;
    }


    /**
     *  Tests if this lesson supports the given record
     *  <code>Type</code>.
     *
     *  @param  lessonRecordType a lesson record type 
     *  @return <code>true</code> if the lessonRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type lessonRecordType) {
        for (org.osid.course.plan.records.LessonRecord record : this.records) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Lesson</code> record <code>Type</code>.
     *
     *  @param  lessonRecordType the lesson record type 
     *  @return the lesson record 
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(lessonRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonRecord getLessonRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.LessonRecord record : this.records) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(lessonRecordType + " is not supported");
    }


    /**
     *  Adds a record to this lesson. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param lessonRecord the lesson record
     *  @param lessonRecordType lesson record type
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecord</code> or
     *          <code>lessonRecordTypelesson</code> is
     *          <code>null</code>
     */
            
    protected void addLessonRecord(org.osid.course.plan.records.LessonRecord lessonRecord, 
                                   org.osid.type.Type lessonRecordType) {

        nullarg(lessonRecord, "lesson record");
        addRecordType(lessonRecordType);
        this.records.add(lessonRecord);
        
        return;
    }
}
