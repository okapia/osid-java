//
// AbstractStock.java
//
//     Defines a Stock builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.stock.spi;


/**
 *  Defines a <code>Stock</code> builder.
 */

public abstract class AbstractStockBuilder<T extends AbstractStockBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inventory.stock.StockMiter stock;


    /**
     *  Constructs a new <code>AbstractStockBuilder</code>.
     *
     *  @param stock the stock to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractStockBuilder(net.okapia.osid.jamocha.builder.inventory.stock.StockMiter stock) {
        super(stock);
        this.stock = stock;
        return;
    }


    /**
     *  Builds the stock.
     *
     *  @return the new stock
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inventory.Stock build() {
        (new net.okapia.osid.jamocha.builder.validator.inventory.stock.StockValidator(getValidations())).validate(this.stock);
        return (new net.okapia.osid.jamocha.builder.inventory.stock.ImmutableStock(this.stock));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the stock miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.inventory.stock.StockMiter getMiter() {
        return (this.stock);
    }


    /**
     *  Sets the sku.
     *
     *  @param sku a stock keeping unit
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sku</code> is
     *          <code>null</code>
     */

    public T sku(String sku) {
        getMiter().setSKU(sku);
        return (self());
    }


    /**
     *  Adds a model.
     *
     *  @param model a model
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>model</code> is
     *          <code>null</code>
     */

    public T model(org.osid.inventory.Model model) {
        getMiter().addModel(model);
        return (self());
    }


    /**
     *  Sets all the models.
     *
     *  @param models a collection of models
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>models</code> is
     *          <code>null</code>
     */

    public T models(java.util.Collection<org.osid.inventory.Model> models) {
        getMiter().setModels(models);
        return (self());
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public T locationDescription(org.osid.locale.DisplayText description) {
        getMiter().setLocationDescription(description);
        return (self());
    }


    /**
     *  Adds a location.
     *
     *  @param location a location
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    public T location(org.osid.mapping.Location location) {
        getMiter().addLocation(location);
        return (self());
    }


    /**
     *  Sets all the locations.
     *
     *  @param locations a collection of locations
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>locations</code>
     *          is <code>null</code>
     */

    public T locations(java.util.Collection<org.osid.mapping.Location> locations) {
        getMiter().setLocations(locations);
        return (self());
    }


    /**
     *  Adds a Stock record.
     *
     *  @param record a stock record
     *  @param recordType the type of stock record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inventory.records.StockRecord record, org.osid.type.Type recordType) {
        getMiter().addStockRecord(record, recordType);
        return (self());
    }
}       


