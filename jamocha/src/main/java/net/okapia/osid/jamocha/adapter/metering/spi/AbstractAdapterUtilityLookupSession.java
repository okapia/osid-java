//
// AbstractAdapterUtilityLookupSession.java
//
//    An Utility lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.metering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Utility lookup session adapter.
 */

public abstract class AbstractAdapterUtilityLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.metering.UtilityLookupSession {

    private final org.osid.metering.UtilityLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterUtilityLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterUtilityLookupSession(org.osid.metering.UtilityLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Utility} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupUtilities() {
        return (this.session.canLookupUtilities());
    }


    /**
     *  A complete view of the {@code Utility} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeUtilityView() {
        this.session.useComparativeUtilityView();
        return;
    }


    /**
     *  A complete view of the {@code Utility} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryUtilityView() {
        this.session.usePlenaryUtilityView();
        return;
    }

     
    /**
     *  Gets the {@code Utility} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Utility} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Utility} and
     *  retained for compatibility.
     *
     *  @param utilityId {@code Id} of the {@code Utility}
     *  @return the utility
     *  @throws org.osid.NotFoundException {@code utilityId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code utilityId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUtility(utilityId));
    }


    /**
     *  Gets an {@code UtilityList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  utilities specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Utilities} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  utilityIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Utility} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code utilityIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByIds(org.osid.id.IdList utilityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUtilitiesByIds(utilityIds));
    }


    /**
     *  Gets an {@code UtilityList} corresponding to the given
     *  utility genus {@code Type} which does not include
     *  utilities of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  utilityGenusType an utility genus type 
     *  @return the returned {@code Utility} list
     *  @throws org.osid.NullArgumentException
     *          {@code utilityGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByGenusType(org.osid.type.Type utilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUtilitiesByGenusType(utilityGenusType));
    }


    /**
     *  Gets an {@code UtilityList} corresponding to the given
     *  utility genus {@code Type} and include any additional
     *  utilities with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  utilityGenusType an utility genus type 
     *  @return the returned {@code Utility} list
     *  @throws org.osid.NullArgumentException
     *          {@code utilityGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByParentGenusType(org.osid.type.Type utilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUtilitiesByParentGenusType(utilityGenusType));
    }


    /**
     *  Gets an {@code UtilityList} containing the given
     *  utility record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  utilityRecordType an utility record type 
     *  @return the returned {@code Utility} list
     *  @throws org.osid.NullArgumentException
     *          {@code utilityRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByRecordType(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUtilitiesByRecordType(utilityRecordType));
    }


    /**
     *  Gets an {@code UtilityList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Utility} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUtilitiesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Utilities}. 
     *
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Utilities} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getUtilities());
    }
}
