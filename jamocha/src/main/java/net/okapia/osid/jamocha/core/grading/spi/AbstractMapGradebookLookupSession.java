//
// AbstractMapGradebookLookupSession
//
//    A simple framework for providing a Gradebook lookup service
//    backed by a fixed collection of gradebooks.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Gradebook lookup service backed by a
 *  fixed collection of gradebooks. The gradebooks are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Gradebooks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapGradebookLookupSession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradebookLookupSession
    implements org.osid.grading.GradebookLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.grading.Gradebook> gradebooks = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.grading.Gradebook>());


    /**
     *  Makes a <code>Gradebook</code> available in this session.
     *
     *  @param  gradebook a gradebook
     *  @throws org.osid.NullArgumentException <code>gradebook<code>
     *          is <code>null</code>
     */

    protected void putGradebook(org.osid.grading.Gradebook gradebook) {
        this.gradebooks.put(gradebook.getId(), gradebook);
        return;
    }


    /**
     *  Makes an array of gradebooks available in this session.
     *
     *  @param  gradebooks an array of gradebooks
     *  @throws org.osid.NullArgumentException <code>gradebooks<code>
     *          is <code>null</code>
     */

    protected void putGradebooks(org.osid.grading.Gradebook[] gradebooks) {
        putGradebooks(java.util.Arrays.asList(gradebooks));
        return;
    }


    /**
     *  Makes a collection of gradebooks available in this session.
     *
     *  @param  gradebooks a collection of gradebooks
     *  @throws org.osid.NullArgumentException <code>gradebooks<code>
     *          is <code>null</code>
     */

    protected void putGradebooks(java.util.Collection<? extends org.osid.grading.Gradebook> gradebooks) {
        for (org.osid.grading.Gradebook gradebook : gradebooks) {
            this.gradebooks.put(gradebook.getId(), gradebook);
        }

        return;
    }


    /**
     *  Removes a Gradebook from this session.
     *
     *  @param  gradebookId the <code>Id</code> of the gradebook
     *  @throws org.osid.NullArgumentException <code>gradebookId<code> is
     *          <code>null</code>
     */

    protected void removeGradebook(org.osid.id.Id gradebookId) {
        this.gradebooks.remove(gradebookId);
        return;
    }


    /**
     *  Gets the <code>Gradebook</code> specified by its <code>Id</code>.
     *
     *  @param  gradebookId <code>Id</code> of the <code>Gradebook</code>
     *  @return the gradebook
     *  @throws org.osid.NotFoundException <code>gradebookId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>gradebookId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.grading.Gradebook gradebook = this.gradebooks.get(gradebookId);
        if (gradebook == null) {
            throw new org.osid.NotFoundException("gradebook not found: " + gradebookId);
        }

        return (gradebook);
    }


    /**
     *  Gets all <code>Gradebooks</code>. In plenary mode, the returned
     *  list contains all known gradebooks or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradebooks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Gradebooks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradebook.ArrayGradebookList(this.gradebooks.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradebooks.clear();
        super.close();
        return;
    }
}
