//
// AbstractFederatingParticipantLookupSession.java
//
//     An abstract federating adapter for a ParticipantLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ParticipantLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingParticipantLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.offering.ParticipantLookupSession>
    implements org.osid.offering.ParticipantLookupSession {

    private boolean parallel = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();


    /**
     *  Constructs a new <code>AbstractFederatingParticipantLookupSession</code>.
     */

    protected AbstractFederatingParticipantLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.offering.ParticipantLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>Participant</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParticipants() {
        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            if (session.canLookupParticipants()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Participant</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParticipantView() {
        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            session.useComparativeParticipantView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Participant</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParticipantView() {
        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            session.usePlenaryParticipantView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include participants in catalogues which are
     *  children of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            session.useFederatedCatalogueView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            session.useIsolatedCatalogueView();
        }

        return;
    }


    /**
     *  Only participants whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveParticipantView() {
        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            session.useEffectiveParticipantView();
        }

        return;
    }


    /**
     *  All participants of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveParticipantView() {
        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            session.useAnyEffectiveParticipantView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Participant</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Participant</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Participant</code> and
     *  retained for compatibility.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  participantId <code>Id</code> of the
     *          <code>Participant</code>
     *  @return the participant
     *  @throws org.osid.NotFoundException <code>participantId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>participantId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Participant getParticipant(org.osid.id.Id participantId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            try {
                return (session.getParticipant(participantId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(participantId + " not found");
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  participants specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Participants</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  participantIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>participantIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByIds(org.osid.id.IdList participantIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.offering.participant.MutableParticipantList ret = new net.okapia.osid.jamocha.offering.participant.MutableParticipantList();

        try (org.osid.id.IdList ids = participantIds) {
            while (ids.hasNext()) {
                ret.addParticipant(getParticipant(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  participant genus <code>Type</code> which does not include
     *  participants of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByGenusType(participantGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  participant genus <code>Type</code> and include any additional
     *  participants with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByParentGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByParentGenusType(participantGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ParticipantList</code> containing the given
     *  participant record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  participantRecordType a participant record type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByRecordType(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByRecordType(participantRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ParticipantList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In active mode, participants are returned that are currently
     *  active. In any status mode, active and inactive participants
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Participant</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>ParticipantList</code> by genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In active mode, participants are returned that are currently
     *  active. In any status mode, active and inactive participants
     *  are returned.
     *
     *  @param participantGenusType an offering genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Participant</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeOnDate(org.osid.type.Type participantGenusType, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByGenusTypeOnDate(participantGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a list of participants corresponding to an offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsForOffering(org.osid.id.Id offeringId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsForOffering(offeringId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants by genus type corresponding to an
     *  offering <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param participantGenusType an offering genus type
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>
     *          or <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOffering(org.osid.id.Id offeringId,
                                                                                    org.osid.type.Type participantGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByGenusTypeForOffering(offeringId, participantGenusType));
        }

        ret.noMore();
        return (ret);
    }

     
    /**
     *  Gets a list of participants corresponding to an offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingOnDate(org.osid.id.Id offeringId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsForOfferingOnDate(offeringId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants by genus type corresponding to an
     *  offering <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param participantGenusType a participant genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offeringId</code>,
     *          <code>participantGenusType</code>, <code>from</code>,
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingOnDate(org.osid.id.Id offeringId,
                                                                                         org.osid.type.Type participantGenusType,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByGenusTypeForOfferingOnDate(offeringId, participantGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code> ParticipantList </code> for the given offering
     *  in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param offeringId an offering <code> Id </code>
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId
     *          </code> or <code> timePeriodId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOffering(org.osid.id.Id offeringId, 
                                                                                    org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodForOffering(offeringId, timePeriodId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for the
     *  given offering in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          timePeriodId, </code> or <code> participantGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOffering(org.osid.id.Id offeringId, 
                                                                                                org.osid.id.Id timePeriodId, 
                                                                                                org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodAndGenusTypeForOffering(offeringId, timePeriodId, participantGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> for an offering in a
     *  time period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          timePeriodId, from, </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingOnDate(org.osid.id.Id offeringId, 
                                                                                          org.osid.id.Id timePeriodId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodForOfferingOnDate(offeringId, timePeriodId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for an
     *  offering in a time period and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          timePeriodId, participantGenusType, from, </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingOnDate(org.osid.id.Id offeringId, 
                                                                                                      org.osid.id.Id timePeriodId, 
                                                                                                      org.osid.type.Type participantGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodAndGenusTypeForOfferingOnDate(offeringId, timePeriodId, participantGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants by genus type corresponding to a
     *  resource <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param participantGenusType an resource genus type
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>participantGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsByGenusTypeForResource(org.osid.id.Id resourceId,
                                                                                    org.osid.type.Type participantGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
           ret.addParticipantList(session.getParticipantsByGenusTypeForResource(resourceId, participantGenusType));
        }

        ret.noMore();
        return (ret);
    }

     
    /**
     *  Gets a list of participants corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForResourceOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants by genus type corresponding to a
     *  resource <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param participantGenusType a participant genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>,
     *          <code>participantGenusType</code>, <code>from</code>,
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId,
                                                                                         org.osid.type.Type participantGenusType,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByGenusTypeForResourceOnDate(resourceId, participantGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }

    

    /**
     *  Gets a <code> ParticipantList </code> for the given resource
     *  in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> timePeriodId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForResource(org.osid.id.Id resourceId, 
                                                                                    org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodForResource(resourceId, timePeriodId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for the
     *  given resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          timePeriodId, </code> or <code> participantGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                                org.osid.id.Id timePeriodId, 
                                                                                                org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodAndGenusTypeForResource(resourceId, timePeriodId, participantGenusType));
        }
                
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> for a resource in a time
     *  period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          timePeriodId, from, </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                          org.osid.id.Id timePeriodId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodForResourceOnDate(resourceId, timePeriodId, from, to));
        }
                
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for a
     *  resource in a time period and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          timePeriodId, participantGenusType, from, </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                                      org.osid.id.Id timePeriodId, 
                                                                                                      org.osid.type.Type participantGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodAndGenusTypeForResourceOnDate(resourceId, timePeriodId, participantGenusType, from, to));
        }
                
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants corresponding to offering and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResource(org.osid.id.Id offeringId,
                                                                                   org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsForOfferingAndResource(offeringId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants by genus type corresponding to
     *  offering and resource <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param participantGenusType an offering genus type
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offeringId</code>, <code>resourceId</code>, or,
     *          <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingAndResource(org.osid.id.Id offeringId,
                                                                                              org.osid.id.Id resourceId,
                                                                                              org.osid.type.Type participantGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByGenusTypeForOfferingAndResource(offeringId, resourceId, participantGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a list of participants corresponding to offering and
     *  resource <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResourceOnDate(org.osid.id.Id offeringId,
                                                                                         org.osid.id.Id resourceId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsForOfferingAndResourceOnDate(offeringId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of participants by genus type corresponding to
     *  offering and resource <code>Ids</code> and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  participantGenusType an offering genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>offeringId</code>, <code>resourceId</code>,
     *          <code>participantGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingAndResourceOnDate(org.osid.id.Id offeringId,
                                                                                                    org.osid.id.Id resourceId,
                                                                                                    org.osid.type.Type participantGenusType,
                                                                                                    org.osid.calendaring.DateTime from,
                                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByGenusTypeForOfferingAndResourceOnDate(offeringId, resourceId, participantGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code> ParticipantList </code> for the given offering
     *  and resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, </code> or <code> timePeriodId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingAndResource(org.osid.id.Id offeringId, 
                                                                                               org.osid.id.Id resourceId, 
                                                                                               org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodForOfferingAndResource(offeringId, resourceId, timePeriodId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code> ParticipantList </code> by genus type for the
     *  given offering and resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code> ParticipantList </code> 
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, </code> <code> timePeriodId </code> or
     *          <code> participantGenusType </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingAndResource(org.osid.id.Id offeringId, 
                                                                                                           org.osid.id.Id resourceId, 
                                                                                                           org.osid.id.Id timePeriodId, 
                                                                                                           org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodAndGenusTypeForOfferingAndResource(offeringId, resourceId, timePeriodId, participantGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> for an offering and
     *  resource in a time period and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, timePeriodId, from, </code> or <code> to
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingAndResourceOnDate(org.osid.id.Id offeringId, 
                                                                                                     org.osid.id.Id resourceId, 
                                                                                                     org.osid.id.Id timePeriodId, 
                                                                                                     org.osid.calendaring.DateTime from, 
                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodForOfferingAndResourceOnDate(offeringId, resourceId, timePeriodId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ParticipantList </code> by genus type for an
     *  offering and resource in a time period and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> offeringId,
     *          resourceId, timePeriodId,participantGenusType, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingAndResourceOnDate(org.osid.id.Id offeringId, 
                                                                                                                 org.osid.id.Id resourceId, 
                                                                                                                 org.osid.id.Id timePeriodId, 
                                                                                                                 org.osid.type.Type participantGenusType, 
                                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipantsByTimePeriodAndGenusTypeForOfferingAndResourceOnDate(offeringId, resourceId, timePeriodId, participantGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>Participants</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @return a list of <code>Participants</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipants()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList ret = getParticipantList();

        for (org.osid.offering.ParticipantLookupSession session : getSessions()) {
            ret.addParticipantList(session.getParticipants());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.offering.participant.FederatingParticipantList getParticipantList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.participant.ParallelParticipantList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.participant.CompositeParticipantList());
        }
    }
}
