//
// EmptyFloorBatchFormList
//
//     Implements an empty FloorBatchFormList.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.room.batch.floorbatchform;


/**
 *  Implements an empty FloorBatchFormList.
 */

public final class EmptyFloorBatchFormList
    extends net.okapia.osid.jamocha.room.batch.floorbatchform.spi.AbstractFloorBatchFormList
    implements org.osid.room.batch.FloorBatchFormList {


    /**
     *  Creates a new empty <code>FloorBatchFormList</code>.
     */

    public EmptyFloorBatchFormList() {
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>false</code>
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    public boolean hasNext() {
        return (false);
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *
     *  @return 0
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    public long available() {
        return (0);
    }


    /**
     *  Does nothing.
     *
     *  @param  n 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    public void skip(long n) {
        return;
    }


    /**
     *  There are no elements in this list. Throws exception.
     *
     *  @return the next <code>FloorBatchForm</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>FloorBatchForm</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    public org.osid.room.batch.FloorBatchForm getNextFloorBatchForm()
        throws org.osid.OperationFailedException {

        throw new org.osid.IllegalStateException("no more elements available in floorbatchform list");
    }
}
