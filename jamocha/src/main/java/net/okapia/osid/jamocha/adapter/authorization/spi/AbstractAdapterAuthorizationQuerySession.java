//
// AbstractQueryAuthorizationLookupSession.java
//
//    An AuthorizationQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuthorizationQuerySession adapter.
 */

public abstract class AbstractAdapterAuthorizationQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authorization.AuthorizationQuerySession {

    private final org.osid.authorization.AuthorizationQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAuthorizationQuerySession.
     *
     *  @param session the underlying authorization query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuthorizationQuerySession(org.osid.authorization.AuthorizationQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeVault</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeVault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the {@codeVault</code> associated with this 
     *  session.
     *
     *  @return the {@codeVault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform {@codeAuthorization</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAuthorizations() {
        return (this.session.canSearchAuthorizations());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include authorizations in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this vault only.
     */
    
    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    
      
    /**
     *  Sets the view for methods in this session to implicit
     *  authorizations.  An implicit view will include authorizations
     *  derived from other authorizations as a result of the {@code
     *  Qualifier}, {@code Function} or {@code Resource}
     *  hierarchies. This method is the opposite of {@code
     *  explicitAuthorizationView()}.
     */

    @OSID @Override
    public void useImplicitAuthorizationView() {
        this.session.useImplicitAuthorizationView();
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit
     *  authorizations.  An explicit view includes only those
     *  authorizations that were explicitly defined and not
     *  implied. This method is the opposite of {@code
     *  implicitAuthorizationView()}.
     */

    public void useExplicitAuthorizationView() {
        this.session.useExplicitAuthorizationView();
        return;
    }


    /**
     *  Gets an authorization query. The returned query will not have an
     *  extension query.
     *
     *  @return the authorization query 
     */
      
    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getAuthorizationQuery() {
        return (this.session.getAuthorizationQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  authorizationQuery the authorization query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code authorizationQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code authorizationQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByQuery(org.osid.authorization.AuthorizationQuery authorizationQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAuthorizationsByQuery(authorizationQuery));
    }
}
