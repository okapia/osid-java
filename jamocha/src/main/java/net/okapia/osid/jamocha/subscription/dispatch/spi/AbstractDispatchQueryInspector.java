//
// AbstractDispatchQueryInspector.java
//
//     A template for making a DispatchQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.dispatch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for dispatches.
 */

public abstract class AbstractDispatchQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.subscription.DispatchQueryInspector {

    private final java.util.Collection<org.osid.subscription.records.DispatchQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the address genus <code> Type </code> terms. 
     *
     *  @return the address genus <code> Type </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getAddressGenusTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the subscription <code> Id </code> terms. 
     *
     *  @return the subscription <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubscriptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subscription terms. 
     *
     *  @return the subscription terms 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQueryInspector[] getSubscriptionTerms() {
        return (new org.osid.subscription.SubscriptionQueryInspector[0]);
    }


    /**
     *  Gets the publisher <code> Id </code> terms. 
     *
     *  @return the publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPublisherIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the publisher terms. 
     *
     *  @return the publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given dispatch query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a dispatch implementing the requested record.
     *
     *  @param dispatchRecordType a dispatch record type
     *  @return the dispatch query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dispatchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchQueryInspectorRecord getDispatchQueryInspectorRecord(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.DispatchQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dispatchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this dispatch query. 
     *
     *  @param dispatchQueryInspectorRecord dispatch query inspector
     *         record
     *  @param dispatchRecordType dispatch record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDispatchQueryInspectorRecord(org.osid.subscription.records.DispatchQueryInspectorRecord dispatchQueryInspectorRecord, 
                                                   org.osid.type.Type dispatchRecordType) {

        addRecordType(dispatchRecordType);
        nullarg(dispatchRecordType, "dispatch record type");
        this.records.add(dispatchQueryInspectorRecord);        
        return;
    }
}
