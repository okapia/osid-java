//
// AbstractAdapterDemographicLookupSession.java
//
//    A Demographic lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Demographic lookup session adapter.
 */

public abstract class AbstractAdapterDemographicLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.demographic.DemographicLookupSession {

    private final org.osid.resource.demographic.DemographicLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDemographicLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDemographicLookupSession(org.osid.resource.demographic.DemographicLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bin/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bin Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the {@code Bin} associated with this session.
     *
     *  @return the {@code Bin} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform {@code Demographic} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDemographics() {
        return (this.session.canLookupDemographics());
    }


    /**
     *  A complete view of the {@code Demographic} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDemographicView() {
        this.session.useComparativeDemographicView();
        return;
    }


    /**
     *  A complete view of the {@code Demographic} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDemographicView() {
        this.session.usePlenaryDemographicView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include demographics in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    

    /**
     *  Only active demographics are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveDemographicView() {
        this.session.useActiveDemographicView();
        return;
    }


    /**
     *  Active and inactive demographics are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDemographicView() {
        this.session.useAnyStatusDemographicView();
        return;
    }
    
     
    /**
     *  Gets the {@code Demographic} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Demographic} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Demographic} and
     *  retained for compatibility.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param demographicId {@code Id} of the {@code Demographic}
     *  @return the demographic
     *  @throws org.osid.NotFoundException {@code demographicId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code demographicId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.Demographic getDemographic(org.osid.id.Id demographicId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographic(demographicId));
    }


    /**
     *  Gets a {@code DemographicList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  demographics specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Demographics} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Demographic} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code demographicIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByIds(org.osid.id.IdList demographicIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicsByIds(demographicIds));
    }


    /**
     *  Gets a {@code DemographicList} corresponding to the given
     *  demographic genus {@code Type} which does not include
     *  demographics of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicGenusType a demographic genus type 
     *  @return the returned {@code Demographic} list
     *  @throws org.osid.NullArgumentException
     *          {@code demographicGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByGenusType(org.osid.type.Type demographicGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicsByGenusType(demographicGenusType));
    }


    /**
     *  Gets a {@code DemographicList} corresponding to the given
     *  demographic genus {@code Type} and include any additional
     *  demographics with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicGenusType a demographic genus type 
     *  @return the returned {@code Demographic} list
     *  @throws org.osid.NullArgumentException
     *          {@code demographicGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByParentGenusType(org.osid.type.Type demographicGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicsByParentGenusType(demographicGenusType));
    }


    /**
     *  Gets a {@code DemographicList} containing the given
     *  demographic record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @param  demographicRecordType a demographic record type 
     *  @return the returned {@code Demographic} list
     *  @throws org.osid.NullArgumentException
     *          {@code demographicRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographicsByRecordType(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographicsByRecordType(demographicRecordType));
    }


    /**
     *  Gets all {@code Demographics}. 
     *
     *  In plenary mode, the returned list contains all known
     *  demographics or an error results. Otherwise, the returned list
     *  may contain only those demographics that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, demographics are returned that are currently
     *  active. In any status mode, active and inactive demographics
     *  are returned.
     *
     *  @return a list of {@code Demographics} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographics()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDemographics());
    }
}
