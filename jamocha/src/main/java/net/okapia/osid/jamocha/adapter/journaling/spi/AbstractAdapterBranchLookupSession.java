//
// AbstractAdapterBranchLookupSession.java
//
//    A Branch lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.journaling.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Branch lookup session adapter.
 */

public abstract class AbstractAdapterBranchLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.journaling.BranchLookupSession {

    private final org.osid.journaling.BranchLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBranchLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBranchLookupSession(org.osid.journaling.BranchLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Journal/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Journal Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.session.getJournalId());
    }


    /**
     *  Gets the {@code Journal} associated with this session.
     *
     *  @return the {@code Journal} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getJournal());
    }


    /**
     *  Tests if this user can perform {@code Branch} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBranches() {
        return (this.session.canLookupBranches());
    }


    /**
     *  A complete view of the {@code Branch} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBranchView() {
        this.session.useComparativeBranchView();
        return;
    }


    /**
     *  A complete view of the {@code Branch} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBranchView() {
        this.session.usePlenaryBranchView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include branches in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        this.session.useFederatedJournalView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        this.session.useIsolatedJournalView();
        return;
    }
    

    /**
     *  Only active branches are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBranchView() {
        this.session.useActiveBranchView();
        return;
    }


    /**
     *  Active and inactive branches are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBranchView() {
        this.session.useAnyStatusBranchView();
        return;
    }
    
     
    /**
     *  Gets the {@code Branch} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Branch} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Branch} and
     *  retained for compatibility.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param branchId {@code Id} of the {@code Branch}
     *  @return the branch
     *  @throws org.osid.NotFoundException {@code branchId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code branchId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Branch getBranch(org.osid.id.Id branchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBranch(branchId));
    }


    /**
     *  Gets a {@code BranchList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  branches specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Branches} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Branch} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code branchIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByIds(org.osid.id.IdList branchIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBranchesByIds(branchIds));
    }


    /**
     *  Gets a {@code BranchList} corresponding to the given
     *  branch genus {@code Type} which does not include
     *  branches of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchGenusType a branch genus type 
     *  @return the returned {@code Branch} list
     *  @throws org.osid.NullArgumentException
     *          {@code branchGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByGenusType(org.osid.type.Type branchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBranchesByGenusType(branchGenusType));
    }


    /**
     *  Gets a {@code BranchList} corresponding to the given
     *  branch genus {@code Type} and include any additional
     *  branches with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchGenusType a branch genus type 
     *  @return the returned {@code Branch} list
     *  @throws org.osid.NullArgumentException
     *          {@code branchGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByParentGenusType(org.osid.type.Type branchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBranchesByParentGenusType(branchGenusType));
    }


    /**
     *  Gets a {@code BranchList} containing the given
     *  branch record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchRecordType a branch record type 
     *  @return the returned {@code Branch} list
     *  @throws org.osid.NullArgumentException
     *          {@code branchRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByRecordType(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBranchesByRecordType(branchRecordType));
    }


    /**
     *  Gets all {@code Branches}. 
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @return a list of {@code Branches} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranches()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBranches());
    }
}
