//
// MutableIndexedMapRecurringEventEnablerLookupSession
//
//    Implements a RecurringEventEnabler lookup service backed by a collection of
//    recurringEventEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements a RecurringEventEnabler lookup service backed by a collection of
 *  recurring event enablers. The recurring event enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some recurring event enablers may be compatible
 *  with more types than are indicated through these recurring event enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of recurring event enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRecurringEventEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractIndexedMapRecurringEventEnablerLookupSession
    implements org.osid.calendaring.rules.RecurringEventEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRecurringEventEnablerLookupSession} with no recurring event enablers.
     *
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumentException {@code calendar}
     *          is {@code null}
     */

      public MutableIndexedMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRecurringEventEnablerLookupSession} with a
     *  single recurring event enabler.
     *  
     *  @param calendar the calendar
     *  @param  recurringEventEnabler a single recurringEventEnabler
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEventEnabler} is {@code null}
     */

    public MutableIndexedMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler) {
        this(calendar);
        putRecurringEventEnabler(recurringEventEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRecurringEventEnablerLookupSession} using an
     *  array of recurring event enablers.
     *
     *  @param calendar the calendar
     *  @param  recurringEventEnablers an array of recurring event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEventEnablers} is {@code null}
     */

    public MutableIndexedMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.rules.RecurringEventEnabler[] recurringEventEnablers) {
        this(calendar);
        putRecurringEventEnablers(recurringEventEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRecurringEventEnablerLookupSession} using a
     *  collection of recurring event enablers.
     *
     *  @param calendar the calendar
     *  @param  recurringEventEnablers a collection of recurring event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEventEnablers} is {@code null}
     */

    public MutableIndexedMapRecurringEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.rules.RecurringEventEnabler> recurringEventEnablers) {

        this(calendar);
        putRecurringEventEnablers(recurringEventEnablers);
        return;
    }
    

    /**
     *  Makes a {@code RecurringEventEnabler} available in this session.
     *
     *  @param  recurringEventEnabler a recurring event enabler
     *  @throws org.osid.NullArgumentException {@code recurringEventEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRecurringEventEnabler(org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler) {
        super.putRecurringEventEnabler(recurringEventEnabler);
        return;
    }


    /**
     *  Makes an array of recurring event enablers available in this session.
     *
     *  @param  recurringEventEnablers an array of recurring event enablers
     *  @throws org.osid.NullArgumentException {@code recurringEventEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putRecurringEventEnablers(org.osid.calendaring.rules.RecurringEventEnabler[] recurringEventEnablers) {
        super.putRecurringEventEnablers(recurringEventEnablers);
        return;
    }


    /**
     *  Makes collection of recurring event enablers available in this session.
     *
     *  @param  recurringEventEnablers a collection of recurring event enablers
     *  @throws org.osid.NullArgumentException {@code recurringEventEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRecurringEventEnablers(java.util.Collection<? extends org.osid.calendaring.rules.RecurringEventEnabler> recurringEventEnablers) {
        super.putRecurringEventEnablers(recurringEventEnablers);
        return;
    }


    /**
     *  Removes a RecurringEventEnabler from this session.
     *
     *  @param recurringEventEnablerId the {@code Id} of the recurring event enabler
     *  @throws org.osid.NullArgumentException {@code recurringEventEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId) {
        super.removeRecurringEventEnabler(recurringEventEnablerId);
        return;
    }    
}
