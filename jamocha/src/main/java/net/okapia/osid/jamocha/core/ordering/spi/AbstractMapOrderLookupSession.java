//
// AbstractMapOrderLookupSession
//
//    A simple framework for providing an Order lookup service
//    backed by a fixed collection of orders.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Order lookup service backed by a
 *  fixed collection of orders. The orders are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Orders</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOrderLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractOrderLookupSession
    implements org.osid.ordering.OrderLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ordering.Order> orders = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ordering.Order>());


    /**
     *  Makes an <code>Order</code> available in this session.
     *
     *  @param  order an order
     *  @throws org.osid.NullArgumentException <code>order<code>
     *          is <code>null</code>
     */

    protected void putOrder(org.osid.ordering.Order order) {
        this.orders.put(order.getId(), order);
        return;
    }


    /**
     *  Makes an array of orders available in this session.
     *
     *  @param  orders an array of orders
     *  @throws org.osid.NullArgumentException <code>orders<code>
     *          is <code>null</code>
     */

    protected void putOrders(org.osid.ordering.Order[] orders) {
        putOrders(java.util.Arrays.asList(orders));
        return;
    }


    /**
     *  Makes a collection of orders available in this session.
     *
     *  @param  orders a collection of orders
     *  @throws org.osid.NullArgumentException <code>orders<code>
     *          is <code>null</code>
     */

    protected void putOrders(java.util.Collection<? extends org.osid.ordering.Order> orders) {
        for (org.osid.ordering.Order order : orders) {
            this.orders.put(order.getId(), order);
        }

        return;
    }


    /**
     *  Removes an Order from this session.
     *
     *  @param  orderId the <code>Id</code> of the order
     *  @throws org.osid.NullArgumentException <code>orderId<code> is
     *          <code>null</code>
     */

    protected void removeOrder(org.osid.id.Id orderId) {
        this.orders.remove(orderId);
        return;
    }


    /**
     *  Gets the <code>Order</code> specified by its <code>Id</code>.
     *
     *  @param  orderId <code>Id</code> of the <code>Order</code>
     *  @return the order
     *  @throws org.osid.NotFoundException <code>orderId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>orderId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder(org.osid.id.Id orderId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ordering.Order order = this.orders.get(orderId);
        if (order == null) {
            throw new org.osid.NotFoundException("order not found: " + orderId);
        }

        return (order);
    }


    /**
     *  Gets all <code>Orders</code>. In plenary mode, the returned
     *  list contains all known orders or an error
     *  results. Otherwise, the returned list may contain only those
     *  orders that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Orders</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.order.ArrayOrderList(this.orders.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.orders.clear();
        super.close();
        return;
    }
}
