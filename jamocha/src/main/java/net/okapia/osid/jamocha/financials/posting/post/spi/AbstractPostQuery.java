//
// AbstractPostQuery.java
//
//     A template for making a Post Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.post.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for posts.
 */

public abstract class AbstractPostQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.financials.posting.PostQuery {

    private final java.util.Collection<org.osid.financials.posting.records.PostQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FiscalPeriod </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Matches any fiscal period. 
     *
     *  @param  match <code> true </code> to match posts with any fiscal 
     *          period, <code> false </code> to match posts with no fiscal 
     *          period 
     */

    @OSID @Override
    public void matchAnyFiscalPeriod(boolean match) {
        return;
    }


    /**
     *  Clears the fiscal period terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        return;
    }


    /**
     *  Matches posts that have been posted. 
     *
     *  @param  match <code> true </code> to match posted posts <code> false 
     *          </code> to match unposted posts 
     */

    @OSID @Override
    public void matchPosted(boolean match) {
        return;
    }


    /**
     *  Clears the posted terms. 
     */

    @OSID @Override
    public void clearPostedTerms() {
        return;
    }


    /**
     *  Matches the date between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime low, 
                          org.osid.calendaring.DateTime high, boolean match) {
        return;
    }


    /**
     *  Matches items that have any date set. 
     *
     *  @param  match <code> true </code> to match items with any date, <code> 
     *          false </code> to match items with no date 
     */

    @OSID @Override
    public void matchAnyDate(boolean match) {
        return;
    }


    /**
     *  Clears the date terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        return;
    }


    /**
     *  Sets the post entry <code> Id </code> for this query to match posts 
     *  that have a related post entries. 
     *
     *  @param  postEntryId a post entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPostEntryId(org.osid.id.Id postEntryId, boolean match) {
        return;
    }


    /**
     *  Clears the post entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PostEntryQuery </code> is available for the 
     *  location. 
     *
     *  @return <code> true </code> if a post entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post entries. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the post entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuery getPostEntryQuery() {
        throw new org.osid.UnimplementedException("supportsPostEntryQuery() is false");
    }


    /**
     *  Matches any posts with post entries. 
     *
     *  @param  match <code> true </code> to match posts with any post entry, 
     *          <code> false </code> to match posts with no entries 
     */

    @OSID @Override
    public void matchAnyPostEntry(boolean match) {
        return;
    }


    /**
     *  Clears the post entry terms. 
     */

    @OSID @Override
    public void clearPostEntryTerms() {
        return;
    }


    /**
     *  Sets the post entry <code> Id </code> for this query to match posts 
     *  that have a corrected post. 
     *
     *  @param  postId a post <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCorrectedPostId(org.osid.id.Id postId, boolean match) {
        return;
    }


    /**
     *  Clears the corrected post <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCorrectedPostIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PostQuery </code> is available. 
     *
     *  @return <code> true </code> if a post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCorrectedPostQuery() {
        return (false);
    }


    /**
     *  Gets the query for a corrected posts. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the post query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCorrectedPostQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuery getCorrectedPostQuery() {
        throw new org.osid.UnimplementedException("supportsCorrectedPostQuery() is false");
    }


    /**
     *  Matches any posts with corrected posts. 
     *
     *  @param  match <code> true </code> to match posts with any corrected 
     *          post, <code> false </code> to match posts with no corrected 
     *          posts 
     */

    @OSID @Override
    public void matchAnyCorrectedPost(boolean match) {
        return;
    }


    /**
     *  Clears the corrected post terms. 
     */

    @OSID @Override
    public void clearCorrectedPostTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match posts 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given post query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a post implementing the requested record.
     *
     *  @param postRecordType a post record type
     *  @return the post query record
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostQueryRecord getPostQueryRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostQueryRecord record : this.records) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Adds a record to this post query. 
     *
     *  @param postQueryRecord post query record
     *  @param postRecordType post record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPostQueryRecord(org.osid.financials.posting.records.PostQueryRecord postQueryRecord, 
                                          org.osid.type.Type postRecordType) {

        addRecordType(postRecordType);
        nullarg(postQueryRecord, "post query record");
        this.records.add(postQueryRecord);        
        return;
    }
}
