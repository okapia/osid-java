//
// AbstractIssueLookupSession.java
//
//    A starter implementation framework for providing an Issue
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Issue lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getIssues(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractIssueLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.tracking.IssueLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();
    

    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>Issue</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupIssues() {
        return (true);
    }


    /**
     *  A complete view of the <code>Issue</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeIssueView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Issue</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryIssueView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include issues in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only issues whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveIssueView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All issues of any effective dates are returned by all methods
     *  in this session.
     */

    @OSID @Override
    public void useAnyEffectiveIssueView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Issue</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Issue</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Issue</code> and retained for
     *  compatibility.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  issueId <code>Id</code> of the
     *          <code>Issue</code>
     *  @return the issue
     *  @throws org.osid.NotFoundException <code>issueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>issueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.Issue getIssue(org.osid.id.Id issueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.tracking.IssueList issues = getIssues()) {
            while (issues.hasNext()) {
                org.osid.tracking.Issue issue = issues.getNextIssue();
                if (issue.getId().equals(issueId)) {
                    return (issue);
                }
            }
        } 

        throw new org.osid.NotFoundException(issueId + " not found");
    }


    /**
     *  Gets an <code>IssueList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the issues
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Issues</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getIssues()</code>.
     *
     *  @param issueIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>issueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByIds(org.osid.id.IdList issueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.tracking.Issue> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = issueIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getIssue(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("issue " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.tracking.issue.LinkedIssueList(ret));
    }


    /**
     *  Gets an <code>IssueList</code> corresponding to the given
     *  issue genus <code>Type</code> which does not include issues of
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getIssues()</code>.
     *
     *  @param issueGenusType an issue genus type
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.IssueGenusFilterList(getIssues(), issueGenusType));
    }


    /**
     *  Gets an <code>IssueList</code> corresponding to the given
     *  issue genus <code>Type</code> and include any additional
     *  issues with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getIssues()</code>.
     *
     *  @param issueGenusType an issue genus type
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByParentGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getIssuesByGenusType(issueGenusType));
    }


    /**
     *  Gets an <code>IssueList</code> containing the given issue
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getIssues()</code>.
     *
     *  @param  issueRecordType an issue record type 
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByRecordType(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.IssueRecordFilterList(getIssues(), issueRecordType));
    }


    /**
     *  Gets an <code>IssueList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *  
     *  In active mode, issues are returned that are currently
     *  active. In any status mode, active and inactive issues are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Issue</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.tracking.IssueList getIssuesOnDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.TemporalIssueFilterList(getIssues(), from, to));
    }
        

    /**
     *  Gets a list of issues corresponding to a queue
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.tracking.IssueList getIssuesForQueue(org.osid.id.Id queueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.IssueFilterList(new QueueFilter(queueId), getIssues()));
    }


    /**
     *  Gets a list of issues corresponding to a queue <code>Id</code>
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param queueId the <code>Id</code> of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueOnDate(org.osid.id.Id queueId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.TemporalIssueFilterList(getIssuesForQueue(queueId), from, to));
    }


    /**
     *  Gets a list of issues corresponding to a customer
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the customer
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.tracking.IssueList getIssuesForCustomer(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.IssueFilterList(new CustomerFilter(resourceId), getIssues()));
    }


    /**
     *  Gets a list of issues corresponding to a customer
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForCustomerOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.TemporalIssueFilterList(getIssuesForCustomer(resourceId), from, to));
    }


    /**
     *  Gets a list of issues corresponding to queue and customer
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  resourceId the <code>Id</code> of the customer
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueAndCustomer(org.osid.id.Id queueId,
                                                                        org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.IssueFilterList(new CustomerFilter(resourceId), getIssuesForQueue(queueId)));
    }


    /**
     *  Gets a list of issues corresponding to queue and customer
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueAndCustomerOnDate(org.osid.id.Id queueId,
                                                                          org.osid.id.Id resourceId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.issue.TemporalIssueFilterList(getIssuesForQueueAndCustomer(queueId, resourceId), from, to));
    }


    /**
     *  Gets all <code>Issues</code>.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Issues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.tracking.IssueList getIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the issue list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of issues
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.tracking.IssueList filterIssuesOnViews(org.osid.tracking.IssueList list)
        throws org.osid.OperationFailedException {

        org.osid.tracking.IssueList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.tracking.issue.EffectiveIssueFilterList(ret);
        }

        return (ret);
    }


    public static class QueueFilter
        implements net.okapia.osid.jamocha.inline.filter.tracking.issue.IssueFilter {
         
        private final org.osid.id.Id queueId;
         
         
        /**
         *  Constructs a new <code>QueueFilter</code>.
         *
         *  @param queueId the queue to filter
         *  @throws org.osid.NullArgumentException
         *          <code>queueId</code> is <code>null</code>
         */
        
        public QueueFilter(org.osid.id.Id queueId) {
            nullarg(queueId, "queue Id");
            this.queueId = queueId;
            return;
        }

         
        /**
         *  Used by the IssueFilterList to filter the issue list based
         *  on queue.
         *
         *  @param issue the issue
         *  @return <code>true</code> to pass the issue,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.tracking.Issue issue) {
            return (issue.getQueueId().equals(this.queueId));
        }
    }


    public static class CustomerFilter
        implements net.okapia.osid.jamocha.inline.filter.tracking.issue.IssueFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>CustomerFilter</code>.
         *
         *  @param resourceId the customer to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public CustomerFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "customer Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the IssueFilterList to filter the 
         *  issue list based on customer.
         *
         *  @param issue the issue
         *  @return <code>true</code> to pass the issue,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.tracking.Issue issue) {
            return (issue.getCustomerId().equals(this.resourceId));
        }
    }
}
