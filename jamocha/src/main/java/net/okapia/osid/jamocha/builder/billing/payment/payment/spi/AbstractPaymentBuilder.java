//
// AbstractPayment.java
//
//     Defines a Payment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.payment.spi;


/**
 *  Defines a <code>Payment</code> builder.
 */

public abstract class AbstractPaymentBuilder<T extends AbstractPaymentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.billing.payment.payment.PaymentMiter payment;


    /**
     *  Constructs a new <code>AbstractPaymentBuilder</code>.
     *
     *  @param payment the payment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPaymentBuilder(net.okapia.osid.jamocha.builder.billing.payment.payment.PaymentMiter payment) {
        super(payment);
        this.payment = payment;
        return;
    }


    /**
     *  Builds the payment.
     *
     *  @return the new payment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.billing.payment.Payment build() {
        (new net.okapia.osid.jamocha.builder.validator.billing.payment.payment.PaymentValidator(getValidations())).validate(this.payment);
        return (new net.okapia.osid.jamocha.builder.billing.payment.payment.ImmutablePayment(this.payment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the payment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.billing.payment.payment.PaymentMiter getMiter() {
        return (this.payment);
    }


    /**
     *  Sets the payer.
     *
     *  @param payer a payer
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>payer</code> is
     *          <code>null</code>
     */

    public T payer(org.osid.billing.payment.Payer payer) {
        getMiter().setPayer(payer);
        return (self());
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public T customer(org.osid.billing.Customer customer) {
        getMiter().setCustomer(customer);
        return (self());
    }


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public T period(org.osid.billing.Period period) {
        getMiter().setPeriod(period);
        return (self());
    }


    /**
     *  Sets the payment date.
     *
     *  @param date a payment date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T paymentDate(org.osid.calendaring.DateTime date) {
        getMiter().setPaymentDate(date);
        return (self());
    }


    /**
     *  Sets the process date.
     *
     *  @param date a process date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T processDate(org.osid.calendaring.DateTime date) {
        getMiter().setProcessDate(date);
        return (self());
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T amount(org.osid.financials.Currency amount) {
        getMiter().setAmount(amount);
        return (self());
    }


    /**
     *  Adds a Payment record.
     *
     *  @param record a payment record
     *  @param recordType the type of payment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.billing.payment.records.PaymentRecord record, org.osid.type.Type recordType) {
        getMiter().addPaymentRecord(record, recordType);
        return (self());
    }
}       


