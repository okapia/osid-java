//
// AbstractIndexedMapRelationshipLookupSession.java
//
//    A simple framework for providing a Relationship lookup service
//    backed by a fixed collection of relationships with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Relationship lookup service backed by a
 *  fixed collection of relationships. The relationships are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some relationships may be compatible
 *  with more types than are indicated through these relationship
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Relationships</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRelationshipLookupSession
    extends AbstractMapRelationshipLookupSession
    implements org.osid.relationship.RelationshipLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.relationship.Relationship> relationshipsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.relationship.Relationship>());
    private final MultiMap<org.osid.type.Type, org.osid.relationship.Relationship> relationshipsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.relationship.Relationship>());


    /**
     *  Makes a <code>Relationship</code> available in this session.
     *
     *  @param  relationship a relationship
     *  @throws org.osid.NullArgumentException <code>relationship<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRelationship(org.osid.relationship.Relationship relationship) {
        super.putRelationship(relationship);

        this.relationshipsByGenus.put(relationship.getGenusType(), relationship);
        
        try (org.osid.type.TypeList types = relationship.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relationshipsByRecord.put(types.getNextType(), relationship);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a relationship from this session.
     *
     *  @param relationshipId the <code>Id</code> of the relationship
     *  @throws org.osid.NullArgumentException <code>relationshipId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRelationship(org.osid.id.Id relationshipId) {
        org.osid.relationship.Relationship relationship;
        try {
            relationship = getRelationship(relationshipId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.relationshipsByGenus.remove(relationship.getGenusType());

        try (org.osid.type.TypeList types = relationship.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relationshipsByRecord.remove(types.getNextType(), relationship);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRelationship(relationshipId);
        return;
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the given
     *  relationship genus <code>Type</code> which does not include
     *  relationships of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known relationships or an error results. Otherwise,
     *  the returned list may contain only those relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.relationship.ArrayRelationshipList(this.relationshipsByGenus.get(relationshipGenusType)));
    }


    /**
     *  Gets a <code>RelationshipList</code> containing the given
     *  relationship record <code>Type</code>. In plenary mode, the
     *  returned list contains all known relationships or an error
     *  results. Otherwise, the returned list may contain only those
     *  relationships that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  relationshipRecordType a relationship record type 
     *  @return the returned <code>relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByRecordType(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.relationship.ArrayRelationshipList(this.relationshipsByRecord.get(relationshipRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relationshipsByGenus.clear();
        this.relationshipsByRecord.clear();

        super.close();

        return;
    }
}
