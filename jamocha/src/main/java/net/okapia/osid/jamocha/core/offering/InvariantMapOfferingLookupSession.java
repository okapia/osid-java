//
// InvariantMapOfferingLookupSession
//
//    Implements an Offering lookup service backed by a fixed collection of
//    offerings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements an Offering lookup service backed by a fixed
 *  collection of offerings. The offerings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapOfferingLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractMapOfferingLookupSession
    implements org.osid.offering.OfferingLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapOfferingLookupSession</code> with no
     *  offerings.
     *  
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumnetException {@code catalogue} is
     *          {@code null}
     */

    public InvariantMapOfferingLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOfferingLookupSession</code> with a single
     *  offering.
     *  
     *  @param catalogue the catalogue
     *  @param offering an single offering
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code offering} is <code>null</code>
     */

      public InvariantMapOfferingLookupSession(org.osid.offering.Catalogue catalogue,
                                               org.osid.offering.Offering offering) {
        this(catalogue);
        putOffering(offering);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOfferingLookupSession</code> using an array
     *  of offerings.
     *  
     *  @param catalogue the catalogue
     *  @param offerings an array of offerings
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code offerings} is <code>null</code>
     */

      public InvariantMapOfferingLookupSession(org.osid.offering.Catalogue catalogue,
                                               org.osid.offering.Offering[] offerings) {
        this(catalogue);
        putOfferings(offerings);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOfferingLookupSession</code> using a
     *  collection of offerings.
     *
     *  @param catalogue the catalogue
     *  @param offerings a collection of offerings
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code offerings} is <code>null</code>
     */

      public InvariantMapOfferingLookupSession(org.osid.offering.Catalogue catalogue,
                                               java.util.Collection<? extends org.osid.offering.Offering> offerings) {
        this(catalogue);
        putOfferings(offerings);
        return;
    }
}
