//
// AbstractActivityQueryInspector.java
//
//     A template for making an ActivityQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for activities.
 */

public abstract class AbstractActivityQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.learning.ActivityQueryInspector {

    private final java.util.Collection<org.osid.learning.records.ActivityQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the objective bank <code> Id </code> query terms. 
     *
     *  @return the objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective bank query terms. 
     *
     *  @return the objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given activity query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an activity implementing the requested record.
     *
     *  @param activityRecordType an activity record type
     *  @return the activity query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ActivityQueryInspectorRecord getActivityQueryInspectorRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ActivityQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity query. 
     *
     *  @param activityQueryInspectorRecord activity query inspector
     *         record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityQueryInspectorRecord(org.osid.learning.records.ActivityQueryInspectorRecord activityQueryInspectorRecord, 
                                                   org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);
        nullarg(activityRecordType, "activity record type");
        this.records.add(activityQueryInspectorRecord);        
        return;
    }
}
