//
// LocaleMiter.java
//
//     Defines a Locale miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.locale.locale;


/**
 *  Defines a <code>Locale</code> miter for use with the builders.
 */

public interface LocaleMiter
    extends net.okapia.osid.jamocha.builder.spi.Miter,
            org.osid.locale.Locale {


    /**
     *  Sets the language <code>Type</code>.
     *
     *  @param type the language type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setLanguageType(org.osid.type.Type type);


    /**
     *  Sets the script <code>Type</code>.
     *
     *  @param type the script type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setScriptType(org.osid.type.Type type);


    /**
     *  Sets the calendar <code>Type</code>.
     *
     *  @param type the calendar type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setCalendarType(org.osid.type.Type type);


    /**
     *  Sets the time <code>Type</code>.
     *
     *  @param type the time type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setTimeType(org.osid.type.Type type);


    /**
     *  Sets the currency <code>Type</code>.
     *
     *  @param type the currency type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setCurrencyType(org.osid.type.Type type);


    /**
     *  Sets the unit system <code>Type</code>.
     *
     *  @param type the unit system type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setUnitSystemType(org.osid.type.Type type);


    /**
     *  Sets the numeric format <code>Type</code>.
     *
     *  @param type the numeric format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setNumericFormatType(org.osid.type.Type type);


    /**
     *  Sets the calendar format <code>Type</code>.
     *
     *  @param type the calendar format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setCalendarFormatType(org.osid.type.Type type);


    /**
     *  Sets the time format <code>Type</code>.
     *
     *  @param type the time format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setTimeFormatType(org.osid.type.Type type);


    /**
     *  Sets the currency format <code>Type</code>.
     *
     *  @param type the currency format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setCurrencyFormatType(org.osid.type.Type type);


    /**
     *  Sets the coordinate format <code>Type</code>.
     *
     *  @param type the coordinate format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void setCoordinateFormatType(org.osid.type.Type type);
}       


