//
// AbstractAssemblyControllerQuery.java
//
//     A ControllerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.controller.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ControllerQuery that stores terms.
 */

public abstract class AbstractAssemblyControllerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.control.ControllerQuery,
               org.osid.control.ControllerQueryInspector,
               org.osid.control.ControllerSearchOrder {

    private final java.util.Collection<org.osid.control.records.ControllerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.ControllerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.ControllerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyControllerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyControllerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Mathes an address. 
     *
     *  @param  address an address 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> address </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchAddress(String address, 
                             org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getAddressColumn(), address, stringMatchType, match);
        return;
    }


    /**
     *  Clears the address query terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        getAssembler().clearTerms(getAddressColumn());
        return;
    }


    /**
     *  Gets the address query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getAddressTerms() {
        return (getAssembler().getStringTerms(getAddressColumn()));
    }


    /**
     *  Orders the results by address. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddress(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAddressColumn(), style);
        return;
    }


    /**
     *  Gets the Address column name.
     *
     * @return the column name
     */

    protected String getAddressColumn() {
        return ("address");
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId the model <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        getAssembler().addIdTerm(getModelIdColumn(), modelId, match);
        return;
    }


    /**
     *  Clears the model <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        getAssembler().clearTerms(getModelIdColumn());
        return;
    }


    /**
     *  Gets the model <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModelIdTerms() {
        return (getAssembler().getIdTerms(getModelIdColumn()));
    }


    /**
     *  Orders the results by model. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByModel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getModelColumn(), style);
        return;
    }


    /**
     *  Gets the ModelId column name.
     *
     * @return the column name
     */

    protected String getModelIdColumn() {
        return ("model_id");
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Model. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches any models. 
     *
     *  @param  match <code> true </code> to match controllers with models, 
     *          <code> false </code> to match controllers with no model 
     *          defined 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        getAssembler().addIdWildcardTerm(getModelColumn(), match);
        return;
    }


    /**
     *  Clears the model query terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        getAssembler().clearTerms(getModelColumn());
        return;
    }


    /**
     *  Gets the model query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inventory.ModelQueryInspector[] getModelTerms() {
        return (new org.osid.inventory.ModelQueryInspector[0]);
    }


    /**
     *  Tests if a model search order is available. 
     *
     *  @return <code> true </code> if a model search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelSearchOrder() {
        return (false);
    }


    /**
     *  Gets the model search order. 
     *
     *  @return the model search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsModelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchOrder getModelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsModelSearchOrder() is false");
    }


    /**
     *  Gets the Model column name.
     *
     * @return the column name
     */

    protected String getModelColumn() {
        return ("model");
    }


    /**
     *  Sets the version for this query. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersion(org.osid.installation.Version version, 
                             boolean match) {
        getAssembler().addVersionTerm(getVersionColumn(), version, match);
        return;
    }


    /**
     *  Matches controllers with any version. 
     *
     *  @param  match <code> true </code> to match controllers with versions, 
     *          <code> false </code> to match controllers with no version 
     *          defined 
     */

    @OSID @Override
    public void matchAnyVersion(boolean match) {
        getAssembler().addVersionWildcardTerm(getVersionColumn(), match);
        return;
    }


    /**
     *  Clears the version query terms. 
     */

    @OSID @Override
    public void clearVersionTerms() {
        getAssembler().clearTerms(getVersionColumn());
        return;
    }


    /**
     *  Gets the version query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionTerms() {
        return (getAssembler().getVersionTerms(getVersionColumn()));
    }


    /**
     *  Orders the results by version. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVersion(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVersionColumn(), style);
        return;
    }


    /**
     *  Gets the Version column name.
     *
     * @return the column name
     */

    protected String getVersionColumn() {
        return ("version");
    }


    /**
     *  Matches controllers with versions including and more recent than the 
     *  given version. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersionSince(org.osid.installation.Version version, 
                                  boolean match) {
        getAssembler().addVersionTerm(getVersionSinceColumn(), version, match);
        return;
    }


    /**
     *  Clears the version since query terms. 
     */

    @OSID @Override
    public void clearVersionSinceTerms() {
        getAssembler().clearTerms(getVersionSinceColumn());
        return;
    }


    /**
     *  Gets the version since query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionSinceTerms() {
        return (getAssembler().getVersionTerms(getVersionSinceColumn()));
    }


    /**
     *  Gets the VersionSince column name.
     *
     * @return the column name
     */

    protected String getVersionSinceColumn() {
        return ("version_since");
    }


    /**
     *  Matches toggleable controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchToggleable(boolean match) {
        getAssembler().addBooleanTerm(getToggleableColumn(), match);
        return;
    }


    /**
     *  Clears the toggleable query terms. 
     */

    @OSID @Override
    public void clearToggleableTerms() {
        getAssembler().clearTerms(getToggleableColumn());
        return;
    }


    /**
     *  Gets the toggle query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getToggleableTerms() {
        return (getAssembler().getBooleanTerms(getToggleableColumn()));
    }


    /**
     *  Orders the results by the toggles. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByToggleable(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getToggleableColumn(), style);
        return;
    }


    /**
     *  Gets the Toggleable column name.
     *
     * @return the column name
     */

    protected String getToggleableColumn() {
        return ("toggleable");
    }


    /**
     *  Matches variable controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchVariable(boolean match) {
        getAssembler().addBooleanTerm(getVariableColumn(), match);
        return;
    }


    /**
     *  Clears the variable query terms. 
     */

    @OSID @Override
    public void clearVariableTerms() {
        getAssembler().clearTerms(getVariableColumn());
        return;
    }


    /**
     *  Gets the variable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getVariableTerms() {
        return (getAssembler().getBooleanTerms(getVariableColumn()));
    }


    /**
     *  Orders the results by the variable capabilities. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariable(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVariableColumn(), style);
        return;
    }


    /**
     *  Gets the Variable column name.
     *
     * @return the column name
     */

    protected String getVariableColumn() {
        return ("variable");
    }


    /**
     *  Matches variable by percentage controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchVariableByPercentage(boolean match) {
        getAssembler().addBooleanTerm(getVariableByPercentageColumn(), match);
        return;
    }


    /**
     *  Clears the variable query terms. 
     */

    @OSID @Override
    public void clearVariableByPercentageTerms() {
        getAssembler().clearTerms(getVariableByPercentageColumn());
        return;
    }


    /**
     *  Gets the variable percentage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getVariableByPercentageTerms() {
        return (getAssembler().getBooleanTerms(getVariableByPercentageColumn()));
    }


    /**
     *  Orders the results by the variable percentage capabilities. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariablePercentage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVariableByPercentageColumn(), style);
        return;
    }


    /**
     *  Gets the VariableByPercentage column name.
     *
     * @return the column name
     */

    protected String getVariableByPercentageColumn() {
        return ("variable_by_percentage");
    }


    /**
     *  Matches variable minimums between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariableMinimum(java.math.BigDecimal start, 
                                     java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getVariableMinimumColumn(), start, end, match);
        return;
    }


    /**
     *  Matches any variable minimums. 
     *
     *  @param  match <code> true </code> to match controllers with variable 
     *          minimums, <code> false </code> to match controllers with no 
     *          variable minimums 
     */

    @OSID @Override
    public void matchAnyVariableMinimum(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getVariableMinimumColumn(), match);
        return;
    }


    /**
     *  Clears the variable minimum query terms. 
     */

    @OSID @Override
    public void clearVariableMinimumTerms() {
        getAssembler().clearTerms(getVariableMinimumColumn());
        return;
    }


    /**
     *  Gets the variable minimum query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariableMinimumTerms() {
        return (getAssembler().getDecimalRangeTerms(getVariableMinimumColumn()));
    }


    /**
     *  Orders the results by the variable minimum. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariableMinimum(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVariableMinimumColumn(), style);
        return;
    }


    /**
     *  Gets the VariableMinimum column name.
     *
     * @return the column name
     */

    protected String getVariableMinimumColumn() {
        return ("variable_minimum");
    }


    /**
     *  Matches variable maximums between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariableMaximum(java.math.BigDecimal start, 
                                     java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getVariableMaximumColumn(), start, end, match);
        return;
    }


    /**
     *  Matches any variable maximums. 
     *
     *  @param  match <code> true </code> to match controllers with variable 
     *          maximums, <code> false </code> to match controllers with no 
     *          variable maximums 
     */

    @OSID @Override
    public void matchAnyVariableMaximum(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getVariableMaximumColumn(), match);
        return;
    }


    /**
     *  Clears the variable maximum query terms. 
     */

    @OSID @Override
    public void clearVariableMaximumTerms() {
        getAssembler().clearTerms(getVariableMaximumColumn());
        return;
    }


    /**
     *  Gets the variable maximum query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getVariableMaximumTerms() {
        return (getAssembler().getDecimalRangeTerms(getVariableMaximumColumn()));
    }


    /**
     *  Orders the results by variable maximum. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVariableMaximum(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVariableMaximumColumn(), style);
        return;
    }


    /**
     *  Gets the VariableMaximum column name.
     *
     * @return the column name
     */

    protected String getVariableMaximumColumn() {
        return ("variable_maximum");
    }


    /**
     *  Matches discreet states controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchDiscreetStates(boolean match) {
        getAssembler().addBooleanTerm(getDiscreetStatesColumn(), match);
        return;
    }


    /**
     *  Clears the discreet states query terms. 
     */

    @OSID @Override
    public void clearDiscreetStatesTerms() {
        getAssembler().clearTerms(getDiscreetStatesColumn());
        return;
    }


    /**
     *  Gets the discreet query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDiscreetStatesTerms() {
        return (getAssembler().getBooleanTerms(getDiscreetStatesColumn()));
    }


    /**
     *  Orders the results by the variable by discreet state capabilities. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDiscreetStates(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDiscreetStatesColumn(), style);
        return;
    }


    /**
     *  Gets the DiscreetStates column name.
     *
     * @return the column name
     */

    protected String getDiscreetStatesColumn() {
        return ("discreet_states");
    }


    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDiscreetStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getDiscreetStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateIdTerms() {
        getAssembler().clearTerms(getDiscreetStateIdColumn());
        return;
    }


    /**
     *  Gets the state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDiscreetStateIdTerms() {
        return (getAssembler().getIdTerms(getDiscreetStateIdColumn()));
    }


    /**
     *  Gets the DiscreetStateId column name.
     *
     * @return the column name
     */

    protected String getDiscreetStateIdColumn() {
        return ("discreet_state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> State. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDiscreetStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getDiscreetStateQuery() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateQuery() is false");
    }


    /**
     *  Matches any discreet states. 
     *
     *  @param  match <code> true </code> to match controllers with discreet 
     *          states, <code> false </code> to match controllers with no 
     *          discreet states 
     */

    @OSID @Override
    public void matchAnyDiscreetState(boolean match) {
        getAssembler().addIdWildcardTerm(getDiscreetStateColumn(), match);
        return;
    }


    /**
     *  Clears the state query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateTerms() {
        getAssembler().clearTerms(getDiscreetStateColumn());
        return;
    }


    /**
     *  Gets the state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getDiscreetStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the DiscreetState column name.
     *
     * @return the column name
     */

    protected String getDiscreetStateColumn() {
        return ("discreet_state");
    }


    /**
     *  Sets the system <code> Id </code> for this query. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this controller supports the given record
     *  <code>Type</code>.
     *
     *  @param  controllerRecordType a controller record type 
     *  @return <code>true</code> if the controllerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type controllerRecordType) {
        for (org.osid.control.records.ControllerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  controllerRecordType the controller record type 
     *  @return the controller query record 
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(controllerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerQueryRecord getControllerQueryRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ControllerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  controllerRecordType the controller record type 
     *  @return the controller query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(controllerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerQueryInspectorRecord getControllerQueryInspectorRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ControllerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param controllerRecordType the controller record type
     *  @return the controller search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(controllerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerSearchOrderRecord getControllerSearchOrderRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ControllerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this controller. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param controllerQueryRecord the controller query record
     *  @param controllerQueryInspectorRecord the controller query inspector
     *         record
     *  @param controllerSearchOrderRecord the controller search order record
     *  @param controllerRecordType controller record type
     *  @throws org.osid.NullArgumentException
     *          <code>controllerQueryRecord</code>,
     *          <code>controllerQueryInspectorRecord</code>,
     *          <code>controllerSearchOrderRecord</code> or
     *          <code>controllerRecordTypecontroller</code> is
     *          <code>null</code>
     */
            
    protected void addControllerRecords(org.osid.control.records.ControllerQueryRecord controllerQueryRecord, 
                                      org.osid.control.records.ControllerQueryInspectorRecord controllerQueryInspectorRecord, 
                                      org.osid.control.records.ControllerSearchOrderRecord controllerSearchOrderRecord, 
                                      org.osid.type.Type controllerRecordType) {

        addRecordType(controllerRecordType);

        nullarg(controllerQueryRecord, "controller query record");
        nullarg(controllerQueryInspectorRecord, "controller query inspector record");
        nullarg(controllerSearchOrderRecord, "controller search odrer record");

        this.queryRecords.add(controllerQueryRecord);
        this.queryInspectorRecords.add(controllerQueryInspectorRecord);
        this.searchOrderRecords.add(controllerSearchOrderRecord);
        
        return;
    }
}
