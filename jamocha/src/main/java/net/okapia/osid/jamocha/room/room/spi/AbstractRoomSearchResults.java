//
// AbstractRoomSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRoomSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.room.RoomSearchResults {

    private org.osid.room.RoomList rooms;
    private final org.osid.room.RoomQueryInspector inspector;
    private final java.util.Collection<org.osid.room.records.RoomSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRoomSearchResults.
     *
     *  @param rooms the result set
     *  @param roomQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>rooms</code>
     *          or <code>roomQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRoomSearchResults(org.osid.room.RoomList rooms,
                                            org.osid.room.RoomQueryInspector roomQueryInspector) {
        nullarg(rooms, "rooms");
        nullarg(roomQueryInspector, "room query inspectpr");

        this.rooms = rooms;
        this.inspector = roomQueryInspector;

        return;
    }


    /**
     *  Gets the room list resulting from a search.
     *
     *  @return a room list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.room.RoomList getRooms() {
        if (this.rooms == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.room.RoomList rooms = this.rooms;
        this.rooms = null;
	return (rooms);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.room.RoomQueryInspector getRoomQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  room search record <code> Type. </code> This method must
     *  be used to retrieve a room implementing the requested
     *  record.
     *
     *  @param roomSearchRecordType a room search 
     *         record type 
     *  @return the room search
     *  @throws org.osid.NullArgumentException
     *          <code>roomSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(roomSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.RoomSearchResultsRecord getRoomSearchResultsRecord(org.osid.type.Type roomSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.room.records.RoomSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(roomSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(roomSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record room search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRoomRecord(org.osid.room.records.RoomSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "room record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
