//
// AbstractAgencyQueryInspector.java
//
//     A template for making an AgencyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for agencies.
 */

public abstract class AbstractAgencyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.authentication.AgencyQueryInspector {

    private final java.util.Collection<org.osid.authentication.records.AgencyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the ancestor agency <code> Id </code> terms. 
     *
     *  @return the ancestor agency <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAgencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor agency terms. 
     *
     *  @return the ancestor agency terms 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQueryInspector[] getAncestorAgencyTerms() {
        return (new org.osid.authentication.AgencyQueryInspector[0]);
    }


    /**
     *  Gets the descendant agency <code> Id </code> terms. 
     *
     *  @return the descendant agency <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAgencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant agency terms. 
     *
     *  @return the descendant agency terms 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQueryInspector[] getDescendantAgencyTerms() {
        return (new org.osid.authentication.AgencyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given agency query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an agency implementing the requested record.
     *
     *  @param agencyRecordType an agency record type
     *  @return the agency query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agencyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgencyQueryInspectorRecord getAgencyQueryInspectorRecord(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgencyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agencyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agency query. 
     *
     *  @param agencyQueryInspectorRecord agency query inspector
     *         record
     *  @param agencyRecordType agency record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgencyQueryInspectorRecord(org.osid.authentication.records.AgencyQueryInspectorRecord agencyQueryInspectorRecord, 
                                                   org.osid.type.Type agencyRecordType) {

        addRecordType(agencyRecordType);
        nullarg(agencyRecordType, "agency record type");
        this.records.add(agencyQueryInspectorRecord);        
        return;
    }
}
