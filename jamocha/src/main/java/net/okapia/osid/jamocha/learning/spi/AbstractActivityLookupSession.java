//
// AbstractActivityLookupSession.java
//
//    A starter implementation framework for providing an Activity
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Activity
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getActivities(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractActivityLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ActivityLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();
    

    /**
     *  Gets the <code>ObjectiveBank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>ObjectiveBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.objectiveBank.getId());
    }


    /**
     *  Gets the <code>ObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the <code>ObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.objectiveBank);
    }


    /**
     *  Sets the <code>ObjectiveBank</code>.
     *
     *  @param  objectiveBank the objective bank for this session
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>
     */

    protected void setObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        nullarg(objectiveBank, "objective bank");
        this.objectiveBank = objectiveBank;
        return;
    }


    /**
     *  Tests if this user can perform <code>Activity</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (true);
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Activity</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Activity</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Activity</code> and
     *  retained for compatibility.
     *
     *  @param  activityId <code>Id</code> of the
     *          <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.learning.ActivityList activities = getActivities()) {
            while (activities.hasNext()) {
                org.osid.learning.Activity activity = activities.getNextActivity();
                if (activity.getId().equals(activityId)) {
                    return (activity);
                }
            }
        } 

        throw new org.osid.NotFoundException(activityId + " not found");
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActivities()</code>.
     *
     *  @param  activityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.learning.Activity> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = activityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getActivity(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("activity " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.learning.activity.LinkedActivityList(ret));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.activity.ActivityGenusFilterList(getActivities(), activityGenusType));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> and include any additional
     *  activities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivitiesByGenusType(activityGenusType));
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivities()</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.activity.ActivityRecordFilterList(getActivities(), activityRecordType));
    }


    /**
     *  Gets the activities for the given objective. In plenary mode,
     *  the returned list contains all of the activities mapped to the
     *  objective <code>Id</code> or an error results if an Id in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible <code>Activities</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  @param  objectiveId <code>Id</code> of the <code>Objective</code> 
     *  @return list of enrollments 
     *  @throws org.osid.NotFoundException <code>objectiveId</code>
     *          not found
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesForObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.activity.ActivityFilterList(new ObjectiveFilter(objectiveId), getActivities()));
    }        


    /**
     *  Gets the activities for the given objectives. In plenary mode,
     *  the returned list contains all of the activities specified in
     *  the objective <code>Id</code> list, in the order of the list,
     *  including duplicates, or an error results if a course offering
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  objectiveIds a list of objective <code>Ids</code> 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException an <code>objectiveId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveIds</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesForObjectives(org.osid.id.IdList objectiveIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.activity.CompositeActivityList ret = new net.okapia.osid.jamocha.adapter.federator.learning.activity.CompositeActivityList();

        try (org.osid.id.IdList ids = objectiveIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                ret.addActivityList(getActivitiesForObjective(id));
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the activities for the given asset. In plenary mode, the
     *  returned list contains all of the activities mapped to the
     *  asset <code>Id</code> or an error results if an <code> Id
     *  </code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Activities
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  @param  assetId <code>Id</code> of an <code>Asset</code> 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException <code>assetId</code> not found 
     *  @throws org.osid.NullArgumentException <code>assetId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByAsset(org.osid.id.Id assetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.learning.Activity> ret = new java.util.ArrayList<>();

        try (org.osid.learning.ActivityList activities = getActivities()) {
            while (activities.hasNext()) {
                org.osid.learning.Activity activity = activities.getNextActivity();
                if (activity.isAssetBasedActivity()) {
                    try (org.osid.id.IdList ids = activity.getAssetIds()) {
                        while (ids.hasNext()) {
                            if (ids.getNextId().equals(assetId)) {
                                ret.add(activity);
                            }
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.learning.activity.LinkedActivityList(ret));
    }


    /**
     *  Gets the activities for the given asset. In plenary mode, the
     *  returned list contains all of the activities mapped to the
     *  asset <code>Id</code> or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assetIds <code>Ids</code> of <code>Assets</code> 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException an <code>assetId</code> not found 
     *  @throws org.osid.NullArgumentException <code>assetIds/code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByAssets(org.osid.id.IdList assetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.activity.CompositeActivityList ret = new net.okapia.osid.jamocha.adapter.federator.learning.activity.CompositeActivityList();

        try (org.osid.id.IdList ids = assetIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                ret.addActivityList(getActivitiesByAsset(id));
            }
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>Activities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.learning.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the activity list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of activities
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.learning.ActivityList filterActivitiesOnViews(org.osid.learning.ActivityList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class ObjectiveFilter
        implements net.okapia.osid.jamocha.inline.filter.learning.activity.ActivityFilter {
         
        private final org.osid.id.Id objectiveId;
         
         
        /**
         *  Constructs a new <code>ObjectiveFilter</code>.
         *
         *  @param objectiveId the objective to filter
         *  @throws org.osid.NullArgumentException
         *          <code>objectiveId</code> is <code>null</code>
         */
        
        public ObjectiveFilter(org.osid.id.Id objectiveId) {
            nullarg(objectiveId, "objective Id");
            this.objectiveId = objectiveId;
            return;
        }

         
        /**
         *  Used by the ActivityFilterList to filter the 
         *  activity list based on objective.
         *
         *  @param activity the activity
         *  @return <code>true</code> to pass the activity,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.learning.Activity activity) {
            return (activity.getObjectiveId().equals(this.objectiveId));
        }
    }
}
