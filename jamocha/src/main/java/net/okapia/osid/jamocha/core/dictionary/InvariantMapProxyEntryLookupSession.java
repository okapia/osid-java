//
// InvariantMapProxyEntryLookupSession
//
//    Implements an Entry lookup service backed by a fixed
//    collection of entries. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  Implements an Entry lookup service backed by a fixed
 *  collection of entries. The entries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyEntryLookupSession
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractMapEntryLookupSession
    implements org.osid.dictionary.EntryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyEntryLookupSession} with no
     *  entries.
     *
     *  @param dictionary the dictionary
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionary} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyEntryLookupSession(org.osid.dictionary.Dictionary dictionary,
                                                  org.osid.proxy.Proxy proxy) {
        setDictionary(dictionary);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyEntryLookupSession} with a single
     *  entry.
     *
     *  @param dictionary the dictionary
     *  @param entry an single entry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionary},
     *          {@code entry} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyEntryLookupSession(org.osid.dictionary.Dictionary dictionary,
                                                  org.osid.dictionary.Entry entry, org.osid.proxy.Proxy proxy) {

        this(dictionary, proxy);
        putEntry(entry);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyEntryLookupSession} using
     *  an array of entries.
     *
     *  @param dictionary the dictionary
     *  @param entries an array of entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionary},
     *          {@code entries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyEntryLookupSession(org.osid.dictionary.Dictionary dictionary,
                                                  org.osid.dictionary.Entry[] entries, org.osid.proxy.Proxy proxy) {

        this(dictionary, proxy);
        putEntries(entries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyEntryLookupSession} using a
     *  collection of entries.
     *
     *  @param dictionary the dictionary
     *  @param entries a collection of entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionary},
     *          {@code entries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyEntryLookupSession(org.osid.dictionary.Dictionary dictionary,
                                                  java.util.Collection<? extends org.osid.dictionary.Entry> entries,
                                                  org.osid.proxy.Proxy proxy) {

        this(dictionary, proxy);
        putEntries(entries);
        return;
    }
}
