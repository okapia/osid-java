//
// AbstractImmutableDemographic.java
//
//     Wraps a mutable Demographic to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Demographic</code> to hide modifiers. This
 *  wrapper provides an immutized Demographic from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying demographic whose state changes are visible.
 */

public abstract class AbstractImmutableDemographic
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.resource.demographic.Demographic {

    private final org.osid.resource.demographic.Demographic demographic;


    /**
     *  Constructs a new <code>AbstractImmutableDemographic</code>.
     *
     *  @param demographic the demographic to immutablize
     *  @throws org.osid.NullArgumentException <code>demographic</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDemographic(org.osid.resource.demographic.Demographic demographic) {
        super(demographic);
        this.demographic = demographic;
        return;
    }


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code> whose 
     *  members are added to this demographic. The union of these demographics 
     *  is added to this demographic. 
     *
     *  @return the demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedDemographicIds() {
        return (this.demographic.getIncludedDemographicIds());
    }


    /**
     *  Gets a list of <code> Demographics </code> whose members are added to 
     *  this demographic. The union of these demographics is added to this 
     *  demographic. 
     *
     *  @return the demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getIncludedDemographics()
        throws org.osid.OperationFailedException {

        return (this.demographic.getIncludedDemographics());
    }


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code> whose 
     *  intersection is added to this demographic. The intersection of these 
     *  demographics is added to this demographic. 
     *
     *  @return the intersecting demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedIntersectingDemographicIds() {
        return (this.demographic.getIncludedIntersectingDemographicIds());
    }


    /**
     *  Gets a list of <code> Demographics </code> whose intersection is added 
     *  to this demographic. The intersection of these demographics is added 
     *  to this demographic. 
     *
     *  @return the intersecting demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getIncludedIntersectingDemographics()
        throws org.osid.OperationFailedException {

        return (this.demographic.getIncludedIntersectingDemographics());
    }


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code> whose 
     *  non-members are added to this demographic. 
     *
     *  @return the exclusive demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedExclusiveDemographicIds() {
        return (this.demographic.getIncludedExclusiveDemographicIds());
    }


    /**
     *  Gets a list of <code> Demographics </code> whose non-members are added 
     *  to this demographic. 
     *
     *  @return the exclusive demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getIncludedExclusiveDemographics()
        throws org.osid.OperationFailedException {

        return (this.demographic.getIncludedExclusiveDemographics());
    }


    /**
     *  Gets a list of <code> Demographic </code> <code> Ids </code> whose 
     *  members are subtracted from in this demographic. 
     *
     *  @return the excluded demographic <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getExcludedDemographicIds() {
        return (this.demographic.getExcludedDemographicIds());
    }


    /**
     *  Gets a list of <code> Demographics </code> whose members subtracted 
     *  from this demographic. 
     *
     *  @return the excluded demographics 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getExcludedDemographics()
        throws org.osid.OperationFailedException {

        return (this.demographic.getExcludedDemographics());
    }


    /**
     *  Gets a list of individual <code> Resource </code> <code> Ids </code> 
     *  to be added to this demographic. 
     *
     *  @return the included resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getIncludedResourceIds() {
        return (this.demographic.getIncludedResourceIds());
    }


    /**
     *  Gets a list of individual <code> Resources </code> to be added to this 
     *  demographic. 
     *
     *  @return the included resources 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getIncludedResources()
        throws org.osid.OperationFailedException {

        return (this.demographic.getIncludedResources());
    }


    /**
     *  Gets a list of <code> Resource </code> <code> Ids </code> to be 
     *  subtracted from this demographic. 
     *
     *  @return the excluded resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getExcludedResourceIds() {
        return (this.demographic.getExcludedResourceIds());
    }


    /**
     *  Gets a list of <code> Resources </code> to be subtracted from this 
     *  demographic. 
     *
     *  @return the exempted resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getExcludedResources()
        throws org.osid.OperationFailedException {

        return (this.demographic.getExcludedResources());
    }


    /**
     *  Gets the demographic record corresponding to the given <code> 
     *  Demographic </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  demographicRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(demographicRecordType) </code> is <code> true </code> . 
     *
     *  @param  demographicRecordType the type of demographic record to 
     *          retrieve 
     *  @return the demographic record 
     *  @throws org.osid.NullArgumentException <code> demographicRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(demographicRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicRecord getDemographicRecord(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException {

        return (this.demographic.getDemographicRecord(demographicRecordType));
    }
}

