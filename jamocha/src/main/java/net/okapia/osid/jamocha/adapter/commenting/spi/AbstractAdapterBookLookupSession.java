//
// AbstractAdapterBookLookupSession.java
//
//    A Book lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.commenting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Book lookup session adapter.
 */

public abstract class AbstractAdapterBookLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.commenting.BookLookupSession {

    private final org.osid.commenting.BookLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBookLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBookLookupSession(org.osid.commenting.BookLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Book} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBooks() {
        return (this.session.canLookupBooks());
    }


    /**
     *  A complete view of the {@code Book} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBookView() {
        this.session.useComparativeBookView();
        return;
    }


    /**
     *  A complete view of the {@code Book} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBookView() {
        this.session.usePlenaryBookView();
        return;
    }

     
    /**
     *  Gets the {@code Book} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Book} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Book} and
     *  retained for compatibility.
     *
     *  @param bookId {@code Id} of the {@code Book}
     *  @return the book
     *  @throws org.osid.NotFoundException {@code bookId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code bookId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBook(bookId));
    }


    /**
     *  Gets a {@code BookList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  books specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Books} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  bookIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Book} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code bookIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByIds(org.osid.id.IdList bookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBooksByIds(bookIds));
    }


    /**
     *  Gets a {@code BookList} corresponding to the given
     *  book genus {@code Type} which does not include
     *  books of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned {@code Book} list
     *  @throws org.osid.NullArgumentException
     *          {@code bookGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBooksByGenusType(bookGenusType));
    }


    /**
     *  Gets a {@code BookList} corresponding to the given
     *  book genus {@code Type} and include any additional
     *  books with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned {@code Book} list
     *  @throws org.osid.NullArgumentException
     *          {@code bookGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByParentGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBooksByParentGenusType(bookGenusType));
    }


    /**
     *  Gets a {@code BookList} containing the given
     *  book record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookRecordType a book record type 
     *  @return the returned {@code Book} list
     *  @throws org.osid.NullArgumentException
     *          {@code bookRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByRecordType(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBooksByRecordType(bookRecordType));
    }


    /**
     *  Gets a {@code BookList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Book} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBooksByProvider(resourceId));
    }


    /**
     *  Gets all {@code Books}. 
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Books} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBooks());
    }
}
