//
// AbstractImmutableObstacle.java
//
//     Wraps a mutable Obstacle to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.obstacle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Obstacle</code> to hide modifiers. This
 *  wrapper provides an immutized Obstacle from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying obstacle whose state changes are visible.
 */

public abstract class AbstractImmutableObstacle
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.mapping.path.Obstacle {

    private final org.osid.mapping.path.Obstacle obstacle;


    /**
     *  Constructs a new <code>AbstractImmutableObstacle</code>.
     *
     *  @param obstacle the obstacle to immutablize
     *  @throws org.osid.NullArgumentException <code>obstacle</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableObstacle(org.osid.mapping.path.Obstacle obstacle) {
        super(obstacle);
        this.obstacle = obstacle;
        return;
    }


    /**
     *  Gets the path <code> Id. </code> 
     *
     *  @return the <code> Id </code> of the path 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.obstacle.getPathId());
    }


    /**
     *  Gets the path. 
     *
     *  @return the path 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.obstacle.getPath());
    }


    /**
     *  Gets the starting coordinate of the obstacle on the path. 
     *
     *  @return the start of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getStartingCoordinate() {
        return (this.obstacle.getStartingCoordinate());
    }


    /**
     *  Gets the ending coordinate of the obstacle on the path. 
     *
     *  @return the end of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getEndingCoordinate() {
        return (this.obstacle.getEndingCoordinate());
    }


    /**
     *  Gets the obstacle record corresponding to the given <code> Obstacle 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record.. The <code> 
     *  obstacleRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(obstacleRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  obstacleRecordType the type of obstacle record to retrieve 
     *  @return the obstacle record 
     *  @throws org.osid.NullArgumentException <code> obstacleRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(obstacleRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.records.ObstacleRecord getObstacleRecord(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException {

        return (this.obstacle.getObstacleRecord(obstacleRecordType));
    }
}

