//
// CurrencyMetadata.java
//
//     Defines a currency Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a currency Metadata.
 */

public final class CurrencyMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractCurrencyMetadata 
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code CurrencyMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public CurrencyMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code CurrencyMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public CurrencyMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code CurrencyMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public CurrencyMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
	return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *  	is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }


    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }

    
    /**
     *  Add support for a currency type.
     *
     *  @param currencyType the type of currency
     *  @throws org.osid.NullArgumentException {@code currencyType} is
     *          {@code null}
     */

    public void addCurrencyType(org.osid.type.Type currencyType) {
        super.addCurrencyType(currencyType);
        return;
    }


    /**
     *  Sets the currency set.
     *
     *  @param values a collection of accepted currency values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setCurrencySet(java.util.Collection<org.osid.financials.Currency> values) {
        super.setCurrencySet(values);
        return;
    }


    /**
     *  Adds a collection of values to the currency set.
     *
     *  @param values a collection of accepted currency values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToCurrencySet(java.util.Collection<org.osid.financials.Currency> values) {
        super.addToCurrencySet(values);
        return;
    }


    /**
     *  Adds a value to the currency set.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addToCurrencySet(org.osid.financials.Currency value) {
        super.addToCurrencySet(value);
        return;
    }


    /**
     *  Removes a value from the currency set.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeFromCurrencySet(org.osid.financials.Currency value) {
        super.removeFromCurrencySet(value);
        return;
    }


    /**
     *  Clears the currency set.
     */

    public void clearCurrencySet() {
        super.clearCurrencySet();
        return;
    }


    /**
     *  Sets the default currency set.
     *
     *  @param values a collection of default currency values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        super.setDefaultCurrencyValues(values);
        return;
    }


    /**
     *  Adds a collection of default currency values.
     *
     *  @param values a collection of default currency values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        super.addDefaultCurrencyValues(values);
        return;
    }


    /**
     *  Adds a default currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultCurrencyValue(org.osid.financials.Currency value) {
        super.addDefaultCurrencyValue(value);
        return;
    }


    /**
     *  Removes a default currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultCurrencyValue(org.osid.financials.Currency value) {
        super.removeDefaultCurrencyValue(value);
        return;
    }


    /**
     *  Clears the default currency values.
     */

    public void clearDefaultCurrencyValues() {
        super.clearDefaultCurrencyValues();
        return;
    }


    /**
     *  Sets the existing currency set.
     *
     *  @param values a collection of existing currency values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        super.setExistingCurrencyValues(values);
        return;
    }


    /**
     *  Adds a collection of existing currency values.
     *
     *  @param values a collection of existing currency values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        super.addExistingCurrencyValues(values);
        return;
    }


    /**
     *  Adds a existing currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingCurrencyValue(org.osid.financials.Currency value) {
        super.addExistingCurrencyValue(value);
        return;
    }


    /**
     *  Removes a existing currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingCurrencyValue(org.osid.financials.Currency value) {
        super.removeExistingCurrencyValue(value);
        return;
    }


    /**
     *  Clears the existing currency values.
     */

    public void clearExistingCurrencyValues() {
        super.clearExistingCurrencyValues();
        return;
    }    
}
