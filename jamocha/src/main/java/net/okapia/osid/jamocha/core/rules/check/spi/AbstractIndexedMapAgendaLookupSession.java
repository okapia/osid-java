//
// AbstractIndexedMapAgendaLookupSession.java
//
//    A simple framework for providing an Agenda lookup service
//    backed by a fixed collection of agendas with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Agenda lookup service backed by a
 *  fixed collection of agendas. The agendas are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some agendas may be compatible
 *  with more types than are indicated through these agenda
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Agendas</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAgendaLookupSession
    extends AbstractMapAgendaLookupSession
    implements org.osid.rules.check.AgendaLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.rules.check.Agenda> agendasByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.check.Agenda>());
    private final MultiMap<org.osid.type.Type, org.osid.rules.check.Agenda> agendasByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.check.Agenda>());


    /**
     *  Makes an <code>Agenda</code> available in this session.
     *
     *  @param  agenda an agenda
     *  @throws org.osid.NullArgumentException <code>agenda<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAgenda(org.osid.rules.check.Agenda agenda) {
        super.putAgenda(agenda);

        this.agendasByGenus.put(agenda.getGenusType(), agenda);
        
        try (org.osid.type.TypeList types = agenda.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.agendasByRecord.put(types.getNextType(), agenda);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an agenda from this session.
     *
     *  @param agendaId the <code>Id</code> of the agenda
     *  @throws org.osid.NullArgumentException <code>agendaId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAgenda(org.osid.id.Id agendaId) {
        org.osid.rules.check.Agenda agenda;
        try {
            agenda = getAgenda(agendaId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.agendasByGenus.remove(agenda.getGenusType());

        try (org.osid.type.TypeList types = agenda.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.agendasByRecord.remove(types.getNextType(), agenda);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAgenda(agendaId);
        return;
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  agenda genus <code>Type</code> which does not include
     *  agendas of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known agendas or an error results. Otherwise,
     *  the returned list may contain only those agendas that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.agenda.ArrayAgendaList(this.agendasByGenus.get(agendaGenusType)));
    }


    /**
     *  Gets an <code>AgendaList</code> containing the given
     *  agenda record <code>Type</code>. In plenary mode, the
     *  returned list contains all known agendas or an error
     *  results. Otherwise, the returned list may contain only those
     *  agendas that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  agendaRecordType an agenda record type 
     *  @return the returned <code>agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByRecordType(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.agenda.ArrayAgendaList(this.agendasByRecord.get(agendaRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.agendasByGenus.clear();
        this.agendasByRecord.clear();

        super.close();

        return;
    }
}
